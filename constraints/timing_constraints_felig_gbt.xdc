#create 40 MHz TTC clock
create_clock -name clk_ttc_40 -period 24.95 [get_pins */ttc_dec/from_cdr_to_AandB/clock_iter/O]


#MT already felix_top*v20.xdc create_clock -period 6.237 -name clk_adn_160 [get_ports CLK_TTC_P]


create_clock -period 10.000 -name sys_clk0_p -waveform {0.000 5.000} [get_ports {sys_clk_p[0]}]
create_clock -quiet -period 10.000 -name sys_clk1_p -waveform {0.000 5.000} [get_ports {sys_clk_p[1]}]

#MT already felix_top*v20.xdc create_clock -name emcclk -period 20.000 [get_ports emcclk]
#create_clock -name u2/GT_TX_WORD_CLK[0] -period 4.158 [get_pins u2/clk_generate[0].GTTXOUTCLK_BUFG/O]

#set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets clk0/g_200M.clk0/inst/clk40]
set_property -quiet CLOCK_DEDICATED_ROUTE FALSE [get_nets emcclk_IBUF_inst/O]

#We need this line when we move to the second SRL
create_clock -name GTHREFCLK_0 -period 4.158 [get_ports {GTREFCLK_P_IN[0]}]
create_clock -quiet -name GTHREFCLK_1 -period 4.158 [get_ports {GTREFCLK_P_IN[1]}]
create_clock -quiet -name GTHREFCLK_2 -period 4.158 [get_ports {GTREFCLK_P_IN[2]}]
create_clock -quiet -name GTHREFCLK_3 -period 4.158 [get_ports {GTREFCLK_P_IN[3]}]
create_clock -quiet -name GTHREFCLK_4 -period 4.158 [get_ports {GTREFCLK_P_IN[4]}]

create_clock -name GT_TX_WORD_CLK[0] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[0].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[1] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[0].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[2] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[0].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[3] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[0].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[4] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[1].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[5] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[1].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[6] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[1].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[7] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[1].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[8] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[2].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[9] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[2].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[10] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[2].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[11] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[2].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[12] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[3].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[13] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[3].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[14] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[3].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[15] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[3].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[16] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[4].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[17] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[4].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[18] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[4].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[19] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[4].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[20] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[5].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[21] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[5].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[22] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[5].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[23] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[5].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[24] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[6].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[25] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[6].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[26] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[6].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[27] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[6].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[28] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[7].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[29] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[7].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[30] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[7].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[31] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[7].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[32] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[8].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[33] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[8].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[34] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[8].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[35] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[8].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[36] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[9].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[37] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[9].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[38] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[9].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[39] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[9].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[40] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[10].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[41] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[10].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[42] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[10].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[43] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[10].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[44] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[11].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[45] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[11].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[46] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[11].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[47] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[11].GTH_TOP_INST/txoutclk_out[3]"}]

create_clock -name GT_RX_WORD_CLK[0] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[0].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[1] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[0].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[2] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[0].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[3] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[0].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[4] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[1].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[5] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[1].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[6] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[1].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[7] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[1].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[8] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[2].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[9] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[2].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[10] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[2].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[11] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[2].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[12] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[3].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[13] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[3].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[14] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[3].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[15] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[3].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[16] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[4].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[17] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[4].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[18] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[4].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[19] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[4].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[20] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[5].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[21] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[5].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[22] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[5].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[23] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[5].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[24] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[6].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[25] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[6].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[26] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[6].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[27] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[6].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[28] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[7].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[29] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[7].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[30] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[7].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[31] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[7].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[32] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[8].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[33] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[8].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[34] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[8].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[35] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[8].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[36] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[9].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[37] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[9].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[38] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[9].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[39] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[9].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[40] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[10].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[41] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[10].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[42] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[10].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[43] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[10].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[44] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[11].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[45] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[11].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[46] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[11].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[47] -period 4.167 [get_pins -hierarchical -filter {NAME =~ "*/g_GBTMODE.u1/*PLL_GEN.GTH_inst[11].GTH_TOP_INST/rxoutclk_out[3]"}]

create_clock -quiet -name i2c_clock_pex -period 2500 [get_pins hk0/g_711_712.pex_init0/data_clk_reg/Q]

#added FELIG
create_clock -period 4.167 -name cdrclk_in [get_pins g_GBTMODE.u1/ibufds_instq2_clk0/O]
create_clock -period 4.167 -name CXP2_GTH_RefClk [get_pins g_GBTMODE.u1/ibufds_instq8_clk0/O]
create_clock -period 4.167 -name CXP1_GTH_RefClk_LMK [get_pins g_GBTMODE.u1/ibufds_LMK1/O] 
create_clock -period 4.167 -name CXP2_GTH_RefClk_LMK [get_pins g_GBTMODE.u1/ibufds_LMK2/O] 
create_clock -period 4.167 -name CXP3_GTH_RefClk [get_pins g_GBTMODE.u1/ibufds_instq4_clk0/O]
create_clock -period 4.167 -name CXP4_GTH_RefClk [get_pins g_GBTMODE.u1/ibufds_instq5_clk0/O]
create_clock -period 4.167 -name CXP5_GTH_RefClk [get_pins g_GBTMODE.u1/refclkgen_v2p0.g_refclk12.ibufds_instq6_clk0/O]

#Clock domain crossings within Central Router
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk160_clk_wiz_40_0*] 24.950

set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk80_clk_wiz_40_0*] 12.475
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk160_clk_wiz_40_0*] 6.237

set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 4.170
#set_max_delay -from [get_pins */rst0/cr_rst_rr_reg*/C] -to [get_clocks clk160_clk_wiz_40_0*] 6.237


#Clock domain crossings in GBT
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks {GT_TX_WORD_CLK[*]}] 6.237
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks {GT_RX_WORD_CLK[*]}] 6.237
set_max_delay -quiet -datapath_only -from [get_clocks clk40_clk_wiz_156_0*] -to [get_clocks {GT_TX_WORD_CLK[*]}] 24.950
set_max_delay -from [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/*/bit_synchronizer_rxcdrlock_inst/i_in_out_reg/C"}] -to [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/*/gbtRxTx[*].BITSLIP_MANUAL_r_reg[*]/D"}] 24.95

#Clock domain crossings in TTC decoder
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk_ttc_40] 6.237
set_max_delay -datapath_only -from [get_clocks clk_ttc_40] -to [get_clocks clk_adn_160] 6.237
set_max_delay -from [get_pins u2/ttc_dec/from_cdr_to_AandB/ttcclk_reg/Q] -to [get_pins u2/ttc_dec/from_cdr_to_AandB/ttcclk_reg/D] 6.237

#Clock domain crossings between Wupper and Central Router
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_*] -to [get_clocks *rd_clk] 4.000


#Register map to 400kHz I2C clock
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk400] 100.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks i2c_clock_pex] 100.000

#Register map to central router clocks
set_max_delay -datapath_only -from [get_clocks *rd_clk] -to [get_clocks clk80_clk_wiz_40_0*] 12.475


#Register map control / monitor
set_max_delay -datapath_only -from [get_clocks clk_out25*] -to [get_clocks *] 40.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk_out25*] 40.000

#Clock domain crossings in ttc decoder
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk_ttc_40] 24.95

#Clock domain crossings between Central Router and GBT
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks txoutclk*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {GT_RX_WORD_CLK[*]}] 24.950
set_max_delay -datapath_only -from [get_clocks txoutclk*] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks {GT_RX_WORD_CLK[*]}] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks {GT_TX_WORD_CLK[*]}] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks {GT_TX_WORD_CLK[0]}] 6.237
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {GT_TX_WORD_CLK[*]}] 24.950

# added #FELIG
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk10_clk_wiz_200_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_200_0*] -to [get_clocks {GT_TX_WORD_CLK[*]}] 24.950
set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks {GT_RX_WORD_CLK[*]}] 4.170

set_max_delay -from [get_pins -hierarchical -filter {NAME =~ "g_endpoints[*].crth0/rst0/*/C"}] 24.95
set_max_delay -from [get_pins -hierarchical -filter {NAME =~ "g_endpoints[*].crfh0/rst0/*/C"}] 24.95
set_max_delay -from [get_pins -hierarchical -filter {NAME =~ "g_endpoints[*].pcie0/regsync0/register_map_40_control_reg*/C"}] 24.95
set_max_delay -from [get_pins {g_endpoints[*].decoding0/g_gbtmode.g_GBT_Links[*].g_Egroups[*].PathEnable_reg[*]/C}] 24.95
set_max_delay -from [get_pins {g_endpoints[*].pcie0/regsync0/rst_soft_40*/C}]  24.95
set_max_delay -from [get_pins  -hierarchical -filter {NAME =~ "*/rxalign_auto[*].auto_rxrst/AUTO_GBT_RXRST_reg/C"}] 24.95
set_max_delay -from [get_pins  -hierarchical -filter {NAME =~ "*/QPLL_GEN.GTH_inst[*].RxCdrLock_int_reg[*]/C"}] 24.95
set_max_delay -from [get_pins  -hierarchical -filter {NAME =~ "*/rxalign_auto[*].rafsm/RxSlide_reg/C"}] 24.95
set_max_delay -from [get_pins  -hierarchical -filter {NAME =~ "**/rxalign_auto[*].alignment_done_f_reg[*]/C"}] 24.95
#set_max_delay -from [get_pins {g_endpoints[*].decoding0/g_gbtmode.g_GBT_Links[*].g_Egroups[*].eGroup0/g_Epaths[*].Epath0/toAxis0/g_onebyte.s_axis_reg[tuser][*]*/C}] -to [get_pins {g_endpoints[*].decoding0/g_gbtmode.g_GBT_Links[*].g_Egroups[*].eGroup0/g_Epaths[*].Epath0/toAxis0/fifo0/g_BIF.trunc_sync_v_reg*/D}] 4
#set_max_delay -from [get_pins {g_endpoints[*].decoding0/g_gbtmode.g_GBT_Links[*].g_Egroups[*].eGroup0/g_Epaths[*].Epath0/toAxis0/g_onebyte.s_axis_reg[tuser][*]*/C}] -to [get_pins {g_endpoints[*].decoding0/g_gbtmode.g_GBT_Links[*].g_Egroups[*].eGroup0/g_Epaths[*].Epath0/toAxis0/fifo0/g_BIF.busy_sync_v_reg*/D}] 4

#Only used so far to report busy.
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
#Used for link alignment.
set_max_delay -datapath_only -from [get_clocks rxoutclk_out*]  -to [get_clocks clk40_clk_wiz_40_0*] 24.95
set_max_delay -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk40_clk_wiz_40_0*] 6.237
set_max_delay -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk_ttc_40*] 6.237
###TX240 sample TX_FRAME_CLK
set_multicycle_path 2 -setup -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "*timedomaincrossing_C/TX_FRAMECLK_I*/D"}]
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_*] -to [get_clocks clk40_clk_wiz_40_*] 24.950
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 4.170

#switchable output clock can switch at any time
set_false_path -from [get_clocks clk_adn_160] -to [get_clocks clk160_clk_wiz_40_0]

#RL
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks GT_TX_WORD_CLK[*]] 4.170
set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks clk240_clk_wiz_40_0*] 4.170

#Multicycle paths in the RxGearbox
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 4
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 3
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 4
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 3
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 4
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 3
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 4
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 3

#TX side
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 3
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 3
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 1

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 1

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0

##lpgbt
#set_multicycle_path -setup -from [get_pins -hierarchical -filter {NAME =~ "*LpGBT_FPGA_Uplink_datapath_inst*descrambledData_reg[*]/C"}] 3
#set_multicycle_path -hold -from [get_pins -hierarchical -filter {NAME =~ "*LpGBT_FPGA_Uplink_datapath_inst*descrambledData_reg[*]/C"}] 2

#set_multicycle_path -setup -from [get_pins -hierarchical -filter {NAME =~ "*LpGBT_Model_dataPath_inst*scrambledData_reg[*]/C"}] 2
#set_multicycle_path -hold -from [get_pins -hierarchical -filter {NAME =~ "*LpGBT_Model_dataPath_inst*scrambledData_reg[*]/C"}] 1

#set_multicycle_path -setup -from [get_pins -hierarchical -filter { NAME =~ "*LpGBT_FPGA_Uplink_datapath_inst/uplinkFrame_pipelined_s_reg[*]/C"}] 3
#set_multicycle_path -hold -from [get_pins -hierarchical -filter { NAME =~ "*LpGBT_FPGA_Uplink_datapath_inst/uplinkFrame_pipelined_s_reg[*]/C"}] 2
#set_multicycle_path -setup -from [get_pins -hierarchical -filter {NAME =~ "*LpGBT_FPGA_Uplink_datapath_inst*descrambledData_reg[*]/C"}] 3
#set_multicycle_path -hold -from [get_pins -hierarchical -filter {NAME =~ "*LpGBT_FPGA_Uplink_datapath_inst*descrambledData_reg[*]/C"}] 2

#create_clock -period 3.125 -name LMK0_REFCLK [get_pins g_LPGBTMODE.u2/Reference_Clk_Gen/lmk0_gen/O]
#create_clock -period 3.125 -name LMK1_REFCLK [get_pins g_LPGBTMODE.u2/Reference_Clk_Gen/lmk1_gen/O]
#create_clock -period 3.125 -name LMK2_REFCLK [get_pins g_LPGBTMODE.u2/Reference_Clk_Gen/lmk2_gen/O]
#create_clock -period 3.125 -name LMK3_REFCLK [get_pins g_LPGBTMODE.u2/Reference_Clk_Gen/lmk3_gen/O]
#create_clock -period 3.125 -name LMK4_REFCLK [get_pins g_LPGBTMODE.u2/Reference_Clk_Gen/lmk4_gen/O]
#create_clock -period 3.125 -name LMK5_REFCLK [get_pins g_LPGBTMODE.u2/Reference_Clk_Gen/lmk5_gen/O]
#create_clock -period 3.125 -name LMK6_REFCLK [get_pins g_LPGBTMODE.u2/Reference_Clk_Gen/lmk6_gen/O]
#create_clock -period 3.125 -name LMK7_REFCLK [get_pins g_LPGBTMODE.u2/Reference_Clk_Gen/lmk7_gen/O]
