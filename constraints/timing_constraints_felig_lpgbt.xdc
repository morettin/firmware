#create 40 MHz TTC clock. RL: changed to 25 to stop errors in the implementation. this clock is not used in FELIG anyway
create_clock -name clk_ttc_40 -period 25 [get_pins */ttc_dec/from_cdr_to_AandB/clock_iter/O]


create_clock -period 6.25 -name clk_adn_160 [get_ports CLK_TTC_P]


create_clock -period 10.000 -name sys_clk0_p -waveform {0.000 5.000} [get_ports {sys_clk_p[0]}]
create_clock -quiet -period 10.000 -name sys_clk1_p -waveform {0.000 5.000} [get_ports {sys_clk_p[1]}]

create_clock -name emcclk -period 20.000 [get_ports emcclk]

set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets emcclk_IBUF_inst/O]

#We need this line when we move to the second SRL
#MT 4.158 -> 3.119
create_clock -name GTHREFCLK_0 -period 3.119 [get_ports {GTREFCLK_P_IN[0]}]
create_clock -quiet -name GTHREFCLK_1 -period 3.119 [get_ports {GTREFCLK_P_IN[1]}]
create_clock -quiet -name GTHREFCLK_2 -period 3.119 [get_ports {GTREFCLK_P_IN[2]}]
create_clock -quiet -name GTHREFCLK_3 -period 3.119 [get_ports {GTREFCLK_P_IN[3]}]
create_clock -quiet -name GTHREFCLK_4 -period 3.119 [get_ports {GTREFCLK_P_IN[4]}]

create_clock -period 4.158 -name LMK0_REFCLK [get_ports {LMK_P[0]}]
create_clock -period 4.158 -name LMK1_REFCLK [get_ports {LMK_P[1]}]
create_clock -period 4.158 -name LMK2_REFCLK [get_ports {LMK_P[2]}]
create_clock -period 4.158 -name LMK3_REFCLK [get_ports {LMK_P[3]}]
create_clock -period 4.158 -name LMK4_REFCLK [get_ports {LMK_P[4]}]
create_clock -period 4.158 -name LMK5_REFCLK [get_ports {LMK_P[5]}]
create_clock -period 4.158 -name LMK6_REFCLK [get_ports {LMK_P[6]}]
create_clock -period 4.158 -name LMK7_REFCLK [get_ports {LMK_P[7]}]

#linkwrapper0/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/txoutclk_out[0]
g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/GTH_inst g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst
create_clock -name GT_TX_WORD_CLK[0] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[1] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[2] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[3] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[4] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[1].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[5] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[1].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[6] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[1].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[7] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[1].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[8] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[2].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[9] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[2].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[10] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[2].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[11] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[2].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[12] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[3].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[13] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[3].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[14] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[3].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[15] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[3].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[16] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[4].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[17] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[4].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[18] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[4].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[19] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[4].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[20] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[5].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[21] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[5].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[22] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[5].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[23] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[5].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[24] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[6].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[25] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[6].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[26] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[6].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[27] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[6].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[28] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[7].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[29] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[7].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[30] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[7].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[31] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[7].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[32] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[8].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[33] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[8].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[34] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[8].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[35] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[8].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[36] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[9].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[37] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[9].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[38] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[9].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[39] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[9].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[40] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[10].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[41] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[10].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[42] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[10].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[43] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[10].GTH_TOP_INST/txoutclk_out[3]"}]
create_clock -name GT_TX_WORD_CLK[44] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[11].GTH_TOP_INST/txoutclk_out[0]"}]
create_clock -name GT_TX_WORD_CLK[45] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[11].GTH_TOP_INST/txoutclk_out[1]"}]
create_clock -name GT_TX_WORD_CLK[46] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[11].GTH_TOP_INST/txoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[47] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[11].GTH_TOP_INST/txoutclk_out[3]"}]

#linkwrapper0/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/rxoutclk_out[0]
create_clock -name GT_RX_WORD_CLK[0] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[1] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[2] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[3] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[0].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[4] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[1].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[5] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[1].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[6] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[1].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[7] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[1].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[8] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[2].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[9] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[2].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[10] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[2].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[11] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[2].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[12] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[3].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[13] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[3].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[14] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[3].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[15] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[3].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[16] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[4].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[17] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[4].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[18] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[4].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[19] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[4].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[20] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[5].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[21] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[5].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[22] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[5].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[23] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[5].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[24] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[6].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[25] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[6].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[26] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[6].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[27] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[6].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[28] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[7].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[29] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[7].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[30] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[7].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[31] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[7].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[32] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[8].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[33] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[8].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[34] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[8].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[35] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[8].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[36] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[9].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[37] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[9].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[38] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[9].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[39] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[9].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[40] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[10].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[41] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[10].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[42] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[10].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_RX_WORD_CLK[43] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[10].GTH_TOP_INST/rxoutclk_out[3]"}]
create_clock -name GT_RX_WORD_CLK[44] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[11].GTH_TOP_INST/rxoutclk_out[0]"}]
create_clock -name GT_RX_WORD_CLK[45] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[11].GTH_TOP_INST/rxoutclk_out[1]"}]
create_clock -name GT_RX_WORD_CLK[46] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[11].GTH_TOP_INST/rxoutclk_out[2]"}]
create_clock -name GT_TX_WORD_CLK[47] -period 3.119 [get_pins -hierarchical -filter {NAME =~ "*/g_lpGBTMODE.u2/FLX_LpGBT_FE_INST/mgt_notsim.GTH_inst[11].GTH_TOP_INST/rxoutclk_out[3]"}]

create_clock -quiet -name i2c_clock_pex -period 2500 [get_pins hk0/g_711_712.pex_init0/data_clk_reg/Q]

#Clock domain crossings within Central Router
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk160_clk_wiz_40_0*] 24.950

set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk80_clk_wiz_40_0*] 12.475
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk160_clk_wiz_40_0*] 6.238

set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 6.238


#Clock domain crossings in GBT
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks {GT_TX_WORD_CLK[*]}] 6.238
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks {GT_RX_WORD_CLK[*]}] 6.238

#Clock domain crossings in TTC decoder
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk_ttc_40] 6.238
set_max_delay -datapath_only -from [get_clocks clk_ttc_40] -to [get_clocks clk_adn_160] 6.238

#Register map to 400kHz I2C clock
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk400] 100.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks i2c_clock_pex] 100.000

#Register map to central router clocks


#Register map control / monitor
set_max_delay -datapath_only -from [get_clocks clk_out25*] -to [get_clocks *] 40.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk_out25*] 40.000

#Clock domain crossings in ttc decoder
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk_ttc_40] 24.95

#Clock domain crossings between Central Router and GBT
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks txoutclk*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {GT_RX_WORD_CLK[*]}] 24.950
set_max_delay -datapath_only -from [get_clocks txoutclk*] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks {GT_RX_WORD_CLK[*]}] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks {GT_TX_WORD_CLK[*]}] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks {GT_TX_WORD_CLK[0]}] 6.238
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
#MT more aggressive for lpgbt
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {GT_TX_WORD_CLK[*]}] 24.950

# added #FELIG
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk10_clk_wiz_200_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_200_0*] -to [get_clocks {GT_TX_WORD_CLK[*]}] 24.950
set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks {GT_RX_WORD_CLK[*]}] 3.119
set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks {GT_TX_WORD_CLK[*]}] 3.119
set_max_delay -datapath_only -from [get_clocks GT_RX_WORD_CLK[*]] -to [get_clocks {GT_RX_WORD_CLK[*]}] 3.119
set_max_delay -datapath_only -from [get_clocks clk_out1_rxclkgen] -to [get_clocks {GT_RX_WORD_CLK[*]}] 3.119


#set_max_delay -from [get_pins -hierarchical -filter {NAME =~ "g_endpoints[*].crth0/rst0/*/C"}] 24.95
#set_max_delay -from [get_pins -hierarchical -filter {NAME =~ "g_endpoints[*].crfh0/rst0/*/C"}] 24.95
set_max_delay -from [get_pins -hierarchical -filter {NAME =~ "g_endpoints[*].pcie0/regsync0/register_map_40_control_reg*/C"}] 24.95
set_max_delay -from [get_pins {g_endpoints[*].decoding0/g_gbtmode.g_GBT_Links[*].g_Egroups[*].PathEnable_reg[*]/C}] 24.95
set_max_delay -from [get_pins {g_endpoints[*].pcie0/regsync0/rst_soft_40*/C}]  24.95

#Only used so far to report busy.
set_max_delay -datapath_only -from [get_clocks clk250_clk_wiz_250*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95
#Used for link alignment.
set_max_delay -datapath_only -from [get_clocks rxoutclk_out*]  -to [get_clocks clk40_clk_wiz_40_0*] 24.95
set_max_delay -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk40_clk_wiz_40_0*] 6.238
#MT added
set_max_delay -datapath_only -from [get_clocks rxoutclk_out*]  -to [get_clocks clk_out25_clk_wiz_regmap*] 40.00
set_max_delay -datapath_only -from [get_clocks txoutclk_out*]  -to [get_clocks clk_out25_clk_wiz_regmap*] 40.00
#
set_max_delay -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk_ttc_40*] 6.238
###TX240 sample TX_FRAME_CLK
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_*] -to [get_clocks clk40_clk_wiz_40_*] 24.950
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 4.170

#switchable output clock can switch at any time
set_false_path -from [get_clocks clk_adn_160] -to [get_clocks clk160_clk_wiz_40_0]



#lpgbt
#set_multicycle_path -setup -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/lpgbtfpga_uplink_fec*_inst/frame_pipelined_s_reg[*]/C" }] 3
#set_multicycle_path -hold -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/lpgbtfpga_uplink_fec*_inst/frame_pipelined_s_reg[*]/C" }] 2
#set_multicycle_path -setup -from [get_pins -hierarchical -filter {NAME =~ */lpgbtfpga_uplink_fec*_inst/*descrambledData_reg[*]/C }] 3
#set_multicycle_path -hold -from [get_pins -hierarchical -filter {NAME =~ */lpgbtfpga_uplink_fec*_inst/*descrambledData_reg[*]/C }] 2

#lpgbt downlink
#set_multicycle_path 3 -setup -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/TXCLK40_r_reg/D"}]
#set_multicycle_path 2 -hold -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/TXCLK40_r_reg/D"}]
#set_multicycle_path 3 -setup -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/cnt_reg[*]/R"}]
#set_multicycle_path 2 -hold -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/cnt_reg[*]/R"}]


#create_clock -period 3.119 -name LMK0_REFCLK [get_ports {LMK_P[0]}]
#create_clock -period 3.119 -name LMK1_REFCLK [get_ports {LMK_P[1]}]
#create_clock -period 3.119 -name LMK2_REFCLK [get_ports {LMK_P[2]}]
#create_clock -period 3.119 -name LMK3_REFCLK [get_ports {LMK_P[3]}]
#create_clock -period 3.119 -name LMK4_REFCLK [get_ports {LMK_P[4]}]
#create_clock -period 3.119 -name LMK5_REFCLK [get_ports {LMK_P[5]}]
#create_clock -period 3.119 -name LMK6_REFCLK [get_ports {LMK_P[6]}]
#create_clock -period 3.119 -name LMK7_REFCLK [get_ports {LMK_P[7]}]

#######################

##RL NEW
#set_max_delay -datapath_only -from [get_clocks clk320_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
#set_max_delay -datapath_only -from [get_clocks clk320_clk_wiz_40_0*] -to [get_clocks clk80_clk_wiz_40_0*] 12.475
#set_max_delay -datapath_only -from [get_clocks clk320_clk_wiz_40_0*] -to [get_clocks clk160_clk_wiz_40_0*] 6.238
#set_max_delay -datapath_only -from [get_clocks clk320_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 4.170

#set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk320_clk_wiz_40_0*] 24.950
#set_max_delay -datapath_only -from [get_clocks clk80_clk_wiz_40_0*] -to [get_clocks clk320_clk_wiz_40_0*] 12.475
#set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks clk320_clk_wiz_40_0*] 6.238
#set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk320_clk_wiz_40_0*] 4.170

#set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk250_clk_wiz_40_0*] 24.950

set_max_delay -datapath_only -from [get_clocks clk10_clk_wiz_200_*] -to [get_clocks clk40_clk_wiz_200_*] 100
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_200_*] -to [get_clocks clk10_clk_wiz_200_*] 100

set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks clk40_clk_wiz_200_*] 25
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_200_*] -to [get_clocks GT_TX_WORD_CLK[*]] 25

#RL
set_max_delay -datapath_only -from [get_clocks clk320_clk_wiz_40_0*] -to [get_clocks GT_TX_WORD_CLK[*]] 3.125
set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks clk320_clk_wiz_40_0*] 3.125
set_max_delay -datapath_only -from [get_clocks clk320_clk_wiz_40_0*] -to [get_clocks clk320_clk_wiz_40_0*] 3.125

set_max_delay -datapath_only -from [get_clocks GT_RX_WORD_CLK[*]] -to [get_clocks {clk_out1_rxclkgen}] 3.125
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_200_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk_out1_rxclkgen*] 24.950
set_max_delay -datapath_only -from [get_clocks clk_out1_rxclkgen*] -to [get_clocks clk40_clk_wiz_40_0*] 24.950


#RL getting 40MHz for tx
set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks {txclk40m*}] 24.950
set_max_delay -datapath_only -from [get_clocks {txclk40m*}] -to [get_clocks GT_TX_WORD_CLK[*]] 24.950

#set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks {ila_tx40MHz}] 24.950
#set_max_delay -datapath_only -from [get_clocks {ila_tx40MHz}] -to [get_clocks GT_TX_WORD_CLK[*]] 24.950

#set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks {ila_tx320MHz}] 3.125
#set_max_delay -datapath_only -from [get_clocks {ila_tx320MHz}] -to [get_clocks GT_TX_WORD_CLK[*]] 3.125
