#create 40 MHz TTC clock
create_clock -period 24.950 -name clk_ttc_40 [get_pins *u2/ttc_dec/from_cdr_to_AandB/ttcclk_reg/Q]

create_clock -period 6.237 -name ts_clk_adn_160 [get_nets clk_adn_160]
set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets clk_adn_160_BUFG]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets emcclk_buf/O]
create_clock -period 4.166 -name cdrclk_in [get_pins g1.u2/ibufds_instq2_clk0/O]
create_clock -period 4.166 -name CXP2_GTH_RefClk [get_pins g1.u2/ibufds_instq8_clk0/O]
#SS
create_clock -period 4.166 -name CXP1_GTH_RefClk_LMK [get_pins g1.u2/ibufds_LMK1/O] 
create_clock -period 4.166 -name CXP2_GTH_RefClk_LMK [get_pins g1.u2/ibufds_LMK2/O] 

create_clock -period 4.166 -name CXP3_GTH_RefClk [get_pins g1.u2/ibufds_instq4_clk0/O]
create_clock -period 4.166 -name CXP4_GTH_RefClk [get_pins g1.u2/ibufds_instq5_clk0/O]
create_clock -period 4.166 -name CXP5_GTH_RefClk [get_pins g1.u2/refclkgen_v2p0.g_refclk12.ibufds_instq6_clk0/O]

#Clock domain crossings within Central Router
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk80_clk_wiz_40_0*] 12.475
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk160_clk_wiz_40_0*] 6.237

set_max_delay -datapath_only -from [get_clocks clk160_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 4.170
set_max_delay -from [get_pins */rst0/cr_rst_rr_reg*/C] -to [get_clocks clk160_clk_wiz_40_0*] 6.237

#Clock domain crossings in GBT
set_max_delay -datapath_only -from [get_clocks ts_clk_adn_160] -to [get_clocks {GT_TX_WORD_CLK[*]}] 6.237
set_max_delay -datapath_only -from [get_clocks ts_clk_adn_160] -to [get_clocks {GT_RX_WORD_CLK[*]}] 6.237

#Clock domain crossings in TTC decoder
set_max_delay -datapath_only -from [get_clocks ts_clk_adn_160] -to [get_clocks clk_ttc_40] 6.237
set_max_delay -datapath_only -from [get_clocks clk_ttc_40] -to [get_clocks ts_clk_adn_160] 6.237
set_max_delay -from [get_pins u2/ttc_dec/from_cdr_to_AandB/ttcclk_reg/Q] -to [get_pins u2/ttc_dec/from_cdr_to_AandB/ttcclk_reg/D] 6.237

#Clock domain crossings between Wupper and Central Router
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_*] -to [get_clocks *rd_clk] 4.000


#Register map to 400kHz I2C clock
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk400] 100.000

#Register map to central router clocks
set_max_delay -datapath_only -from [get_clocks *rd_clk] -to [get_clocks clk80_clk_wiz_40_0*] 12.475


#Register map control / monitor
set_max_delay -datapath_only -from [get_clocks clk_out25*] -to [get_clocks *] 40.000
set_max_delay -datapath_only -from [get_clocks *] -to [get_clocks clk_out25*] 40.000

#Clock domain crossings in ttc decoder
set_max_delay -datapath_only -from [get_clocks ts_clk_adn_160] -to [get_clocks clk_ttc_40] 24.95

#Clock domain crossings between Central Router and GBT
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks txoutclk*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {GT_RX_WORD_CLK[*]}] 24.950
set_max_delay -datapath_only -from [get_clocks txoutclk*] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks {GT_RX_WORD_CLK[*]}] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks {GT_TX_WORD_CLK[*]}] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks ts_clk_adn_160] -to [get_clocks {GT_TX_WORD_CLK[0]}] 6.237
set_max_delay -datapath_only -from [get_clocks ts_clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk_adn_160] -to [get_clocks clk40_clk_wiz_40_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks {GT_TX_WORD_CLK[*]}] 24.950
# added SS
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk10_clk_wiz_200_0*] 24.950
set_max_delay -datapath_only -from [get_clocks clk40_clk_wiz_200_0*] -to [get_clocks {GT_TX_WORD_CLK[*]}] 24.950
# RL
set_max_delay -datapath_only -from [get_clocks GT_TX_WORD_CLK[*]] -to [get_clocks {GT_RX_WORD_CLK[*]}] 4.170

###TX240 sample TX_FRAME_CLK
set_multicycle_path 2 -setup -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "*timedomaincrossing_C/TX_FRAMECLK_I*/D"}]
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_*] -to [get_clocks clk40_clk_wiz_40_*] 24.950
set_max_delay -datapath_only -from [get_clocks clk240_clk_wiz_40_0*] -to [get_clocks clk240_clk_wiz_40_0*] 4.170

#switchable output clock can switch at any time
set_false_path -from [get_clocks ts_clk_adn_160] -to [get_clocks clk160_clk_wiz_40_0]

#Multicycle paths towards Wupper fifos
set_multicycle_path -setup -start 3 -from [ get_pins pcie1/dma0/u0/fromHostFifo_din_reg[*]/C ] -to [ get_pins fromHostFifo1_din_pipe_reg[*]/D ]
set_multicycle_path -hold  -end   2 -from [ get_pins pcie1/dma0/u0/fromHostFifo_din_reg[*]/C ] -to [ get_pins fromHostFifo1_din_pipe_reg[*]/D ]

set_multicycle_path -setup -start 3 -from [ get_pins pcie1/dma0/u0/fromHostFifo_we_reg/C ] -to [ get_pins fromHostFifo1_wr_en_pipe_reg/D ]
set_multicycle_path -hold  -end   2 -from [ get_pins pcie1/dma0/u0/fromHostFifo_we_reg/C ] -to [ get_pins fromHostFifo1_wr_en_pipe_reg/D ]

set_multicycle_path -setup -start 3 -from [ get_pins cr1/cr_data_out_rdy_reg/C ] -to [ get_pins toHostFifo1_wr_en_pipe_reg/D]
set_multicycle_path -hold  -end   2 -from [ get_pins cr1/cr_data_out_rdy_reg/C ] -to [ get_pins toHostFifo1_wr_en_pipe_reg/D]

set_multicycle_path -setup -start 3 -from [ get_pins cr1/cr_data_out_reg[*]/C ] -to [ get_pins toHostFifo1_din_pipe_reg[*]/D]
set_multicycle_path -hold  -end   2 -from [ get_pins cr1/cr_data_out_reg[*]/C ] -to [ get_pins toHostFifo1_din_pipe_reg[*]/D]

set_multicycle_path -setup -start 3 -from [ get_pins pcie0/dma0/u0/fromHostFifo_din_reg[*]/C ] -to [ get_pins fromHostFifo0_din_pipe_reg[*]/D ]
set_multicycle_path -hold  -end   2 -from [ get_pins pcie0/dma0/u0/fromHostFifo_din_reg[*]/C ] -to [ get_pins fromHostFifo0_din_pipe_reg[*]/D ]

set_multicycle_path -setup -start 3 -from [ get_pins pcie0/dma0/u0/fromHostFifo_we_reg/C ] -to [ get_pins fromHostFifo0_wr_en_pipe_reg/D ]
set_multicycle_path -hold  -end   2 -from [ get_pins pcie0/dma0/u0/fromHostFifo_we_reg/C ] -to [ get_pins fromHostFifo0_wr_en_pipe_reg/D ]

set_multicycle_path -setup -start 3 -from [ get_pins cr0/cr_data_out_rdy_reg/C ] -to [ get_pins toHostFifo0_wr_en_pipe_reg/D]
set_multicycle_path -hold  -end   2 -from [ get_pins cr0/cr_data_out_rdy_reg/C ] -to [ get_pins toHostFifo0_wr_en_pipe_reg/D]

set_multicycle_path -setup -start 3 -from [ get_pins cr0/cr_data_out_reg[*]/C ] -to [ get_pins toHostFifo0_din_pipe_reg[*]/D]
set_multicycle_path -hold  -end   2 -from [ get_pins cr0/cr_data_out_reg[*]/C ] -to [ get_pins toHostFifo0_din_pipe_reg[*]/D]


#Multicycle paths in the RxGearbox
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 4
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 3
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 4
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 3
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 4
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 3
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 4
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 3

#TX side
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 3
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 3
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 1

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 2
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 1

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0

set_multicycle_path -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
