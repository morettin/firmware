#will be set from .tcl
set TRANSCEIVER_TYPE [source ../../../../constraints/TRANSCEIVER_TYPE.tcl] 
#
#0: GBT core, 240.474MHz clock
#1: lpGBT core, 320.632MHz clock
#2: Interlaken, 156.25MHz clock
if { ($TRANSCEIVER_TYPE == 1 ) } {
    set GTHREFCLK_PERIOD 3.125
} elseif { ($TRANSCEIVER_TYPE == 2 ) } {
    set GTHREFCLK1_PERIOD 6.4
    set GTHREFCLK_PERIOD 4.158
    
    create_clock -quiet -name GTHREFCLK1_0 -period $GTHREFCLK1_PERIOD [get_ports {GTREFCLK1_P_IN[0]}]
    create_clock -quiet -name GTHREFCLK1_1 -period $GTHREFCLK1_PERIOD [get_ports {GTREFCLK1_P_IN[1]}]
    create_clock -quiet -name GTHREFCLK1_2 -period $GTHREFCLK1_PERIOD [get_ports {GTREFCLK1_P_IN[2]}]
    create_clock -quiet -name GTHREFCLK1_3 -period $GTHREFCLK1_PERIOD [get_ports {GTREFCLK1_P_IN[3]}]
    create_clock -quiet -name GTHREFCLK1_4 -period $GTHREFCLK1_PERIOD [get_ports {GTREFCLK1_P_IN[4]}]
    create_clock -quiet -name GTHREFCLK1_5 -period $GTHREFCLK1_PERIOD [get_ports {GTREFCLK1_P_IN[5]}]
    
} else {
    set GTHREFCLK_PERIOD 4.158
}

create_clock -quiet -period 6.25 -name clk_adn_160 [get_ports {CLK_TTC_P[0]}]

#create 40 MHz TTC clock
create_clock -quiet -name clk_ttc_40 -period 24.95 [get_pins */ttc_dec/from_cdr_to_AandB/clock_iter/O]

create_clock -quiet -period 10.000 -name sys_clk0_p -waveform {0.000 5.000} [get_ports {sys_clk_p[0]}]
create_clock -quiet -period 10.000 -name sys_clk1_p -waveform {0.000 5.000} [get_ports {sys_clk_p[1]}]

set_property -quiet CLOCK_DEDICATED_ROUTE FALSE [get_nets emcclk_IBUF[0]_inst/O]

#GTREFCLKS up to 6, use -quiet for the ones that don't exist due to limited number of channels.
create_clock -quiet -name GTHREFCLK_0 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[0]}]
create_clock -quiet -name GTHREFCLK_1 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[1]}]
create_clock -quiet -name GTHREFCLK_2 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[2]}]
create_clock -quiet -name GTHREFCLK_3 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[3]}]
create_clock -quiet -name GTHREFCLK_4 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[4]}]
create_clock -quiet -name GTHREFCLK_5 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[5]}]
create_clock -quiet -name GTHREFCLK_6 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[6]}]
create_clock -quiet -name GTHREFCLK_7 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[7]}]
create_clock -quiet -name GTHREFCLK_8 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[8]}]
create_clock -quiet -name GTHREFCLK_9 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[9]}]
create_clock -quiet -name GTHREFCLK_10 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[10]}]
create_clock -quiet -name GTHREFCLK_11 -period $GTHREFCLK_PERIOD [get_ports {GTREFCLK_P_IN[11]}]
create_clock -quiet -period 3.125 -name LMK0_REFCLK [get_ports {LMK_P[0]}]
create_clock -quiet -period 3.125 -name LMK1_REFCLK [get_ports {LMK_P[1]}]
create_clock -quiet -period 3.125 -name LMK2_REFCLK [get_ports {LMK_P[2]}]
create_clock -quiet -period 3.125 -name LMK3_REFCLK [get_ports {LMK_P[3]}]
create_clock -quiet -period 3.125 -name LMK4_REFCLK [get_ports {LMK_P[4]}]
create_clock -quiet -period 3.125 -name LMK5_REFCLK [get_ports {LMK_P[5]}]
create_clock -quiet -period 3.125 -name LMK6_REFCLK [get_ports {LMK_P[6]}]
create_clock -quiet -period 3.125 -name LMK7_REFCLK [get_ports {LMK_P[7]}]

create_clock -quiet -name i2c_clock_pex -period 2500 [get_pins hk0/g_711_712.pex_init0/data_clk_reg/Q]

#Register map to 400kHz I2C clock
set_max_delay -quiet -datapath_only -from [get_clocks *] -to [get_clocks clk400] 100.000
set_max_delay -quiet -datapath_only -from [get_clocks *] -to [get_clocks i2c_clock_pex] 100.000

#Register map control / monitor
set_max_delay -quiet -datapath_only -from [get_clocks clk_out25*] -to [get_clocks *] 40.000
set_max_delay -quiet -datapath_only -from [get_clocks *] -to [get_clocks clk_out25*] 40.000

#switchable output clock can switch at any time
set_max_delay -quiet -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk40_clk_wiz_40_0*] 6.238
set_max_delay -quiet -datapath_only -from [get_clocks clk_adn_160*] -to [get_clocks clk_ttc_40*] 6.238
set_max_delay -quiet -datapath_only -from [get_clocks clk40_clk_wiz_40_0*] -to [get_clocks clk_ttc_40*] 24.95
set_max_delay -quiet -datapath_only -from [get_clocks clk_ttc_40*] -to [get_clocks clk40_clk_wiz_40_0*] 24.95

if { ($TRANSCEIVER_TYPE == 0 ) } {
#Multicycle paths in the RxGearbox
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" }] 3
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" }] 3
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" }] 3
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 4
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" }] 3

#TX side
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 1
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 3
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 3
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" }] 2

set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 1
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 2
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" }] 1
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0
 
set_multicycle_path -quiet -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 1
set_multicycle_path -quiet -hold -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" }] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" }] 0

#For Versal/GBT only
set_multicycle_path -quiet -setup -start -from [get_pins {linkwrapper0/g_GBTMODE.g_versal.u2/GTH_inst[*].GTH_TOP_INST/transceiver_versal_i/gt_quad_base/inst/quad_inst/*RXUSRCLK}] -to [get_pins {linkwrapper0/g_GBTMODE.g_versal.u2/gbtRxTx[*].gbtTxRx_inst/gbtRx_inst/FelixRxGearbox/*[*]*/D}] 2
set_multicycle_path -quiet -hold -end -from [get_pins {linkwrapper0/g_GBTMODE.g_versal.u2/GTH_inst[*].GTH_TOP_INST/transceiver_versal_i/gt_quad_base/inst/quad_inst/*RXUSRCLK}] -to [get_pins {linkwrapper0/g_GBTMODE.g_versal.u2/gbtRxTx[*].gbtTxRx_inst/gbtRx_inst/FelixRxGearbox/*[*]*/D}] 1
}
if { ($TRANSCEIVER_TYPE == 1 ) } {
#lpgbt
set_multicycle_path -quiet -setup -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/lpgbtfpga_uplink_fec*_inst/frame_pipelined_s_reg[*]/C" }] 3
set_multicycle_path -quiet -hold -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/lpgbtfpga_uplink_fec*_inst/frame_pipelined_s_reg[*]/C" }] 2
set_multicycle_path -quiet -setup -from [get_pins -hierarchical -filter {NAME =~ */lpgbtfpga_uplink_fec*_inst/*descrambledData_reg[*]/C }] 3
set_multicycle_path -quiet -hold -from [get_pins -hierarchical -filter {NAME =~ */lpgbtfpga_uplink_fec*_inst/*descrambledData_reg[*]/C }] 2

set_multicycle_path -quiet -setup -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkSelectFEC_multicycle_reg*/C" }] 3
set_multicycle_path -quiet -hold -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkSelectFEC_multicycle_reg*/C" }] 2
set_multicycle_path -quiet -setup -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkBypassInterleaver_multicycle_reg*/C" }] 3
set_multicycle_path -quiet -hold -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkBypassInterleaver_multicycle_reg*/C" }] 2
set_multicycle_path -quiet -setup -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkBypassFECEncoder_multicycle_reg*/C" }] 3
set_multicycle_path -quiet -hold -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkBypassFECEncoder_multicycle_reg*/C" }] 2
set_multicycle_path -quiet -setup -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkBypassScrambler_multicycle_reg*/C" }] 3
set_multicycle_path -quiet -hold -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkBypassScrambler_multicycle_reg*/C" }] 2
set_multicycle_path -quiet -setup -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkSelectDataRate_multicycle_reg*/C" }] 3
set_multicycle_path -quiet -hold -from [get_pins -hierarchical -filter { NAME=~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/uplinkSelectDataRate_multicycle_reg*/C" }] 2


##lpgbt downlink
set_multicycle_path -quiet 3 -setup -from [get_pins {linkwrapper0/*/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/lpgbtfpga_scrambler_inst/scrambledData_reg[*]/C}] -to [get_pins {linkwrapper0/*/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/TxData_Interleaved_latched_reg[*]/D}]
set_multicycle_path -quiet 2 -hold -from [get_pins {linkwrapper0/*/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/lpgbtfpga_scrambler_inst/scrambledData_reg[*]/C}] -to [get_pins {linkwrapper0/*/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/TxData_Interleaved_latched_reg[*]/D}]
#set_multicycle_path -quiet 3 -setup -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/TXCLK40_r_reg/D"}]
#set_multicycle_path -quiet 2 -hold -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/TXCLK40_r_reg/D"}]
#set_multicycle_path -quiet 3 -setup -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/cnt_reg[*]/R"}]
#set_multicycle_path -quiet 2 -hold -from [get_pins clk0/clk0/inst/mmcme3_adv_inst/CLKOUT0] -to [get_pins -hierarchical -filter {NAME =~ "linkwrapper0/g_LPGBTMODE.u2/FLX_LpGBT_BE_INST/gbtRxTx[*].lpgbt_inst/cnt_reg[*]/R"}]

}
