# felix_minipod_BNL712_2x24ch_MROD.xdc
# FELIX_MROD Transceiver pins

# EP0 (bottom SLR0)
# MGTs in bank 126,127,128 use clk Q2 from bank 127, pin AH37
# MGTs in bank 228         use clk Q5 from bank 228, pin AF8
# MGTs in bank 224,225     use clk Q6 from bank 225, pin AP8

#bank 128 - GTREF clk Q2
set_property PACKAGE_PIN AC43 [get_ports {RX_P[0]}]
set_property PACKAGE_PIN AG43 [get_ports {RX_P[1]}]
set_property PACKAGE_PIN AE43 [get_ports {RX_P[2]}]
set_property PACKAGE_PIN AF41 [get_ports {RX_P[3]}]
#bank 127 - GTREF clk Q2
set_property PACKAGE_PIN AH41 [get_ports {RX_P[4]}]
set_property PACKAGE_PIN AJ43 [get_ports {RX_P[5]}]
set_property PACKAGE_PIN AN43 [get_ports {RX_P[6]}]
set_property PACKAGE_PIN AL43 [get_ports {RX_P[7]}]
#bank 126 - GTREF clk Q2
set_property PACKAGE_PIN AU43 [get_ports {RX_P[8]}]
set_property PACKAGE_PIN AR43 [get_ports {RX_P[9]}]
set_property PACKAGE_PIN BA43 [get_ports {RX_P[10]}]
set_property PACKAGE_PIN AW43 [get_ports {RX_P[11]}]

#bank 228 - GTREF clk Q5
set_property PACKAGE_PIN AG2  [get_ports {RX_P[12]}]
set_property PACKAGE_PIN AF4  [get_ports {RX_P[13]}]
set_property PACKAGE_PIN AC2  [get_ports {RX_P[14]}]
set_property PACKAGE_PIN AE2  [get_ports {RX_P[15]}]

#bank 224 - GTREF clk Q6
set_property PACKAGE_PIN BA10 [get_ports {RX_P[16]}]
set_property PACKAGE_PIN BC10 [get_ports {RX_P[17]}]
set_property PACKAGE_PIN AY8  [get_ports {RX_P[18]}]
set_property PACKAGE_PIN BB12 [get_ports {RX_P[19]}]
#bank 225 - GTREF clk Q6
set_property PACKAGE_PIN BB4  [get_ports {RX_P[20]}]
set_property PACKAGE_PIN BD4  [get_ports {RX_P[21]}]
set_property PACKAGE_PIN BA2  [get_ports {RX_P[22]}]
set_property PACKAGE_PIN BC6  [get_ports {RX_P[23]}]


# EP1 (top SLR1)
# MGTs in bank 131,132,133 use clk Q8 from bank 132, pin T37
# MGTs in bank 231,232,233 use clk Q4 from bank 232, pin M8

#bank 133 - GTREF clk Q8
set_property PACKAGE_PIN C43  [get_ports {RX_P[24]}]
set_property PACKAGE_PIN E43  [get_ports {RX_P[25]}]
set_property PACKAGE_PIN G43  [get_ports {RX_P[26]}]
set_property PACKAGE_PIN J43  [get_ports {RX_P[27]}]
#bank 132 - GTREF clk Q8
set_property PACKAGE_PIN L43  [get_ports {RX_P[28]}]
set_property PACKAGE_PIN N43  [get_ports {RX_P[29]}]
set_property PACKAGE_PIN R43  [get_ports {RX_P[30]}]
set_property PACKAGE_PIN U43  [get_ports {RX_P[31]}]
#bank 131 - GTREF clk Q8
set_property PACKAGE_PIN W43  [get_ports {RX_P[32]}]
set_property PACKAGE_PIN V41  [get_ports {RX_P[33]}]
set_property PACKAGE_PIN AB41 [get_ports {RX_P[34]}]
set_property PACKAGE_PIN AA43 [get_ports {RX_P[35]}]

#bank 231 - GTREF clk Q4
set_property PACKAGE_PIN J2   [get_ports {RX_P[36]}]
set_property PACKAGE_PIN H4   [get_ports {RX_P[37]}]
set_property PACKAGE_PIN G2   [get_ports {RX_P[38]}]
set_property PACKAGE_PIN F4   [get_ports {RX_P[39]}]
#bank 232 - GTREF clk Q4
set_property PACKAGE_PIN C2   [get_ports {RX_P[40]}]
set_property PACKAGE_PIN E2   [get_ports {RX_P[41]}]
set_property PACKAGE_PIN B4   [get_ports {RX_P[42]}]
set_property PACKAGE_PIN D4   [get_ports {RX_P[43]}]
#bank 233 - GTREF clk Q4
set_property PACKAGE_PIN C10  [get_ports {RX_P[44]}]
set_property PACKAGE_PIN A6   [get_ports {RX_P[45]}]
set_property PACKAGE_PIN B12  [get_ports {RX_P[46]}]
set_property PACKAGE_PIN E10  [get_ports {RX_P[47]}]

#
