--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Israel Grayzman
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- ATLAS 
-- FELIX UVVM test automation project
--  A.SKAF
--  University of Goettingen
--  July 2019
----------------------------------------------------------------------------------
-- file: EGroup.vhd.  
----------------------------------------------------------------------------------

--! based on DownstreamEgroup.vhd - by Engineer: juna
--! 
--!
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library work, ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.all;
use work.centralRouter_package.all;

--! consists of 15 EPROCs and 8 possible E-paths
entity Egroup is
generic (
--=======================
	   GC_DATA_WIDTH : natural :=16;
    	   GC_DATA_DEPTH : natural :=32 ;
--========================
            GBTid                           : integer := 0;
         egroupID                        : integer := 0;
            EnableToHo_Egroup0Eproc2_HDLC   : boolean := true;
            EnableToHo_Egroup0Eproc2_8b10b  : boolean := true;
            EnableToHo_Egroup0Eproc4_8b10b  : boolean := true;
            EnableToHo_Egroup0Eproc8_8b10b  : boolean := true;
            EnableToHo_Egroup0Eproc16_8b10b : boolean := true;
            EnableToHo_Egroup1Eproc2_HDLC   : boolean := true;
            EnableToHo_Egroup1Eproc2_8b10b  : boolean := true;
            EnableToHo_Egroup1Eproc4_8b10b  : boolean := true;
            EnableToHo_Egroup1Eproc8_8b10b  : boolean := true;
            EnableToHo_Egroup1Eproc16_8b10b : boolean := true;
            EnableToHo_Egroup2Eproc2_HDLC   : boolean := true;
            EnableToHo_Egroup2Eproc2_8b10b  : boolean := true;
            EnableToHo_Egroup2Eproc4_8b10b  : boolean := true;
            EnableToHo_Egroup2Eproc8_8b10b  : boolean := true;
            EnableToHo_Egroup2Eproc16_8b10b : boolean := true;
            EnableToHo_Egroup3Eproc2_HDLC   : boolean := true;
            EnableToHo_Egroup3Eproc2_8b10b  : boolean := true;
            EnableToHo_Egroup3Eproc4_8b10b  : boolean := true;
            EnableToHo_Egroup3Eproc8_8b10b  : boolean := true;
            EnableToHo_Egroup3Eproc16_8b10b : boolean := true;
            EnableToHo_Egroup4Eproc2_HDLC   : boolean := true;
            EnableToHo_Egroup4Eproc2_8b10b  : boolean := true;
            EnableToHo_Egroup4Eproc4_8b10b  : boolean := true;
            EnableToHo_Egroup4Eproc8_8b10b  : boolean := true;
            EnableToHo_Egroup4Eproc16_8b10b : boolean := true;
            toHostTimeoutBitn           : integer := 16;
            includeNoEncodingCase           : boolean := true;
		GENERATE_FEI4B          		: boolean := false;
	 	GENERATE_TRUNCATION_MECHANISM   : boolean := true
        );
port ( 
    clk40       : 	in std_logic;
    clk80       : 	in std_logic;
    clk160      : 	in std_logic;
    clk240      :	in std_logic;
    rst_clk40   : 	in std_logic;
    timeCntIn   :   in std_logic_vector ((toHostTimeoutBitn-1) downto 0);
    TimeoutEnaIn:   in std_logic;
    ------
    fifoFlush   : 	in std_logic;
    xoffAllData : 	in std_logic;
    ------
    thCR_REVERSE_10B : in std_logic;
    CONFIG_REG  : in std_logic_vector (63 downto 0);
    ------
    DATA16bitIN : in std_logic_vector (15 downto 0);
    ------
    EPATH_AFs   : out std_logic_vector (7 downto 0); -- almost full flag, xoff case monitor
    EPATH_HFs   : out std_logic_vector (7 downto 0); -- half-full flag: ready to be read
    EPATH_REs   : in std_logic_vector (7 downto 0); -- enable array reading of a specified EPATH ID
    EPATH_ID    : in std_logic_vector (2 downto 0); -- EPATH ID to read out
    EPATH_BUSY  : out std_logic_vector (7 downto 0); -- BUSY per EPATH
    ------
    EOUT        : out std_logic_vector (15 downto 0);
    EOUT_rdy    : out std_logic
    ------
    );
end Egroup;

architecture Behavioral of Egroup is

----------------------------------
----------------------------------
component MUX2_Nbit
generic (N : integer := 1);
port ( 
	data0    : in std_logic_vector((N-1) downto 0);
	data1    : in std_logic_vector((N-1) downto 0);
	sel      : in std_logic;
	data_out : out std_logic_vector((N-1) downto 0)
	);
end component;
----------------------------------
----------------------------------
component MUX2
port (
	data0 : in std_logic;
	data1 : in std_logic;
	sel : in std_logic;          
	data_out : out std_logic
	);
end component;
----------------------------------
----------------------------------

--
signal EPATH_HFs_s : std_logic_vector (7 downto 0);
----
type array8_std_logic_vector_16 is array (0 to 7) of std_logic_vector(15 downto 0); 
signal BWORD_array, EPATH_DOUT_array, EPATH_DOUT_array_s : array8_std_logic_vector_16;
----
type array8_std_logic_vector_10 is array (0 to 7) of std_logic_vector(9 downto 0); 
signal DATA_OUT_array : array8_std_logic_vector_10;
----
type array8_std_logic is array (0 to 7) of std_logic; 
signal DATA_RDY_array, BWORD_RDY_array, prog_full_array : array8_std_logic;
--
signal epathBusy_array : std_logic_vector (7 downto 0);
----
type array8_std_logic_vector_2 is array (0 to 7) of std_logic_vector(1 downto 0); 
signal PATHencoding_array : array8_std_logic_vector_2;
----
signal EPATH_ID_clk1 : std_logic_vector (2 downto 0) := "000";
------------------------------------------------------------------------------------------
----
signal EPROC_ENA_bits : std_logic_vector (14 downto 0) := (others => '0');
signal PATH_ENCODING  : std_logic_vector (15 downto 0) := (others => '0');
signal MAX_CHUNK_LEN  : std_logic_vector (15 downto 0) := (others => '0');
signal epath_input_bit_order,instant_timeout_ena : std_logic_vector (7 downto 0) := (others => '0');
----
------------------------------------------------------------------------------------------
type array15_std_logic_vector_10 is array (0 to 14) of std_logic_vector(9 downto 0); 
signal DATA_OUT_EID : array15_std_logic_vector_10;
----
type array15_std_logic is array (0 to 14) of std_logic; 
signal DATA_RDY_EID : array15_std_logic;
------------------------------------------------------------------------------------------
--
signal maxClen_EPROC2,maxClen_EPROC4,maxClen_EPROC8,maxClen_EPROC16  : std_logic_vector (2 downto 0);
type array8_std_logic_vector_3 is array (0 to 7) of std_logic_vector(2 downto 0); 
signal maxClen_array  : array8_std_logic_vector_3; -- per EPATH
signal EPATH_REs_clk1 : std_logic_vector (7 downto 0) := (others=>'0');
--

begin

------------------------------------------------------------
-- CONFIG_REG in written with 40MHz clock, this is 2nd FF
------------------------------------------------------------
process(clk40,rst_clk40)
begin
    if rst_clk40 = '1' then
       --
        EPROC_ENA_bits          <= (others=>'0');
        PATH_ENCODING           <= (others=>'0');
        maxClen_EPROC2          <= (others=>'0');
        maxClen_EPROC4          <= (others=>'0');
        maxClen_EPROC8          <= (others=>'0');
        maxClen_EPROC16         <= (others=>'0');
        epath_input_bit_order   <= (others=>'0');
        instant_timeout_ena     <= (others=>'0');
        -- 
    elsif rising_edge (clk40) then
        --
        EPROC_ENA_bits          <= CONFIG_REG(14 downto 0);
        PATH_ENCODING           <= CONFIG_REG(30 downto 15);
        maxClen_EPROC2          <= CONFIG_REG(33 downto 31);
        maxClen_EPROC4          <= CONFIG_REG(36 downto 34);
        maxClen_EPROC8          <= CONFIG_REG(39 downto 37);
        maxClen_EPROC16         <= CONFIG_REG(42 downto 40);
        epath_input_bit_order   <= CONFIG_REG(50 downto 43); 
        instant_timeout_ena     <= CONFIG_REG(58 downto 51);
        --
    end if;
end process;
--

------------------------------------------------------------
-- Egroup path encoding mapping
------------------------------------------------------------
PATHencoding_array(7) <= PATH_ENCODING(15 downto 14);
PATHencoding_array(6) <= PATH_ENCODING(13 downto 12);
PATHencoding_array(5) <= PATH_ENCODING(11 downto 10);
PATHencoding_array(4) <= PATH_ENCODING( 9 downto  8);
PATHencoding_array(3) <= PATH_ENCODING( 7 downto  6);
PATHencoding_array(2) <= PATH_ENCODING( 5 downto  4);
PATHencoding_array(1) <= PATH_ENCODING( 3 downto  2);
PATHencoding_array(0) <= PATH_ENCODING( 1 downto  0);
			

------------------------------------------------------------
-- E-PATH-7
------------------------------------------------------------
EPROC_IN2_ID14_PATH7: entity work.EPROC_IN2 (Behavioral)
generic map (
                IC_Elink                        => false,
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
        --IG    do_generate             => includeToHoEproc2s,
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	bitCLKx2   => clk80,
	rst        => rst_clk40,
	ENA        => EPROC_ENA_bits(14),
	swap_inputbits => epath_input_bit_order(7),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(7),
	EDATA_IN   => DATA16bitIN(15 downto 14),
	DATA_OUT   => DATA_OUT_EID(14), 
	DATA_RDY   => DATA_RDY_EID(14)
);
---
DATA_OUT_array(7)   <= DATA_OUT_EID(14);
DATA_RDY_array(7)   <= DATA_RDY_EID(14);
maxClen_array(7)    <= maxClen_EPROC2;
---


------------------------------------------------------------
-- E-PATH-6
------------------------------------------------------------
--- MUX2:IN0
EPROC_IN4_ID06_PATH6: entity work.EPROC_IN4 (Behavioral)
generic map (
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc4_8b10b  => EnableToHo_Egroup0Eproc4_8b10b,
                EnableToHo_Egroup1Eproc4_8b10b  => EnableToHo_Egroup1Eproc4_8b10b,
                EnableToHo_Egroup2Eproc4_8b10b  => EnableToHo_Egroup2Eproc4_8b10b,
                EnableToHo_Egroup3Eproc4_8b10b  => EnableToHo_Egroup3Eproc4_8b10b,
                EnableToHo_Egroup4Eproc4_8b10b  => EnableToHo_Egroup4Eproc4_8b10b,
               includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(6),
	swap_inputbits => epath_input_bit_order(6),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(6),
	EDATA_IN   => DATA16bitIN(15 downto 12),
	DATA_OUT   => DATA_OUT_EID(6),
	DATA_RDY   => DATA_RDY_EID(6)
);
--- 
--- MUX2:IN1
EPROC_IN2_ID13_PATH6: entity work.EPROC_IN2 (Behavioral)
generic map (
                IC_Elink                        => false,
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
               includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	bitCLKx2   => clk80,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(13),
	swap_inputbits => epath_input_bit_order(6),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(6),
	EDATA_IN   => DATA16bitIN(13 downto 12),
	DATA_OUT   => DATA_OUT_EID(13),
	DATA_RDY   => DATA_RDY_EID(13)
);
--- 
MUX2_DATA_OUT_EPATH6: MUX2_Nbit generic map(N => 10)    port map (DATA_OUT_EID(6) ,DATA_OUT_EID(13) ,EPROC_ENA_bits(13) ,DATA_OUT_array(6));
MUX2_DATA_RDY_EPATH6: MUX2                              port map (DATA_RDY_EID(6) ,DATA_RDY_EID(13) ,EPROC_ENA_bits(13) ,DATA_RDY_array(6));
MAX_CHUNK_LEN_EPATH6: MUX2_Nbit generic map(N => 3 )    port map (maxClen_EPROC4  ,maxClen_EPROC2   ,EPROC_ENA_bits(13) ,maxClen_array(6));
---


------------------------------------------------------------
-- E-PATH-5
------------------------------------------------------------
--- MUX2:IN0
EPROC_IN8_ID02_PATH5: entity work.EPROC_IN8 (Behavioral)
generic map (
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc8_8b10b  => EnableToHo_Egroup0Eproc8_8b10b,
                EnableToHo_Egroup1Eproc8_8b10b  => EnableToHo_Egroup1Eproc8_8b10b,
                EnableToHo_Egroup2Eproc8_8b10b  => EnableToHo_Egroup2Eproc8_8b10b,
                EnableToHo_Egroup3Eproc8_8b10b  => EnableToHo_Egroup3Eproc8_8b10b,
                EnableToHo_Egroup4Eproc8_8b10b  => EnableToHo_Egroup4Eproc8_8b10b,
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(2),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(5),
	EDATA_IN   => DATA16bitIN(15 downto 8),
	DATA_OUT   => DATA_OUT_EID(2),
	DATA_RDY   => DATA_RDY_EID(2)
);
--- 
--- MUX2:IN1
EPROC_IN2_ID12_PATH5: entity work.EPROC_IN2 (Behavioral)
generic map (
                IC_Elink                        => false,
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	bitCLKx2   => clk80,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(12),
	swap_inputbits => epath_input_bit_order(5),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(5),
	EDATA_IN   => DATA16bitIN(11 downto 10),
	DATA_OUT   => DATA_OUT_EID(12),
	DATA_RDY   => DATA_RDY_EID(12)
);
--- 
MUX2_DATA_OUT_EPATH5: MUX2_Nbit generic map(N => 10)    port map (DATA_OUT_EID(2) ,DATA_OUT_EID(12) ,EPROC_ENA_bits(12) ,DATA_OUT_array(5));
MUX2_DATA_RDY_EPATH5: MUX2                              port map (DATA_RDY_EID(2) ,DATA_RDY_EID(12) ,EPROC_ENA_bits(12) ,DATA_RDY_array(5));
MAX_CHUNK_LEN_EPATH5: MUX2_Nbit generic map(N => 3 )    port map (maxClen_EPROC8  ,maxClen_EPROC2   ,EPROC_ENA_bits(12) ,maxClen_array(5));
---


--Add new directory
------------------------------------------------------------
-- E-PATH-4
------------------------------------------------------------
--- MUX2:IN0
EPROC_IN4_ID05_PATH4: entity work.EPROC_IN4 (Behavioral)
generic map (
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc4_8b10b  => EnableToHo_Egroup0Eproc4_8b10b,
                EnableToHo_Egroup1Eproc4_8b10b  => EnableToHo_Egroup1Eproc4_8b10b,
                EnableToHo_Egroup2Eproc4_8b10b  => EnableToHo_Egroup2Eproc4_8b10b,
                EnableToHo_Egroup3Eproc4_8b10b  => EnableToHo_Egroup3Eproc4_8b10b,
                EnableToHo_Egroup4Eproc4_8b10b  => EnableToHo_Egroup4Eproc4_8b10b,
               includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(5),
	swap_inputbits => epath_input_bit_order(4),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(4),
	EDATA_IN   => DATA16bitIN(11 downto 8),
	DATA_OUT   => DATA_OUT_EID(5),
	DATA_RDY   => DATA_RDY_EID(5)
);
--- 
--- MUX2:IN1
EPROC_IN2_ID11_PATH4: entity work.EPROC_IN2 (Behavioral)
generic map (
                IC_Elink                        => false,
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
               includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	bitCLKx2   => clk80,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(11),
	swap_inputbits => epath_input_bit_order(4),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(4),
	EDATA_IN   => DATA16bitIN(9 downto 8),
	DATA_OUT   => DATA_OUT_EID(11),
	DATA_RDY   => DATA_RDY_EID(11)
);

--- 
MUX2_DATA_OUT_EPATH4: MUX2_Nbit generic map(N => 10)    port map (DATA_OUT_EID(5) ,DATA_OUT_EID(11) ,EPROC_ENA_bits(11) ,DATA_OUT_array(4));
MUX2_DATA_RDY_EPATH4: MUX2                              port map (DATA_RDY_EID(5) ,DATA_RDY_EID(11) ,EPROC_ENA_bits(11) ,DATA_RDY_array(4));
MAX_CHUNK_LEN_EPATH4: MUX2_Nbit generic map(N => 3 )    port map (maxClen_EPROC4  ,maxClen_EPROC2   ,EPROC_ENA_bits(11) ,maxClen_array(4));
---


------------------------------------------------------------
-- E-PATH-3
------------------------------------------------------------
--- MUX2:IN0
EPROC_IN16_ID00_PATH3: entity work.EPROC_IN16 (Behavioral)
generic map (
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc16_8b10b => EnableToHo_Egroup0Eproc16_8b10b,
                EnableToHo_Egroup1Eproc16_8b10b => EnableToHo_Egroup1Eproc16_8b10b,
                EnableToHo_Egroup2Eproc16_8b10b => EnableToHo_Egroup2Eproc16_8b10b,
                EnableToHo_Egroup3Eproc16_8b10b => EnableToHo_Egroup3Eproc16_8b10b,
                EnableToHo_Egroup4Eproc16_8b10b => EnableToHo_Egroup4Eproc16_8b10b,
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	bitCLKx2   => clk80,
	bitCLKx4   => clk160,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(0),
	ENCODING   => PATHencoding_array(3),
	EDATA_IN   => DATA16bitIN,
	DATA_OUT   => DATA_OUT_EID(0),
	DATA_RDY   => DATA_RDY_EID(0)
    );
--- 
--- MUX2:IN1
EPROC_IN2_ID10_PATH3: entity work.EPROC_IN2 (Behavioral)
generic map (
                IC_Elink                        => false,
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	bitCLKx2   => clk80,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(10),
	swap_inputbits => epath_input_bit_order(3),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(3),
	EDATA_IN   => DATA16bitIN(7 downto 6),
	DATA_OUT   => DATA_OUT_EID(10),
	DATA_RDY   => DATA_RDY_EID(10)
);

--- 
MUX2_DATA_OUT_EPATH3: MUX2_Nbit generic map(N => 10)    port map (DATA_OUT_EID(0) ,DATA_OUT_EID(10) ,EPROC_ENA_bits(10) ,DATA_OUT_array(3));
MUX2_DATA_RDY_EPATH3: MUX2                              port map (DATA_RDY_EID(0) ,DATA_RDY_EID(10) ,EPROC_ENA_bits(10) ,DATA_RDY_array(3));
MAX_CHUNK_LEN_EPATH3: MUX2_Nbit generic map(N => 3 )    port map (maxClen_EPROC16 ,maxClen_EPROC2   ,EPROC_ENA_bits(10) ,maxClen_array(3));
---


------------------------------------------------------------
-- E-PATH-2
------------------------------------------------------------
--- MUX2:IN0
EPROC_IN4_ID04_PATH2: entity work.EPROC_IN4 (Behavioral)
generic map (
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc4_8b10b  => EnableToHo_Egroup0Eproc4_8b10b,
                EnableToHo_Egroup1Eproc4_8b10b  => EnableToHo_Egroup1Eproc4_8b10b,
                EnableToHo_Egroup2Eproc4_8b10b  => EnableToHo_Egroup2Eproc4_8b10b,
                EnableToHo_Egroup3Eproc4_8b10b  => EnableToHo_Egroup3Eproc4_8b10b,
                EnableToHo_Egroup4Eproc4_8b10b  => EnableToHo_Egroup4Eproc4_8b10b,
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(4),
	swap_inputbits => epath_input_bit_order(2),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(2),
	EDATA_IN   => DATA16bitIN(7 downto 4),
	DATA_OUT   => DATA_OUT_EID(4),
	DATA_RDY   => DATA_RDY_EID(4)
);
--- 
--- MUX2:IN1
EPROC_IN2_ID09_PATH2: entity work.EPROC_IN2 (Behavioral)
generic map (
                IC_Elink                        => false,
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	bitCLKx2   => clk80,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(9),
	swap_inputbits => epath_input_bit_order(2),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(2),
	EDATA_IN   => DATA16bitIN(5 downto 4),
	DATA_OUT   => DATA_OUT_EID(9),
	DATA_RDY   => DATA_RDY_EID(9)
);

--- 
MUX2_DATA_OUT_EPATH2: MUX2_Nbit generic map(N => 10)    port map (DATA_OUT_EID(4) ,DATA_OUT_EID(9) ,EPROC_ENA_bits(9) ,DATA_OUT_array(2));
MUX2_DATA_RDY_EPATH2: MUX2                              port map (DATA_RDY_EID(4) ,DATA_RDY_EID(9) ,EPROC_ENA_bits(9) ,DATA_RDY_array(2));
MAX_CHUNK_LEN_EPATH2: MUX2_Nbit generic map(N => 3 )    port map (maxClen_EPROC4  ,maxClen_EPROC2  ,EPROC_ENA_bits(9) ,maxClen_array(2));
---



------------------------------------------------------------
-- E-PATH-1
------------------------------------------------------------
--- MUX2:IN0
EPROC_IN8_ID01_PATH1: entity work.EPROC_IN8 (Behavioral)
generic map (
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc8_8b10b  => EnableToHo_Egroup0Eproc8_8b10b,
                EnableToHo_Egroup1Eproc8_8b10b  => EnableToHo_Egroup1Eproc8_8b10b,
                EnableToHo_Egroup2Eproc8_8b10b  => EnableToHo_Egroup2Eproc8_8b10b,
                EnableToHo_Egroup3Eproc8_8b10b  => EnableToHo_Egroup3Eproc8_8b10b,
                EnableToHo_Egroup4Eproc8_8b10b  => EnableToHo_Egroup4Eproc8_8b10b,
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(1),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(1),
	EDATA_IN   => DATA16bitIN(7 downto 0),
	DATA_OUT   => DATA_OUT_EID(1),
	DATA_RDY   => DATA_RDY_EID(1)
);
--- 
--- MUX2:IN1
EPROC_IN2_ID08_PATH1: entity work.EPROC_IN2 (Behavioral)
generic map (
                IC_Elink                        => false,
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	bitCLKx2   => clk80,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(8),
	swap_inputbits => epath_input_bit_order(1),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(1),
	EDATA_IN   => DATA16bitIN(3 downto 2),
	DATA_OUT   => DATA_OUT_EID(8),
	DATA_RDY   => DATA_RDY_EID(8)
);

--- 
MUX2_DATA_OUT_EPATH1: MUX2_Nbit generic map(N => 10)    port map (DATA_OUT_EID(1) ,DATA_OUT_EID(8) ,EPROC_ENA_bits(8) ,DATA_OUT_array(1));
MUX2_DATA_RDY_EPATH1: MUX2                              port map (DATA_RDY_EID(1) ,DATA_RDY_EID(8) ,EPROC_ENA_bits(8) ,DATA_RDY_array(1));
MAX_CHUNK_LEN_EPATH1: MUX2_Nbit generic map(N => 3 )    port map (maxClen_EPROC8  ,maxClen_EPROC2  ,EPROC_ENA_bits(8) ,maxClen_array(1));
---


------------------------------------------------------------
-- E-PATH-0
------------------------------------------------------------
--- MUX2:IN0
EPROC_IN4_ID03_PATH0: entity work.EPROC_IN4 (Behavioral)
generic map (
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc4_8b10b  => EnableToHo_Egroup0Eproc4_8b10b,
                EnableToHo_Egroup1Eproc4_8b10b  => EnableToHo_Egroup1Eproc4_8b10b,
                EnableToHo_Egroup2Eproc4_8b10b  => EnableToHo_Egroup2Eproc4_8b10b,
                EnableToHo_Egroup3Eproc4_8b10b  => EnableToHo_Egroup3Eproc4_8b10b,
                EnableToHo_Egroup4Eproc4_8b10b  => EnableToHo_Egroup4Eproc4_8b10b,
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(3),
	swap_inputbits => epath_input_bit_order(0),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(0),
	EDATA_IN   => DATA16bitIN(3 downto 0),
	DATA_OUT   => DATA_OUT_EID(3),
	DATA_RDY   => DATA_RDY_EID(3)
);
--- 
--- MUX2:IN1
EPROC_IN2_ID07_PATH0: entity work.EPROC_IN2 (Behavioral)
generic map (
                IC_Elink                        => false,
                egroupID                        => egroupID,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B
            )
port map (
	bitCLK     => clk40,
	bitCLKx2   => clk80,
	rst        => rst_clk40, 
	ENA        => EPROC_ENA_bits(7),
	swap_inputbits => epath_input_bit_order(0),
	thCR_REVERSE_10B => thCR_REVERSE_10B,
	ENCODING   => PATHencoding_array(0),
	EDATA_IN   => DATA16bitIN(1 downto 0),
	DATA_OUT   => DATA_OUT_EID(7),
	DATA_RDY   => DATA_RDY_EID(7)
);

--- 
MUX2_DATA_OUT_EPATH0: MUX2_Nbit generic map(N => 10)    port map (DATA_OUT_EID(3) ,DATA_OUT_EID(7) ,EPROC_ENA_bits(7) ,DATA_OUT_array(0));
MUX2_DATA_RDY_EPATH0: MUX2                              port map (DATA_RDY_EID(3) ,DATA_RDY_EID(7) ,EPROC_ENA_bits(7) ,DATA_RDY_array(0));
MAX_CHUNK_LEN_EPATH0: MUX2_Nbit generic map(N => 3 )    port map (maxClen_EPROC4  ,maxClen_EPROC2  ,EPROC_ENA_bits(7) ,maxClen_array(0));
---


process(clk40)
begin
    if rising_edge (clk40) then
        EPATH_BUSY <= epathBusy_array;
	end if;
end process;



------------------------------------------------------------
-- 8 EPATH FIFO DRIVERs
------------------------------------------------------------
PATH_FIFO_DRIVERs:  for I in 0 to 7 generate
--
EPATH_HFs <= EPATH_HFs_s;

--
FDn: entity work.EPROC_FIFO_DRIVER (Behavioral)
generic map (
                GBTid                           => GBTid,
                egroupID                        => egroupID,
                epathID                         => I,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup0Eproc4_8b10b  => EnableToHo_Egroup0Eproc4_8b10b,
                EnableToHo_Egroup0Eproc8_8b10b  => EnableToHo_Egroup0Eproc8_8b10b,
                EnableToHo_Egroup0Eproc16_8b10b => EnableToHo_Egroup0Eproc16_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup1Eproc4_8b10b  => EnableToHo_Egroup1Eproc4_8b10b,
                EnableToHo_Egroup1Eproc8_8b10b  => EnableToHo_Egroup1Eproc8_8b10b,
                EnableToHo_Egroup1Eproc16_8b10b => EnableToHo_Egroup1Eproc16_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup2Eproc4_8b10b  => EnableToHo_Egroup2Eproc4_8b10b,
                EnableToHo_Egroup2Eproc8_8b10b  => EnableToHo_Egroup2Eproc8_8b10b,
                EnableToHo_Egroup2Eproc16_8b10b => EnableToHo_Egroup2Eproc16_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup3Eproc4_8b10b  => EnableToHo_Egroup3Eproc4_8b10b,
                EnableToHo_Egroup3Eproc8_8b10b  => EnableToHo_Egroup3Eproc8_8b10b,
                EnableToHo_Egroup3Eproc16_8b10b => EnableToHo_Egroup3Eproc16_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup4Eproc4_8b10b  => EnableToHo_Egroup4Eproc4_8b10b,
                EnableToHo_Egroup4Eproc8_8b10b  => EnableToHo_Egroup4Eproc8_8b10b,
                EnableToHo_Egroup4Eproc16_8b10b => EnableToHo_Egroup4Eproc16_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
                toHostTimeoutBitn               => toHostTimeoutBitn,
                includeNoEncodingCase           => includeNoEncodingCase,
			    GENERATE_FEI4B          		=> GENERATE_FEI4B,
			    GENERATE_TRUNCATION_MECHANISM   => GENERATE_TRUNCATION_MECHANISM
            )
port map (
    clk40           => clk40,
    clk160          => clk160,
    rst             => rst_clk40,
    encoding        => PATHencoding_array(I),
    maxCLEN         => maxClen_array(0), 
    raw_DIN         => DATA_OUT_array(I),
    raw_DIN_RDY     => DATA_RDY_array(I),
    prog_full_in    => prog_full_array(I),
    timeCntIn       => timeCntIn,
    TimeoutEnaIn    => TimeoutEnaIn,
    instTimeoutEnaIn=> instant_timeout_ena(I),
    wordOUT         => BWORD_array(I),
    wordOUT_RDY     => BWORD_RDY_array(I),
    busyOut         => epathBusy_array(I) 
    );
end generate PATH_FIFO_DRIVERs;


------------------------------------------------------------
-- 8 EPATH FIFOs
------------------------------------------------------------
EPATH_FIFOs:  for I in 0 to 7 generate
EFn: entity work.EPATH_FIFO_WRAP (Behavioral)
generic map (
                GBTid                           => GBTid,
                egroupID                        => egroupID,
                epathID                         => I,
                EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
                EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
                EnableToHo_Egroup0Eproc4_8b10b  => EnableToHo_Egroup0Eproc4_8b10b,
                EnableToHo_Egroup0Eproc8_8b10b  => EnableToHo_Egroup0Eproc8_8b10b,
                EnableToHo_Egroup0Eproc16_8b10b => EnableToHo_Egroup0Eproc16_8b10b,
                EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
                EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
                EnableToHo_Egroup1Eproc4_8b10b  => EnableToHo_Egroup1Eproc4_8b10b,
                EnableToHo_Egroup1Eproc8_8b10b  => EnableToHo_Egroup1Eproc8_8b10b,
                EnableToHo_Egroup1Eproc16_8b10b => EnableToHo_Egroup1Eproc16_8b10b,
                EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
                EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
                EnableToHo_Egroup2Eproc4_8b10b  => EnableToHo_Egroup2Eproc4_8b10b,
                EnableToHo_Egroup2Eproc8_8b10b  => EnableToHo_Egroup2Eproc8_8b10b,
                EnableToHo_Egroup2Eproc16_8b10b => EnableToHo_Egroup2Eproc16_8b10b,
                EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
                EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
                EnableToHo_Egroup3Eproc4_8b10b  => EnableToHo_Egroup3Eproc4_8b10b,
                EnableToHo_Egroup3Eproc8_8b10b  => EnableToHo_Egroup3Eproc8_8b10b,
                EnableToHo_Egroup3Eproc16_8b10b => EnableToHo_Egroup3Eproc16_8b10b,
                EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
                EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
                EnableToHo_Egroup4Eproc4_8b10b  => EnableToHo_Egroup4Eproc4_8b10b,
                EnableToHo_Egroup4Eproc8_8b10b  => EnableToHo_Egroup4Eproc8_8b10b,
                EnableToHo_Egroup4Eproc16_8b10b => EnableToHo_Egroup4Eproc16_8b10b,
                EnableToHo_Egroup7Eproc2_HDLC   => false, --EC E-link
                EnableToHo_Egroup7Eproc2_8b10b  => false, --EC E-link
                includeNoEncodingCase           => includeNoEncodingCase
            )
  port map (
    rst             => rst_clk40,
    fifoFlush       => fifoFlush,
    wr_clk 	        => clk160,
    rd_clk 	        => clk240,
    din             => BWORD_array(I),
    wr_en           => BWORD_RDY_array(I),
    rd_en           => EPATH_REs(I),
    dout            => EPATH_DOUT_array(I), --_s(I),
    prog_full    	=> prog_full_array(I),
    half_full       => EPATH_HFs_s(I) -- Half-Full - output
    );
--
EPATH_AFs(I) <= prog_full_array(I);
--
end generate EPATH_FIFOs;
---
------------------------------------------------------------
------------------------------------------------------------
--
process(clk240)
begin
    if rising_edge (clk240) then
        EPATH_REs_clk1 <= EPATH_REs;
        EPATH_ID_clk1 <= EPATH_ID;
	end if;
end process;
---
MUX8_16bit_EOUT: entity work.MUX8_16bit_sync
port map (
	clk        => clk240,
	data_rdy   => EPATH_REs_clk1,
	data0      => EPATH_DOUT_array(0),
	data1      => EPATH_DOUT_array(1),
	data2      => EPATH_DOUT_array(2),
	data3      => EPATH_DOUT_array(3),
	data4      => EPATH_DOUT_array(4),
	data5      => EPATH_DOUT_array(5),
	data6      => EPATH_DOUT_array(6),
	data7      => EPATH_DOUT_array(7),
	sel        => EPATH_ID_clk1,
	data_out       => EOUT, 
	data_out_rdy   => EOUT_rdy
);
------------------------------------------------------------
------------------------------------------------------------






end Behavioral;
