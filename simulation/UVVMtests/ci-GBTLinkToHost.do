
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

source ./GBTLinkToHost_import_questa.tcl
vsim -voptargs="+acc" work.GBTLinkToHost_tb work.glbl
add wave -group top -position insertpoint sim:/gbtlinktohost_tb/*
for {set i 0} {$i < 12} {incr i} {
    add wave -group chStreamController${i} -format analog -min 0 -max 2027 -height 100 -position insertpoint sim:/gbtlinktohost_tb/crt0/thFMdataManagers(${i})/thFMdmN/wr_data_count
    add wave -position insertpoint -group chStreamController${i} sim:/gbtlinktohost_tb/crt0/thFMdataManagers(${i})/thFMdmN/g_32b/chStreamController/chunk_proc/*
    add wave -position insertpoint -group chStreamController${i} sim:/gbtlinktohost_tb/crt0/thFMdataManagers(${i})/thFMdmN/g_32b/chStreamController/*
}
run -all
quit
