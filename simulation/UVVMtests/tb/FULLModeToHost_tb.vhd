--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Filiberto Bonini
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  F. Schreuder (Nikhef), A. Skaf (University of Goettingen)
----------------------------------------------------------------------------------
-- file: FULLModeToHost_tb
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

    use work.pcie_package.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.centralRouter_package.all;
    use work.axi_stream_package.all;
    use work.interlaken_package.all;

-- Test bench entity
entity FULLModeToHost_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;

-- Test bench architecture
architecture arch of FULLModeToHost_tb is

    constant C_SCOPE : string := "FULLModeToHost_tb";
    constant C_CLK240_PERIOD : time    := 4.158 ns;
    constant C_CLK40_PERIOD  : time    := C_CLK240_PERIOD*6;
    constant C_CLK160_PERIOD : time    := C_CLK40_PERIOD/4;
    constant C_CLK250_PERIOD : time    := 4 ns;
    constant C_APPREGCLK_PERIOD : time := 30 ns;

    constant GBT_NUM : integer := 12;
    constant ENDPOINTS : integer := 1;
    constant FIRMWARE_MODE : integer := FIRMWARE_MODE_FULL;
    constant NUMBER_OF_DESCRIPTORS : integer := 5;
    constant STREAMS_TOHOST: integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE);
    constant BLOCKSIZE : integer :=4096;
    constant LOCK_PERIOD : integer := 640;

    constant DATA_WIDTH : integer := 256;
    constant SIM_NUMBER_OF_BLOCKS : integer := 200;

    constant pcie_endpoint: integer := 0;
    signal GBT_UPLINK_USER_DATA                : txrx120b_type(0 to (GBT_NUM-1)); -- @suppress "signal GBT_UPLINK_USER_DATA is never read"
    signal register_map_xoff_monitor           : register_map_xoff_monitor_type; -- @suppress "signal register_map_xoff_monitor is never read"
    signal register_map_gbtemu_monitor         : register_map_gbtemu_monitor_type; -- @suppress "signal register_map_gbtemu_monitor is never read"
    signal register_map_decoding_monitor       : register_map_decoding_monitor_type; -- @suppress "signal register_map_decoding_monitor is never read"
    signal register_map_40_control             : register_map_control_type;
    signal register_map_control_appreg_clk     : register_map_control_type;
    signal emuToHost_GBTdata: std_logic_vector(119 downto 0); -- @suppress "Unused declaration"

    constant GBT_UPLINK_USER_DATA_FOSEL : txrx120b_type (0 to GBT_NUM/ENDPOINTS-1) := (others => (others => '0'));
    --signal GBT_DOWNLINK_USER_DATA_ENCODING : txrx120b_type (0 to GBT_NUM/ENDPOINTS-1);
    signal LinkAligned_FOSEL : std_logic_vector(GBT_NUM/ENDPOINTS-1 downto 0);
    signal toHostFifo_din                      : slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
    signal toHostFifo_wr_en                    : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_prog_full                : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
    signal toHostFifo_wr_clk                   : std_logic;
    signal decoding_aclk                       : std_logic;
    signal decoding_axis                       : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal decoding_axis_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal decoding_axis_prog_empty            : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal emu_axis                            : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal emu_axis_tready                     : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal emu_axis_prog_empty                 : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);

    signal fanout_sel_axis                     : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal fanout_sel_axis_tready              : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal fanout_sel_axis_prog_empty            : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
    signal LinkAligned                         : std_logic_vector(GBT_NUM-1 downto 0);
    signal RXUSRCLK                            : std_logic_vector(GBT_NUM-1 downto 0);
    signal appreg_clk                           : std_logic;

    signal clk40, clk240, clk250, clk160  : std_logic:='0';
    signal clock_ena     : boolean   := false;

    signal reset : std_logic;
    signal aresetn : std_logic;

    signal start_check: std_logic:='0'; -- @suppress "signal start_check is never read"

    signal Trunc      : std_logic:='0'; -- @suppress "signal Trunc is never read"
    signal TrailerError:std_logic:='0'; -- @suppress "signal TrailerError is never read"
    signal CRCError : std_logic:='0'; -- @suppress "signal CRCError is never read"
    signal ChunkLengthError: std_logic:='0'; -- @suppress "signal ChunkLengthError is never read"
    signal GTH_FM_RX_33b_out                   : txrx33b_type(0 to (GBT_NUM-1));
    signal checker_done: std_logic;
    constant Trailer32b : boolean := true;
    constant ElinkBusyIn : busyOut_array_type(0 to GBT_NUM-1) := (others => (others => '0'));
    constant DmaBusyIn : std_logic := '0';
    constant FifoBusyIn : std_logic := '0';
    constant BusySumIn : std_logic := '0';
    signal decoding_axis_aux : axis_32_array_type(0 to 1);
    signal decoding_axis_aux_prog_empty : axis_tready_array_type(0 to 1);
    signal decoding_axis_aux_tready : axis_tready_array_type(0 to 1);
    constant TTC_ToHost_Data_in : TTC_ToHost_data_type := (
                                                            BCID => x"000",
                                                            FMT => x"02",
                                                            L0ID => x"0000_0000",
                                                            L1ID => x"00_0000",
                                                            XL1ID => x"00",
                                                            LEN => x"1A",
                                                            orbit => x"0000_0000",
                                                            reserved0 => x"0",
                                                            reserved1 => x"0000",
                                                            trigger_type => x"0000",
                                                            data_rdy => '0',
                                                            TAG => x"00"
                                                          );
    signal CRTOHOST_DMA_DESCRIPTOR_WREN    : std_logic_vector(0 downto 0);
    signal CRTOHOST_DMA_DESCRIPTOR_DESCR   : std_logic_vector(2 downto 0);
    signal CRTOHOST_DMA_DESCRIPTOR_AXIS_ID : std_logic_vector(10 downto 0);
    signal emu_FE_BUSY : busyOut_array_type(0 to GBT_NUM/ENDPOINTS-1);
    signal decoding_FE_BUSY : busyOut_array_type(0 to GBT_NUM/ENDPOINTS-1);
    signal FE_BUSY_out : busyOut_array_type(0 to GBT_NUM/ENDPOINTS-1); -- @suppress "signal FE_BUSY_out is never read"

begin

    LinkAligned <= (others => '0');
    GTH_FM_RX_33b_out <= (others => (others => '0'));
    GBT_UPLINK_USER_DATA <= (others => (others => '0'));

    register_map_40_control.DECODING_REVERSE_10B <= "1";
    register_map_40_control.ENCODING_REVERSE_10B <= "1";
    register_map_40_control.TIMEOUT_CTRL.TIMEOUT <= x"0000FFFF";
    register_map_40_control.TIMEOUT_CTRL.ENABLE <= "1";
    register_map_40_control.DISCARD_DATA_FOR_DESCR.DMA_DISABLED <= x"00";
    register_map_40_control.DISCARD_DATA_FOR_DESCR.FIFO_FULL <= x"00";

    --register_map_40_control.CR_FM_PATH_ENA <= (others => '1');

    GEN_SCF_LINKS_0_5: for i in 0 to 5 generate
        register_map_40_control.SUPER_CHUNK_FACTOR_LINK(i) <= x"06";
    end generate;
    GEN_SCF_LINKS_6_11: for i in 6 to 11 generate
        register_map_40_control.SUPER_CHUNK_FACTOR_LINK(i) <= x"01";
    end generate;

    RXUSRCLK <= (others => clk240);

    reset_proc: process
    begin
        reset <= '1';
        wait for C_CLK40_PERIOD*15;
        reset <= '0';
        wait;
    end process;

    aresetn     <= not reset;

    register_map_40_control.FE_EMU_ENA.EMU_TOHOST <= "1";
    g_path_ena: for i in 0 to GBT_NUM-1 generate
        register_map_40_control.DECODING_EGROUP_CTRL(i)(0).EPATH_ENA(0) <= '1';
    end generate;
    register_map_40_control.FE_EMU_ENA.EMU_TOFRONTEND <= "1";
    register_map_40_control.FE_EMU_CONFIG.WE <= (others => '0');
    register_map_40_control.FE_EMU_CONFIG.WRADDR   <= (others => '0');
    register_map_40_control.FE_EMU_CONFIG.WRDATA   <= (others => '0');
    register_map_40_control.GBT_TOHOST_FANOUT.SEL <= (others => '1');
    register_map_control_appreg_clk <= register_map_40_control;
    register_map_40_control.CRTOHOST_DMA_DESCRIPTOR_1.WR_EN <= CRTOHOST_DMA_DESCRIPTOR_WREN;
    register_map_40_control.CRTOHOST_DMA_DESCRIPTOR_1.DESCR <= CRTOHOST_DMA_DESCRIPTOR_DESCR;
    register_map_40_control.CRTOHOST_DMA_DESCRIPTOR_2.AXIS_ID <= CRTOHOST_DMA_DESCRIPTOR_AXIS_ID;
    register_map_40_control.FE_EMU_LOGIC.ENA <= "1";
    register_map_40_control.FE_EMU_LOGIC.L1A_TRIGGERED <= "0";
    register_map_40_control.FE_EMU_LOGIC.IDLES <= x"0010";
    register_map_40_control.FE_EMU_LOGIC.CHUNK_LENGTH <= x"0010";
    register_map_40_control.FULLMODE_32B_SOP <= "1";
    associate_epath_id: process
    begin
        CRTOHOST_DMA_DESCRIPTOR_WREN <= "0";
        wait until falling_edge(reset);
        wait for C_CLK40_PERIOD*100;
        CRTOHOST_DMA_DESCRIPTOR_WREN <= "1";
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00000000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "000";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00001000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "001";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00010000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "010";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00011000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "011";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00100000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR  <= "000";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00101000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR  <= "001";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00110000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "010";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "00111000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "011";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "01000000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "000";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "01001000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "001";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "01010000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "010";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_AXIS_ID <= "01011000000";
        CRTOHOST_DMA_DESCRIPTOR_DESCR   <= "011";
        wait for C_CLK40_PERIOD;
        CRTOHOST_DMA_DESCRIPTOR_WREN <= "0";
        wait;
    end process;

    g_NogbtEmu: if FIRMWARE_MODE /= FIRMWARE_MODE_GBT generate
        LinkAligned_FOSEL <= LinkAligned(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint);
    end generate;

    decoding0: entity work.decoding
        generic map(
            CARD_TYPE => 712,
            GBT_NUM => GBT_NUM/ENDPOINTS,
            FIRMWARE_MODE => FIRMWARE_MODE,
            STREAMS_TOHOST => STREAMS_TOHOST,
            BLOCKSIZE => BLOCKSIZE,
            LOCK_PERIOD => LOCK_PERIOD,
            IncludeDecodingEpath2_HDLC => "1111111",
            IncludeDecodingEpath2_8b10b => "1111111",
            IncludeDecodingEpath4_8b10b => "1111111",
            IncludeDecodingEpath8_8b10b => "1111111",
            IncludeDecodingEpath16_8b10b => "1111111",
            IncludeDecodingEpath32_8b10b => "1111111",
            IncludeDirectDecoding => "0000000",
            RD53Version => "A",
            PCIE_ENDPOINT => 0,
            VERSAL => false,
            AddFULLMODEForDUNE => false
        )
        Port map(
            RXUSRCLK => RXUSRCLK(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint), --: in  std_logic_vector(GBT_NUM-1 downto 0);
            FULL_UPLINK_USER_DATA => GTH_FM_RX_33b_out((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1), --: in  txrx33b_type(0 to (GBT_NUM-1)); --Full mode data input
            GBT_UPLINK_USER_DATA => GBT_UPLINK_USER_DATA_FOSEL, -- : in  txrx120b_type(GBT_NUM-1 downto 0);   --GBT data input
            lpGBT_UPLINK_USER_DATA => (others => (others => '0')), -- TODO: connect lpGBT data : in  txrx230b_type(GBT_NUM-1 downto 0); --lpGBT data input
            lpGBT_UPLINK_EC_DATA => (others => (others => '0')), -- TODO: connect lpGBT data : in  txrx2b_type(GBT_NUM-1 downto 0);   --lpGBT EC data input
            lpGBT_UPLINK_IC_DATA => (others => (others => '0')), -- TODO: connect lpGBT data : in  txrx2b_type(GBT_NUM-1 downto 0);   --lpGBT IC data input
            LinkAligned => LinkAligned_FOSEL, --: in  std_logic_vector(GBT_NUM-1 downto 0);
            clk160 => clk160, --: in  std_logic;
            clk240 => clk240,
            clk250 => clk250, --: in  std_logic;
            clk40 => clk40, --: in  std_logic;
            clk365                        => '0', --not used in FULL mode
            aclk_out => decoding_aclk, --: out std_logic;
            aresetn => aresetn, --: in  std_logic;
            m_axis => decoding_axis,
            m_axis_tready => decoding_axis_tready,
            m_axis_prog_empty => decoding_axis_prog_empty,
            m_axis_noSC => open,
            m_axis_noSC_tready => (others => (others => '1')),
            m_axis_noSC_prog_empty => open,
            TTC_ToHost_Data_in => TTC_ToHost_Data_in,
            FE_BUSY_out => decoding_FE_BUSY,
            ElinkBusyIn => ElinkBusyIn,
            DmaBusyIn => DmaBusyIn,
            FifoBusyIn => FifoBusyIn,
            BusySumIn => BusySumIn,
            m_axis_aux => decoding_axis_aux,
            m_axis_aux_prog_empty => decoding_axis_aux_prog_empty,
            m_axis_aux_tready => decoding_axis_aux_tready,
            register_map_control => register_map_40_control,
            register_map_decoding_monitor => register_map_decoding_monitor,
            Interlaken_RX_Data_In         => (others => (others => '0')), --Interlaken_RX_Data(pcie_endpoint * (GBT_NUM / ENDPOINTS) to ((pcie_endpoint + 1) * (GBT_NUM / ENDPOINTS)) - 1),
            Interlaken_RX_Datavalid       => (others => '0'), --Interlaken_RX_Datavalid(((pcie_endpoint + 1) * (GBT_NUM / ENDPOINTS)) - 1 downto pcie_endpoint * (GBT_NUM / ENDPOINTS)),
            Interlaken_RX_Gearboxslip     => open,
            Interlaken_Decoder_Aligned_out => open,
            m_axis64                      => open,
            m_axis64_tready               => (others => '0'),
            m_axis64_prog_empty           => open,
            toHost_axis64_aclk_out        => open
        );
    --CR_TOHOST_GBT_MON     => register_map_cr_monitor.CR_TOHOST_GBT_MON      );

    crth0: entity work.CRToHost
        generic map(
            NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
            NUMBER_OF_INTERRUPTS => 8,
            LINK_NUM => GBT_NUM/ENDPOINTS,
            LINK_CONFIG => (0 to GBT_NUM-1 => 0),
            toHostTimeoutBitn => 16,
            STREAMS_TOHOST => STREAMS_TOHOST,
            BLOCKSIZE => BLOCKSIZE,
            DATA_WIDTH => DATA_WIDTH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_URAM => false)
        port map(
            clk40 => clk40,
            clk160 => clk160,
            clk250 => clk250,
            aclk_tohost => decoding_aclk,
            aclk64_tohost => '0',
            aresetn => aresetn,
            register_map_control => register_map_40_control,
            register_map_xoff_monitor => register_map_xoff_monitor,
            register_map_crtohost_monitor => open,
            interrupt_call => open,
            xoff_out => open,
            s_axis => fanout_sel_axis,
            s_axis_tready => fanout_sel_axis_tready,
            s_axis_prog_empty => fanout_sel_axis_prog_empty,
            s_axis_aux => decoding_axis_aux,
            s_axis_aux_tready => decoding_axis_aux_tready,
            s_axis_aux_prog_empty => decoding_axis_aux_prog_empty,
            s_axis64 => (others => axis_64_zero_c),
            s_axis64_tready => open,
            s_axis64_prog_empty => (others => '1'),
            dma_enable_in => (others => '1'),
            toHostFifo_din => toHostFifo_din,
            toHostFifo_wr_en => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            toHostFifo_rst => open);

    g_EnableFullModeEmulator: if FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
        emu0: entity work.FullModeDataEmulator
            generic map(
                GBT_NUM => GBT_NUM/ENDPOINTS,
                BLOCKSIZE => BLOCKSIZE,
                AddFULLMODEForDUNE => false,
                VERSAL => false)
            port map(
                appreg_clk => appreg_clk,
                register_map_control => register_map_40_control,
                register_map_control_appreg_clk => register_map_control_appreg_clk,
                register_map_gbtemu_monitor => register_map_gbtemu_monitor,
                FE_BUSY_out => emu_FE_BUSY,
                clk40 => clk40,
                clk240 => clk240,
                aclk => decoding_aclk,
                aresetn => aresetn,
                L1A_IN => '0',
                m_axis => emu_axis,
                m_axis_tready => emu_axis_tready,
                m_axis_prog_empty => emu_axis_prog_empty,
                m_axis_noSC => open,
                m_axis_noSC_tready => (others => (others => '1')),
                m_axis_noSC_prog_empty => open
            );

        fosel0: entity work.axis_32_fanout_selector
            generic map(
                GBT_NUM        => GBT_NUM/ENDPOINTS,
                STREAMS_TOHOST => STREAMS_TOHOST
            )
            port map(
                aclk                       => decoding_aclk,
                --aresetn                    => aresetn,
                emu_axis                   => emu_axis,
                emu_axis_tready            => emu_axis_tready,
                emu_axis_prog_empty        => emu_axis_prog_empty,
                emu_FE_BUSY_in             => emu_FE_BUSY,
                decoding_axis              => decoding_axis,
                decoding_axis_tready       => decoding_axis_tready,
                decoding_axis_prog_empty   => decoding_axis_prog_empty,
                decoding_FE_BUSY_in        => decoding_FE_BUSY,
                fanout_sel_axis            => fanout_sel_axis,
                fanout_sel_axis_tready     => fanout_sel_axis_tready,
                fanout_sel_axis_prog_empty => fanout_sel_axis_prog_empty,
                fanout_sel_FE_BUSY_out     => FE_BUSY_out,
                register_map_control       => register_map_40_control
            );

    end generate;

    clock_generator(clk40, clock_ena, C_CLK40_PERIOD, "40 MHz clock");
    clock_generator(clk160, clock_ena, C_CLK160_PERIOD, "160 MHz clock");
    clock_generator(clk240, clock_ena, C_CLK240_PERIOD, "240 MHz clock");
    clock_generator(clk250, clock_ena, C_CLK250_PERIOD, "250 MHz clock");
    clock_generator(appreg_clk, clock_ena, C_APPREGCLK_PERIOD, "25 MHz clock");

    sequencer : process
    begin
        clock_ena <= false;
        -- Print the configuration to the log
        --wait for 1 ns;
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        --enable_log_msg(ALL_MESSAGES);
        disable_log_msg(ALL_MESSAGES, NON_QUIET, C_SCOPE);
        enable_log_msg(ID_SEQUENCER, NON_QUIET, C_SCOPE);
        disable_log_msg(ID_POS_ACK, NON_QUIET, C_SCOPE);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "----- initializing test bench       ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Wait for UVVM to finish initialization
        --await_uvvm_initialization(VOID);

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-----    end of initialisation        ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        wait for (10*C_CLK40_PERIOD);
        clock_ena <= true;

        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);
        log(ID_SEQUENCER, "-- test bench sequence starts here      ", C_SCOPE);
        log(ID_SEQUENCER, "----------------------------------------", C_SCOPE);

        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);

        log(ID_LOG_HDR, "Starting simulation of TB for EGROUP using VVCs", C_SCOPE);
        ------------------------------------------------------------

        await_value(checker_done, '1', 0 ns, 1000 ms, TB_ERROR, "wait for all tests to finish", C_SCOPE);
        enable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_UVVM_SEND_CMD);

        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    --
    end process; -- sequencer

    sink0: entity work.FELIXDataSink
        generic map(
            NUMBER_OF_DESCRIPTORS           => NUMBER_OF_DESCRIPTORS,
            BLOCKSIZE                       => BLOCKSIZE,
            GBT_NUM                         => GBT_NUM,
            FIRMWARE_MODE                   => FIRMWARE_MODE,
            DATA_WIDTH                      => DATA_WIDTH,
            SIM_NUMBER_OF_BLOCKS            => SIM_NUMBER_OF_BLOCKS,
            APPLY_BACKPRESSURE_AFTER_BLOCKS => SIM_NUMBER_OF_BLOCKS/10,
            Trailer32b                      => Trailer32b
        )
        port map(
            checker_done         => checker_done,
            toHostFifo_din       => toHostFifo_din,
            toHostFifo_wr_en     => toHostFifo_wr_en,
            toHostFifo_prog_full => toHostFifo_prog_full,
            toHostFifo_wr_clk    => toHostFifo_wr_clk,
            reset                => reset,
            start_check          => start_check,
            Trunc                => Trunc,
            TrailerError         => TrailerError,
            CRCError             => CRCError,
            ChunkLengthError     => ChunkLengthError,
            register_map_control => register_map_40_control
        );

end architecture;

