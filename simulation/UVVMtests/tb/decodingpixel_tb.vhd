--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marco
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  M. Trovato
--  ANL
--  January 2023
----------------------------------------------------------------------------------
-- file: decodingpixel_tb.vhd.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.centralRouter_package.all;
    use ieee.std_logic_textio.all;
    use std.textio.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

--library uvvm_vvc_framework;
--use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

--use work.vvc_methods_pkg.all;
--use work.td_vvc_framework_common_methods_pkg.all;
--use work.egr_bfm_pkg.all;


-- Test bench entity
entity decodingpixel_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end decodingpixel_tb;

architecture tb of decodingpixel_tb is
    type array_4x4b is array (0 to 3) of std_logic_vector(3 downto 0);
    constant fname                        : string                                         := "../64b66b/RD53A_refout.txt";
    constant fnameCB                      : string                                         := "../64b66b/RD53A_refout_CB.txt";
    constant STREAMS_TOHOST               : integer                                        := 30;
    constant clk40_period                 : time                                           := 25 ns;
    constant aclk_period                  : time                                           := 6.25 ns;

    --signal PCIE_ENDPOINT                  : integer                                        := 0;
    constant LINK                           : integer                                        :=0;

    --file fptr, fptrCB : text;
    signal data32_ref                     : std_logic_vector(31 downto 0);
    signal data32_ref_dly                 : std_logic_vector(31 downto 0);
    signal data32_ref_dlydly              : std_logic_vector(31 downto 0); -- @suppress "signal data32_ref_dlydly is never read"
    signal isErr                          : std_logic := '0';

    signal register_map_control           : register_map_control_type;

    signal clk40                          : std_logic:= '0';
    signal clk40_tmp                      : std_logic:= '0';
    signal clk40_en                       : boolean   := false;
    signal aclk                           : std_logic:= '0';
    signal aclk_tmp                       : std_logic:= '0';
    signal aclk_en                        : boolean   := false;
    signal reset_dopulse : std_logic := '0';
    signal dopulse : std_logic := '1';
    signal reset                          : std_logic_vector(4 downto 0)                    := (others => '0');

    --signal DecoderAligned                 : std_logic_vector(STREAMS_TOHOST-3 downto 0);
    signal m_axis                         : axis_32_array_type(0 to STREAMS_TOHOST-1);
    signal m_axis_tready                  : axis_tready_array_type(0 to STREAMS_TOHOST-1);
    signal m_axis_prog_empty              : axis_tready_array_type(0 to STREAMS_TOHOST-1); -- @suppress "signal m_axis_prog_empty is never read" -- @suppress "signal trigid is never read"

    signal trigid                         : std_logic_vector(4 downto 0)                    := (others => '0'); -- @suppress "signal trigid is never read"
    signal trigtag                        : std_logic_vector(4 downto 0)                    := (others => '0'); -- @suppress "signal trigtag is never read"
    signal bcid                           : std_logic_vector(14 downto 0)                   := (others => '0'); -- @suppress "signal bcid is never read"
    signal isHDR                          : std_logic                                       := '0'; -- @suppress "signal isHDR is never read"
    signal col                            : std_logic_vector(5 downto 0)                    := (others => '0'); -- @suppress "signal col is never read"
    signal row                            : std_logic_vector(8 downto 0)                    := (others => '0'); -- @suppress "signal row is never read"
    signal onebside                       : std_logic := '0'; -- @suppress "signal onebside is never read"
    signal tot                            : array_4x4b                                      := (others => (others => '0')); -- @suppress "signal tot is never read"

    --signal CBOPT                          : std_logic_vector(3 downto 0); --to be included in register map
    --signal DISLANE                        : std_logic_vector(6 downto 0);
    --signal mask_k_char                    : std_logic_vector(3 downto 0);


    signal emuToHost_lpGBTdata            : std_logic_vector(223 downto 0);
    signal emuToHost_lpGBTECdata          : std_logic_vector(1 downto 0); -- @suppress "signal emuToHost_lpGBTECdata is never read"
    signal emuToHost_lpGBTICdata          : std_logic_vector(1 downto 0); -- @suppress "signal emuToHost_lpGBTICdata is never read"
    signal emuToHost_GBTlinkValid         : std_logic;

    signal m_axis0_dly                    : axis_32_type;
    signal m_axis0_dly_dly                : axis_32_type; -- @suppress "signal m_axis0_dly_dly is never read"
    --signal count_data                     : integer                                         := 0;
    signal countHDR                       : integer                                         := 0;
    signal tvalid_doublerate              : std_logic;
    signal start_simuCB0                  : boolean := false; -- @suppress "signal start_simuCB0 is never read"
    signal start_simuCB3                  : boolean := false; -- @suppress "signal start_simuCB3 is never read"
    signal done_simuCB0                   : boolean := false; -- @suppress "signal done_simuCB0 is never read"
    signal done_simuCB3                   : boolean := false; -- @suppress "signal done_simuCB3 is never read"
    signal timeout                        : boolean := false;
begin
    -----------------------------------------------------------------------------
    -- Registers
    -----------------------------------------------------------------------------
    --internal emulator
    register_map_control.GBT_TOHOST_FANOUT.SEL(0 downto 0)       <= "1";
    register_map_control.FE_EMU_ENA.EMU_TOHOST(0)                <= '1';
    register_map_control.FE_EMU_CONFIG.WRADDR                    <= (others => '0');
    --
    register_map_control.DECODING_DISEGROUP                      <= "1111000"; --disable all lane but 0,1,2
    register_map_control.DECODING_MASK64B66BKBLOCK               <= x"a";

    --egroup*: link 0 (hit data), 1 (DCS)
    --egroup>2 disabled
    register_map_control.DECODING_EGROUP_CTRL(0)(0).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(0).EPATH_ENA <= x"03";
    register_map_control.DECODING_EGROUP_CTRL(0)(1).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(1).EPATH_ENA <= x"03";
    register_map_control.DECODING_EGROUP_CTRL(0)(2).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(2).EPATH_ENA <= x"03";
    --
    m_axis_tready     <= (others => '1');

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clk40_tmp  <= not clk40_tmp after clk40_period/2; --12.5ns;
    clk40      <= clk40_tmp     when clk40_en else '0';
    aclk_tmp   <= not aclk_tmp  after aclk_period/2; --3.125ns;
    aclk       <= aclk_tmp      when aclk_en else '0';

    --clock_generator(clk40, clk40_en, clk40_period, "40 MHz CLK");
    --clock_generator(s_axis_aclk_i, s_axis_aclk_en, axiclk_period, "AXI CLK");

    -----------------------------------------------------------------------------
    -- Resets
    -----------------------------------------------------------------------------
    rst_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            if (reset_dopulse = '1') then
                reset    <= (others => '0');
                dopulse  <= '1';
            elsif(dopulse = '1' and reset(0) = '0') then
                reset(0) <= '1';
                dopulse  <= '1';
            elsif(dopulse = '1' and reset(0) = '1') then
                reset(0) <= '0';
                dopulse  <= '0';
            else
                reset(0) <= '0';
            end if;
            reset(1)   <= reset(0);
            reset(2)   <= reset(1);
            reset(3)   <= reset(2);
            reset(4)   <= reset(3);
        end if;
    end process;

    -----------------------------------------------------------------------------
    -- Emulator
    -----------------------------------------------------------------------------
    gbtEmuToHost0: entity work.GBTdataEmulator
        generic map(
            EMU_DIRECTION        => "ToHost",
            FIRMWARE_MODE        => FIRMWARE_MODE_PIXEL,
            MEM_DEPTH => 16384,
            LPGBT_TOHOST_WIDTH => 32,
            LPGBT_TOFE_WIDTH => 8
        )
        port map(
            clk40                => clk40,
            wrclk                => clk40,
            rst_hw               => reset(1),
            rst_soft             => reset(1),
            xoff                 => '0',
            register_map_control => register_map_control,
            GBTdata              => open,
            lpGBTdataToFE        => open,
            lpGBTdataToHost      => emuToHost_lpGBTdata,
            lpGBTECdata          => emuToHost_lpGBTECdata,
            lpGBTICdata          => emuToHost_lpGBTICdata,
            GBTlinkValid         => emuToHost_GBTlinkValid);

    -----------------------------------------------------------------------------
    -- UUT
    -----------------------------------------------------------------------------
    DecodingPixelLinkLPGBT_uut: entity work.DecodingPixelLinkLPGBT
        generic map(
            CARD_TYPE            => 712,
            RD53Version          => "A",
            BLOCKSIZE            => 1024,
            LINK                 => LINK,
            SIMU                 => 1,
            STREAMS_TOHOST       => STREAMS_TOHOST,
            PCIE_ENDPOINT        => 0,
            VERSAL               => false
        )
        port map(
            clk40                => clk40,
            reset                => reset(4),
            MsbFirst             => '1',

            LinkData             => emuToHost_lpGBTdata,
            LinkAligned          => emuToHost_GBTlinkValid,

            m_axis               => m_axis(0 to STREAMS_TOHOST-3),
            m_axis_tready        => m_axis_tready(0 to STREAMS_TOHOST-3),
            m_axis_aclk          => aclk,
            m_axis_prog_empty    => m_axis_prog_empty(0 to STREAMS_TOHOST-3),
            register_map_control => register_map_control,
            DecoderAligned_out   => open,
            DecoderDeskewed_out  => open,
            cnt_rx_64b66bhdr_out => open,
            rx_soft_err_cnt_out  => open,
            rx_soft_err_rst      => '0'


        );

    -----------------------------------------------------------------------------
    -- Data Checker
    -----------------------------------------------------------------------------
    interpretdata_proc: process(aclk)
    --variable line_out, file_line  : line;
    --variable good : boolean;
    begin
        if rising_edge(aclk) then
            if reset(1) = '1' then
                countHDR          <= 0;
                isErr             <= '0';
            else
                data32_ref_dlydly <= data32_ref_dly;
                data32_ref_dly    <= data32_ref;
                m_axis0_dly_dly   <= m_axis0_dly;
                m_axis0_dly       <= m_axis(0);

                if(m_axis(0).tdata /= data32_ref and m_axis(0).tvalid = '1') then
                    isErr         <= '1';
                end if;

                if(m_axis(0).tvalid = '1') then
                    if(m_axis(0).tdata(31 downto 25) = "0000001") then
                        countHDR  <= countHDR + 1;
                        isHDR     <= '1';
                        trigid    <= m_axis(0).tdata(24 downto 20);
                        trigtag   <= m_axis(0).tdata(19 downto 15);
                        bcid      <= m_axis(0).tdata(14 downto 0);
                        col       <= (others => '0');
                        row       <= (others => '0');
                        onebside  <= '0';
                        tot       <= (others => (others => '0'));
                    else
                        isHDR     <= '0';
                        trigid    <= (others => '0');
                        trigtag   <= (others => '0');
                        bcid      <= (others => '0');
                        col       <= m_axis(0).tdata(31 downto 26);
                        row       <= m_axis(0).tdata(25 downto 17);
                        onebside  <= m_axis(0).tdata(16);
                        tot(0)    <= m_axis(0).tdata(15 downto 12);
                        tot(1)    <= m_axis(0).tdata(11 downto  8);
                        tot(2)    <= m_axis(0).tdata( 7 downto  4);
                        tot(3)    <= m_axis(0).tdata( 3 downto  0);
                    end if;
                else
                    isHDR         <= '0';
                    trigid        <= (others => '0');
                    trigtag       <= (others => '0');
                    bcid          <= (others => '0');
                    col           <= (others => '0');
                    row           <= (others => '0');
                    onebside      <= '0';
                    tot           <= (others => (others => '0'));
                end if;
            end if;
        end if;
    end process;


    tvalid_doublerate_proc: process(aclk)
    begin
        if aclk'event and aclk = '1' then
            tvalid_doublerate <= m_axis(0).tvalid;
        elsif aclk'event and aclk = '0' then
            tvalid_doublerate <= '0';
        end if;
    end process;

    -----------------------------------------------------------------------------
    -- Main Process
    -----------------------------------------------------------------------------
    mainproc : process
        file fptr                : text open READ_MODE is fname;
        file fptrCB              : text open READ_MODE is fnameCB;
        variable file_line       : line;
        variable good            : boolean; -- @suppress "variable good is never read"
        variable data32          : std_logic_vector(31 downto 0);
        variable count_data      : integer                                         := 0;


        variable count_dataCB0   : integer                                         := 0; -- @suppress "variable count_dataCB0 is never read"
        variable countHDRCB0     : integer                                         := 0; -- @suppress "variable countHDRCB0 is never read"
        variable isERRCB0        : std_logic := '0'; -- @suppress "variable count_dataCB3 is never read"
        variable count_dataCB3   : integer                                         := 0; -- @suppress "variable count_dataCB3 is never read"
        variable countHDRCB3     : integer                                         := 0; -- @suppress "variable countHDRCB3 is never read"
        variable isERRCB3        : std_logic := '0';

        procedure printMT (arg: in string := "") is
            variable lineMT : line;
        begin
            std.textio.write(lineMT, arg);
            std.textio.writeline(std.textio.output, lineMT);
        end;

    begin
        assert false
            report "START SIMULATION"
            severity NOTE;

        clk40_en <= true;
        aclk_en  <= true;

        ------------START SIMULATING CB=0 scenario-------------
        start_simuCB0 <= true;

        readline(fptr, file_line); --skip first line

        register_map_control.DECODING_LINK_CB(0).CBOPT <= x"0";

        while (not endfile(fptr)) loop
            if timeout then
                exit;
            end if;
            readline(fptr, file_line);
            hread(file_line,data32,good);
            data32_ref <= data32;
            count_data := count_data + 1;
            wait until tvalid_doublerate = '1' or timeout;
        end loop;

        wait for 100 ns;

        file_close(fptr);

        isERRCB0 := isErr;
        count_dataCB0 := count_data;
        countHDRCB0 := countHDR;
        done_simuCB0 <= true;

        wait for 1000 ns;
        ------------DONE SIMULATING CB=0 scenario-------------

        ------------START SIMULATING CB=3 scenario-------------
        start_simuCB3 <= true;
        readline(fptrCB, file_line); --skip first line
        register_map_control.DECODING_LINK_CB(0).CBOPT <= x"3";
        count_data := 0;

        reset_dopulse <= '1'; --start reset chain
        wait for 100 ns;
        reset_dopulse <= '0';

        while (not endfile(fptrCB)) loop
            if timeout then
                exit;
            end if;
            readline(fptrCB, file_line);
            hread(file_line,data32,good);
            data32_ref <= data32;
            count_data := count_data + 1;
            wait until tvalid_doublerate = '1' or timeout;
        end loop;

        wait for 100 ns;

        -- Finish the simulation
        file_close(fptrCB);

        isERRCB3 := isErr;
        count_dataCB3 := count_data;
        countHDRCB3 := countHDR;
        done_simuCB3 <= true;
        wait for 100 ns;


        check_value(isERRCB0, '0', ERROR, "Single lane: Error while checking decoded data against reference", C_SCOPE);
        printMT("***Simulation passed, Checked #data = " & integer'image(count_data) & "; Headers " & integer'image(countHDR));
        check_value(isERRCB3, '0', ERROR, "Three lanes (channel bonding): Error while checking decoded data against reference", C_SCOPE);
        printMT("***Simulation passed, Checked #data = " & integer'image(count_data) & "; Headers " & integer'image(countHDR));

        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
        finish;
    end process;

    -----------------------------------------------------------------------------
    -- Timeout
    -----------------------------------------------------------------------------
    timeoutproc : process
    begin
        wait for 1000 us;
        timeout <= true;
    end process;

end tb;
