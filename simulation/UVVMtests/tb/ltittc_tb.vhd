----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  M. Trovato
--  ANL
--  January 2020
----------------------------------------------------------------------------------
-- file: DecodingGearBox_tb.vhd.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std_unsigned.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

--library uvvm_vvc_framework;
--use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

--use work.vvc_methods_pkg.all;
--use work.td_vvc_framework_common_methods_pkg.all;
--use work.egr_bfm_pkg.all;

    use work.pcie_package.all;
    use work.centralRouter_package.all;

-- Test bench entity
entity ltittc_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end ltittc_tb;

architecture tb of ltittc_tb is
    signal register_map_control        : register_map_control_type;

    signal rst_hw                      : std_logic                      := '0';
    signal reset_cnt                   : std_logic                      := '0';

    signal clk40                       : std_logic                      := '0';
    signal clk40_xtal                  : std_logic                      := '0';
    signal tx_refclk240_p              : std_logic                      := '1';
    signal tx_refclk240_n              : std_logic                      := '0';
    signal rx_refclk240_p              : std_logic                      := '1';
    signal rx_refclk240_n              : std_logic                      := '0';
    signal clk240                      : std_logic                      := '0';
    signal clk_ttc_40                  : std_logic                      := '0';

    type array_192b is array (natural range <>) of std_logic_vector(191 downto 0);
    --type array_184b is array (natural range <>) of std_logic_vector(183 downto 0);

    signal tx_data_40MHZ               : array_192b(0 to 7)             := (others => (others => '0'));

    signal tx_data_ttc                 : std_logic_vector(15 downto 0)  := (others => '0');
    signal isk_ttc                     : std_logic_vector( 1 downto 0)  := (others => '0');

    signal linkaligned_ttc             : std_logic                      := '0';

    signal rxusrclk_out_ttc            : std_logic                      := '0';
    signal rx_data_ttc                 : std_logic_vector(32 downto 0)  := (others => '0');
    signal txusrclk_out_ttc            : std_logic                      := '0';
    signal TTC_data_decoder_40         : array_192b(0 to 2)             := (others => (others => '0'));
    signal decoder_aligned             : std_logic                      := '0';

    signal ready                       : std_logic; -- @suppress "signal ready is never read"
    signal start_chk                   : std_logic                      := '0';
    signal end_chk                     : std_logic                      := '0';
    signal error_data                       : std_logic_vector(15 downto 0)  := (others => '0');
    signal error_decoder_align         : std_logic                      := '0';
    signal IsK_in : std_logic_vector(1 downto 0);
    signal RX_IsK_OUT : std_logic_vector(3 downto 0);

begin

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clk40            <= not clk40 after 12.5 ns;
    clk40_xtal      <= clk40;
    tx_refclk240_p  <= not tx_refclk240_p after 2.083 ns;
    tx_refclk240_n  <= not tx_refclk240_p;
    rx_refclk240_p  <= not rx_refclk240_p after 2.083 ns;
    rx_refclk240_n  <= not rx_refclk240_p;
    clk240          <= not clk240 after 2.083 ns;

    ---------------
    ---REGISTERS---
    ---------------
    register_map_control.LTITTC_CTRL.LTITTC_CHANNEL_DISABLE      <= "0";
    register_map_control.LTITTC_CTRL.LTITTC_GENERAL_CTRL         <= "00"; --do not the logic for 000
    register_map_control.LTITTC_CTRL.LTITTC_SOFT_RESET           <= "0";
    register_map_control.LTITTC_CTRL.LTITTC_CPLL_RESET           <= "0";
    register_map_control.LTITTC_CTRL.LTITTC_SOFT_TX_RESET        <= "0";
    register_map_control.LTITTC_CTRL.LTITTC_SOFT_RX_RESET        <= "0";
    register_map_control.LTITTC_CTRL.LTITTC_GTH_LOOPBACK_CONTROL <= "010";


    register_map_control.TTC_EMU.SEL                             <= "0";
    register_map_control.TTC_DEC_CTRL.TOHOST_RST                 <= "0";
    register_map_control.TTC_ECR_MONITOR.CLEAR                   <= "0";
    register_map_control.TTC_TTYPE_MONITOR.CLEAR                 <= "0";
    register_map_control.TTC_BCR_PERIODICITY_MONITOR.CLEAR       <= "0";

    rst_hw_proc: process
    begin
        rst_hw <= '1';
        wait for 25 ns;
        rst_hw <= '0';
        wait;
    end process;


    ---------------------------------------------
    ---TX DATA (to be looped back in the MGT---
    ---------------------------------------------
    FLX_LTITTCLink_Wrapper_inst : entity work.FLX_LTITTCLink_Wrapper
        generic map(
            SIMULATION => true
        )
        port map
    (
            rst_hw                      => rst_hw,
            register_map_control        => register_map_control,
            register_map_ltittc_monitor => open,
            RXP_IN(0)                   => '0', --not needed MGT in loopback
            RXN_IN(0)                   => '0',

            clk40_in                    => clk40,

            DRP_CLK_IN                  => clk40_xtal,

            GTREFCLK0_P_IN              => tx_refclk240_p,
            GTREFCLK0_N_IN              => tx_refclk240_n,
            GTREFCLK1_P_IN              => rx_refclk240_p,
            GTREFCLK1_N_IN              => rx_refclk240_n,

            RXUSERCLK_OUT               => rxusrclk_out_ttc,
            RX_DATA_33b_out             => rx_data_ttc,
            RX_DATA_33b_rdy_out         => linkaligned_ttc,

            --for testing only (either simu or loopback)
            TXUSERCLK_OUT               => txusrclk_out_ttc,
            TX_DATA_16b_in              => tx_data_ttc,
            ISK_in                      => isk_ttc,
            RX_IsK_OUT => RX_IsK_OUT
        --

        );

    --=====================--
    -- DUT: DECODER
    --=====================--

    ltittc_dec: entity work.ltittc_decoder
        port map
        (
            reset_in            => rst_hw,
            clk240_in           => rxusrclk_out_ttc,
            LinkAligned_in      => linkaligned_ttc,
            IsK_in => RX_IsK_OUT,
            data_in             => rx_data_ttc,

            data_out            => TTC_data_decoder_40(0),
            clk40_out           => clk_ttc_40,
            clk40_ready_out     => ready,  -- the ttc_clk_gated is present
            cnt_error_out       => open, --ltittc_bit_err_40
            decoder_aligned_out => decoder_aligned,
            crc_valid_out       => open
        );

    --=====================--
    -- WAVE GENERATOR
    --=====================--
    reset_cnt <= not start_chk;
    ltittc_wavegen: entity work.ltittc_wavegen
        generic map(
            GENCOUNTER => true
        )
        port map
    (
            reset_in           => rst_hw,
            reset_cnt_in       => reset_cnt,
            clk40_in           => clk40, --clk_ttc_40,
            clk240_in          => txusrclk_out_ttc,
            data_out           => open,
            isK_out            => open,
            tx_data_40MHZ_out  => tx_data_40MHZ(0)
        );


    check_proc : process
    begin
        wait for 10000 ns; --before that decoder_aligned is unstable...eyeballed

        check_value(linkaligned_ttc, '1', ERROR, "Link Alignment", C_SCOPE);
        check_value(decoder_aligned, '1', ERROR, "Decoder Alignment", C_SCOPE);



        start_chk <= '1';
        wait for 10000 ns;

        end_chk <= '1';
        wait for 100 ns;

        check_value(error_decoder_align, '0', ERROR,  "Decoder Alignment Lost During Data Check", C_SCOPE);

        check_value(error_data, x"0000", ERROR,  "Decoder Data Errors", C_SCOPE);

        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);
        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely
    end process;

    checkalignment_proc : process(clk_ttc_40)
    begin
        if rising_edge(clk_ttc_40) then
            if( start_chk = '1' and end_chk = '0' and
          decoder_aligned = '0' ) then
                error_decoder_align <= '1';
            end if;
        end if;
    end process;

    checkdata_proc : process(clk_ttc_40)
    begin
        if rising_edge(clk_ttc_40) then
            tx_data_40MHZ(7)           <= tx_data_40MHZ(6);
            tx_data_40MHZ(6)           <= tx_data_40MHZ(5);
            tx_data_40MHZ(5)           <= tx_data_40MHZ(4);
            tx_data_40MHZ(4)           <= tx_data_40MHZ(3);
            tx_data_40MHZ(3)           <= tx_data_40MHZ(2);
            tx_data_40MHZ(2)           <= tx_data_40MHZ(1);
            tx_data_40MHZ(1)           <= tx_data_40MHZ(0);

            TTC_data_decoder_40(2)     <= TTC_data_decoder_40(1);
            TTC_data_decoder_40(1)     <= TTC_data_decoder_40(0);
            if( start_chk = '1' and end_chk = '0' and
          tx_data_40MHZ(7)(23 downto 0) /= TTC_data_decoder_40(2)(23 downto 0) ) then
                error_data <= error_data + x"1";
            end if;
        end if;
    end process;

end tb;
