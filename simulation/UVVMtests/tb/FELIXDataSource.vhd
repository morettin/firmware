--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  F. Schreuder (Nikhef)
----------------------------------------------------------------------------------
-- file: FELIXDataSink.vhd
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

    use work.pcie_package.all;
--use work.lpgbtfpga_package.all;
--use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.centralRouter_package.all;
    use work.axi_stream_package.all;

library xpm;
    use xpm.vcomponents.all;



-- Test bench entity
entity FELIXDataSource is
    generic(
        DATA_WIDTH : integer := 256;
        ThrottleData : boolean := false
    );
    port(
        clk                : in std_logic;
        NumberOfChunks     : in integer;
        ChunkLength        : in integer;
        GBTId              : in integer;
        StreamID           : in integer;
        ElinkWidth         : in integer;
        fromHostFifo_dout  : out std_logic_vector(DATA_WIDTH-1 downto 0);
        fromHostFifo_empty : out std_logic;
        fromHostFifo_rd_en : in  std_logic;
        start              : in std_logic;
        done               : out std_logic
    );
end entity;

architecture arch of FELIXDataSource is
    signal fromHostFifo_wr_en : std_logic;
    signal fromHostFifo_almost_full : std_logic;
    signal fromHostFifo_din : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal wrclk : std_logic;
    constant wrclk_period : time := 2 ns;
    signal reset: std_logic;
    signal wr_rst_busy: std_logic;
begin

    wrclk_proc: process
    begin
        wrclk <= '1';
        wait for wrclk_period/2;
        wrclk <= '0';
        wait for wrclk_period/2;
    end process;


    reset_proc: process
    begin
        reset <= '1';
        wait for wrclk_period * 5;
        reset <= '0';
        wait;
    end process;



    fromHostFifo0: xpm_fifo_async generic map( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK, WAKEUP_TIME"
            FIFO_MEMORY_TYPE => "auto",
            FIFO_WRITE_DEPTH => 4096/32,
            RELATED_CLOCKS => 0,
            WRITE_DATA_WIDTH => DATA_WIDTH,
            READ_MODE => "std",
            FIFO_READ_LATENCY => 1,
            FULL_RESET_VALUE => 0,
            USE_ADV_FEATURES => "070F",
            READ_DATA_WIDTH => DATA_WIDTH,
            CDC_SYNC_STAGES => 2,
            WR_DATA_COUNT_WIDTH => 8,
            PROG_FULL_THRESH => 4096/32-5,
            RD_DATA_COUNT_WIDTH => 7,
            PROG_EMPTY_THRESH => 5,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc"
        )
        port map(
            sleep => '0',
            rst => reset,
            wr_clk => wrclk,
            wr_en => fromHostFifo_wr_en,
            din => fromHostFifo_din,
            full => open,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => wr_rst_busy,
            almost_full => fromHostFifo_almost_full,
            wr_ack => open,
            rd_clk => clk,
            rd_en => fromHostFifo_rd_en,
            dout => fromHostFifo_dout,
            empty => fromHostFifo_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );



    dataSource0: process(wrclk)
        variable bytesToDo: integer:= 0;
        variable l: integer := 0;
        variable byteCNT : integer := 0;
        variable byteIndex : integer := 0;
        variable chunklengthSLV, L1ID : std_logic_vector(15 downto 0);
        variable pauseCNT : integer := 0;
        variable chunk: integer;
        variable fromHostFifo_din_v: std_logic_vector(DATA_WIDTH-1 downto 0);
    begin
        if rising_edge(wrclk) then
            if done /= '0' then
                done <= '1';
                pauseCNT := 0;
                bytesToDo := 0;
                byteCNT := 0;
                chunk := 0;
                fromHostFifo_din_v := (others => '0');
                fromHostFifo_wr_en <= '0';
                if start = '1' then
                    done <= '0';
                end if;
                fromHostFifo_din <= fromHostFifo_din_v;
            else
                fromHostFifo_wr_en <= '0';
                if bytesToDo = 0 then
                    bytesToDo := ChunkLength;
                end if;
                if ThrottleData and (fromHostFifo_almost_full = '0') and (wr_rst_busy = '0') then
                    if pauseCNT < 500 then
                        pauseCNT := pauseCNT + 1;
                    else
                        pauseCNT := 0;
                    end if;
                end if;
                fromHostFifo_wr_en <= '0';
                if (pauseCNT < 4) and (fromHostFifo_almost_full = '0') and (wr_rst_busy = '0') then
                    fromHostFifo_din_v := (others => '0');
                    fromHostFifo_wr_en <= '1';
                    fromHostFifo_din_v(DATA_WIDTH-32+23 downto DATA_WIDTH-32+16) := std_logic_vector(to_unsigned(GBTId, 8));
                    fromHostFifo_din_v(DATA_WIDTH-32+15 downto DATA_WIDTH-32+8) := std_logic_vector(to_unsigned(StreamID, 8));
                    L1ID := std_logic_vector(to_unsigned(chunk,16));
                    chunklengthSLV := std_logic_vector(to_unsigned(ChunkLength-8, 16));
                    if bytesToDo <= (DATA_WIDTH-32)/8 then
                        l := bytesToDo;
                        bytesToDo := 0;
                        fromHostFifo_din_v(DATA_WIDTH-32+7 downto DATA_WIDTH-32) := std_logic_vector(to_unsigned(l, 8)); --Last part of a chunk, fill length field with remaining bytes

                    else
                        l := (DATA_WIDTH-32)/8;
                        fromHostFifo_din_v(DATA_WIDTH-32+7 downto DATA_WIDTH-32) := "11111111"; --More than 26 bytes to do, fill length field with x"3F"
                    end if;
                    byteIndex := (DATA_WIDTH-32)/8-1; --Point to highest byte below 32b header.
                    if byteCNT = 0 then --First 8 bytes have to be formatted
                        fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := x"AA"; --0
                        byteIndex := byteIndex - 1;

                        fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := chunklengthSLV(15 downto 8); --1
                        byteIndex := byteIndex - 1;
                        fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := chunklengthSLV(7 downto 0); --2
                        byteIndex := byteIndex - 1;

                        fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := L1ID(15 downto 8); --3
                        byteIndex := byteIndex - 1;
                        fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := L1ID(7 downto 0); --4
                        byteIndex := byteIndex - 1;

                        fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := x"BB"; --5
                        byteIndex := byteIndex - 1;
                        fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := x"AA"; --6
                        byteIndex := byteIndex - 1;

                        fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := std_logic_vector(to_unsigned(ElinkWidth, 8)); --7
                        byteIndex := byteIndex - 1;
                        for i in 0 to l-9 loop
                            fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := std_logic_vector(to_unsigned(byteCNT, 8)); --8 and on.
                            byteIndex := byteIndex - 1;
                            byteCNT := byteCNT + 1;
                        end loop;
                    else
                        for i in 0 to l-1 loop
                            fromHostFifo_din_v(byteIndex*8+7 downto byteIndex*8) := std_logic_vector(to_unsigned(byteCNT, 8)); --just incremental counter per byte
                            byteIndex := byteIndex - 1;
                            byteCNT := byteCNT + 1;
                        end loop;
                    end if;
                    if bytesToDo <= (DATA_WIDTH-32)/8 then
                        byteCNT := 0;
                        if chunk  < NumberOfChunks-1 then --chunk completed, if we need to send more chunks, increment chunk, otherwise exit the state by setting done.
                            chunk := chunk + 1;
                        else
                            chunk := 0;
                            done <= '1';
                        end if;
                    else
                        bytesToDo := bytesToDo - (DATA_WIDTH-32)/8;
                    end if;
                    fromHostFifo_din <= fromHostFifo_din_v;
                end if; --not almost_full and pauseCnt<4
            end if; --start = '0'/else

        end if; --rising edge wrclk
    end process;

end architecture;

