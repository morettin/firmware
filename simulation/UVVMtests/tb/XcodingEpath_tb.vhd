--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Filiberto Bonini
--!               Nico Giangiacomi
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--  ATLAS
--  FELIX UVVM test automation project
--  F. Schreuder
--  Nikhef
--  January 2020
----------------------------------------------------------------------------------
-- file: DecodingGearBox_tb.vhd.
----------------------------------------------------------------------------------
library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use work.axi_stream_package.all;
    use work.centralRouter_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.interlaken_package.all;
    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

--library uvvm_vvc_framework;
--use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

--use work.vvc_methods_pkg.all;
--use work.td_vvc_framework_common_methods_pkg.all;
--use work.egr_bfm_pkg.all;


-- Test bench entity
entity XcodingEpath_tb is
end XcodingEpath_tb;

architecture tb of XcodingEpath_tb is

    signal clk40        : std_logic:= '0';
    signal clk40_en     : boolean   := false;
    signal clk160 : std_logic;
    signal clk240 : std_logic;
    signal clk250 : std_logic;

    constant clk40_period : time := 25 ns;
    constant clk160_period : time := 6.25 ns;
    constant clk240_period : time := 4.17 ns;
    constant clk250_period : time := 4 ns;
    signal s_axis_aclk_i : std_logic:= '0';
    --signal s_axis_aclk_en     : boolean   := false;
    --constant axiclk_period : time := 10 ns;
    constant GBT_NUM : integer := 1;
    constant FIRMWARE_MODE : integer := FIRMWARE_MODE_GBT;
    constant BLOCKSIZE : integer := 1024;
    constant STREAMS_TOHOST: integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE);
    constant GROUP_CONFIG_FROMHOST : IntArray(0 to 7) := STREAMS_FROMHOST_MODE(FIRMWARE_MODE);
    constant STREAMS_FROMHOST: integer := sum(GROUP_CONFIG_FROMHOST);
    constant IncludeEncodingEpath2_HDLC : std_logic_vector(4 downto 0) := "11111";
    constant IncludeEncodingEpath2_8b10b : std_logic_vector(4 downto 0) := "11111";
    constant IncludeEncodingEpath4_8b10b : std_logic_vector(4 downto 0) := "11111";
    constant IncludeEncodingEpath8_8b10b : std_logic_vector(4 downto 0) := "11111";
    signal aresetn : std_logic;
    signal s_axis : axis_8_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_FROMHOST-1);
    signal s_axis_tready : axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_FROMHOST-1); -- @suppress "signal s_axis_tready is never read"
    signal register_map_control : register_map_control_type;
    signal register_map_encoding_monitor : register_map_encoding_monitor_type; -- @suppress "signal register_map_encoding_monitor is never read"
    signal GBT_DOWNLINK_USER_DATA : txrx120b_type(0 to GBT_NUM-1);
    signal lpGBT_DOWNLINK_USER_DATA : txrx32b_type(0 to GBT_NUM-1); -- @suppress "signal lpGBT_DOWNLINK_USER_DATA is never read"
    signal lpGBT_DOWNLINK_IC_DATA : txrx2b_type(0 to GBT_NUM-1); -- @suppress "signal lpGBT_DOWNLINK_IC_DATA is never read"
    signal lpGBT_DOWNLINK_EC_DATA : txrx2b_type(0 to GBT_NUM-1); -- @suppress "signal lpGBT_DOWNLINK_EC_DATA is never read"
    signal TTCin : TTC_data_type;

    signal RXUSRCLK : std_logic_vector(GBT_NUM-1 downto 0);
    constant FULL_UPLINK_USER_DATA : txrx33b_type(0 to GBT_NUM-1) := (others => (others => '0'));
    signal GBT_UPLINK_USER_DATA : txrx120b_type(0 to GBT_NUM-1);
    constant lpGBT_UPLINK_USER_DATA : txrx224b_type(0 to GBT_NUM-1) := (others => (others => '0'));
    constant lpGBT_UPLINK_EC_DATA : txrx2b_type(0 to GBT_NUM-1) := (others => (others => '0'));
    constant lpGBT_UPLINK_IC_DATA : txrx2b_type(0 to GBT_NUM-1) := (others => (others => '0'));
    constant LinkAligned : std_logic_vector(GBT_NUM-1 downto 0) := (others => '1');
    constant LOCK_PERIOD : integer := 32*5*10;
    constant IncludeDecodingEpath2_HDLC : std_logic_vector(6 downto 0) := "0011111";
    constant IncludeDecodingEpath2_8b10b : std_logic_vector(6 downto 0) := "0011111";
    constant IncludeDecodingEpath4_8b10b : std_logic_vector(6 downto 0) := "0011111";
    constant IncludeDecodingEpath8_8b10b : std_logic_vector(6 downto 0) := "0011111";
    constant IncludeDecodingEpath16_8b10b : std_logic_vector(6 downto 0) := "0011111";
    constant IncludeDecodingEpath32_8b10b : std_logic_vector(6 downto 0) := "0011111";
    signal m_axis : axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); -- @suppress "signal m_axis is never read"
    signal m_axis_tready : axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);
    signal m_axis_prog_empty : axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); -- @suppress "signal m_axis_prog_empty is never read"
    signal register_map_decoding_monitor : register_map_decoding_monitor_type; -- @suppress "signal register_map_decoding_monitor is never read"

    constant TTC_ToHost_Data_in : TTC_ToHost_data_type := (
                                                            BCID => x"000",
                                                            FMT => x"02",
                                                            L0ID => x"0000_0000",
                                                            L1ID => x"00_0000",
                                                            XL1ID => x"00",
                                                            LEN => x"1A",
                                                            orbit => x"0000_0000",
                                                            reserved0 => x"0",
                                                            reserved1 => x"0000",
                                                            trigger_type => x"0000",
                                                            data_rdy => '0',
                                                            TAG => x"00"
                                                          );
    constant ElinkBusyIn : busyOut_array_type(0 to GBT_NUM-1) := (others => (others => '0'));
    constant DmaBusyIn : std_logic := '0';
    constant FifoBusyIn : std_logic := '0';
    constant BusySumIn : std_logic := '0';
    signal decoding_axis_aux : axis_32_array_type(0 to 1); -- @suppress "signal decoding_axis_aux is never read"
    signal decoding_axis_aux_prog_empty : axis_tready_array_type(0 to 1); -- @suppress "signal decoding_axis_aux_prog_empty is never read"
    signal decoding_axis_aux_tready : axis_tready_array_type(0 to 1);



begin

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk40, clk40_en, clk40_period, "40 MHz CLK");
    clock_generator(clk160, clk40_en, clk160_period, "160 MHz CLK");
    clock_generator(clk240, clk40_en, clk240_period, "240 MHz CLK");
    clock_generator(clk250, clk40_en, clk250_period, "250 MHz CLK");

    TTCin <= TTC_zero;
    m_axis_tready <= (others => (others => '1'));
    decoding_axis_aux_tready <= (others => '1');

    GBT_UPLINK_USER_DATA <= GBT_DOWNLINK_USER_DATA;

    register_map_control.DECODING_HGTD_ALTIROC <= "0";

    rm_control_proc: process
    begin
        for link in 0 to GBT_NUM-1 loop
            register_map_control.DECODING_REVERSE_10B <= "1";
            register_map_control.ENCODING_REVERSE_10B <= "1";

            register_map_control.MINI_EGROUP_TOHOST(link).AUX_BIT_SWAPPING <= "0";
            register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING <= "0";
            register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING <= "0";
            register_map_control.MINI_EGROUP_TOHOST(link).AUX_ENABLE <= "0";
            register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE <= "1";
            register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE <= "1";
            register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING <= "0001"; --8b10b

            register_map_control.MINI_EGROUP_FROMHOST(link).AUX_BIT_SWAPPING <= "0";
            register_map_control.MINI_EGROUP_FROMHOST(link).EC_BIT_SWAPPING <= "0";
            register_map_control.MINI_EGROUP_FROMHOST(link).IC_BIT_SWAPPING <= "0";
            register_map_control.MINI_EGROUP_FROMHOST(link).AUX_ENABLE <= "0";
            register_map_control.MINI_EGROUP_FROMHOST(link).EC_ENABLE <= "1";
            register_map_control.MINI_EGROUP_FROMHOST(link).IC_ENABLE <= "1";
            register_map_control.MINI_EGROUP_FROMHOST(link).EC_ENCODING <= "0001"; --8b10b

            register_map_control.DECODING_EGROUP_CTRL(link)(0).EPATH_ENA <= x"FF";
            register_map_control.DECODING_EGROUP_CTRL(link)(1).EPATH_ENA <= x"55";
            register_map_control.DECODING_EGROUP_CTRL(link)(2).EPATH_ENA <= x"11";
            register_map_control.DECODING_EGROUP_CTRL(link)(3).EPATH_ENA <= x"FF";
            register_map_control.DECODING_EGROUP_CTRL(link)(4).EPATH_ENA <= x"FF";
            register_map_control.DECODING_EGROUP_CTRL(link)(0).EPATH_WIDTH <= "000";
            register_map_control.DECODING_EGROUP_CTRL(link)(1).EPATH_WIDTH <= "001";
            register_map_control.DECODING_EGROUP_CTRL(link)(2).EPATH_WIDTH <= "010";
            register_map_control.DECODING_EGROUP_CTRL(link)(3).EPATH_WIDTH <= "000";
            register_map_control.DECODING_EGROUP_CTRL(link)(4).EPATH_WIDTH <= "000";
            for egroup in 0 to 4 loop
                register_map_control.DECODING_EGROUP_CTRL(link)(egroup).PATH_ENCODING <= x"11111111";
                register_map_control.DECODING_EGROUP_CTRL(link)(egroup).REVERSE_ELINKS <= x"00";
            end loop;
            register_map_control.ENCODING_EGROUP_CTRL(link)(0).EPATH_ENA <= x"FF";
            register_map_control.ENCODING_EGROUP_CTRL(link)(1).EPATH_ENA <= x"55";
            register_map_control.ENCODING_EGROUP_CTRL(link)(2).EPATH_ENA <= x"11";
            register_map_control.ENCODING_EGROUP_CTRL(link)(3).EPATH_ENA <= x"FF";
            register_map_control.ENCODING_EGROUP_CTRL(link)(4).EPATH_ENA <= x"FF";
            register_map_control.ENCODING_EGROUP_CTRL(link)(0).EPATH_WIDTH <= "000";
            register_map_control.ENCODING_EGROUP_CTRL(link)(1).EPATH_WIDTH <= "001";
            register_map_control.ENCODING_EGROUP_CTRL(link)(2).EPATH_WIDTH <= "010";
            register_map_control.ENCODING_EGROUP_CTRL(link)(3).EPATH_WIDTH <= "000";
            register_map_control.ENCODING_EGROUP_CTRL(link)(4).EPATH_WIDTH <= "000";
            for egroup in 0 to 4 loop
                register_map_control.ENCODING_EGROUP_CTRL(link)(egroup).PATH_ENCODING <= x"11111111";
                register_map_control.ENCODING_EGROUP_CTRL(link)(egroup).REVERSE_ELINKS <= x"00";
            end loop;

        end loop;
        wait;
    end process;

    reset_proc: process
    begin
        aresetn <= '0';
        wait for clk40_period * 10;
        aresetn <= '1';
        wait;
    end process;

    encoding0: entity work.encoding
        generic map(
            GBT_NUM => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE,
            --BLOCKSIZE => BLOCKSIZE,
            STREAMS_FROMHOST => STREAMS_FROMHOST,
            IncludeEncodingEpath2_HDLC => IncludeEncodingEpath2_HDLC,
            IncludeEncodingEpath2_8b10b => IncludeEncodingEpath2_8b10b,
            IncludeEncodingEpath4_8b10b => IncludeEncodingEpath4_8b10b,
            IncludeEncodingEpath8_8b10b => IncludeEncodingEpath8_8b10b,
            INCLUDE_DIRECT => "11111",
            INCLUDE_TTC => "11111",
            INCLUDE_RD53 => "00000",
            DEBUGGING_RD53 => false,
            RD53Version => "A",
            DISTR_RAM => false,
            SUPPORT_HDLC_DELAY => false,
            USE_ULTRARAM_LCB => false,
            INCLUDE_XOFF => false
        )
        port map(
            clk40 => clk40,
            aclk => s_axis_aclk_i,
            aresetn => aresetn,
            aresetn_ttc_lti => aresetn,
            s_axis => s_axis,
            s_axis_tready => s_axis_tready,
            register_map_control => register_map_control,
            register_map_encoding_monitor => register_map_encoding_monitor,
            GBT_DOWNLINK_USER_DATA => GBT_DOWNLINK_USER_DATA,
            lpGBT_DOWNLINK_USER_DATA => lpGBT_DOWNLINK_USER_DATA,
            lpGBT_DOWNLINK_IC_DATA => lpGBT_DOWNLINK_IC_DATA,
            lpGBT_DOWNLINK_EC_DATA => lpGBT_DOWNLINK_EC_DATA,
            TTCin => TTCin,
            toHostXoff => (others => '0'),
            LTI_TX_Data_Transceiver => open,
            LTI_TX_TX_CharIsK => open,
            LTI_TXUSRCLK_in => (others => clk240)
        );

    decoding0: entity work.decoding
        generic map(
            CARD_TYPE => 712,
            GBT_NUM => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE,
            STREAMS_TOHOST => STREAMS_TOHOST,
            BLOCKSIZE => BLOCKSIZE,
            LOCK_PERIOD => LOCK_PERIOD,
            IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
            IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
            IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
            IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
            IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
            IncludeDecodingEpath32_8b10b => IncludeDecodingEpath32_8b10b,
            IncludeDirectDecoding => (others => '0'),
            RD53Version => "A",
            PCIE_ENDPOINT => 0,
            VERSAL => false,
            AddFULLMODEForDUNE => false
        )
        port map(
            RXUSRCLK => RXUSRCLK,
            FULL_UPLINK_USER_DATA => FULL_UPLINK_USER_DATA,
            GBT_UPLINK_USER_DATA => GBT_UPLINK_USER_DATA,
            lpGBT_UPLINK_USER_DATA => lpGBT_UPLINK_USER_DATA,
            lpGBT_UPLINK_EC_DATA => lpGBT_UPLINK_EC_DATA,
            lpGBT_UPLINK_IC_DATA => lpGBT_UPLINK_IC_DATA,
            LinkAligned => LinkAligned,
            clk160 => clk160,
            clk240 => clk240,
            clk250 => clk250,
            clk40 => clk40,
            clk365 => '0',
            aclk_out => s_axis_aclk_i,
            aresetn => aresetn,
            m_axis => m_axis,
            m_axis_tready => m_axis_tready,
            m_axis_prog_empty => m_axis_prog_empty,
            m_axis_noSC => open,
            m_axis_noSC_tready => (others => (others => '1')),
            m_axis_noSC_prog_empty => open,
            TTC_ToHost_Data_in => TTC_ToHost_Data_in,
            FE_BUSY_out => open,
            ElinkBusyIn => ElinkBusyIn,
            DmaBusyIn => DmaBusyIn,
            FifoBusyIn => FifoBusyIn,
            BusySumIn => BusySumIn,
            m_axis_aux => decoding_axis_aux,
            m_axis_aux_prog_empty => decoding_axis_aux_prog_empty,
            m_axis_aux_tready => decoding_axis_aux_tready,
            register_map_control => register_map_control,
            register_map_decoding_monitor => register_map_decoding_monitor,
            Interlaken_RX_Data_In         => (others => (others => '0')),
            Interlaken_RX_Datavalid       => (others => '0'),
            Interlaken_RX_Gearboxslip     => open,
            Interlaken_Decoder_Aligned_out => open,
            m_axis64                      => open,
            m_axis64_tready               => (others => '0'),
            m_axis64_prog_empty           => open,
            toHost_axis64_aclk_out        => open
        );

    RXUSRCLK <= (others => clk40);


    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;

        procedure write_axis(
            constant link : integer;
            constant stream: integer;
            constant tdata   : in std_logic_vector(7 downto 0);
            constant tvalid : in std_logic;
            constant tlast  : in std_logic) is
        begin
            s_axis(link,stream).tdata <= tdata;
            s_axis(link,stream).tvalid <= tvalid;
            s_axis(link,stream).tlast <= tlast;
        end;



    begin
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        log(ID_LOG_HDR, "Simulation of TB for Encoding Epath", C_SCOPE);

        --enable clock
        clk40_en<= true;

        for link in 0 to GBT_NUM-1 loop
            for stream in 0 to STREAMS_FROMHOST-1 loop
                write_axis(link,stream,x"00", '0', '0');
            end loop;
        end loop;
        wait until rising_edge(s_axis_aclk_i);

        --start simulation
        log(ID_LOG_HDR, "Starting simulation", C_SCOPE);

        log(ID_LOG_HDR, "Stoping simulation", C_SCOPE);


        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        wait for 20 ms;             -- to allow some time for completion
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        std.env.stop;
        wait;  -- to stop completely
    end process p_main;


end tb;
