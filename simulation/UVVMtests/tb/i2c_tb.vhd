--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--========================================================================================================================
-- Copyright (c) 2017 by Bitvis AS.  All rights reserved.
-- You should have received a copy of the license file containing the MIT License (see LICENSE.TXT), if not,
-- contact Bitvis AS <support@bitvis.no>.
--
-- UVVM AND ANY PART THEREOF ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
-- OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH UVVM OR THE USE OR OTHER DEALINGS IN UVVM.
--========================================================================================================================

------------------------------------------------------------------------------------------
-- VHDL unit     : crc20 (FULL mode CRC implementation)
--
-- Description   : This toplevel testbench instantiates a data generator (crc20_datagen.vhd)
-- which creates random data of random length and a CRC20, generated using UVVM functions
-- The data is also going through the FELIX crc module with the FELIX full mode polynomial
-- And the data is verified in this testbench.
------------------------------------------------------------------------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use work.pcie_package.all;

    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;
library bitvis_vip_i2c;
    use bitvis_vip_i2c.i2c_bfm_pkg.all;



-- Test case entity
entity i2c_tb is
    generic(
        use_vunit: boolean := false
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;

-- Test case architecture
architecture func of i2c_tb is
    constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
    -- DSP interface and general control signals
    signal clk25           : std_logic  := '0';
    -- interface
    signal clk, ena, nReset, start, stop, read, write, ack_in, cmd_ack: std_logic;
    signal appreg_clk, RST, rst_soft: std_logic;
    signal Din, Dout: std_logic_vector(7 downto 0);
    -- output

    -- Interrupt related signals
    signal clock_ena     : boolean   := false;

    constant C_CLK_PERIOD_25     : time := 40 ns;

    signal register_map_control: register_map_control_type;
    signal register_map_hk_monitor: register_map_hk_monitor_type;
    signal rden, wren, rnw, WRITE_2BYTES, I2C_WR_RW16BIT: std_logic_vector(0 downto 0);


    constant C_I2C_BFM_CONFIG : t_i2c_bfm_config := (
                                                      enable_10_bits_addressing       => false,
                                                      master_sda_to_scl               => 10 ms,
                                                      master_scl_to_sda               => 10 ms,
                                                      master_stop_condition_hold_time => 1 ms,
                                                      max_wait_scl_change             => 10 ms,
                                                      max_wait_scl_change_severity    => failure,
                                                      max_wait_sda_change             => 10 ms,
                                                      max_wait_sda_change_severity    => failure,
                                                      i2c_bit_time                    => 5 us,
                                                      i2c_bit_time_severity           => failure,
                                                      acknowledge_severity            => failure,
                                                      slave_mode_address              => "0001000100",
                                                      slave_mode_address_severity     => failure,
                                                      slave_rw_bit_severity           => failure,
                                                      reserved_address_severity       => warning,
                                                      match_strictness                => MATCH_EXACT,
                                                      id_for_bfm                      => ID_BFM,
                                                      id_for_bfm_wait                 => ID_BFM_WAIT,
                                                      id_for_bfm_poll                 => ID_BFM_POLL
                                                    );
    signal i2c_if : t_i2c_if;
    signal SDAo, SCLo : std_logic;

begin
    appreg_clk <= clk25;

    i2c_if.scl <= '0' when SCLo = '0' else 'H';
    i2c_if.sda <= '0' when SDAo = '0' else 'H';

    i2c0: entity work.simple_i2c
        port map(
            clk     => clk,
            ena     => ena,
            nReset  => nReset,
            clk_cnt => "01100100",
            start   => start,
            stop    => stop,
            read    => read,
            write   => write,
            ack_in  => ack_in,
            Din     => Din,
            cmd_ack => cmd_ack,
            ack_out => open, --ack_out,
            Dout    => Dout,
            SCL_i    => i2c_if.SCL,
            SDA_i    => i2c_if.SDA,
            SCL_o    => SCLo,
            SDA_o    => SDAo
        );


    i2cint0: entity work.i2c_interface
        port map(
            Din                  => Din,
            Dout                 => Dout,
            I2C_RD               => register_map_hk_monitor.I2C_RD,
            I2C_WR               => register_map_hk_monitor.I2C_WR,
            RST                  => RST,
            ack_in               => ack_in,
            --ack_out              => ack_out,
            appreg_clk           => appreg_clk,
            clk                  => clk,
            cmd_ack              => cmd_ack,
            ena                  => ena,
            nReset               => nReset,
            read                 => read,
            register_map_control => register_map_control,
            rst_soft             => rst_soft,
            start                => start,
            stop                 => stop,
            write                => write);


    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clock_generator(clk25, clock_ena, C_CLK_PERIOD_25, "25 TB clock");

    reset_proc: process
    begin
        RST <= '1';
        rst_soft <= '1';
        wait for C_CLK_PERIOD_25 * 5;
        RST <= '0';
        rst_soft <= '0';
        wait;
    end process;

    register_map_control.I2C_WR.DATA_BYTE1 <= x"11";
    register_map_control.I2C_WR.DATA_BYTE2 <= x"55";
    register_map_control.I2C_WR.DATA_BYTE3 <= x"99";
    register_map_control.I2C_WR.I2C_WREN <= wren;
    register_map_control.I2C_WR.READ_NOT_WRITE <= rnw;
    register_map_control.I2C_WR.SLAVE_ADDRESS <= "1000100";
    register_map_control.I2C_WR.WRITE_2BYTES <= WRITE_2BYTES;
    register_map_control.I2C_WR.RW16BIT <= I2C_WR_RW16BIT;
    register_map_control.I2C_RD.I2C_RDEN <= rden;

    ------------------------------------------------
    -- PROCESS: p_main
    ------------------------------------------------
    p_main: process
        constant C_SCOPE     : string  := C_TB_SCOPE_DEFAULT;
    begin
        wren <= "0";
        rden <= "0";
        rnw <= "0"; --Write
        WRITE_2BYTES <= "1";
        I2C_WR_RW16BIT <= "0";
        -- Print the configuration to the log
        report_global_ctrl(VOID);
        report_msg_id_panel(VOID);
        enable_log_msg(ALL_MESSAGES);
        --disable_log_msg(ALL_MESSAGES);
        --enable_log_msg(ID_LOG_HDR);
        clock_ena <= true;



        log(ID_LOG_HDR, "Simulation of TB for I2C", C_SCOPE);
        wait for 10 ps;
        wait for C_CLK_PERIOD_25 * 10;
        wren <= "1";
        wait for C_CLK_PERIOD_25;
        wren <= "0";
        wait for 1 ms;
        WRITE_2BYTES <= "0";
        rnw <= "1"; --Read
        wait for 10 ps;
        wren <= "1";
        wait for C_CLK_PERIOD_25;
        wren <= "0";
        await_value(register_map_hk_monitor.I2C_RD.I2C_EMPTY, "0", 0 ms, 1 ms, ERROR, C_SCOPE);


        rden <= "1";
        wait for C_CLK_PERIOD_25;
        rden <= "0";
        wait for C_CLK_PERIOD_25;
        check_value(register_map_hk_monitor.I2C_RD.I2C_DOUT, x"00", ERROR, "Comparing received data", C_SCOPE);
        wait for 10 ms;
        --==================================================================================================
        -- Ending the simulation
        --------------------------------------------------------------------------------------
        report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        if use_vunit = false then
            std.env.stop;
        end if;
        uvvm_completed <= '1';
        wait;  -- to stop completely

    end process p_main;

    slave_proc: process
        variable v_data_out: t_byte_array(0 to 1);
        constant c_data_to_receive: t_byte_array(0 to 1) := (x"11", x"55");
        constant c_nulldata: t_byte_array(0 to -1) := (others => x"00"); -- @suppress "Null range: The left argument is strictly larger than the right"
    begin
        i2c_if <= init_i2c_if_signals(VOID);
        i2c_slave_receive(v_data_out, "Receive from master", i2c_if, C_SCOPE, shared_msg_id_panel,C_I2C_BFM_CONFIG);
        check_value(v_data_out, c_data_to_receive, ERROR, "Checking received data on slave", C_SCOPE);
        i2c_slave_check (c_nulldata, "Receiving slave address", i2c_if, '1', ERROR, C_SCOPE, shared_msg_id_panel, C_I2C_BFM_CONFIG);
        i2c_slave_transmit(x"00", "Transmitting data to master", i2c_if, C_SCOPE, shared_msg_id_panel, C_I2C_BFM_CONFIG);

        wait;
    end process;

end func;
