library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.STD_LOGIC_UNSIGNED.ALL;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.axi_stream_package.all;
    use work.centralRouter_package.all;
    use ieee.std_logic_textio.all;
    use std.textio.all;

    use std.env.all;

    use work.package_pixel.all;


-- Test bench entity
entity Decoding_pixel_tb is
    generic(
        BLOCKSIZE                       : integer := 1024;
        CHUNK_TRAILER_32B               : boolean := false;
        toHostTimeoutBitn               : integer := 16
    );

end Decoding_pixel_tb;

architecture tb of Decoding_pixel_tb is
    --constant fnameW : string := "RD53A_refout_CB.txt";
    --copy both files to Projects/FLX712_FELIX/FLX712_FELIX.sim/sim_1/behav/xsim
    constant fname                        : string                                         := "RD53A_refout.txt";
    constant fnameCB                        : string                                         := "RD53A_refout_CB.txt";
    constant STREAMS_TOHOST               : integer                                        := 30;
    constant clk40_period                 : time                                           := 25 ns;
    constant aclk_period                  : time                                           := 6.25 ns;

    signal PCIE_ENDPOINT                  : integer                                        := 0;
    signal LINK                           : integer                                        :=0;

    file fptr, fptrCB : text;
    --    file fptr_write : text;

    signal data32_ref                     : std_logic_vector(31 downto 0);
    signal data32_ref_dly                 : std_logic_vector(31 downto 0);
    signal data32_ref_dlydly              : std_logic_vector(31 downto 0);
    signal isErr                          : std_logic := '0';

    signal register_map_control           : register_map_control_type;

    signal clk40                          : std_logic:= '0';
    signal clk40_tmp                      : std_logic:= '0';
    signal clk40_en                       : boolean   := false;
    signal aclk                           : std_logic:= '0';
    signal aclk_tmp                       : std_logic:= '0';
    signal aclk_en                        : boolean   := false;
    signal reset_dopulse : std_logic := '0';
    signal dopulse : std_logic := '1';
    signal reset                          : std_logic_vector(4 downto 0)                    := (others => '0');

    signal DecoderAligned                 : std_logic_vector(STREAMS_TOHOST-3 downto 0);
    signal m_axis                         : axis_32_array_type(0 to STREAMS_TOHOST-1);
    signal m_axis_tready                  : axis_tready_array_type(0 to STREAMS_TOHOST-1);
    signal m_axis_prog_empty              : axis_tready_array_type(0 to STREAMS_TOHOST-1);

    signal trigid                         : std_logic_vector(4 downto 0)                    := (others => '0');
    signal trigtag                        : std_logic_vector(4 downto 0)                    := (others => '0');
    signal bcid                           : std_logic_vector(14 downto 0)                   := (others => '0');
    signal isHDR                          : std_logic                                       := '0';
    signal col                            : std_logic_vector(5 downto 0)                    := (others => '0');
    signal row                            : std_logic_vector(8 downto 0)                    := (others => '0');
    signal onebside                       : std_logic := '0';
    signal tot                            : array_4x4b                                      := (others => (others => '0'));

    signal CBOPT                          : std_logic_vector(3 downto 0); --to be included in register map
    signal DISLANE                        : std_logic_vector(6 downto 0);
    signal mask_k_char                    : std_logic_vector(3 downto 0);


    signal emuToHost_lpGBTdata            : std_logic_vector(223 downto 0);
    signal emuToHost_lpGBTECdata          : std_logic_vector(1 downto 0);
    signal emuToHost_lpGBTICdata          : std_logic_vector(1 downto 0);
    signal emuToHost_GBTlinkValid         : std_logic;

    signal m_axis0_dly                    : axis_32_type;
    signal m_axis0_dly_dly                : axis_32_type;
    signal countHDR                       : std_logic_vector(3 downto 0)                    := x"0";

    signal tvalid_doublerate : std_logic;
    signal start_simuCB0 : boolean := false;
    signal start_simuCB3 : boolean := false;
    signal done_simuCB0  : boolean := false;
    signal done_simuCB3  : boolean := false;
    signal timeout       : boolean := false;
begin
    -----------------------------------------------------------------------------
    -- Registers
    -----------------------------------------------------------------------------
    --internal emulator
    register_map_control.GBT_TOHOST_FANOUT.SEL(0 downto 0) <= "1";
    register_map_control.FE_EMU_ENA.EMU_TOHOST(0) <= '1';
    register_map_control.FE_EMU_CONFIG.WRADDR <= (others => '0');
    --
    register_map_control.DECODING_DISEGROUP <= "1111000"; --disable all lane
    --but 0,1,2
    register_map_control.DECODING_MASK64B66BKBLOCK <= x"a";

    --egroup0: link 0 (hit data), 1 (DCS)
    --egroup>0 disabled
    register_map_control.DECODING_EGROUP_CTRL(0)(0).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(0).EPATH_ENA <= x"03";
    register_map_control.DECODING_EGROUP_CTRL(0)(1).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(1).EPATH_ENA <= x"03";
    register_map_control.DECODING_EGROUP_CTRL(0)(2).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(2).EPATH_ENA <= x"03";
    --
    m_axis_tready     <= (others => '1');

    -----------------------------------------------------------------------------
    -- Clock Generator
    -----------------------------------------------------------------------------
    clk40_tmp       <= not clk40_tmp after clk40_period/2; --12.5ns;
    clk40 <= clk40_tmp when clk40_en = true else '0';
    aclk_tmp <= not aclk_tmp after aclk_period/2; --3.125ns;
    aclk <= aclk_tmp when aclk_en = true else '0';

    -----------------------------------------------------------------------------
    -- Resets
    -----------------------------------------------------------------------------
    rst_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            if (reset_dopulse = '1') then
                reset <= (others => '0');
                dopulse <= '1';
            elsif(dopulse = '1' and reset(0) = '0') then
                reset(0) <= '1';
                dopulse <= '1';
            elsif(dopulse = '1' and reset(0) = '1') then
                reset(0) <= '0';
                dopulse <= '0';
            else
                reset(0) <= '0';
            end if;
            reset(1) <= reset(0);
            reset(2) <= reset(1);
            reset(3) <= reset(2);
            reset(4) <= reset(3);
        end if;
    end process;

    -----------------------------------------------------------------------------
    -- Emulator
    -----------------------------------------------------------------------------
    gbtEmuToHost0: entity work.GBTdataEmulator
        generic map(
            EMU_DIRECTION      => "ToHost",
            FIRMWARE_MODE      => FIRMWARE_MODE_PIXEL
        )
        port map(
            clk40                => clk40,
            wrclk                => clk40,
            rst_hw               => reset(1),
            rst_soft             => reset(1),
            xoff                 => '0',
            register_map_control => register_map_control,
            GBTdata              => open,
            lpGBTdataToFE        => open,
            lpGBTdataToHost      => emuToHost_lpGBTdata,
            lpGBTECdata          => emuToHost_lpGBTECdata,
            lpGBTICdata          => emuToHost_lpGBTICdata,
            GBTlinkValid         => emuToHost_GBTlinkValid);

    -----------------------------------------------------------------------------
    -- UUT
    -----------------------------------------------------------------------------
    DecodingPixelLinkLPGBT_uut: entity work.DecodingPixelLinkLPGBT
        generic map(
            CARD_TYPE      => 712,
            RD53Version    => "A",
            BLOCKSIZE      => 1024,
            LINK           => LINK,
            SIMU           => 1,
            STREAMS_TOHOST => STREAMS_TOHOST,
            PCIE_ENDPOINT  => 0,
            VERSAL         => false
        )
        port map(
            clk40                => clk40,
            reset                => reset(4),
            MsbFirst             => '1',

            LinkData             => emuToHost_lpGBTdata,
            LinkAligned          => emuToHost_GBTlinkValid,

            m_axis               => m_axis(0 to STREAMS_TOHOST-3),
            m_axis_tready        => m_axis_tready(0 to STREAMS_TOHOST-3),
            m_axis_aclk          => aclk,
            m_axis_prog_empty    => m_axis_prog_empty(0 to STREAMS_TOHOST-3),
            register_map_control => register_map_control,
            DecoderAligned_out   => open,
            DecoderDeskewed_out  => open,
            cnt_rx_64b66bhdr_out => open,
            rx_soft_err_cnt_out  => open


        );

    -----------------------------------------------------------------------------
    -- Data Checker
    -----------------------------------------------------------------------------
    interpretdata_proc: process(aclk)
        variable line_out, file_line  : line;
        variable good : boolean;
    begin
        if rising_edge(aclk) then
            if reset(1) = '1' then
                countHDR  <= (others => '0');
                isErr     <= '0';
            else
                data32_ref_dlydly <= data32_ref_dly;
                data32_ref_dly    <= data32_ref;
                m_axis0_dly_dly   <= m_axis0_dly;
                m_axis0_dly       <= m_axis(0);

                if(m_axis(0).tdata /= data32_ref and m_axis(0).tvalid = '1') then
                    isErr <= '1';
                end if;

                if(m_axis(0).tvalid = '1') then
                    --              hwrite(line_out,m_axis(0).tdata,RIGHT,9);
                    --              writeline(fptr_write,line_out);

                    if(m_axis(0).tdata(31 downto 25) = "0000001") then
                        countHDR  <= countHDR + x"1";
                        isHDR     <= '1';
                        trigid    <= m_axis(0).tdata(24 downto 20);
                        trigtag   <= m_axis(0).tdata(19 downto 15);
                        bcid      <= m_axis(0).tdata(14 downto 0);
                        col       <= (others => '0');
                        row       <= (others => '0');
                        onebside  <= '0';
                        tot       <= (others => (others => '0'));
                    else
                        isHDR     <= '0';
                        trigid    <= (others => '0');
                        trigtag   <= (others => '0');
                        bcid      <= (others => '0');
                        col       <= m_axis(0).tdata(31 downto 26);
                        row       <= m_axis(0).tdata(25 downto 17);
                        onebside  <= m_axis(0).tdata(16);
                        tot(0)    <= m_axis(0).tdata(15 downto 12);
                        tot(1)    <= m_axis(0).tdata(11 downto  8);
                        tot(2)    <= m_axis(0).tdata( 7 downto  4);
                        tot(3)    <= m_axis(0).tdata( 3 downto  0);
                    end if;
                else
                    isHDR       <= '0';
                    trigid      <= (others => '0');
                    trigtag     <= (others => '0');
                    bcid        <= (others => '0');
                    col         <= (others => '0');
                    row         <= (others => '0');
                    onebside    <= '0';
                    tot         <= (others => (others => '0'));
                end if;
            end if;
        end if;
    end process;


    tvalid_doublerate_proc: process(aclk)
    begin
        if aclk'event and aclk = '1' then
            tvalid_doublerate <= m_axis(0).tvalid;
        elsif aclk'event and aclk = '0' then
            tvalid_doublerate <= '0';
        end if;
    end process;

    -----------------------------------------------------------------------------
    -- Main Processes
    -----------------------------------------------------------------------------
    mainproc : process
        file fptr           : text open READ_MODE is fname;
        file fptrCB           : text open READ_MODE is fnameCB;

        variable file_line  : line;
        variable good       : boolean;
        variable data32     : std_logic_vector(31 downto 0);
        variable count_data                     : integer                                         := 0;


        variable count_dataCB0                     : integer                                         := 0;
        variable countHDRCB0                     : std_logic_vector(3 downto 0)                                         := x"0";
        variable isERRCB0                     : std_logic := '0';
        variable count_dataCB3                     : integer                                         := 0;
        variable countHDRCB3                     : std_logic_vector(3 downto 0)                                         := x"0";
        variable isERRCB3                     : std_logic := '0';

        procedure printMT (arg: in string := "") is
            variable lineMT : line;
        begin
            std.textio.write(lineMT, arg);
            std.textio.writeline(std.textio.output, lineMT);
        end;

    begin
        assert false
            report "START SIMULATION"
            severity NOTE;

        clk40_en<= true;
        aclk_en<= true;

        ------------START SIMULATING CB=0 scenario-------------
        start_simuCB0 <= true;

        readline(fptr, file_line); --skip first line
        --        file_open(fptr_write, fnameW,write_mode);        --

        register_map_control.DECODING_LINK_CB(0).CBOPT <= x"0";
        --        dopulse <= '1'; --start reset chain


        while (not endfile(fptr)) loop
            if timeout then
                exit;
            end if;
            readline(fptr, file_line);
            hread(file_line,data32,good);
            data32_ref <= data32;
            count_data := count_data + 1;
            wait until tvalid_doublerate = '1' or timeout;
        end loop;

        wait for 100 ns;

        --    file_close(fptr);

        -- Finish the simulation
        file_close(fptr);
        --        file_close(fptr_write);

        isErrCB0 := isErr;
        count_dataCB0 := count_data;
        countHDRCB0 := countHDR;
        done_simuCB0 <= true;

        --std.env.stop;
        --wait;  -- to stop completely
        --finish;

        wait for 1000 ns;
        ------------DONE SIMULATING CB=0 scenario-------------
        ------------START SIMULATING CB=3 scenario-------------
        start_simuCB3 <= true;
        readline(fptrCB, file_line); --skip first line
        register_map_control.DECODING_LINK_CB(0).CBOPT <= x"3";
        count_data := 0;
        reset_dopulse <= '1'; --start reset chain
        wait for 100 ns;
        reset_dopulse <= '0';

        while (not endfile(fptrCB)) loop
            if timeout then
                exit;
            end if;
            readline(fptrCB, file_line);
            hread(file_line,data32,good);
            data32_ref <= data32;
            count_data := count_data + 1;
            wait until tvalid_doublerate = '1' or timeout;
        end loop;

        wait for 100 ns;

        -- Finish the simulation
        file_close(fptrCB);

        isErrCB3 := isErr;
        count_dataCB3 := count_data;
        countHDRCB3 := countHDR;
        done_simuCB3 <= true;
        wait for 100 ns;

        printMT("***************");
        printMT("*****CB=0******");
        printMT("***************");
        if isErrCB0 = '0' and countHDRCB0 = x"2" then
            printMT("***Simulation passed, Checked #data = " & integer'image(count_dataCB0));
        else
            printMT("***Simulation failed, Checked #data = " & integer'image(count_dataCB0));
        end if;
        printMT("***************");
        printMT("***************");
        printMT("***************");

        printMT("***************");
        printMT("*****CB=3******");
        printMT("***************");
        if isErrCB3 = '0' and countHDRCB3 = x"2" then
            printMT("***Simulation passed, Checked #data = " & integer'image(count_dataCB3));
        else
            printMT("***Simulation failed, Checked #data = " & integer'image(count_dataCB3));
        end if;
        printMT("***************");
        printMT("***************");
        printMT("***************");


        std.env.stop;
        wait;  -- to stop completely
        finish;

    end process;

    -----------------------------------------------------------------------------
    -- Timeout
    -----------------------------------------------------------------------------
    timeoutproc : process
    begin
        wait for 1000 us;
        timeout <= true;
    end process;


end tb;
