--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench simulation for lcb_scheduler_encoder
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_lcb_scheduler_encoder.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Wed Oct 30 10:06:40 2019
-- Last update : Thu Aug 12 17:30:38 2021
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 User Company Name
-------------------------------------------------------------------------------
-- Description:
-- 1. Reset, verify that IDLE frames dequeue
-- 2. Enqueue nothing, verify that IDLE frames come out
-- 3. Verify that valid TTC L0A frames produce IDLE when ttc_l0a_en='0'
-- 4. Verify that valid TTC L0A frames come out when ttc_l0a_en='1'
-- 5. Verify that invalid TTC L0A frames result in IDLE frames
-- 6. Verify that L0A frames are prioritized over LCB frames
-- 7. Verify that L0A frames are prioritized over bypass frames
-- 8. Verify that bypass frames are prioritized over LCB frames
-- 9. Check that when trickle_bc_gating_en='0' LCB frames come out
--    independently of trickle_bc_gating_i value
-- 10. Check that when trickle_bc_gating_en='1' and trickle_allow_register_cmd_i='1'
--     LCB and bypass frames come out only if trickle_bc_gating_i='1'
-- 11. Check that when trickle_bc_gating_en='1' and trickle_bc_gating_i='1',
--     non-register LCB frames come out when trickle_allow_register_cmd_i='0'
-- 12. Check that when trickle_bc_gating_en='1' and trickle_bc_gating_i='1',
--     register LCB frames are allowed to finish when trickle_allow_register_cmd_i='0'
-- 13. Check that when trickle_bc_gating_en='1' and trickle_bc_gating_i='1',
--     register LCB frames are not scheduled when trickle_allow_register_cmd_i='0'
-- 14. Check that all possible L0A frames are encoded correctly (decode to the original)
-- 15. Check that all possible LCB frames are encoded correctly (decode to the original)
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use work.strips_package.all;

Library xpm;
    use xpm.vcomponents.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_lcb_scheduler_encoder is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_lcb_scheduler_encoder(behavioral)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_lcb_scheduler_encoder;

architecture behavioral of tb_lcb_scheduler_encoder is
    constant K0         : std_logic_vector(7 downto 0)  := x"78";
    constant K1         : std_logic_vector(7 downto 0)  := x"55";
    constant K2         : std_logic_vector(7 downto 0)  := x"47";
    constant K3         : std_logic_vector(7 downto 0)  := x"6A";
    constant idle_frame : std_logic_vector(15 downto 0) := K0 & K1;

    constant C_CLK_PERIOD : time                          := 25 ns; -- 40 MHz BC clk
    constant C_CYCLES_RST : integer                       := 3;
    signal clk            : std_logic                     := '0';
    signal clk_en         : boolean                       := false;
    constant null_l0a     : std_logic_vector(11 downto 0) := "000000000000";

    signal rst                          : std_logic                     := '0';
    signal lcb_frame_wr_en              : std_logic                     := '0';
    signal lcb_frame_rd_en              : std_logic                     := '0';
    signal frame_start_pulse_i          : std_logic                     := '0';
    signal trickle_bc_gating_i          : std_logic                     := '0';
    signal trickle_allow_register_cmd_i : std_logic                     := '0';
    signal trickle_bc_gating_en         : std_logic                     := '0';
    signal ttc_l0a_en                   : std_logic                     := '0';
    signal l0a_frame                    : std_logic_vector(11 downto 0) := (others => '0');
    signal lcb_frame                    : std_logic_vector(12 downto 0);
    signal lcb_frame_din                : std_logic_vector(12 downto 0) := (others => '0');
    signal l0a_frame_delay_i            : std_logic_vector(1 downto 0)  := (others => '0');
    signal lcb_frame_full               : std_logic;
    signal lcb_frame_almost_full        : std_logic; -- @suppress "signal lcb_frame_almost_full is never read"
    signal lcb_frame_wr_rst_busy        : std_logic;
    signal lcb_frame_empty              : std_logic;
    signal lcb_frame_almost_empty       : std_logic;
    signal lcb_frame_rd_rst_busy        : std_logic;
    signal encoded_frame                : std_logic_vector(15 downto 0);
    signal lcb_rd_ready, lcb_wr_ready   : std_logic;
    signal lcb_wr_ctr, lcb_rd_ctr       : integer                       := 0;
    signal bypass_frame                 : std_logic_vector(15 downto 0) := (others => '0'); -- bypass FromHost frame (already 6b8b encoded)
    signal bypass_frame_valid           : std_logic                     := '0'; -- bypass frame is valid this clk cycle
    signal bypass_frame_ready           : std_logic; -- request next bypass frame

begin

    test : process is
        --------------------------------------------------------------------------------
        -- Some macro declarations; keep scrolling to the actual tests
        --------------------------------------------------------------------------------

        --variable delay : time := 0 ns;

        -- places an unencoded LCB frame into the LCB FIFO
        procedure enqueue_lcb_frame(
            constant data : std_logic_vector(12 downto 0)
        ) is
        begin
            await_value(lcb_wr_ready, '1', 0 ns, 10 * C_CLK_PERIOD,
                        ERROR, "LCB FIFO is ready (wr side)");
            lcb_frame_wr_en <= '1';
            lcb_frame_din   <= data;
            wait until rising_edge(clk);
            lcb_frame_wr_en <= '0';
        end procedure enqueue_lcb_frame;

        -- dequeue a scheduler frame without verifying it
        procedure dequeue_frame is
        begin
            frame_start_pulse_i <= '1';
            wait until rising_edge(clk);
            frame_start_pulse_i <= '0';
            wait until rising_edge(clk);
            wait until rising_edge(clk);
        end;

        -- appends words "received idle" to message if encoded frame is IDLE
        function build_msg(
            encoded_frame : std_logic_vector(15 downto 0);
            msg           : string
        ) return string is
        begin
            if encoded_frame = idle_frame then
                return msg & " (received idle)";
            else
                return msg;
            end if;
        end;

        -- verify that the correct frame is scheduled (compare to the provided encoded frame)
        procedure verify_frame_encoded( -- @suppress "Unused declaration"
            constant expected : std_logic_vector(15 downto 0);
            constant msg      : string := "Encoded frame mismatch"
        ) is
        begin
            frame_start_pulse_i <= '1';
            wait until rising_edge(clk);
            frame_start_pulse_i <= '0';
            check_value(encoded_frame, expected, ERROR, build_msg(encoded_frame, msg));
        end;

        -- verify that correct bypass frame is scheduled (compare to the provided encoded frame)
        procedure verify_bypass_frame(
            constant expected : std_logic_vector(15 downto 0)
        ) is
        begin
            frame_start_pulse_i <= '1';
            wait until rising_edge(clk);
            frame_start_pulse_i <= '0';
            wait until rising_edge(clk);
            check_value(bypass_frame_ready, '1', ERROR, "Bypass frame is not acknowledged");
            wait until rising_edge(clk);
            check_value(encoded_frame, expected, ERROR, "Incorrect bypass frame");
        end;

        -- verify that the correct frame is scheduled (compare to the provided non-encoded frame)
        procedure verify_frame(
            constant expected : std_logic_vector(12 downto 0);
            constant msg      : string := "Encoded frame mismatch" -- @suppress "Unused declaration"
        ) is
        begin
            frame_start_pulse_i <= '1';
            wait until rising_edge(clk);
            frame_start_pulse_i <= '0';
            wait until rising_edge(clk);
            wait until rising_edge(clk);

            -- decode the frame and compare it to the original
            if (encoded_frame = idle_frame) and (expected(12 downto 6) /= "1000010") then
                error("Received IDLE instead of the original frame");
            else
                if encoded_frame(15 downto 8) = K2 then
                    if expected(12 downto 6) /= "1000000" then
                        error("Received K2, but the original frame doesn't have it");
                    end if;
                elsif encoded_frame(15 downto 8) = K3 then
                    if expected(12 downto 6) /= "1000001" then
                        error("Received K3, but the original frame doesn't have it");
                    end if;
                elsif (expected(12 downto 6) = "1000010") then
                    if (encoded_frame /= idle_frame) then
                        error("Expected IDLE frame, received " & to_string(encoded_frame));
                    else
                        return;
                    end if;
                elsif decode_6b8b(encoded_frame(15 downto 8)) /= expected(11 downto 6) then
                    error("Decoded frame doesn't match the original (upper byte, expected =" & to_string(expected(11 downto 6), HEX) & " actual = " & to_string(decode_6b8b(encoded_frame(15 downto 8))) & ")");
                end if;

                if decode_6b8b(encoded_frame(7 downto 0)) /= expected(5 downto 0) then
                    error("Decoded frame doesn't match the original (lower byte, expected =" & to_string(expected(5 downto 0)) & " actual = " & to_string(decode_6b8b(encoded_frame(7 downto 0))) & ")");
                end if;
            end if;
        end;

        -- verify that the number of enqueued and dequeued LCB frames match
        procedure verify_lcb_count is
        begin
            wait until rising_edge(clk);
            check_value(lcb_wr_ctr, lcb_rd_ctr, ERROR, "LCB FIFO - numer of enqueued and dequeued frames match");
        end;

        -- verify that expected LCB frame is received
        procedure verify_lcb_frame(
            constant frame : std_logic_vector(12 downto 0)
        ) is
        begin
            await_value(lcb_rd_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "LCB FIFO is ready (rd side)");
            verify_frame(frame, "LCB frame mismatch");
        end;

        -- enqueue provided LCB frame and verify that the same LCB frame is scheduled
        procedure enqueue_and_verify_lcb_frame(
            constant frame : std_logic_vector(12 downto 0)
        ) is
        begin
            enqueue_lcb_frame(frame);
            verify_lcb_frame(frame);
            verify_lcb_count;
        end;

        -- set bypass frame and verify it's scheduled
        procedure set_and_verify_bypass_frame(
            constant frame : std_logic_vector(15 downto 0)
        ) is
        begin
            bypass_frame_valid <= '1';
            bypass_frame       <= frame;
            wait until rising_edge(clk);
            verify_bypass_frame(frame);
            bypass_frame_valid <= '0';
        end;

        -- check that IDLE frame is scheduled
        procedure verify_idle(
            constant msg : string := "Scheduled frame is different from IDLE"
        ) is
        begin
            frame_start_pulse_i <= '1';
            wait until rising_edge(clk);
            frame_start_pulse_i <= '0';
            wait until rising_edge(clk);
            wait until rising_edge(clk);
            check_value(encoded_frame, idle_frame, ERROR, msg);
        end;

        -- set TTC L0A frame invalid and check that an IDLE frame is scheduled
        procedure enqueue_and_verify_invalid_l0a is -- @suppress "Unused declaration"
        begin
            l0a_frame <= (others => '0');
            verify_idle;
        end;

        -- sets TTC L0A frame to the provided value
        procedure set_l0a_frame(
            constant frame : std_logic_vector(11 downto 0)
        ) is
        begin
            l0a_frame <= frame;
        --wait until rising_edge(clk);
        end;

        -- verify that the expected L0A frame is scheduled
        procedure verify_l0a_frame(
            constant expected : std_logic_vector(11 downto 0)
        ) is
        begin
            verify_frame("0" & expected, "L0A frame mismatch");
        end;

        -- sets TTC L0A frame to the provided value and verify that it's scheduled
        procedure set_and_verify_l0a_frame(
            constant frame : std_logic_vector(11 downto 0)
        ) is
        begin
            set_l0a_frame(frame);
            verify_l0a_frame(frame);
        end;

        -- set RST to a given value and wait C_CYCLES_RST cycles
        procedure set_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
        end;

        -- variables for storing intermediate and random values
        variable l0a_frame_var   : std_logic_vector(11 downto 0);
        variable lcb_frame_var   : std_logic_vector(12 downto 0);
        variable l0a_frame_valid : std_logic;
        variable frame_range     : integer;

    begin
        --------------------------------------------------------------------------------
        -- Actual tests begin here
        --------------------------------------------------------------------------------

        clk_en <= true;
        disable_log_msg(ID_POS_ACK);

        ----------------------------------
        log(ID_LOG_HDR, "Checking that IDLE frames are scheduled when rst = 1");
        set_rst('1');
        for i in 0 to 9 loop
            verify_idle;
        end loop;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that IDLE frames are scheduled when TTC L0A, LCB, and bypass frames are unavailable");
        set_rst('0');
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '0';
        ttc_l0a_en           <= '1';
        bypass_frame_valid   <= '0';
        await_value(lcb_wr_ready, '1', 0 ns, 10 * C_CLK_PERIOD,
                    ERROR, "LCB FIFO is ready (wr side)");
        for i in 0 to 9 loop
            verify_idle;
        end loop;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that LCB frames are scheduled when available");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '0';
        bypass_frame_valid   <= '0';
        set_l0a_frame(null_l0a);
        enqueue_and_verify_lcb_frame("0111100110011");
        enqueue_and_verify_lcb_frame("0001110110110");
        enqueue_and_verify_lcb_frame("1000001010100");

        ----------------------------------

        log(ID_LOG_HDR, "Verify that bypass frames are scheduled when available");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '0';
        set_l0a_frame(null_l0a);
        set_and_verify_bypass_frame(x"1357");
        set_and_verify_bypass_frame(x"CAF9");
        set_and_verify_bypass_frame(x"5417");
        set_and_verify_bypass_frame(x"8721");

        ----------------------------------

        log(ID_LOG_HDR, "Verify that TTC L0A frames are scheduled when ttc_l0a_en='1'");
        ttc_l0a_en         <= '1';
        bypass_frame_valid <= '0';
        set_and_verify_l0a_frame("011011000101");
        set_and_verify_l0a_frame("111111111111");
        set_and_verify_l0a_frame("111111000000");
        set_and_verify_l0a_frame("100001010110");
        set_and_verify_l0a_frame("010000001010");
        set_and_verify_l0a_frame("001000011110");
        set_and_verify_l0a_frame("000100000000");
        set_and_verify_l0a_frame("000101010101");
        set_and_verify_l0a_frame("000010010101");
        set_l0a_frame(null_l0a);

        ----------------------------------

        log(ID_LOG_HDR, "Verify that TTC L0A frames are delayed according to the l0a_frame_delay_i input");
        ttc_l0a_en         <= '1';
        bypass_frame_valid <= '0';
        for i in 1 to 3 loop
            l0a_frame_delay_i <= std_logic_vector(to_unsigned(i, 2));
            l0a_frame_var     := '1' & random(l0a_frame_var'length - 1);
            wait until rising_edge(clk);

            -- flush out the delayed frame buffer, fill it with IDLE frames
            for j in 1 to 4 loop
                set_l0a_frame(null_l0a);
                dequeue_frame;
            end loop;

            -- set a valid L0A frame
            set_l0a_frame(l0a_frame_var);
            verify_idle;
            set_l0a_frame(null_l0a);

            for j in 2 to 4 loop
                if j <= i then
                    verify_idle;
                elsif j = i + 1 then
                    verify_l0a_frame(l0a_frame_var);
                else
                    verify_idle;
                end if;
            end loop;
        end loop;

        -- clean up
        set_l0a_frame(null_l0a);
        l0a_frame_delay_i <= std_logic_vector(to_unsigned(0, 2));
        wait until rising_edge(clk);

        ----------------------------------
        log(ID_LOG_HDR, "Verify that TTC L0A frames are ignored when ttc_l0a_en='0'");
        ttc_l0a_en <= '0';
        set_l0a_frame("011011000101");
        verify_idle;
        set_l0a_frame("111111111111");
        verify_idle;
        set_l0a_frame("111111000000");
        verify_idle;
        set_l0a_frame("100001010110");
        verify_idle;
        set_l0a_frame("010000001010");
        verify_idle;

        ----------------------------------
        log(ID_LOG_HDR, "Verify that invalid TTC L0A frames are ignored when ttc_l0a_en='1'");
        ttc_l0a_en <= '1';
        set_l0a_frame("000001111111");
        verify_idle;
        set_l0a_frame("000001010101");
        verify_idle;
        set_l0a_frame("000001100111");
        verify_idle;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that LCB frames are scheduled correctly [when BC gating is disabled] (1/2)");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '0';
        bypass_frame_valid   <= '0';
        set_l0a_frame(null_l0a);
        enqueue_and_verify_lcb_frame("0000000000000");
        enqueue_and_verify_lcb_frame("0000000000001");
        enqueue_and_verify_lcb_frame("1000001000000");
        enqueue_and_verify_lcb_frame("0101100110001");
        enqueue_and_verify_lcb_frame("0110010110110");
        enqueue_and_verify_lcb_frame("1000001010111");

        ----------------------------------

        log(ID_LOG_HDR, "Verify that LCB frames are scheduled [when BC gating is disabled] (2/2)");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '0';
        set_l0a_frame(null_l0a);
        bypass_frame_valid   <= '0';
        enqueue_lcb_frame("0000000000000");
        enqueue_lcb_frame("0000000000000");
        enqueue_lcb_frame("1000001000000");
        enqueue_lcb_frame("0101100110001");
        enqueue_lcb_frame("0110010110110");
        enqueue_lcb_frame("1000001010111");
        verify_lcb_frame("0000000000000");
        verify_lcb_frame("0000000000000");
        verify_lcb_frame("1000001000000");
        verify_lcb_frame("0101100110001");
        verify_lcb_frame("0110010110110");
        verify_lcb_frame("1000001010111");
        verify_lcb_count;


        ----------------------------------

        log(ID_LOG_HDR, "Verify that bypass frames are only scheduled if trickle_bc_gating_i='1' [when BC gating is enabled]");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '1';
        bypass_frame_valid   <= '0';
        set_l0a_frame(null_l0a);
        bypass_frame         <= x"BAD5";
        bypass_frame_valid   <= '1';
        verify_idle;
        verify_idle;
        trickle_bc_gating_i  <= '1';
        verify_bypass_frame(x"BAD5");
        trickle_bc_gating_i  <= '0';
        verify_idle;
        verify_idle;
        trickle_bc_gating_i  <= '1';
        verify_bypass_frame(x"BAD5");
        trickle_bc_gating_i  <= '0';
        verify_idle;
        verify_idle;
        trickle_bc_gating_i  <= '1';
        verify_bypass_frame(x"BAD5");

        ----------------------------------

        log(ID_LOG_HDR, "Verify that non-register-frame-start LCB frames are only scheduled if trickle_bc_gating_i='1' [when BC gating is enabled]");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '1';
        trickle_allow_register_cmd_i <= '0';
        bypass_frame_valid   <= '0';
        set_l0a_frame(null_l0a);
        enqueue_lcb_frame("0000000000000");
        enqueue_lcb_frame("0000000000001");
        enqueue_lcb_frame("1000001000000");
        enqueue_lcb_frame("0101100110001");
        enqueue_lcb_frame("0110010110110");
        enqueue_lcb_frame("1000000001111");
        verify_idle;
        verify_idle;
        trickle_bc_gating_i  <= '1';
        trickle_allow_register_cmd_i <= '1';
        verify_lcb_frame("0000000000000");
        verify_lcb_frame("0000000000001");
        trickle_bc_gating_i  <= '0';
        trickle_allow_register_cmd_i <= '0';
        verify_idle;
        verify_idle;
        trickle_bc_gating_i  <= '1';
        verify_lcb_frame("1000001000000");
        verify_lcb_frame("0101100110001");
        trickle_bc_gating_i  <= '0';
        verify_idle;
        verify_idle;
        trickle_bc_gating_i  <= '1';
        verify_lcb_frame("0110010110110");
        verify_lcb_frame("1000000001111");
        verify_lcb_count;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that register-frame-start LCB frames are only scheduled if trickle_bc_gating_i='1' and trickle_allow_register_cmd_i='1'");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '1';
        trickle_allow_register_cmd_i <= '0';
        bypass_frame_valid   <= '0';
        set_l0a_frame(null_l0a);
        enqueue_lcb_frame("100000001" & x"A");
        enqueue_lcb_frame("100000001" & x"B");
        enqueue_lcb_frame("100000001" & x"C");
        enqueue_lcb_frame("100000001" & x"D");
        enqueue_lcb_frame("100000001" & x"E");
        enqueue_lcb_frame("100000000" & x"F");
        verify_idle;
        verify_idle;
        trickle_allow_register_cmd_i <= '1';
        verify_idle;
        verify_idle;
        trickle_allow_register_cmd_i <= '0';
        trickle_bc_gating_i  <= '1';
        verify_idle;
        verify_idle;
        trickle_allow_register_cmd_i <= '1';
        verify_lcb_frame("100000001" & x"A");
        verify_lcb_frame("100000001" & x"B");
        trickle_bc_gating_i  <= '0';
        trickle_allow_register_cmd_i <= '0';
        verify_idle;
        verify_idle;
        trickle_bc_gating_i  <= '1';
        verify_idle;
        verify_idle;
        trickle_allow_register_cmd_i <= '1';
        verify_lcb_frame("100000001" & x"C");
        trickle_allow_register_cmd_i <= '0';
        verify_idle;
        verify_idle;
        trickle_allow_register_cmd_i <= '1';
        verify_lcb_frame("100000001" & x"D");
        trickle_allow_register_cmd_i <= '0';
        trickle_bc_gating_i  <= '0';
        verify_idle;
        verify_idle;
        trickle_bc_gating_i  <= '1';
        trickle_allow_register_cmd_i <= '1';
        verify_lcb_frame("100000001" & x"E");
        verify_lcb_frame("100000000" & x"F");
        trickle_allow_register_cmd_i <= '0';
        trickle_bc_gating_i  <= '0';
        verify_lcb_count;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that TTC L0A frames are prioritized over LCB frames");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '0';
        bypass_frame_valid   <= '0';
        enqueue_lcb_frame("0101100110001");
        enqueue_lcb_frame("0110010110110");
        await_value(lcb_rd_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "LCB FIFO becomes valid");
        set_and_verify_l0a_frame("111111111111");
        set_l0a_frame(null_l0a);
        verify_lcb_frame("0101100110001");
        verify_lcb_frame("0110010110110");
        verify_lcb_count;

        ----------------------------------

        log(ID_LOG_HDR, "Verify that TTC L0A frames are prioritized over bypass frames");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '0';
        bypass_frame         <= x"BAD5";
        bypass_frame_valid   <= '1';
        set_and_verify_l0a_frame("111111111111");
        set_l0a_frame(null_l0a);
        verify_bypass_frame(x"BAD5");

        ----------------------------------

        log(ID_LOG_HDR, "Verify that bypass frames are prioritized over LCB frames");
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '0';
        set_l0a_frame(null_l0a);
        enqueue_lcb_frame("0101100110001");
        enqueue_lcb_frame("0110010110110");
        await_value(lcb_rd_ready, '1', 0 ns, 10 * C_CLK_PERIOD, ERROR, "LCB FIFO becomes valid");
        set_and_verify_bypass_frame(x"BAD5");
        verify_lcb_frame("0101100110001");
        verify_lcb_frame("0110010110110");
        verify_lcb_count;

        ----------------------------------
        log(ID_LOG_HDR, "Verify that every valid L0A frame decodes back to itself, and every invalid one results in IDLE");
        set_l0a_frame(null_l0a);
        bypass_frame_valid   <= '0';
        trickle_bc_gating_i  <= '0';
        trickle_bc_gating_en <= '0';
        frame_range          := 12;
        for i in 0 to 2**frame_range - 1 loop
            l0a_frame_var   := std_logic_vector(to_unsigned(i, frame_range));
            l0a_frame_valid := l0a_frame_var(11) or l0a_frame_var(10) or l0a_frame_var(9) or l0a_frame_var(8) or l0a_frame_var(7);
            set_l0a_frame(l0a_frame_var);
            if l0a_frame_valid = '1' then
                verify_frame("0" & l0a_frame_var, "L0A frame mismatch (dec=" & integer'image(i) & ")");
            else
                verify_idle("Expected IDLE for invalid L0A frame (dec=" & integer'image(i) & ")");
            end if;
        end loop;

        ----------------------------------
        log(ID_LOG_HDR, "Verify that every LCB frame decodes back to itself: no commas");
        set_l0a_frame(null_l0a);
        frame_range := 12;
        for i in 0 to 2**frame_range - 1 loop
            lcb_frame_var := "0" & std_logic_vector(to_unsigned(i, frame_range));
            enqueue_and_verify_lcb_frame(lcb_frame_var);
        end loop;

        ----------------------------------
        log(ID_LOG_HDR, "Verify that every LCB frame decodes back to itself: K2 & K3 commas, IDLE");
        set_l0a_frame(null_l0a);
        frame_range := 6;
        for i in 0 to 2**frame_range - 1 loop
            lcb_frame_var := "1" & "000000" & std_logic_vector(to_unsigned(i, frame_range));
            enqueue_and_verify_lcb_frame(lcb_frame_var);
            lcb_frame_var := "1" & "000001" & std_logic_vector(to_unsigned(i, frame_range));
            enqueue_and_verify_lcb_frame(lcb_frame_var);
            lcb_frame_var := "1" & "000010" & std_logic_vector(to_unsigned(i, frame_range));
            enqueue_and_verify_lcb_frame(lcb_frame_var);
        end loop;
        ----------------------------------

        -- Ending the simulation
        wait for 100 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED");

        -- Finish the simulation
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;                           -- to stop completely
    end process;

    clock_generator(clk, clk_en, C_CLK_PERIOD, "40 MHz BC clock");

    xpm_fifo_sync_inst : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            FIFO_MEMORY_TYPE => "distributed", -- String
            FIFO_READ_LATENCY => 0,     -- DECIMAL
            FIFO_WRITE_DEPTH => 64,   -- DECIMAL
            FULL_RESET_VALUE => 0,      -- DECIMAL
            PROG_EMPTY_THRESH => 4,    -- DECIMAL
            PROG_FULL_THRESH => 63,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 7,   -- DECIMAL = log2(FIFO_READ_DEPTH)+1.
            -- FIFO_READ_DEPTH =  FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH / READ_DATA_WIDTH
            READ_DATA_WIDTH => 13,      -- DECIMAL
            READ_MODE => "fwft",         -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_ADV_FEATURES => "0808", -- String
            WAKEUP_TIME => 0,           -- DECIMAL
            WRITE_DATA_WIDTH => 13,     -- DECIMAL
            WR_DATA_COUNT_WIDTH => 7    -- DECIMAL = log2(FIFO_WRITE_DEPTH)+1
        )
        port map (
            sleep => '0',
            rst => rst,
            wr_clk => clk,
            wr_en => lcb_frame_wr_en,
            din => lcb_frame_din,
            full => lcb_frame_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => lcb_frame_wr_rst_busy,
            almost_full => lcb_frame_almost_full,
            wr_ack => open,
            rd_en => lcb_frame_rd_en,
            dout => lcb_frame,
            empty => lcb_frame_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => lcb_frame_rd_rst_busy,
            almost_empty => lcb_frame_almost_empty,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    DUT : entity work.lcb_scheduler_encoder
        port map(
            clk => clk,
            rst => rst,
            l0a_frame_i => l0a_frame,
            lcb_frame_i => lcb_frame,
            lcb_frame_o_rd_en => lcb_frame_rd_en,
            lcb_frame_i_empty => lcb_frame_empty,
            lcb_frame_i_almost_empty => lcb_frame_almost_empty,
            l0a_frame_delay_i => l0a_frame_delay_i,
            bypass_frame_i => bypass_frame,
            bypass_frame_valid_i => bypass_frame_valid,
            bypass_frame_ready_o => bypass_frame_ready,
            frame_start_pulse_i => frame_start_pulse_i,
            trickle_bc_gating_i => trickle_bc_gating_i,
            trickle_allow_register_cmd_i => trickle_allow_register_cmd_i,
            trickle_bc_gating_en => trickle_bc_gating_en,
            ttc_l0a_en => ttc_l0a_en,
            encoded_frame_o => encoded_frame
        );

    lcb_rd_ready <= (not lcb_frame_empty) and (not lcb_frame_rd_rst_busy);
    lcb_wr_ready <= (not lcb_frame_full) and (not lcb_frame_wr_rst_busy);

    lcb_fifo_counters : process(clk)    -- keep track of how many data frames passed the LCB fifo
    begin
        if (rising_edge(clk)) then
            if lcb_frame_rd_en = '1' then
                lcb_rd_ctr <= lcb_rd_ctr + 1;
            end if;
            if lcb_frame_wr_en = '1' then
                lcb_wr_ctr <= lcb_wr_ctr + 1;
            end if;
        end if;
    end process;

    sanity_check : process(clk)
    begin
        if rising_edge(clk) then
            if lcb_frame_rd_en = '1' then
                check_value(lcb_frame_empty, '0', ERROR, "Attempting to read from empty FIFO (lcb_frame)");
                check_value(lcb_frame_rd_rst_busy, '0', ERROR, "Attempting to read from FIFO while it's reset (lcb_frame)");
            end if;

            if lcb_frame_wr_en = '1' then
                check_value(lcb_frame_full, '0', ERROR, "Attempting to write into a full FIFO (lcb_frame)");
                check_value(lcb_frame_wr_rst_busy, '0', ERROR, "Attempting write into the FIFO while it's reset (lcb_frame)");
            end if;
        end if;
    end process;

end architecture behavioral;
