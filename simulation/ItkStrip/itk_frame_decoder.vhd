--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench helper module for decoding ITK Strips data
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_bypass_scheduler_continuous_write.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Tue May 26 20:00:40 2020
-- Last update : Tue May 26 20:00:40 2020
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 BNL
-------------------------------------------------------------------------------
-- Description:
--
-- This module is not synthesizable.
--
-- Align the frame by locking to the IDLEs pattern.
-- Output aligned frames to the testbench. Output frame_next_o = '1'
-- pulse when frame is updated. If skip_idle_i = '1', suppress
-- IDLE frames
--
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;

entity itk_frame_decoder is
    port(
        clk             : in  std_logic;
        rst             : in  std_logic;
        bcr             : in  std_logic;
        stopwatch_set  : in  std_logic;
        stopwatch_rst  : in  std_logic;
        edata_i         : in  std_logic_vector(3 downto 0);
        skip_idles_i    : in  std_logic;
        frame_encoded_o : out std_logic_vector(15 downto 0);
        is_locked_o     : out std_logic;
        is_idle_o       : out std_logic;
        is_k2_o         : out std_logic;
        is_k3_o         : out std_logic;
        frame_decoded_o : out std_logic_vector(11 downto 0);
        frame_next_o    : out std_logic;
        phase_wrt_bcr_o : out integer range 0 to 3;
        stopwatch_o    : out integer
    );
end entity itk_frame_decoder;

architecture RTL of itk_frame_decoder is
    constant K0 : std_logic_vector(7 downto 0) := x"78";
    constant K1 : std_logic_vector(7 downto 0) := x"55";
    constant K2 : std_logic_vector(7 downto 0) := x"47";
    constant K3 : std_logic_vector(7 downto 0) := x"6A";

    constant idle_frame : std_logic_vector(15 downto 0) := K0 & K1;
    constant max_idles  : integer                       := 3;

    signal locked            : std_logic            := '0';
    signal frame_buffer      : std_logic_vector(19 downto 0);
    signal frame_lock_buffer : std_logic_vector(16 * max_idles - 1 downto 0);
    signal frame_buffer_old  : std_logic_vector(15 downto 0);
    signal quad_counter      : integer range 0 to 3 := 0;
    signal stopwatch_reg    : integer := 0;
    signal stopwatch_en     : boolean := false;

begin

    frame_buffer_old <= frame_buffer(19 downto 4);
    stopwatch_o <= stopwatch_reg;

    p_lock : process(clk) is
        variable ones_ctr, zeroes_ctr : integer := 0;
    begin
        if rising_edge(clk) then
            if rst = '1' then
                locked            <= '0';
                frame_buffer      <= (others => '0');
                frame_lock_buffer <= (others => '0');
                quad_counter      <= 0;
            else
                frame_lock_buffer <= frame_lock_buffer(frame_lock_buffer'high - 4 downto 0) & edata_i;
                frame_buffer      <= frame_buffer(15 downto 0) & edata_i;

                if locked = '1' then
                    quad_counter <= 0 when quad_counter = 3 else quad_counter + 1;
                else
                    quad_counter <= 0;
                end if;

                if frame_lock_buffer = idle_frame & idle_frame & idle_frame then
                    quad_counter <= 0;
                    locked       <= '1';
                end if;

                -- check the frame is balanced
                if locked = '1' and quad_counter = 0 then
                    ones_ctr   := 0;
                    zeroes_ctr := 0;
                    for i in frame_buffer_old'range loop
                        if frame_buffer_old(i) = '1' then
                            ones_ctr := ones_ctr + 1;
                        else
                            zeroes_ctr := zeroes_ctr + 1;
                        end if;
                    end loop;
                    locked     <= '0' when ones_ctr /= zeroes_ctr;
                end if;

            end if;
        end if;
    end process;

    p_decode : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' or locked = '0' then
                frame_encoded_o <= (others => '0');
                frame_decoded_o <= (others => '0');
                is_locked_o     <= '0';
                is_idle_o       <= '0';
                is_k2_o         <= '0';
                is_k3_o         <= '0';
                frame_next_o    <= '0';
            else
                frame_next_o <= '0';
                is_locked_o     <= locked;
                is_idle_o       <= '1' when frame_buffer_old = idle_frame else '0';
                if quad_counter = 0 and (skip_idles_i = '0' or frame_buffer_old /= idle_frame) then
                    frame_next_o    <= '1';
                    frame_encoded_o <= frame_buffer_old;
                    is_k2_o         <= '1' when frame_buffer_old(15 downto 8) = K2 else '0';
                    is_k3_o         <= '1' when frame_buffer_old(15 downto 8) = K3 else '0';
                    frame_decoded_o <= decode_6b8b(frame_buffer_old(15 downto 8), report_errors => false) & decode_6b8b(frame_buffer_old(7 downto 0), report_errors => false);
                end if;
            end if;
        end if;
    end process;


    p_phase_measurement : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' or locked = '0' then
                phase_wrt_bcr_o <= 0;
            else
                phase_wrt_bcr_o <= quad_counter when bcr = '1';
            end if;
        end if;
    end process;


    p_stopwatch : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                stopwatch_reg <= 0;
                stopwatch_en <= false;
            else
                if stopwatch_set = '1' then
                    stopwatch_en <= true;
                end if;

                if stopwatch_en then
                    stopwatch_reg <= stopwatch_reg + 1;
                end if;

                if stopwatch_rst = '1' then
                    stopwatch_en <= false;
                    stopwatch_reg <= 0;
                end if;
            end if;
        end if;
    end process;

end architecture RTL;
