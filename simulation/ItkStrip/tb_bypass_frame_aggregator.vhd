--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_bypass_frame_aggregator.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Fri Apr 10 16:31:36 2020
-- Last update : Wed Nov 16 13:18:59 2022
-- Platform    : Default Part Number
-- Standard    : VHDL-2008
--------------------------------------------------------------------------------
-- Copyright (c) 2020 BNL
-------------------------------------------------------------------------------
-- Description:  Testbench for strips_bypass_frame_aggregator module
--
-- Test criteria:
--
-- 0. When rst is asserted, byte_ready and frame_valid are deasserted
-- 1. When input is valid and the output is not ready, byte_ready is deasserted, the data is not lost
-- 2. When input is not valid and the output is ready, frame_valid is deasserted, no garbage data is inserted
-- 3. When input is not valid and the output is ready, frame_valid is deasserted, no garbage data is inserted
-- 4. Frame is formed as expected in all cases, LSB byte is written first
-- 5. Frame is formed in 4 clk cycles or less
--
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_bypass_frame_aggregator is
end entity tb_bypass_frame_aggregator;

architecture RTL of tb_bypass_frame_aggregator is
    constant C_CLK_PERIOD_40  : time    := 25 ns;
    constant C_CYCLES_RST     : integer := 3;
    --constant C_STABLE_PERIODS : integer := 10;
    constant C_MAX_CYCLES_PER_BYTE : integer := 4;

    signal clk    : std_logic;          -- BC clock
    signal clk_en : boolean   := false;
    signal rst    : std_logic := '0';
    signal byte_i        : std_logic_vector(7 downto 0) := (others => '0'); -- byte from elink
    signal byte_valid_i  : std_logic := '0'; -- elink byte is valid this clk cycle
    signal byte_ready_o  : std_logic; -- elink byte can be stored this clk cycle
    signal frame_o       : std_logic_vector(15 downto 0); -- formed frame output
    signal frame_valid_o : std_logic; -- frame is valid this clk cycle
    signal frame_ready_i : std_logic := '0'; -- receiver is ready for the next frame

    type t_data is array (natural range <>) of std_logic_vector(7 downto 0);
    signal test_data  :t_data(0 to 1000);

    signal generator_start : boolean := false;
    signal generator_done : boolean := false;
    signal generator_done_ack : boolean := false;
    signal generator_delay : natural := 0;

    signal checker_start : boolean := false;
    signal checker_done : boolean := false;
    signal checker_done_ack : boolean := false;
    signal checker_delay : natural := 0;
    signal next_frame : std_logic_vector(frame_o'range) := (others => '0');

begin
    test : process is
        -- set RST to a given value and wait C_CYCLES_RST cycles
        procedure set_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
        end;

        procedure toggle_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
            rst <= not value;
        end;

        -- fills the test data with random bytes
        procedure generate_random_data is
        begin
            for i in test_data'range loop
                test_data(i) <= random(test_data(i)'length);
            end loop;
        end;

        -- wait for the data generator to complete
        procedure wait_for_process(
            signal done_signal : in boolean;
            signal ack_signal : out boolean;
            delay : natural;
            process_name : string := "(not specified)"
        ) is
            variable timeout : time;
        begin
            timeout := C_CLK_PERIOD_40 * (C_MAX_CYCLES_PER_BYTE + delay) * test_data'length;
            await_value(done_signal, true, 0 ns, timeout, ERROR, "Waiting for process: " & process_name);
            log(ID_BFM, "Process '" & process_name & "' is complete");
            wait until rising_edge(clk);
            ack_signal <= true;
            wait until rising_edge(clk);
            ack_signal <= false;
        end;

        -- wait for the data generator process
        procedure wait_for_generator is
        begin
            wait_for_process(
				done_signal => generator_done,
				ack_signal => generator_done_ack,
				delay => generator_delay + checker_delay,
				process_name => string'("elink data generator")
			);
        end;

        -- wait for the data generator process
        procedure wait_for_checker is
        begin
            wait_for_process(
				done_signal => checker_done,
				ack_signal => checker_done_ack,
				delay => generator_delay + checker_delay,
				process_name => string'("frame checker")
			);
        end;

        -- start another process
        procedure start_process(
            signal start_signal : out boolean;
            signal delay_signal : out natural;
            process_name : string := "(not specified)";
            delay : natural
        ) is
        begin
            log(ID_BFM, "Starting '" & process_name & "' process");
            delay_signal <= delay;
            start_signal <= true;
            wait until rising_edge(clk);
            start_signal <= false;
            wait until rising_edge(clk);
        end;

        -- start the data generator process
        procedure start_generator(
            delay : natural := 0;
            generate_new_data : boolean := true
        ) is
        begin
            if generate_new_data then
                generate_random_data;
            end if;
            start_process(
				start_signal => generator_start,
				delay_signal => generator_delay,
				process_name => string'("elink data generator"),
				delay => delay
			);
        end;

        -- start the data checker process
        procedure start_checker(
            delay : natural := 0
        ) is
        begin
            start_process(
				start_signal => checker_start,
				delay_signal => checker_delay,
				process_name => string'("frame checker"),
				delay => delay
			);
        end;

        -- wait for both generator and checker
        procedure await_test_completion is
        begin
            wait_for_generator;
            wait_for_checker;
        end;
    begin
        ------------------------- Actual tests begin here -----------------------------
        disable_log_msg(ID_POS_ACK);
        disable_log_msg(ID_BFM);
        clk_en <= true;
        log(ID_LOG_HDR, "Checking behavior when rst = 1");
        set_rst('1');
        check_value(byte_ready_o, '1', ERROR, "byte_ready_o signal should be '1'");
        check_value(frame_valid_o, '0', ERROR, "frame_valid_o signal should be '0'");

        log(ID_LOG_HDR, "Checking behavior when input is fast, the output is slow");
        set_rst('0');
        start_generator(delay => 0);
        start_checker(delay => 0);
        await_test_completion;

        log(ID_LOG_HDR, "Checking behavior when input is slow, the output is fast");
        toggle_rst('1');
        start_generator(delay => 10);
        start_checker(delay => 0);
        await_test_completion;

        log(ID_LOG_HDR, "Checking behavior when both input and output are fast");
        toggle_rst('1');
        start_generator(delay => 0);
        start_checker(delay => 10);
        await_test_completion;


        log(ID_LOG_HDR, "Checking behavior when both input and output are slow");
        toggle_rst('1');
        start_generator(delay => 11);
        start_checker(delay => 7);
        await_test_completion;

        ------------------------- Tests end here -----------------------------

        -- Ending the simulation
        wait for 1000 ns;               -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        clk_en <= false;
        wait;                           -- to stop completely

    end process;

    clock_generator(clk, clk_en, C_CLK_PERIOD_40, "40 MHz BC clock");

    dut : entity work.strips_bypass_frame_aggregator
        generic map (
            TIMEOUT => 4000
        )
        port map (
            clk => clk,
            rst => rst,
            byte_i => byte_i,
            byte_valid_i => byte_valid_i,
            byte_ready_o => byte_ready_o,
            frame_o => frame_o,
            frame_valid_o => frame_valid_o,
            frame_ready_i => frame_ready_i
        );

    --- Boiler-plate data checker process (sort of)
    data_checker : process is
        variable checker_ptr : integer := 0;
        variable checker_data_is_valid : boolean := False;

        procedure get_next_valid_byte(
            signal byte : out std_logic_vector(7 downto 0)
        ) is
        begin
            if checker_ptr >= test_data'length then
                checker_data_is_valid := false;
                return;
            end if;

            while checker_ptr < test_data'length loop
                byte <= test_data(checker_ptr);
                checker_ptr := checker_ptr + 1;
                checker_data_is_valid := true;
                return;
            end loop;
            checker_data_is_valid := false;
        end;

        procedure get_next_valid_frame is
        begin
            get_next_valid_byte(next_frame(15 downto 8));
            get_next_valid_byte(next_frame(7 downto 0));
        end;
    begin
        if checker_start then
            log(ID_BFM, "Data checker has started");
            ---- data checker logic start ----
            wait until rising_edge(clk);
            checker_ptr := 0;
            get_next_valid_frame;
            frame_ready_i <= '1';

            while checker_data_is_valid loop
                wait until rising_edge(clk);
                if frame_valid_o = '1' then
                    check_value(frame_o, next_frame, ERROR,
						"Correct frame is dequeued (ptr = " & to_string(checker_ptr) & ")"
					);
                    get_next_valid_frame;
                    if checker_delay > 0 then
                        frame_ready_i <= '0';
                        wait_num_rising_edge(clk, checker_delay);
                        frame_ready_i <= '1';
                    end if;
                end if;
            end loop;
            frame_ready_i <= '0';

            ---- data checker logic end ----
            log(ID_BFM, "Data checker has stopped");
            checker_done <= true;
            wait until rising_edge(checker_done_ack);
            checker_done <= false;
        end if;
        wait until rising_edge(clk);
    end process;

    --- Boiler-plate data provider process (sort of)
    data_generator : process is
        variable generator_ptr : integer := 0;
    begin
        if generator_start then
            log(ID_BFM, "Data generator has started");
            ---- data checker logic start ----
            wait until rising_edge(clk);
            byte_i <= test_data(0);
            byte_valid_i <= '1';
            generator_ptr := 0;

            while generator_ptr < test_data'length loop
                wait until rising_edge(clk);
                if byte_ready_o = '1' then
                    if generator_ptr = test_data'length - 1 then
                        byte_valid_i <= '0';
                    else
                        byte_i <= test_data(generator_ptr + 1);
                        if generator_delay > 0 then
                            byte_valid_i <= '0';
                            wait_num_rising_edge(clk, generator_delay);
                            byte_valid_i <= '1';
                        end if;
                    end if;
                    generator_ptr := generator_ptr + 1;
                end if;
            end loop;

            log(ID_BFM, "Data generator has stopped");
            ---- data generator logic end ----
            generator_done <= true;
            wait until rising_edge(generator_done_ack);
            generator_done <= false;
        end if;
        wait until rising_edge(clk);
    end process;



end architecture RTL;
