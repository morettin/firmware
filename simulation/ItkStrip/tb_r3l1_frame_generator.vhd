--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_r3l1_frame_generator.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Wed May 20 16:32:32 2020
-- Last update : Thu May 21 18:30:59 2020
-- Platform    : Default Part Number
-- Standard    : VHDL-2008
--------------------------------------------------------------------------------
-- Copyright (c) 2020 BNL
-------------------------------------------------------------------------------
-- Description:
-- Set of tests for r3l1_frame_generator
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.MATH_REAL.all;
    use work.strips_package.all;

--library STD;
    use std.env.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_r3l1_frame_generator is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_r3l1_frame_generator(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_r3l1_frame_generator;

architecture RTL of tb_r3l1_frame_generator is
    constant C_CLK_PERIOD_40               : time    := 25 ns;
    constant C_CYCLES_RST                  : integer := 3;
    constant C_STABLE_PERIODS              : integer := 10;
    --constant C_MAX_FIRST_FRAME_PULSE_DELAY : integer := 6;
    --constant C_MAX_FRAME_PULSE_DELAY       : integer := 4;
    --constant C_ONE_FRAME_DELAY             : time    := C_CLK_PERIOD_40 * 4;

    signal clk    : std_logic;          -- BC clock
    signal clk_en : boolean   := false;
    signal rst    : std_logic := '0';

    signal enable_i             : std_logic; -- enables generation of L1 frames
    signal trigger_i            : std_logic; -- L1 or R3 trigger signal
    signal module_mask_i        : std_logic_vector(4 downto 0);
    signal tag_i                : std_logic_vector(6 downto 0); -- L0 tag for this frame
    signal frame_o              : STD_LOGIC_VECTOR(11 downto 0); -- FIFO data out
    signal frame_rd_en_i        : std_logic := '0'; -- FIFO rd_en signal
    signal frame_empty_o        : std_logic; -- FIFO empty signal
    signal frame_almost_empty_o : std_logic; -- FIFO contains one word -- @suppress "signal frame_almost_empty_o is never read"
--signal frame_overflow_o     : std_logic; -- FIFO overflow has occured
begin

    TEST : process is
        -- set RST to a given value and wait C_CYCLES_RST cycles
        procedure set_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
        end;

        -- send a single r3/l1 frame
        procedure generate_single_r3l1_frame(
            frame : std_logic_vector(11 downto 0) -- frame contents
        ) is
        begin
            log(ID_SEQUENCER_SUB, "Sending R3/L1 frame " & to_hstring(frame));
            module_mask_i <= frame(11 downto 7);
            tag_i         <= frame(6 downto 0);
            trigger_i     <= '1';
            wait until rising_edge(clk);
            trigger_i     <= '0';
        end;

        -- send and verify a single r3/l1 frame
        procedure generate_and_verify_r3l1_frame(
            frame : std_logic_vector(11 downto 0) -- frame contents
        ) is
        begin
            generate_single_r3l1_frame(frame);
            await_value(frame_empty_o, '0', 0 ns, 10 * C_CLK_PERIOD_40, ERROR, "R3/L1 frame is enqueued");
            check_value(frame_o, frame, ERROR, "R3/L1 frame matches the expected value");
            frame_rd_en_i <= '1';
            wait until rising_edge(clk);
            frame_rd_en_i <= '0';
        end;

        variable frame_range : integer;
    begin
        disable_log_msg(ID_POS_ACK);
        disable_log_msg(ID_SEQUENCER_SUB);
        clk_en <= true;

        --------------------   Tests begin here -------------------------------

        log(ID_LOG_HDR, "Checking behavior when rst = 1");
        set_rst('1');
        enable_i  <= '0';
        check_value(frame_empty_o, '1', ERROR, "There are no enqueued R3/L1 frames");
        wait_num_rising_edge(clk, C_STABLE_PERIODS);
        check_value(frame_empty_o, '1', ERROR, "There are no enqueued R3/L1 frames");
        trigger_i <= '1';
        wait_num_rising_edge(clk, C_STABLE_PERIODS);
        check_value(frame_empty_o, '1', ERROR, "There are no enqueued R3/L1 frames");
        trigger_i <= '0';
        wait until rising_edge(clk);

        -----------------------------------------------------------------------

        log(ID_LOG_HDR, "Checking behavior when enable = 0", C_SCOPE);
        set_rst('0');
        enable_i  <= '0';
        check_value(frame_empty_o, '1', ERROR, "There are no enqueued R3/L1 frames");
        wait_num_rising_edge(clk, C_STABLE_PERIODS);
        check_value(frame_empty_o, '1', ERROR, "There are no enqueued R3/L1 frames");
        trigger_i <= '1';
        wait_num_rising_edge(clk, C_STABLE_PERIODS);
        check_value(frame_empty_o, '1', ERROR, "There are no enqueued R3/L1 frames");
        trigger_i <= '0';
        wait until rising_edge(clk);

        -----------------------------------------------------------------------

        enable_i    <= '1';
        wait until rising_edge(clk);
        log(ID_LOG_HDR, "Checking that every possible frame is enqueued correctly");
        frame_range := 12;
        for i in 0 to 2**frame_range - 1 loop
            generate_and_verify_r3l1_frame(std_logic_vector(to_unsigned(i, frame_range)));
        end loop;
        wait until rising_edge(clk);
        check_value(frame_empty_o, '1', ERROR, "There are no stray R3/L1 frames");

        -----------------------------------------------------------------------

        log(ID_LOG_HDR, "SIMULATION COMPLETED");
        report_alert_counters(FINAL);
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;
    end process;

    clock_generator(clk, clk_en, C_CLK_PERIOD_40, "40 MHz BC clock");

    dut : entity work.r3l1_frame_generator
        port map(
            clk                  => clk,
            rst                  => rst,
            enable_i             => enable_i,
            trigger_i            => trigger_i,
            module_mask_i        => module_mask_i,
            tag_i                => tag_i,
            frame_o              => frame_o,
            frame_rd_en_i        => frame_rd_en_i,
            frame_empty_o        => frame_empty_o,
            frame_almost_empty_o => frame_almost_empty_o
        --frame_overflow_o     => frame_overflow_o
        );

end architecture RTL;
