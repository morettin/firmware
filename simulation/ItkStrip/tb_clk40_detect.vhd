--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : CLK40 transition detection testbench
-- Project     : Default Project Name
--------------------------------------------------------------------------------
-- File        : tb_clk40_detect.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : User Company Name
-- Created     : Tue Nov 10 14:05:33 2020
-- Last update : Tue Nov 10 14:32:36 2020
-- Platform    : Default Part Number
-- Standard    : VHDL-2008
--------------------------------------------------------------------------------
-- Copyright (c) 2020 User Company Name
-------------------------------------------------------------------------------
-- Description: 
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library IEEE;
library work;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

entity tb_clk40_detect is
	
end entity tb_clk40_detect;

architecture rtl of tb_clk40_detect is
	signal clk40 	: std_logic;
	signal clk320 	: std_logic;
	signal clk_en   : boolean := false;
	signal rst      : std_logic := '0';

	signal TXCLK40_reg       : std_logic := '0';
	signal TXCLK40_reg_z     : std_logic := '0';
	signal cnt               : unsigned(2 downto 0);


	constant C_CLK_40_PERIOD        : time    := 25 ns;
	constant C_CLK_320_PERIOD       : time    := C_CLK_40_PERIOD / 8;	
begin
	clock_generator(clk40, clk_en, C_CLK_40_PERIOD, "40 MHz clock");
	clock_generator(clk320, clk_en, C_CLK_320_PERIOD, "320 MHz clock");

    process(clk40)
    begin
        if rising_edge(clk40) then            
            TXCLK40_reg  <= not TXCLK40_reg;
        end if;
    end process;

    process(clk320)
    begin
        if rising_edge(clk320) then
        	TXCLK40_reg_z <= TXCLK40_reg;

        	if TXCLK40_reg_z /= TXCLK40_reg then
                cnt <="001";
            else
                cnt <=cnt + '1';
            end if;

        end if;
    end process;

    test : process is
    begin
    	clk_en  <= true;
    	wait_num_rising_edge(clk40, 50);
    	clk_en  <= false;
    	wait;
    end process;

end architecture rtl;