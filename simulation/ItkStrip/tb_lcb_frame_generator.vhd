--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench simulation for lcb_frame_generator
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : tb_lcb_frame_generator.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Tue Oct 22 09:51:14 2019
-- Last update : Thu May 21 18:31:23 2020
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008 | VHDL-2002 | VHDL-1993 | VHDL-1987>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 User Company Name
-------------------------------------------------------------------------------
-- Description:
-- 1. Verify that single command passes to the output of the FIFO and is correctly formed
-- 2. Verify that multiple commands pass to the output of the FIFO in the correct order
-- 3. Verify that the commands are not dropped if the module's output fifo is full
-- 4. Verify that the command is formed correctly start_pulse is high every clk cycle
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use work.strips_package.all;
library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_lcb_frame_generator is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_lcb_frame_generator(Behavioral)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_lcb_frame_generator;

architecture Behavioral of tb_lcb_frame_generator is

    --constant clk_T               : time    := 25 ns;
    constant over_l0a_max_depth  : integer := 120;
    --constant over_fc_max_depth   : integer := 120;
    --constant over_rw_max_depth   : integer := 20;
    constant clk_wait_after_loop : integer := 3;

    signal clk                      : std_logic; -- 40 MHz BC clk
    signal clk_en                   : boolean   := false;
    signal rst                      : std_logic := '0'; -- synchronous reset
    signal cmd_i                    : t_lcb_command := LCB_CMD_IDLE; -- which command to execute
    signal cmd_start_pulse_i        : std_logic := '0'; -- pulse for 1 clk cycle to start command
    signal fast_cmd_data_i          : std_logic_vector(5 downto 0) := (others => '0'); -- bc & cmd
    signal l0a_data_i               : std_logic_vector(11 downto 0) := (others => '0'); -- bcr & L0A mask & L0 tag
    signal abc_id_i                 : std_logic_vector(3 downto 0) := (others => '0'); -- ABC* address (0xF = broadcast)
    signal hcc_id_i                 : std_logic_vector(3 downto 0) := (others => '0'); -- HCC* address (0xF = broadcast)
    signal reg_addr_i               : std_logic_vector(7 downto 0) := (others => '0'); -- register address for reg. read/write
    signal reg_data_i               : std_logic_vector(31 downto 0) := (others => '0'); -- register data for reg. write
    signal ready_o                  : std_logic; -- indicates it's ready for a new command
    signal lcb_frame_o              : STD_LOGIC_VECTOR(12 downto 0); -- FIFO out
    signal lcb_frame_i_rd_en        : std_logic := '0'; -- FIFO rd_en signal
    signal lcb_frame_o_empty        : std_logic; -- FIFO empty signal
    signal lcb_frame_o_almost_empty : std_logic; -- FIFO contains one word
    signal lcb_frame_fifo_almost_full_o : std_logic;

    signal lcb_frame : std_logic_vector(lcb_frame_o'range) := (others => '0'); -- temporary buffer for the last read out LCB word

    signal abc_mask_i : t_abc_mask := (others => (others => '0'));
    signal hcc_mask_i : std_logic_vector(15 downto 0) := (others => '0');

    constant C_CLK_PERIOD : time    := 25 ns; -- 40 MHz BC clk
    constant C_CYCLES_RST : integer := 3;
    constant C_CYCLES_WAIT_MAX : integer := 10;

    constant COMMA      : std_logic_vector := "1";
    constant NO_COMMA   : std_logic_vector := "0";
    constant K2         : std_logic_vector := "000000";
    constant K3         : std_logic_vector := "000001";
    constant IDLE_FRAME : std_logic_vector := "1000010000000";
    constant MARKER     : std_logic_vector := "00000";

    type t_lcb_command_array is array (natural range <>) of t_lcb_command;
    type t_natural_array is array (natural range <>) of natural;
    constant delay_array : t_natural_array := (0, 10);
begin

    DUT : entity work.lcb_frame_generator
        port map(
            clk => clk,
            rst => rst,
            cmd_i => cmd_i,
            cmd_start_pulse_i => cmd_start_pulse_i,
            fast_cmd_data_i => fast_cmd_data_i,
            hcc_mask_i => hcc_mask_i,
            abc_mask_i => abc_mask_i,
            l0a_data_i => l0a_data_i,
            abc_id_i => abc_id_i,
            hcc_id_i => hcc_id_i,
            reg_addr_i => reg_addr_i,
            reg_data_i => reg_data_i,
            ready_o => ready_o,
            lcb_frame_o => lcb_frame_o,
            lcb_frame_i_rd_en => lcb_frame_i_rd_en,
            lcb_frame_o_empty => lcb_frame_o_empty,
            lcb_frame_o_almost_empty => lcb_frame_o_almost_empty,
            lcb_frame_fifo_almost_full_o => lcb_frame_fifo_almost_full_o
        );

    clock_generator(clk, clk_en, C_CLK_PERIOD, "40 MHz BC clock");

    test : process is
        --------------------------------------------------------------------------------
        -- Some macro declarations; keep scrolling to the actual tests
        --------------------------------------------------------------------------------
        function register_read_parameters(
            constant abc_not_hcc : std_logic; -- @suppress "Unused declaration"
            constant hcc_id  : std_logic_vector(3 downto 0)  := (others => '0');
            constant abc_id  : std_logic_vector(3 downto 0)  := (others => '0');
            constant addr    : std_logic_vector(7 downto 0)  := (others => '0')
        ) return string is
        begin
            return string'("hcc_id = " & to_string(hcc_id, HEX) & ", abc_id = "
                & to_string(abc_id, HEX) & ", addr = " & to_string(addr, HEX));
        end;

        function register_write_parameters(
            constant abc_not_hcc : std_logic;
            constant hcc_id  : std_logic_vector(3 downto 0)  := (others => '0');
            constant abc_id  : std_logic_vector(3 downto 0)  := (others => '0');
            constant addr    : std_logic_vector(7 downto 0)  := (others => '0');
            constant data    : std_logic_vector(31 downto 0) := (others => '0')
        ) return string is
        begin
            return register_read_parameters(abc_not_hcc, hcc_id, abc_id, addr)
                & string'(", data = ") & to_string(data, HEX);
        end;

        -- wait until the frame is valid
        procedure read_lcb_frame is
        begin
            if lcb_frame_o_almost_empty = '1' and lcb_frame_i_rd_en'last_value = '1' then
                wait until rising_edge(clk);
            end if;
            await_value(lcb_frame_o_empty, '0', 0 ns, C_CYCLES_WAIT_MAX * C_CLK_PERIOD,
                ERROR, "Waiting for the next LCB frame to become available"
            );
            lcb_frame_i_rd_en <= '1';
            wait for C_CLK_PERIOD / 2;
            lcb_frame <= lcb_frame_o;
            wait until rising_edge(clk);
            lcb_frame_i_rd_en <= '0';
        end;

        -- wait for the command encoder to become ready and issues the command
        procedure issue_command_when_ready is
        begin
            --wait until rising_edge(clk);
            if ready_o = '0' then
                await_value(ready_o, '1', 0 ns, C_CYCLES_WAIT_MAX * C_CLK_PERIOD,
                    ERROR, "Waiting for the frame generator to become ready");
            end if;

            cmd_start_pulse_i <= '1';
            wait until rising_edge(clk);
            cmd_start_pulse_i <= '0';
        end;

        -- Issues L0A command
        procedure issue_l0a(
            constant bcr  : std_logic                    := '0';
            constant mask : std_logic_vector(3 downto 0) := (others => '0');
            constant tag  : std_logic_vector(6 downto 0) := (others => '0')
        ) is
        begin
            log(ID_SEQUENCER_SUB, "-- Send command: L0A: bcr = " & to_string(bcr)
                    & ", mask = " & to_string(mask) & ", tag = " & to_string(tag, HEX));
            cmd_i      <= LCB_CMD_L0A;
            l0a_data_i <= bcr & mask & tag;
            issue_command_when_ready;
        end;

        -- verifies the IDLE is encoded correctly (and dequeues it)
        procedure verify_idle is
        begin
            log(ID_SEQUENCER_SUB, "-- Verify: IDLE frame");
            read_lcb_frame;
            check_value(lcb_frame, IDLE_FRAME, ERROR, "IDLE frame is encoded as expected");
        end;


        -- masks HCC* chip with givern hcc_id
        procedure fill_hcc_mask(
            hcc_id : integer;
            inverse : boolean
        ) is
        begin
            for i in 0 to hcc_mask_i'length - 1 loop
                if inverse then
                    hcc_mask_i(i) <= '0' when i = hcc_id else '1';
                else
                    hcc_mask_i(i) <= '1' when i = hcc_id else '0';
                end if;
            end loop;
        end;


        -- masks ABC* chip with given hcc_id and abc_id
        procedure fill_abc_mask(
            hcc_id, abc_id : integer;
            inverse : boolean
        ) is
        begin
            for i in 0 to abc_mask_i'length - 1 loop
                for j in 0 to abc_mask_i(0)'length - 1 loop
                    if hcc_id = i then
                        if inverse then
                            abc_mask_i(i)(j) <= '0' when j = abc_id else '1';
                        else
                            abc_mask_i(i)(j) <= '1' when j = abc_id else '0';
                        end if;
                    else
                        abc_mask_i(i) <= (others => '0') when inverse = false else (others => '1');
                    end if;
                end loop;
            end loop;
        end;

        -- Issues IDLE command
        procedure issue_idle is
        begin
            log(ID_SEQUENCER_SUB, "-- Send command: IDLE");
            cmd_i <= LCB_CMD_IDLE;
            issue_command_when_ready;
        end;

        procedure issue_and_verify_idle is
        begin
            issue_idle;
            verify_idle;
        end;

        -- verifies the L0A is encoded correctly (and dequeues it)
        procedure verify_l0a(
            constant bcr    : std_logic                    := '0';
            constant mask   : std_logic_vector(3 downto 0) := (others => '0');
            constant l0_tag : std_logic_vector(6 downto 0) := (others => '0')
        ) is
        begin
            log(ID_SEQUENCER_SUB, "-- Verify: L0A frame, bcr = " & to_string(bcr)
                & ", mask = " & to_string(mask) & ", tag = " & to_string(l0_tag, HEX));
            read_lcb_frame;
            check_value(lcb_frame(12), '0', ERROR, "L0A frame: there is no comma");
            check_value(lcb_frame(11), bcr, ERROR, "L0A frame: BCR field equals expected value");
            check_value(lcb_frame(10 downto 7), mask, ERROR, "L0A frame: MASK field equals the expected value");
            check_value(lcb_frame(6 downto 0), l0_tag, ERROR, "L0A frame: TAG field equals the expected value");
        end;

        procedure issue_and_verify_l0a(
            constant bcr    : std_logic                    := '0';
            constant mask   : std_logic_vector(3 downto 0) := (others => '0');
            constant l0_tag : std_logic_vector(6 downto 0) := (others => '0')
        ) is
        begin
            issue_l0a(bcr, mask, l0_tag);
            verify_l0a(bcr, mask, l0_tag);
        end;

        -- Issues fast command
        procedure issue_fast_cmd(
            constant bc      : std_logic_vector(1 downto 0) := (others => '0');
            constant command : std_logic_vector(3 downto 0) := (others => '0')
        ) is
        begin
            log(ID_SEQUENCER_SUB, "-- Send command: FAST, bc = " & to_string(bc)
                    & ", command = " & to_string(command, HEX));
            cmd_i           <= LCB_CMD_FAST;
            fast_cmd_data_i <= bc & command;
            issue_command_when_ready;
        end;

        -- verifies the frame contents for the fast command
        procedure verify_fast_cmd(
            constant bc      : std_logic_vector(1 downto 0) := (others => '0');
            constant command : std_logic_vector(3 downto 0) := (others => '0')
        ) is
        begin
            log(ID_SEQUENCER_SUB, "-- Verify: fast command, bc = " & to_string(bc)
                    & ", command = " & to_string(command, HEX));
            read_lcb_frame;
            check_value(lcb_frame(12 downto 6), COMMA & K3, ERROR, "Fast command frame: has K3 comma");
            check_value(lcb_frame(5 downto 4), bc, ERROR, "Fast command frame: BC_SELECT field equals the expected value");
            check_value(lcb_frame(3 downto 0), command, ERROR, "Fast command frame: COMMAND field equals the expected value");
        end;

        procedure issue_and_verify_fast_cmd(
            constant bc      : std_logic_vector(1 downto 0) := (others => '0');
            constant command : std_logic_vector(3 downto 0) := (others => '0')
        ) is
        begin
            issue_fast_cmd(bc, command);
            verify_fast_cmd(bc, command);
        end;

        -- helper for verifying register read/write packets
        procedure verify_reg_start_stop_frame(
            constant start_not_stop : std_logic                    := '0';
            constant abc_not_hcc    : std_logic                    := '0';
            constant hcc_id         : std_logic_vector(3 downto 0) := (others => '0')
        ) is
            variable prefix    : string(1 to 6);
        begin
            if start_not_stop = '1' then
                prefix := "Start ";
            else
                prefix := "Stop  ";
            end if;
            read_lcb_frame;
            check_value(lcb_frame(12 downto 6), COMMA & K2, ERROR, prefix & "frame: has K2 comma");
            check_value(lcb_frame(5), abc_not_hcc, ERROR, prefix & "frame: ABC__NOT_HCC field has the expected value");
            check_value(lcb_frame(4), start_not_stop, ERROR, prefix & "frame: START__NOT_STOP flag has the expected value");
            check_value(lcb_frame(3 downto 0), hcc_id, ERROR, prefix & "frame: HCC_ID field has the expected value");
        end;

        -- helper for verifying header #2 of read/write packets
        procedure verify_reg_header_2(
            constant addr : std_logic_vector(7 downto 0) := (others => '0')
        ) is
        begin
            read_lcb_frame;
            check_value(lcb_frame(12 downto 12), NO_COMMA, ERROR, "Header frame #2: has no comma");
            check_value(lcb_frame(11 downto 7), MARKER, ERROR, "Header frame #2: has the register r/w command marker");
            check_value(lcb_frame(6 downto 1), addr(5 downto 0), ERROR, "Header frame #2: field REG_ADDR[5:0] has expected value");
        end;

        -- helper for verifying header #1 of read/write packets
        procedure verify_reg_header_1(
            constant r_not_w : std_logic                    := '0';
            constant abc_id  : std_logic_vector(3 downto 0) := (others => '0');
            constant addr    : std_logic_vector(7 downto 0) := (others => '0')
        ) is
        begin
            read_lcb_frame;
            check_value(lcb_frame(12 downto 12), NO_COMMA, ERROR, "Header frame #1: has no comma");
            check_value(lcb_frame(11 downto 7), MARKER, ERROR, "Header frame #1: has the register r/w command marker");
            check_value(lcb_frame(6), r_not_w, ERROR, "Header frame #1: READ__NOT_WRITE flag has the expected value");
            check_value(lcb_frame(5 downto 2), abc_id, ERROR, "Header frame #1: field ABC_ID has expected value");
            check_value(lcb_frame(1 downto 0), addr(7 downto 6), ERROR, "Header frame #1: field REG_ADDR[7:6] has expected value");
        end procedure verify_reg_header_1;

        -- verifies register read is encoded correctly
        procedure verify_reg_read(
            constant abc_not_hcc : std_logic;
            constant hcc_id  : std_logic_vector(3 downto 0) := (others => '0');
            constant abc_id  : std_logic_vector(3 downto 0) := (others => '0');
            constant addr    : std_logic_vector(7 downto 0) := (others => '0')
        ) is
            variable dest : string(1 to 3);
        begin
            dest := string'("ABC") when ?? abc_not_hcc else string'("HCC");
            log(ID_SEQUENCER_SUB, "-- Verify: Register read (" & dest & ") "
                & register_read_parameters(abc_not_hcc, hcc_id, abc_id, addr));
            verify_reg_start_stop_frame('1', abc_not_hcc, hcc_id);
            verify_reg_header_1('1', abc_id, addr);
            verify_reg_header_2(addr);
            verify_reg_start_stop_frame('0', abc_not_hcc, hcc_id);
        end procedure verify_reg_read;

        -- verifies register write is encoded correctly
        procedure verify_reg_write(
            constant abc_not_hcc : std_logic;
            constant hcc_id  : std_logic_vector(3 downto 0)  := (others => '0');
            constant abc_id  : std_logic_vector(3 downto 0)  := (others => '0');
            constant addr    : std_logic_vector(7 downto 0)  := (others => '0');
            constant data    : std_logic_vector(31 downto 0) := (others => '0')
        ) is
            variable dest : string(1 to 3);
        begin
            dest := string'("ABC") when ?? abc_not_hcc else string'("HCC");
            log(ID_SEQUENCER_SUB, "-- Verify: Register write (" & dest & ") "
                & register_write_parameters(abc_not_hcc, hcc_id, abc_id, addr, data));
            verify_reg_start_stop_frame('1', abc_not_hcc, hcc_id);
            verify_reg_header_1('0', abc_id, addr);
            verify_reg_header_2(addr);
            read_lcb_frame;
            check_value(lcb_frame(12 downto 7), NO_COMMA & MARKER, ERROR, "Data frame #1: no comma, has the register r/w command marker");
            check_value(lcb_frame(3 downto 0), data(31 downto 28), ERROR, "Data frame #1: field DATA[31:20] has the expected value");
            read_lcb_frame;
            check_value(lcb_frame(12 downto 7), NO_COMMA & MARKER, ERROR, "Data frame #2: no comma, has the register r/w command marker");
            check_value(lcb_frame(6 downto 0), data(27 downto 21), ERROR, "Data frame #2: field DATA[27:21] has the expected value");
            read_lcb_frame;
            check_value(lcb_frame(12 downto 7), NO_COMMA & MARKER, ERROR, "Data frame #3: no comma, has the register r/w command marker");
            check_value(lcb_frame(6 downto 0), data(20 downto 14), ERROR, "Data frame #3: field DATA[20:14] has the expected value");
            read_lcb_frame;
            check_value(lcb_frame(12 downto 7), NO_COMMA & MARKER, ERROR, "Data frame #4: no comma, has the register r/w command marker");
            check_value(lcb_frame(6 downto 0), data(13 downto 7), ERROR, "Data frame #4: field DATA[13:7] has the expected value");
            read_lcb_frame;
            check_value(lcb_frame(12 downto 7), NO_COMMA & MARKER, ERROR, "Data frame #5: no comma, has the register r/w command marker");
            check_value(lcb_frame(6 downto 0), data(6 downto 0), ERROR, "Data frame #5: field DATA[6:0] has the expected value");
            verify_reg_start_stop_frame('0', abc_not_hcc, hcc_id);
        end procedure verify_reg_write;

        -- Issues register read command
        procedure issue_reg_read(
            constant abc_not_hcc : std_logic;
            constant hcc_id  : std_logic_vector(3 downto 0) := (others => '0');
            constant abc_id  : std_logic_vector(3 downto 0) := (others => '0');
            constant addr    : std_logic_vector(7 downto 0) := (others => '0')
        ) is
            variable dest : string(1 to 3);
        begin
            dest := string'("ABC") when ?? abc_not_hcc else string'("HCC");
            log(ID_SEQUENCER_SUB, "-- Send command: Register read (" & dest & ") "
                & register_read_parameters(abc_not_hcc, hcc_id, abc_id, addr));
            cmd_i      <= LCB_CMD_ABC_REG_READ when ?? abc_not_hcc else LCB_CMD_HCC_REG_READ;
            abc_id_i   <= abc_id;
            hcc_id_i   <= hcc_id;
            reg_addr_i <= addr;
            issue_command_when_ready;
            wait until rising_edge(clk);
        end procedure issue_reg_read;

        procedure issue_and_verify_reg_read(
            constant abc_not_hcc : std_logic;
            constant hcc_id  : std_logic_vector(3 downto 0) := (others => '0');
            constant abc_id  : std_logic_vector(3 downto 0) := (others => '0');
            constant addr    : std_logic_vector(7 downto 0) := (others => '0')
        ) is
        begin
            issue_reg_read(abc_not_hcc, hcc_id, abc_id, addr);
            verify_reg_read(abc_not_hcc, hcc_id, abc_id, addr);
        end procedure issue_and_verify_reg_read;

        -- Issues register write command
        procedure issue_reg_write(
            constant abc_not_hcc  : std_logic;
            constant hcc_id   : std_logic_vector(3 downto 0)  := (others => '0');
            constant abc_id   : std_logic_vector(3 downto 0)  := (others => '0');
            constant addr     : std_logic_vector(7 downto 0)  := (others => '0');
            constant data : std_logic_vector(31 downto 0) := (others => '0')
        ) is
            variable dest : string(1 to 3);
        begin
            dest := string'("ABC") when ?? abc_not_hcc else string'("HCC");
            log(ID_SEQUENCER_SUB, "-- Send command: Register write (" & dest & ") "
                & register_write_parameters(abc_not_hcc, hcc_id, abc_id, addr, data));
            cmd_i      <= LCB_CMD_ABC_REG_WRITE when ?? abc_not_hcc else LCB_CMD_HCC_REG_WRITE;
            abc_id_i   <= abc_id;
            hcc_id_i   <= hcc_id;
            reg_addr_i <= addr;
            reg_data_i <= data;
            issue_command_when_ready;
            wait until rising_edge(clk);
        end procedure issue_reg_write;

        procedure issue_and_verify_reg_write(
            constant abc_not_hcc  : std_logic;
            constant hcc_id   : std_logic_vector(3 downto 0)  := (others => '0');
            constant abc_id   : std_logic_vector(3 downto 0)  := (others => '0');
            constant addr     : std_logic_vector(7 downto 0)  := (others => '0');
            constant reg_data : std_logic_vector(31 downto 0) := (others => '0')
        ) is
        begin
            issue_reg_write(abc_not_hcc, hcc_id, abc_id, addr, reg_data);
            verify_reg_write(abc_not_hcc, hcc_id, abc_id, addr, reg_data);
        end procedure issue_and_verify_reg_write;

        -- set RST to a given value and wait C_CYCLES_RST cycles
        procedure set_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
        end;

        procedure fill_buffer_and_verify(
            command : t_lcb_command
        ) is
            variable i_max : integer := 0;
            variable fast_slv : std_logic_vector(5 downto 0);
        begin
            fill_fifo : for i in 0 to over_l0a_max_depth loop
                if lcb_frame_fifo_almost_full_o = '1' or ready_o = '0' then
                    i_max := i;
                    exit;
                end if;
                case command is
                    when LCB_CMD_IDLE =>
                        issue_idle;
                    when LCB_CMD_FAST =>
                        fast_slv := std_logic_vector(to_unsigned(i, 6));
                        issue_fast_cmd(fast_slv(5 downto 4), fast_slv(3 downto 0));
                    when LCB_CMD_L0A =>
                        issue_l0a('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 7)));
                    when LCB_CMD_ABC_REG_READ =>
                        issue_reg_read('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 4)),
                                       std_logic_vector(to_unsigned(i, 8)));
                        wait_num_rising_edge(clk, 5);
                    when LCB_CMD_ABC_REG_WRITE =>
                        issue_reg_write('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 4)),
                                        std_logic_vector(to_unsigned(i, 8)), std_logic_vector(to_unsigned(i, 32)));
                        wait_num_rising_edge(clk, 10);
                    when LCB_CMD_HCC_REG_READ =>
                        issue_reg_read('0', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 4)),
                                       std_logic_vector(to_unsigned(i, 8)));
                        wait_num_rising_edge(clk, 5);
                    when LCB_CMD_HCC_REG_WRITE =>
                        issue_reg_write('0', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 4)),
                                        std_logic_vector(to_unsigned(i, 8)), std_logic_vector(to_unsigned(i, 32)));
                        wait_num_rising_edge(clk, 10);
                    when others =>
                        report "fill_buffer_and_verify: Unknown operation" severity FAILURE;
                end case;
                wait until rising_edge(clk);
            end loop;
            log(ID_SEQUENCER_SUB, "Written " & to_string(i_max) & " "
                & to_string(command) & " commands to the buffer");
            wait_num_rising_edge(clk, clk_wait_after_loop);
            check_value(ready_o, '0', ERROR, "LCB frame generator is not ready for the next command");
            check_value(lcb_frame_o_empty, '0', ERROR, "IDLE frames are enqueued in LCB frame buffer");

            verify_fifo : for i in 0 to i_max-1 loop
                case command is
                    when LCB_CMD_IDLE =>
                        verify_idle;
                    when LCB_CMD_FAST =>
                        fast_slv := std_logic_vector(to_unsigned(i, 6));
                        verify_fast_cmd(fast_slv(5 downto 4), fast_slv(3 downto 0));
                    when LCB_CMD_L0A =>
                        verify_l0a('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 7)));
                    when LCB_CMD_ABC_REG_READ =>
                        verify_reg_read('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 4)),
                                        std_logic_vector(to_unsigned(i, 8)));
                    when LCB_CMD_ABC_REG_WRITE =>
                        verify_reg_write('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 4)),
                                         std_logic_vector(to_unsigned(i, 8)), std_logic_vector(to_unsigned(i, 32)));
                    when LCB_CMD_HCC_REG_READ =>
                        verify_reg_read('0', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 4)),
                                        std_logic_vector(to_unsigned(i, 8)));
                    when LCB_CMD_HCC_REG_WRITE =>
                        verify_reg_write('0', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(i, 4)),
                                         std_logic_vector(to_unsigned(i, 8)), std_logic_vector(to_unsigned(i, 32)));
                    when others =>
                        report "fill_buffer_and_verify: Unknown operation" severity FAILURE;
                end case;
            end loop;
        end;

        -- issue and verify a given combination of commands
        procedure issue_and_verify(
            commands : t_lcb_command_array;
            delay_clk_cycles : natural := 0
        ) is
        begin
            for i in commands'range loop
                case commands(i) is
                    when LCB_CMD_IDLE =>
                        issue_idle;
                    when LCB_CMD_FAST =>
                        issue_fast_cmd("01", "0101");
                    when LCB_CMD_L0A =>
                        issue_l0a('0', "1000", "0011100");
                    when LCB_CMD_ABC_REG_READ =>
                        issue_reg_read('1', X"C", X"0", X"0B");
                    when LCB_CMD_ABC_REG_WRITE =>
                        issue_reg_write('1', X"5", X"2", x"43", X"BABEABBA");
                    when LCB_CMD_HCC_REG_READ =>
                        issue_reg_read('0', X"2", X"9", X"AA");
                    when LCB_CMD_HCC_REG_WRITE =>
                        issue_reg_write('0', X"3", X"A", x"31", X"CAFEBEEF");
                    when others =>
                        report "issue_and_verify: Unknown operation" severity FAILURE;
                end case;
            end loop;
            for i in commands'range loop
                if (delay_clk_cycles > 0) then
                    wait_num_rising_edge(clk, delay_clk_cycles);
                end if;
                case commands(i) is
                    when LCB_CMD_IDLE =>
                        verify_idle;
                    when LCB_CMD_FAST =>
                        verify_fast_cmd("01", "0101");
                    when LCB_CMD_L0A =>
                        verify_l0a('0', "1000", "0011100");
                    when LCB_CMD_ABC_REG_READ =>
                        verify_reg_read('1', X"C", X"0", X"0B");
                    when LCB_CMD_ABC_REG_WRITE =>
                        verify_reg_write('1', X"5", X"2", x"43", X"BABEABBA");
                    when LCB_CMD_HCC_REG_READ =>
                        verify_reg_read('0', X"2", X"9", X"AA");
                    when LCB_CMD_HCC_REG_WRITE =>
                        verify_reg_write('0', X"3", X"A", x"31", X"CAFEBEEF");
                    when others =>
                        report "issue_and_verify: Unknown operation" severity FAILURE;
                end case;
            end loop;
            wait until rising_edge(clk);
            check_value(lcb_frame_o_empty, '1', ERROR, "No stray frames are enqueued in LCB frame buffer");
        end;

    --------------------------------------------------------------------------------
    -- Actual test begins here
    --------------------------------------------------------------------------------

    begin
        clk_en <= true;
        disable_log_msg(ID_POS_ACK);

        log(ID_LOG_HDR, "Verify behavior when rst = 1");
        set_rst('1');
        check_value(ready_o, '0', ERROR, "ready_o = '0' when rst = '1'");

        -- Test IDLE commands
        log(ID_LOG_HDR, "Verify that IDLE commands are formed correctly");
        set_rst('0');
        for i in 0 to 4 loop
            issue_and_verify_idle;
        end loop;
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty after IDLE frames are dequeued");

        -- Test L0A commands
        log(ID_LOG_HDR, "Verify that L0A commands are formed correctly");
        issue_and_verify_l0a('1', "0101", "1111000");
        issue_and_verify_l0a('1', "1010", "1010101");
        issue_and_verify_l0a('1', "0000", "0000000");
        issue_and_verify_l0a('0', "1010", "1010101");
        issue_and_verify_l0a('0', "1111", "1111111");
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty after L0A frames are dequeued");

        log(ID_LOG_HDR, "Verify that invalid L0A commands are ignored");
        issue_l0a('0', "0000", "0000000");
        wait_num_rising_edge(clk, 10);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty, no invalid L0A commands are enqued");

        log(ID_LOG_HDR, "Verify that multiple L0A commands are enqueued and dequed in the correct order");
        issue_l0a('1', "1100", "1011100");
        issue_l0a('0', "1010", "1101011");
        issue_l0a('1', "0101", "0011110");
        verify_l0a('1', "1100", "1011100");
        verify_l0a('0', "1010", "1101011");
        verify_l0a('1', "0101", "0011110");
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty after L0A commands are dequeued");

        -- test fast commands
        log(ID_LOG_HDR, "Verify that fast commands are formed correctly");
        issue_and_verify_fast_cmd("10", "1100");
        issue_and_verify_fast_cmd("00", "0000");
        issue_and_verify_fast_cmd("11", "1111");
        issue_and_verify_fast_cmd("01", "1010");
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty after fast commands are dequeued");

        log(ID_LOG_HDR, "Verify that multiple valid fast commands are dequeued in the correct order");
        issue_fast_cmd("11", "1010");
        issue_fast_cmd("01", "1111");
        issue_fast_cmd("10", "0011");
        verify_fast_cmd("11", "1010");
        verify_fast_cmd("01", "1111");
        verify_fast_cmd("10", "0011");
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty after fast commands are dequeued");

        -- test register read and write commands (1 out of 2)
        log(ID_LOG_HDR, "Verify that register r/w are formed correctly");
        issue_and_verify_reg_read('1', "1100", "1010", X"15");
        issue_and_verify_reg_read('0', X"0", X"0", X"00");
        issue_and_verify_reg_read('1', X"F", X"F", X"FF");
        issue_and_verify_reg_read('0', "0000", "0111", X"42");
        issue_and_verify_reg_read('1', "1111", "1000", X"7A");
        issue_and_verify_reg_read('0', "1010", "0111", X"09");
        issue_and_verify_reg_write('1', "1100", "1010", X"15", X"FEEDBEEF");
        issue_and_verify_reg_write('0', X"0", X"0", X"00", X"00000000");
        issue_and_verify_reg_write('1', X"F", X"F", X"FF", X"FFFFFFFF");
        issue_and_verify_reg_write('0', "0000", "0111", X"42", X"ACCEDED0");
        issue_and_verify_reg_write('1', "1111", "1000", X"7A", X"BABEABBA");
        issue_and_verify_reg_write('0', "1010", "0111", X"09", X"DEAFFADE");
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty after register r/w commands are dequeued");


        -- test masking of HCC* chips
        log(ID_LOG_HDR, "Verify that HCC* register r/w commands are skipped for masked chips");
        for i in 0 to hcc_mask_i'length - 1 loop
            fill_hcc_mask(i, inverse => false);
            issue_reg_read('0', std_logic_vector(to_unsigned(i, 4)), "0000", X"23");
            issue_reg_write('0', std_logic_vector(to_unsigned(i, 4)), "0000", X"65", X"DEADBEEF");
        end loop;
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty");


        log(ID_LOG_HDR, "Verify that HCC* register r/w commands are not skipped for non-masked chips");
        for i in 0 to hcc_mask_i'length - 1 loop
            fill_hcc_mask(i, inverse => true);
            issue_and_verify_reg_read('0', std_logic_vector(to_unsigned(i, 4)), "0000", X"23");
            issue_and_verify_reg_write('0', std_logic_vector(to_unsigned(i, 4)), "0000", X"65", X"DEADBEEF");
        end loop;
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty");


        -- test masking of ABC* chips
        log(ID_LOG_HDR, "Verify that ABC* register r/w commands are skipped for masked chips");
        for i in 0 to abc_mask_i'length - 1 loop
            for j in 0 to abc_mask_i(0)'length - 1 loop
                fill_abc_mask(i, j, inverse => false);
                issue_reg_read('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(j, 4)), X"23");
                issue_reg_write('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(j, 4)), X"65", X"DEADBEEF");
            end loop;
        end loop;
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty");

        log(ID_LOG_HDR, "Verify that ABC* register r/w commands are not skipped for non-masked chips");
        for i in 0 to abc_mask_i'length - 1 loop
            for j in 0 to abc_mask_i(0)'length - 1 loop
                fill_abc_mask(i, j, inverse => true);
                fill_hcc_mask(i, inverse => true);
                issue_and_verify_reg_read('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(j, 4)), X"23");
                issue_and_verify_reg_write('1', std_logic_vector(to_unsigned(i, 4)), std_logic_vector(to_unsigned(j, 4)), X"65", X"DEADBEEF");
            end loop;
        end loop;
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty");

        hcc_mask_i <= (others => '0');
        abc_mask_i <= (others => (others => '0'));


        -- test register read and write commands (2 out of 2)
        log(ID_LOG_HDR, "Verify that multiple valid register r/w commands are dequeued in the correct order");
        issue_reg_read('0', "0000", "0111", X"23");
        issue_reg_read('1', "1001", "1010", X"A0");
        issue_reg_read('0', "1010", "0110", X"97");
        issue_reg_write('1', "1100", "1010", X"15", X"DADABAFF");
        issue_reg_write('0', X"8", X"2", X"10", X"DEAFDEED");
        issue_reg_write('1', X"3", X"6", X"AF", X"FACECAFE");
        verify_reg_read('0', "0000", "0111", X"23");
        verify_reg_read('1', "1001", "1010", X"A0");
        verify_reg_read('0', "1010", "0110", X"97");
        verify_reg_write('1', "1100", "1010", X"15", X"DADABAFF");
        verify_reg_write('0', X"8", X"2", X"10", X"DEAFDEED");
        verify_reg_write('1', X"3", X"6", X"AF", X"FACECAFE");
        wait until rising_edge(clk);
        check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty after register r/w commands are dequeued");


        -- Test that the commands don't get lost
        for cmd in t_lcb_command loop
            log(ID_LOG_HDR, "Verify that ready_o signal becomes low when too many " & to_string(cmd) & " commands are enqued");
            fill_buffer_and_verify(cmd);
            wait until rising_edge(clk);
            check_value(lcb_frame_o_empty, '1', FAILURE, "LCB frame buffer is empty after checking ready_o signal");
        end loop;


        -- Test that multiple commands sent in arbitrary order work correctly
        log(ID_LOG_HDR, "Check that any binary command combination results in the correct frames");
        for delay in delay_array'range loop
            for cmd1 in t_lcb_command loop
                for cmd2 in t_lcb_command loop
                    issue_and_verify((cmd1, cmd2), delay);
                end loop;
            end loop;
        end loop;

        -- Ending the simulation
        wait for 100 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;                           -- to stop completely
    end process;

end architecture Behavioral;
