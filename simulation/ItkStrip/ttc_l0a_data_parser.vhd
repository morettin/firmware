--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : Testbench helper module for sending L0A data to Strips modules
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : ttc_ttc_l0a_data_parser.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Wed May 28 20:00:40 2020
-- Last update : Wed May 28 20:00:40 2020
-- Platform    : Default Part Number
-- Standard    : <VHDL-2008>
--------------------------------------------------------------------------------
-- Copyright (c) 2019 BNL
-------------------------------------------------------------------------------
-- Description:
--
-- This module is not synthesizable.
--
-- Loads specified file containing bypass data
-- and returns it frame by frame
--
-------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use std.textio.all;
    use ieee.std_logic_textio.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

package ttc_l0a_data_parser is
    constant record_max_length : natural := 1000;

    type t_inputs_record is record
        bcr  : std_logic;
        mask : std_logic_vector(3 downto 0);
        tag  : std_logic_vector(6 downto 0);
    end record t_inputs_record;

    type t_frame_array is array (natural range 0 to record_max_length - 1) of std_logic_vector(15 downto 0);
    type t_inputs_array is array (natural range 0 to record_max_length - 1) of t_inputs_record;

    type t_l0a_data_record is record
        count   : natural;
        inputs  : t_inputs_array;
        outputs : t_frame_array;
    end record t_l0a_data_record;

    type t_ttc_l0a_file_parser is protected
    -- Loads text file with the provided name, parse and return the dataset
    impure function parse_file(
        file_name_inputs  : string;
        file_name_outputs : string
    ) return t_l0a_data_record;
	end protected t_ttc_l0a_file_parser;
end package ttc_l0a_data_parser;

package body ttc_l0a_data_parser is
    type t_ttc_l0a_file_parser is protected body
    file hfile                   : TEXT;
    variable fstatus             : file_open_status;
    variable buf                 : LINE;
    --variable ptr                 : integer;
    variable data_count          : natural := 0;
    variable line_ctr            : natural := 0;
    variable result              : t_l0a_data_record;
    variable good1, good2, good3 : boolean;
    --variable bcr_bit : bit;

    impure function parse_file(
        file_name_inputs  : string;
        file_name_outputs : string
    ) return t_l0a_data_record is
    begin
        -- loading module inputs from file
        log(ID_SEQUENCER, "Loading L0A frame data (inputs) from " & file_name_inputs & " ...");
        data_count := 0;
        file_open(fstatus, hfile, file_name_inputs, read_mode);
        if fstatus /= open_ok then
            error("Could not open the file: " & file_name_inputs);
        else
            line_ctr := 0;
            while not (endfile(hfile)) loop
                line_ctr := line_ctr + 1;
                readline(hfile, buf);
                if buf(1) = '-' then
						next;
                else
                    read(buf, result.inputs(data_count).bcr, good1);
                    readline(hfile, buf);
                    line_ctr := line_ctr + 1;
                    read(buf, result.inputs(data_count).mask, good2);
                    readline(hfile, buf);
                    line_ctr := line_ctr + 1;
                    read(buf, result.inputs(data_count).tag, good3);

                    if good1 and good2 and good3 then
                        data_count := data_count + 1;
                    else
                        error("Issue parsing file " & file_name_inputs & " line " & to_string(line_ctr));
                    end if;
                end if;
            end loop;
            log(ID_SEQUENCER, "Parsed file " & file_name_inputs & " successfully, loaded " & to_string(data_count) & " L0A frames (inputs)");
        end if;
        file_close(hfile);

        -- loading module outputs from file
        log(ID_SEQUENCER, "Loading L0A frame data (outputs) from " & file_name_outputs & " ...");
        data_count := 0;
        file_open(fstatus, hfile, file_name_outputs, read_mode);
        if fstatus /= open_ok then
            error("Could not open the file: " & file_name_outputs);
        else
            line_ctr := 0;
            while not (endfile(hfile)) loop
                line_ctr := line_ctr + 1;
                readline(hfile, buf);
                if buf(1) = '-' then
						next;
                else
                    hread(buf, result.outputs(data_count), good1);

                    if good1 then
                        data_count := data_count + 1;
                    else
                        error("Issue parsing file " & file_name_outputs & "line " & to_string(line_ctr));
                    end if;
                end if;
            end loop;
            log(ID_SEQUENCER, "Parsed file " & file_name_outputs & " successfully, loaded " & to_string(data_count) & " L0A frames (outputs)");
        end if;
        file_close(hfile);

        result.count := data_count;
        return result;
    end;
	end protected body;
end package body ttc_l0a_data_parser;
