--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use work.strips_package.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

entity tb_trickle_trigger is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_trickle_trigger(RTL)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity tb_trickle_trigger;

architecture RTL of tb_trickle_trigger is
    signal clk               : std_logic; -- 40 MHz BC clk
    signal clk_en            : boolean   := false;
    signal rst               : std_logic := '0'; -- synchronous reset
    signal en_i              : std_logic := '0'; -- enable trigger generation
    signal bcr_i             : std_logic := '0'; -- BCR from TTC module
    signal bc_start_i        : t_bcid    := (others => '0'); -- BC at which the trigger pulse start
    signal bc_stop_i         : t_bcid    := (others => '0'); -- BC at which the trigger pulse stops
    signal trickle_trigger_o : std_logic;
    signal trickle_allow_register_cmd_o : std_logic;

    constant C_CLK_PERIOD : time    := 25 ns; -- 40 MHz BC clk
    constant C_CYCLES_RST : integer := 3;
    constant bcid_max     : natural := 2**t_bcid'length - 1;
    constant guard_window : integer range 0 to 4095 := 16;

    signal pulse_checker_en       : boolean   := false;
    signal pulse_checker_done     : std_logic := '0';
    signal pulse_checker_ack      : std_logic := '0';
    signal expect_no_pulse      : boolean := false;
    constant pulse_checker_debug : boolean := false;

begin

    test : process is
        -- set RST to a given value and wait C_CYCLES_RST cycles
        procedure set_rst(
            value    : std_logic;
            duration : positive := C_CYCLES_RST
        ) is
        begin
            wait until rising_edge(clk);
            rst <= value;
            wait_num_rising_edge(clk, duration);
        end;

        procedure send_trigger_pulse(
            bc_start, bc_stop : t_bcid
        ) is
        begin
            bc_start_i <= bc_start;
            bc_stop_i <= bc_stop;
            bcr_i <= '1';
            wait until rising_edge(clk);
            bcr_i <= '0';
        end;

        -- prepare the pulse verification process and wait for its completion
        procedure verify_trigger_pulse(
            exp_bc_start, exp_bc_stop : t_bcid; -- @suppress "Unused declaration"
            no_pulse : boolean := false
        ) is
        begin
            expect_no_pulse <= no_pulse;
            pulse_checker_en <= true;
            await_value(pulse_checker_done, '1',
                10 * C_CLK_PERIOD, (bcid_max + 10)* C_CLK_PERIOD, ERROR,
                "Waiting for the pulse verification to finish"
            );
            pulse_checker_ack <= '1';
            wait until rising_edge(clk);
            pulse_checker_ack <= '0';
        end;

        procedure send_and_verify_trigger_pulse(
            bc_start, bc_stop : t_bcid;
            no_pulse : boolean := false
        ) is
        begin
            send_trigger_pulse(bc_start, bc_stop);
            verify_trigger_pulse(bc_start, bc_stop, no_pulse);
        end;

    begin
        clk_en <= true;
        en_i <= '1';
        disable_log_msg(ID_POS_ACK);


        log(ID_LOG_HDR, "When rst = 1 there is no trigger pulse");
        set_rst('1');
        send_and_verify_trigger_pulse(x"123", x"456", no_pulse => true);
        send_and_verify_trigger_pulse(x"076", x"62F", no_pulse => true);


        log(ID_LOG_HDR, "When en = 0 there is no trigger pulse");
        en_i <= '0';
        set_rst('0');
        send_and_verify_trigger_pulse(x"4AD", x"DAC", no_pulse => true);
        send_and_verify_trigger_pulse(x"231", x"F3F", no_pulse => true);


        log(ID_LOG_HDR, "Trigger pulse starts and ends as expected");
        en_i <= '1';
        --pulse_checker_debug <= true;
        send_and_verify_trigger_pulse(x"000", x"120");
        send_and_verify_trigger_pulse(x"FA0", x"FFF");
        send_and_verify_trigger_pulse(x"000", x"FFF");
        send_and_verify_trigger_pulse(x"002", x"1EA");
        send_and_verify_trigger_pulse(x"73A", x"BEC");
        send_and_verify_trigger_pulse(x"3A5", x"A58");
        send_and_verify_trigger_pulse(x"042", x"F00");


        log(ID_LOG_HDR, "When bc_start_i = bc_stop_i where is no trigger pulse");
        send_and_verify_trigger_pulse(x"024", x"024", no_pulse => true);
        send_and_verify_trigger_pulse(x"4FA", x"4FA", no_pulse => true);
        send_and_verify_trigger_pulse(x"6F6", x"6F6", no_pulse => true);


        log(ID_LOG_HDR, "There are no repeating pulses when there are no BCR signal");
        send_and_verify_trigger_pulse(x"002", x"1EA");
        verify_trigger_pulse(x"000", x"000", no_pulse => true);
        send_and_verify_trigger_pulse(x"73A", x"BEC");
        verify_trigger_pulse(x"000", x"000", no_pulse => true);
        send_and_verify_trigger_pulse(x"3A5", x"A58");
        verify_trigger_pulse(x"000", x"000", no_pulse => true);


        -- Ending the simulation
        wait for 100 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)
        log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);

        -- Finish the simulation
        uvvm_completed <= '1';
        wait for 1 us;
        clk_en <= false;
        wait;                           -- to stop completely
    end process;

    -- measure when trigger pulse starts and ends
    pulse_checker : process is
        variable pulse_start_int, pulse_stop_int : integer;
        variable register_pulse_stop_int : integer;
    begin
        if pulse_checker_en then
            log(ID_SEQUENCER_SUB, "Pulse checker is enabled");
            wait until rising_edge(clk);
            for i in 0 to bcid_max loop
                wait for C_CLK_PERIOD / 2;
                pulse_start_int := to_integer(unsigned(bc_start_i));
                pulse_stop_int := to_integer(unsigned(bc_stop_i));
                register_pulse_stop_int := pulse_stop_int - guard_window;

                if pulse_checker_debug then
                    log(ID_SEQUENCER_SUB, "i, start, stop = " &  -- @suppress "Dead code"
                        to_string(i) & ", " & to_string(pulse_start_int) &
                        ", " & to_string(pulse_stop_int)
                    );
                end if;
                -- check trickle trigger pulse
                if (i <= pulse_stop_int) and (i > pulse_start_int) and (not expect_no_pulse) then
                    check_value(trickle_trigger_o, '1', ERROR, "Verifying pulse timing (trickle trigger enable)");
                else
                    check_value(trickle_trigger_o, '0', ERROR, "Verifying pulse timing (trickle trigger enable)");
                end if;
                -- check trickle register command allow pulse
                if (i <= register_pulse_stop_int) and (i > pulse_start_int) and (not expect_no_pulse) then
                    check_value(trickle_allow_register_cmd_o, '1', ERROR, "Verifying pulse timing (register command enable)");
                else
                    check_value(trickle_allow_register_cmd_o, '0', ERROR, "Verifying pulse timing (register command enable)");
                end if;
                wait until rising_edge(clk);
            end loop;

            -- handshaking with the main test process
            pulse_checker_done <= '1';
            log(ID_SEQUENCER_SUB, "Pulse checker finished verifying the pulse");
            wait until rising_edge(pulse_checker_ack);
            pulse_checker_done <= '0';
        end if;
        wait until rising_edge(clk);
    end process;


    DUT : entity work.lcb_trickle_trigger
        generic map (guard_window => guard_window)
        port map(
            clk               => clk,
            rst               => rst,
            en_i              => en_i,
            bcr_i             => bcr_i,
            bc_start_i        => bc_start_i,
            bc_stop_i         => bc_stop_i,
            trickle_trigger_o => trickle_trigger_o,
            trickle_allow_register_cmd_o => trickle_allow_register_cmd_o
        );

    clock_generator(clk, clk_en, C_CLK_PERIOD, "40 MHz BC clock");

end architecture RTL;
