--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

library uvvm_util;
    context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
    use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_gpio;
    context bitvis_vip_gpio.vvc_context;

library bitvis_vip_axistream;
    context bitvis_vip_axistream.vvc_context;

library bitvis_vip_clock_generator;
    context bitvis_vip_clock_generator.vvc_context;

    use work.endeavour_package.all;
    use work.axi_stream_package.all;


entity tb_amac_decoder is
    generic(
        use_vunit: boolean := false -- @suppress "Unused generic: use_vunit is not used in work.tb_amac_decoder(func)"
    );
    port(
        uvvm_completed: out std_logic := '0'
    );
end entity;


architecture func of tb_amac_decoder is
    constant C_CLK_PERIOD          : time    := 25 ns;
    constant C_AXI_CLK_PERIOD      : time    := 15 ns;
    constant C_CYCLES_RST          : integer := 3;
    --constant C_STABLE_PERIODS      : integer := 10;
    --constant C_MAX_CYCLES_PER_BYTE : integer := 4;
    constant C_USER_WIDTH          : integer := 4;
    constant C_ID_WIDTH            : integer := 1;
    constant C_DEST_WIDTH          : integer := 1;
    constant C_DATA_WIDTH          : integer := 32;

    constant USER_OK : t_user_array(0 to 0) := (others => b"00000000");--@suppress
    constant USER_ERROR : t_user_array(0 to 0) := (others => b"00000010");--@suppress

    signal clk              : std_logic;
    signal m_axis_aclk      : std_logic;
    signal rst              : std_logic := '0';
    signal arst             : std_logic := '1';
    signal amac_signal      : std_logic_vector(0 downto 0) := (others => '0');

    signal data_axi_tohost : axis_32_type;

    signal decoder : t_axistream_if(tdata(C_DATA_WIDTH - 1 downto 0),--@suppress
                                    tkeep((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tuser(C_USER_WIDTH - 1 downto 0),
                                    tstrb((C_DATA_WIDTH / 8) - 1 downto 0),
                                    tid(C_ID_WIDTH - 1 downto 0),
                                    tdest(C_DEST_WIDTH - 1 downto 0)
                                   );

begin

    main_test : process

        -- send a byte in AMAC encoding
        procedure send_byte(
            data : std_logic_vector(7 downto 0);
            zero_bit_too_short : boolean := false;
            one_bit_too_short : boolean := false;
            bit_gap_too_short : boolean := false;
            do_await_completion : boolean := false
        ) is
        begin
            log(ID_SEQUENCER_SUB, "Sending byte 0x" & to_hstring(data) & " to AMAC decoder");
            for i in data'left downto data'right loop
                gpio_set(GPIO_VVCT, 4, "1", "Set AMAC signal to 1");--@suppress
                if data(i) = '1' then -- send '1' pulse to AMAC
                    if one_bit_too_short then
                        insert_delay(GPIO_VVCT, 4, (TICKS_DAH_MIN + TICKS_DIT_MAX) / 2 * C_CLK_PERIOD);--@suppress
                    else
                        insert_delay(GPIO_VVCT, 4, TICKS_DAH_MID * C_CLK_PERIOD);--@suppress
                    end if;
                else -- send '0' pulse to AMAC
                    if zero_bit_too_short then
                        insert_delay(GPIO_VVCT, 4, TICKS_DIT_MIN / 2 * C_CLK_PERIOD);--@suppress
                    else
                        insert_delay(GPIO_VVCT, 4, TICKS_DIT_MID * C_CLK_PERIOD);--@suppress
                    end if;
                end if;
                gpio_set(GPIO_VVCT, 4, "0", "Set AMAC signal to 0");--@suppress
                -- send gap between the pulses
                if bit_gap_too_short then
                    insert_delay(GPIO_VVCT, 4, TICKS_BITGAP_MIN / 2 * C_CLK_PERIOD);--@suppress
                else
                    insert_delay(GPIO_VVCT, 4, TICKS_BITGAP_MID * C_CLK_PERIOD);--@suppress
                end if;
            end loop;

            if do_await_completion then
                insert_delay(GPIO_VVCT, 4, TICKS_QUIESCENT*2 * C_CLK_PERIOD);--@suppress
                await_completion(GPIO_VVCT, 4, 8*(TICKS_DAH_MAX + TICKS_BITGAP_MAX) * C_CLK_PERIOD,--@suppress
                "Wait until GPIO is done sending the data");
            end if;
        end;

        -- send a random sequence of bytes to AMAC and verify the result is correct
        procedure send_and_verify_random_word(
            length_bytes : integer
        ) is
            variable bytes : t_slv_array(0 to 127)(7 downto 0);
            variable expected_data : std_logic_vector(C_DATA_WIDTH-1 downto 0);
            variable j: integer;
        begin
            -- save expected data to the buffer
            for i in 0 to length_bytes-1 loop
                bytes(i) := random(8);
                send_byte(bytes(i));
            end loop;
            insert_delay(GPIO_VVCT, 4, TICKS_QUIESCENT*2 * C_CLK_PERIOD);--@suppress

            await_completion(GPIO_VVCT, 4,--@suppress
            length_bytes * 8*(TICKS_DAH_MAX + TICKS_BITGAP_MAX) * C_CLK_PERIOD,--@suppress
            "Wait until GPIO is done sending the data");

            -- There is no easy way to make assertions about tlast field
            for i in 0 to length_bytes/4 - 1 loop
                expected_data := bytes(4*i) & bytes(4*i + 1) & bytes(4*i + 2) & bytes(4*i + 3);
                axistream_expect(AXISTREAM_VVCT, 3, expected_data, USER_OK,-- axistream_expect enforces little endian--@suppress
                "The decoder has expected data", ERROR);
            end loop;

            j := length_bytes/4;
            if (length_bytes mod 4) /= 0 then
                if (length_bytes mod 4) = 1 then
                    axistream_expect(AXISTREAM_VVCT, 3, bytes(4*j), USER_OK,--@suppress
                    "The decoder has expected data", ERROR);
                elsif (length_bytes mod 4) = 2 then
                    axistream_expect(AXISTREAM_VVCT, 3, std_logic_vector'(bytes(4*j) & bytes(4*j + 1)), USER_OK,--@suppress
                    "The decoder has expected data", ERROR);
                elsif (length_bytes mod 4) = 3 then
                    axistream_expect(AXISTREAM_VVCT, 3, std_logic_vector'(bytes(4*j) & bytes(4*j + 1) & bytes(4*j + 2)), USER_OK,--@suppress
                    "The decoder has expected data", ERROR);
                end if;

            end if;

            await_completion(AXISTREAM_VVCT, 3, length_bytes * 50 * C_CLK_PERIOD,--@suppress
            "Wait until AXI stream is done verifyring the data");
        end;

    begin
        await_uvvm_initialization(VOID);
        start_clock(CLOCK_GENERATOR_VVCT, 1, "Start clock generator (BC clock)");--@suppress
        start_clock(CLOCK_GENERATOR_VVCT, 2, "Start clock generator (AXI clock)");--@suppress

        -- Print the configuration to the log
        --report_global_ctrl(VOID);
        --report_msg_id_panel(VOID);

        disable_log_msg(ALL_MESSAGES);
        enable_log_msg(ID_LOG_HDR);
        enable_log_msg(ID_SEQUENCER);
        enable_log_msg(ID_SEQUENCER_SUB);
        --enable_log_msg(ID_UVVM_SEND_CMD);

        disable_log_msg(CLOCK_GENERATOR_VVCT, 1, ALL_MESSAGES);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 1, ID_BFM);--@suppress
        disable_log_msg(CLOCK_GENERATOR_VVCT, 2, ALL_MESSAGES);--@suppress
        enable_log_msg(CLOCK_GENERATOR_VVCT, 2, ID_BFM);--@suppress
        disable_log_msg(AXISTREAM_VVCT, 3, ALL_MESSAGES);--@suppress
        enable_log_msg(AXISTREAM_VVCT, 3, ID_BFM);--@suppress
        disable_log_msg(GPIO_VVCT, 4, ALL_MESSAGES);--@suppress
        --enable_log_msg(GPIO_VVCT, 4, ID_BFM);

        gen_pulse(rst, C_CYCLES_RST * C_CLK_PERIOD, "Reset DUT");

        log(ID_LOG_HDR, "Check that non-compliant waveforms decode with chunk error bit set", C_SCOPE);

        log(ID_SEQUENCER, "Sending a sequence with short bit gaps", C_SCOPE);
        send_byte(x"AA", bit_gap_too_short => true, do_await_completion => true);
        axistream_expect(AXISTREAM_VVCT, 3, x"AA", USER_ERROR, "The decoder has expected data", ERROR);--@suppress
        await_completion(AXISTREAM_VVCT, 3, 50 * C_CLK_PERIOD, "Wait until AXI stream is done verifyring the data");--@suppress

        log(ID_SEQUENCER, "Sending a sequence with short zero bit pulses", C_SCOPE);
        send_byte(x"BB", zero_bit_too_short => true, do_await_completion => true);
        axistream_expect(AXISTREAM_VVCT, 3, x"BB", USER_ERROR, "The decoder has expected data", ERROR);--@suppress
        await_completion(AXISTREAM_VVCT, 3, 50 * C_CLK_PERIOD, "Wait until AXI stream is done verifyring the data");--@suppress

        log(ID_SEQUENCER, "Sending a sequence with short one bit pulses", C_SCOPE);
        send_byte(x"6C", one_bit_too_short => true, do_await_completion => true);
        axistream_expect(AXISTREAM_VVCT, 3, x"6C", USER_ERROR, "The decoder has expected data", ERROR);--@suppress
        await_completion(AXISTREAM_VVCT, 3, 50 * C_CLK_PERIOD, "Wait until AXI stream is done verifyring the data");--@suppress

        log(ID_SEQUENCER, "Sending a sequence with pulses too long", C_SCOPE);
        send_byte(x"3A");
        gpio_set(GPIO_VVCT, 4, "1", "Set AMAC signal to 1");--@suppress
        insert_delay(GPIO_VVCT, 4, TICKS_DAH_MAX * 2 * C_CLK_PERIOD);--@suppress
        gpio_set(GPIO_VVCT, 4, "0", "Set AMAC signal to 1");--@suppress
        insert_delay(GPIO_VVCT, 4, TICKS_QUIESCENT * 2 * C_CLK_PERIOD);--@suppress
        await_completion(GPIO_VVCT, 4, 8*(TICKS_DAH_MAX + TICKS_BITGAP_MAX) * C_CLK_PERIOD,--@suppress
                "Wait until GPIO is done sending the data");
        axistream_expect(AXISTREAM_VVCT, 3, x"3A", USER_ERROR, "The decoder has expected data", ERROR);--@suppress

        log(ID_LOG_HDR, "Send some random data to the AMAC and verify the output (compliant waveform)", C_SCOPE);

        for j in 1 to 10 loop
            log(ID_SEQUENCER, "Sending " & to_string(j) & "-byte words", C_SCOPE);
            for i in 0 to 4 loop
                send_and_verify_random_word(length_bytes => j);
            end loop;
        end loop;


        wait for 1000 ns;                -- to allow some time for completion
        report_alert_counters(FINAL);   -- Report final counters and print conclusion for simulation (Success/Fail)

        uvvm_completed <= '1';
        wait for 1 us;
        stop_clock(CLOCK_GENERATOR_VVCT, 1, "Stop clock generator (BC clock)");--@suppress
        stop_clock(CLOCK_GENERATOR_VVCT, 2, "Stop clock generator (AXI clock)");--@suppress
        wait;
    end process;

    decoder.tdata <= data_axi_tohost.tdata;--@suppress
    decoder.tkeep <= data_axi_tohost.tkeep;--@suppress
    decoder.tstrb <= (others => '1');--@suppress
    decoder.tid <= (others => '1');--@suppress
    decoder.tdest <= (others => '1');--@suppress
    decoder.tuser <= data_axi_tohost.tuser;--@suppress
    decoder.tlast <= data_axi_tohost.tlast;--@suppress
    decoder.tvalid <= data_axi_tohost.tvalid;--@suppress

    arst <= not rst;

    DUT : entity work.EndeavourDecoder
        generic map(
            DEBUG_en => FALSE,
            VERSAL => false
        )
        port map
    (
            clk40 => clk,
            m_axis_aclk => m_axis_aclk, --: in std_logic;
            amac_signal => amac_signal(0), --: in std_logic_vector(32 downto 0);
            LinkAligned => '1', --: in std_logic;
            aresetn => arst, --: in std_logic;
            rst => rst,
            m_axis => data_axi_tohost, --m_axis(i,0), --: out axis_32_type;
            invert_polarity => '0',
            m_axis_tready => decoder.tready, --m_axis_tready(i,0) --: in std_logic;
            m_axis_prog_empty => open    );

    i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

    i_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 1,
            GC_CLOCK_NAME      => "Clock",
            GC_CLOCK_PERIOD    => C_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_CLK_PERIOD / 2
        )
        port map(
            clk => clk
        );

    i_axi_clock_generator_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_INSTANCE_IDX    => 2,
            GC_CLOCK_NAME      => "Clock",
            GC_CLOCK_PERIOD    => C_AXI_CLK_PERIOD,
            GC_CLOCK_HIGH_TIME => C_AXI_CLK_PERIOD / 2
        )
        port map(
            clk => m_axis_aclk
        );

    i_axistream_vvc_slave : entity bitvis_vip_axistream.axistream_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_PACKETINFO_QUEUE_COUNT_MAX, GC_AXISTREAM_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_VVC_IS_MASTER => false,
            GC_DATA_WIDTH    => C_DATA_WIDTH,
            GC_USER_WIDTH    => C_USER_WIDTH,
            GC_ID_WIDTH      => C_ID_WIDTH,
            GC_DEST_WIDTH    => C_DEST_WIDTH,
            GC_INSTANCE_IDX  => 3)
        port map(
            clk              => m_axis_aclk,
            axistream_vvc_if => decoder
        );

    i_gpio_vvc : entity bitvis_vip_gpio.gpio_vvc
        generic map( -- @suppress "Generic map uses default values. Missing optional actuals: GC_GPIO_BFM_CONFIG, GC_CMD_QUEUE_COUNT_MAX, GC_CMD_QUEUE_COUNT_THRESHOLD, GC_CMD_QUEUE_COUNT_THRESHOLD_SEVERITY, GC_RESULT_QUEUE_COUNT_MAX, GC_RESULT_QUEUE_COUNT_THRESHOLD, GC_RESULT_QUEUE_COUNT_THRESHOLD_SEVERITY"
            GC_DATA_WIDTH  => 1,
            GC_DEFAULT_LINE_VALUE => "0",
            GC_INSTANCE_IDX  => 4
        )
        port map(
            gpio_vvc_if => amac_signal
        );

end func;
