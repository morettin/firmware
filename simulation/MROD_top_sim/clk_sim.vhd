--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.pcie_package.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;


entity clk_sim is
  PORT (
    clk50        : out    std_logic;
    MReset      : out    std_logic;
    RClk        : out    std_logic;
    SClk        : out    std_logic;
    WrthClk     : out    std_logic;
    clk160      : out    std_logic;
    clk200      : out    std_logic;
    clk250      : out    std_logic;
    clk40       : out    std_logic;
    sys_reset_n : out    std_logic
  );
end Clk_sim;
 
 
 
architecture a0 of clk_sim is

  constant Tlhc   : time := 25000 ps;   --  40 MHz
  constant Tcsm   : time := 20000 ps;   --  50 MHz
  constant Tsys   : time := 12500 ps;   --  80 MHz
  constant Txta   : time := 10000 ps;   -- 100 MHz
  constant Txtb   : time :=  6250 ps;   -- 160 MHz
  constant Txtc   : time :=  4000 ps;   -- 250 MHz
  constant Txtd   : time :=  5000 ps;   -- 200 MHz

  signal MyClk0, MyClk1, MyClk2, MyClk3, MyClk4, MyClk5, MyClk6 : std_logic;
  signal MyRst1   : std_logic;

begin

  clk40 <= MyClk0;  -- 40 MHz
  clk50 <= MyClk1;   -- 50 MHz
  RClk <= MyClk2;   -- 80 MHz
  SClk <= MyClk3;   -- 100 MHz

  WrthClk <= MyClk2; -- 80MHz

  clk160 <= MyClk4;
  clk250 <= MyClk5;
  clk200 <= MyClk6;

  sys_reset_n <= not MyRst1;

  prRst:
  process (MyClk0)
  begin
    if (rising_edge(MyClk0)) then
      MReset <= MyRst1;
    end if;
  end process;

  ---------------------------------------------------------------------------

  MyRst1 <= '0', '1' after 10 ns, '0' after 85 ns;  --

  prClock0:
  process
  begin
    MyClk0 <= '0', '1' after Tlhc/2 ;
    wait for Tlhc;
  end process;

  prClock1:
  process
  begin
    MyClk1 <= '0', '1' after Tcsm/2 ;
    wait for Tcsm;
  end process;

  prClock2:
  process
  begin
    MyClk2 <= '0', '1' after Tsys/2 ;
    wait for Tsys;
  end process;

  prClock3:
  process
  begin
    MyClk3 <= '0', '1' after Txta/2 ;
    wait for Txta;
  end process;

  prClock4:
  process
  begin
    MyClk4 <= '0', '1' after Txtb/2 ;
    wait for Txtb;
  end process;

  prClock5:
  process
  begin
    MyClk5 <= '0', '1' after Txtc/2 ;
    wait for Txtc;
  end process;

  prClock6:
  process
  begin
    MyClk6 <= '0', '1' after Txtd/2 ;
    wait for Txtd;
  end process;

end architecture a0 ; -- of clk_sim
