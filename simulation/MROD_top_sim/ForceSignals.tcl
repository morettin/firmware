
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Rene
#               Thei Wijnen
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

relaunch_sim

add_force {/felix_mrod_top/u11/u10/TRXloopback} -radix bin {1 0ns}
#add_force {/felix_mrod_top/u11/u10/RX_CHxReset} -radix bin {0 0ns}
#add_force {/felix_mrod_top/u11/u10/TXCVR_ResetAll} -radix bin {0 0ns}
#add_force {/felix_mrod_top/u11/u10/TX_CHxReset} -radix bin {0 0ns}
add_force {/felix_mrod_top/u11/EnManSlide} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_CSMENABLE} -radix hex {1 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_CSMENABLE} -radix hex {f 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TRXLOOPBACK} -radix hex {f 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {f 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_RXRESET} -radix hex {0 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXRESET} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_CSMENABLE} -radix hex {f 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_CLRFIFOS} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_TRXLOOPBACK} -radix hex {f 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_TXCVRRESET} -radix hex {0 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_RXRESET} -radix hex {0 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_TXRESET} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_EMULOADENA} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_EMPTYSUPPR} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_HPTDCMODE} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_EMPTYSUPPR} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_HPTDCMODE} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_CLRFIFOS} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_EMULOADENA} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_CTRL.OPTIONS} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_CTRL.GOLTESTMODE} -radix hex {0 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {0 0ns}
add_force {/felix_mrod_top/u11/u10/SlideMax} -radix hex {08 0ns}
add_force {/felix_mrod_top/u11/u10/FrameSize} -radix hex {14 0ns}
add_force {/felix_mrod_top/u11/u10/SlideWait} -radix unsigned {32 0ns}
add_force {/felix_mrod_top/u11/u1/TXCVR_ResetAll} -radix hex {0 0ns}

#add_force {/felix_mrod_top/u12/\u2(0)\/u1/u16/MReset} -radix hex {1 0ns} -cancel_after 1us
add_force {/felix_mrod_top/u11/u10/MReset} -radix hex {1 0ns}
run 1 us
add_force {/felix_mrod_top/u11/u10/MReset} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_RXRESET} -radix hex {0 0ns}
add_force {/felix_mrod_top/prmap_app_control.MROD_EP1_TXRESET} -radix hex {0 0ns}
add_force {/felix_mrod_top/u11/u10/RX_CHxReset} -radix bin {0 0ns}

#start sending idles
run 20 us

#start simple count
add_force {/felix_mrod_top/u12/EnaTxCount} -radix hex {1 0ns}
run 5 us

#reset TXCVR
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {f 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {1 0ns}
#add_force {/felix_mrod_top/u11/u1/MReset_buf} -radix hex {1 0ns}
#run 1 us
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {0 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {0 0ns}
#add_force {/felix_mrod_top/u11/u1/MReset_buf} -radix hex {0 0ns}

#reset TXCVR
#add_force {/felix_mrod_top/u11/u1/MReset_buf} -radix hex {3 0ns}
#run 1 us
#add_force {/felix_mrod_top/u11/u1/MReset_buf} -radix hex {0 0ns}
#remove_forces { {/felix_mrod_top/u11/u1/TCVRall_rst} }

#reset TXCVR
#add_force {/felix_mrod_top/u11/u1/TCVRall_rst} -radix hex {3 0ns} -cancel_after 1us
#remove_forces { {/felix_mrod_top/u11/u1/TCVRall_rst} }

#reset TXCVR
#add_force {/felix_mrod_top/u12/\u2(0)\/u1/u16/MReset} -radix hex {1 0ns} -cancel_after 1us
add_force {/felix_mrod_top/u11/u10/MReset} -radix hex {1 0ns}
run 1 us
add_force {/felix_mrod_top/u11/u10/MReset} -radix hex {0 0ns}
#remove_forces { {/felix_mrod_top/u11/u1/TCVRall_rst} }

#Start alignment on running data
#reset TXCVR
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {1 0ns}

#add_force {/felix_mrod_top/u11/u1/TXCVR_ResetAll} -radix hex {1 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {1 0ns}
#run 1 us
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {0 0ns}
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {0 0ns}
#remove_forces { {/felix_mrod_top/u11/u1/TCVRall_rst} }

#run
run 50 us

#disable simple count
add_force {/felix_mrod_top/u12/EnaTxCount} -radix hex {0 0ns}
run 10 us

# test manual slide
#add_force {/felix_mrod_top/prmap_app_control.MROD_CTRL.OPTIONS} -radix hex {101 0ns}
#run 100 ns
#reset TXCVR
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {1 0ns}
#run 1 us
#add_force {/felix_mrod_top/prmap_app_control.MROD_EP0_TXCVRRESET} -radix hex {0 0ns}
#run 5 us
# end of reset

#add_force {/felix_mrod_top/u11/u1/rxslide_VIO} -radix hex {f 0ns}
#run 100 ns
#add_force {/felix_mrod_top/u11/u1/rxslide_VIO} -radix hex {0 0ns}
#run 1 us

#add_force {/felix_mrod_top/u11/u1/rxslide_VIO} -radix hex {f 0ns}
#run 100 ns
#add_force {/felix_mrod_top/u11/u1/rxslide_VIO} -radix hex {0 0ns}
#run 1 us

#add_force {/felix_mrod_top/u11/u1/rxslide_VIO} -radix hex {f 0ns}
#run 100 ns
#add_force {/felix_mrod_top/u11/u1/rxslide_VIO} -radix hex {0 0ns}
#run 1 us

#add_force {/felix_mrod_top/u11/u1/rxslide_VIO} -radix hex {f 0ns}
#run 1 us
#add_force {/felix_mrod_top/u11/u1/rxslide_VIO} -radix hex {0 0ns}
#run 1 us
