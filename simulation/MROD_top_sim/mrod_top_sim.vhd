--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.felix_mrod_top
--! =project    FELIX_MROD
--! @modified   Mon Apr 12 17:05:56 2021
--!-----------------------------------------------------------------------------

library ieee, work, UNISIM;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;
use UNISIM.VCOMPONENTS.all;

entity felix_mrod_top is
  generic(
    NUMBER_OF_INTERRUPTS            : integer := 8;
    NUMBER_OF_DESCRIPTORS           : integer := 2;
    OPTO_TRX                        : integer := 4;
    APP_CLK_FREQ                    : integer := 200;
    GBT_NUM                         : integer := 2; -- number of GBT channels
    NUMCH                           : integer := 2;
    MROD_GENERATE_REGS              : boolean := true;
    CARD_TYPE                       : integer := 712;
    USE_BACKUP_CLK                  : boolean := true; -- true to use 100/200 mhz board crystal, false to use TTC clock
    DEBUG_MODE                      : boolean := false; -- true debug ON
    GENERATE_GBT                    : boolean := false;
    GBT_MAPPING                     : integer := 0; -- GBT mapping: 0 NORMAL CXP1 -> GBT1-12 | 1 ALTERNATE CXP1 -> GBT 1-4,9-12,17-20
    GENERATE_XOFF                   : boolean := true; -- FromHost Xoff transmission enabled on Full mode busy
    GTHREFCLK_SEL                   : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
    STATIC_CENTRALROUTER            : boolean := true; -- removes update process from central router register map, only initial constant values are used
    PLL_SEL                         : std_logic := '1'; -- 0: CPLL, 1: QPLL
    AUTOMATIC_CLOCK_SWITCH          : boolean := true;
    toHostTimeoutBitn               : integer := 10;
    crInternalLoopbackMode          : boolean := false;
    serialConfig_mode               : boolean := false;
    useToFrontendGBTdataEmulator    : boolean := false;
    useToHostGBTdataEmulator        : boolean := false;
    TTC_test_mode                   : boolean := false;
    CREnableFromHost                : boolean := true;
    ENDPOINTS                       : integer := 2;
    FIRMWARE_MODE                   : integer := 1;
    BUILD_DATETIME                  : std_logic_vector(39 downto 0) := x"0000FE71CE";
    GIT_HASH                        : std_logic_vector(159 downto 0) := x"0000000000000000000000000000000000000000";
    GIT_TAG                         : std_logic_vector(127 downto 0) := x"00000000000000000000000000000000";
    GIT_COMMIT_NUMBER               : integer := 0;
    COMMIT_DATETIME                 : std_logic_vector(39 downto 0) := x"0000FE71CE";
    includeDirectMode               : boolean := false;
    USE_Si5324_RefCLK               : boolean := false;
    generateTTCemu                  : boolean := false;
    generate_IC_EC_TTC_only         : boolean := false;
    GENERATE_FEI4B                  : boolean := false;
    GENERATE_TRUNCATION_MECHANISM   : boolean := false;
    GENERATE_FM_WRAP                : boolean := false;
    BLOCKSIZE                       : integer := 1024;
    CHUNK_TRAILER_32B               : boolean := false;
    SUPER_CHUNK_FACTOR              : integer := 1;
    EnableFrHo_Egroup0Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup0Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup0Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup0Eproc8_8b10b  : boolean := false;
    EnableFrHo_Egroup1Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup1Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup1Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup1Eproc8_8b10b  : boolean := false;
    EnableFrHo_Egroup2Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup2Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup2Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup2Eproc8_8b10b  : boolean := false;
    EnableFrHo_Egroup3Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup3Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup3Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup3Eproc8_8b10b  : boolean := false;
    EnableFrHo_Egroup4Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup4Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup4Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup4Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup0Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup0Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup0Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup0Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup0Eproc16_8b10b : boolean := false;
    EnableToHo_Egroup1Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup1Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup1Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup1Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup1Eproc16_8b10b : boolean := false;
    EnableToHo_Egroup2Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup2Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup2Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup2Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup2Eproc16_8b10b : boolean := false;
    EnableToHo_Egroup3Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup3Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup3Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup3Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup3Eproc16_8b10b : boolean := false;
    EnableToHo_Egroup4Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup4Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup4Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup4Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup4Eproc16_8b10b : boolean := false);
  port (
    BUSY_OUT                  : out    std_logic;
    CLK40_FPGA2LMK_N          : out    std_logic;
    CLK40_FPGA2LMK_P          : out    std_logic;
    CLK_TTC_N                 : in     std_logic;
    CLK_TTC_P                 : in     std_logic;
    DATA_TTC_N                : in     std_logic;
    DATA_TTC_P                : in     std_logic;
    I2C_SMB                   : out    std_logic;
    I2C_SMBUS_CFG_nEN         : out    std_logic;
    I2C_nRESET                : out    std_logic;
    I2C_nRESET_PCIe           : out    std_logic;
    LMK_CLK                   : out    std_logic;
    LMK_DATA                  : out    std_logic;
    LMK_GOE                   : out    std_logic;
    LMK_LD                    : in     std_logic;
    LMK_LE                    : out    std_logic;
    LMK_SYNCn                 : out    std_logic;
    LOL_ADN                   : in     std_logic;
    LOS_ADN                   : in     std_logic;
    MGMT_PORT_EN              : out    std_logic;
    NT_PORTSEL                : out    std_logic_vector(2 downto 0);
    PCIE_PERSTn1              : out    std_logic;
    PCIE_PERSTn2              : out    std_logic;
    PEX_PERSTn                : out    std_logic;
    PEX_SCL                   : out    std_logic;
    PEX_SDA                   : inout  std_logic;
    PORT_GOOD                 : in     std_logic_vector(7 downto 0);
    Perstn1_open              : in     std_logic;
    Perstn2_open              : in     std_logic;
    Q2_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q2_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    Q4_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q4_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    Q5_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q5_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    Q6_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q6_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    Q8_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q8_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    RX_N                      : in     std_logic_vector(NUMCH*2-1 downto 0);
    RX_P                      : in     std_logic_vector(NUMCH*2-1 downto 0);
    SCL                       : inout  std_logic;
    SDA                       : inout  std_logic;
    SHPC_INT                  : out    std_logic;
    SI5345_A                  : out    std_logic_vector(1 downto 0);
    SI5345_INSEL              : out    std_logic_vector(1 downto 0);
    SI5345_OE                 : out    std_logic;
    SI5345_SEL                : out    std_logic;
    SI5345_nLOL               : in     std_logic;
    STN0_PORTCFG              : out    std_logic_vector(1 downto 0);
    STN1_PORTCFG              : out    std_logic_vector(1 downto 0);
    SmaOut_x3                 : out    std_logic;
    SmaOut_x4                 : out    std_logic;
    SmaOut_x5                 : out    std_logic;
    SmaOut_x6                 : out    std_logic;
    TACH                      : in     std_logic;
    TESTMODE                  : out    std_logic_vector(2 downto 0);
    TX_N                      : out    std_logic_vector(NUMCH*2-1 downto 0);
    TX_P                      : out    std_logic_vector(NUMCH*2-1 downto 0);
    UPSTREAM_PORTSEL          : out    std_logic_vector(2 downto 0);
    app_clk_in_n              : in     std_logic;
    app_clk_in_p              : in     std_logic;
    clk40_ttc_ref_out_n       : out    std_logic;
    clk40_ttc_ref_out_p       : out    std_logic;
    clk_ttcfx_ref1_in_n       : in     std_logic;
    clk_ttcfx_ref1_in_p       : in     std_logic;
    clk_ttcfx_ref2_in_n       : in     std_logic;
    clk_ttcfx_ref2_in_p       : in     std_logic;
    emcclk                    : in     std_logic;
    flash_SEL                 : out    std_logic;
    flash_a                   : out    std_logic_vector(24 downto 0);
    flash_a_msb               : inout  std_logic_vector(1 downto 0);
    flash_adv                 : out    std_logic;
    flash_cclk                : out    std_logic;
    flash_ce                  : out    std_logic;
    flash_d                   : inout  std_logic_vector(15 downto 0);
    flash_re                  : out    std_logic;
    flash_we                  : out    std_logic;
    opto_inhibit              : out    std_logic_vector(OPTO_TRX-1 downto 0);
    pcie_rxn                  : in     std_logic_vector(15 downto 0);
    pcie_rxp                  : in     std_logic_vector(15 downto 0);
    pcie_txn                  : out    std_logic_vector(15 downto 0);
    pcie_txp                  : out    std_logic_vector(15 downto 0);
    sys_clk0_n                : in     std_logic;
    sys_clk0_p                : in     std_logic;
    sys_clk1_n                : in     std_logic;
    sys_clk1_p                : in     std_logic;
    --sys_reset_n               : in     std_logic;
    uC_reset_N                : out    std_logic);
end entity felix_mrod_top;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.felix_mrod_top.a0
--! =project    FELIX_MROD
--! @modified   Mon Apr 12 17:05:56 2021
--!-----------------------------------------------------------------------------



architecture a0 of felix_mrod_top is

  signal pcie_txlon        : std_logic_vector(7 downto 0);
  signal pcie_txlop        : std_logic_vector(7 downto 0);
  signal pcie_txhip        : std_logic_vector(7 downto 0);
  signal pcie_txhin        : std_logic_vector(7 downto 0);
  signal pcie_rxlon        : std_logic_vector(7 downto 0);
  signal pcie_rxlop        : std_logic_vector(7 downto 0);
  signal pcie_rxhin        : std_logic_vector(7 downto 0);
  signal pcie_rxhip        : std_logic_vector(7 downto 0);
  signal LMK_Locked        : std_logic;
  signal rst_hw            : std_logic;
  signal clk80             : std_logic;
  signal clk160            : std_logic;
  signal clk250            : std_logic;
  signal clk200, clk200_p, clk200_n            : std_logic;
  signal clk_ttc_40_s      : std_logic;
  signal CDRlocked         : std_logic;
  signal clk10_xtal        : std_logic;
  signal clk40_xtal        : std_logic;
  signal MMCM_Locked       : std_logic;
  signal MMCM_OscSelect    : std_logic;
  signal rst0_soft_40      : std_logic;
  signal CR0_FIFO_Busy     : std_logic;
  signal CR1_FIFO_Busy     : std_logic;
  signal TTC_ToHost_Data   : TTC_ToHost_data_type;
  signal clk40             : std_logic;
  signal TTC_out           : std_logic_vector(15 downto 0);
  signal prmap_ttc_monitor : register_map_ttc_monitor_type;
  signal pcie0_appreg_clk  : std_logic;
  signal pcie0_soft_reset  : std_logic;
  signal prmap_hk_monitor  : register_map_hk_monitor_type;
  signal prmap_board_info  : register_map_gen_board_info_type;
  signal prmap0_40_control : register_map_control_type;
  signal lnk0_up           : std_logic;
  signal pcie0_DMA_Busy    : std_logic;
  signal pcie1_DMA_Busy    : std_logic;
  signal lnk1_up           : std_logic;
  signal u22_ChValid       : std_logic_vector(NUMCH-1 downto 0);
  signal u12_ChValid       : std_logic_vector(NUMCH-1 downto 0);
  signal u11_RxValid       : std_logic_vector(NUMCH-1 downto 0);
  signal u21_RxValid       : std_logic_vector(NUMCH-1 downto 0);
  signal u12_TxValid       : std_logic_vector(NUMCH-1 downto 0);
  signal u22_TxValid       : std_logic_vector(NUMCH-1 downto 0);
  signal prmap_app_control : register_map_control_type;
  signal u12_ChData        : slv33_array(0 to NUMCH-1);
  signal u22_ChData        : slv33_array(0 to NUMCH-1);
  signal u13_ChBusy        : std_logic_vector(NUMCH-1 downto 0);
  signal u23_ChBusy        : std_logic_vector(NUMCH-1 downto 0);
  signal u12_fhFifoRE      : std_logic;
  signal u13_fhFifoEmpty   : std_logic;
  signal u22_fhFifoRE      : std_logic;
  signal u23_fhFifoEmpty   : std_logic;
  signal u12_TxData        : slv33_array(0 to NUMCH-1);
  signal u22_TxData        : slv33_array(0 to NUMCH-1);
  signal u11_RxData        : slv32_array(0 to NUMCH-1);
  signal u21_RxData        : slv32_array(0 to NUMCH-1);
  signal u13_fhFifoValid   : std_logic;
  signal u23_fhFifoValid   : std_logic;
  signal u13_fhFifoD       : std_logic_vector(31 downto 0);
  signal u23_fhFifoD       : std_logic_vector(31 downto 0);
  signal u11_RX_N          : std_logic_vector(NUMCH-1 downto 0);
  signal u11_RX_P          : std_logic_vector(NUMCH-1 downto 0);
  signal u21_RX_N          : std_logic_vector(NUMCH-1 downto 0);
  signal u21_RX_P          : std_logic_vector(NUMCH-1 downto 0);
  signal u11_TX_N          : std_logic_vector(NUMCH-1 downto 0);
  signal u11_TX_P          : std_logic_vector(NUMCH-1 downto 0);
  signal u21_TX_P          : std_logic_vector(NUMCH-1 downto 0);
  signal u21_TX_N          : std_logic_vector(NUMCH-1 downto 0);
  signal CR0BusyOut        : std_logic_vector(NUMCH-1 downto 0);
  signal CR1BusyOut        : std_logic_vector(NUMCH-1 downto 0);
  signal clk50             : std_logic;
  signal QX_GTREFCLK_N     : std_logic_vector(4 downto 0);
  signal QX_GTREFCLK_P     : std_logic_vector(4 downto 0);
  signal u21_TxClk         : std_logic_vector(NUMCH-1 downto 0);
  signal u11_TxClk         : std_logic_vector(NUMCH-1 downto 0);
  signal BUSY_INTERRUPT    : std_logic;
  signal u11_RxClk         : std_logic_vector(NUMCH-1 downto 0);
  signal u21_RxClk         : std_logic_vector(NUMCH-1 downto 0);
  signal MasterBusy        : std_logic;
  signal Trx0Monitor       : regs_trx_monitor;
  signal Trx1Monitor       : regs_trx_monitor;
  signal CSM0Monitor       : regs_csm_monitor;
  signal CSM1Monitor       : regs_csm_monitor;

  signal sys_reset_n        : std_logic;
  
begin
  BUSY_OUT <= MasterBusy;

  u11: entity work.TrxBuffer(a0)
    generic map(
      NUMCH      => NUMCH,
      W_ENDPOINT => 0)
    port map(
      QX_GTREFCLK_N     => QX_GTREFCLK_N,
      QX_GTREFCLK_P     => QX_GTREFCLK_P,
      RXtoGTH_N         => u11_RX_N,
      RXtoGTH_P         => u11_RX_P,
      RxClk             => u11_RxClk,
      RxData            => u11_RxData,
      RxValid           => u11_RxValid,
      TXfrGTH_N         => u11_TX_N,
      TXfrGTH_P         => u11_TX_P,
      TrxMonitor        => Trx0Monitor,
      TxClk             => u11_TxClk,
      TxData            => u12_TxData,
      TxValid           => u12_TxValid,
      clk40             => clk40,
      clk50             => clk50,
      prmap_app_control => prmap_app_control,
      sys_reset_n       => sys_reset_n);

  u12: entity work.CSMHandler(a0)
    generic map(
      NUMCH      => NUMCH,
      W_ENDPOINT => 0)
    port map(
      CSMMonitor        => CSM0Monitor,
      ChBusy            => u13_ChBusy,
      ChData            => u12_ChData,
      ChValid           => u12_ChValid,
      RxClk             => u11_RxClk,
      RxData            => u11_RxData,
      RxValid           => u11_RxValid,
      TTC_out           => TTC_out,
      TxClk             => u11_TxClk,
      TxData            => u12_TxData,
      TxValid           => u12_TxValid,
      clk250            => clk250,
      clk40             => clk40,
      clk50             => clk50,
      clk80             => clk80,
      fhFifoD           => u13_fhFifoD,
      fhFifoEmpty       => u13_fhFifoEmpty,
      fhFifoRE          => u12_fhFifoRE,
      fhFifoValid       => u13_fhFifoValid,
      prmap_app_control => prmap_app_control,
      sys_reset_n       => sys_reset_n);

  u32: entity work.clk_sim(a0)
    port map(
      clk50           => clk50,
      MReset    => open,
      RClk    => open,
      SClk             => clk80,
      WrthClk            => open,
      clk160              => clk160,
      clk200               => clk200,
      clk250              => clk250,
      clk40               => clk40,
      sys_reset_n         => sys_reset_n);
  
  -- input to the Si5345 from Main MMCM 40 MHz
      OBUFDS_clk200 : OBUFDS
      generic map
        ( IOSTANDARD => "DEFAULT")
      port map (
        O  => clk200_p,
        OB => clk200_n,
        I  => clk200                   -- clock from 200MHz crystal
     --   I  => clk_ttcfx_ref_s --clk_ttc_40    -- clock from the TTC decoder module (phase aligned)
        );
        
  u34: entity work.GlueBox(a0)
    generic map(
      NUMCH => NUMCH)
    port map(
      Q2_CLK0_GTREFCLK_PAD_N_IN => clk200_n,
      Q2_CLK0_GTREFCLK_PAD_P_IN => clk200_p,
      Q4_CLK0_GTREFCLK_PAD_N_IN => clk200_n,
      Q4_CLK0_GTREFCLK_PAD_P_IN => clk200_p,
      Q5_CLK0_GTREFCLK_PAD_N_IN => clk200_n,
      Q5_CLK0_GTREFCLK_PAD_P_IN => clk200_p,
      Q6_CLK0_GTREFCLK_PAD_N_IN => clk200_n,
      Q6_CLK0_GTREFCLK_PAD_P_IN => clk200_p,
      Q8_CLK0_GTREFCLK_PAD_N_IN => clk200_n,
      Q8_CLK0_GTREFCLK_PAD_P_IN => clk200_p,
      QX_GTREFCLK_N             => QX_GTREFCLK_N,
      QX_GTREFCLK_P             => QX_GTREFCLK_P,
      RXA_N                     => u11_RX_N,
      RXA_P                     => u11_RX_P,
      RXB_N                     => u21_RX_N,
      RXB_P                     => u21_RX_P,
      RX_N                      => RX_N,
      RX_P                      => RX_P,
      TXA_N                     => u11_TX_N,
      TXA_P                     => u11_TX_P,
      TXB_N                     => u21_TX_N,
      TXB_P                     => u21_TX_P,
      TX_N                      => TX_N,
      TX_P                      => TX_P,
      pcie_rxhin                => pcie_rxhin,
      pcie_rxhip                => pcie_rxhip,
      pcie_rxlon                => pcie_rxlon,
      pcie_rxlop                => pcie_rxlop,
      pcie_rxn                  => pcie_rxn,
      pcie_rxp                  => pcie_rxp,
      pcie_txhin                => pcie_txhin,
      pcie_txhip                => pcie_txhip,
      pcie_txlon                => pcie_txlon,
      pcie_txlop                => pcie_txlop,
      pcie_txn                  => pcie_txn,
      pcie_txp                  => pcie_txp);

end architecture a0 ; -- of felix_mrod_top
