--TO DO: embed trigger in gbt_rx_data_120b once every X kHZ. For now simulation
--works only with internal triggering
--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Marco Trovato
--
-- Design Name:  felig_sim_top_bnl712
-- Version:    1.0
-- Date:    5/8/2019
--
-- Description:
--
-- Change Log:  V1.0 -
--
--==============================================================================
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;
--use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.function_lib.ALL;
    use work.sim_lib.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.FELIX_gbt_package.all;
    use work.centralRouter_package.all;
    use work.axi_stream_package.all;
    use work.type_lib.ALL;
--*SIM use work.pcie_package.all;

entity felig_sim_top_bnl712 is
end entity felig_sim_top_bnl712;

architecture Behavioral of felig_sim_top_bnl712 is
    constant GBT_NUM           : integer := 4;
    constant NUMELINKmax       : integer := 112;
    constant NUMEGROUPmax      : integer := 7;
    constant PROT_FELIG        : integer := 1; -- 0 for GBT, 1 for lpGBT. REGISTER in the future
    constant FIRMWARE_MODE     : integer := 11; -- 6 for GBT 11 for LPGBT
    --constant AUR_DEC           : integer := 1;
    signal   link_rx_flag_i          : std_logic_vector(GBT_NUM-1 downto 0)  := (others => '0');
    signal   link_tx_flag_i                    : std_logic_vector(GBT_NUM-1 downto 0)  := (others => '0');
    signal   rst_hw                           : std_logic                := '0';
    signal  pcie0_register_map_40_control     : register_map_control_type;
    signal   clk40_in                  : std_logic                := '0';
    signal   clk240_in                   : std_logic                := '0';
    signal   clk320_in                        : std_logic                := '0';
    signal   TX_120b_i                        : txrx120b_type(0 to GBT_NUM-1)      := (others => (others => '0'));
    signal   RX_120b_i                : txrx120b_type(0 to GBT_NUM-1)      := (others => (others => '0'));
    signal   linkValid_array        : std_logic_vector(GBT_NUM-1 downto 0)  := (others => '0');

    signal clk_xtal_40                        : std_logic            := '0';
    signal lane_control                       : array_of_lane_control_type(GBT_NUM-1 downto 0);
    signal lane_monitor                       : array_of_lane_monitor_type(GBT_NUM-1 downto 0);

    signal gbt_tx_reset                       : std_logic                  := '0';
    signal gbt_rx_reset                       : std_logic                  := '0';
    signal felix_reset                        : std_logic                  := '0';
    signal elink_sync                         : std_logic                  := '0';
    signal emu_reset                          : std_logic                  := '0';
    signal elink_control                      : lane_elink_control_array(0 to NUMELINKmax-1);--switched to maximum    ;
    signal emu_control                        : lane_emulator_control_array(0 to NUMEGROUPmax-1);--(4 downto 0)  ; switched to maximum
    signal elink_enable                       : std_logic_vector(NUMELINKmax-1 downto 0) := (others => '1');--08"; switched to maximum
    signal elink_output_width                 : array_of_slv_2_0(NUMELINKmax-1 downto 0) := (others => "111");
    --TO DO: understand why not 200 anymore? --X"0000000200";
    --link 9 activated
    --2hex per group
    --MT activatink link in group3
    signal l1a_trigger_count                  : integer                := 0;
    signal time_count                         : integer                := 0;
    signal time_count_40                      : integer                := 0;
    signal select_random_i                    : std_logic_vector(0 downto 0) := "0";
    signal fmemu_random_ram_addr_i            : std_logic_vector(9 downto 0) := (others => '0');
    signal fmemu_random_ram_i                 : bitfield_fmemu_random_ram_t_type := (others => (others => '0'));
    signal fmemu_random_control_i             : bitfield_fmemu_random_control_w_type := (others => (others => '0'));

    signal gt_txusrclk_i                      : std_logic_vector(GBT_NUM-1 downto 0);
    signal gt_rxusrclk_i                      : std_logic_vector(GBT_NUM-1 downto 0);
    --  signal link_tx_data_256b_array_i          : txrx256b_type(0 to GBT_NUM-1);
    --  signal link_tx_data_256b_array_tmp        : txrx256b_type(0 to GBT_NUM-1);
    signal link_tx_data_228b_array_tmp        : txrx228b_type(0 to GBT_NUM-1);
    signal link_rx_data_36b_array_i           : txrx36b_type(0 to GBT_NUM-1);
    signal link_rx_data_120b_array_tmp        : txrx120b_type(0 to GBT_NUM-1);
    signal lpgbt_rx_data_120b_array_tmp       : txrx120b_type(0 to GBT_NUM-1);

    signal lpgbt_tx_flag_i                    : std_logic_vector(GBT_NUM-1 downto 0);
    signal lpgbt_rx_flag_i                    : std_logic_vector(GBT_NUM-1 downto 0);

    signal GBT_DOWNLINK_USER_DATA              : txrx120b_type(0 to (GBT_NUM-1));
    signal GBT_UPLINK_USER_DATA                : txrx120b_type(0 to (GBT_NUM-1));
    signal GTH_FM_RX_33b_out                   : txrx33b_type(0 to (GBT_NUM-1));
    signal lpGBT_DOWNLINK_USER_DATA            : txrx224b_type(0 to GBT_NUM-1);
    signal lpGBT_DOWNLINK_IC_DATA              : txrx2b_type(0 to GBT_NUM-1);
    signal lpGBT_DOWNLINK_EC_DATA              : txrx2b_type(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_USER_DATA              : txrx32b_type(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_EC_DATA                : txrx2b_type(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_IC_DATA                : txrx2b_type(0 to GBT_NUM-1);
    --signal PROTOCOL      : std_logic := '1';
    signal DATARATE       : std_logic;-- := '1';
    signal FECMODE        : std_logic;-- := '0';
    signal LINKSconfig    : std_logic_vector(2 downto 0) := "110";
    --  signal NUMELINK       : std_logic_vector(2 downto 0) := "111";
    --  signal NUMEGROUP      : std_logic_vector(1 downto 0) := "11";
    --  signal ELINKdivEGROUP : std_logic_vector(1 downto 0) := "11";
    signal lpgbt_DOWNLINK_DATA_zeros : std_logic_vector(127 downto 0);
    signal input_width    : std_logic;
    signal MSB            : std_logic;
    signal data_format    : std_logic_vector(1 downto 0) := "01";
    signal reset_decoder  : std_logic;
    signal gbt_downlink0  : std_logic_vector(119 downto 0);
    signal lpgbt_downlink0: std_logic_vector(223 downto 0);
    signal aclk_tmp       : std_logic:= '0';
    signal clk40_in_b     : std_logic:= '0';
begin

    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    ----------------------------------------------LINK CONFIGURATION-----------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------

    DATARATE          <= '1'; -- for lpGBT. 0 = 5.12 Gbps, 1 = 10.24 Gbps. REGISTER in the future
    FECMODE           <= '0'; -- for lpGBT. 0 = FEC5, 1 = FEC12. REGISTER in the future
    LINKSconfig       <= '0' & DATARATE & FECMODE when FIRMWARE_MODE = 6 else
                         '1' & DATARATE & FECMODE when FIRMWARE_MODE = 11;

    MSB               <= '0'; --0 for LSB first, 1 for MSB first
    data_format       <= "01"; --00 for direct, 01 for 8b10b, 10 for AURORA
    input_width       <= '0' when data_format = "00" else '1';--0 for 8b, 1 for 10b

    --PROTOCOL, DATARATE, FEC. 0.X,X = 4.8 Gbps    (GBT   Phase1) 5 Egroups  40 links,
    --                         1,0.0 = 5.12 FEC5   (lpGBT Phase2) 7 Egroups  56 links,
    --                         1,0.1 = 5.12 FEC12  (lpGBT Phase2) 6 Egroups  48 links,
    --                         1.1,0 = 10.24 FEC5  (lpGBT Phase2) 7 Egroups 112 links,
    --                         1.1,1 = 10.24 FEC12 (lpGBT Phase2) 6 Egroups  96 links

    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    ----------------------------------------------------CLOCKS-----------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------

    clk40_in    <= clk_xtal_40;        -- xtal is synchronous in this simulation case to 240.
    clk40_in_b   <= not clk40_in_b after 12.5ns;
    --RL clocks were not synchronos for some reason
    gen_clk_LPGBT: if PROT_FELIG = 1 generate
        signal count_clk40    : std_logic_vector(2 downto 0) := "000";
        signal clk40from320   : std_logic := '0';
        signal aclk40from320   : std_logic := '0';
    begin
        clk320_in     <= not clk320_in after 1.5625ns;
        clk240_in     <= '0';
        clk_xtal_40   <= clk40from320;
        gen_clk40fromclk320: process(clk320_in)
        begin
            if rising_edge(clk320_in) then
                aclk_tmp <= not aclk_tmp;
            end if;
            if count_clk40 = "111" then
                count_clk40 <= "000";
                clk40from320 <= not clk40from320;
            else
                count_clk40 <= count_clk40 + "001";
            end if;
        end process gen_clk40fromclk320;
    end generate gen_clk_LPGBT;

    gen_clk_GBT: if PROT_FELIG = 0 generate
        signal count_clk40    : std_logic_vector(2 downto 0) := "000";
        signal clk40from240   : std_logic := '0';
    begin
        clk320_in     <= '0';
        clk240_in     <= not clk240_in after 2.083ns;
        clk_xtal_40   <= clk40from240;
        aclk_tmp      <= '0';
        gen_clk40fromclk240: process(clk240_in)
        begin
            if count_clk40 = "101" then
                count_clk40 <= "000";
                clk40from240 <= not clk40from240;
            else
                count_clk40 <= count_clk40 + "001";
            end if;
        end process gen_clk40fromclk240;
    end generate gen_clk_GBT;

    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------REGISTERS---------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------

    pcie0_register_map_40_control.GBT_GENERAL_CTRL            <= (others => '0');
    pcie0_register_map_40_control.GBT_GTTX_RESET      <= (others => felix_reset);
    pcie0_register_map_40_control.GBT_GTRX_RESET      <= (others => felix_reset);
    pcie0_register_map_40_control.GBT_PLL_RESET.CPLL_RESET    <= (others => felix_reset);
    pcie0_register_map_40_control.GBT_PLL_RESET.QPLL_RESET    <= (others => felix_reset);
    pcie0_register_map_40_control.GBT_TX_TC_DLY_VALUE1    <= (others => '0');
    pcie0_register_map_40_control.GBT_TX_TC_DLY_VALUE2    <= (others => '0');
    pcie0_register_map_40_control.GBT_TX_TC_DLY_VALUE3    <= (others => '0');
    pcie0_register_map_40_control.GBT_TX_TC_DLY_VALUE4    <= (others => '0');
    pcie0_register_map_40_control.GBT_DATA_TXFORMAT1    <= (others => '0');
    pcie0_register_map_40_control.GBT_DATA_RXFORMAT1    <= (others => '0');
    pcie0_register_map_40_control.GBT_DATA_TXFORMAT2    <= (others => '0');
    pcie0_register_map_40_control.GBT_DATA_RXFORMAT2    <= (others => '0');
    pcie0_register_map_40_control.GBT_TX_RESET      <= (others => felix_reset);
    pcie0_register_map_40_control.GBT_RX_RESET      <= (others => felix_reset);
    pcie0_register_map_40_control.GBT_TX_TC_METHOD      <= (others => '0');
    pcie0_register_map_40_control.GBT_TC_EDGE      <= (others => '0');
    pcie0_register_map_40_control.GBT_OUTMUX_SEL      <= (others => '0');
    pcie0_register_map_40_control.GBT_MODE_CTRL.DESMUX_USE_SW  <= (others => '0');
    pcie0_register_map_40_control.GBT_MODE_CTRL.RX_ALIGN_SW  <= (others => '0');
    pcie0_register_map_40_control.GBT_MODE_CTRL.RX_ALIGN_TB_SW  <= (others => '0');

    pcie0_register_map_40_control.FELIG_GLOBAL_CONTROL.FAKE_L1A_RATE <= x"0000190";
    pcie0_register_map_40_control.FMEMU_RANDOM_CONTROL.SELECT_RANDOM <= "0";
    pcie0_register_map_40_control.FMEMU_RANDOM_RAM_ADDR <= "0000000000";
    pcie0_register_map_40_control.FMEMU_RANDOM_RAM.WE <= "0";
    pcie0_register_map_40_control.FMEMU_RANDOM_RAM.CHANNEL_SELECT <= (others=>'0');
    pcie0_register_map_40_control.FMEMU_RANDOM_RAM.DATA <= (others=>'0');
    --pcie0_register_map_40_control.FMEMU_RANDOM_CONTROL.SELECT_RANDOM <= "0";
    pcie0_register_map_40_control.FMEMU_RANDOM_CONTROL.SEED <= (others=>'0');
    pcie0_register_map_40_control.FMEMU_RANDOM_CONTROL.POLYNOMIAL <= (others=>'0');
    pcie0_register_map_40_control.ENCODING_REVERSE_10B <= "0";


    --  lane_control.global.lane_reset        <= '0';
    gen_lane_control : for i in lane_control'range generate
        lane_control(i).global.framegen_reset    <= '0';
        lane_control(i).global.elink_sync    <= elink_sync;
        lane_control(i).global.framegen_data_select  <= '0';
        lane_control(i).global.emu_data_select    <= '1';
        lane_control(i).global.l1a_source    <= '0';
        lane_control(i).global.loopback_fifo_delay  <= "00010";
        lane_control(i).global.loopback_fifo_reset  <= '0';
        lane_control(i).global.a_ch_bit_sel    <= "0000001";
        lane_control(i).global.b_ch_bit_sel    <= "0000010";
        lane_control(i).global.MSB                <= MSB;
        lane_control(i).clock.gth_tx_pi_hold    <= '0';
        lane_control(i).clock.picxo_offset_en    <= '0';
        lane_control(i).clock.picxo_offset_ppm    <= (others => '0');
        lane_control(i).gbt.rxslide_select    <= '0';
        lane_control(i).gbt.rx_reset      <= gbt_rx_reset;
        lane_control(i).gbt.tc_edge      <= '0';
        lane_control(i).gbt.tx_reset      <= gbt_tx_reset;
        lane_control(i).gbt.tx_tc_method      <= '0';
        lane_control(i).gbt.rx_data_format    <= "00";
        lane_control(i).gbt.tx_data_format    <= "00";
        lane_control(i).gbt.tx_tc_dly_value    <= X"0";
        lane_control(i).gbt.loopback_enable    <= '0';
        lane_control(i).gbt.rxslide_count_reset    <= '0';
        lane_control(i).gth.manual_gth_rxreset    <= '0';
        lane_control(i).gth.txpolarity      <= '0';
        lane_control(i).gth.rxpolarity      <= '0';
        lane_control(i).gth.loopback_enable    <= '0';
        lane_control(i).emulator          <= emu_control;
        lane_control(i).elink              <= elink_control;
    end generate gen_lane_control;

    gen_emu_control : for i in emu_control'range generate
        emu_control(i).pattern_select  <= "00";
        emu_control(i).data_format    <= data_format;
        emu_control(i).sw_busy      <= '0';
        emu_control(i).reset      <= '0';--emu_reset;
        --    emu_control(i).chunk_length    <= X"0040";
        emu_control(i).chunk_length    <= X"003C";  --MT
        --    => 76 payload (N.B: max for elink = 400(40Mhz/100kHz)/5(10b/2belink) =
        --    80, I chose 76 to account for SOP/EOP and a couple of idles
        emu_control(i).userdata      <= X"ABCD";
    --  emu_control(i).pack_gen_select  <= '1';  --RC: 1 = NSW generator / 0 = default generator
    --  emu_control(i).nsw_even_parity  <= '1';
    end generate gen_emu_control;

    --RL: restructured
    --"000=2b, 001=4b 010=8b 011=16b 100=32b";

    --emu_control(0).output_width  <= "000";--2-bit
    --emu_control(1).output_width  <= "000";--2-bit
    --emu_control(2).output_width  <= "001";--4-bit
    --emu_control(3).output_width  <= "001";--4-bit
    --emu_control(4).output_width  <= "010";--8-bit

    emu_control(0).output_width  <= "000";-- 2b--"000";--2-bit
    emu_control(1).output_width  <= "001";-- 4b--"000";--2-bit
    emu_control(2).output_width  <= "010";-- 8b--"001";--4-bit
    emu_control(3).output_width  <= "010";-- 8b--"001";--4-bit
    emu_control(4).output_width  <= "011";--16b--"010";--8-bit
    emu_control(5).output_width  <= "011";--16b--"010";--8-bit
    emu_control(6).output_width  <= "100";--32b--"011";--16-bit
    --  emu_control(0).output_width  <= "100";--32b
    --  emu_control(1).output_width  <= "100";--32b
    --  emu_control(2).output_width  <= "100";--32b
    --  emu_control(3).output_width  <= "100";--32b
    --  emu_control(4).output_width  <= "100";--32b
    --  emu_control(5).output_width  <= "100";--32b
    --  emu_control(6).output_width  <= "100";--32b
    --  emu_control(0).output_width  <= "011";--8b
    --  emu_control(1).output_width  <= "011";--8b
    --  emu_control(2).output_width  <= "011";--8b
    --  emu_control(3).output_width  <= "011";--16b
    --  emu_control(4).output_width  <= "011";--16b
    --  emu_control(5).output_width  <= "011";--16b
    --  emu_control(6).output_width  <= "011";--16b
    --  emu_control(0).output_width  <= "001";--4b
    --  emu_control(1).output_width  <= "001";--4b
    --  emu_control(2).output_width  <= "001";--4b
    --  emu_control(3).output_width  <= "001";--4b
    --  emu_control(4).output_width  <= "001";--4b
    --  emu_control(5).output_width  <= "001";--4b
    --  emu_control(6).output_width  <= "001";--4b

    gen_elink_en_out_width: for i in emu_control'range generate
        less_that_five : if i<5 generate
            elink_enable(i*16+7 downto i*16)    <= X"FF" when (emu_control(i).output_width <= "000") else
                                                   X"55" when (emu_control(i).output_width <= "001") else
                                                   X"11" when (emu_control(i).output_width <= "010") else
                                                   X"01" when (emu_control(i).output_width <= "011") else
                                                   X"01" when (emu_control(i).output_width <= "100");
            elink_enable(i*16+15 downto i*16+8) <= X"FF" when (emu_control(i).output_width <= "000" and LINKSconfig(2 downto 1) = "11") else
                                                   X"55" when (emu_control(i).output_width <= "001" and LINKSconfig(2 downto 1) = "11") else
                                                   X"11" when (emu_control(i).output_width <= "010" and LINKSconfig(2 downto 1) = "11") else
                                                   X"01" when (emu_control(i).output_width <= "011" and LINKSconfig(2 downto 1) = "11") else
                                                   X"00";
        --      elink_enable(i*16+15 downto i*16+8) <= X"FF" when (emu_control(i).output_width <= "000") else
        --                                             X"55" when (emu_control(i).output_width <= "001") else
        --                                             X"11" when (emu_control(i).output_width <= "010") else
        --                                             X"01" when (emu_control(i).output_width <= "011") else
        --                                             X"00";
        end generate less_that_five;
        five : if i=5 generate
            elink_enable(i*16+7 downto i*16)    <= X"FF" when (emu_control(i).output_width <= "000" and LINKSconfig(2) = '1') else
                                                   X"55" when (emu_control(i).output_width <= "001" and LINKSconfig(2) = '1') else
                                                   X"11" when (emu_control(i).output_width <= "010" and LINKSconfig(2) = '1') else
                                                   X"01" when (emu_control(i).output_width <= "011" and LINKSconfig(2) = '1') else
                                                   X"01" when (emu_control(i).output_width <= "100" and LINKSconfig(2) = '1') else
                                                   X"00";
            elink_enable(i*16+15 downto i*16+8) <= X"FF" when (emu_control(i).output_width <= "000" and LINKSconfig(2 downto 1) = "11") else
                                                   X"55" when (emu_control(i).output_width <= "001" and LINKSconfig(2 downto 1) = "11") else
                                                   X"11" when (emu_control(i).output_width <= "010" and LINKSconfig(2 downto 1) = "11") else
                                                   X"01" when (emu_control(i).output_width <= "011" and LINKSconfig(2 downto 1) = "11") else
                                                   X"00";
        end generate five;
        six : if i=6 generate
            elink_enable(i*16+7 downto i*16)    <= X"FF" when (emu_control(i).output_width <= "000" and LINKSconfig(2) = '1' and LINKSconfig(0) = '0') else
                                                   X"55" when (emu_control(i).output_width <= "001" and LINKSconfig(2) = '1' and LINKSconfig(0) = '0') else
                                                   X"11" when (emu_control(i).output_width <= "010" and LINKSconfig(2) = '1' and LINKSconfig(0) = '0') else
                                                   X"01" when (emu_control(i).output_width <= "011" and LINKSconfig(2) = '1' and LINKSconfig(0) = '0') else
                                                   X"01" when (emu_control(i).output_width <= "100" and LINKSconfig(2) = '1' and LINKSconfig(0) = '0') else
                                                   X"00";
            elink_enable(i*16+15 downto i*16+8) <= X"FF" when (emu_control(i).output_width <= "000" and LINKSconfig = "110") else
                                                   X"55" when (emu_control(i).output_width <= "001" and LINKSconfig = "110") else
                                                   X"11" when (emu_control(i).output_width <= "010" and LINKSconfig = "110") else
                                                   X"01" when (emu_control(i).output_width <= "011" and LINKSconfig = "110") else
                                                   X"00";
        end generate six;
        width : for j in 0 to 7 generate
            elink_output_width(i*16+j)   <= emu_control(i).output_width;
            elink_output_width(i*16+j+8) <= emu_control(i).output_width when (LINKSconfig(2 downto 1) = "11") else "111";
        end generate width;
    end generate gen_elink_en_out_width;

    gen_elink_control : for i in elink_control'range generate
        elink_control(i).input_width  <= input_width;
        elink_control(i).endian_mode  <= '0';
        elink_control(i).enable      <= elink_enable(i);
        elink_control(i).output_width   <= elink_output_width(i);
    end generate gen_elink_control;

    sim_proc : process(gt_txusrclk_i(0))
    begin
        if gt_txusrclk_i(0)'event and gt_txusrclk_i(0)='1' then
            time_count <= time_count + 1;
            if(time_count = 0) then
                lane_control(0).global.l1a_counter_reset    <= '0';
                reset_decoder <= '1';
            elsif(time_count = 200) then
                felix_reset <= '1';
            elsif(time_count = 400) then
                felix_reset <= '0';
                reset_decoder <= '0';
            elsif(time_count = 800) then
                rst_hw <= '1';
            elsif(time_count = 1200) then
                rst_hw <= '0';
            --      elsif(time_count = 1600) then
            --        elink_sync <= '1';
            --      elsif(time_count = 1800) then
            --        elink_sync <= '0';
            elsif(time_count = 4000) then
                lane_control(0).global.lane_reset  <= '1';
            elsif(time_count = 4200) then
                lane_control(0).global.lane_reset  <= '0';
            elsif(time_count = 4400) then
                gbt_rx_reset  <= '1';
            elsif(time_count = 4600) then
                gbt_rx_reset  <= '0';
            elsif(time_count = 4800) then
                emu_reset <= '1';
            elsif(time_count = 5000) then
                emu_reset <= '0';
            --      elsif(time_count = 5200) then
            --        elink_sync <= '1';
            --      elsif(time_count = 5400) then
            --        elink_sync <= '0';
            --      elsif(time_count = 10200) then
            --        elink_sync <= '1';
            --      elsif(time_count = 10400) then
            --        elink_sync <= '0';
            elsif(time_count = 5600) then
                gbt_tx_reset <= '1';
            elsif(time_count = 5800) then
                gbt_tx_reset <= '0';
            --      elsif(time_count = 6500) then
            --        lane_control(0).global.l1a_counter_reset  <= '1';
            --      elsif(time_count = 6700) then
            --        lane_control(0).global.l1a_counter_reset    <= '0';
            end if;
        end if;
    end process;

    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    -------------------------------------------------LINK  WRAPPER-------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------


    TXRXDATA_inst: for I in 0 to GBT_NUM - 1 generate
        link_rx_data_120b_array_tmp(I)  <= GBT_UPLINK_USER_DATA(I)         when PROT_FELIG = 0 else
                                           lpgbt_rx_data_120b_array_tmp(I) when PROT_FELIG = 1;
        lpgbt_rx_data_120b_array_tmp(I)(119 downto 36) <= (others=>'0');
        lpgbt_rx_data_120b_array_tmp(I)( 35 downto  0) <= lpGBT_UPLINK_IC_DATA(I) & lpGBT_UPLINK_EC_DATA(I) & lpGBT_UPLINK_USER_DATA(I);

        GBT_DOWNLINK_USER_DATA(I)   <= link_tx_data_228b_array_tmp(I)(119 downto 0);
        lpGBT_DOWNLINK_USER_DATA(I) <= link_tx_data_228b_array_tmp(I)(223 downto 0);
        lpGBT_DOWNLINK_EC_DATA(I)   <= link_tx_data_228b_array_tmp(I)(225 downto 224);
        lpGBT_DOWNLINK_IC_DATA(I)   <= link_tx_data_228b_array_tmp(I)(227 downto 226);
    end generate TXRXDATA_inst;
    lpgbt_DOWNLINK_DATA_zeros <= (others => '0');
    gbt_downlink0 <= GBT_DOWNLINK_USER_DATA(0);
    lpgbt_downlink0 <= lpGBT_DOWNLINK_USER_DATA(0);

    linkwrapper0: entity work.link_wrapper_FELIG
        generic map(
            GBT_NUM => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE,
            sim_emulator => true)
        port map(
            register_map_control => pcie0_register_map_40_control,
            register_map_link_monitor => open,
            clk40 => clk40_in,
            clk240 => clk240_in,
            clk40_xtal => clk_xtal_40,
            GTREFCLK_N_in => (others=>'0'),
            GTREFCLK_P_in => (others=>'0'),
            rst_hw => rst_hw,
            OPTO_LOS => (others=>'0'),
            RXUSRCLK_OUT => gt_rxusrclk_i,
            GBT_DOWNLINK_USER_DATA => GBT_DOWNLINK_USER_DATA,
            GBT_UPLINK_USER_DATA => GBT_UPLINK_USER_DATA,
            lpGBT_DOWNLINK_USER_DATA => lpGBT_DOWNLINK_USER_DATA,
            lpGBT_DOWNLINK_IC_DATA => lpGBT_DOWNLINK_IC_DATA,
            lpGBT_DOWNLINK_EC_DATA => lpGBT_DOWNLINK_EC_DATA,
            lpGBT_UPLINK_USER_DATA => lpGBT_UPLINK_USER_DATA,
            lpGBT_UPLINK_EC_DATA => lpGBT_UPLINK_EC_DATA,
            lpGBT_UPLINK_IC_DATA => lpGBT_UPLINK_IC_DATA,
            LinkAligned => linkValid_array,
            TX_P => open,
            TX_N => open,
            RX_P => (others=>'0'),
            RX_N => (others=>'0'),
            GTH_FM_RX_33b_out => GTH_FM_RX_33b_out,
            LMK_P => (others=>'0'),
            LMK_N => (others=>'0'),
            link_rx_flag_i=>link_rx_flag_i,
            link_tx_flag_i=>link_tx_flag_i,
            TXUSRCLK_OUT => gt_txusrclk_i,
            appreg_clk => '0',
            CLK40_FPGA2LMK_N => open,
            CLK40_FPGA2LMK_P => open,
            LMK_LD => '0',
            RESET_TO_LMK => open,
            clk40_rxusrclk => open,
            clk320_in => clk320_in,
            LINKSconfig => LINKSconfig);

    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    -----------------------------------------------EMULATOR  WRAPPER-----------------------------------------------
    ---------------------------------------------------------------------------------------------------------------
    ---------------------------------------------------------------------------------------------------------------

    --  FELIG_lane_wrapper_comp : entity work.FELIG_lane_wrapper
    EmulatorWrapper_comp : entity work.EmulatorWrapper
        generic map(
            GBT_NUM                     => GBT_NUM,
            NUMELINKmax                 => NUMELINKmax,
            NUMEGROUPmax                => NUMEGROUPmax
        )
        port map (
            clk_xtal_40                => clk_xtal_40      ,
            gt_txusrclk_in              => gt_txusrclk_i,
            gt_rxusrclk_in              => gt_rxusrclk_i,
            gt_rx_clk_div_2_mon         => open,
            gt_tx_clk_div_2_mon         => open,
            l1a_trigger_out            => open,
            link_tx_data_228b_array_out  => link_tx_data_228b_array_tmp,
            link_rx_data_120b_array_in   => link_rx_data_120b_array_tmp,

            link_tx_flag_in              => link_tx_flag_i, --gbt_tx_flag_i,
            link_rx_flag_in              => link_rx_flag_i, -- gbt_rx_flag_i,
            lane_control      => lane_control,
            lane_monitor      => open, --lane_monitor,
            register_map_control_40xtal => pcie0_register_map_40_control,
            link_frame_locked_array_in   => linkValid_array,
            LINKSconfig       => LINKSconfig
        );

---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
------------------------------------------------AURORA DECODING------------------------------------------------
---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------

--gen_aurora_decoding: if AUR_DEC = 1 generate

--felig_sim_64b66b_decoding : entity work.felig_sim_64b66b_decoding
--  port map (
--      clk40_in                  => clk40_in,
--      clk320_in                 => clk320_in,
--      aclk_tmp                  => aclk_tmp,
--      reset_decoder             => reset_decoder,
--      lpGBT_DOWNLINK_USER_DATA  => lpGBT_DOWNLINK_USER_DATA(0 to 0),
--      lpGBT_DOWNLINK_EC_DATA    => lpGBT_DOWNLINK_EC_DATA(0 to 0),
--      lpGBT_DOWNLINK_IC_DATA    => lpGBT_DOWNLINK_IC_DATA(0 to 0),
--      MSB                       => MSB
--  );

--end generate gen_aurora_decoding;

end architecture Behavioral;
