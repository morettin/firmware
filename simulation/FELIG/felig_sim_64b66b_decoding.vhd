--Adapted from Decoding_pixel_tb_noUVVM.vhd,originally written by Marco Trovato

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;
--use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.function_lib.ALL;
    use work.sim_lib.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.FELIX_gbt_package.all;
    use work.centralRouter_package.all;
    use work.axi_stream_package.all;
    use work.type_lib.ALL;

entity felig_sim_64b66b_decoding is
    generic(
        toHostTimeoutBitn         : integer := 16;
        STREAMS_TOHOST            : integer := 30;
        LINK                      : integer :=0
    );
    port (
        clk40_in                  : in std_logic;
        clk320_in                 : in std_logic;
        aclk_tmp                  : in std_logic;
        reset_decoder             : in std_logic;
        lpGBT_DOWNLINK_USER_DATA  : in txrx224b_type(0 to 0);
        lpGBT_DOWNLINK_EC_DATA    : in txrx2b_type(0 to 0);
        lpGBT_DOWNLINK_IC_DATA    : in txrx2b_type(0 to 0);
        MSB                       : in std_logic
    );
end felig_sim_64b66b_decoding;

architecture Behavioral of felig_sim_64b66b_decoding is


    signal clk40                          : std_logic:= '0';                                  --input
    signal reset                          : std_logic_vector(4 downto 0) := (others => '0');  --input
    signal lpGBT_UPLINK_USER_DATA_FOSEL   : txrx224b_type(0 to 0);                            --input
    signal LinkAligned_FOSEL              : std_logic_vector(0 downto 0);                     --input
    signal DecoderAligned                 : std_logic_vector(STREAMS_TOHOST-3 downto 0);      --out
    signal m_axis                         : axis_32_array_type(0 to STREAMS_TOHOST-1);        --out
    signal m_axis_tready                  : axis_tready_array_type(0 to STREAMS_TOHOST-1);    --input
    signal aclk                           : std_logic:= '0';                                  --input
    signal m_axis_prog_empty              : axis_tready_array_type(0 to STREAMS_TOHOST-1);    --out
    signal register_map_control           : register_map_control_type;                        --input

    --  signal PCIE_ENDPOINT : integer := 0;
    --  signal clk40_tmp    : std_logic:= '0';
    --  signal clk40_en     : boolean   := false;
    --  constant clk40_period : time := 25 ns;
    --  constant aclk_period : time := 6.25 ns;
    --  signal aclk_en        : boolean   := false;
    signal aclk_tmp_b : std_logic := '0';
    --  signal   ElinkData0 : std_logic_vector(31 downto 0) := (others => '0'); --egroup0
    --  signal   ElinkData1 : std_logic_vector(31 downto 0) := (others => '0');
    --  signal   ElinkData2 : std_logic_vector(31 downto 0) := (others => '0');
    --  signal   ElinkData3 : std_logic_vector(31 downto 0) := (others => '0');
    --  signal   ElinkData4 : std_logic_vector(31 downto 0) := (others => '0');
    --  signal   ElinkData5 : std_logic_vector(31 downto 0) := (others => '0');
    --  signal   ElinkData6 : std_logic_vector(31 downto 0) := (others => '0'); --egroup6
    --  signal LinkData : std_logic_vector(223 downto 0) := (others=>'0');
    signal LinkAligned : std_logic_vector(0 downto 0) := "0";

    --  --signal debug_flag: std_logic;

    --  signal fileopen: std_logic := '0';
    --  signal fileopen_dly: std_logic := '0';
    --  signal countfilecycle : integer range 0 to 10 := 0;

    --  type array_4x4b is array (0 to 3) of std_logic_vector(3 downto 0);
    --  signal trigid  : std_logic_vector(4 downto 0) := (others => '0');
    --  signal trigtag : std_logic_vector(4 downto 0) := (others => '0');
    --  signal bcid    : std_logic_vector(14 downto 0) := (others => '0');
    --  signal isHDR   : std_logic := '0';
    --  signal col     : std_logic_vector(5 downto 0) := (others => '0');
    --  signal row     : std_logic_vector(8 downto 0) := (others => '0');
    --  signal onebside : std_logic := '0';
    --  signal tot     : array_4x4b := (others => (others => '0'));

    --  signal data32_ref : std_logic_vector(31 downto 0) := (others => '0');
    --  signal isErr: std_logic := '0';

    signal timeCnt_max    : std_logic_vector ((toHostTimeoutBitn-1) downto 0);
    signal timeCnt_ena    : std_logic;
    signal chFIFO_din32     : std_logic_vector(31 downto 0);
    signal chFIFO_din32_valid: std_logic;
    signal s_axis: axis_32_array_type(0 to STREAMS_TOHOST-1);
    signal s_axis_tready: axis_tready_array_type(0 to STREAMS_TOHOST-1);
    signal s_axis_prog_empty: axis_tready_array_type(0 to STREAMS_TOHOST-1);

    signal CBOPT   : std_logic_vector(3 downto 0); --to be included in register map
    signal DISLANE : std_logic_vector(6 downto 0);
    signal mask_k_char : std_logic_vector(3 downto 0);


    signal emuToHost_lpGBTdata: std_logic_vector(223 downto 0);
    signal emuToHost_lpGBTECdata: std_logic_vector(1 downto 0);
    signal emuToHost_lpGBTICdata: std_logic_vector(1 downto 0);
    signal emuToHost_GBTlinkValid : std_logic;
    signal lpGBT_UPLINK_EC_DATA_FOSEL        : txrx2b_type(0 to 0);
    signal lpGBT_UPLINK_IC_DATA_FOSEL        : txrx2b_type(0 to 0);
    signal lpGBT_UPLINK_USER_DATA : txrx224b_type(0 to 0);
    signal lpGBT_UPLINK_EC_DATA                : txrx2b_type(0 to 0);
    signal lpGBT_UPLINK_IC_DATA                : txrx2b_type(0 to 0);
    signal count_lal : std_logic_vector(7 downto 0) := (others => '0');  --input
    signal new_tx: std_logic;
    signal count_tx : std_logic_vector(2 downto 0) := "100";
    signal aresetn : std_logic := '0';
begin

    --registers    --only link 0 (hit data), 1 (DCS) , egroup0
    register_map_control.DECODING_LINK_CB(0).CBOPT <= x"3";--  register_map_control.DECODING_MASK64B66BKBLOCK <= x"3"; --mask autoread
    register_map_control.DECODING_DISEGROUP <= "1111100"; --disable lane 2,3
    register_map_control.DECODING_EGROUP_CTRL(0)(0).PATH_ENCODING <= x"00000033";
    register_map_control.DECODING_EGROUP_CTRL(0)(0).EPATH_ENA <= x"03";
    register_map_control.TIMEOUT_CTRL.TIMEOUT(15 downto 0) <= x"00FF"; --x"FFFF";FF seems a good compromize. Test prog_full and write a slide on how toblock works and the timeout in both streamselect and tobloc
    register_map_control.TIMEOUT_CTRL.ENABLE <= "1";
    timeCnt_max <= register_map_control.TIMEOUT_CTRL.TIMEOUT((toHostTimeoutBitn-1) downto 0);
    timeCnt_ena <= to_sl(register_map_control.TIMEOUT_CTRL.ENABLE);
    CBOPT <= register_map_control.DECODING_LINK_CB(0).CBOPT;
    DISLANE <= register_map_control.DECODING_DISEGROUP;
    mask_k_char <= register_map_control.DECODING_MASK64B66BKBLOCK;
    register_map_control.GBT_TOHOST_FANOUT.SEL(0 downto 0) <= "1"; --felig_data
    register_map_control.FE_EMU_ENA.EMU_TOHOST(0) <= '1';

    g_LPGBT_Egroups: for egroup in 1 to 6 generate
    begin
        register_map_control.DECODING_EGROUP_CTRL(0)(egroup).PATH_ENCODING <= (others => '0');
        register_map_control.DECODING_EGROUP_CTRL(0)(egroup).EPATH_ENA <= (others => '0');
    end generate;

    --clock
    clk40 <= clk40_in;
    aclk_tmp_b <= not aclk_tmp_b after 3.125ns;
    aclk <= aclk_tmp;

    rst_proc: process(clk40)
        variable dopulse : std_logic := '1';
    begin
        if rising_edge(clk40) then
            if(dopulse = '1' and reset(0) = '0') then
                reset(0) <= '1';
                dopulse := '1';
            elsif(dopulse = '1' and reset(0) = '1') then
                reset(0) <= '0';
                dopulse := '0';
            else
                reset(0) <= reset_decoder;--not fileopen after 12.5ns;
            end if;
            reset(1) <= reset(0);
            reset(2) <= reset(1);
            reset(3) <= reset(2);
            reset(4) <= reset(3);
        end if;
    end process;

    linkalign_proc: process(new_tx)--link_tx_flag_i(0)
    --linkalign_proc: process(link_tx_flag_i(0))--
    begin
        if falling_edge(new_tx) then
            --if falling_edge(link_tx_flag_i(0)) then
            if count_lal = "00100000" then
                LinkAligned(0) <= '1';
            else
                count_lal <= count_lal + "00000001";
            end if;
        end if;
    end process;

    count8: process(clk320_in)
    begin
        if rising_edge(clk320_in) then
            if count_tx = "111" then
                new_tx <='1';
                count_tx <= "000";
            else
                new_tx <= '0';
                count_tx <=  count_tx + "001";
            end if;
        end if;
    end process;

    --    variable count : integer range 0 to 10 := 0;
    --  begin
    --    if rising_edge(clk40) then
    --      count := count+1;
    --      if(count = 10) then
    --        LinkAligned(0) <= '1';
    --      end if;
    --    end if;
    --  end process;

    --do I need this?
    --  gbtEmuToHost0: entity work.GBTdataEmulator
    --    generic map(
    --      EMU_DIRECTION      => "ToHost",
    --      FIRMWARE_MODE      => FIRMWARE_MODE_PIXEL
    --      )
    --    port map(
    --      clk40                => clk40,
    --      wrclk                => clk40,
    --      rst_hw               => reset(1),
    --      rst_soft             => reset(1),
    --      xoff                 => '0',
    --      register_map_control => register_map_control,
    --      GBTdata              => open,
    --      lpGBTdataToFE        => open,
    --      lpGBTdataToHost      => emuToHost_lpGBTdata,
    --      lpGBTECdata          => emuToHost_lpGBTECdata,
    --      lpGBTICdata          => emuToHost_lpGBTICdata,
    --      GBTlinkValid         => emuToHost_GBTlinkValid);

    --copy_lpgbt: process(clk40)
    --begin
    lpGBT_UPLINK_USER_DATA(0) <= lpGBT_DOWNLINK_USER_DATA(0);
    lpGBT_UPLINK_EC_DATA(0) <= lpGBT_DOWNLINK_EC_DATA(0);
    lpGBT_UPLINK_IC_DATA(0) <= lpGBT_DOWNLINK_IC_DATA(0);
    --end process;
    gbtFoSelToHost0: entity work.GBT_fanout_selector
        generic map(
            GBT_NUM            => 1
        )
        port map(
            GBTDataIn => (others => (others => '0')),
            lpGBTDataToFEIn => (others => (others => '0')),
            lpGBTDataToHostIn => lpGBT_UPLINK_USER_DATA(0 to 0),
            lpGBTECDataIn => lpGBT_UPLINK_EC_DATA(0 to 0),
            lpGBTICDataIn => lpGBT_UPLINK_IC_DATA(0 to 0),
            GBTLinkValidIn => LinkAligned(0 downto 0),
            EMU_GBTlinkValidIn => emuToHost_GBTlinkValid,
            EMU_GBTDataIn => (others => '0'),
            EMU_lpGBTDataToFEIn => (others => '0'),
            EMU_lpGBTDataToHostIn => emuToHost_lpGBTdata,
            EMU_lpGBTECDataIn => emuToHost_lpGBTECdata,
            EMU_lpGBTICDataIn => emuToHost_lpGBTICdata,
            GBTDataOut => open,
            lpGBTDataToFEOut => open,
            lpGBTDataToHostOut => lpGBT_UPLINK_USER_DATA_FOSEL,
            lpGBTECDataOut => lpGBT_UPLINK_EC_DATA_FOSEL,
            lpGBTICDataOut => lpGBT_UPLINK_IC_DATA_FOSEL,
            GBTLinkValidOut => LinkAligned_FOSEL, --: out    std_logic_vector(0 to (GBT_NUM-1));
            clk40 => clk40, --: in     std_logic;
            sel => "0"--register_map_control.GBT_TOHOST_FANOUT.SEL(0 downto 0)
        );

    DecodingPixelLinkLPGBT_uut: entity work.DecodingPixelLinkLPGBT
        generic map(
            BLOCKSIZE => 1024,
            SIMU      => 1,
            STREAMS_TOHOST => STREAMS_TOHOST,
            Link => LINK
        )
        port map(
            clk40             => clk40,--: in std_logic; --BC clock for DataIn
            reset             => '0',--reset(4),--: in std_logic; --Acitve high reset
            MsbFirst          => MSB,--: in std_logic; --Default 1, make 0 to reverse the bit order

            LinkData          => lpGBT_UPLINK_USER_DATA_FOSEL(0), --LinkData, --to create 223 downto 0
            LinkAligned       => LinkAligned_FOSEL(0), --1 --LinkAligned,

            DecoderAligned_out    => DecoderAligned,

            m_axis            => m_axis(0 to STREAMS_TOHOST-3),--: out axis_32_type;  --FIFO read port (axi stream)
            m_axis_tready     => m_axis_tready(0 to STREAMS_TOHOST-3), --x"FE00000",--: in std_logic; --FIFO read tready (axi stream)
            m_axis_aclk       => aclk,--: in std_logic; --FIFO read clock (axi stream)
            m_axis_prog_empty => m_axis_prog_empty(0 to STREAMS_TOHOST-3), --: out std_logic
            register_map_control => register_map_control

        );

    --axis stuff
    m_axis(STREAMS_TOHOST-2).tvalid     <= '0';
    m_axis(STREAMS_TOHOST-2).tdata      <= (others => '0');
    m_axis(STREAMS_TOHOST-2).tlast      <= '0';
    m_axis(STREAMS_TOHOST-2).tkeep      <= (others => '0');
    m_axis(STREAMS_TOHOST-2).tuser      <= (others => '0');
    m_axis_prog_empty(STREAMS_TOHOST-2) <= '1';
    m_axis(STREAMS_TOHOST-1).tvalid     <= '0';
    m_axis(STREAMS_TOHOST-1).tdata      <= (others => '0');
    m_axis(STREAMS_TOHOST-1).tlast      <= '0';
    m_axis(STREAMS_TOHOST-1).tkeep      <= (others => '0');
    m_axis(STREAMS_TOHOST-1).tuser      <= (others => '0');
    m_axis_prog_empty(STREAMS_TOHOST-1) <= '1';

    s_axis            <= m_axis;
    m_axis_tready     <= s_axis_tready;
    s_axis_prog_empty <= m_axis_prog_empty;

    aresetn <= not reset(4);
    chStreamController: entity work.ToHostAxiStreamController
        generic map(
            FMCHid              => LINK,
            toHostTimeoutBitn   => toHostTimeoutBitn,
            BLOCKSIZE           => 1024, --BLOCKSIZE,
            CHUNK_TRAILER_32B   => false,--;CHUNK_TRAILER_32B,
            STREAMS_TOHOST      => STREAMS_TOHOST
        --debug
        --      PCIE_ENDPOINT => PCIE_ENDPOINT
        )
        port map (
            aclk                 => aclk,
            aresetn              => aresetn,
            s_axis_in            => s_axis,
            s_axis_tready_out    => s_axis_tready,
            s_axis_prog_empty_in => s_axis_prog_empty,
            timeOutEna_i         => timeCnt_ena,
            timeCnt_max          => timeCnt_max,
            prog_full_in         => '0', --ch_prog_full, -- in, downstream wm fifo is full or main , can't happen in normal operation
            word32out            => chFIFO_din32,    -- to chFIFO
            word32out_valid      => chFIFO_din32_valid -- to chFIFO
        --debug
        --      wr_data_count_in => (others=>'0')
        );

end Behavioral;
