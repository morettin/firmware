source ../helper/do_implementation_pre.tcl

set STOP_TO_ADD_ILA 0

set GENERATE_FM_WRAP false
set GBT_NUM 24
set GTREFCLKS 6
set OPTO_TRX 4
set CARD_TYPE 182
set app_clk_freq 200
set ENDPOINTS 2
set NUM_LEDS 4
set DATA_WIDTH 512

set USE_VERSAL_CPM false

set FIRMWARE_MODE $FIRMWARE_MODE_INTERLAKEN 

set ENABLE_RQS false

#For Interlaken Versal use 64b67b GTY gearbox or use external gearbox
set INTERLAKEN_VERSAL_RAW_MODE true

#Set to false to set BUILD_TIME to the clock, true for GIT_COMMIT_TIME. With false, the build is reproducible.
set DETERMINISTIC_BUILD_TIME false

source ../helper/do_implementation_post.tcl
