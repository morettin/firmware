#!/bin/bash
if [ -z "$1" ]
  then
    echo "give an existing FLX182 .tar.gz build archive as argument"
    exit
fi
rm -rf flx-petalinux-2022.2/
git clone https://gitlab.cern.ch/atlas-tdaq-felix/felix-versal-tools/flx-petalinux-2022.2.git
cp $1 flx-petalinux-2022.2/
cd flx-petalinux-2022.2/
tar -zxvf *.tar.gz
XSAFILE=*.xsa
petalinux-config --silentconfig --get-hw-description $XSAFILE
petalinux-build
petalinux-package --boot --u-boot --force
petalinux-package --wic
mkdir PETALINUX_KERNEL/
mkdir PETALINUX_ROOTFS/
cp images/linux/BOOT.BIN PETALINUX_KERNEL/
cp images/linux/boot.scr PETALINUX_KERNEL/
cp images/linux/image.ub PETALINUX_KERNEL/
cp images/linux/rootfs.cpio PETALINUX_ROOTFS/
GZFILE=*.tar.gz
gunzip $GZFILE
TARFILE=*.tar
tar rf $TARFILE PETALINUX_KERNEL/
tar rf $TARFILE PETALINUX_ROOTFS/
gzip $TARFILE
rm -r PETALINUX_KERNEL/
rm -r PETALINUX_ROOTFS/
