#!/bin/bash
#We are building for an engineering sample device, so we have to use Vivado 2021.2
if [ -f /eda/fpga/xilinx/Vivado/2021.2/settings64.sh ]; then
    #Vivado at Nikhef machines, is installed in /localstore
    export XILINXD_LICENSE_FILE="$LM_LICENSE_FILE"
    source /eda/fpga/xilinx/Vivado/2021.2/settings64.sh
elif [ -f /afs/cern.ch/work/f/fschreud/public/Xilinx/Vivado/2021.2/settings64.sh ]; then
    #Vivado at Cern machines is installed on /afs
    export XILINXD_LICENSE_FILE="2112@licenxilinx"
    source /afs/cern.ch/work/f/fschreud/public/Xilinx/Vivado/2021.2/settings64.sh
else
    #Otherwise set both Nikhef and Cern license servers and try to find in /opt 
    export XILINXD_LICENSE_FILE="$LM_LICENSE_FILE,2112@licenxilinx"
    source /opt/Xilinx/Vivado/2021.2/settings64.sh
fi

vivado -mode batch -nojournal -nolog -notrace -source FLX128_FELIX_import_vivado.tcl
 
vivado -mode batch -nojournal -nolog -notrace ../../Projects/FLX128_FELIX/FLX128_FELIX.xpr -source do_implementation_VCU128_INTERLAKEN.tcl

cd ../../output
rm -f *.bit
rm -f *.mcs
rm -f *.txt
rm -f *.prm

if ls FLX128_INTERLAKEN*.tar.gz 1> /dev/null 2>&1; then
    echo "Build successful"
else
    echo "FELIX archive does not exist"
    exit 2 
fi

