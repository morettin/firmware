
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Kai Chen
#               Weihao Wu
#               Andrea Borga
#               RHabraken
#               Mesfin Gebyehu
#               Enrico Gamberini
#               Frans Schreuder
#               William Wulff
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# Implementation script for BNL711 Fullmode (DUNE configuration)

# Get defaults
source ../helper/do_implementation_pre.tcl

set GENERATE_FM_WRAP true

# JBSC settings
set BLOCKSIZE 4096

# 32B trailer
set CHUNK_TRAILER_32B true

set GBT_NUM 12
set OPTO_TRX 4
set CARD_TYPE 711
set app_clk_freq 200

set CREnableFromHost false

set generateTTCemu false
set TTC_test_mode false

set useToFrontendGBTdataEmulator false

# Defining the Egroups
# ToHost - Egroup 0
set EnableToHo_Egroup0Eproc2_HDLC  false 
set EnableToHo_Egroup0Eproc2_8b10b false 
set EnableToHo_Egroup0Eproc4_8b10b false 
set EnableToHo_Egroup0Eproc8_8b10b false 
set EnableToHo_Egroup0Eproc16_8b10b false 
# ToHost - Egroup 1
set EnableToHo_Egroup1Eproc2_HDLC  false 
set EnableToHo_Egroup1Eproc2_8b10b false 
set EnableToHo_Egroup1Eproc4_8b10b false 
set EnableToHo_Egroup1Eproc8_8b10b false 
set EnableToHo_Egroup1Eproc16_8b10b false 
# ToHost - Egroup 2
set EnableToHo_Egroup2Eproc2_HDLC  false 
set EnableToHo_Egroup2Eproc2_8b10b false 
set EnableToHo_Egroup2Eproc4_8b10b false 
set EnableToHo_Egroup2Eproc8_8b10b false 
set EnableToHo_Egroup2Eproc16_8b10b false 
# ToHost - Egroup 3
set EnableToHo_Egroup3Eproc2_HDLC  false 
set EnableToHo_Egroup3Eproc2_8b10b false 
set EnableToHo_Egroup3Eproc4_8b10b false 
set EnableToHo_Egroup3Eproc8_8b10b false 
set EnableToHo_Egroup3Eproc16_8b10b false 
# ToHost - Egroup 4
set EnableToHo_Egroup4Eproc2_HDLC  false 
set EnableToHo_Egroup4Eproc2_8b10b false 
set EnableToHo_Egroup4Eproc4_8b10b false 
set EnableToHo_Egroup4Eproc8_8b10b false 
set EnableToHo_Egroup4Eproc16_8b10b false 

# FromHost - Egroup 0
set EnableFrHo_Egroup0Eproc2_HDLC  false  
set EnableFrHo_Egroup0Eproc2_8b10b false 
set EnableFrHo_Egroup0Eproc4_8b10b false 
set EnableFrHo_Egroup0Eproc8_8b10b false 
# FromHost - Egroup 1
set EnableFrHo_Egroup1Eproc2_HDLC  false 
set EnableFrHo_Egroup1Eproc2_8b10b false 
set EnableFrHo_Egroup1Eproc4_8b10b false 
set EnableFrHo_Egroup1Eproc8_8b10b false 
# FromHost - Egroup 2
set EnableFrHo_Egroup2Eproc2_HDLC  false 
set EnableFrHo_Egroup2Eproc2_8b10b false 
set EnableFrHo_Egroup2Eproc4_8b10b false 
set EnableFrHo_Egroup2Eproc8_8b10b false 
# FromHost - Egroup 3
set EnableFrHo_Egroup3Eproc2_HDLC  false 
set EnableFrHo_Egroup3Eproc2_8b10b false  
set EnableFrHo_Egroup3Eproc4_8b10b false 
set EnableFrHo_Egroup3Eproc8_8b10b false 
# FromHost - Egroup 4
set EnableFrHo_Egroup4Eproc2_HDLC  false 
set EnableFrHo_Egroup4Eproc2_8b10b false 
set EnableFrHo_Egroup4Eproc4_8b10b false 
set EnableFrHo_Egroup4Eproc8_8b10b false 

# 1: full mode
set FIRMWARE_MODE 1 

source ../helper/do_implementation_post.tcl
