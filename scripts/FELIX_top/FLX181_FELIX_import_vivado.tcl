#
#  File import script for the FELIX hdl Vivado project
#  Board: BNL181
#

source ../helper/clear_filesets.tcl

set PROJECT_NAME FLX181_FELIX
set BOARD_TYPE 181
set TOPLEVEL felix_top
#set TOPLEVEL wupper_oc_top

#Import blocks for different filesets
source ../filesets/fullmode_toplevel_fileset.tcl
source ../filesets/crToHost_fileset.tcl
source ../filesets/crFromHost_fileset.tcl
source ../filesets/decoding_fileset.tcl
source ../filesets/encoding_fileset.tcl
source ../filesets/gbt_toplevel_fileset.tcl
source ../filesets/wupper_fileset.tcl
source ../filesets/gbt_core_fileset.tcl
source ../filesets/fullmode_gbt_core_fileset.tcl
source ../filesets/lpgbt_core_fileset.tcl
source ../filesets/housekeeping_fileset.tcl
source ../filesets/gbt_fanout_fileset.tcl
source ../filesets/gbt_emulator_fileset.tcl
source ../filesets/fullmode_fanout_fileset.tcl
source ../filesets/fullmode_emulator_fileset.tcl
source ../filesets/ttc_decoder_fileset.tcl
source ../filesets/ltittc_wrapper_fileset.tcl
source ../filesets/ttc_emulator_fileset.tcl
source ../filesets/endeavour_fileset.tcl
source ../filesets/itk_strips_fileset.tcl
source ../filesets/encrd53_fileset.tcl
source ../filesets/64b66b_fileset.tcl
source ../filesets/itk_strips_fileset.tcl
source ../filesets/core1990_interlaken_fileset.tcl
#source ../filesets/wupper_oc_fileset.tcl


#Actually execute all the filesets
source ../helper/vivado_import_generic.tcl

puts "INFO: Done!"
