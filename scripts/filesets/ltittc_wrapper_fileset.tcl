set VHDL_FILES [concat $VHDL_FILES \
  ttc/ltittc_wrapper/FLX_LTITTCLink_Wrapper.vhd \
  ttc/ltittc_wrapper/gth_ltittclink_wrapper_ku.vhd \
  ttc/ltittc_wrapper/ltittc_wrapper.vhd \
  ttc/ltittc_wrapper/ltittc_decoder.vhd \
  ttc/ltittc_wrapper/ltittc_wavegen.vhd \
  ttc/ltittc_wrapper/ltittc_transmitter.vhd \
  ttc/ltittc_wrapper/crc16_ltittc.vhd \
  encoding/ExtendedTestPulse.vhd \
  encoding/CRC16_LTI.vhd \
  ttc/itk_trigtag_generator.vhd]
 
set XCI_FILES [concat $XCI_FILES \
  gtwizard_ttc_rxcpll.xci \
  wavegen_dsp_counter.xci]

set SIM_FILES [concat $SIM_FILES \
  UVVMtests/tb/ltittc_tb.vhd]
#  LTITTC/ltittc_sim.vhd

#set WCFG_FILES [concat $WCFG_FILES \
#  LTITTC/ltittc.wcfg]
