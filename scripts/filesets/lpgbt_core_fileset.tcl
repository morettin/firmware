
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Kai Chen
#               Frans Schreuder
#               Elena Zhivun
#               mtrovato
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  LpGBT/LpGBT_FELIX/FELIX_LpGBT_Wrapper.vhd \
  LpGBT/LpGBT_FELIX/FLX_LpGBT_BE.vhd \
  LpGBT/LpGBT_FELIX/FLX_LpGBT_BE_Wrapper.vhd \
  LpGBT/LpGBT_FELIX/RefClk_Gen.vhd \
  LpGBT/LpGBT_FELIX/Regs_RW.vhd \
  LpGBT/LpGBT_FELIX/lpgbtfpga_uplink.vhd \
  LpGBT/LpGBT_FELIX/lpgbtfpga_framealigner.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/lpgbtfpga_package.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/lpgbtfpga_downlink.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/downlink/lpgbtfpga_encoder.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/downlink/lpgbtfpga_interleaver.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/downlink/lpgbtfpga_scrambler.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/downlink/lpgbtfpga_txgearbox.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/downlink/rs_encoder_N7K5.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/descrambler_51bitOrder49.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/descrambler_58bitOrder58.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/fec_rsDecoderN15K13.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/lpgbtfpga_decoder.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/lpgbtfpga_descrambler.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/lpgbtfpga_rxgearbox.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/descrambler_53bitOrder49.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/descrambler_60bitOrder58.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/fec_rsDecoderN31K29.vhd \
  LpGBT/LpGBT_CERN/BE/lpgbt-fpga/uplink/lpgbtfpga_deinterleaver.vhd \
  LpGBT/LpGBT_FELIX/FLX_LpGBT_GTH_BE_VC7.vhd \
  LpGBT/LpGBT_FELIX/FLX_LpGBT_GTH_BE.vhd]

set XCI_FILES_V7 [concat $XCI_FILES_V7 \
  VC7_PMA_QPLL_4CH_LPGBT.xci]

set XCI_FILES_KU [concat $XCI_FILES_KU \
  KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT.xci \
  KCU_RXBUF_PMA_CPLL_1CH_LPGBT.xci \
  KCU_PMA_QPLL_4CH_LPGBT.xci]

set XCI_FILESVU37P [concat $XCI_FILES_VU37P \
  rxclkgen.xci \
  VU37P_PMA_QPLL_4CH_LPGBT.xci \
  VU37P_RXBUF_PMA_QPLL_4CH_LPGBT.xci \
  KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT.xci \
  KCU_RXBUF_PMA_QPLL_4CH_LPGBT.xci]

set XCI_FILESVU9P [concat $XCI_FILES_VU9P \
  rxclkgen.xci \
  KCU_RXBUF_PMA_QPLL_FE4CH_LPGBT.xci \
  KCU_RXBUF_PMA_QPLL_4CH_LPGBT.xci]
  
set BD_FILES_BNL181 [concat $BD_FILES_BNL181 \
  transceiver_versal_lpgbt.bd]
  
set BD_FILES_BNL182 [concat $BD_FILES_BNL182 \
  transceiver_versal_lpgbt.bd]

set BD_FILES_FLX155 [concat $BD_FILES_FLX155 \
  transceiver_versal_lpgbt.bd]
