
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Elena Zhivun
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  ItkStrip/l0a_frame_generator.vhd \
  ItkStrip/lcb_command_decoder.vhd \
  ItkStrip/strips_bypass_frame_aggregator.vhd \
  ItkStrip/playback_controller.vhd \
  ItkStrip/lcb_frame_generator.vhd \
  ItkStrip/strips_package.vhd \
  ItkStrip/lcb_scheduler_encoder.vhd \
  ItkStrip/lcb_trickle_trigger.vhd \
  ItkStrip/lcb_wrapper.vhd \
  ItkStrip/r3l1_frame_synchronizer.vhd \
  ItkStrip/r3l1_frame_generator.vhd \
  ItkStrip/r3l1_scheduler_encoder.vhd \
  ItkStrip/r3l1_wrapper.vhd \
  ItkStrip/lcb_axi_encoder.vhd \
  ItkStrip/r3l1_axi_encoder.vhd \
  ItkStrip/strips_configuration_decoder.vhd \
  ItkStrip/lcb_regmap_package.vhd \
  ItkStrip/lcb_regmap.vhd \
  ItkStrip/r3l1_regmap_package.vhd \
  ItkStrip/r3l1_regmap.vhd \
  AxisUtils/Axis8Fifo.vhd \
  templates/generated/pcie_package.vhd \
  packages/axi_stream_package.vhd \
  packages/centralRouter_package.vhd]

#set VHDL_FILES_V7 [concat $VHDL_FILES_V7 \
#	pcie/data_width_package_256.vhd]
#
#set VHDL_FILES_KU [concat $VHDL_FILES_KU \
#	pcie/data_width_package_256.vhd]
#
#set VHDL_FILES_VU37P [concat $VHDL_FILES_VU37P \
#	pcie/data_width_package_512.vhd]
#
#set VHDL_FILES_VU9P [concat $VHDL_FILES_VU9P \
#	pcie/data_width_package_512.vhd]
#
#set VHDL_FILES_VERSAL [concat $VHDL_FILES_VERSAL \
#	pcie/data_width_package_512.vhd]

set XCI_FILES [concat $XCI_FILES \
  axi8_fifo_bif.xci]

#  strips_lcb_ila.xci \
#  strips_r3l1_ila.xci \
  
set SIM_FILES [concat $SIM_FILES \
  ItkStrip/bypass_data_parser.vhd \
  ItkStrip/decoder_queue_pkg.vhd \
  ItkStrip/elink_data_parser.vhd \
  ItkStrip/itk_frame_decoder.vhd \
  ItkStrip/tb_bypass_frame_aggregator.vhd \
  ItkStrip/tb_bypass_frame_vvc.vhd \
  ItkStrip/tb_bypass_scheduler_continuous_write.vhd \
  ItkStrip/tb_l0a_frame_generator.vhd \
  ItkStrip/tb_lcb_axi_encoder.vhd \
  ItkStrip/tb_lcb_command_decoder.vhd \
  ItkStrip/tb_lcb_frame_generator.vhd \
  ItkStrip/tb_lcb_regmap.vhd \
  ItkStrip/tb_lcb_scheduler_encoder.vhd \
  ItkStrip/tb_playback_controller.vhd \
  ItkStrip/tb_r3l1_axi_encoder.vhd \
  ItkStrip/tb_r3l1_frame_generator.vhd \
  ItkStrip/tb_r3l1_frame_synchronizer.vhd \
  ItkStrip/tb_r3l1_regmap.vhd \
  ItkStrip/tb_r3l1_scheduler_encoder.vhd \
  ItkStrip/tb_strips_configuration_decoder.vhd \
  ItkStrip/tb_trickle_trigger.vhd \
  ItkStrip/ttc_l0a_data_parser.vhd]
