
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Rene
#               Thei Wijnen
#               mtrovato
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  templates/generated/pcie_package.vhd \
  templates/generated/dma_control.vhd \
  templates/generated/register_map_sync.vhd \
  templates/generated/wupper.vhd \
  packages/centralRouter_package.vhd \
  pcie/pcie_clocking.vhd \
  pcie/pcie_slow_clock.vhd \
  pcie/dma_read_write.vhd \
  pcie/intr_ctrl.vhd \
  pcie/wupper_core.vhd \
  pcie/pcie_ep_wrap.vhd \
  pcie/vsec_null.vhd \
  pcie/pcie_init.vhd \
  packages/FELIX_package.vhd \
  pcie/WupperFifos.vhd]

set DISABLED_VHDL_FILES [concat $DISABLED_VHDL_FILES \
  templates/generated/dma_control_0.vhd \
  templates/generated/dma_control_1.vhd \
  templates/generated/dma_control_2.vhd \
  templates/generated/dma_control_3.vhd \
  templates/generated/dma_control_4.vhd \
  templates/generated/dma_control_5.vhd \
  templates/generated/dma_control_6.vhd \
  templates/generated/dma_control_7.vhd \
  templates/generated/dma_control_8.vhd \
  templates/generated/dma_control_9.vhd \
  templates/generated/dma_control_10.vhd \
  templates/generated/dma_control_11.vhd \
  templates/generated/dma_control_12.vhd \
  templates/generated/dma_control_13.vhd]
  

set SIM_FILES [concat $SIM_FILES \
  Wupper/wupper_tb.vhd \
  Wupper/pcie_ep_sim_model.vhd]
  
set WCFG_FILES [concat $WCFG_FILES \
  Wupper/wupper_tb_behav.wcfg]
  
set XCI_FILES [concat $XCI_FILES \
  clk_wiz_regmap.xci]

#Kintex ultrascale specific files
set VHDL_FILES_KU [concat $VHDL_FILES_KU \
  ip_cores/kintexUltrascale/pcie_x8_gen3_3_0_stub.vhdl]

set XCI_FILES_KU [concat $XCI_FILES_KU \
  pcie3_ultrascale_7038.xci \
  pcie3_ultrascale_7039.xci \
  debug_bridge_0.xci]

#Virtex 7 specific files
set VHDL_FILES_V7 [concat $VHDL_FILES_V7 \
  ip_cores/virtex7/pcie3_ultrascale_7038_stub.vhdl \
  ip_cores/virtex7/pcie3_ultrascale_7039_stub.vhdl]

set XCI_FILES_V7 [concat $XCI_FILES_V7 \
  pcie_x8_gen3_3_0.xci]

#Virtex Ultrascale+ VU9P specific files
set XCI_FILES_VU9P [concat $XCI_FILES_VU9P \
  pcie4c_uscale_plus_0.xci \
  pcie4c_uscale_plus_1.xci ]
  
#set VHDL_FILES_VU9P [concat $VHDL_FILES_VU9P \
#  pcie/data_width_package_512.vhd]

#Virtex Ultrascale+ VU37P specific files
set XCI_FILES_VU37P [concat $XCI_FILES_VU37P \
  pcie4c_uscale_plus_0.xci \
  pcie4c_uscale_plus_1.xci]
  
#set VHDL_FILES_VERSAL [concat $VHDL_FILES_VERSAL \
#  pcie/data_width_package_512.vhd]
  
set BD_FILES_VMK180 [concat $BD_FILES_VMK180 \
  versal_pcie_block.bd]

set BD_FILES_BNL181 [concat $BD_FILES_BNL181 \
  versal_pcie_block_BNL181_ep0.bd \
  versal_pcie_block_BNL181_ep1.bd]

set BD_FILES_BNL182 [concat $BD_FILES_BNL182 \
  versal_pcie_block_BNL182_ep0.bd \
  versal_pcie_block_BNL182_ep1.bd]
  
set BD_FILES_VPK120 [concat $BD_FILES_VPK120 \
  pcie_gen5_vpk_ep0.bd \
  ]


#set VHDL_FILES_VU37P [concat $VHDL_FILES_VU37P \
#  pcie/data_width_package_512.vhd]
  
#Versal only, we need the CIPS block somewhere. For BNL182 we add the CPM, containing 2 Gen4x8 PCIe endpoints.
set BD_FILES_BNL182 [concat $BD_FILES_BNL182 \
  versal_cpm_pcie.bd]
  
set BD_FILES_FLX155 [concat $BD_FILES_FLX155 \
  FLX155_cips.bd]
  
set VHDL_FILES_BNL182 [concat $VHDL_FILES_BNL182 \
  pcie/versal_cpm_pcie_endpoints.vhd]
  
set VHDL_FILES_FLX155 [concat $VHDL_FILES_FLX155 \
  pcie/versal_cpm_pcie_endpoints.vhd]
