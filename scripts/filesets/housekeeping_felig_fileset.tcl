
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               mtrovato
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set VHDL_FILES [concat $VHDL_FILES \
  shared/card_type_specific_ios.vhd \
  shared/xadc_drp.vhd \
  shared/dna.vhd \
  housekeeping/housekeeping_module.vhd \
  housekeeping/i2c_interface.vhd \
  housekeeping/clock_and_reset.vhd \
  housekeeping/debug_port_module.vhd \
  housekeeping/GenericConstantsToRegs.vhd \
  housekeeping/gc_multichannel_frequency_meter.vhd \
  housekeeping/gc_pulse_synchronizer.vhd \
  housekeeping/gc_pulse_synchronizer2.vhd \
  housekeeping/gc_sync_ffs.vhd \
  i2c_master/i2c.vhd]

set VHDL_FILES_V7 [concat $VHDL_FILES_V7 \
  flash/flash_wrapper_stub.vhd]

set VHDL_FILES_KU [concat $VHDL_FILES_KU \
  i2c_master/I2C_Master_PEX.vhd \
  FELIG/felix_modified/spi/LMK03200_spi.vhd \
  FELIG/felix_modified/spi/LMK03200_wrapper.vhd \
  shared/pex_init.vhd \
  flash/flash_wrapper.vhd \
  flash/flash_ipcore_bnl.vhd]

set XCI_FILES [concat $XCI_FILES \
  I2C_RDFifo.xci \
  I2C_WRFifo.xci \
  clk_wiz_40_0.xci \
  clk_wiz_200_0.xci \
  clk_wiz_156_0.xci \
  clk_wiz_100_0.xci \
  clk_wiz_250.xci]

#Kintex ultrascale only
set XCI_FILES_KU [concat $XCI_FILES_KU \
  system_management_wiz_0.xci]
  
set VHDL_FILES_KU [concat $VHDL_FILES_KU \
  ip_cores/kintexUltrascale/xadc_wiz_0_stub.vhdl]

#Virtex 7 only
set XCI_FILES_V7 [concat $XCI_FILES_V7 \
  xadc_wiz_0.xci]
  
set VHDL_FILES_V7 [concat $VHDL_FILES_V7 \
  ip_cores/virtex7/system_management_wiz_0_stub.vhdl]
