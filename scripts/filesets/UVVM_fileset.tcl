
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set SIM_FILES [concat $SIM_FILES \
  UVVMtests/sim/vip_egr/egr_vvc.vhd \
  UVVMtests/sim/vip_egr/vvc_methods_pkg.vhd \
  UVVMtests/sim/vip_egr/td_target_support_pkg.vhd \
  UVVMtests/sim/vip_egr/td_vvc_framework_common_methods_pkg.vhd \
  UVVMtests/sim/vip_egr/vvc_cmd_pkg.vhd \
  UVVMtests/sim/vip_egr/td_queue_pkg.vhd \
  UVVMtests/sim/vip_egr/td_vvc_entity_support_pkg.vhd \
  UVVMtests/sim/vip_egr/egr_bfm_pkg.vhd]
