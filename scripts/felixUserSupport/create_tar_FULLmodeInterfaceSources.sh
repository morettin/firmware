#!/bin/bash
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Mark Donszelmann
#               Frans Schreuder
#               RHabraken
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set -e
#
#	create examples tar.gz file
#
#

usage()
{
  echo "usage: $(basename $0) <name> <svn-version>"
  #exit 1
}

if [ $# -lt 2 ]; then
    usage
	COMMIT_DATETIME=$(git show -s --format=%cd --date=iso)
	YY=${COMMIT_DATETIME:2:2}
	MM=${COMMIT_DATETIME:5:2}
	DD=${COMMIT_DATETIME:8:2}
	hh=${COMMIT_DATETIME:11:2}
	mm=${COMMIT_DATETIME:14:2}
	COMMIT_DATETIME=${YY}${MM}${DD}_${hh}_${mm}
	git_tag_str=$(git describe --abbrev=0 --tags)
	GIT_COMMIT_NUMBER=$(git rev-list ${git_tag_str}..HEAD)
	GIT_COMMIT_NUMBER=$(echo "$GIT_COMMIT_NUMBER" | wc -l)
	GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
	TARGET_NAME=FM_UserExample
	TARGET_VERSION=GIT_${GIT_BRANCH}_${git_tag_str}_${GIT_COMMIT_NUMBER}_${COMMIT_DATETIME}
	TARGET=${TARGET_NAME}-${TARGET_VERSION}
	echo ${TARGET}
else
	TARGET_NAME=${1}
	TARGET_VERSION=${2}

fi

TARGET=${TARGET_NAME}-s${TARGET_VERSION}

# Set the support files directory path
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# firmware directory:
FIRMWARE_DIR=${SCRIPT_DIR}/../../
DOC_DIR=${FIRMWARE_DIR}/../documents/FullMode/Requirements/
CORE_DIR=${FIRMWARE_DIR}/sources/ip_cores/
#CORE_DIR=${FIRMWARE_DIR}/sources/ip_cores/kintexUltrascale
#CORE_DIR=${FIRMWARE_DIR}/sources/ip_cores/virtex7
# sources directories:
CR_SOURCES_DIR=${FIRMWARE_DIR}/sources/centralRouter/
FMTX_SOURCES_DIR=${FIRMWARE_DIR}/sources/FullModeTransmitter/
PROJ_SOURCES_DIR=${FIRMWARE_DIR}/sources/felixUserSupport/FullModeUserInterface/
# source share directory path
SHARE_DIR=/tmp
#

mkdir -p ${SHARE_DIR}/${TARGET}
mkdir -p ${SHARE_DIR}/${TARGET}/doc
mkdir -p ${SHARE_DIR}/${TARGET}/src
mkdir -p ${SHARE_DIR}/${TARGET}/src/example_design_kintexultrascale
mkdir -p ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
mkdir -p ${SHARE_DIR}/${TARGET}/constraints
mkdir -p ${SHARE_DIR}/${TARGET}/scripts/

mkdir -p ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale
mkdir -p ${SHARE_DIR}/${TARGET}/src/ip_cores
mkdir -p ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7
mkdir -p ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7/vc707
mkdir -p ${SHARE_DIR}/${TARGET}/src/ip_cores/kintexUltrascale

# --------------------------------------------------------------------------
#      local FullModeUserInterface sources
# --------------------------------------------------------------------------
cp ${PROJ_SOURCES_DIR}/gth_qpll_wrapper_proca.vhd ${SHARE_DIR}/${TARGET}/src/example_design_kintexultrascale
cp ${PROJ_SOURCES_DIR}/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gthe3_channel_wrapper.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gthe3_common_wrapper.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gtwizard_gthe3.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gth_tx_fullmode_rx_gbtmode_qpll_ultrascale_gtwizard_top.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_bit_synchronizer.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gthe3_channel.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gthe3_common.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gthe3_cpll_cal.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gthe3_cpll_cal_freq_counter.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gtwiz_buffbypass_rx.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gtwiz_buffbypass_tx.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gtwiz_reset.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gtwiz_userclk_rx.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gtwiz_userclk_tx.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gtwiz_userdata_rx.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_gtwiz_userdata_tx.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_reset_inv_synchronizer.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/
cp ${PROJ_SOURCES_DIR}/gtwizard_ultrascale_v1_6_reset_synchronizer.v ${SHARE_DIR}/${TARGET}/src/gth_kintexultrascale/


cp ${FIRMWARE_DIR}/sources/felixUserSupport/FM_UserExample.vhd                                                        ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
                                                                                                              
# -- I2C master                                                                                               
cp ${FIRMWARE_DIR}/sources/housekeeping/si5324_init.vhd                                                           ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/i2c_master/i2c.vhd                                                                 ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
                                                                                                              
cp ${FIRMWARE_DIR}/sources/felixUserSupport/FM_example_clocking.vhd                                                   ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/felixUserSupport/FM_example_emuram.vhd                                                     ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/felixUserSupport/FM_example_FIFOctrl.vhd                                                   ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/FullModeEmulator/FIFO34to34b.vhd                                                           ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/felixUserSupport/fullmodetransceiver_gth_gth/fullmodetransceiver.vhd               ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
#cp ${FIRMWARE_DIR}/sources/felixUserSupport/fullmodetransceiver_gth_gth/fullmodetransceiver_reset_fsm.vhd     ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/felixUserSupport/packages/FMTransceiverPackage.vhd                                 ${SHARE_DIR}/${TARGET}/src/example_design_virtex7



cp ${FIRMWARE_DIR}/sources/packages/FELIX_gbt_package.vhd                                                     ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_descrambler_21bit.vhd                                          ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_descrambler_16bit.vhd                                          ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_rs2errcor.vhd                                 ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_elpeval.vhd                                   ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_lmbddet.vhd                                   ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_syndrom.vhd                                   ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_chnsrch.vhd                                   ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_errlcpoly.vhd                                 ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec.vhd                                     ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_deintlver.vhd                                 ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_gbtframe_rsdec_sync.vhd                                ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_descrambler_FELIX.vhd                                          ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_gearbox_FELIX_wi_rxbuffer.vhd                                  ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/FELIX_GBT_RX_AUTO_RST.vhd                                             ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/FELIX_GBT_RXSLIDE_FSM.vhd                                             ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbt_rx_decoder_FELIX.vhd                                              ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbtRx_wrap_FELIX.vhd                                                  ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/gbtRx_FELIX.vhd                                                       ${SHARE_DIR}/${TARGET}/src/example_design_virtex7
cp ${FIRMWARE_DIR}/sources/GBT/gbt_code/FELIX_gbt_wrapper_no_gth.vhd                                          ${SHARE_DIR}/${TARGET}/src/example_design_virtex7



cp ${CORE_DIR}/virtex7/fifo_GBT2CR.xci         ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7
cp ${CORE_DIR}/virtex7/clk_wiz_156_1.xci       ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7
cp ${CORE_DIR}/virtex7/DPram_32b.xci           ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7
cp ${CORE_DIR}/virtex7/DPram_32b.coe           ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7
cp ${CORE_DIR}/virtex7/fifo1KB_34bit.xci       ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7
cp ${CORE_DIR}/virtex7/ila_gbt_rx.xci          ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7
cp ${CORE_DIR}/virtex7/ila_fullmode_rx_tx.xci  ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7
cp ${CORE_DIR}/virtex7/vio_si5324_init.xci     ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7
cp ${CORE_DIR}/virtex7/fullmodetransceiver_core.xci ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7

cp ${CORE_DIR}/virtex7/vc707/fifo_GBT2CR.xci         ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7/vc707
cp ${CORE_DIR}/virtex7/vc707/clk_wiz_156_1.xci       ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7/vc707
cp ${CORE_DIR}/virtex7/vc707/DPram_32b.xci           ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7/vc707
cp ${CORE_DIR}/virtex7/vc707/DPram_32b.coe           ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7/vc707
cp ${CORE_DIR}/virtex7/vc707/fifo1KB_34bit.xci       ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7/vc707
cp ${CORE_DIR}/virtex7/vc707/ila_gbt_rx.xci          ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7/vc707
cp ${CORE_DIR}/virtex7/vc707/ila_fullmode_rx_tx.xci  ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7/vc707
cp ${CORE_DIR}/virtex7/vc707/vio_si5324_init.xci     ${SHARE_DIR}/${TARGET}/src/ip_cores/virtex7/vc707
                                                                    

cp ${FIRMWARE_DIR}/scripts/FM_UserExample/vivado_import_felix_VC707.tcl ${SHARE_DIR}/${TARGET}/scripts
cp ${FIRMWARE_DIR}/scripts/FM_UserExample/vivado_import_felix_VC709.tcl ${SHARE_DIR}/${TARGET}/scripts
cp ${FIRMWARE_DIR}/scripts/FM_UserExample/do_implementation_VC707.tcl ${SHARE_DIR}/${TARGET}/scripts
cp ${FIRMWARE_DIR}/scripts/FM_UserExample/do_implementation_VC709.tcl ${SHARE_DIR}/${TARGET}/scripts



# --------------------------------------------------------------------------
#      FULL mode TX controller/user example sources
# --------------------------------------------------------------------------
cp ${FIRMWARE_DIR}/sources/CRC20/crc.vhd                       ${SHARE_DIR}/${TARGET}/src/
cp ${FMTX_SOURCES_DIR}/FMchannelTXctrl.vhd                     ${SHARE_DIR}/${TARGET}/src/
cp ${FMTX_SOURCES_DIR}/FullModeUserLogic.vhd                   ${SHARE_DIR}/${TARGET}/src/example_design_kintexultrascale


# --------------------------------------------------------------------------
#      aux - CR sources
# --------------------------------------------------------------------------
#cp ${CR_SOURCES_DIR}/pulse_fall_pw01.vhd ${SHARE_DIR}/${TARGET}/src/
cp ${CR_SOURCES_DIR}/pulse_pdxx_pwxx.vhd ${SHARE_DIR}/${TARGET}/src/


# --------------------------------------------------------------------------
#          FULL mode interface cores
# --------------------------------------------------------------------------
cp ${CORE_DIR}/kintexUltrascale/FMuserFIFO.xci    ${SHARE_DIR}/${TARGET}/src/ip_cores/kintexUltrascale
cp ${CORE_DIR}/kintexUltrascale/FMuserDataRAM.xci ${SHARE_DIR}/${TARGET}/src/ip_cores/kintexUltrascale
cp ${CORE_DIR}/kintexUltrascale/fm_user_ram.coe   ${SHARE_DIR}/${TARGET}/src/ip_cores/kintexUltrascale


# --------------------------------------------------------------------------
#      all packages
# --------------------------------------------------------------------------


# --------------------------------------------------------------------------
#      constraints
# --------------------------------------------------------------------------
cp ${FIRMWARE_DIR}/constraints/FMUserExample_VC709.xdc ${SHARE_DIR}/${TARGET}/constraints
cp ${FIRMWARE_DIR}/constraints/FMUserExample_VC707.xdc ${SHARE_DIR}/${TARGET}/constraints



# --------------------------------------------------------------------------
#      simulation sources
# --------------------------------------------------------------------------


# --------------------------------------------------------------------------
#      Documents
# --------------------------------------------------------------------------
cp ${DOC_DIR}/FullMode.pdf ${SHARE_DIR}/${TARGET}/doc/FullMode.pdf

tar zcvf ${TARGET}.tar.gz -C ${SHARE_DIR} ${TARGET}
