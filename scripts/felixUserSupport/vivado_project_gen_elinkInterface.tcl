
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Julia Narevicius
#               Israel Grayzman
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#
#	File import script for the FELIX hdl project
#   generates project for stand alone elinkInterface 
#	

#Script Configuration 
set proj_name "elinkInterface"
set PART "xc7vx690tffg1761-2"
# Set the support files directory path
set scriptdir [pwd]
# firmware directory:
set firmware_dir $scriptdir/../../
set core_dir $firmware_dir/sources/ip_cores/virtex7/
# sources directories:
set cr_sources_dir $firmware_dir/sources/centralRouter/
set proj_sources_dir $firmware_dir/sources/felixUserSupport/elinkInterface/
# project directory:
set project_dir $firmware_dir/Projects/$proj_name

#Close currently open project and create a new one. (OVERWRITES PROJECT!!)
close_project -quiet
create_project -force -part xc7vx690tffg1761-2 $proj_name $project_dir
set_property target_language VHDL [current_project]
set_property default_lib work [current_project]
set_param general.maxThreads 8

# --------------------------------------------------------------------------
#      local elinkInterface source import
# --------------------------------------------------------------------------
read_vhdl -library work $proj_sources_dir/elinkInterface_top.vhd
read_vhdl -library work $proj_sources_dir/Elink2FIFO.vhd
read_vhdl -library work $proj_sources_dir/FIFO2Elink.vhd


# --------------------------------------------------------------------------
#      relevant Central Router sources import
# --------------------------------------------------------------------------
read_vhdl -library work $cr_sources_dir/8b10_dec.vhd
read_vhdl -library work $cr_sources_dir/8b10_dec_wrap.vhd
read_vhdl -library work $cr_sources_dir/BLOCK_WORD_COUNTER.vhd
read_vhdl -library work $cr_sources_dir/CD_COUNTER.vhd
read_vhdl -library work $cr_sources_dir/CRresetManager.vhd
read_vhdl -library work $cr_sources_dir/enc_8b10.vhd
read_vhdl -library work $cr_sources_dir/enc8b10_wrap.vhd
read_vhdl -library work $cr_sources_dir/EPATH_FIFO_WRAP.vhd
read_vhdl -library work $cr_sources_dir/EPROC_FIFO_DRIVER.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN2.vhd
#read_vhdl -library work $cr_sources_dir/EPROC_IN2_ALIGN_BLOCK.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN2_DEC8b10b.vhd
#read_vhdl -library work $cr_sources_dir/EPROC_IN2_direct.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN2_HDLC.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN4.vhd
#read_vhdl -library work $cr_sources_dir/EPROC_IN4_ALIGN_BLOCK.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN4_DEC8b10b.vhd
#read_vhdl -library work $cr_sources_dir/EPROC_IN4_direct.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN8.vhd
#read_vhdl -library work $cr_sources_dir/EPROC_IN8_ALIGN_BLOCK.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN8_DEC8b10b.vhd
#read_vhdl -library work $cr_sources_dir/EPROC_IN8_direct.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN16.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN16_ALIGN_BLOCK.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN16_DEC8b10b.vhd
read_vhdl -library work $cr_sources_dir/EPROC_IN16_direct.vhd
read_vhdl -library work $cr_sources_dir/EPROC_OUT2.vhd
read_vhdl -library work $cr_sources_dir/EPROC_OUT2_direct.vhd
read_vhdl -library work $cr_sources_dir/EPROC_OUT2_ENC8b10b.vhd
read_vhdl -library work $cr_sources_dir/EPROC_OUT2_HDLC.vhd
read_vhdl -library work $cr_sources_dir/EPROC_OUT4.vhd
read_vhdl -library work $cr_sources_dir/EPROC_OUT4_direct.vhd
read_vhdl -library work $cr_sources_dir/EPROC_OUT4_ENC8b10b.vhd
read_vhdl -library work $cr_sources_dir/EPROC_OUT8.vhd
read_vhdl -library work $cr_sources_dir/EPROC_OUT8_ENC8b10b.vhd
read_vhdl -library work $cr_sources_dir/KcharTest.vhd
read_vhdl -library work $cr_sources_dir/MUX2_Nbit.vhd
read_vhdl -library work $cr_sources_dir/MUX4.vhd
read_vhdl -library work $cr_sources_dir/MUX4_Nbit.vhd
read_vhdl -library work $cr_sources_dir/MUX8_Nbit.vhd
read_vhdl -library work $cr_sources_dir/pulse_fall_pw01.vhd
read_vhdl -library work $cr_sources_dir/pulse_pdxx_pwxx.vhd
read_vhdl -library work $cr_sources_dir/reg8to16bit.vhd
read_vhdl -library work $cr_sources_dir/SCDataMANAGER.vhd
read_vhdl -library work $cr_sources_dir/upstreamEpathFifoWrap.vhd



# --------------------------------------------------------------------------
#          centralRouter cores
# --------------------------------------------------------------------------
import_ip $core_dir/EPATH_FIFO_bif.xci
import_ip $core_dir/hdlc_bist_fifo.xci
import_ip $core_dir/fh_epath_fifo2K_18bit_wide.xci

upgrade_ip [get_ips  {EPATH_FIFO_bif hdlc_bist_fifo fh_epath_fifo2K_18bit_wide}] 

# --------------------------------------------------------------------------
#          GBTlinksDataEmulator cores
# --------------------------------------------------------------------------
import_ip $core_dir/emuram_2.xci
file copy -force $core_dir/emurom_2.coe $project_dir/$proj_name.srcs/sources_1/ip/emuram_2/emurom_2.coe

upgrade_ip [get_ips  {emuram_2}] 


# --------------------------------------------------------------------------
#      all packages
# --------------------------------------------------------------------------
read_vhdl -library work $firmware_dir/sources/packages/centralRouter_package.vhd
read_vhdl -library work $proj_sources_dir/elinkInterface_package.vhd



# --------------------------------------------------------------------------
#      local cores
# --------------------------------------------------------------------------
import_ip $cr_sources_dir/crTOPaux/CR_CLKs.xci

upgrade_ip [get_ips  {CR_CLKs}] 

# --------------------------------------------------------------------------
#      constraints
# --------------------------------------------------------------------------



# --------------------------------------------------------------------------
#      top module
# --------------------------------------------------------------------------
set_property top elinkInterface_top [current_fileset]


# --------------------------------------------------------------------------
#      simulation sources
# --------------------------------------------------------------------------
set_property SOURCE_SET sources_1 [get_filesets sim_1]
import_files -fileset sim_1 -force -norecurse $proj_sources_dir/TB_elinkInterface_top.vhd
update_compile_order -fileset sim_1
set_property library work [get_files  $project_dir/elinkInterface.srcs/sim_1/imports/$proj_name/TB_elinkInterface_top.vhd]
update_compile_order -fileset sim_1

set_property -name {xsim.simulate.runtime} -value {100ns} -objects [current_fileset -simset]




puts "INFO: Done!"







