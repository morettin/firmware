#!/usr/bin/tclsh
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Julia Narevicius
#               Israel Grayzman
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#	project sources copy script for elinkInterface
#   
#	
#set COMMIT_DATETIME [exec git show -s --format=%cd --date=format:'%y%m%d%H%M']
set COMMIT_DATETIME [exec git show -s --format=%cd --date=iso]
set YY [string range $COMMIT_DATETIME 2 3]
set MM [string range $COMMIT_DATETIME 5 6]
set DD [string range $COMMIT_DATETIME 8 9]
set hh [string range $COMMIT_DATETIME 11 12]
set mm [string range $COMMIT_DATETIME 14 15]
 
set COMMIT_DATETIME ${YY}${MM}${DD}_${hh}_${mm}

set git_tag_str [exec git describe --abbrev=0 --tags]
set GIT_COMMIT_NUMBER [exec git rev-list ${git_tag_str}..HEAD]
set GIT_COMMIT_NUMBER [expr [regexp -all {[\n]+} $GIT_COMMIT_NUMBER ] +1]
     

set GIT_BRANCH [exec git rev-parse --abbrev-ref HEAD]

set TARGET_NAME elinkInterface_project_sources
set TARGET_VERSION GIT_${GIT_BRANCH}_${git_tag_str}_${GIT_COMMIT_NUMBER}_${COMMIT_DATETIME}
set TARGET ${TARGET_NAME}-${TARGET_VERSION}


# Set the support files directory path
set scriptdir [pwd]
# firmware directory:
set firmware_dir $scriptdir/../..
set core_dir $firmware_dir/sources/ip_cores/virtex7
# sources directories:
set cr_sources_dir $firmware_dir/sources/centralRouter
set proj_sources_dir $firmware_dir/sources/felixUserSupport/elinkInterface
# source share directory path
set user_firmware_dir $scriptdir/${TARGET}
set user_scriptdir $scriptdir/${TARGET}/scripts/felixUserSupport
set user_proj_sources_dir $scriptdir/${TARGET}/sources/felixUserSupport/elinkInterface
set user_cr_sources_dir $scriptdir/${TARGET}/sources/centralRouter
set user_core_dir $scriptdir/${TARGET}/sources/ip_cores/virtex7
file mkdir $user_firmware_dir
file mkdir $user_firmware_dir/scripts
file mkdir $user_scriptdir
file mkdir $user_firmware_dir/sources
file mkdir $user_firmware_dir/sources/felixUserSupport
file mkdir $user_proj_sources_dir
file mkdir $user_cr_sources_dir
file mkdir $user_cr_sources_dir/crTOPaux
file mkdir $user_firmware_dir/sources/ip_cores
file mkdir $user_core_dir
file mkdir $user_firmware_dir/sources/packages
#

# directory structure:
#
#	${TARGET} [$user_firmware_dir]
#	|-scripts
#	|	|-felixUserSupport [$user_scriptdir]
#	|-sources
#	|	|-felixUserSupport
#	|	|	|-elinkInterface [$user_proj_sources_dir]
#	|	|-centralRouter [$user_cr_sources_dir]
#	|	|	|-crTOPaux
#	|	|-ip_cores
#	|	|	|-virtex7 [$user_core_dir]
#	|-packages



# --------------------------------------------------------------------------
#      Vivado project
# --------------------------------------------------------------------------
file copy -force $scriptdir/vivado_project_gen_elinkInterface.tcl $user_scriptdir/vivado_project_gen_elinkInterface.tcl

# --------------------------------------------------------------------------
#      local elinkInterface sources
# --------------------------------------------------------------------------
file copy -force $proj_sources_dir/elinkInterface_top.vhd $user_proj_sources_dir/elinkInterface_top.vhd
file copy -force $proj_sources_dir/Elink2FIFO.vhd $user_proj_sources_dir/Elink2FIFO.vhd 
file copy -force $proj_sources_dir/FIFO2Elink.vhd $user_proj_sources_dir/FIFO2Elink.vhd  


# --------------------------------------------------------------------------
#      relevant Central Router sources 
# --------------------------------------------------------------------------
file copy -force $cr_sources_dir/8b10_dec.vhd $user_cr_sources_dir/8b10_dec.vhd
file copy -force $cr_sources_dir/8b10_dec_wrap.vhd $user_cr_sources_dir/8b10_dec_wrap.vhd
file copy -force $cr_sources_dir/BLOCK_WORD_COUNTER.vhd $user_cr_sources_dir/BLOCK_WORD_COUNTER.vhd
file copy -force $cr_sources_dir/CD_COUNTER.vhd $user_cr_sources_dir/CD_COUNTER.vhd
file copy -force $cr_sources_dir/CRresetManager.vhd $user_cr_sources_dir/CRresetManager.vhd
file copy -force $cr_sources_dir/enc_8b10.vhd $user_cr_sources_dir/enc_8b10.vhd
file copy -force $cr_sources_dir/enc8b10_wrap.vhd $user_cr_sources_dir/enc8b10_wrap.vhd
file copy -force $cr_sources_dir/EPATH_FIFO_WRAP.vhd $user_cr_sources_dir/EPATH_FIFO_WRAP.vhd
file copy -force $cr_sources_dir/EPROC_FIFO_DRIVER.vhd $user_cr_sources_dir/EPROC_FIFO_DRIVER.vhd
file copy -force $cr_sources_dir/EPROC_IN2.vhd $user_cr_sources_dir/EPROC_IN2.vhd
#file copy -force $cr_sources_dir/EPROC_IN2_ALIGN_BLOCK.vhd $user_cr_sources_dir/EPROC_IN2_ALIGN_BLOCK.vhd
file copy -force $cr_sources_dir/EPROC_IN2_DEC8b10b.vhd $user_cr_sources_dir/EPROC_IN2_DEC8b10b.vhd
#file copy -force $cr_sources_dir/EPROC_IN2_direct.vhd $user_cr_sources_dir/EPROC_IN2_direct.vhd
file copy -force $cr_sources_dir/EPROC_IN2_HDLC.vhd $user_cr_sources_dir/EPROC_IN2_HDLC.vhd
file copy -force $cr_sources_dir/EPROC_IN4.vhd $user_cr_sources_dir/EPROC_IN4.vhd
#file copy -force $cr_sources_dir/EPROC_IN4_ALIGN_BLOCK.vhd $user_cr_sources_dir/EPROC_IN4_ALIGN_BLOCK.vhd
file copy -force $cr_sources_dir/EPROC_IN4_DEC8b10b.vhd $user_cr_sources_dir/EPROC_IN4_DEC8b10b.vhd
#file copy -force $cr_sources_dir/EPROC_IN4_direct.vhd $user_cr_sources_dir/EPROC_IN4_direct.vhd
file copy -force $cr_sources_dir/EPROC_IN8.vhd $user_cr_sources_dir/EPROC_IN8.vhd
#file copy -force $cr_sources_dir/EPROC_IN8_ALIGN_BLOCK.vhd $user_cr_sources_dir/EPROC_IN8_ALIGN_BLOCK.vhd
file copy -force $cr_sources_dir/EPROC_IN8_DEC8b10b.vhd $user_cr_sources_dir/EPROC_IN8_DEC8b10b.vhd
#file copy -force $cr_sources_dir/EPROC_IN8_direct.vhd $user_cr_sources_dir/EPROC_IN8_direct.vhd
file copy -force $cr_sources_dir/EPROC_IN16.vhd $user_cr_sources_dir/EPROC_IN16.vhd
file copy -force $cr_sources_dir/EPROC_IN16_ALIGN_BLOCK.vhd $user_cr_sources_dir/EPROC_IN16_ALIGN_BLOCK.vhd
file copy -force $cr_sources_dir/EPROC_IN16_DEC8b10b.vhd $user_cr_sources_dir/EPROC_IN16_DEC8b10b.vhd
file copy -force $cr_sources_dir/EPROC_IN16_direct.vhd $user_cr_sources_dir/EPROC_IN16_direct.vhd
file copy -force $cr_sources_dir/EPROC_OUT2.vhd $user_cr_sources_dir/EPROC_OUT2.vhd
file copy -force $cr_sources_dir/EPROC_OUT2_direct.vhd $user_cr_sources_dir/EPROC_OUT2_direct.vhd
file copy -force $cr_sources_dir/EPROC_OUT2_ENC8b10b.vhd $user_cr_sources_dir/EPROC_OUT2_ENC8b10b.vhd
file copy -force $cr_sources_dir/EPROC_OUT2_HDLC.vhd $user_cr_sources_dir/EPROC_OUT2_HDLC.vhd
file copy -force $cr_sources_dir/EPROC_OUT4.vhd $user_cr_sources_dir/EPROC_OUT4.vhd
file copy -force $cr_sources_dir/EPROC_OUT4_direct.vhd $user_cr_sources_dir/EPROC_OUT4_direct.vhd
file copy -force $cr_sources_dir/EPROC_OUT4_ENC8b10b.vhd $user_cr_sources_dir/EPROC_OUT4_ENC8b10b.vhd
file copy -force $cr_sources_dir/EPROC_OUT8.vhd $user_cr_sources_dir/EPROC_OUT8.vhd
file copy -force $cr_sources_dir/EPROC_OUT8_ENC8b10b.vhd $user_cr_sources_dir/EPROC_OUT8_ENC8b10b.vhd
file copy -force $cr_sources_dir/KcharTest.vhd $user_cr_sources_dir/KcharTest.vhd
file copy -force $cr_sources_dir/MUX2_Nbit.vhd $user_cr_sources_dir/MUX2_Nbit.vhd
file copy -force $cr_sources_dir/MUX4.vhd $user_cr_sources_dir/MUX4.vhd
file copy -force $cr_sources_dir/MUX4_Nbit.vhd $user_cr_sources_dir/MUX4_Nbit.vhd
file copy -force $cr_sources_dir/MUX8_Nbit.vhd $user_cr_sources_dir/MUX8_Nbit.vhd
file copy -force $cr_sources_dir/pulse_fall_pw01.vhd $user_cr_sources_dir/pulse_fall_pw01.vhd
file copy -force $cr_sources_dir/pulse_pdxx_pwxx.vhd $user_cr_sources_dir/pulse_pdxx_pwxx.vhd
file copy -force $cr_sources_dir/reg8to16bit.vhd $user_cr_sources_dir/reg8to16bit.vhd
file copy -force $cr_sources_dir/SCDataMANAGER.vhd $user_cr_sources_dir/SCDataMANAGER.vhd
file copy -force $cr_sources_dir/upstreamEpathFifoWrap.vhd $user_cr_sources_dir/upstreamEpathFifoWrap.vhd



# --------------------------------------------------------------------------
#          centralRouter cores
# --------------------------------------------------------------------------
file copy -force $core_dir/EPATH_FIFO_bif.xci $user_core_dir/EPATH_FIFO_bif.xci
file copy -force $core_dir/hdlc_bist_fifo.xci $user_core_dir/hdlc_bist_fifo.xci
file copy -force $core_dir/fh_epath_fifo2K_18bit_wide.xci $user_core_dir/fh_epath_fifo2K_18bit_wide.xci


# --------------------------------------------------------------------------
#          GBTlinksDataEmulator cores
# --------------------------------------------------------------------------
file copy -force $core_dir/emuram_2.xci $user_core_dir/emuram_2.xci
file copy -force $core_dir/emurom_2.coe $user_core_dir/emurom_2.coe



# --------------------------------------------------------------------------
#      all packages
# --------------------------------------------------------------------------
file copy -force $firmware_dir/sources/packages/centralRouter_package.vhd $user_firmware_dir/sources/packages/centralRouter_package.vhd
file copy -force $proj_sources_dir/elinkInterface_package.vhd $user_proj_sources_dir/elinkInterface_package.vhd



# --------------------------------------------------------------------------
#      local cores
# --------------------------------------------------------------------------
file copy -force $cr_sources_dir/crTOPaux/CR_CLKs.xci $user_cr_sources_dir/crTOPaux/CR_CLKs.xci



# --------------------------------------------------------------------------
#      constraints
# --------------------------------------------------------------------------



# --------------------------------------------------------------------------
#      simulation sources
# --------------------------------------------------------------------------
file copy -force $proj_sources_dir/TB_elinkInterface_top.vhd $user_proj_sources_dir/TB_elinkInterface_top.vhd

exec tar -zcvf ${TARGET}.tar.gz $TARGET

puts "INFO: Done!"







