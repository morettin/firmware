#!/bin/bash
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Mark Donszelmann
#               Frans Schreuder
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

set -e
#
#	create examples tar.gz file
#
#

usage()
{
  echo "usage: $(basename $0) <name> <svn-version>"
  #exit 1
}

if [ $# -lt 2 ]; then
    usage
	COMMIT_DATETIME=$(git show -s --format=%cd --date=iso)
	YY=${COMMIT_DATETIME:2:2}
	MM=${COMMIT_DATETIME:5:2}
	DD=${COMMIT_DATETIME:8:2}
	hh=${COMMIT_DATETIME:11:2}
	mm=${COMMIT_DATETIME:14:2}
	COMMIT_DATETIME=${YY}${MM}${DD}_${hh}_${mm}
	git_tag_str=$(git describe --abbrev=0 --tags)
	GIT_COMMIT_NUMBER=$(git rev-list ${git_tag_str}..HEAD)
	GIT_COMMIT_NUMBER=$(echo "$GIT_COMMIT_NUMBER" | wc -l)
	GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
	TARGET_NAME=elinkInterfaceSources
	TARGET_VERSION=GIT_${GIT_BRANCH}_${git_tag_str}_${GIT_COMMIT_NUMBER}_${COMMIT_DATETIME}
	TARGET=${TARGET_NAME}-${TARGET_VERSION}
	echo ${TARGET}
else
	TARGET_NAME=${1}
	TARGET_VERSION=${2}

fi

TARGET=${TARGET_NAME}-s${TARGET_VERSION}

# Set the support files directory path
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# firmware directory:
FIRMWARE_DIR=${SCRIPT_DIR}/../../
DOC_DIR=${SCRIPT_DIR}/../../../documents
CORE_DIR=${FIRMWARE_DIR}/sources/ip_cores/virtex7/
# sources directories:
CR_SOURCES_DIR=${FIRMWARE_DIR}/sources/centralRouter/
PROJ_SOURCES_DIR=${FIRMWARE_DIR}/sources/felixUserSupport/elinkInterface/
# source share directory path
SHARE_DIR=/tmp
#

mkdir -p ${SHARE_DIR}/${TARGET}

# --------------------------------------------------------------------------
#      local elinkInterface sources
# --------------------------------------------------------------------------
cp ${PROJ_SOURCES_DIR}/elinkInterface_top.vhd ${SHARE_DIR}/${TARGET}
cp ${PROJ_SOURCES_DIR}/Elink2FIFO.vhd ${SHARE_DIR}/${TARGET}
cp ${PROJ_SOURCES_DIR}/FIFO2Elink.vhd ${SHARE_DIR}/${TARGET}


# --------------------------------------------------------------------------
#      relevant Central Router sources
# --------------------------------------------------------------------------
cp ${CR_SOURCES_DIR}/8b10_dec.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/8b10_dec_wrap.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/BLOCK_WORD_COUNTER.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/CD_COUNTER.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/CRresetManager.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/enc_8b10.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/enc8b10_wrap.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPATH_FIFO_WRAP.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_FIFO_DRIVER.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN2.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN2_ALIGN_BLOCK.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN2_DEC8b10b.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN2_direct.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN2_HDLC.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN4.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN4_ALIGN_BLOCK.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN4_DEC8b10b.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN4_direct.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN8.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN8_ALIGN_BLOCK.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN8_DEC8b10b.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN8_direct.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN16.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN16_ALIGN_BLOCK.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN16_DEC8b10b.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_IN16_direct.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_OUT2.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_OUT2_direct.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_OUT2_ENC8b10b.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_OUT2_HDLC.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_OUT4.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_OUT4_direct.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_OUT4_ENC8b10b.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_OUT8.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/EPROC_OUT8_ENC8b10b.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/KcharTest.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/MUX2_Nbit.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/MUX4.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/MUX4_Nbit.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/MUX8_Nbit.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/pulse_fall_pw01.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/pulse_pdxx_pwxx.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/reg8to16bit.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/SCDataMANAGER.vhd ${SHARE_DIR}/${TARGET}
cp ${CR_SOURCES_DIR}/upstreamEpathFifoWrap.vhd ${SHARE_DIR}/${TARGET}



# --------------------------------------------------------------------------
#          centralRouter cores
# --------------------------------------------------------------------------
cp ${CORE_DIR}/EPATH_FIFO.xci ${SHARE_DIR}/${TARGET}
cp ${CORE_DIR}/hdlc_bist_fifo.xci ${SHARE_DIR}/${TARGET}
cp ${CORE_DIR}/fh_epath_fifo2K_18bit_wide.xci ${SHARE_DIR}/${TARGET}


# --------------------------------------------------------------------------
#          GBTlinksDataEmulator cores
# --------------------------------------------------------------------------
cp ${CORE_DIR}/emuram_2.xci ${SHARE_DIR}/${TARGET}
cp ${CORE_DIR}/emurom_2.coe ${SHARE_DIR}/${TARGET}



# --------------------------------------------------------------------------
#      all packages
# --------------------------------------------------------------------------
cp ${FIRMWARE_DIR}/sources/packages/centralRouter_package.vhd ${SHARE_DIR}/${TARGET}
cp ${PROJ_SOURCES_DIR}/elinkInterface_package.vhd ${SHARE_DIR}/${TARGET}



# --------------------------------------------------------------------------
#      local cores
# --------------------------------------------------------------------------
cp ${CR_SOURCES_DIR}/crTOPaux/CR_CLKs.xci ${SHARE_DIR}/${TARGET}



# --------------------------------------------------------------------------
#      constraints
# --------------------------------------------------------------------------



# --------------------------------------------------------------------------
#      simulation sources
# --------------------------------------------------------------------------
cp ${PROJ_SOURCES_DIR}/TB_elinkInterface_top.vhd ${SHARE_DIR}/${TARGET}

#
# Documentation
#
cp ${DOC_DIR}/UserExamples/e-link_Interface_fw_UG.pdf  ${SHARE_DIR}/${TARGET}

tar zcvf ${TARGET}.tar.gz -C ${SHARE_DIR} ${TARGET}
