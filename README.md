# Readme for developers
Point of contact: Frans Schreuder <f.schreuder@nikhef.nl>
## Cloning the repository
This FELIX firmware repository can be cloned on it's own, and contains all the necessary files to generate a bitfile or run a simulation. Some dependencies, like WupperCodeGen are added as a git submodule.

```
#with only some scripts.
git clone ssh://git@gitlab.cern.ch:7999/atlas-tdaq-felix/firmware.git
cd firmware
#Update all submodules
git submodule update --init --recursive
#Now you have a complete FELIX firmware directory tree
```

## Building firmware
Vivado 2021.2 is recommended for building the firmware (FLX709, FLX712), but for the Versal Prime VM1802 (FLX182) devices, Vivado 2022.2 is used. Vivado 2023.2 is used for VP1552 (FLX155)

To create a bitfile from the sources in this repository, two steps are required, but first Vivado and the Xilinx licenses must be available. At Cern, Vivado is installed on the following path:
 `/eos/project/f/felix/xilinx/Vivado/`

To use Vivado at Cern, one can issue the following commands:
```
export XILINXD_LICENSE_FILE="2112@licenxilinx"
source /eos/project/f/felix/xilinx/Vivado/<version>/settings64.sh
vivado
```
**Other flavors**, e.g. FELIG, also have dedicated tcl scripts.

### Creating a project for branches based on phase2/master
In Phase 2 the different flavours are generated from the same project and the same toplevel VHDL file. Therefore the different flavours are built using different do_implementation*.tcl scripts.
**For all flavours**
```
cd felix/firmware/scripts/FELIX_top
vivado
```
Then from the Vivado tcl console:
```
#To create the project, replace FLX712 with FLX709 when building for the VC709 card
source ./FLX712_FELIX_import_vivado.tcl
#To run synthesis, implementation and create a bitstream in felix/firmware/output
source ./do_implementation_BNL712_GBT_24ch.tcl #GBT mode
source ./do_implementation_BNL712_FULL_24ch.tcl #FULL mode
#There are more flavours in that directory that can be built with similar scripts
```

### Simulation
The FELIX firmware is verified per functional block, not as an entire design. There are several testbenches that can be found in the repository.

As a simulation library, UVVM is used to help with utilities and functional models. VUnit is used to streamline the simulation process.

The VUnit simulation has some dependencies:
* Vivado 2021.2, assumed to be installed in /opt/Xilinx/Vivado/2021.2/
* Questasim 2019.1, assumed to be installed in /opt/questasim-2019.1/ 
** Instead of questasim, you can also use modelsim or GHDL. Some simulations will fail with GHDL due to the encrypted IP of transceivers
* The tcl filesets are loaded in VUnit using python3-tkinter. The package `python3-tkinter` must be installed on the system
* VUnit can be installed by running `pip3 install --user vunit-hdl`

To run a certain testbench
```
#Source Vivado settings:
~/felix/firmware/simulation/VUnit$ source /opt/Xilinx/Vivado/2021.2/settings64.sh 
#Add questasim to the path:
~/felix/firmware/simulation/VUnit$ export PATH=$PATH:/opt/questasim-2019.1/bin
#List available simulations in VUnit
~/felix/firmware/simulation/VUnit$ ./run.py -l
lib.ltittc_vunit_tb.all
lib.lpgbtlinktohost_vunit_tb.all
lib.tb_lcb_command_decoder_vunit.all
lib.decegroup_8b10b_vunit_tb.all
lib.tb_r3l1_regmap_vunit.all
lib.wuppergen4_vunit_tb.all
lib.tb_r3l1_frame_generator_vunit.all
lib.encodingepath_vunit_tb.all
lib.tb_amac_deglitcher_vunit.all
lib.crtohost_vunit_tb.all
lib.ttc_lti_transmitter_vunit_tb.all
lib.busyvirtualelink_vunit_tb.all
lib.decodingpixel_vunit_tb.all
lib.i2c_vunit_tb.all
lib.tb_lcb_regmap_vunit.all
lib.tb_trickle_trigger_vunit.all
lib.tb_lcb_axi_encoder_vunit.all
lib.hgtd_fastcmd_vunit_tb.all
lib.tb_r3l1_scheduler_encoder_vunit.all
lib.crc20_vunit_tb.all
lib.ttctohostvirtualelink_vunit_tb.all
lib.validate_8b10b_vunit_tb.all
lib.crfromhost_vunit_tb.all
lib.loopback25g_vunit_tb.all
lib.tb_lcb_frame_generator_vunit.all
lib.gbtcrcoding_vunit_tb.all
lib.amac_demo_vunit_tb.all
lib.gbtlinktohost_vunit_tb.all
lib.tb_lcb_scheduler_encoder_vunit.all
lib.tb_r3l1_axi_encoder_vunit.all
lib.fullmodetohost_vunit_tb.all
lib.tb_bypass_frame_vvc_vunit.all
lib.tb_strips_configuration_decoder_vunit.all
lib.tb_r3l1_frame_synchronizer_vunit.all
lib.tb_amac_encoder_vunit.all
lib.decodinggearbox_vunit_tb.all
lib.tb_playback_controller_vunit.all
lib.tb_amac_decoder_vunit.all
lib.tb_l0a_frame_generator_vunit.all
lib.wupper_vunit_tb.all
lib.tb_bypass_scheduler_continuous_write_vunit.all
Listed 41 tests
#Run the simulation of your choice in GUI mode
~/felix/firmware/simulation/VUnit$ ./run.py lib.wupper_vunit_tb.all --gui
```

The UVVM testbenches can be found in `firmware/simulation/UVVMtests/tb` (although some testbenches can also be found at other places).

VUnit uses separate testbenches which are simple wrappers around the UVVM testbenches, they can be found in `firmware/simulation/VUnit/tb`

### Sharing bitfiles
Bitfiles can be shared through the Cernbox user interface. For details, and other useful information about firmware development, please check the [felix-developer-manual](https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/felix-developer-manual.html#sec:firmware)
