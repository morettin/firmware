--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene Habraken
--!               RHabraken
--!               Mesfin Gebyehu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: Radboud University Nijmegen
-- Engineer: Rene Habraken
-- 
-- Create Date: 01/25/2019 02:28:15 PM
-- Design Name: Finite State Machine for Full mode emulator
-- Module Name: FMEmu_FSM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
--  State Machine Options:
--  Clock : CLK240 (Rising edge).
--  State assignment : Enumerate.
--  State decoding : Case construct.
--  Actions on transitions : Comb.(in 1 Block).
--  Actions on states : Comb.(in 1 Block).

-- Datatype:
--    - 00: intermediate word of current packet
--    - 01: start of packet
--    - 10: end of packet
--    - 11: idle
----------------------------------------------------------------------------------


library ieee, UNISIM;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.pcie_package.all;


entity FMEmu_FSM is
  port (
    CLK240              : in     std_logic;
    REC_CLK40           : in     std_logic;
    register_map_control: in     register_map_control_type;
    Xon_TTC             : in     std_logic_vector(0 downto 0);
    L1A_Incr            : in     std_logic; -- signal coming from ttc decoders
    RST                 : in     std_logic;
    CRC                 : in     std_logic_vector(31 downto 0);
    DataToCRC           : out    std_logic_vector(31 downto 0);
    CALC_CRC            : out    std_logic;
    CLR_CRC             : out    std_logic;
    RAM_EN              : out    std_logic;
    RAM_DOUT            : in     std_logic_vector(31 downto 0);
    RAM_DVALID          : in     std_logic;
    FIFO256b32b_DOUT : in   std_logic_vector(31 downto 0);
    FIFO256b32b_DVALID  : in     std_logic;
    FIFO256b32b_EMPTY   : in     std_logic;
    FIFO256b32b_RE      : out    std_logic;
    EMUout              : out    std_logic_vector(31 downto 0);
    EMUout_dtype        : out    std_logic_vector(1 downto 0);
    FIFO34b_full        : in     std_logic;
    FIFO34b_WE          : out    std_logic;
    BUSY_ON             : out    std_logic);
end entity FMEmu_FSM;


architecture fsm of FMEmu_FSM is

  signal sBUSY_TH_HIGH_RM                       : unsigned(7 downto 0) := (OTHERS => '0');
  signal sBUSY_TH_LOW_RM                        : unsigned(7 downto 0) := (OTHERS => '0');
  signal sIDLE_CNT_RM                           : std_logic_vector(15 downto 0) := (OTHERS => '0');
  signal sINCL_CRC32_RM                         : std_logic_vector(0 downto 0) := (OTHERS => '0');
  signal sL1A_CNT_TTCmode                       : std_logic := '0';
  signal sTTC_MODE_RM                           : std_logic_vector(0 downto 0) := (OTHERS => '0');
  signal sEMU_START_r, sEMU_START_r1            : std_logic_vector(0 downto 0) := (OTHERS => '0');
  signal sEMU_START_r2, sEMU_START_r3           : std_logic_vector(0 downto 0) := (OTHERS => '0');
  signal sEMU_START_pulse                       : std_logic_vector(0 downto 0) := (OTHERS => '0');
  signal sXONXOFF_RM                            : std_logic_vector(0 downto 0) := (OTHERS => '0');
  signal sBUSY_ON_IsSet                         : std_logic := '0';
  signal sIDLE_CNT, sIDLE_CNT_r                 : std_logic_vector(15 downto 0) := (OTHERS => '0');
  signal sWORD_CNT, sWORD_CNT_act               : unsigned(15 downto 0) := (OTHERS => '0');
  signal sL1ID, sL1ID_r                         : unsigned(31 downto 0) := (OTHERS => '0');
  signal sBCID, sBCID_RM, sBCID_r1              : unsigned(31 downto 0) := (OTHERS => '0');
  signal sL1A_CNT,sL1A_CNT_r                    : unsigned(15 downto 0) := (OTHERS => '0');
  signal sL1A_CNT_RM                            : unsigned(15 downto 0) := (OTHERS => '0');
  signal sDATA_SRC_SEL_RM                       : std_logic_vector(0 downto 0) := (OTHERS => '0');
  signal sOutputSel, sOutputSel_d1              : std_logic := '0';
  signal sL1A_Decr, sL1A_Decr_r, sL1A_Decr_r1   : std_logic := '0';
  signal sL1A_Incr_r, sL1A_Incr_r1              : std_logic := '0';
  signal sXon                                   : std_logic_vector(0 downto 0) := (OTHERS => '0');
  signal sXon_TTC_r                             : std_logic_vector(0 downto 0) := (OTHERS => '0');
  signal sXon_TTC_pulse                         : std_logic_vector(0 downto 0) := (OTHERS => '0');
  
  type state_type is (SETUP_EMU, PAUSE_EMU, SEND_L1ID, SEND_BCID, SEND_DATA, 
            SEND_CRC) ;
  signal cur_state  : state_type  := SETUP_EMU;
  signal next_state : state_type ;
  
begin
-- process to monitor: L1A_CNT
  process (CLK240) is
  
  variable vL1A_CntUP, vL1A_CntDN : std_logic := '0';
  begin
    if (RST = '1') then
      sL1A_Decr_r <= '0';
      sL1A_Incr_r <= '0';
    elsif (rising_edge(CLK240) ) then
      if (cur_state=SETUP_EMU) then
         sL1A_CNT <= sL1A_CNT_r;
      end if;
     sL1A_Incr_r <= L1A_Incr; -- from TTC information
     sL1A_Incr_r1 <= sL1A_Incr_r;
     sL1A_Decr_r <= sL1A_Decr;
     sL1A_Decr_r1 <= sL1A_Decr_r;
     
     if sL1A_Incr_r1 = '0' and sL1A_Incr_r = '1' then -- create cnt dn pulse
       vL1A_CntUP := '1';
     else 
       vL1A_CntUP := '0';
     end if;

     if sL1A_Decr_r1 = '0' and sL1A_Decr_r = '1' then -- create cnt dn pulse
       vL1A_CntDN := '1';
     else 
       vL1A_CntDN := '0';
     end if;

      if (sL1A_CNT_TTCmode <= '1') then -- only increment in TTC mode
        if ( (vL1A_CntUP = '1') and (vL1A_CntDN ='0') )then -- only increment in TTC mode
         sL1A_CNT <= sL1A_CNT + 1;
        elsif ( (vL1A_CntUP = '1') and (vL1A_CntDN ='1') ) then
         sL1A_CNT <= sL1A_CNT;
       elsif ( (vL1A_CntUP = '0') and (vL1A_CntDN ='1') ) then
         sL1A_CNT <= sL1A_CNT - 1;
        end if;
     else --(sL1A_CNT_TTCmode <= '0')
        if (vL1A_CntDN ='1') then
        sL1A_CNT <= sL1A_CNT - 1;
       end if;
      end if;

    -- busy logic
    if (sL1A_CNT >= sBUSY_TH_HIGH_RM) then
      BUSY_ON <= '1';
      sBUSY_ON_IsSet <= '1';
    elsif ( (sL1A_CNT >= sBUSY_TH_LOW_RM) and (sBUSY_ON_IsSet = '1') ) then 
      BUSY_ON <= '1';
      sBUSY_ON_IsSet <= '1';
    elsif (sBUSY_ON_IsSet = '1') then 
      BUSY_ON <= '0';
      sBUSY_ON_IsSet <= '0';
    else --(sL1A_CNT <= sBUSY_TH_HIGH_RM and (sL1A_CNT >= sBUSY_TH_LOW_RM) or < sBUSY_TH_LOW_RM and (sBUSY_ON_IsSet = '0')
      BUSY_ON <= '0';
      sBUSY_ON_IsSet <= '0';          
    end if;       
  
    end if;
  end process;
 
-- process to monitor: BCID_update  
  process (REC_CLK40) is
  begin
    if (RST = '1') then
      sBCID <= (OTHERS => '0');
    elsif (rising_edge(REC_CLK40)) then
      if (register_map_control.FM_EMU_CONTROL.BCR = "1") then
        sBCID <= x"00000000";
      else
        sBCID_r1 <= sBCID_RM;
        if (sBCID_r1 /= sBCID_RM) then
          sBCID <= sBCID_RM;
        else
          sBCID <= sBCID + 1; 
        end if;
      end if;         
    end if;
  end process;
  
  process (CLK240) is
  begin -- State Transition and RST
    if (RST = '1') then --or (register_map_control.FM_EMU_CONTROL.EMU_START(0) = '0') then
      cur_state <= SETUP_EMU;
    elsif (rising_edge(CLK240)) then
      cur_state <= next_state ;
      -- voeg register toekenningen toe van de counters
      sWORD_CNT <= sWORD_CNT_act;
      if (register_map_control.FM_EMU_CONTROL.ECR = "1") then
        sL1ID <=  (OTHERS => '0');
      else
        sL1ID <=  sL1ID_r;
      end if;
      sEMU_START_r1 <= register_map_control.FM_EMU_CONTROL.EMU_START;
      sEMU_START_r2 <= sEMU_START_r1;      
      sEMU_START_r3 <= sEMU_START_r2;
      if (sEMU_START_r3 = "0" and sEMU_START_r2 = "1") then
        sEMU_START_pulse <= "1";
      else
        sEMU_START_pulse <= "0";       
      end if;
      sIDLE_CNT <= sIDLE_CNT_RM;
      
      -- handle XonXoff
      if (FIFO34b_full = '1') then
        sXon <= "0"; --XOFF!
      elsif (register_map_control.FM_EMU_CONTROL.TTC_MODE = "1") then
        sXon_TTC_r <= Xon_TTC;
          if (Xon_TTC = "1" and sXon_TTC_r = "0") then
            sXon_TTC_pulse <= "1";
          elsif (Xon_TTC = "0" and sXon_TTC_r = "1") then
            sXon_TTC_pulse <= "0";
          else 
            sXon_TTC_pulse <= sXon_TTC_pulse;
          end if;
        sXon <= sXon_TTC_pulse;
      else -- (register_map_control.XXX.TTC_MODE = '0')
        sXon <= register_map_control.FM_EMU_CONTROL.XONXOFF; -- default '0' --> regmap mode 
      end if;
    end if ; -- Clock
  end process;

  next_state_decoding: process (cur_state, sL1A_CNT_RM,
  sWORD_CNT, sIDLE_CNT, sL1A_CNT, sL1ID, CRC, sXon, sEMU_START_pulse, sEMU_START_r1,sEMU_START_r3,
  register_map_control.FM_EMU_CONTROL.EMU_START, sWORD_CNT_act,
  RAM_DVALID, sINCL_CRC32_RM, RST, sBUSY_TH_HIGH_RM, sBUSY_TH_LOW_RM, sXONXOFF_RM,
  sL1A_CNT_r, sL1ID_r, sBCID, sBCID_RM, sTTC_MODE_RM) is
--    variable vData_rdy  : std_logic := '0';

  begin
    next_state <= cur_state ;
    -- declare defaults here (makes sure that signals return to their 
    -- correct value after being set in a state.
    EMUout_dtype <="11"; -- default send idle
    EMUout <= x"FFFFFFFF";
    DataToCRC <= x"FFFFFFFF";
    CALC_CRC <= '0';
    FIFO34b_WE <= '0';   
    CLR_CRC <=  '0';
    RAM_EN <=  '0';
    FIFO256b32b_RE <=  '0';
    sL1A_Decr <=  '0';
    sL1A_CNT_r  <= sL1A_CNT;
    sIDLE_CNT_RM <= sIDLE_CNT; 
    sINCL_CRC32_RM <= register_map_control.FM_EMU_CONTROL.INLC_CRC32;
    sBUSY_TH_HIGH_RM <= unsigned(register_map_control.FM_EMU_COUNTERS.BUSY_TH_HIGH);
    sBUSY_TH_LOW_RM <= unsigned(register_map_control.FM_EMU_COUNTERS.BUSY_TH_LOW);
    sL1A_CNT_RM <= unsigned(register_map_control.FM_EMU_COUNTERS.L1A_CNT); -- 0xFFFF send data continuously
    sL1ID_r <= sL1ID;
    sBCID_RM <= sBCID;
    sWORD_CNT_act <= sWORD_CNT;       
    sTTC_MODE_RM <= register_map_control.FM_EMU_CONTROL.TTC_MODE; -- default '0' --> regmap mode
    sDATA_SRC_SEL_RM <= register_map_control.FM_EMU_CONTROL.DATA_SRC_SEL; -- default '0' --> data from emu ram 

    case cur_state is
-- SETUP_EMU -----------------------------------------------------------
      -- Init: 
      when SETUP_EMU =>
        sWORD_CNT_act <= (OTHERS=> '0');
        sIDLE_CNT_RM <= register_map_control.FM_EMU_COUNTERS.IDLE_CNT; 
        sINCL_CRC32_RM <= register_map_control.FM_EMU_CONTROL.INLC_CRC32;
        sBUSY_TH_HIGH_RM <= unsigned(register_map_control.FM_EMU_COUNTERS.BUSY_TH_HIGH);
        sBUSY_TH_LOW_RM <= unsigned(register_map_control.FM_EMU_COUNTERS.BUSY_TH_LOW);
        sL1A_CNT_RM <= unsigned(register_map_control.FM_EMU_COUNTERS.L1A_CNT); -- 0xFFFF send data continuously
        sL1ID_r <= unsigned(register_map_control.FM_EMU_EVENT_INFO.L1ID);     -- initialise L1ID from regmap
        sBCID_RM <= unsigned(register_map_control.FM_EMU_EVENT_INFO.BCID);     -- initialise BCID from regmap            
        sTTC_MODE_RM <= register_map_control.FM_EMU_CONTROL.TTC_MODE; -- default '0' --> regmap mode
        CLR_CRC <=  '1';
        --sDATA_SRC_SEL_RM <= register_map_control.FM_EMU_CONTROL.DATA_SRC_SEL;
        
        -- load and configure L1A counter
        if (sTTC_MODE_RM = "1") then -- use L1A counter from TTC
          sL1A_CNT_TTCmode <= '1';
          sL1A_CNT_r <= (OTHERS => '0');         
        else -- use L1A counter from regmap
          sL1A_CNT_TTCmode <= '0';
          sL1A_CNT_r <= sL1A_CNT_RM;
        end if;
         
        -- next_state logic   
        if (  (sEMU_START_pulse = "0") or ( (sTTC_MODE_RM = "0") and (sL1A_CNT = x"0000") )  ) then
          next_state <= SETUP_EMU; -- prevent start of emu from reg map without L1A_CNT set to other value than '0'
        else
          next_state <= PAUSE_EMU;
        end if;

-- PAUSE_EMU -----------------------------------------------------------     
      when PAUSE_EMU =>
        EMUout_dtype <= "11";
        EMUout <= x"FFFFFFFF";
        DataToCRC <= x"FFFFFFFF";
        CALC_CRC <= '0';
        FIFO34b_WE <= '1';
        if (sIDLE_CNT /= x"0000") then
          sIDLE_CNT_RM <= sIDLE_CNT - 1;
        else
          sIDLE_CNT_RM <= sIDLE_CNT;
        end if;
                
        -- next_state logic
        if ( (sXon = "0") or (sL1A_CNT = x"0000") or (sIDLE_CNT /= "0000000000000000") ) then
          next_state <= PAUSE_EMU ; -- wait for Xon, L1A trigger (in TTC mode) or finish idles
        elsif (sWORD_CNT /= x"0000") then
          next_state <= SEND_DATA;
        else
          next_state <= SEND_L1ID;
        end if;
        
-- SEND_l1ID -----------------------------------------------------------
      -- send L1ID + data type to FIFO34
      when SEND_L1ID =>
        sIDLE_CNT_RM <= register_map_control.FM_EMU_COUNTERS.IDLE_CNT; -- reset IDLE counter to reg map value 
        EMUout_dtype <= "01"; -- start of packet
        EMUout <= std_logic_vector(sL1ID);
        DataToCRC <= std_logic_vector(sL1ID);
        CALC_CRC <= '1';     
        sL1ID_r <= sL1ID + 1;
        FIFO34b_WE <= '1';
        sWORD_CNT_act <= unsigned(register_map_control.FM_EMU_COUNTERS.WORD_CNT);
      
        -- "trigger L1A_CNT decrease in clocked process
        if (sL1A_CNT /= x"FFFF") then -- 0xFFFF set by regmap and loop data continiously
          sL1A_Decr <= '1';
        end if;
      
        -- next_state logic
        next_state <= SEND_BCID ; --always send L1ID and BCID
        
-- SEND_BCID -----------------------------------------------------------        
      -- send BCID + data type to FIFO34 
      when SEND_BCID =>
        EMUout_dtype <= "00"; -- intermediate word of current packet
        EMUout <= std_logic_vector(sBCID);
        DataToCRC <= std_logic_vector(sBCID);
        CALC_CRC <= '1';
        FIFO34b_WE <= '1';
   
        -- next_state logic
        if(sXon = "0") then
          next_state <= PAUSE_EMU ;
        else
          next_state <= SEND_DATA ;
        end if ;

-- SEND_DATA -----------------------------------------------------------        
      -- send payload + data type to FIFO34
      when SEND_DATA =>
        if  (sDATA_SRC_SEL_RM = "0") then-- default val, send RAM data
          if (sWORD_CNT <= 2) then 
            RAM_EN <= '0';
          else
            RAM_EN <= '1';
          end if;
          if (RAM_DVALID = '1') then 
            if (sWORD_CNT = 0 and sINCL_CRC32_RM = "0") then
              EMUout_dtype <= "10"; -- End Of Packet        
            else 
              EMUout_dtype <= "00"; -- intermediate word of current packet
            end if;
            EMUout <= RAM_DOUT;
            DataToCRC <= RAM_DOUT;
            CALC_CRC <= '1';
            FIFO34b_WE <= '1';
            if(sWORD_CNT /= x"00000000") then
              sWORD_CNT_act <= sWORD_CNT - 1;      
            else 
              sWORD_CNT_act <= sWORD_CNT;
            end if;        
          else
            CALC_CRC <= '0';
            FIFO34b_WE <= '0';          
          end if;
        else  -- sDATA_SRC_SEL_RM = '1' then send SW data
          if (sWORD_CNT <= 2) then 
            FIFO256b32b_RE <= '0';
          else
            FIFO256b32b_RE <= '1';
          end if;
          if (FIFO256b32b_EMPTY = '0' and FIFO256b32b_DVALID = '1') then
            if (sWORD_CNT = 0 and sINCL_CRC32_RM = "0") then 
              EMUout_dtype <= "10"; -- End Of Packet        
            else 
              EMUout_dtype <= "00"; -- intermediate word of current packet
            end if;
            EMUout <= FIFO256b32b_DOUT;
            DataToCRC <= FIFO256b32b_DOUT;
            CALC_CRC <= '1';
            FIFO34b_WE <= '1';
            if(sWORD_CNT /= x"00000000") then
              sWORD_CNT_act <= sWORD_CNT - 1;        
            else 
              sWORD_CNT_act <= sWORD_CNT;            
            end if;        
          else
            CALC_CRC <= '0';
            FIFO34b_WE <= '0';           
          end if;
        end if;
      
        -- next_state logic   
        if (sXon = "0") then
          next_state <= PAUSE_EMU ;
        elsif (sWORD_CNT /= x"0000") then
          next_state <= SEND_DATA ;
        elsif (sINCL_CRC32_RM = "0") then
          if (sL1A_CNT = x"0000") then
            next_state <= SETUP_EMU ; -- reinit emulator
          else -- (sL1A_CNT /= x"0000") 
            next_state <= PAUSE_EMU ; -- start next frame
          end if ;
        else -- (sINCL_CRC32_RM = "1") 
          next_state <= SEND_CRC ;
        end if ;
        
-- SEND_CRC ------------------------------------------------------------        
      -- append data with CRC
      when SEND_CRC =>
        EMUout_dtype <= "10"; -- End Of Packet
        EMUout <= CRC;
        CALC_CRC <= '0';          
        FIFO34b_WE <= '1';
      
        -- next_state logic 
        if (sXon = "0") then
          next_state <= PAUSE_EMU ;
        elsif (sL1A_CNT = x"0000") then
          next_state <= SETUP_EMU ;
        else --(sL1A_CNT /= x"0000")
          next_state <= PAUSE_EMU ;
        end if ;
    end case;
  end process next_state_decoding ;
end architecture fsm ; -- of FMEmu_FSM

