--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               RHabraken
--!               Mesfin Gebyehu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.pcie_package.all;

entity OUTPUTctrl is
  port (
    CRC                  : in     std_logic_vector;
    CRC_Din              : out    std_logic_vector(31 downto 0);
    Calc                 : out    std_logic;
    Clr_CRC              : out    std_logic;
    EMU32b_EN            : out    std_logic;
    EMU32b_dout          : in     std_logic_vector(31 downto 0);
    EMU32b_dvalid        : in     std_logic;
    FIFO256b32b_RE       : out    std_logic;
    FIFO32b_dvalid       : in     std_logic;
    FIFO32b_empty        : in     std_logic;
    FIFO34b_WE           : out    std_logic;
    clk240               : in     std_logic;
    fifo32_dout          : in     std_logic_vector(31 downto 0);
    fifo34_din           : out    std_logic_vector(31 downto 0);
    fifo34_dtype         : out    std_logic_vector(1 downto 0);
    fifo34_full          : in     std_logic;
    register_map_control : in     register_map_control_type;
	clk40				 : in     std_logic;
    rst                  : in     std_logic;
    busy_in              : in     std_logic;
    L1A_in               : in     std_logic);
end entity OUTPUTctrl;



architecture rtl of OUTPUTctrl is

signal rEMU_ENABLE: std_logic := '0';
signal rPACKAGE_COUNT : std_logic_vector(15 downto 0) := (others => '0');
signal rPACKAGE_LENGTH : std_logic_vector(15 downto 0) := (others => '0');
signal rTIMESTAMP_INIT : std_logic_vector(63 downto 0) := (others => '0');
signal rTIMESTAMP_INCR : std_logic_vector(63 downto 0) := (others => '0');

signal outputctrl_st : integer := 0;

signal PACKAGE_COUNT_t : std_logic_vector(15 downto 0) := (others => '0');
signal PACKAGE_LENGTH_t : std_logic_vector(15 downto 0) := (others => '0');
signal EMU32b_dvalid_d : std_logic := '0';

signal FrameCnt : std_logic_vector(31 downto 0) := (others => '0');
signal fifo34_din_d : std_logic_vector(31 downto 0) := (others => '0');
signal rg_doutb : std_logic_vector(15 downto 0) := (others => '0');
signal rg_rst : std_logic := '0';
signal rg_enb : std_logic := '0';

constant comma32word    : std_logic_vector (31 downto 0) := "00000000000000000000000010111100"; -- K28.5
constant eop32word      : std_logic_vector (31 downto 0) := "00000000000000000000000011011100"; -- K28.6
constant sop32word      : std_logic_vector (31 downto 0) := "00000000000000000000000000111100"; -- K28.1

begin

  rEMU_ENABLE <= to_sl(register_map_control.FMEMU_CONTROL.EMU_START);
  rPACKAGE_COUNT <=  register_map_control.FMEMU_COUNTERS.WORD_CNT;--x"FFFF" default (16b field), set to x"FFFF" for streaming mode
---- %%%%%%% for test only
   
process (clk240, rst)


begin
   if (rising_edge(clk240)) then
      if (rst = '1' ) then 
		outputctrl_st <= 0;
		PACKAGE_COUNT_t <= (others => '0');
		PACKAGE_LENGTH_t <= (others => '0');
		EMU32b_dvalid_d <= '0';
		EMU32b_EN <= '0';
		fifo34_dtype <= "11";
		FIFO34b_WE <= '0';
		fifo34_din <= (others => '0');
		FrameCnt <= (others => '0');
		rg_rst <= '1';
		rg_enb <= '0';
      else
		EMU32b_EN <= '0';
		rg_rst <= '0';
		rg_enb <= '0';
		case outputctrl_st is
			when 0 =>
				if(rEMU_ENABLE = '1' and rPACKAGE_COUNT /= x"0000") then
					outputctrl_st <= outputctrl_st + 1;
					PACKAGE_COUNT_t <= rPACKAGE_COUNT;
					FrameCnt <= (others => '0');
				end if;
			when 1 =>
				PACKAGE_LENGTH_t <= rg_doutb;
				if(rEMU_ENABLE = '1' ) then 
					outputctrl_st <= outputctrl_st + 1;
				else
					outputctrl_st <= 0;
				end if;	
			when 2 =>
				if(PACKAGE_LENGTH_t = X"0000") then
					outputctrl_st <= outputctrl_st + 1;
				else	
					EMU32b_EN <= '1';
					PACKAGE_LENGTH_t <= PACKAGE_LENGTH_t - 1;
				end if;
			when 3 =>
				FrameCnt <= FrameCnt + 1;
				outputctrl_st <= outputctrl_st + 1;
			when 4 =>
				outputctrl_st <= outputctrl_st + 1;
			when 5 =>
				rg_enb <= '1';
				outputctrl_st <= outputctrl_st + 1;
			when 6 =>
				if(PACKAGE_COUNT_t /= x"FFFF") then
					if(PACKAGE_COUNT_t /= x"0000") then
						PACKAGE_COUNT_t <= PACKAGE_COUNT_t - 1;
						outputctrl_st <= 1;
					else
						outputctrl_st <= 0;
					end if;
				else
					outputctrl_st <= 1;
				end if;	
			when others => NULL;
		end case;

		EMU32b_dvalid_d <= EMU32b_dvalid;
		--fifo34_din <= fifo34_din_d;
		if( EMU32b_dvalid_d = '0' and EMU32b_dvalid = '1') then
			fifo34_dtype <= "01";
			fifo34_din <= FrameCnt;
		elsif( EMU32b_dvalid_d = '1' and EMU32b_dvalid = '0') then
			fifo34_dtype <= "10";
			fifo34_din <= comma32word; --EMU32b_dout;
		elsif( EMU32b_dvalid_d = '1' and  EMU32b_dvalid = '1') then
			fifo34_dtype <= "00";
			fifo34_din <= EMU32b_dout;
		else --if( EMU32b_dvalid_d = '0' and  EMU32b_dvalid = '0') then
			fifo34_dtype <= "11";
			fifo34_din <= comma32word;
		end if;	
	
     end if; 
   end if; 
end process;

Distr_LUT_COMP: entity work.Random_gen
  port map(
    rst		  =>  rst, 
    clk40     =>  clk40,
    clk240    =>  clk240,
    register_map_control =>  register_map_control,
	rg_rst	  =>  rg_rst,
	rg_enb	  =>  rg_enb,
    rg_doutb  =>  rg_doutb
  );
  
end architecture rtl ; -- of OUTPUTctrl

