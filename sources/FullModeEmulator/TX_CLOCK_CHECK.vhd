--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Mesfin Gebyehu
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    --use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;

entity TX_CLOCK_CHECK is
    port (
        clk240                  : in     std_logic;
        clk40                  : in     std_logic;
        RESET                   : in     std_logic;
        TXCLK_RESET_DONE_OUT    : out     std_logic;
        TXCLK_RESET_OUT         : out     std_logic
    );
end entity TX_CLOCK_CHECK;


architecture structure of TX_CLOCK_CHECK is

    signal txclk_counter : std_logic_vector(3 downto 0):= (others => '0');
    signal txclk_timeout_check : std_logic_vector(15 downto 0):= (others => '0');
    signal TXCLK_RESET_DONE_OUT_t : std_logic;
begin

    TXCLK_RESET_DONE_OUT <= TXCLK_RESET_DONE_OUT_t;
    process(clk240, RESET)
    begin
        if(RESET = '1') then
            txclk_counter <= (others => '0');
            TXCLK_RESET_DONE_OUT_t <= '0';
        elsif(rising_edge(  clk240)) then
            if(txclk_counter /= "1111") then
                txclk_counter <= txclk_counter + 1;
                TXCLK_RESET_DONE_OUT_t <= '0';
            else
                txclk_counter <= txclk_counter;
                TXCLK_RESET_DONE_OUT_t <= '1';
            end if;
        end if;
    end process;

    process(clk40, RESET)
    begin
        if(RESET = '1') then
            txclk_timeout_check <= (others => '0');
            TXCLK_RESET_OUT <= '0';
        elsif(rising_edge(  clk40)) then
            if(TXCLK_RESET_DONE_OUT_t = '0') then
                if(txclk_timeout_check /= X"FFFF") then
                    txclk_timeout_check <= txclk_timeout_check + 1;
                    TXCLK_RESET_OUT <= '0';
                else
                    txclk_timeout_check <= txclk_timeout_check;
                    TXCLK_RESET_OUT <= '1';
                end if;
            else
                TXCLK_RESET_OUT <= '0';
                txclk_timeout_check <= (others => '0');
            end if;
        end if;
    end process;

end architecture structure ;

