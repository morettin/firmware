--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;

entity FIFOfromHost_256to32 is
  port (
    FIFO256b32b_RE                  : in     std_logic;
    FIFO32b_dvalid                  : out    std_logic;
    FIFO32b_empty                   : out    std_logic;
    UpFifoPfull                     : out    std_logic;
    appreg_clk                      : in     std_logic;
    clk240                          : in     std_logic;
    fifo32_dout                     : out    std_logic_vector(31 downto 0);
    fifo_wr_clk                     : in     std_logic;
    flush_fifo                      : in     std_logic;
    fromhost_pfull_threshold_assert : in     std_logic_vector(6 downto 0);
    fromhost_pfull_threshold_negate : in     std_logic_vector(6 downto 0);
    upfifo_din                      : in     std_logic_vector(255 downto 0);
    upfifo_we                       : in     std_logic);
end entity FIFOfromHost_256to32;



architecture structure of FIFOfromHost_256to32 is
COMPONENT fifo4KB_256bit_to_32bit
  PORT (
    rst : IN std_logic;
    wr_clk : IN std_logic;
    rd_clk : IN std_logic;
    din : IN std_logic_vector(255 DOWNTO 0);
    wr_en : IN std_logic;
    rd_en : IN std_logic;
    prog_full_thresh_assert : IN std_logic_vector(6 DOWNTO 0);
    prog_full_thresh_negate : IN std_logic_vector(6 DOWNTO 0);
    dout : OUT std_logic_vector(31 DOWNTO 0);
    full : OUT std_logic;
    empty : OUT std_logic;
    almost_empty : OUT std_logic;
    prog_full : OUT std_logic
  );
END COMPONENT;

signal upfifo_din_swap: std_logic_vector(255 downto 0);
signal sFIFO32b_EN_r1, sFIFO32b_EN_r2, sFIFO32b_EN_r3: std_logic;
signal fifo32_dout_r1, fifo32_dout_r2, fifo32_dout_r3: std_logic_vector(31 downto 0);

begin

g0: for i in 0 to 7 generate
   upfifo_din_swap((i+1)*32-1 downto i*32) <= upfifo_din(((7-i)+1)*32-1 downto (7-i)*32); --swap fifo 32 words to match with SW order
end generate;


FIFOfromHost: fifo4KB_256bit_to_32bit
  PORT MAP (
    rst => flush_fifo,
    wr_clk => fifo_wr_clk,
    rd_clk => clk240,
    din => upfifo_din_swap,
    wr_en => upfifo_we,
    rd_en => FIFO256b32b_RE,
    prog_full_thresh_assert => fromhost_pfull_threshold_assert,
    prog_full_thresh_negate => fromhost_pfull_threshold_negate,
    dout => fifo32_dout_r1,
    full => open,
    empty => FIFO32b_empty,
    almost_empty => open, 
    prog_full => UpFifoPfull
  );

FIFO_Pipe: process(clk240)
begin
    if rising_edge (clk240) then
        sFIFO32b_EN_r1 <= FIFO256b32b_RE;
        sFIFO32b_EN_r2 <= sFIFO32b_EN_r1;
        sFIFO32b_EN_r3 <= sFIFO32b_EN_r2;    
        fifo32_dout_r2 <= fifo32_dout_r1;
        fifo32_dout_r3 <= fifo32_dout_r2;    
	end if;
end process;

FIFO32b_dvalid <= sFIFO32b_EN_r3;
fifo32_dout <= fifo32_dout_r3;
  
end architecture structure ; -- of FIFOfromHost_256to32

