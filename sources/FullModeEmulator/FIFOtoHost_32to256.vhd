--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;

entity FIFOtoHost_32to256 is
  port (
    clk240                        : in     std_logic;
    downfifo_dout                 : out    std_logic_vector(255 downto 0);
    downfifo_empty_thresh         : in     std_logic_vector(11 downto 0);
    downfifo_prog_empty           : out    std_logic;
    downfifo_re                   : in     std_logic;
    fifo_rd_clk                   : in     std_logic;
    flush_fifo                    : in     std_logic;
    gt0_rxcharisk_out             : in     std_logic_vector(3 downto 0);
    gt0_rxdata_out                : in     std_logic_vector(31 downto 0);
    tohost_pfull_threshold_assert : in     std_logic_vector(11 downto 0);
    tohost_pfull_threshold_negate : in     std_logic_vector(11 downto 0));
end entity FIFOtoHost_32to256;



architecture structure of FIFOtoHost_32to256 is

COMPONENT fifo128KB_32bit_to_256bit
  PORT (
    rst : IN std_logic;
    wr_clk : IN std_logic;
    rd_clk : IN std_logic;
    din : IN std_logic_vector(31 DOWNTO 0);
    wr_en : IN std_logic;
    rd_en : IN std_logic;
    prog_empty_thresh : IN std_logic_vector(8 DOWNTO 0);
    prog_full_thresh_assert : IN std_logic_vector(11 DOWNTO 0);
    prog_full_thresh_negate : IN std_logic_vector(11 DOWNTO 0);
    dout : OUT std_logic_vector(255 DOWNTO 0);
    full : OUT std_logic;
    empty : OUT std_logic;
    almost_empty : OUT std_logic;
    prog_full : OUT std_logic;
    prog_empty : OUT std_logic
  );
END COMPONENT;


signal FIFO32to256b_wr_en: std_logic;
signal downfifo_dout_swap: std_logic_vector(255 downto 0);

begin
FIFOtoHost0 : fifo128KB_32bit_to_256bit

  PORT MAP (
    rst => flush_fifo,
    wr_clk => clk240,
    rd_clk => fifo_rd_clk,
    din => gt0_rxdata_out,
    wr_en => FIFO32to256b_wr_en,
    rd_en => downfifo_re,
    prog_empty_thresh => downfifo_empty_thresh(8 downto 0),
    prog_full_thresh_assert => tohost_pfull_threshold_assert,
    prog_full_thresh_negate => tohost_pfull_threshold_negate,
    dout => downfifo_dout_swap,
    full => open,
    empty => open,
    almost_empty => open,
    prog_full => open,
    prog_empty => downfifo_prog_empty
  );


process (gt0_rxcharisk_out)
begin
   if gt0_rxcharisk_out = x"0" then
      FIFO32to256b_wr_en <= '1';
   else 
      FIFO32to256b_wr_en <= '0';
   end if;
end process;

g0: for i in 0 to 7 generate
   downfifo_dout((i+1)*32-1 downto i*32) <= downfifo_dout_swap(((7-i)+1)*32-1 downto (7-i)*32); --swap fifo 32 words to match with SW order
end generate;

end architecture structure ; -- of FIFOtoHost_32to256




