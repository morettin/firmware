--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene Habraken
--!               Frans Schreuder
--!               RHabraken
--!               Mesfin Gebyehu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.pcie_package.all;

entity OUTPUTctrl is
  port (
    CRC                  : in     std_logic_vector;
    CRC_Din              : out    std_logic_vector(31 downto 0);
    Calc                 : out    std_logic;
    Clr_CRC              : out    std_logic;
    EMU32b_EN            : out    std_logic;
    EMU32b_dout          : in     std_logic_vector(31 downto 0);
    EMU32b_dvalid        : in     std_logic;
    FIFO256b32b_RE       : out    std_logic;
    FIFO32b_dvalid       : in     std_logic;
    FIFO32b_empty        : in     std_logic;
    FIFO34b_WE           : out    std_logic;
    clk240               : in     std_logic;
    fifo32_dout          : in     std_logic_vector(31 downto 0);
    fifo34_din           : out    std_logic_vector(31 downto 0);
    fifo34_dtype         : out    std_logic_vector(1 downto 0);
    fifo34_full          : in     std_logic;
    register_map_control : in     register_map_control_type;
    rst                  : in     std_logic;
    busy_in              : in     std_logic;
    L1A_in               : in     std_logic);
end entity OUTPUTctrl;



architecture rtl of OUTPUTctrl is

constant package_Length: std_logic_vector(15 downto 0) := x"0078"; -- decimal 120

signal sDO_READ: std_logic;  -- signal is high when data is ready from CNT or FIFO32b
signal sPACKAGE_COUNT, sWORD_COUNT: std_logic_vector (15 downto 0);
signal sOUTPUT_SELECT, sOUTPUT_SELECT_d1: std_logic;
signal sEMU32b_EN, sFIFO256b32b_RE, sCRC_ready: std_logic;
signal sDATA_EMU_ON: std_logic;

signal usgnTIMESTAMP64, usgnTIMESTAMP_INCR64: unsigned(63 downto 0);

begin

process (clk240, rst)

variable vData_Type: std_logic_vector(1 downto 0):= "11"; -- set default to "ignore data".
variable vFIFO34b_WE, vSendTimestamp, vSendCRC, vSendIdle, vSendError, vSendData: std_logic;
variable usgnTIMESTAMP32 : unsigned(31 downto 0);
variable SendOnePacket : std_logic;

begin
   if (rising_edge(clk240)) then
      if (rst = '1' ) then 
         sOUTPUT_SELECT <= to_sl(register_map_control.GEN_FM_CONTROL2.EMU_ENABLE);
         sPACKAGE_COUNT <= register_map_control.GEN_FM_CONTROL1.PACKAGE_COUNT; --x"FFFF" default (16b field), set to x"FFFF" for streaming mode
         sWORD_COUNT <= register_map_control.GEN_FM_CONTROL1.PACKAGE_LENGTH; --120 default (16b field)
         usgnTIMESTAMP64 <= unsigned(x"00000000" & register_map_control.GEN_FM_CONTROL1.TIMESTAMP_INIT); -- 0 default (32b field)
         usgnTIMESTAMP_INCR64 <= unsigned(x"000000000000" & register_map_control.GEN_FM_CONTROL2.TIMESTAMP_INCR); -- default increment with 1 (16b field)
         usgnTIMESTAMP32 := x"12345678";
         sDO_READ <= '0';
         vSendTimestamp := '0';
         vSendCRC := '0';
         vSendIdle := '0';
         Clr_CRC <= '1';
         Calc <= '0';
         vData_Type := "11"; --IDLE
         vFIFO34b_WE := '0';
         sDATA_EMU_ON <= '1';
         SendOnePacket := '0';
      else
         sOUTPUT_SELECT_d1 <= sOUTPUT_SELECT;
         sOUTPUT_SELECT <= to_sl(register_map_control.GEN_FM_CONTROL2.EMU_ENABLE);
         if (sOUTPUT_SELECT  /=  sOUTPUT_SELECT_d1) then -- re?nitialize
            sOUTPUT_SELECT <= to_sl(register_map_control.GEN_FM_CONTROL2.EMU_ENABLE);
            sPACKAGE_COUNT <= register_map_control.GEN_FM_CONTROL1.PACKAGE_COUNT; --x"FFFF" default (16b field), set to x"FFFF" for streaming mode
            sWORD_COUNT <= register_map_control.GEN_FM_CONTROL1.PACKAGE_LENGTH; --120 default (16b field) (add 1 for the IDLE, so for ProtoDune use d121 --> h0x79
            usgnTIMESTAMP64 <= unsigned(x"00000000" & register_map_control.GEN_FM_CONTROL1.TIMESTAMP_INIT); -- 0 default (32b field)
            usgnTIMESTAMP_INCR64 <= unsigned(x"000000000000" & register_map_control.GEN_FM_CONTROL2.TIMESTAMP_INCR); -- default increment with 1 (16b field)
            usgnTIMESTAMP32 := x"12345678";
            CRC_Din <= x"FFFFFFFF";
            fifo34_din <= x"FFFFFFFF";
            vData_Type := "11"; --IDLE
         end if;

         sDO_READ <= '0';
         Clr_CRC <=  '1';
         Calc <= '0'; -- calculate crc
         vSendTimestamp := '0';
         vSendCRC := '0';-- Do not send CRC to FIFO34
         vSendIdle := '0';
         vFIFO34b_WE := '0';
         vSendError := '0';
         vData_Type := "11";
         if(L1A_in = '1') then
            SendOnePacket := '1';
         end if;
         
         if ( (FIFO32b_dvalid = '1') or 
            (EMU32b_dvalid = '1') or 
            (sWORD_COUNT = register_map_control.GEN_FM_CONTROL1.PACKAGE_LENGTH) or 
            (sWORD_COUNT = register_map_control.GEN_FM_CONTROL1.PACKAGE_LENGTH -1) or 
            (sWORD_COUNT = 1) or 
            (sWORD_COUNT = 0) 
            ) 
         then 
            vSendData := '1';
         else
            vSendData := '0';
         end if;

         if(sPACKAGE_COUNT = 0 and SendOnePacket = '0') then
            sDATA_EMU_ON <= '0';
         else
            sDATA_EMU_ON <= '1';
         end if;
         
         if SendOnePacket = '1' and sPACKAGE_COUNT = x"0000" and L1A_in = '0' then
            sPACKAGE_COUNT <= x"0001";
            SendOnePacket := '0';
         end if;

         if (sDATA_EMU_ON = '1') then
         --   vFIFO34b_WE := '1';
            sDO_READ <= '1';
            Clr_CRC <=  '0';
            --Calc <= '1'; -- calculate crc
            sCRC_ready <= '0';
            if (vSendData = '1') then
               vFIFO34b_WE := '1';
               Calc <= '1'; -- calculate crc, (p.e. do NOT calc when fifo34 is full)
               if (sWORD_COUNT = register_map_control.GEN_FM_CONTROL1.PACKAGE_LENGTH) then 
               --send 1st part of timestamp
                  vData_Type := "01";  -- start of packet
                  vSendTimestamp := '1';
                  sWORD_COUNT <= (sWORD_COUNT -1);
                  usgnTIMESTAMP32 := usgnTIMESTAMP64 (31 downto 0);
                  Clr_CRC <= '1';-- active on rising edge
               elsif (sWORD_COUNT = register_map_control.GEN_FM_CONTROL1.PACKAGE_LENGTH -1) then 
               --send 2nd part of timestamp
                  vData_Type := "00";
                  vSendTimestamp := '1';
                  sWORD_COUNT <= (sWORD_COUNT -1);
                  usgnTIMESTAMP32 := usgnTIMESTAMP64 (63 downto 32);
               elsif ( (sWORD_COUNT = 2) or (sWORD_COUNT = 3) or (sWORD_COUNT = 4) or (sWORD_COUNT = 5) ) then 
               -- DISABLE the fifo256b32b and EMU_RAM while sending CRC + EOP, IDLE and timestamp
                  vData_Type := "00";  -- intermediate word of current packet
                  sWORD_COUNT <= (sWORD_COUNT -1);
                  sDO_READ <= '0';
               elsif(sWORD_COUNT = 1) then 
               -- EOP, send EOP and decrease package count
                  -- wait one clk cycle for CRC
                  sCRC_ready <= '1';
                  vFIFO34b_WE := '0'; -- disable FIFO for 1 clk cycle
                  sDO_READ <= '0';
                  Calc <= '0'; -- do not calc while sending
                  if sCRC_ready = '1' then
                     sCRC_ready <= '0'; -- reset after 1 clk cycle
                     sDO_READ <= '1';
                     vFIFO34b_WE := '1';
                     vData_Type := "10"; -- EOP
                     sWORD_COUNT <= (sWORD_COUNT -1);
                     vSendCRC := '1';  -- Prepare sending CRC signal to FIFO34
                     usgnTIMESTAMP64 <= (usgnTIMESTAMP64 + usgnTIMESTAMP_INCR64);
                     if(sPACKAGE_COUNT > 0) then
                        if (sPACKAGE_COUNT = x"FFFF") then -- stream data
                           sDATA_EMU_ON <= '1';
                        else
                           sDATA_EMU_ON <= '1';
                           sPACKAGE_COUNT <= (sPACKAGE_COUNT -1);
                        end if;
                     end if;
                  end if; -- sCRC_ready
               elsif(sWORD_COUNT = 0) then 
               --IDLE, end every package with one idle character into the FIFO34b
                  vSendIdle := '1';
                  sWORD_COUNT <= (register_map_control.GEN_FM_CONTROL1.PACKAGE_LENGTH);
               else 
               -- payload data, send data and write to fifo34b
                  vData_Type := "00";  -- intermediate word of current packet
                  sWORD_COUNT <= (sWORD_COUNT -1);
               end if; -- sWORD_COUNT
            end if; --vSendData = '1'
         else
         -- (sDATA_EMU_ON = '0' ) Package count = 0
            sWORD_COUNT <= (register_map_control.GEN_FM_CONTROL1.PACKAGE_LENGTH);
         end if;-- sPACKAGE_COUNT > 1 or 0

      -- not in reset, send data 
         if (vSendCRC = '1') then
            fifo34_din <= CRC; -- The CRC should be on the output with data type "10" EOP
         elsif (vSendTimestamp = '1') then
            CRC_Din <= std_logic_vector(usgnTIMESTAMP32);
            fifo34_din <= std_logic_vector(usgnTIMESTAMP32);
         elsif (vSendIdle = '1') then
            CRC_Din <= x"12345678";
            fifo34_din <= x"12345678";
            vData_Type := "11";
            Calc <= '0';
         elsif ( (sOUTPUT_SELECT = '1') and (vSendTimestamp = '0') ) then
            CRC_Din <= EMU32b_dout;
            fifo34_din <= EMU32b_dout;
         elsif ( (sOUTPUT_SELECT = '0') and (vSendTimestamp = '0') ) then
            CRC_Din <= fifo32_dout; 
            fifo34_din <= fifo32_dout;
         elsif (vSendError = '1') then
            CRC_Din <= x"DEADBEEF";
            fifo34_din <= x"DEADBEEF";
            vData_Type := "00";
            Calc <= '0';
         end if;
         fifo34_dtype <= vData_Type;
         FIFO34b_WE <= vFIFO34b_WE;  -- update on output after 1 clk cycle
      end if; --end not reset
   end if; -- end rising clk 
end process;

-- FIFO combinational logic: CRC input and data flow (SW/EMUram) selector
process (FIFO32b_empty, fifo34_full, sDO_READ, sOUTPUT_SELECT, EMU32b_dout, fifo32_dout)
begin
   sFIFO256b32b_RE <= '0';
   sEMU32b_EN <= '0'; 
   if (sDATA_EMU_ON = '1') then
      if (sOUTPUT_SELECT = '1') then
         if (fifo34_full = '0' and sDO_READ = '1' and busy_in = '0') then
            sEMU32b_EN <= '1';
         end if;
      else --(sOUTPUT_SELECT = '0') input from sw 
         if (FIFO32b_empty = '0' and fifo34_full = '0' and sDO_READ = '1' and busy_in = '0') then
             sFIFO256b32b_RE <= '1';
         end if;
      end if;
   end if;
 end process;

FIFO256b32b_RE <= sFIFO256b32b_RE;
EMU32b_EN <= sEMU32b_EN;

end architecture rtl ; -- of OUTPUTctrl

