--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!                    Atlas - FELIX                                          --
--!                                                                           --
--!-----------------------------------------------------------------------------
--!
--! unit name: axi_stream_package
--!
--! author: Frans Schreuder <f.schreuder@nikhef.nl>
--!
--! date: 08/04/2019    : created
--!
--! description: Axi stream record types to be used to connect Phase II toplevel
--! blocks, especially between Decoding and Central Router
--!
--!-----------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;


package axi_stream_package is


    --! This is a typical usage of this axi stream type is as follows:
    --! Master:
    --! entity AxisMaster is
    --! generic(
    --!   NUM_STREAMS : positive := 1
    --! );
    --! port(
    --!   m_axis : out axis_32_array_type(0 to NUM_STREAMS-1);
    --!   m_axis_tready : in axis_tready_array_type( 0 to NUM_STREAMS-1);
    --!   aclk   : in std_logic;
    --!   aresetn : in std_logic
    --! );
    --! end entity AxisMaster;
    --!
    --! Slave:
    --! entity AxisSlave is
    --! generic(
    --!   NUM_STREAMS : positive := 1
    --! );
    --! port(
    --!   s_axis : in axis_32_array_type(0 to NUM_STREAMS-1);
    --!   s_axis_tready : out axis_tready_array_type(0 to NUM_STREAMS-1);
    --!   aclk   : in std_logic;
    --!   aresetn : in std_logic
    --! );
    --! end entity AxisSlave;
    --!
    --! Instantiation template:
    --! Decoder0: entity work.AxisMaster
    --! generic map(
    --!   NUM_STREAMS     => NUM_STREAMS
    --! )
    --! port map(
    --!   m_axis        => Decoder0_axis,
    --!   m_axis_tready => Decoder0_axis_tready,
    --!   aclk          => clk40,
    --!   aresetn       => resetn --negative reset
    --! );
    --!
    --!
    --! CRTH0: entity work.AxisSlave
    --! generic map(
    --!   NUM_STREAMS     => NUM_STREAMS
    --! )
    --! port map(
    --!   s_axis        => Decoder0_axis,
    --!   s_axis_tready => Decoder0_axis_tready,
    --!   aclk          => clk40,
    --!   aresetn       => resetn --negative reset
    --! );

    type axis_64_type is record
        tdata        : std_logic_vector(63 downto 0);  --! Data bus
        tvalid       : std_logic;                      --! Indicates valid data when tready is '1', when tready is '0' tvalid has to stay high in order to not break the packet.
        tlast        : std_logic;                      --! Indicates the last cycle of a chunk / packet.
        tkeep        : std_logic_vector(7 downto 0);   --! Serves as byte enable
        tuser        : std_logic_vector(3 downto 0);   --! Meaning of tuser bits:
        --!   3: link truncation or FIFO full
        --!   2: link BUSY
        --!   1: Chunk error
        --!   0: CRC error
        tid          : std_logic_vector(7 downto 0);   --! Converts to channel on 25G links
    end record;

    type axis_64_array_type is array (natural range <>) of axis_64_type;
    --TBD: Do we need/want 2d arrays, to split at link level?
    type axis_64_2d_array_type is array (natural range <>, natural range <>) of axis_64_type;

    constant axis_64_zero_c: axis_64_type := (tdata => (others => '0'), tvalid => '0', tlast => '0', tkeep => (others => '0'), tuser => (others => '0'), tid => (others => '0'));

    type axis_32_type is record
        tdata        : std_logic_vector(31 downto 0);  --! Data bus
        tvalid       : std_logic;                      --! Indicates valid data when tready is '1', when tready is '0' tvalid has to stay high in order to not break the packet.
        tlast        : std_logic;                      --! Indicates the last cycle of a chunk / packet.
        tkeep        : std_logic_vector(3 downto 0);   --! Serves as byte enable
        tuser        : std_logic_vector(3 downto 0);   --! Meaning of tuser bits:
    --!   3: link truncation or FIFO full
    --!   2: link BUSY
    --!   1: Chunk error
    --!   0: CRC error
    end record;

    type axis_32_array_type is array (natural range <>) of axis_32_type;
    --TBD: Do we need/want 2d arrays, to split at link level?
    type axis_32_2d_array_type is array (natural range <>, natural range <>) of axis_32_type;


    type axis_8_type is record
        tdata        : std_logic_vector(7 downto 0);   --! Data bus
        tvalid       : std_logic;                      --! Indicates valid data when tready is '1', when tready is '0' tvalid has to stay high in order to not break the packet.
        tlast        : std_logic;                      --! Indicates the last cycle of a chunk / packet.
    end record;

    type axis_8_array_type is array (natural range <>) of axis_8_type;
    --TBD: Do we need/want 2d arrays, to split at link level?
    type axis_8_2d_array_type is array (natural range <>, natural range <>) of axis_8_type;


    --1D and 2D std_logic_vectors for slave->master port (tready).
    type axis_tready_array_type is array(natural range <>) of std_logic;
    type axis_tready_2d_array_type is array(natural range <>, natural range <>) of std_logic;

    type axis_512_type is record
        tdata        : std_logic_vector(511 downto 0);  --! Data bus
        tvalid       : std_logic;                      --! Indicates valid data when tready is '1', when tready is '0' tvalid has to stay high in order to not break the packet.
        tlast        : std_logic;                      --! Indicates the last cycle of a chunk / packet.
    --tkeep        : std_logic_vector(7 downto 0);   --! Serves as byte enable
    --tuser        : std_logic_vector(3 downto 0);   --! Meaning of tuser bits:
    --!   3: link truncation or FIFO full
    --!   2: link BUSY
    --!   1: Chunk error
    --!   0: CRC error
    --tid          : std_logic_vector(7 downto 0);   --! Converts to channel on 25G links
    end record;

    type axis_512_array_type is array (natural range <>) of axis_512_type;


end package axi_stream_package ;
