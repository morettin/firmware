--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Julia Narevicius
--!               Israel Grayzman
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-----------------------------------------------------------------------------
-- used for simulation only
-----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


package centralRouterTOPpackage is
  
constant NUMBER_OF_INTERRUPTS : integer := 8;
constant GBT_NUM              : integer := 1;
constant FMCH_NUM             : integer := 1;
constant includeToHoEproc16      : boolean := false; -- [set in top module]
constant includeToHoEproc8s      : boolean := true; -- [set in top module]
constant includeToHoEproc4s      : boolean := true; -- [set in top module]
constant includeToHoEproc2s      : boolean := true; -- [set in top module]
constant includeFrHoEproc8s      : boolean := true; -- [set in top module]
constant includeFrHoEproc4s      : boolean := true; -- [set in top module]
constant includeFrHoEproc2s      : boolean := true; -- [set in top module]

constant TTC_test_mode           : boolean := false;  -- if true, the initial congifuration of TTC-from-host matches the direct-to-host congifuration (can be looped back)
constant serialConfig_mode       : boolean := false;
constant STATIC_CENTRALROUTER    : boolean := true;--false; --IG -- removes update process from central router register map, only initial constant values are used

constant wideMode             : boolean := false; -- in TTC test there can be only normal mode (wideMode:=false)

constant useGBTdataEmulator   : boolean := true; --true; -- *no use in loopback mode, but will be optimized out anyway

constant crInternalLoopbackMode : boolean := true; -- false; -- wideMode has to be false in a loopback mode
constant includeDebugLogic      : boolean := false; --true; --IG 

--constant useTTCemu            : boolean := true; -- if true, the source of the TTC signal will be internal 10bit counter
--constant TTC_test_mode        : boolean := true; -- if true, the initial configuration is according to centralRouter_package.vhd, l.237-254

constant toHostTimeoutBitn : integer := 16;

                                                                       
end package centralRouterTOPpackage ;

    
  
