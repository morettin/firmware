--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Nico Giangiacomi
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:58:50 08/23/2013
-- Design Name:   
-- Module Name:   C:/Users/balbi/Documents/IBL/RODM_S/RodM_S/test_RodM_S.vhd
-- Project Name:  RodM_S
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: RodM_S
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
--use IEEE.std_logic_arith.all; -- needed for +/- operations
--use IEEE.std_logic_unsigned.all; -- needed for +/- operations
use IEEE.std_logic_textio.all;

-- library efb;
-- library router;

-- use efb.all;
-- use router.all;

use std.textio.all;
use ieee.std_logic_textio.all;
use work.axi_stream_package.all;
-- library Boc_sim;
-- use Boc_sim.txt_util.all;
-- use Boc_sim.bocDemoPack.all;
-- use Boc_sim.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
 
ENTITY EncodingEpathGBT_tb IS
END EncodingEpathGBT_tb;
 
ARCHITECTURE behavior OF EncodingEpathGBT_tb IS 

--#######################################################################
--##                                                                   ##
--##                       COMPONENT DEFINITION                        ##
--##                                                                   ##
--#######################################################################

    -- Component Declaration for the Unit Under Test (UUT)
 
	


--#######################################################################
--##                                                                   ##
--##                        SIGNALS DEFINITION                         ##
--##                                                                   ##
--#######################################################################
	 -- Clock period definitions

	constant clk40_period : time := 25 ns;
	constant clk80_period : time := 12.5 ns;
	constant axiclk_period : time := 10ns;
	signal clk40		: std_logic:= '0';
	signal clk80		: std_logic:= '0';
	signal reset		: std_logic:= '0';

	signal   ElinkData_i : std_logic_vector(7 downto 0)  := (others => '0');
	signal s_axis_i :  axis_8_type;
	signal s_axis_tready_i : std_logic;
	signal s_axis_aclk_i : std_logic:= '0';

	file OUTPUT, OUTPUT2 : text;
	
	signal running, finished : std_logic := '0';
	
	signal encodedData	: std_logic_vector(9 downto 0) := (others => '0');
	signal encodedDataValid : std_logic := '0';
	
	signal TTCin_i : std_logic_vector(10 downto 0) := "00000000000";
	signal TTCOption_i : std_logic_vector(3 downto 0):= x"0";
	
	signal data_HDLC : std_logic_vector(7 downto 0);
	signal data_HDLC_valid : std_logic;
	
	signal data_HDLC_extended : std_logic_vector(9 downto 0);
	
BEGIN

	--clk Process
	clk40_process :process
	begin
		clk40 <= '0';
		
		wait for clk40_period/2;
		clk40 <= '1';
		wait for clk40_period/2;
	end process;
	
	clk80_process :process
	begin
		clk80 <= '0';
		
		wait for clk80_period/2;
		clk80 <= '1';
		wait for clk80_period/2;
	end process;
	
	--axiclk Process
	axiclk_process :process
	begin
		s_axis_aclk_i <= '0';
		
		wait for axiclk_period/2;
		s_axis_aclk_i <= '1';
		wait for axiclk_period/2;
	end process;

	-- Reset process
	reset_proc: process
	begin		
		reset <= '0';
		wait for 100 * clk40_period;
		reset <= '1';
		wait for 100 * clk40_period;	
		reset <= '0';
		wait for 100ms;
	end process;


EncodingEpathGBT_uut: entity work.EncodingEpathGBT
 generic Map(
  MAX_OUTPUT		 => 8,
  INCLUDE_8b     => '1',
  INCLUDE_4b     => '1',
  INCLUDE_2b     => '1',
  INCLUDE_8b10b  => '1',
  INCLUDE_HDLC   => '1',
  INCLUDE_DIRECT => '1',
  BLOCKSIZE      => 1056,
  GENERATE_FEI4B => false,
  GENERATE_LCB_ENC => false,
  HDLC_IDLE_STATE => x"7F" 
 )
 port Map(
  clk40 				=> clk40,
  reset 				=> reset,
  
  EpathEnable   => '1',
	EpathEncoding => x"2", --HDLC
	ElinkWidth 		=> "00", --2bits
	MsbFirst    	=> '1',     
	ReverseOutputBits => '0',
	ElinkData 		=> ElinkData_i,
	XOff					=> '0',
	epath_almost_full => open,
	s_axis 				=> s_axis_i,
	s_axis_tready => s_axis_tready_i,
	s_axis_aclk 	=> s_axis_aclk_i, 
	TTCOption			=> TTCOption_i,
	TTCin					=> TTCin_i

  );

--HDLC decoding
dec_HDLC_inst: entity work.DecoderHDLC 
PORT MAP (  
	clk40         => clk40,
	reset         => reset,
	ena           => '1',
	DataIn        => ElinkData_i(1 downto 0),
	DataOut       => data_HDLC,
	DataOutValid  => data_HDLC_valid,
	EOP           => open,
	TruncateHDLC  => open
);
 
--eproc_HDLC_inst: entity work.EPROC_IN2_HDLC 
--port Map(  
--    bitCLK      => clk40,
--    bitCLKx2    => clk80,
--    rst         => reset,
--    ena         => '1',
--    edataIN     => ElinkData_i(1 downto 0),
--    dataOUT     => data_HDLC_extended,
--    dataOUTrdy  => open
--    );


	-- Data process
	data_proc: process
	begin		
		s_axis_i.tdata <= (others => '0');
		s_axis_i.tvalid <= '0';
		s_axis_i.tlast <= '0';
					
		wait for 1250 * axiclk_period;
		
		running		<= '1';
		s_axis_i.tdata <= x"ff";
				s_axis_i.tvalid <= '1';
				s_axis_i.tlast <= '0';
				wait for axiclk_period;
	--	s_axis_i.tdata <= x"ff";
	--	s_axis_i.tvalid <= '1';
	--	s_axis_i.tlast <= '0';
	--	wait for axiclk_period;
		for i in 1 to 100 loop	
			s_axis_i.tdata <= std_logic_vector(to_unsigned(i, 8));
			s_axis_i.tvalid <= '1';
			s_axis_i.tlast <= '0';
			wait for axiclk_period;
		end loop;
		s_axis_i.tdata <= x"fF";
		s_axis_i.tvalid <= '1';
		s_axis_i.tlast <= '0';
		wait for axiclk_period;
		s_axis_i.tdata <= x"fd";
		s_axis_i.tvalid <= '1';
		s_axis_i.tlast <= '0';
		wait for axiclk_period;
		s_axis_i.tdata <= x"0a";
		s_axis_i.tvalid <= '1';
		s_axis_i.tlast <= '1';
		wait for  axiclk_period;
		s_axis_i.tvalid <= '0';
		--second set of data
		wait for 300 * axiclk_period;
		for i in 0 to 45 loop	
			s_axis_i.tdata <= std_logic_vector(to_unsigned(i, 8));
			s_axis_i.tvalid <= '1';
			s_axis_i.tlast <= '0';
			wait for axiclk_period;
		end loop;
		s_axis_i.tdata <= x"ff";
		s_axis_i.tvalid <= '1';
		s_axis_i.tlast <= '1';
		wait for axiclk_period;
		
		s_axis_i.tdata <= (others => '0');
		s_axis_i.tvalid <= '0';
		s_axis_i.tlast <= '0';
					
		wait for 20us;
		running		<= '0';
		finished	<= '1';
		wait;
	end process;


	--encodedData <= <<signal EncodingEpathGBT_uut.g_include8b10b.Encoder8b10b0.DataOut : std_logic_vector(9 downto 0) >>;
	--encodedDataValid <= <<signal EncodingEpathGBT_uut.g_include8b10b.Encoder8b10b0.DataOutValid : std_logic >>;

--	write_file_proc2: process
--		variable OLINE : line;
--	begin
--		file_open(OUTPUT2, "/home/ngiangia/Output2.txt", append_mode);
--		wait for clk40_period;
--		if(running = '1') then
--			if (encodedDataValid = '1') then
--				write(OLINE, encodedData);
--			end if;
--		elsif(finished = '1') then
			
--			writeline(OUTPUT2, OLINE);
--			file_close(OUTPUT2);
--			wait;
--		end if;
--	end process write_file_proc2;


--	write_file_proc: process
--		variable OLINE : line;
--	begin
--		wait for clk40_period;
--		if(running = '1') then
			
--				write(OLINE, ElinkData_i(1 downto 0));
--		elsif(finished = '1') then
--			file_open(OUTPUT, "/home/ngiangia/Output.txt", write_mode);
--			writeline(OUTPUT, OLINE);
--			file_close(OUTPUT);
--			wait;
--		end if;
--	end process write_file_proc;

END;
