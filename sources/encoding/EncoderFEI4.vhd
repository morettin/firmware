--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!               Nico Giangiacomi
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  University of Wuppertal
--! Engineer: Carsten Duelsen <carsten.dulsen@cern.ch>
--!           Marius Wensing <marius.wensing@cern.ch>
--!
--! Create Date:    28/08/2020
--! Module Name:    EncoderFEI4
--! Project Name:   FELIX
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;


    use work.FELIX_package.all;

entity EncoderFEI4 is
    port (
        reset       : in  std_logic; --Active high reset
        clk40       : in  std_logic; --BC clock for DataIn
        EnableIn    : in  std_logic;

        DataIn      : in  std_logic_vector(7 downto 0); --8b Data from AxiStreamtoByte
        DataInValid : in  std_logic; -- Data validated by AxiStreamtoByte
        EOP_in      : in  std_logic; --End of Packet from AxiStreamtoByte
        readyOut    : out std_logic;

        TTCin       : in  TTC_data_type;

        ElinkOut    : out std_logic_vector(7 downto 0); --ElinkElinkOut

        config      : in  std_logic_vector(5 downto 0) := (others => '0')
    );
end EncoderFEI4;

architecture rtl of EncoderFEI4 is
    -- FE-I4 fast commands
    constant C_LV1            : std_logic_vector(8 downto 0)  := "111010000";
    constant C_ECR            : std_logic_vector(8 downto 0)  := "101100010";
    constant C_BCR            : std_logic_vector(8 downto 0)  := "101100001";
    constant C_CAL            : std_logic_vector(8 downto 0)  := "101100100";

    -- TTC encoder
    signal ttc_serializer : std_logic_vector(8 downto 0) := (others => '0');
    signal ttc_deadtime : integer range 0 to 8 := 0;
    signal ttc_active : std_logic := '0';
    signal ttc_active_reg : std_logic := '0';
    signal ttc_out : std_logic := '0';

    -- data serializer
    signal data_serializer : std_logic_vector(7 downto 0) := (others => '0');
    signal data_cnt : integer range 0 to 7 := 0;
    signal data_last : std_logic := '0';
    signal data_inchunk : std_logic := '0';
    signal data_out : std_logic := '0';

    -- merger
    signal merger_out : std_logic := '0';

    -- line encoder
    signal line_out : std_logic_vector(7 downto 0) := (others => '0');

    -- data delay
    signal delay_value : integer range 0 to 7 := 0;
    signal delay_shiftreg : std_logic_vector(14 downto 0) := (others => '0');
    signal delay_out : std_logic_vector(7 downto 0) := (others => '0');
begin

    -----------------------
    -- part 1: TTC encoder
    -----------------------

    ttc: process (clk40, reset) begin
        if reset = '1' then
            ttc_serializer <= (others => '0');
            ttc_deadtime <= 0;
            ttc_active_reg <= '0';
        else
            if rising_edge(clk40) then
                ttc_active_reg <= ttc_active;

                if EnableIn = '1' then
                    -- shift TTC serializer
                    ttc_serializer <= ttc_serializer(7 downto 0) & '0';

                    -- deadtime counter for active commands
                    if ttc_deadtime > 0 then
                        ttc_deadtime <= ttc_deadtime - 1;
                    end if;

                    -- if we are not in a chunk and the deadtime expired we can accept new commands
                    if (data_inchunk = '0') and (ttc_deadtime = 0) then
                        if TTCin.L1A = '1' then
                            ttc_serializer <= C_LV1;
                            ttc_deadtime <= 4;
                        elsif TTCin.BCR = '1' then
                            ttc_serializer <= C_BCR;
                            ttc_deadtime <= 8;
                        elsif TTCin.ECR = '1' then
                            ttc_serializer <= C_ECR;
                            ttc_deadtime <= 8;
                        elsif TTCin.Brcst(0) = '1' then
                            ttc_serializer <= C_CAL;
                            ttc_deadtime <= 8;
                        end if;
                    end if;
                else
                    ttc_deadtime <= 0;
                    ttc_serializer <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    ttc_out <= ttc_serializer(8);

    ttc_active <= '1' when ((ttc_deadtime > 0) or
                             (TTCin.L1A = '1') or
                             (TTCin.BCR = '1') or
                             (TTCin.ECR = '1') or
                             (TTCin.Brcst(0) = '1')) else
                  '0';

    -----------------------
    -- part 2: data encoder
    -----------------------

    data: process (clk40, reset) begin
        if reset = '1' then
            data_last <= '0';
            data_cnt <= 0;
            data_inchunk <= '0';
            data_serializer <= (others => '0');
        else
            if rising_edge(clk40) then
                if EnableIn = '1' then
                    if data_cnt = 0 then
                        -- accept new data
                        if (ttc_active = '0') and (DataInValid = '1') then
                            data_inchunk <= '1';
                            data_cnt <= 7;
                            data_last <= EOP_in;
                            data_serializer <= DataIn;
                        elsif data_last = '1' then
                            -- clear inchunk status
                            data_inchunk <= '0';
                        end if;
                    else
                        -- just shift
                        data_serializer <= data_serializer(6 downto 0) & '0';
                        data_cnt <= data_cnt - 1;
                    end if;
                else
                    data_last <= '0';
                    data_inchunk <= '0';
                    data_cnt <= 0;
                    data_serializer <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -- generate a ready towards AXI stream FIFO
    readyOut <= '1' when (ttc_active = '0') and (data_cnt = 0) else '0';

    -- output MSB of the serializer
    data_out <= data_serializer(7);

    -----------------------
    -- part 3: merger
    -----------------------
    merger: process (ttc_active_reg, data_inchunk, data_out, ttc_out, config) begin
        if config(1) = '1' then
            if config(0) = '1' then
                merger_out <= data_out;
            else
                merger_out <= ttc_out;
            end if;
        else
            if ttc_active_reg = '1' then
                merger_out <= ttc_out;
            elsif data_inchunk = '1' then
                merger_out <= data_out;
            else
                merger_out <= '0';
            end if;
        end if;
    end process;

    -----------------------
    -- part 4: line encoder
    -----------------------

    line_encoder: process (clk40) begin
        if rising_edge(clk40) then
            if config(2) = '0' then
                line_out <= (others => merger_out);
            else
                line_out(7 downto 4) <= (others => merger_out);
                line_out(3 downto 0) <= (others => (not merger_out));
            end if;
        end if;
    end process;

    -----------------------
    -- part 5: data delay
    -----------------------

    delay_value <= to_integer(unsigned(config(5 downto 3)));

    delay: process (clk40) begin
        if rising_edge(clk40) then
            delay_shiftreg <= delay_shiftreg(6 downto 0) & line_out;
            delay_out <= delay_shiftreg(7+delay_value downto delay_value);
        end if;
    end process;

    -----------------------
    -- part 6: output
    -----------------------
    ElinkOut <= delay_out;

end architecture;
