--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  University and INFN Bologna  
--! Engineer: Nico Giangiacomi
--! 
--! Create Date:    02/02/2020 
--! Module Name:    Encoder8b10b
--! Project Name:   FELIX
--! Project description: Wrapper for 8b10b encoder
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.centralRouter_package.all;

--! a wrap for 8b10b decoder and alignment
entity Encoder8b10b_tb is
end Encoder8b10b_tb;

architecture Behavioral of Encoder8b10b_tb is
 signal dataOut_tmp: std_logic_vector(9 downto 0);
 signal data_te: std_logic_vector(7 downto 0);
 signal isK : std_logic;
 signal clk40, reset: std_logic;
 constant clk40_period : time := 25 ns;
 signal dataValid_te : std_logic;
begin

clk_proc: process
begin
    clk40 <= '1';
    wait for clk40_period / 2;
    clk40 <= '0';
    wait for clk40_period / 2;
end process;

reset_proc: process
begin
    reset <= '1';
    wait for clk40_period * 2;
    reset <= '0';
    wait;
end process;

data_te_proc: process
begin
    isK <= '1';
    data_te <= Kchar_comma;
    wait for clk40_period;
    data_te <= Kchar_sop;
    wait for clk40_period;
end process;
    --char_comma  <=   Kchar_comma;
    --char_SOP    <=   Kchar_sop;
    --char_EOP    <=   Kchar_eop;
    --char_SOB    <=   Kchar_sob;
    --char_EOB    <=   Kchar_eob;
    dataValid_te <= '1';



-- 8b10b encoder
enc_8b10b_INST: entity work.enc_8b10b 
PORT MAP(
    RESET   => reset,
    clk     => clk40,
    ena     => dataValid_te,
    --enaFall : in std_logic ;
    --SBYTECLK : in std_logic ;    -- Master synchronous send byte clock 
    KI     => isK,
    AI     => data_te(0),
    BI     => data_te(1),
    CI     => data_te(2),
    DI     => data_te(3),
    EI     => data_te(4),
    FI     => data_te(5),
    GI     => data_te(6),
    HI     => data_te(7),
    
    JO     => dataOut_tmp(0),
    HO     => dataOut_tmp(1),
    GO     => dataOut_tmp(2),
    FO     => dataOut_tmp(3),
    IO     => dataOut_tmp(4),
    EO     => dataOut_tmp(5),
    DO     => dataOut_tmp(6),
    CO     => dataOut_tmp(7),
    BO     => dataOut_tmp(8),
    AO     => dataOut_tmp(9)

);


end Behavioral;

