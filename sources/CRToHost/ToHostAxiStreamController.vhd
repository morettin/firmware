--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  Nikhef
--! Engineer: Frans Schreuder
--!
--! Create Date:    17/09/2019
--! Module Name:    ToHostAxiStreamController
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.centralRouter_package.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;

--! a driver for EPROC FIFO, manages block header and sub-chunk trailer
entity ToHostAxiStreamController is
    generic (
        FMCHid                  : integer := 0;
        toHostTimeoutBitn       : integer := 8;
        BLOCKSIZE               : integer := 1024;
        STREAMS_TOHOST          : integer := 1;
        ACLK_FREQ               : integer  --Used to compensate the timeout counter to run at 40MHz (BC)
    );
    port (
        aclk        : in  std_logic;
        aresetn     : in  std_logic;

        s_axis_in            : in axis_32_array_type(0 to STREAMS_TOHOST-1);
        s_axis_tready_out    : out axis_tready_array_type(0 to STREAMS_TOHOST-1);
        s_axis_prog_empty_in : in axis_tready_array_type(0 to STREAMS_TOHOST-1);

        timeOutEna_i    : in  std_logic;
        timeCnt_max     : in  std_logic_vector ((toHostTimeoutBitn-1) downto 0);
        instant_timeout_enable : in std_logic_vector(STREAMS_TOHOST-1 downto 0);
        prog_full_in    : in  std_logic;-- in, downstream wm fifo is full or main , can't happen in normal operation
        ----------
        word32out       : out std_logic_vector (31 downto 0); -- to chFIFO
        word32out_valid   : out std_logic;
        end_of_block : out std_logic
    );
end ToHostAxiStreamController;
--
architecture Behavioral of ToHostAxiStreamController is

    --    COMPONENT ila_1

    --        PORT (
    --            clk : IN STD_LOGIC;

    --            probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe6 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    --            probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe9 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    --            probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe12 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    --            probe13 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe14 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    --            probe15 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    --            probe16 : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    --            probe17: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    --            probe18: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe19: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe20: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    --            probe21: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    --            probe22: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe23: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe24: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe25: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    --            probe26: IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    --            probe27: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --            probe28: IN STD_LOGIC_VECTOR(7 DOWNTO 0)
    --        );
    --    END COMPONENT  ;


    signal s_axis_tready_s: std_logic;
    --signal s_axis_p1, s_axis_p2: axis_32_type;
    constant NUMBER_OF_WORDS_PER_BLOCK: integer := BLOCKSIZE/4;
    --
    --
    --signal timeout: std_logic;

    signal reset, rst_p1 : std_logic := '1';


    signal timeOutCnt    :  std_logic_vector ((toHostTimeoutBitn-1) downto 0); -- @ clk240 domain

    signal is_soc : std_logic_vector(STREAMS_TOHOST-1 downto 0); --This is the next valid word after s_axis.tlast.
    signal stream_select_s: integer range 0 to STREAMS_TOHOST;
    signal stream_select_switched: std_logic;
    signal stream_select_and_value : axis_tready_array_type(0 to STREAMS_TOHOST-1);
    signal SwitchAxiStream : std_logic;
    type slv2_array is array (natural range <>) of std_logic_vector(1 downto 0);

    signal s_axis_timeout_cnt : slv2_array(0 to STREAMS_TOHOST-1);
    signal timeout_pulse: std_logic;
    --signal nextAvailable: integer range 0 to STREAMS_TOHOST-1;
    signal nextFound: std_logic; --for streamSelect process

    signal s_axis : axis_32_type;
    signal mux_extra_tready, mux_extra_tready_on_tlast : std_logic;
    --For round robin axi stream selection, we go round robin over all the streams
    --and start searching there for the next available one
    signal startSearchingAt : integer range 0 to STREAMS_TOHOST-1;
    signal timeout_incomplete_frame_cnt: integer range 0 to 255;
    signal chunkCounter : integer range 0 to BLOCKSIZE-1;  --counts the length in the chunk, both for trailer generation and truncation
    signal chunk_contains_data: std_logic;

    signal reset_chunkCounter_chunk, reset_chunkCounter_timeout, increment_chunkCounter: std_logic;
    signal create_timeout_trailer: std_logic;
    signal blockCounter : integer range 0 to NUMBER_OF_WORDS_PER_BLOCK-1;  --counts the number of 32 bit words that form a block.
    signal block_active : std_logic; --1 during the complete sequence of a block.
    type slv5_array is array(natural range <>) of std_logic_vector(4 downto 0);
    signal blockSequence : slv5_array(0 to STREAMS_TOHOST-1); --counts the block sequence in the block header
    signal create_header : std_logic := '0';
    signal block_header : std_logic_vector(31 downto 0) := (others => '0');
    signal timeout_trailer, chunk_trailer    : std_logic_vector(15 downto 0);

    signal create_trailer, create_zero_trailer : std_logic;

    signal word32out_s: std_logic_vector(31 downto 0) := (others => '0');
    signal word32out_valid_s: std_logic;
    signal s_axis_in_tvalid_p1: std_logic_vector(STREAMS_TOHOST-1 downto 0);
--    signal extra_word: std_logic_vector;
--signal extra_word: std_logic;
--signal prev_count: std_logic;


--     signal nextAvailable : integer range 0 to STREAMS_TOHOST-1;
--attribute use_dsp : string;
--attribute use_dsp of blockCounter : signal is "yes";
--attribute use_dsp of chunkCounter : signal is "yes";

begin

    end_of_block <= SwitchAxiStream;


    --Output s_axis_tready to the muxed tready output. When the MUX switched (stream_select_switched) we have to flush the pipeline
    --On the end of the block (mux_extra_tready = '0') we have to hold the pipeline for 1 clock, or for 2 on tlast because an additional trailer is inserted.
    stream_tready_mux: process(s_axis_tready_s, mux_extra_tready, stream_select_switched, mux_extra_tready_on_tlast, s_axis, stream_select_and_value)
    begin
        for i in 0 to STREAMS_TOHOST-1 loop
            --            s_axis_tready_out(i) <= stream_select_and_value(i) and
            --                                    (s_axis_tready_s or stream_select_switched) and
            ----                                    mux_extra_tready and
            --                                    (mux_extra_tready_on_tlast or not s_axis.tlast);
            -- Removed stream_select_switched and mux_extra_tready
            --            s_axis_tready_out(i) <= stream_select_and_value(i) and s_axis_tready_s and
            --                                    (mux_extra_tready_on_tlast or not s_axis.tlast);
            s_axis_tready_out(i) <= (stream_select_and_value(i) and s_axis_tready_s);
        --                                    and (mux_extra_tready_on_tlast or not s_axis.tlast);
        end loop;
    end process;

    --Pipelined MUX. Connects the selected AXI Stream input to s_axis.
    axis_mux_proc: process(aclk, stream_select_s, s_axis_in) --s_axis_in, stream_select_s)
    begin
        if rising_edge(aclk) then
            for i in 0 to STREAMS_TOHOST-1 loop
                s_axis_in_tvalid_p1(i) <= s_axis_in(i).tvalid; --Pipeline tvalid to std_logic_vector for timeout process
            end loop;
        end if;
        if stream_select_s /= STREAMS_TOHOST then
            --Pipeline is not updated when tready is low, however at the moment of switching we need to take data from the
            --correct axi stream. At that moment the tready is always low.
            --                if (s_axis_tready_s = '1' or stream_select_switched = '1') then --Flush pipeline when stream_select_switched = '1' -- Commented out this part
            s_axis <= s_axis_in(stream_select_s);
        --                end if;
        else
            s_axis.tvalid <= '0';
            s_axis.tdata <= (others => '0');
            s_axis.tuser <= (others => '0');
            s_axis.tkeep <= (others => '0');
            s_axis.tlast <= '0';
        end if;
    --        end if;
    end process;

    streamSelect: process(aclk)
        variable nextAvailable_v: integer range 0 to STREAMS_TOHOST-1;
    begin
        if rising_edge(aclk) then
            if reset = '1' then
                stream_select_s <= STREAMS_TOHOST;
                nextAvailable_v := 0;
                nextFound <= '0';
                stream_select_switched <= '0';
                startSearchingAt <= 0;
                stream_select_and_value <= (others => '0');
                mux_extra_tready <= '1';
                mux_extra_tready_on_tlast <= '1';
                SwitchAxiStream <= '0';
            else
                SwitchAxiStream <= '0';
                mux_extra_tready <= '1';
                mux_extra_tready_on_tlast <= '1';


                --Switch to the next axi stream input at the end of the block
                if(blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-2) and word32out_valid_s = '1') then
                    SwitchAxiStream <= '1';
                end if;
                --We need to hold the input tready at the end of block, before a new word is entering the pipeline
                -- Commenting out the following 3 lines to solve the extra/duplicate word problem.
                --                if(blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-3)) and word32out_valid_s = '1' then
                --                    mux_extra_tready <= '0';
                --                end if;
                --If the end of the block contains a tlast before the last word, we need to hold tready for 2 cycles
                -- Commenting out the following 3 lines
                --                if(blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-4)) and word32out_valid_s = '1' then
                --                    mux_extra_tready_on_tlast <= '0';
                --                end if;

                stream_select_switched <= '0';

                --Indication that we reached the end of a block, or no stream is selected (stream_select_s = STREAMS_TOHOST)
                if (SwitchAxiStream = '1' or (stream_select_s = STREAMS_TOHOST and nextFound = '1')) then
                    --Start searching for available data each time at a different start point.
                    if startSearchingAt /= STREAMS_TOHOST-1 then
                        startSearchingAt <= startSearchingAt + 1;
                    else
                        startSearchingAt <= 0;
                    end if;
                    stream_select_and_value <= (others => '0');
                    --NextFound indicates that we have found data in a FIFO
                    if nextFound = '1' and prog_full_in = '0' and (s_axis_prog_empty_in(nextAvailable_v) = '0' or s_axis_timeout_cnt(nextAvailable_v)(1) = '1') then --we found it, but let's check if data is still available
                        stream_select_s <= nextAvailable_v;
                        stream_select_and_value(nextAvailable_v) <= '1';
                        nextAvailable_v := startSearchingAt;
                        --                        nextAvailable <= nextAvailable_v; --debug

                        stream_select_switched <= '1';
                        nextFound <= '0';
                    else
                        stream_select_s <= STREAMS_TOHOST; --deselect, no axi stream coming in
                        nextAvailable_v := startSearchingAt;
                    --                        nextAvailable <= nextAvailable_v; --debug
                    end if;
                else
                    if stream_select_s = STREAMS_TOHOST then --MUX disabled, also set all tready signals low.
                        stream_select_and_value <= (others => '0');
                    end if;
                    if s_axis_prog_empty_in(nextAvailable_v) = '0' or s_axis_timeout_cnt(nextAvailable_v)(1) = '1' then
                        nextFound <= '1';
                    elsif nextFound = '0' then
                        if(nextAvailable_v < STREAMS_TOHOST-1) then
                            nextAvailable_v := nextAvailable_v + 1;
                        else
                            nextAvailable_v := 0;
                        end if;
                    --                        nextAvailable <= nextAvailable_v; --debug
                    end if;
                end if;
            end if;
        end if;
    end process;

    process(aclk)
    begin
        if rising_edge(aclk) then
            rst_p1 <= not aresetn;
            if rst_p1 = '1' then
                reset <= '1';
            else
                reset <= '0';
            end if;
        end if;
    end process;





    --
    --This process counts a 40MHz timeout counter, but in the 240MHz (or 250MHz) domain.
    timeout_cnt: process(aclk)
        variable cntBC : integer range 0 to (ACLK_FREQ/40)-1;
        variable timeOutEna_i_v : std_logic;
        variable timeCnt_max_v : std_logic_vector ((toHostTimeoutBitn-1) downto 0);
    begin
        if rising_edge(aclk) then
            timeCnt_max_v := '0'&timeCnt_max((toHostTimeoutBitn-1) downto 1); --Divide by 2, because we are actually creating 2 timeout pulses before timeout happens, so a maximum of 2 times this period.
            if(reset = '1') then
                cntBC := 0;
                timeOutCnt <= (others => '0');
                timeout_pulse <= '0';
                timeOutEna_i_v := '0';
            else
                timeout_pulse <= '0';
                if (cntBC < (ACLK_FREQ/40)-1) then
                    cntBC := cntBC + 1;
                else
                    cntBC := 0;
                    if timeOutCnt < timeCnt_max_v then
                        timeOutCnt <= timeOutCnt + 1;
                    else
                        timeOutCnt <= (others => '0');
                        timeout_pulse <= timeOutEna_i_v;
                    end if;
                end if;
                timeOutEna_i_v := timeOutEna_i;
            end if;
        end if;
    end process;

    timeout_pr: process(aclk)
    begin
        if rising_edge(aclk) then
            if reset = '1' then
                s_axis_timeout_cnt <= (others => "00");
            else
                for i in 0 to STREAMS_TOHOST-1 loop
                    if s_axis_prog_empty_in(i) = '1' and s_axis_in_tvalid_p1(i) = '1' and
                   ((timeout_pulse = '1') or (instant_timeout_enable(i) = '1')) then
                        s_axis_timeout_cnt(i) <= s_axis_timeout_cnt(i)(0) & '1'; --shift a '1' in, if MSB is 1, it will generate a timeout on this axi stream link
                    end if;

                end loop;
                if stream_select_s /= STREAMS_TOHOST then
                    s_axis_timeout_cnt(stream_select_s) <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    chunkCounter_proc: process(aclk)
        variable chunkCounter_v: integer range 0 to BLOCKSIZE-1;
    begin
        if rising_edge(aclk) then
            if reset = '1' then
                chunkCounter <= 0;
            else
                chunkCounter_v := chunkCounter;
                if reset_chunkCounter_chunk = '1' or reset_chunkCounter_timeout = '1' then
                    chunkCounter_v := 0;
                end if;
                if (s_axis_tready_s = '1' and s_axis.tvalid = '1') or increment_chunkCounter = '1' then
                    if s_axis.tlast = '0' or (increment_chunkCounter = '1' and prog_full_in = '0') then
                        chunkCounter_v  := chunkCounter_v + 4;
                    else
                        case s_axis.tkeep is
                            when "0000"  => chunkCounter_v := chunkCounter_v;
                            when "0001"  => chunkCounter_v := chunkCounter_v+1;
                            when "0011"  => chunkCounter_v := chunkCounter_v+2;
                            when "0111"  => chunkCounter_v := chunkCounter_v+3;
                            when "1111"  => chunkCounter_v := chunkCounter_v+4;
                            when others  => chunkCounter_v := chunkCounter_v+4;
                        end case;
                    end if;
                end if;
                chunkCounter <= chunkCounter_v;
            end if;
        end if;
    end process;

    blockCounter_proc: process(aclk)
    begin
        if rising_edge(aclk) then
            if reset = '1' then
                blockCounter <= 0;
                block_active <= '0';
                create_header <= '0';
                for i in 0 to STREAMS_TOHOST-1 loop
                    blockSequence(i) <= "00000";
                end loop;
            else
                if stream_select_switched = '1' then
                    block_active <= '1';
                    create_header <= '1';
                    blockSequence(stream_select_s) <= blockSequence(stream_select_s) + 1;
                end if;
                if word32out_valid_s = '1' then
                    if blockCounter /= NUMBER_OF_WORDS_PER_BLOCK-1 then
                        blockCounter <= blockCounter + 1;
                        create_header <= '0';
                    else
                        blockCounter <= 0;
                        block_active <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

    s_axis_tready_s <= block_active and not prog_full_in and not create_header and not create_trailer and not create_timeout_trailer and not create_zero_trailer;
    word32out_valid_s <= --block_active and
                         (
                          (create_timeout_trailer or create_trailer or create_zero_trailer or create_header) or --Headers and trailers should be validated
                          (s_axis.tvalid and s_axis_tready_s and       --Validate data payload from axi stream
                            ((or s_axis.tkeep) or (not s_axis.tlast))--Do not validate the last word if tkeep is "0000".
                          )
                        );

    blockheader_proc: process(aclk)
    begin
        if rising_edge(aclk) then
            if stream_select_s /= STREAMS_TOHOST then
                block_header <=  "11"&std_logic_vector(to_unsigned((BLOCKSIZE/1024)-1,6))&
                                x"CE" &
                                blockSequence(stream_select_s) &  --first 16 bits of block header are 0xXXCE for 32b trailer where XX is 2 bits "11" and 6 bits blocksize in kB.
                                std_logic_vector(to_unsigned(FMCHid, 5))&
                                std_logic_vector(to_unsigned(stream_select_s, 6));
            end if;
        end if;
    end process;

    chunk_proc: process(aclk)
        variable CRCErrorFlag, ChunkErrorFlag, BUSYFlag: std_logic;
        variable trunc: std_logic;
        variable first_subchunk, last_subchunk : std_logic_vector(STREAMS_TOHOST-1 downto 0);
        variable trailerType : std_logic_vector(2 downto 0);
        variable create_trailer_v: std_logic;
    begin
        if rising_edge(aclk) then
            if reset = '1' then
                reset_chunkCounter_chunk <= '0';
                create_trailer <= '0';
                create_zero_trailer <= '0';
                is_soc <= (others => '1');
                first_subchunk := (others => '0');
                last_subchunk := (others => '0');
                create_trailer_v := '0';
                CRCErrorFlag := '0';
                ChunkErrorFlag := '0';
                BUSYFlag := '0';
                trunc := '0';

            else
                reset_chunkCounter_chunk <= '0';
                create_trailer_v := '0';
                create_trailer <= '0';
                if(s_axis.tvalid = '1' and s_axis_tready_s = '1') then
                    if(s_axis.tuser(0) = '1') then -- Check for CRC error flag in tuser
                        CRCErrorFlag := '1';
                    end if;

                    if(s_axis.tuser(1) = '1') then -- Check for chunk error flag in tuser
                        ChunkErrorFlag := '1';
                    end if;

                    if(s_axis.tuser(2) = '1') then -- Check for BUSY flag in tuser
                        BUSYFlag := '1';
                    end if;

                    if(s_axis.tuser(3) = '1') then -- truncation mechanism
                        trunc := '1';
                    end if;
                end if;
                if(blockCounter = 0 and stream_select_s /= STREAMS_TOHOST) then
                    first_subchunk(stream_select_s) := '0';
                end if;
                --                if((blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-2)) and create_timeout_trailer = '0' and create_trailer = '0' and ((s_axis.tvalid = '1'and s_axis_tready_s = '1') or blockCounter /= 0)) then --end of block, always create trailer.
                if(((word32out_valid_s = '0' AND blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-1)) OR  (word32out_valid_s = '1' AND blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-2))) and create_timeout_trailer = '0' and create_trailer = '0' and ((s_axis.tvalid = '1'and s_axis_tready_s = '1') or chunkCounter /= 0)) then --end of block, always create trailer.
                    create_trailer <= '1';
                    create_trailer_v := '1';
                end if;


                --First / last subchunk generation.
                if(s_axis.tvalid = '1' and s_axis_tready_s = '1') then
                    if(is_soc(stream_select_s) = '1') then
                        is_soc(stream_select_s) <= '0';
                        first_subchunk(stream_select_s) := '1';
                        last_subchunk(stream_select_s) := '0';
                    end if;
                    if s_axis.tlast = '1' then --Last word of a chunk, we will add a chunk trailer in the next clock cycle.
                        create_trailer <= '1';
                        create_trailer_v := '1';
                        last_subchunk(stream_select_s) := '1';
                        is_soc(stream_select_s) <= '1';
                    end if;
                end if;

                --                if(blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-2) and create_trailer = '1') then --trailer created one word before last, add zero trailer.
                if( ((word32out_valid_s = '0' AND blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-1)) OR  (word32out_valid_s = '1' AND blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-2))) and create_trailer_v = '0' and create_timeout_trailer = '0') then --trailer created one word before last, add zero trailer. -- we made changes here
                    create_zero_trailer <= '1';          --will be pipelined, zero trailer comes next clock cycle
                end if;
                if(create_trailer_v = '1') then    --add trailer to the datastream,
                    if trunc = '1' then
                        is_soc(stream_select_s) <= '1';
                    end if;
                    trailerType := (((not first_subchunk(stream_select_s)) and (not last_subchunk(stream_select_s))) and (not trunc)) &
                                   (last_subchunk(stream_select_s) or trunc) &
                                   first_subchunk(stream_select_s);

                    chunk_trailer <= trailerType & trunc & ChunkErrorFlag & CRCErrorFlag & BUSYFlag & "0" & x"00"; --Excluding chunk counter
                    reset_chunkCounter_chunk <= '1';
                    ChunkErrorFlag := '0';
                    CRCErrorFlag := '0';
                    BUSYFlag := '0';
                    trunc := '0';
                end if;
                if create_zero_trailer = '1' then
                    create_zero_trailer <= '0';
                end if;
                if ( create_timeout_trailer = '1' and create_trailer = '0' and create_zero_trailer = '0') then -- implement timout mechanism. Check reset chunk counter to make sure we don't get 2 timeoout trailers next to each other.
                    is_soc(stream_select_s) <= '1';
                    first_subchunk(stream_select_s) := '0';
                    last_subchunk(stream_select_s) := '0';
                end if;

            end if;
        end if;
    end process;


    timeout_trailer_proc: process(aclk)
        variable create_timeout_trailer_v: std_logic;
    begin
        if rising_edge(aclk) then
            if reset = '1' then
                increment_chunkCounter <= '0';
                reset_chunkCounter_timeout <= '0';
                create_timeout_trailer <= '0';
                timeout_incomplete_frame_cnt <= 0;
                chunk_contains_data <= '0';
                create_timeout_trailer_v := '0';
            else
                reset_chunkCounter_timeout <= '0';
                increment_chunkCounter <= '0'; --Happens only in timeout situation.
                if reset_chunkCounter_chunk = '1' or reset_chunkCounter_timeout = '1' then
                    chunk_contains_data <= '0';
                end if;
                if (s_axis_tready_s = '1' and s_axis.tvalid = '1') then
                    chunk_contains_data <= '1';
                end if;
                if timeout_incomplete_frame_cnt /= 0 then
                    if timeout_incomplete_frame_cnt = 1 and (blockCounter /= 0) and (blockCounter /= NUMBER_OF_WORDS_PER_BLOCK-1)  and reset_chunkCounter_chunk = '0' and reset_chunkCounter_timeout = '0' then
                        create_timeout_trailer_v := not s_axis.tvalid;
                    end if;
                    timeout_incomplete_frame_cnt <= timeout_incomplete_frame_cnt - 1;
                end if;
                --!If we started a block (due to timeout) but no data is arriving, it depends on whether tlast was asserted. If tlast is '0', it may take up to 80 cycles for a 2-bit E-Link to push the next byte.
                if(blockCounter > 2 and (blockCounter /= NUMBER_OF_WORDS_PER_BLOCK-1) and s_axis.tvalid = '0' and stream_select_s /= STREAMS_TOHOST and reset_chunkCounter_chunk = '0' and reset_chunkCounter_timeout = '0') then
                    if(s_axis.tlast = '1') then --If tlast was asserted, create timeout immediately, otherwise wait 80 clocks.
                        create_timeout_trailer_v := '1'; --this signal will be latched and timeout (0000 or 0xA0XX) will be created
                    elsif  timeout_incomplete_frame_cnt = 0 and  create_timeout_trailer = '0' then
                        timeout_incomplete_frame_cnt <= 255;
                    end if;
                end if;
                if s_axis.tvalid = '1' then
                    timeout_incomplete_frame_cnt <= 0;
                end if;
                if create_trailer = '1' then
                    create_timeout_trailer_v := '0'; --If we are already creating a trailer, cancel timeout process
                end if;

                create_timeout_trailer <= create_timeout_trailer_v;

                if ( create_timeout_trailer_v = '1') then -- implement timout mechanism. Check reset chunk counter to make sure we don't get 2 timeoout trailers next to each other.


                    --                    if(blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-2)) then --end of block, we need to stop sending 0 trailers and add the 0xA0XX timeout trailer, then go back to idle
                    if(((word32out_valid_s = '0' AND blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-1)) OR  (word32out_valid_s = '1' AND blockCounter = (NUMBER_OF_WORDS_PER_BLOCK-2)))) then --end of block, we need to stop sending 0 trailers and add the 0xA0XX timeout trailer, then go back to idle
                        timeout_trailer <= x"A000";
                        reset_chunkCounter_timeout <= '1';  --chunk counter is reset here
                        create_timeout_trailer_v := '0';  --back to idle operation

                    else
                        if  chunk_contains_data = '1' then
                            timeout_trailer <= x"B000";
                            reset_chunkCounter_timeout <= '1';--chunk counter is reset here, as we added a timeout + truncation to end an incomplete chunk
                            chunk_contains_data <= '0';
                        elsif(s_axis.tvalid = '1' and blockCounter < (NUMBER_OF_WORDS_PER_BLOCK-4)) then --Busy padding the block, but new data arrived, create timeout trailer and continue normal operation.
                            timeout_trailer <=  x"A000";
                            reset_chunkCounter_timeout <= '1';  --chunk counter is reset here
                            create_timeout_trailer_v := '0';--back to idle operation

                        else      --No end of block, and no data coming in. Fill timeout frame with 0x0000
                            timeout_trailer <= x"0000"; --Fill block with zeros.
                            increment_chunkCounter <= '1';
                        end if;
                    end if;
                end if;

            end if;
        end if;
    end process;

    word32out_proc: process(block_header, chunk_trailer, s_axis, create_header, create_trailer, create_timeout_trailer, create_zero_trailer, chunkCounter, reset_chunkCounter_chunk, reset_chunkCounter_timeout, timeout_trailer)
        constant chunkCounter_bits: integer := f_log2(BLOCKSIZE);
    begin
        if create_header = '1' then
            word32out_s <= block_header;
        elsif create_zero_trailer = '1' then
            word32out_s <= x"0000_0000";
        elsif create_trailer = '1' then
            word32out_s(31 downto 16) <= chunk_trailer;

            if reset_chunkCounter_chunk = '1' then
                word32out_s(15 downto chunkCounter_bits) <= (others => '0');
                word32out_s(chunkCounter_bits-1 downto 0) <= std_logic_vector(to_unsigned(chunkCounter, chunkCounter_bits));
            else
                word32out_s(15 downto 0) <= x"0000";
            end if;
        elsif create_timeout_trailer = '1' then
            word32out_s(31 downto 16) <= timeout_trailer;
            if reset_chunkCounter_timeout = '1' then
                word32out_s(15 downto chunkCounter_bits) <= (others => '0');
                word32out_s(chunkCounter_bits-1 downto 0) <= std_logic_vector(to_unsigned(chunkCounter, chunkCounter_bits));
            else
                word32out_s(15 downto 0) <= x"0000";
            end if;
        else
            word32out_s <= s_axis.tdata;
        end if;
    end process;


    --    debug_proc: process(aclk)
    --    begin
    --        if rising_edge(aclk) then
    --            --SM: Detetion of the extra 32b word
    --            if(word32out_s(31 downto 29) = "001"  AND create_trailer = '1') then
    --                prev_count <= word32out_s(2);
    --            end if;
    --        --            if(chunk_trailer(15 downto 13) = "010") then
    --        --                extra_word <= temp_chunkCounter(2) XOR prev_count;
    --        --            end if;
    --        end if;
    --    end process;


    --    debug2_proc: process(word32out_s)
    --        constant chunkCounter_bits: integer := f_log2(BLOCKSIZE);
    --        variable temp_chunkCounter: std_logic_vector(15 downto 0);
    --    begin
    --        --SM: Detetion of the extra 32b word
    --        if(word32out_s(31 downto 29) = "010" AND create_trailer = '1' ) then
    --            extra_word <= word32out_s(2) XOR prev_count;
    --        else
    --            extra_word <= '0';
    --        end if;

    --    end process;

    output_proc: process(aclk)
    begin
        if rising_edge(aclk) then
            word32out <= word32out_s;
            word32out_valid <= word32out_valid_s;
        end if;
    end process;

--ILA goes here
--create_zero_trailer
--create_header
--increment_chunkCounter
--reset_chunkCounter_timeout
--timeout_incomplete_frame_cnt
--chunk_contains_data
--s_axis_tready_s
--s_axis.tdata --32b
--s_axis.tvalid
--s_axis.tlast
--s_axis.tkeep --4b
--word32out_s
--word32out_valid_s --32b
--extra_word

--    ila_block: block
--        signal s_axis_tready_ila: std_logic_vector(1 downto 0);
--    begin
--        --! Workaround for the case of FULL mode where STREAMS_TOHOST = 1, and the build will fail on probe16
--        g_ilabits: for i in 0 to STREAMS_TOHOST-1 generate
--            g_limit2: if i < 2 generate
--                s_axis_tready_ila(i) <= s_axis_tready_out(i);
--            end generate;
--        end generate;
--        SM_instance_name : ila_1
--            PORT MAP (
--                clk => aclk,

--                probe0(0) => create_timeout_trailer,
--                probe1(0) => create_trailer,
--                probe2(0) => create_zero_trailer,
--                probe3(0) => create_header,
--                probe4(0) => increment_chunkCounter,
--                probe5(0) => reset_chunkCounter_timeout,
--                probe6 => std_logic_vector(to_unsigned(timeout_incomplete_frame_cnt, 8)),
--                probe7(0) => chunk_contains_data,
--                probe8(0) => s_axis_tready_s,
--                probe9 => s_axis.tdata,
--                probe10(0) => s_axis.tvalid,
--                probe11(0) => s_axis.tlast,
--                probe12 => s_axis.tkeep,
--                probe13(0) => word32out_valid_s,
--                probe14 => word32out_s,
--                probe15 => std_logic_vector(to_unsigned(nextAvailable,2)),
--                probe16 => s_axis_tready_out(1) & s_axis_tready_out(0),
--                probe17 => s_axis_in(0).tdata,
--                probe18(0) => s_axis_in(0).tvalid,
--                probe19(0) => s_axis_in(0).tlast,
--                probe20 => s_axis_in(0).tkeep,
--                probe21 => std_logic_vector(to_unsigned(stream_select_s, 2)),
--                probe22(0) => stream_select_switched,
--                probe23(0) => SwitchAxiStream,
--                probe24(0) => nextFound,
--                probe25 => s_axis_prog_empty_in(1) & s_axis_prog_empty_in(0),
--                probe26 => s_axis_timeout_cnt(1) & s_axis_timeout_cnt(0),
--                probe27(0) => prog_full_in,
--                probe28 => std_logic_vector(to_unsigned(blockCounter, 8))
--            );

--    end block;
--  probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe9 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--  probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe12 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--  probe13 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe14 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--  probe15 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)

end Behavioral;
