--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Enrico Gamberini
--!               Thei Wijnen
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    12/09/2016
--! Module Name:    thFMdm
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.std_logic_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;
    use work.centralRouter_package.all;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;

library xpm;
    use xpm.vcomponents.all;
--! to-Host centralRouter logic
entity CRToHostdm is
    generic (
        FMCHid                  : integer := 0;
        toHostTimeoutBitn       : integer := 8;
        STREAMS_TOHOST : integer := 1;
        BLOCKSIZE           : integer := 1024;
        DATA_WIDTH          : integer := 256;
        LINK_CONFIG         : integer := 0;
        USE_URAM            : boolean;
        FIFO_DEPTH          : positive := BLOCKSIZE/2;
        ACLK_FREQ           : integer  --Used to compensate the timeout counter to run at 40MHz (BC)
    );
    port  (
        toHostFifo_wr_clk   : in  std_logic;
        clk40           : in  std_logic;
        aclk            : in  std_logic;
        aclk64          : in  std_logic; --AXI clock for 64-bit interface
        aresetn         : in  std_logic;
        register_map_control               : in  register_map_control_type; --! configuration settings, 64 bit per EGROUP (7 EGROUPS total)
        -- RX side
        s_axis            : in axis_32_array_type(0 to STREAMS_TOHOST-1);
        s_axis_tready     : out axis_tready_array_type(0 to STREAMS_TOHOST-1);
        s_axis_prog_empty : in axis_tready_array_type(0 to STREAMS_TOHOST-1);

        --In case of a 25Gb/s link we use 64b axi stream
        s_axis64            : in axis_64_type;
        s_axis64_tready     : out std_logic;
        s_axis64_prog_empty : in std_logic;
        -- wupper side:
        -- 1. read from specified channel
        fmchFifo_flush      : in  std_logic;
        fmchFifo_re         : in  std_logic;
        fmchFifo_dout       : out std_logic_vector (DATA_WIDTH-1 downto 0);
        fmchFifo_dvalid     : out std_logic;
        fmchFifo_hasBlock   : out std_logic; -- out, 'block_ready' flag
        fmchXoffout         : out std_logic; -- out test purposes only, flag to a data source to stop transmitting
        fmchHighThreshCrossed      : out std_logic;
        fmchHighThreshCrossedLatch : out std_logic;
        fmchLowThreshCrossed       : out std_logic;
        AXIS_ID_out         : out std_logic_vector(10 downto 0);
        --fmchXoffin          : in  std_logic; -- in, crOUTfifo is pfull 4060/3830 (out of 4096), stop writing flag
        --
        ch_prog_full_out        : out std_logic
    );
end CRToHostdm;

architecture Behavioral of CRToHostdm is

    --
    function AXIS_WIDTH(IS64: integer) return integer is
    begin
        if IS64 = 1 then
            return 64;
        else
            return 32;
        end if;
    end function;

    signal aclk_s : std_logic; --Connected to either aclk or aclk64, depending on generics.
    signal timeCnt_max    : std_logic_vector ((toHostTimeoutBitn-1) downto 0);
    signal timeCnt_ena    : std_logic;
    --
    signal ch_prog_full,chFIFO_din_valid,chFIFO_pfull_r1,chFIFO_pfull,chFIFO_pempty  : std_logic;

    signal chFIFO_din     : std_logic_vector(AXIS_WIDTH(LINK_CONFIG)-1 downto 0);
    signal chFIFO_dout_s  : std_logic_vector(DATA_WIDTH-1 downto 0);

    signal chFIFO_din_s   : std_logic_vector(AXIS_WIDTH(LINK_CONFIG)-1 downto 0);
    signal chFIFO_din_valid_s: std_logic;

    --
    --signal xoff_fm_ch_fifo_thresh_low: std_logic_vector(3 downto 0);
    --signal xoff_fm_ch_fifo_thresh_high: std_logic_vector(3 downto 0);
    constant FIFO_RD_COUNT_WIDTH: natural := f_log2(FIFO_DEPTH/(DATA_WIDTH/32))+1;
    constant FIFO_WR_COUNT_WIDTH: natural := f_log2(FIFO_DEPTH)+1;
    signal wr_data_count : std_logic_vector(FIFO_WR_COUNT_WIDTH-1 downto 0);
    constant PROG_EMPTY_THRESH : natural := (BLOCKSIZE/(DATA_WIDTH/8))-1;
    signal rd_data_count : std_logic_vector(FIFO_RD_COUNT_WIDTH-1 downto 0);
    signal timeCnt_max_aclk: std_logic_vector(toHostTimeoutBitn-1 downto 0);
    signal timeCnt_ena_aclk : std_logic;
    signal fmchFifo_flush_aclk : std_logic;
    signal fmchFifo_flush_toHostFifo_wr_clk : std_logic;
    signal aresetn_aclk : std_logic;
    signal wr_data_count_40      : std_logic_vector(3 downto 0);

    signal xoff_threshold_low  : std_logic_vector(3 downto 0);
    signal xoff_threshold_high : std_logic_vector(3 downto 0);
    signal xoff_clear_latch    : std_logic;



begin
    ------------------------------------------------------------
    -- configuration registers map
    ------------------------------------------------------------
    timeCnt_max <= register_map_control.TIMEOUT_CTRL.TIMEOUT((toHostTimeoutBitn-1) downto 0);
    timeCnt_ena <= to_sl(register_map_control.TIMEOUT_CTRL.ENABLE);



    ------------------------------------------------------------
    --  AXI4 channel stream controller
    ------------------------------------------------------------
    g_32b: if LINK_CONFIG = 0 generate
        signal instant_timeout_enable_aclk : std_logic_vector(STREAMS_TOHOST-1 downto 0);
    begin

        xpm_cdc_timeCnt_ena : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => timeCnt_ena,
                dest_clk => aclk,
                dest_out => timeCnt_ena_aclk
            );

        xpm_cdc_array_single_inst_timeCnt_max : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => toHostTimeoutBitn
            )
            port map (
                src_clk => '0',
                src_in => timeCnt_max,
                dest_clk => aclk,
                dest_out => timeCnt_max_aclk
            );

        g_24links: if FMCHid < 24 generate
            xpm_cdc_array_single_inst_instant_timeout_enable : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0,
                    WIDTH => STREAMS_TOHOST
                )
                port map (
                    src_clk => '0',
                    src_in => register_map_control.CRTOHOST_INSTANT_TIMEOUT_ENA(FMCHid)(STREAMS_TOHOST-1 downto 0),
                    dest_clk => aclk,
                    dest_out => instant_timeout_enable_aclk
                );
        else generate
            instant_timeout_enable_aclk <= (others => '0');
        end generate;

        sync_aresetn : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 0,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => aresetn,
                dest_clk => aclk,
                dest_rst => aresetn_aclk
            );

        chStreamController: entity work.ToHostAxiStreamController
            generic map(
                FMCHid              => FMCHid,
                toHostTimeoutBitn   => toHostTimeoutBitn,
                BLOCKSIZE           => BLOCKSIZE,
                STREAMS_TOHOST      => STREAMS_TOHOST,
                ACLK_FREQ           => ACLK_FREQ
            )
            port map (
                aclk                 => aclk,
                aresetn              => aresetn_aclk,
                s_axis_in            => s_axis,
                s_axis_tready_out    => s_axis_tready,
                s_axis_prog_empty_in => s_axis_prog_empty,
                timeOutEna_i         => timeCnt_ena_aclk,
                timeCnt_max          => timeCnt_max_aclk,
                instant_timeout_enable => instant_timeout_enable_aclk,
                prog_full_in         => ch_prog_full, -- in, downstream wm fifo is full or main , can't happen in normal operation
                word32out            => chFIFO_din,    -- to chFIFO
                word32out_valid      => chFIFO_din_valid,-- to chFIFO
                end_of_block         => open
            );
        aclk_s <= aclk;
        s_axis64_tready <= '0';
    end generate;

    g_64b: if LINK_CONFIG = 1 generate
        xpm_cdc_timeCnt_ena : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map (
                src_clk => '0',
                src_in => timeCnt_ena,
                dest_clk => aclk64,
                dest_out => timeCnt_ena_aclk
            );

        xpm_cdc_array_single_inst_timeCnt_max : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => toHostTimeoutBitn
            )
            port map (
                src_clk => '0',
                src_in => timeCnt_max,
                dest_clk => aclk64,
                dest_out => timeCnt_max_aclk
            );

        sync_aresetn : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 0,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => aresetn,
                dest_clk => aclk64,
                dest_rst => aresetn_aclk
            );
        chStreamController: entity work.ToHostAxiStream64Controller
            generic map(
                FMCHid              => FMCHid,
                toHostTimeoutBitn   => toHostTimeoutBitn,
                BLOCKSIZE           => BLOCKSIZE
            )
            port map (
                aclk                 => aclk64,
                aresetn              => aresetn_aclk,
                s_axis_in            => s_axis64,
                s_axis_tready_out    => s_axis64_tready,
                s_axis_prog_empty_in => s_axis64_prog_empty,
                timeOutEna_i         => timeCnt_ena_aclk,
                timeCnt_max          => timeCnt_max_aclk,
                prog_full_in         => ch_prog_full, -- in, downstream wm fifo is full or main , can't happen in normal operation
                word64out            => chFIFO_din,    -- to chFIFO
                word64out_valid      => chFIFO_din_valid,-- to chFIFO
                end_of_block         => open
            );
        aclk_s <= aclk64;
        s_axis_tready <= (others => '0');
    end generate;


    process(aclk_s)
    begin
        if rising_edge(aclk_s) then
            chFIFO_pfull_r1 <= chFIFO_pfull;
        end if;
    end process;
    ch_prog_full     <= chFIFO_pfull or chFIFO_pfull_r1; -- or fmchXoffin;

    --
    ------------------------------------------------------------
    --  full mode channel FIFO
    ------------------------------------------------------------
    chFIFO_din_s     <= chFIFO_din;
    chFIFO_din_valid_s <= chFIFO_din_valid;



    chFifo_block: block
        signal empty: std_logic;
    begin

        chFIFO : entity work.xpm_uram_fifo_async
            generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                FIFO_MEMORY_TYPE => "auto", --string; "auto", "block", or "distributed";
                FIFO_WRITE_DEPTH => FIFO_DEPTH, --positive integer
                RELATED_CLOCKS => 0, --positive integer; 0 or 1
                WRITE_DATA_WIDTH => AXIS_WIDTH(LINK_CONFIG), --positive integer
                READ_MODE => "fwft", --string; "std" or "fwft";
                FIFO_READ_LATENCY => 1, --positive integer;
                FULL_RESET_VALUE => 1, --positive integer; 0 or 1;
                USE_ADV_FEATURES => "0406",
                READ_DATA_WIDTH => DATA_WIDTH, --positive integer
                CDC_SYNC_STAGES => 2, --positive integer
                WR_DATA_COUNT_WIDTH => FIFO_WR_COUNT_WIDTH, --positive integer
                PROG_FULL_THRESH => FIFO_DEPTH-67, --positive integer
                RD_DATA_COUNT_WIDTH => FIFO_RD_COUNT_WIDTH, --positive integer
                PROG_EMPTY_THRESH => 5,
                DOUT_RESET_VALUE => "0", --string
                ECC_MODE => "no_ecc", --string; "no_ecc" or "en_ecc";
                WAKEUP_TIME => 0, --positive integer; 0 or 2;
                USE_URAM => USE_URAM
            )
            port map (
                sleep => '0',
                rst => fmchFifo_flush_aclk,
                wr_clk => aclk_s,
                wr_en => chFIFO_din_valid_s,
                din => chFIFO_din_s,
                full => open,
                prog_full => chFIFO_pfull,
                wr_data_count => wr_data_count,
                overflow => open,
                wr_rst_busy => open,
                almost_full => open,
                wr_ack => open,
                rd_clk => toHostFifo_wr_clk,
                rd_en => fmchFifo_re,
                dout => chFIFO_dout_s,
                empty => empty,
                prog_empty => open,
                rd_data_count => rd_data_count,
                underflow => open,
                rd_rst_busy => open,
                almost_empty => open,
                data_valid => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr => open,
                dbiterr => open
            );

        valid_proc: process(toHostFifo_wr_clk)
        begin
            if rising_edge(toHostFifo_wr_clk) then
                fmchFifo_dvalid <= fmchFifo_re and not empty;
                if fmchFifo_re = '1' then
                    fmchFifo_dout <= chFIFO_dout_s; --Turn FWFT into STD fifo again.
                end if;
            end if;
        end process;

        AXIS_ID_out <= chFIFO_dout_s(10 downto 0);

    end block;


    xoff_threshold_low <= register_map_control.XOFF_FM_CH_FIFO_THRESH_LOW;
    xoff_threshold_high <= register_map_control.XOFF_FM_CH_FIFO_THRESH_HIGH;
    xoff_clear_latch <= to_sl(register_map_control.XOFF_FM_HIGH_THRESH.CLEAR_LATCH);

    xpm_cdc_gray_wr_data_count : xpm_cdc_gray
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            REG_OUTPUT => 1,
            SIM_ASSERT_CHK => 0,
            SIM_LOSSLESS_GRAY_CHK => 0,
            WIDTH => 4
        )
        port map (
            src_clk => aclk_s,
            src_in_bin => wr_data_count(FIFO_WR_COUNT_WIDTH-1 downto FIFO_WR_COUNT_WIDTH-4),
            dest_clk => clk40,
            dest_out_bin => wr_data_count_40
        );


    xoff_thresholds: process(clk40)                  -- fifo write clock
    begin
        if rising_edge(clk40) then
            if(wr_data_count_40 > xoff_threshold_high) then
                fmchXoffout <= '1';
                fmchHighThreshCrossed <= '1';
                fmchHighThreshCrossedLatch <= '1';
            else
                fmchHighThreshCrossed <= '0';
            end if;
            if(xoff_clear_latch='1') then
                fmchHighThreshCrossedLatch <= '0';
            end if;
            if(wr_data_count_40 < xoff_threshold_low) then
                fmchXoffout <= '0';
                fmchLowThreshCrossed <= '0';
            else
                fmchLowThreshCrossed <= '1';
            end if;
        end if;
    end process;


    --! We have to calculate prog_empty using rd_data_count rather than internally in the xpm_fifo_async because the macro won't allow that for BLOCKSIZE=4096 (ProtoDUNE).
    prog_empty_proc: process(toHostFifo_wr_clk)
    begin
        if rising_edge(toHostFifo_wr_clk) then
            if rd_data_count <= PROG_EMPTY_THRESH then
                chFIFO_pempty <= '1';
            else
                chFIFO_pempty <= '0';
            end if;
        end if;
    end process;


    --
    fmchFifo_hasBlock <= (not chFIFO_pempty) and (not fmchFifo_flush_toHostFifo_wr_clk); -- chFIFO_pempty is '0' when there is at least one whole block in the fifo

    sync_fmchFifo_flush1 : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => fmchFifo_flush,
            dest_clk => aclk_s,
            dest_rst => fmchFifo_flush_aclk
        );

    sync_fmchFifo_flush2 : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 1,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => fmchFifo_flush,
            dest_clk => toHostFifo_wr_clk,
            dest_rst => fmchFifo_flush_toHostFifo_wr_clk
        );
    --
    ch_prog_full_out <= ch_prog_full or fmchFifo_flush_aclk;
--


end Behavioral;

