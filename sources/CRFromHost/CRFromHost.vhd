--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Andrea Borga
--!               Enrico Gamberini
--!               Thei Wijnen
--!               Filiberto Bonini
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS, Nikhef.  
--! Engineer: juna, fschreud
--! 
--! Create Date:    01/03/2015  
--! Module Name:    CRFM
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee, unisim;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--use ieee.std_logic_unsigned.all;
use UNISIM.VCOMPONENTS.all;
use work.all;
use work.pcie_package.all;
use work.centralRouter_package.all;
use work.FELIX_package.all;
use work.axi_stream_package.all;

--! top module for CRFM logic
entity CRFromHost is
generic (
    GBT_NUM                         : integer := 1;
    EnableFrHo_Egroup0Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup0Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup0Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup0Eproc8_8b10b  : boolean := true;
    EnableFrHo_Egroup1Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup1Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup1Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup1Eproc8_8b10b  : boolean := true;
    EnableFrHo_Egroup2Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup2Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup2Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup2Eproc8_8b10b  : boolean := true;
    EnableFrHo_Egroup3Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup3Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup3Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup3Eproc8_8b10b  : boolean := true;
    EnableFrHo_Egroup4Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup4Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup4Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup4Eproc8_8b10b  : boolean := true;
    wideMode                : boolean := false; -- [set in top module]
    --
    GENERATE_XOFF           : boolean := true;
    CARD_TYPE               : integer;
    DATA_WIDTH              : integer := 256;
    generate_IC_EC_TTC_only : boolean := false;
    includeNoEncodingCase   : boolean := false;
    GENERATE_FEI4B          : boolean := false;
    GENERATE_LCB_ENC        : boolean := false
    );
port  ( 
    clk40   : in  std_logic; 
    clk80   : in  std_logic;
    clk160  : in  std_logic;
    aresetn     : in std_logic;
    -----
    register_map_control               : in  register_map_control_type; --! configuration settings, 64 bit per EGROUP (7 EGROUPS total)
    CR_FROMHOST_GBT_MON                : out bitfield_cr_fromhost_gbt_mon_r_array_type;
    register_map_control_appreg_clk    : in  register_map_control_type; --! configuration settings, 64 bit per EGROUP (7 EGROUPS total)
    appreg_clk                         : in  std_logic;
    --------------------------------------------------------
    --- 'from-Host' direction: normal GBT mode
    --------------------------------------------------------
    TTCin               : in  std_logic_vector (9 downto 0); 
    fromHostFifo_dout   : in  std_logic_vector(DATA_WIDTH-1 downto 0);
    fromHostFifo_rd_en  : out std_logic;
    fromHostFifo_empty  : in  std_logic;
    fromHostFifo_rd_clk : out std_logic;
    fromHostFifo_rst    : out std_logic;
    fhXoff              : out std_logic;
    
    --This must be moved to the Encoding block
    fhOutData_array     : out txrx120b_type(0 to (GBT_NUM-1))
    );
end CRFromHost;

architecture Behavioral of CRFromHost is


constant zerosGBT_NUMarray  : std_logic_vector ((GBT_NUM-1) downto 0) := (others=>'0');
--
signal cr_fifo_flush,cr_rst : std_logic;
----    
signal TTCin_array,xTTCin_array  : TTCin_array_type(0 to (GBT_NUM-1));
----
signal to_GBTs_array    : cr_DOUT_array_type(0 to (GBT_NUM-1));



type gbtFHconfig_array_type is array (0 to (GBT_NUM-1)) of crUpstreamConfig_type;
signal gbtFHconfig_array : gbtFHconfig_array_type;



----
signal UpFifoPfull_array, fhFifoWE_array, fhXoff_array : std_logic_vector ((GBT_NUM-1) downto 0) := (others=>'0');

signal crINfifo_empty, crINfifo_re, UpFifoPfull_or : std_logic;


signal fhFifoDout_rdy : std_logic := '0';
signal fhFifoDout  : std_logic_vector (DATA_WIDTH-1 downto 0);

--

signal UpFifoFull_mon_array : UpFifoFull_mon_array_type(0 to 23);


--signal fromHostDATA_we_wide,fromHostDATA_we_1wclk : std_logic;
--signal fromHostDATA_we : std_logic := '0';
--signal fromHostDATA : std_logic_vector (255 downto 0);


type TTC_DELAY_array_type is array (0 to 23) of std_logic_vector(3 downto 0);
signal TTC_DELAY_array : TTC_DELAY_array_type;
------------------------------------------------------------------------------------------
signal auxUpstreamConfig : std_logic_vector (0 downto 0); 


signal upfifoDoutClk : std_logic;

signal rst: std_logic;


signal ExtendedTestPulse    : std_logic; -- NSW required a 32x 40MHz clock pulse, 
		
signal TestPulseCounter     : integer range 0 to 31; -- 32 states counter
		

begin

--Assign multiplexer clock according to the number of channels (FromHost direction), we need at least 12.5MHz per GBT_NUM
g_upfifoclk_40M: if GBT_NUM <= 3 generate
    upfifoDoutClk <= clk40;
end generate;
g_upfifoclk_80M: if GBT_NUM > 3 and GBT_NUM<= 6 generate
    upfifoDoutClk <= clk80;
end generate;
g_upfifoclk_160M: if GBT_NUM > 6  generate
    upfifoDoutClk <= clk160;
end generate;




------------------------------------------------------------------------------------------
-- resets and FIFO flush, rst is from appreg_clk
------------------------------------------------------------------------------------------
rst <= not aresetn;
rst0: entity work.CRresetManager (Behavioral)
port map( 
    clk40           => clk40,
    rst        => rst,  -- appreg_clk domain
    -- 
    cr_rst          => cr_rst,
    cr_fifo_flush   => cr_fifo_flush
    );
--- 

g_links: for i in 0 to 23 generate
CR_FROMHOST_GBT_MON(i).EPATH0_ALMOST_FULL      <= UpFifoFull_mon_array(i)(7 downto 0);
CR_FROMHOST_GBT_MON(i).EPATH1_ALMOST_FULL      <= UpFifoFull_mon_array(i)(15 downto 8);
CR_FROMHOST_GBT_MON(i).EPATH2_ALMOST_FULL      <= UpFifoFull_mon_array(i)(23 downto 16);
CR_FROMHOST_GBT_MON(i).EPATH3_ALMOST_FULL      <= UpFifoFull_mon_array(i)(31 downto 24);
CR_FROMHOST_GBT_MON(i).EPATH4_ALMOST_FULL      <= UpFifoFull_mon_array(i)(39 downto 32);
CR_FROMHOST_GBT_MON(i).MINI_EGROUP_ALMOST_FULL <= UpFifoFull_mon_array(i)(40 downto 40);
end generate;


------------------------------------------------------------------------------------------
--
-- from-Host data: GBT-like channels (without IC/EC)
--
------------------------------------------------------------------------------------------

------------------------------------------------------------
-- TTC fanout
------------------------------------------------------------
-- mapping of the TTC delay (per GBT channel)
TTC_DELAY_array <= (
register_map_control.TTC_DELAY( 0),register_map_control.TTC_DELAY( 1),register_map_control.TTC_DELAY( 2),register_map_control.TTC_DELAY( 3),
register_map_control.TTC_DELAY( 4),register_map_control.TTC_DELAY( 5),register_map_control.TTC_DELAY( 6),register_map_control.TTC_DELAY( 7),
register_map_control.TTC_DELAY(08),register_map_control.TTC_DELAY(09),register_map_control.TTC_DELAY(10),register_map_control.TTC_DELAY(11),
register_map_control.TTC_DELAY(12),register_map_control.TTC_DELAY(13),register_map_control.TTC_DELAY(14),register_map_control.TTC_DELAY(15),
register_map_control.TTC_DELAY(16),register_map_control.TTC_DELAY(17),register_map_control.TTC_DELAY(18),register_map_control.TTC_DELAY(19),
register_map_control.TTC_DELAY(20),register_map_control.TTC_DELAY(21),register_map_control.TTC_DELAY(22),register_map_control.TTC_DELAY(23));
---

process (rst, clk40)
begin
    if (rst = '1') then
        ExtendedTestPulse   <= '0'; -- clear the extended test pulse signal
        TestPulseCounter    <= 0;   -- hold the extended test pulse counter
    elsif rising_edge(clk40) then    
        -- NSW Test Pulse bit location
        if (TTCin(4) = '1') then
            ExtendedTestPulse   <= '1'; -- set the extended test pulse signal
            TestPulseCounter    <= 31;  -- initial the extended test pulse counter 
        -- 32x 40MHz clocks pulse width
        elsif (TestPulseCounter = 0) then
            ExtendedTestPulse   <= '0'; -- clear the extended test pulse signal
            TestPulseCounter    <= 0;   -- hold the extended test pulse counter
        else
            ExtendedTestPulse   <= ExtendedTestPulse;       -- keep the value
            TestPulseCounter    <= TestPulseCounter - 1;    -- the extended test pulse counter count down
        end if;
    end if;
end process;


crFH:  for I in 0 to (GBT_NUM-1) generate 
signal FHicFIFOdin  : std_logic_vector (7 downto 0);    
signal FHicFIFOwe,FHicPacketRdy : std_logic;

begin
--
GBTdmOUTPUT:  for J in 0 to 4 generate -- mapping to 120 bit
     fhOutData_array(I)((32+J*16+15) downto (32+J*16)) <= to_GBTs_array(I)(J);     
end generate GBTdmOUTPUT;
-- bits (119 downto 112) are unused
fhOutData_array(I)(119 downto 112) <= "0101" & "1111"; 
--
ttcn: process(clk40)
begin
    if rising_edge(clk40) then
        xTTCin_array(I)(9 downto 0) <= TTCin; 
    end if;
end process;

xTTCin_array(I)(10)    <= ExtendedTestPulse; -- the ExtendedTestPulse already set in one clock delay, like in the "ttcn" process above

-- If you want to generate a per-GBT (per front end) delay, that
--  can be done here, but only in steps of the 40MHz clock.
--
--  If used connect DlydTTCin_array to upstream instead of TTCin_array.

--IG ttcFanDly : for J in 0 to 9 generate
ttcFanDly : for J in 0 to 10 generate --IG: add another bit to support the extended Test Pulse
  TTC_DELAY_SRL : SRL16E
  port map (
      clk => clk40,
      ce => '1', --delay_en,     --input from control register
      D => xTTCin_array(I)(J), --latched copy of TTCin
      Q => TTCin_array(I)(J), --delayed copy of TTCin
      A0 => TTC_DELAY_array(I)(0),  --input from control register
      A1 => TTC_DELAY_array(I)(1),  --input from control register
      A2 => TTC_DELAY_array(I)(2),  --input from control register
      A3 => TTC_DELAY_array(I)(3)  --input from control register
      );
end generate;
--

		
	
-- 
------------------------------------------------------------
-- from-Host configuration 
------------------------------------------------------------
cfg0: entity work.crFHconfigMap (behavioral)
generic map(
    GBTid                   => I,
    wideMode                => false,
    crInternalLoopbackMode  => false,
    TTC_test_mode           => false
    )
port map( 
    clk                                => clk40,
    register_map_control               => register_map_control,
    register_map_control_appreg_clk    => register_map_control_appreg_clk,
    appreg_clk                         => appreg_clk,
    FHicFIFOwe                         => FHicFIFOwe,    -- out, comes from a trigger register  
    FHicFIFOdin                        => FHicFIFOdin,   -- out 8 bit, comes from a register
    FHicPacketRdy                      => FHicPacketRdy, -- out, comes from a register
            
    -----
    crUpstreamConfig        => gbtFHconfig_array(I),
    auxUpstreamConfig       => auxUpstreamConfig
    );
    
--
------------------------------------------------------------
-- GBT data manager: Upstream, to GBT (from-Host)
------------------------------------------------------------
GBTdmUp: entity work.GBTdmUpstream (Behavioral)
generic map(
    generate_IC_EC_TTC_only => generate_IC_EC_TTC_only,
    includeNoEncodingCase => includeNoEncodingCase,
    GENERATE_FEI4B => GENERATE_FEI4B,
    GENERATE_LCB_ENC => GENERATE_LCB_ENC,
    GBT_NUM             => GBT_NUM,
    GBTid               => I,
    EnableFrHo_Egroup0Eproc2_HDLC  => EnableFrHo_Egroup0Eproc2_HDLC,
    EnableFrHo_Egroup0Eproc2_8b10b => EnableFrHo_Egroup0Eproc2_8b10b,
    EnableFrHo_Egroup0Eproc4_8b10b => EnableFrHo_Egroup0Eproc4_8b10b,
    EnableFrHo_Egroup0Eproc8_8b10b => EnableFrHo_Egroup0Eproc8_8b10b,
    EnableFrHo_Egroup1Eproc2_HDLC  => EnableFrHo_Egroup1Eproc2_HDLC,
    EnableFrHo_Egroup1Eproc2_8b10b => EnableFrHo_Egroup1Eproc2_8b10b,
    EnableFrHo_Egroup1Eproc4_8b10b => EnableFrHo_Egroup1Eproc4_8b10b,
    EnableFrHo_Egroup1Eproc8_8b10b => EnableFrHo_Egroup1Eproc8_8b10b,
    EnableFrHo_Egroup2Eproc2_HDLC  => EnableFrHo_Egroup2Eproc2_HDLC,
    EnableFrHo_Egroup2Eproc2_8b10b => EnableFrHo_Egroup2Eproc2_8b10b,
    EnableFrHo_Egroup2Eproc4_8b10b => EnableFrHo_Egroup2Eproc4_8b10b,
    EnableFrHo_Egroup2Eproc8_8b10b => EnableFrHo_Egroup2Eproc8_8b10b,
    EnableFrHo_Egroup3Eproc2_HDLC  => EnableFrHo_Egroup3Eproc2_HDLC,
    EnableFrHo_Egroup3Eproc2_8b10b => EnableFrHo_Egroup3Eproc2_8b10b,
    EnableFrHo_Egroup3Eproc4_8b10b => EnableFrHo_Egroup3Eproc4_8b10b,
    EnableFrHo_Egroup3Eproc8_8b10b => EnableFrHo_Egroup3Eproc8_8b10b,
    EnableFrHo_Egroup4Eproc2_HDLC  => EnableFrHo_Egroup4Eproc2_HDLC,
    EnableFrHo_Egroup4Eproc2_8b10b => EnableFrHo_Egroup4Eproc2_8b10b,
    EnableFrHo_Egroup4Eproc4_8b10b => EnableFrHo_Egroup4Eproc4_8b10b,
    EnableFrHo_Egroup4Eproc8_8b10b => EnableFrHo_Egroup4Eproc8_8b10b,
    wideMode            => wideMode,
    GENERATE_XOFF       => GENERATE_XOFF,
    CARD_TYPE           => CARD_TYPE,
    DATA_WIDTH          => DATA_WIDTH
    )
port map( 
    toHostXoff => (others => '0'), --todo move to encoding and connect to decoding.
    clk40      => clk40,
    clk80      => clk80,
    clk160     => clk160,
    clk240     => upfifoDoutClk,
    rst        => cr_rst,
    -----
    register_map_control => register_map_control,
    gbtUpstreamConfig   => gbtFHconfig_array(I),
    auxUpstreamConfig   => auxUpstreamConfig,
    ----- from-host (upstream) IC e-link fifo write driver
    FHicFIFOwclk        => appreg_clk,
    FHicFIFOwe          => FHicFIFOwe,      -- in, comes from a trigger register  
    FHicFIFOfull        => open,    -- out, goes to a register      
    FHicFIFOdin         => FHicFIFOdin,     -- in 8 bit, comes from a register
    FHicPacketRdy       => FHicPacketRdy,   -- in, comes from a register  
    -----
    TTCin               => TTCin_array(I),
    fhCR_REVERSE_10B    => to_sl(register_map_control.CR_REVERSE_10B.FROMHOST),
    -- TX side
    GBTdm_DOUT_array    => to_GBTs_array(I),
    ICnEC_uplinks       => open, 
    -- wupper side
    fifoWCLK    => upfifoDoutClk,
    fifoWE      => fhFifoWE_array(I),
    fifoDin     => fhFifoDout,   
    fifoPfull   => UpFifoPfull_array(I),  
    fifoFLUSH   => cr_fifo_flush,  
    -----
    xoff		=> fhXoff_array(I),
    -----
    monitor_vec => UpFifoFull_mon_array(I)
    );
-- 
end generate crFH;
--
------------------------------------------------------------
-- from Host input fifo
------------------------------------------------------------
--fanout to all GBTs/Eprocs/Paths!!! (filtered locally)
--
UpFifoPfull_or  <= '0' when (UpFifoPfull_array = zerosGBT_NUMarray) else '1'; -- can't write to at least one GBT wm fifo -> stop reading from crINfifo
crINfifo_re     <= (not crINfifo_empty) and (not UpFifoPfull_or) and (not cr_rst);
--

fromHostFifo_rd_en <= crINfifo_re;
crINfifo_empty <= fromHostFifo_empty;
fromHostFifo_rd_clk <= upfifoDoutClk;
fromHostFifo_rst <= cr_fifo_flush;


--
--fhFifoPfull <= fhFifoPfull_s; -- goes to wupper
--
fhXoff  <= '0' when (fhXoff_array = zerosGBT_NUMarray) else '1';
--
process(upfifoDoutClk) -- 1-clock pipeline 
begin
    if rising_edge(upfifoDoutClk) then
        fhFifoDout_rdy <= crINfifo_re;
    end if;
end process; 
--
UpFifoDout_fanout:  for I in 0 to (GBT_NUM-1) generate   
UpFifoDoutN: process(upfifoDoutClk) -- 1-clock pipeline, fanout to wm fifos
begin
    if rising_edge(upfifoDoutClk) then
        fhFifoDout <= fromHostFifo_dout;
        fhFifoWE_array(I)   <= fhFifoDout_rdy;
    end if;
end process;
end generate UpFifoDout_fanout;
--


--



end Behavioral;

