library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.package_64b66b.all;

--library UNISIM;
--use UNISIM.VComponents.all;

entity RD53Interface is
    generic (
        RD53Version   : String := "A" --A or B
    );
    port (
        Reset                : in std_logic;
        clk40_in             : in std_logic;
        data_in              : in data64V_64b66b_type;
        sep_in               : in std_logic;
        rx_sep_nb            : in std_logic_vector(2 downto 0);
        data_k_in            : in data64V_64b66b_type;
        data_out             : out data32VL_64b66b_type; --std_logic_vector(33 downto 0);   --tlast (1b) + valid (1b) + data (32b)
        byteskeep_out        : out std_logic_vector(3 downto 0);
        datadcs_out          : out data32VL_64b66b_type; --std_logic_vector(33 downto 0);
        --debug
        cnt_rx_64b66bhdr_out : out std_logic_vector(31 downto 0);
        ref_packet_in        : in std_logic_vector(31 downto 0)

    );
end RD53Interface;


architecture Behavioral of RD53Interface is

    signal data_i               : std_logic_vector(63 downto 0);
    signal datav_i              : std_logic;
    signal sep_i                : std_logic;
    signal data_k_i             : std_logic_vector(63 downto 0);
    signal datav_k_i            : std_logic;
    signal dataout_i            : data32VL_64b66b_type; --std_logic_vector(33 downto 0);
    signal byteskeep_i          : std_logic_vector( 3 downto 0);
    signal datadcsout_i         : data32VL_64b66b_type; --std_logic_vector(33 downto 0);


begin

    g_RD53A: if RD53Version = "A" generate
        signal cnt_rx_64b66bhdr_i   : std_logic_vector(31 downto 0);


    begin


        --1) invert 32b order so will output header first (N.B: header always as second 32b word from RD53A
        --doc) : do I still need this with the new YARR?
        --2) avoid fake hit at col=row=0 pixels due to 1E04->0000
        data_i    <= (data_in.data(31 downto 0) & x"FFFFFFFF") when (sep_in = '1' and rx_sep_nb /= "000") else --1E04..
                     (x"FFFFFFFF" & x"FFFFFFFF")          when (sep_in = '1' and rx_sep_nb = "000") else  --1E00..
                     (data_in.data(31 downto 0) & data_in.data(63 downto 32));
        datav_i   <= data_in.valid;
        sep_i     <= sep_in;

        data_k_i  <= data_k_in.data;
        datav_k_i <= data_k_in.valid;


        --cnt #headers after 64b66b module (ie: output split64bword)
        cntRcvdPckts_64b66b_i: entity work.cntRcvdPckts_64b66b port map (
                rst                         => Reset,
                clk40                       => clk40_in,
                data_in                     => dataout_i.data(31 downto 25), --dataout_i(31 downto 25),
                datav_in                    => dataout_i.valid, --dataout_i(32),
                ref_packet_in               => ref_packet_in(31 downto 25),
                cnt_packets_out             => cnt_rx_64b66bhdr_i
            );

        cnt_rx_64b66bhdr_out <= cnt_rx_64b66bhdr_i;


    end generate; --if RD53Version = 'A' generate

    g_RD53B: if RD53Version = "B" generate
    begin
        --changed on Aug 26. Filter out 1E00, keep 1E04. 1E can arrive but they are
        --not guaranteed to be there. Will rely on NS or EoS=000000 for asserting
        --the tlast (see split64bword.vhd)
        --as opposed to RD53A 1E00 will be invalid data (1E04 will be valid as in Rd53A)
        data_i    <= (x"FFFFFFFF" & data_in.data(31 downto 0)) when (sep_in = '1' and rx_sep_nb /= "000") else --1E04..
                     (x"FFFFFFFF" & x"FFFFFFFF")          when (sep_in = '1' and rx_sep_nb = "000") else  --1E00..
                     (data_in.data(63 downto 0));
        datav_i   <= data_in.valid when (sep_in = '1' and rx_sep_nb /= "000") else
                     '0'         when (sep_in = '1' and rx_sep_nb = "000") else
                     data_in.valid;
        sep_i     <= sep_in;

        data_k_i  <= data_k_in.data;
        datav_k_i <= data_k_in.valid;

        cnt_rx_64b66bhdr_out <= (others => '0');
    end generate; --if RD53Version = 'B' generate

    split64word_64b66b_atodmessage_i : entity work.split64bword_64b66b
        generic map(
            RD53Version            => RD53Version
        )
        port map (
            rst          => Reset,
            clk40          => clk40_in,
            data_in        => data_i,
            valid_in       => datav_i,
            isSOPEOP_in    => sep_i,
            data_out       => dataout_i,
            byteskeep_out  => byteskeep_i
        );

    split64word_64b66b_emessage_i : entity work.split64bword_64b66b
        generic map(
            RD53Version            => RD53Version
        )
        port map (
            rst          => Reset,
            clk40        => clk40_in,
            data_in      => data_k_i,
            valid_in     => datav_k_i,
            isSOPEOP_in  => datav_k_i,
            data_out     => datadcsout_i,
            byteskeep_out => open
        );


    data_out    <= dataout_i;
    byteskeep_out <= byteskeep_i;
    datadcs_out <= datadcsout_i;


end Behavioral;
