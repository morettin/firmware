--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;
    use work.rd53_package.ALL;

entity RD53A_CntIssuedTrigger is
    port (
        rst              : in std_logic;
        clk            : in std_logic;
        data_in          : in std_logic_vector(15 downto 0);
        datav_in         : in std_logic;
        --    counter_in       : in std_logic_vector( 1 downto 0);
        cnt_trig_cmd_out : out std_logic_vector(31 downto 0)

    );
end RD53A_CntIssuedTrigger;

architecture Behavioral of RD53A_CntIssuedTrigger is


    -----------------------------------------------------------------
    --count # triggers issued via command. Can be reset via soft_reset
    --Procedure: sample ser_data_i at counter=01 (ie: when first 4b is beig sent
    --out). No need to use valid. If ser_data_i = Trigger_XX increase counter by
    --1, 2, 3, 4 depending on trigger type
    -----------------------------------------------------------------

    --# expected: 64 (mask stages) x 25 (core loop) x 4x4 (triggers) x 100 (injection)
    --parameters
    --json
    --64 from  step=1, min=0, max=64 in "loopAction": "Rd53aMaskLoop"
    --25 from ?"loopAction": "Rd53aCoreColLoop"?
    --100 from count "loopAction": "Rd53aTriggerLoop"
    --4x4 from trigger sequence x"566a", x"566c", x"5671", x"5672"

    signal cnt_trig_cmd: std_logic_vector(31 downto 0);

begin

    process(clk)
    begin
        if rising_edge (clk) then
            if(rst = '1') then
                cnt_trig_cmd <= (others => '0');
            elsif (datav_in = '1' and
             (data_in(15 downto 8) = Trigger_01 or
             data_in(15 downto 8) = Trigger_02 or
             data_in(15 downto 8) = Trigger_04 or
             data_in(15 downto 8) = Trigger_08)) then
                if(cnt_trig_cmd /= x"0FFFFFFF") then
                    cnt_trig_cmd <= cnt_trig_cmd + x"00000001";
                end if;
            elsif (datav_in = '1' and
             (data_in(15 downto 8) = Trigger_03 or
             data_in(15 downto 8) = Trigger_05 or
             data_in(15 downto 8) = Trigger_06 or
             data_in(15 downto 8) = Trigger_09 or
             data_in(15 downto 8) = Trigger_10 or
             data_in(15 downto 8) = Trigger_12)) then
                if(cnt_trig_cmd /= x"0FFFFFFE") then
                    cnt_trig_cmd <= cnt_trig_cmd + x"00000002";
                end if;
            elsif (datav_in = '1' and
             (data_in(15 downto 8) = Trigger_07 or
             data_in(15 downto 8) = Trigger_11 or
             data_in(15 downto 8) = Trigger_13 or
             data_in(15 downto 8) = Trigger_14)) then
                if(cnt_trig_cmd /= x"0FFFFFFD") then
                    cnt_trig_cmd <= cnt_trig_cmd + x"00000003";
                end if;
            elsif (datav_in = '1' and
             data_in(15 downto 8) = Trigger_15) then
                if(cnt_trig_cmd /= x"0FFFFFFC") then
                    cnt_trig_cmd <= cnt_trig_cmd + x"00000004";
                end if;
            end if;
        end if;
    end process;
    cnt_trig_cmd_out <= cnt_trig_cmd;

end Behavioral;
