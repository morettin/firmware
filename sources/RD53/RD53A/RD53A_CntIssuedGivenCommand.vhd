--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;

entity RD53A_CntIssuedGivenCommand is
    port (
        rst              : in std_logic;
        clk            : in std_logic;
        data_in          : in std_logic_vector(15 downto 0);
        datav_in         : in std_logic;
        ref_cmd_in      : in std_logic_vector(15 downto 0);
        cnt_cmd_out      : out std_logic_vector(31 downto 0)
    );
end RD53A_CntIssuedGivenCommand;

architecture Behavioral of RD53A_CntIssuedGivenCommand is


    -----------------------------------------------------------------
    --count # issued command matching the reference.
    -----------------------------------------------------------------



    signal cnt_cmd: std_logic_vector(31 downto 0);

begin

    process(clk)
    begin
        if rising_edge (clk) then
            if(rst = '1') then
                cnt_cmd    <= (others => '0');
            else
                if (datav_in = '1' and data_in = ref_cmd_in and cnt_cmd /= x"0FFFFFFF") then
                    cnt_cmd <= cnt_cmd + x"00000001";
                end if;
            end if; --if(rst = '1')
        end if; --clock
    end process;
    cnt_cmd_out    <= cnt_cmd;

end Behavioral;
