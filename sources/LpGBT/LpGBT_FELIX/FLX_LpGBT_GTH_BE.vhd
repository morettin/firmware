--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Kai Chen
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--! modified by: Marco Trovato (mtrovato@anl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:43:14 PM
-- Design Name: LpGBT BE GTH Top
-- Module Name: LpGBT BE GTH Top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The LpGBT Back-End GTH Top
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
    use UNISIM.VComponents.all;

entity FLX_LpGBT_GTH_BE is
    Generic (
        KCU_LOWER_LATENCY : integer := 0;
        CARD_TYPE: integer
    );


    Port (


        --cpllreset_in                           : in std_logic_vector(0 downto 0);
        --cpllfbclklost_out                      : out std_logic_vector(0 downto 0);
        --cplllock_out                           : out std_logic_vector(0 downto 0);

        gt_rxusrclk_in                         : in std_logic_vector(3 downto 0);
        gt_txusrclk_in                         : in std_logic_vector(3 downto 0);
        gt_rxoutclk_out                        : out std_logic_vector(3 downto 0);
        gt_txoutclk_out                        : out std_logic_vector(3 downto 0);
        gthrxn_in                              : in std_logic_vector(3 downto 0);
        gthrxp_in                              : in std_logic_vector(3 downto 0);
        gthtxn_out                             : out std_logic_vector(3 downto 0);
        gthtxp_out                             : out std_logic_vector(3 downto 0);
        drpclk_in                              : in std_logic_vector(0 downto 0);
        gtrefclk0_in                           : in std_logic_vector(0 downto 0);

        rxpolarity_in                          : in std_logic_vector(3 downto 0);
        txpolarity_in                          : in std_logic_vector(3 downto 0);
        loopback_in                            : in std_logic_vector(2 downto 0);
        rxcdrhold_in                           : in std_logic;
        userdata_tx_in                         : in std_logic_vector(63 downto 0);
        userdata_rx_out                        : out std_logic_vector(127 downto 0);

        userclk_rx_reset_in                    : in std_logic_vector(0 downto 0);
        userclk_tx_reset_in                    : in std_logic_vector(0 downto 0);

        -- reset_clk_freerun_in                 : in std_logic_vector(0 downto 0);
        reset_all_in                            : in std_logic_vector(0 downto 0);
        reset_tx_pll_and_datapath_in            : in std_logic_vector(0 downto 0);
        reset_tx_datapath_in                    : in std_logic_vector(0 downto 0);
        reset_rx_pll_and_datapath_in            : in std_logic_vector(0 downto 0);
        reset_rx_datapath_in                    : in std_logic_vector(3 downto 0);
        qpll1lock_out                           : out std_logic_vector(0 downto 0);
        qpll1fbclklost_out                      : out std_logic_vector(0 downto 0);
        qpll0lock_out                           : out std_logic_vector(0 downto 0);
        qpll0fbclklost_out                      : out std_logic_vector(0 downto 0);

        rxslide_in                              : in std_logic_vector(3 downto 0);

        txresetdone_out                         : out std_logic_vector(3 downto 0);
        txpmaresetdone_out                      : out std_logic_vector(3 downto 0);
        rxresetdone_out                         : out std_logic_vector(3 downto 0);
        rxpmaresetdone_out                      : out std_logic_vector(3 downto 0);

        reset_tx_done_out                       : out std_logic_vector(3 downto 0);
        reset_rx_done_out                       : out std_logic_vector(3 downto 0);
        reset_rx_cdr_stable_out                 : out std_logic_vector(3 downto 0);
        rxcdrlock_out                           : out std_logic_vector(3 downto 0);
        cplllock_out                            : out std_logic_vector(3 downto 0)

    );
end FLX_LpGBT_GTH_BE;

architecture Behavioral of FLX_LpGBT_GTH_BE is

    COMPONENT KCU_PMA_QPLL_4CH_LPGBT
        PORT (
            gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
            gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
            gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll0fbclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll0lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1fbclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            drpclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            gthrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            gthrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtrefclk0_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
            rxcdrhold_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxpolarity_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxslide_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            txpolarity_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            cplllock_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gthtxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gthtxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxcdrlock_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
        );
    END COMPONENT;

    COMPONENT KCU_RXBUF_PMA_CPLL_1CH_LPGBT
        PORT (
            gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            drpclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxn_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxp_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtrefclk0_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            loopback_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            rxcdrhold_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpolarity_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxslide_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxusrclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxusrclk2_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            txpolarity_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            txusrclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            txusrclk2_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            cplllock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthtxn_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthtxp_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtpowergood_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcdrlock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxoutclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txoutclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;

    component VU37P_PMA_QPLL_4CH_LPGBT
        port (
            gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
            gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
            gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll0fbclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll0lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1fbclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtyrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtyrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
            rxcdrhold_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxpolarity_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxslide_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            txpolarity_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            cplllock_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtytxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtytxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxcdrlock_out                      : out STD_LOGIC_VECTOR(3 downto 0);
            rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
        );
    end component;

    COMPONENT VU37P_RXBUF_PMA_QPLL_4CH_LPGBT
        PORT (
            gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
            gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
            gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll0fbclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll0lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1fbclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1lock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtyrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtyrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
            rxcdrhold_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxpolarity_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxslide_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            txpolarity_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
            cplllock_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtytxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtytxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxcdrlock_out                      : out STD_LOGIC_VECTOR(3 downto 0);
            rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            rxresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
            txresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
        );
    END COMPONENT;


    signal rxusrclk               : std_logic_vector(3 downto 0);
    signal rxusrclk_int           : std_logic_vector(3 downto 0);
    signal rxusrclk2_int          : std_logic_vector(3 downto 0);
    signal txoutclk_int           : std_logic_vector(3 downto 0);
    signal rxoutclk_int           : std_logic_vector(3 downto 0);
    signal txusrclk_int           : std_logic_vector(3 downto 0);
    signal txusrclk2_int          : std_logic_vector(3 downto 0);
    signal userclk_tx_active_out  : std_logic_vector(0 downto 0);
    signal userclk_rx_active_out  : std_logic_vector(0 downto 0);
    signal userclk_rx_active_out_p: std_logic_vector(0 downto 0);
    signal userclk_tx_active_out_p: std_logic_vector(0 downto 0);
    signal txusrclk               : std_logic;
    signal gndvec                 : std_logic_vector(0 downto 0);
    signal drpclk_i               : std_logic_vector(3 downto 0);
    signal gtrefclk0_i            : std_logic_vector(3 downto 0);
    signal loopback_i             : std_logic_vector(11 downto 0);
    signal rxcdrhold_i            : std_logic_vector(3 downto 0);
begin

    gndvec(0)     <= '0';
    -- RxUsrClk
    rxusrclk      <= gt_rxusrclk_in;
    rxusrclk_int  <= rxusrclk;
    rxusrclk2_int <= rxusrclk;

    gt_rxoutclk_out <= rxoutclk_int;
    gt_txoutclk_out <= txoutclk_int;

    txusrclk      <= gt_txusrclk_in(0);
    txusrclk2_int <= gt_txusrclk_in;
    txusrclk_int  <= gt_txusrclk_in;

    process(userclk_tx_reset_in(0), txusrclk)
    begin
        if userclk_tx_reset_in(0) = '1' then
            userclk_tx_active_out(0)          <= '0';
        elsif txusrclk'event and txusrclk = '1' then
            userclk_tx_active_out_p(0)        <= '1';
            userclk_tx_active_out             <= userclk_tx_active_out_p;
        end if;
    end process;

    process(userclk_rx_reset_in(0), rxusrclk(0))
    begin
        if userclk_rx_reset_in(0) = '1' then
            userclk_rx_active_out(0)          <= '0';
        elsif rxusrclk(0)'event and rxusrclk(0) = '1' then
            userclk_rx_active_out_p(0)        <= '1';
            userclk_rx_active_out             <= userclk_rx_active_out_p;
        end if;
    end process;


    drpclk_i <= drpclk_in(0) & drpclk_in(0) &  drpclk_in(0) & drpclk_in(0);
    gtrefclk0_i <= gtrefclk0_in(0) & gtrefclk0_in(0) & gtrefclk0_in(0) & gtrefclk0_in(0);
    loopback_i <= loopback_in & loopback_in & loopback_in & loopback_in;
    rxcdrhold_i <= (others=>rxcdrhold_in);

    RXBUF_GEN: if KCU_LOWER_LATENCY = 0 generate
        g_712: if CARD_TYPE = 712 or CARD_TYPE = 711 generate
            g_4ch: for i in 0 to 3 generate
                gtwizard_ultrascale_four_channel_qpll_inst:  KCU_RXBUF_PMA_CPLL_1CH_LPGBT
                    port map(
                        gtwiz_userclk_tx_active_in => userclk_tx_active_out,
                        gtwiz_userclk_rx_active_in => userclk_rx_active_out,
                        gtwiz_buffbypass_tx_reset_in => gndvec,
                        gtwiz_buffbypass_tx_start_user_in => gndvec,
                        gtwiz_buffbypass_tx_done_out => open,
                        gtwiz_buffbypass_tx_error_out => open,
                        gtwiz_reset_clk_freerun_in => drpclk_in,
                        gtwiz_reset_all_in => reset_all_in,
                        gtwiz_reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath_in,
                        gtwiz_reset_tx_datapath_in => reset_tx_datapath_in,
                        gtwiz_reset_rx_pll_and_datapath_in => reset_rx_pll_and_datapath_in,
                        gtwiz_reset_rx_datapath_in => reset_rx_datapath_in(i downto i),
                        gtwiz_reset_rx_cdr_stable_out => reset_rx_cdr_stable_out(i downto i),
                        gtwiz_reset_tx_done_out => reset_tx_done_out(i downto i),
                        gtwiz_reset_rx_done_out => reset_rx_done_out(i downto i),
                        gtwiz_userdata_tx_in => userdata_tx_in(i*16+15 downto i*16),
                        gtwiz_userdata_rx_out => userdata_rx_out(i*32+31 downto i*32),
                        drpclk_in => drpclk_i(i downto i),
                        gthrxn_in => gthrxn_in(i downto i),
                        gthrxp_in => gthrxp_in(i downto i),
                        gtrefclk0_in => gtrefclk0_i(i downto i),
                        loopback_in => loopback_i(3*i+2 downto 3*i),
                        rxcdrhold_in => rxcdrhold_i(i downto i),
                        rxpolarity_in => rxpolarity_in(i downto i),
                        rxslide_in => rxslide_in(i downto i),
                        rxusrclk_in => rxusrclk_int(i downto i),
                        rxusrclk2_in => rxusrclk2_int(i downto i),
                        txpolarity_in => txpolarity_in(i downto i),
                        txusrclk_in => txusrclk_int(i downto i),
                        txusrclk2_in => txusrclk2_int(i downto i),
                        cplllock_out => cplllock_out(i downto i),
                        gthtxn_out => gthtxn_out(i downto i),
                        gthtxp_out => gthtxp_out(i downto i),
                        gtpowergood_out => open,
                        rxcdrlock_out => rxcdrlock_out(i downto i),
                        rxoutclk_out => rxoutclk_int(i downto i),
                        rxpmaresetdone_out => rxpmaresetdone_out(i downto i),
                        rxresetdone_out => rxresetdone_out(i downto i),
                        txoutclk_out => txoutclk_int(i downto i),
                        txpmaresetdone_out => txpmaresetdone_out(i downto i),
                        txprgdivresetdone_out => open,
                        txresetdone_out => txresetdone_out(i downto i)
                    );
            end generate g_4ch;
        end generate g_712;
        g_128: if CARD_TYPE = 128 or CARD_TYPE = 800 generate
            signal reset_rx_datapath_s, reset_rx_cdr_stable_s, reset_tx_done_s, reset_rx_done_s: std_logic_vector(0 downto 0);
        begin
            reset_rx_datapath_s(0) <= reset_rx_datapath_in(0) or reset_rx_datapath_in(1) or reset_rx_datapath_in(2) or reset_rx_datapath_in(3);
            reset_rx_cdr_stable_out <= (others => reset_rx_cdr_stable_s(0));
            reset_tx_done_out <= (others => reset_tx_done_s(0));
            reset_rx_done_out <= (others => reset_rx_done_s(0));

            gtwizard_ultrascale_four_channel_qpll_inst:  VU37P_RXBUF_PMA_QPLL_4CH_LPGBT
                port map(
                    gtwiz_userclk_tx_active_in => userclk_tx_active_out,
                    gtwiz_userclk_rx_active_in => userclk_rx_active_out,
                    gtwiz_buffbypass_tx_reset_in => gndvec,
                    gtwiz_buffbypass_tx_start_user_in => gndvec,
                    gtwiz_buffbypass_tx_done_out => open,
                    gtwiz_buffbypass_tx_error_out => open,
                    gtwiz_reset_clk_freerun_in => drpclk_in,
                    gtwiz_reset_all_in => reset_all_in,
                    gtwiz_reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath_in,
                    gtwiz_reset_tx_datapath_in => reset_tx_datapath_in,
                    gtwiz_reset_rx_pll_and_datapath_in => reset_rx_pll_and_datapath_in,
                    gtwiz_reset_rx_datapath_in => reset_rx_datapath_s,
                    gtwiz_reset_rx_cdr_stable_out => reset_rx_cdr_stable_s,
                    gtwiz_reset_tx_done_out => reset_tx_done_s,
                    gtwiz_reset_rx_done_out => reset_rx_done_s,
                    gtwiz_userdata_tx_in => userdata_tx_in,
                    gtwiz_userdata_rx_out => userdata_rx_out,
                    gtrefclk01_in => gtrefclk0_in,
                    qpll0fbclklost_out => qpll0fbclklost_out,
                    qpll0lock_out => qpll0lock_out,
                    qpll1fbclklost_out => qpll1fbclklost_out,
                    qpll1lock_out => qpll1lock_out,
                    qpll1outclk_out => open,
                    qpll1outrefclk_out => open,
                    gtyrxn_in => gthrxn_in,
                    gtyrxp_in => gthrxp_in,
                    loopback_in => loopback_i,
                    rxcdrhold_in => rxcdrhold_i,
                    rxpolarity_in => rxpolarity_in,
                    rxslide_in => rxslide_in,
                    rxusrclk_in => rxusrclk_int,
                    rxusrclk2_in => rxusrclk2_int,
                    txpolarity_in => txpolarity_in,
                    txusrclk_in => txusrclk_int,
                    txusrclk2_in => txusrclk2_int,
                    cplllock_out => cplllock_out,
                    gtytxn_out => gthtxn_out,
                    gtytxp_out => gthtxp_out,
                    gtpowergood_out => open,
                    rxcdrlock_out => rxcdrlock_out,
                    rxoutclk_out => rxoutclk_int,
                    rxpmaresetdone_out => rxpmaresetdone_out,
                    rxresetdone_out => rxresetdone_out,
                    txoutclk_out => txoutclk_int,
                    txpmaresetdone_out => txpmaresetdone_out,
                    txprgdivresetdone_out => open,
                    txresetdone_out => txresetdone_out
                );
        end generate g_128;
    end generate RXBUF_GEN;

    RXNOBUF_GEN: if KCU_LOWER_LATENCY = 1 generate
        g_712: if CARD_TYPE = 712 or CARD_TYPE = 711 generate
            signal reset_rx_datapath_s, reset_rx_cdr_stable_s, reset_tx_done_s, reset_rx_done_s: std_logic_vector(0 downto 0);
        begin
            reset_rx_datapath_s(0) <= reset_rx_datapath_in(0) or reset_rx_datapath_in(1) or reset_rx_datapath_in(2) or reset_rx_datapath_in(3);
            reset_rx_cdr_stable_out <= (others => reset_rx_cdr_stable_s(0));
            reset_tx_done_out <= (others => reset_tx_done_s(0));
            reset_rx_done_out <= (others => reset_rx_done_s(0));

            gtwizard_ultrascale_four_channel_qpll_inst:  KCU_PMA_QPLL_4CH_LPGBT
                port map(
                    gtwiz_userclk_tx_active_in => userclk_tx_active_out,
                    gtwiz_userclk_rx_active_in => userclk_rx_active_out,
                    gtwiz_buffbypass_tx_reset_in => gndvec,
                    gtwiz_buffbypass_tx_start_user_in => gndvec,
                    gtwiz_buffbypass_tx_done_out => open,
                    gtwiz_buffbypass_tx_error_out => open,

                    gtwiz_buffbypass_rx_reset_in => gndvec,
                    gtwiz_buffbypass_rx_start_user_in => gndvec,
                    gtwiz_buffbypass_rx_done_out => open,
                    gtwiz_buffbypass_rx_error_out => open,

                    gtwiz_reset_clk_freerun_in => drpclk_in,
                    gtwiz_reset_all_in => reset_all_in,
                    gtwiz_reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath_in,
                    gtwiz_reset_tx_datapath_in => reset_tx_datapath_in,
                    gtwiz_reset_rx_pll_and_datapath_in => reset_rx_pll_and_datapath_in,
                    gtwiz_reset_rx_datapath_in => reset_rx_datapath_s,
                    gtwiz_reset_rx_cdr_stable_out => reset_rx_cdr_stable_s,
                    gtwiz_reset_tx_done_out => reset_tx_done_s,
                    gtwiz_reset_rx_done_out => reset_rx_done_s,
                    gtwiz_userdata_tx_in => userdata_tx_in,
                    gtwiz_userdata_rx_out => userdata_rx_out,
                    gtrefclk01_in => gtrefclk0_in,
                    qpll0fbclklost_out => qpll0fbclklost_out,
                    qpll0lock_out => qpll0lock_out,
                    qpll1fbclklost_out => qpll1fbclklost_out,
                    qpll1lock_out => qpll1lock_out,
                    qpll1outclk_out => open,
                    qpll1outrefclk_out => open,
                    drpclk_in => drpclk_i,
                    gthrxn_in => gthrxn_in,
                    gthrxp_in => gthrxp_in,
                    gtrefclk0_in => gtrefclk0_i,
                    loopback_in => loopback_i,
                    rxcdrhold_in => rxcdrhold_i,
                    rxpolarity_in => rxpolarity_in,
                    rxslide_in => rxslide_in,
                    rxusrclk_in => rxusrclk_int,
                    rxusrclk2_in => rxusrclk2_int,
                    txpolarity_in => txpolarity_in,
                    txusrclk_in => txusrclk_int,
                    txusrclk2_in => txusrclk2_int,
                    cplllock_out => cplllock_out,
                    gthtxn_out => gthtxn_out,
                    gthtxp_out => gthtxp_out,
                    gtpowergood_out => open,
                    rxcdrlock_out => rxcdrlock_out,
                    rxoutclk_out => rxoutclk_int,
                    rxpmaresetdone_out => rxpmaresetdone_out,
                    rxresetdone_out => rxresetdone_out,
                    txoutclk_out => txoutclk_int,
                    txpmaresetdone_out => txpmaresetdone_out,
                    txresetdone_out => txresetdone_out
                );
        end generate g_712;
        g_128: if CARD_TYPE = 128 or CARD_TYPE = 800 generate
            signal reset_rx_datapath_s, reset_rx_cdr_stable_s, reset_tx_done_s, reset_rx_done_s: std_logic_vector(0 downto 0);
        begin
            reset_rx_datapath_s(0) <= reset_rx_datapath_in(0) or reset_rx_datapath_in(1) or reset_rx_datapath_in(2) or reset_rx_datapath_in(3);
            reset_rx_cdr_stable_out <= (others => reset_rx_cdr_stable_s(0));
            reset_tx_done_out <= (others => reset_tx_done_s(0));
            reset_rx_done_out <= (others => reset_rx_done_s(0));

            gtwizard_ultrascale_four_channel_qpll_inst:  VU37P_PMA_QPLL_4CH_LPGBT
                port map(
                    gtwiz_userclk_tx_active_in => userclk_tx_active_out,
                    gtwiz_userclk_rx_active_in => userclk_rx_active_out,
                    gtwiz_buffbypass_tx_reset_in => gndvec,
                    gtwiz_buffbypass_tx_start_user_in => gndvec,
                    gtwiz_buffbypass_tx_done_out => open,
                    gtwiz_buffbypass_tx_error_out => open,

                    gtwiz_buffbypass_rx_reset_in => gndvec,
                    gtwiz_buffbypass_rx_start_user_in => gndvec,
                    gtwiz_buffbypass_rx_done_out => open,
                    gtwiz_buffbypass_rx_error_out => open,

                    gtwiz_reset_clk_freerun_in => drpclk_in,
                    gtwiz_reset_all_in => reset_all_in,
                    gtwiz_reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath_in,
                    gtwiz_reset_tx_datapath_in => reset_tx_datapath_in,
                    gtwiz_reset_rx_pll_and_datapath_in => reset_rx_pll_and_datapath_in,
                    gtwiz_reset_rx_datapath_in => reset_rx_datapath_s,
                    gtwiz_reset_rx_cdr_stable_out => reset_rx_cdr_stable_s,
                    gtwiz_reset_tx_done_out => reset_tx_done_s,
                    gtwiz_reset_rx_done_out => reset_rx_done_s,
                    gtwiz_userdata_tx_in => userdata_tx_in,
                    gtwiz_userdata_rx_out => userdata_rx_out,
                    gtrefclk01_in => gtrefclk0_in,
                    qpll0fbclklost_out => qpll0fbclklost_out,
                    qpll0lock_out => qpll0lock_out,
                    qpll1fbclklost_out => qpll1fbclklost_out,
                    qpll1lock_out => qpll1lock_out,
                    qpll1outclk_out => open,
                    qpll1outrefclk_out => open,
                    gtyrxn_in => gthrxn_in,
                    gtyrxp_in => gthrxp_in,
                    loopback_in => loopback_i,
                    rxcdrhold_in => rxcdrhold_i,
                    rxpolarity_in => rxpolarity_in,
                    rxslide_in => rxslide_in,
                    rxusrclk_in => rxusrclk_int,
                    rxusrclk2_in => rxusrclk2_int,
                    txpolarity_in => txpolarity_in,
                    txusrclk_in => txusrclk_int,
                    txusrclk2_in => txusrclk2_int,
                    cplllock_out => cplllock_out,
                    gtytxn_out => gthtxn_out,
                    gtytxp_out => gthtxp_out,
                    gtpowergood_out => open,
                    rxcdrlock_out => rxcdrlock_out,
                    rxoutclk_out => rxoutclk_int,
                    rxpmaresetdone_out => rxpmaresetdone_out,
                    rxresetdone_out => rxresetdone_out,
                    txoutclk_out => txoutclk_int,
                    txpmaresetdone_out => txpmaresetdone_out,
                    txprgdivresetdone_out => open,
                    txresetdone_out => txresetdone_out
                );
        end generate g_128;
    end generate RXNOBUF_GEN;

end Behavioral;
