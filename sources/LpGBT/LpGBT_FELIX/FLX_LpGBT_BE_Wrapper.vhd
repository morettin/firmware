--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               dmatakia
--!               Kai Chen
--!               Elena Zhivun
--!               mtrovato
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen      (kchen@bnl.gov)
--! modified by: Marco Trovato (mtrovato@anl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:23:03 PM
-- Design Name: FLX_LpGBT_BE_Wrapper
-- Module Name: FLX_LpGBT_BE_Wrapper - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The FELIX Back End Wrapper
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;

Library xpm;
    use xpm.vcomponents.all;

entity FLX_LpGBT_BE_Wrapper is
    Generic (
        CARD_TYPE                   : integer := 712;
        GBT_NUM                     : integer   := 24;
        KCU_LOWER_LATENCY : integer := 0;
        GTREFCLKS : integer
    );
    Port (

        FELIX_DOWNLINK_USER_DATA    : in txrx32b_type(0 to GBT_NUM-1);
        FELIX_DOWNLINK_EC_DATA      : in txrx2b_type(0 to GBT_NUM-1);
        FELIX_DOWNLINK_IC_DATA      : in txrx2b_type(0 to GBT_NUM-1);

        FELIX_UPLINK_USER_DATA      : out txrx224b_type(0 to GBT_NUM-1);
        FELIX_UPLINK_EC_DATA        : out txrx2b_type(0 to GBT_NUM-1);
        FELIX_UPLINK_IC_DATA        : out txrx2b_type(0 to GBT_NUM-1);

        clk40_in                    : in std_logic;
        clk100_in                   : in std_logic;
        rst_hw                      : in std_logic;
        --        FELIX_SIDE_RX40MCLK         : out std_logic;

        RX_P                        : in std_logic_vector(GBT_NUM-1 downto 0);
        RX_N                        : in std_logic_vector(GBT_NUM-1 downto 0);
        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);

        GTHREFCLK                   : in std_logic_vector(GBT_NUM-1 downto 0);
        GTREFCLK_p                 : in std_logic_vector(GTREFCLKS-1 downto 0); --Only used for Versal, the clock buffers reside in the block design.
        GTREFCLK_n                 : in std_logic_vector(GTREFCLKS-1 downto 0); --Only used for Versal, the clock buffers reside in the block design.

        CTRL_SOFT_RESET             : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TXPLL_DATAPATH_RESET   : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_RXPLL_DATAPATH_RESET   : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TX_DATAPATH_RESET      : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_RX_DATAPATH_RESET      : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TXPOLARITY             : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_RXPOLARITY             : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBTTXRST               : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBTRXRST               : in std_logic_vector(GBT_NUM-1 downto 0);
        SOFT_TXRST_ALL              : in std_logic_vector(11 downto 0);--vc709
        SOFT_RXRST_ALL              : in std_logic_vector(11 downto 0);
        QPLL_RESET                  : in std_logic_vector(GBT_NUM/4-1 downto 0); --VC709
        --        CTRL_DATARATE               : in std_logic_vector(GBT_NUM-1 downto
        --        0); --MT now hard coded to 1024Gbps
        CTRL_FECMODE                : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_CHANNEL_DISABLE        : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBT_General_ctrl       : in std_logic_vector(63 downto 0);
        CTRL_MULTICYCLE_DELAY       : in std_logic_vector(2 downto 0);



        MON_RXRSTDONE               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXRSTDONE               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXRSTDONE_QUAD          : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_TXRSTDONE_QUAD          : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_RXPMARSTDONE            : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXPMARSTDONE            : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXFSMRESETDONE          : out std_logic_vector(GBT_NUM-1 downto 0);
        --VC709 only
        MON_RXFSMRESETDONE          : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXCDR_LCK               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXCDR_LCK_QUAD          : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_QPLL_LCK                : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_CPLL_LCK                : out std_logic_vector(GBT_NUM-1 downto 0);

        MON_ALIGNMENT_DONE          : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_FEC_ERROR               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_FEC_ERR_CNT             : out array_32b(0 to GBT_NUM-1);
        MON_AUTO_RX_RESET_CNT       : out array_32b(0 to GBT_NUM-1);
        CTRL_AUTO_RX_RESET_CNT_CLEAR: in  std_logic_vector(GBT_NUM-1 downto 0);
        RXUSRCLK_OUT                : out std_logic_vector(GBT_NUM-1 downto 0)

    );
end FLX_LpGBT_BE_Wrapper;


architecture Behavioral of FLX_LpGBT_BE_Wrapper is

    signal pulse_lg             : std_logic;
    signal alignment_done_f     : std_logic_vector(GBT_NUM-1 downto 0);
    signal alignment_done_f_clk40     : std_logic_vector(GBT_NUM-1 downto 0);
    signal alignment_done_f_latched     : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_RESET_i           : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_RESET_i           : std_logic_vector(GBT_NUM-1 downto 0);
    signal TxResetDone          : std_logic_vector(GBT_NUM-1 downto 0);
    signal TxResetDone_clk40          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TX_WORD_CLK       : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_RX_WORD_CLK       : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxresetdone          : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxresetdone_clk40          : std_logic_vector(GBT_NUM-1 downto 0);
    signal data_rdy             : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxSlide              : std_logic_vector(GBT_NUM-1 downto 0);
    --signal UplinkRdy            : std_logic_vector(GBT_NUM-1 downto 0);
    signal fifo_rst             : std_logic_vector(GBT_NUM-1 downto 0);
    signal fifo_empty           : std_logic_vector(GBT_NUM-1 downto 0);
    signal fifo_rden            : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TXOUTCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_RXOUTCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TXUSRCLK          : std_logic_vector(GBT_NUM-1 downto 0); --Per quad for Virtex7, Per channel for KU.
    signal GT_RXUSRCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_N_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_P_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_N_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_P_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxpmaresetdone       : std_logic_vector(GBT_NUM-1 downto 0);
    signal txpmaresetdone       : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxCdrLock            : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxCdrLock_int        : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxCdrLock_a          : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxcdrlock_out        : std_logic_vector(GBT_NUM-1 downto 0);
    signal auto_gth_rxrst       : std_logic_vector(GBT_NUM-1 downto 0);
    signal drpclk_vec           : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal rxresetdone_quad     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal rxresetdone_quad_clk40     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal txresetdone_quad     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal txresetdone_quad_clk40     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal RxCdrLock_quad       : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal userclk_rx_reset_in  : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal userclk_tx_reset_in  : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal RX_DATAPATH_RESET_FINL   : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal RX_DATAPATH_RESET_CHANNEL : std_logic_vector(GBT_NUM-1 downto 0);
    signal GBT_General_ctrl     : std_logic_vector(63 downto 0);
    signal downlinkUserData_i   : txrx32b_type(GBT_NUM-1 downto 0);
    signal downlinkEcData_i     : txrx2b_type(GBT_NUM-1 downto 0);
    signal downlinkIcData_i     : txrx2b_type(GBT_NUM-1 downto 0);
    type data16barray     is array (0 to GBT_NUM-1) of std_logic_vector(15 downto 0);
    type data32barray     is array (0 to GBT_NUM-1) of std_logic_vector(31 downto 0);
    signal TX_DATA_16b    : data16barray := (others => ("0000000000000000"));
    signal RX_DATA_32b    : data32barray := (others => ("00000000000000000000000000000000"));
    type txrx234b_48ch_type        is array (GBT_NUM-1 downto 0) of std_logic_vector(233 downto 0);
    signal uplinkData_i :txrx234b_48ch_type;

    type txrx228b_48ch_type        is array (GBT_NUM-1  downto 0) of std_logic_vector(227 downto 0);
    signal uplinkData_o :txrx228b_48ch_type;
    type txrx128b_12ch_type        is array (GBT_NUM/4-1  downto 0) of std_logic_vector(127 downto 0);
    signal RX_DATA_128b            : txrx128b_12ch_type;
    type txrx64b_12ch_type        is array (GBT_NUM/4-1  downto 0) of std_logic_vector(63 downto 0);
    signal TX_DATA_64b            : txrx64b_12ch_type;

    signal cdr_cnt        : std_logic_vector(19 downto 0);
    signal pulse_cnt      : std_logic_vector(29 downto 0);

    signal MON_CPLL_LCK_reg : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_QPLL_LCK_reg : std_logic_vector(GBT_NUM/4-1 downto 0);

    signal fec_error_i : std_logic_vector(GBT_NUM-1 downto 0);
    signal fec_err_cnt_i : array_32b(0 to GBT_NUM-1);


    --VC709 only
    signal txfsmresetdone : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxfsmresetdone : std_logic_vector(GBT_NUM-1 downto 0);
    --signal rxfsmresetdone_clk40 : std_logic_vector(GBT_NUM-1 downto 0);
    --signal txfsmresetdone_clk40 : std_logic_vector(GBT_NUM-1 downto 0);
    signal gttx_reset_i  : std_logic_vector(GBT_NUM-1 downto 0);
    signal gtrx_reset_i  : std_logic_vector(GBT_NUM-1 downto 0);

    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(2 downto 0);
--attribute MARK_DEBUG : string;
--attribute MARK_DEBUG of fec_error_i, MON_FEC_ERROR: signal is "true";

begin

    tied_to_ground_i                             <= '0';
    tied_to_ground_vec_i                         <= "000";


    MON_ALIGNMENT_DONE <= alignment_done_f_clk40 when GBT_General_ctrl(42)='0' else alignment_done_f_latched;
    GBT_General_ctrl <= CTRL_GBT_General_ctrl;
    MON_CPLL_LCK <= MON_CPLL_LCK_reg;
    MON_QPLL_LCK <= MON_QPLL_LCK_reg;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            pulse_lg <= pulse_cnt(20);
            if pulse_cnt(20)='1' then
                pulse_cnt <=(others=>'0');
            else
                pulse_cnt <= pulse_cnt+'1';
            end if;
        end if;
    end process;

    lpgbtModeAutoRxReset_cnt_g: for i in 0 to GBT_NUM-1 generate
        signal RXRESET_AUTO_CNT: std_logic_vector(31 downto 0);
        signal RXRESET_AUTO_p1: std_logic;
    begin
        cnt_proc: process(clk40_in)
        begin
            if rising_edge(clk40_in) then
                RXRESET_AUTO_p1 <= auto_gth_rxrst(i);
                if rst_hw = '1' or CTRL_AUTO_RX_RESET_CNT_CLEAR(i) = '1' then
                    RXRESET_AUTO_CNT <= (others => '0');
                elsif auto_gth_rxrst(i) = '1' and RXRESET_AUTO_p1 = '0' then
                    RXRESET_AUTO_CNT <= RXRESET_AUTO_CNT + 1;
                end if;
            end if;
        end process;

        MON_AUTO_RX_RESET_CNT(i) <= RXRESET_AUTO_CNT;
    end generate;

    rxreset: for i in 0 to GBT_NUM-1 generate

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if GBT_General_ctrl(44)='1' then
                    alignment_done_f_latched(i) <='1';
                else
                    alignment_done_f_latched(i) <= alignment_done_f_latched(i) and alignment_done_f_clk40(i);
                end if;
                if pulse_lg = '1' then
                    --handles both KCU and VC709
                    --           if alignment_done_f_clk40(i)='0' and (rxresetdone_quad_clk40(i/4)='1' or rxfsmresetdone_clk40(i)='1') and rxresetdone_clk40(i)='1' then
                    if alignment_done_f_clk40(i)='0' and (rxresetdone_quad_clk40(i/4)='1' or rxfsmresetdone(i)='1') and rxresetdone_clk40(i)='1' then
                        auto_gth_rxrst(i) <='1';
                    else
                        auto_gth_rxrst(i) <='0';
                    end if;
                else
                    auto_gth_rxrst(i) <='0';
                end if;
            end if;
        end process;
    end generate;


    rst_loop: for i in 0 to GBT_NUM-1 generate
        rst_712: if CARD_TYPE = 712 generate
            RX_RESET_i(i)       <= CTRL_GBTRXRST(i);
            TX_RESET_i(i)       <= CTRL_GBTTXRST(i) or (not TxResetDone(i));
        end generate; --rst_712
        rst_709: if CARD_TYPE = 709 generate
            RX_RESET_i(i)       <= CTRL_GBTRXRST(i) and (not rxfsmresetdone(i));
            TX_RESET_i(i)       <= CTRL_GBTTXRST(i) or (not TxResetDone(i)) or (not txfsmresetdone(i));
        end generate; --rst_709
    end generate; --rst_loop


    gbtRxTx : for i in 0 to GBT_NUM-1 generate
        signal fifo_inst_din : std_logic_vector(227 downto 0);
        signal RX_RESET_rxusrclk: std_logic;
        signal TX_RESET_txusrclk: std_logic;
        signal fifo_rst_sync: std_logic;
        signal uplinkMulticycleDelay : std_logic_vector(2 downto 0);
    begin
        downlinkUserData_i(i)   <= FELIX_DOWNLINK_USER_DATA(i);
        downlinkEcData_i(i)     <= FELIX_DOWNLINK_EC_DATA(i);
        downlinkIcData_i(i)     <= FELIX_DOWNLINK_IC_DATA(i);

        RX_RESET_sync_inst : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => RX_RESET_i(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_rst => RX_RESET_rxusrclk
            );

        TX_RESET_sync_inst : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => TX_RESET_i(i),
                dest_clk => GT_TX_WORD_CLK(i),
                dest_rst => TX_RESET_txusrclk
            );

        fifo_rst_sync_inst : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => fifo_rst(i),
                dest_clk => GT_RX_WORD_CLK(i),
                dest_rst => fifo_rst_sync
            );

        xpm_cdc_MULTICYCLE_DELAY : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 4, -- DECIMAL; range: 2-10
                INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
                SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
                SRC_INPUT_REG => 1, -- DECIMAL; 0=do not register input, 1=register input
                WIDTH => 3
            )
            port map (
                src_clk => clk40_in, -- 1-bit input: optional; required when SRC_INPUT_REG = 1
                src_in => CTRL_MULTICYCLE_DELAY,-- 1-bit input: Input signal to be synchronized to dest_clk domain.
                -- is registered.
                dest_clk =>  GT_RX_WORD_CLK(i), -- 1-bit input: Clock signal for the destination clock domain.
                dest_out => uplinkMulticycleDelay  -- 1-bit output: src_in synchronized to the destination clock domain. This output
            );

        lpgbt_inst: entity work.FLX_LpGBT_BE
            Port map (
                downlinkUserData_i => downlinkUserData_i(i),
                downlinkEcData_i => downlinkEcData_i(i),
                downlinkIcData_i => downlinkIcData_i(i),
                TXCLK40 => clk40_in,
                TXCLK320 => GT_TX_WORD_CLK(i),
                RXCLK320m => GT_RX_WORD_CLK(i),
                uplinkSelectFEC_i => CTRL_FECMODE(i),
                data_rdy => data_rdy(i),
                Tx_scrambler_bypass => '0',
                Tx_Interleaver_bypass => '0',
                Tx_FEC_bypass => '0',
                TxData_Out => TX_DATA_16b(i),
                rxdatain => RX_DATA_32b(i),
                GBT_TX_RST => TX_RESET_txusrclk,
                GBT_RX_RST => RX_RESET_rxusrclk,
                uplinkBypassInterleaver_i => GBT_General_ctrl(35),
                uplinkBypassFECEncoder_i => GBT_General_ctrl(36),
                uplinkBypassScrambler_i => GBT_General_ctrl(37),
                uplinkMulticycleDelay_i => uplinkMulticycleDelay,
                sta_headerFecLocked_o => alignment_done_f(i),
                ctr_clkSlip_o => RxSlide(i),
                --sta_rxGbRdy_o                           => open,
                uplinkReady_o => open, --UplinkRdy(i),
                uplinkUserData_o => uplinkData_i(i)(229 downto 0),
                uplinkEcData_o => uplinkData_i(i)(231 downto 230),
                uplinkIcData_o => uplinkData_i(i)(233 downto 232),
                fec_error_o    => fec_error_i(i),
                fec_err_cnt_o  => fec_err_cnt_i(i)
            );
        FELIX_UPLINK_USER_DATA(i)(223 downto 0) <= uplinkData_o(i)(223 downto 0);


        fifo_rst(i) <= (not alignment_done_f(i)) or rst_hw or GBT_General_ctrl(4) ;
        fifo_rden(i) <= not fifo_empty(i);


        FELIX_UPLINK_EC_DATA(i) <= uplinkData_o(i)(225 downto 224);
        FELIX_UPLINK_IC_DATA(i) <= uplinkData_o(i)(227 downto 226);

        fifo_inst_din <= uplinkData_i(i)(233 downto 232) & uplinkData_i(i)(231 downto 230) & uplinkData_i(i)(223 downto 0);

        fifo_inst : xpm_fifo_async
            generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                CDC_SYNC_STAGES => 2,
                DOUT_RESET_VALUE => "0",
                ECC_MODE => "no_ecc",
                FIFO_MEMORY_TYPE => "auto",
                FIFO_READ_LATENCY => 1,
                FIFO_WRITE_DEPTH => 64,
                FULL_RESET_VALUE => 1,
                PROG_EMPTY_THRESH => 10,
                PROG_FULL_THRESH => 40,
                RD_DATA_COUNT_WIDTH => 1,
                READ_DATA_WIDTH => 228,
                READ_MODE => "std",
                RELATED_CLOCKS => 0,
                USE_ADV_FEATURES => "0000",
                WAKEUP_TIME => 0,
                WRITE_DATA_WIDTH => 228,
                WR_DATA_COUNT_WIDTH => 1
            )
            port map (
                sleep => '0',
                rst => fifo_rst_sync,
                wr_clk => GT_RX_WORD_CLK(i),
                wr_en => data_rdy(i),
                din => fifo_inst_din,
                full => open,
                prog_full => open,
                wr_data_count => open,
                overflow => open,
                wr_rst_busy => open,
                almost_full => open,
                wr_ack => open,
                rd_clk => clk40_in,
                rd_en => fifo_rden(i),
                dout => uplinkData_o(i),
                empty => fifo_empty(i),
                prog_empty => open,
                rd_data_count => open,
                underflow => open,
                rd_rst_busy => open,
                almost_empty => open,
                data_valid => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr => open,
                dbiterr => open
            );

    end generate;


    clk_709_clk: if CARD_TYPE = 709 generate
        clk_generate_txoutclk: for i in 0 to GBT_NUM/4-1 generate

            GTTXOUTCLK_BUFG: BUFG
                port map(
                    o => GT_TXUSRCLK(i),
                    i => GT_TXOUTCLK(4*i)
                );
            GT_TX_WORD_CLK(i)   <= GT_TXUSRCLK(i);
            GT_TX_WORD_CLK(i+1) <= GT_TXUSRCLK(i);
            GT_TX_WORD_CLK(i+2) <= GT_TXUSRCLK(i);
            GT_TX_WORD_CLK(i+3) <= GT_TXUSRCLK(i);

        end generate; --clk_generate_txoutclk

        clk_generate_rxoutclk : for i in 0 to GBT_NUM-1 generate

            GTRXOUTCLK_BUFG: BUFG
                port map(
                    o => GT_RXUSRCLK(i),
                    i => GT_RXOUTCLK(i)
                );
            GT_RX_WORD_CLK(i) <= GT_RXUSRCLK(i);

        end generate; --clk_generate_rxoutclk
    end generate; --clk_709_clk

    clk_712: if CARD_TYPE = 712 generate
        clk_generate : for i in 0 to GBT_NUM-1 generate

            GTTXOUTCLK_BUFG: bufg_gt -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
                port map(
                    o => GT_TXUSRCLK(i),
                    ce => '1',
                    cemask => '0',
                    clr => '0',
                    clrmask => '0',
                    div => "000",
                    i => GT_TXOUTCLK(i)
                );

            GT_TX_WORD_CLK(i) <= GT_TXUSRCLK(i);

            GTRXOUTCLK_BUFG: bufg_gt -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
                port map(
                    o => GT_RXUSRCLK(i),
                    ce => '1',
                    cemask => '0',
                    clr => '0',
                    clrmask => '0',
                    div => "000",
                    i => GT_RXOUTCLK(i)
                );

            GT_RX_WORD_CLK(i) <= GT_RXUSRCLK(i);
        end generate; --clk_generate
    end generate; --clk_712

    RXUSRCLK_OUT   <= GT_RXUSRCLK;

    port_trans : for i in GBT_NUM-1 downto 0 generate
        RX_N_i(i)   <= RX_N(i);
        RX_P_i(i)   <= RX_P(i);
        TX_N(i)     <= TX_N_i(i);
        TX_P(i)     <= TX_P_i(i);
    end generate;

    x_712: if CARD_TYPE = 712 generate
        quad_cdc: for i in 0 to GBT_NUM/4-1 generate
            -- Ease the timing constraints on auto_gth_rxrst path
            -- by adding CDC for input signals from GT_RXUSRCLK to clk40 MHz

            xpm_cdc_rxresetdone_quad : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 4, -- DECIMAL; range: 2-10
                    INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
                    SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
                    SRC_INPUT_REG => 1 -- DECIMAL; 0=do not register input, 1=register input
                )
                port map (
                    src_clk => GT_RX_WORD_CLK(4*i), -- 1-bit input: optional; required when SRC_INPUT_REG = 1
                    src_in => rxresetdone_quad(i),-- 1-bit input: Input signal to be synchronized to dest_clk domain.
                    -- is registered.
                    dest_clk => clk40_in, -- 1-bit input: Clock signal for the destination clock domain.
                    dest_out => rxresetdone_quad_clk40(i)  -- 1-bit output: src_in synchronized to the destination clock domain. This output
                );

            xpm_cdc_txresetdone_quad : xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 4, -- DECIMAL; range: 2-10
                    INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
                    SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
                    SRC_INPUT_REG => 1 -- DECIMAL; 0=do not register input, 1=register input
                )
                port map (
                    src_clk => GT_TX_WORD_CLK(4*i), -- 1-bit input: optional; required when SRC_INPUT_REG = 1
                    src_in => txresetdone_quad(i),-- 1-bit input: Input signal to be synchronized to dest_clk domain.
                    -- is registered.
                    dest_clk => clk40_in, -- 1-bit input: Clock signal for the destination clock domain.
                    dest_out => txresetdone_quad_clk40(i)  -- 1-bit output: src_in synchronized to the destination clock domain. This output
                );
        end generate; --quad_cdc
    end generate; --  x_712


    channel_cdc: for i in 0 to GBT_NUM-1 generate
        -- Ease the timing constraints on auto_gth_rxrst path
        -- by adding CDC for input signals from GT_RXUSRCLK to clk40 MHz
        xpm_cdc_rxresetdone : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
                INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
                SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
                SRC_INPUT_REG => 0 -- DECIMAL; 0=do not register input, 1=register input
            )
            port map (
                src_clk => '0', -- 1-bit input: optional; required when SRC_INPUT_REG = 1
                src_in => rxresetdone(i),-- 1-bit input: Input signal to be synchronized to dest_clk domain.
                -- is registered.
                dest_clk => clk40_in, -- 1-bit input: Clock signal for the destination clock domain.
                dest_out => rxresetdone_clk40(i)  -- 1-bit output: src_in synchronized to the destination clock domain. This output
            );

        xpm_cdc_txresetdone : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
                INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
                SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
                SRC_INPUT_REG => 0 -- DECIMAL; 0=do not register input, 1=register input
            )
            port map (
                src_clk => '0', -- 1-bit input: optional; required when SRC_INPUT_REG = 1
                src_in => TxResetDone(i),-- 1-bit input: Input signal to be synchronized to dest_clk domain.
                -- is registered.
                dest_clk => clk40_in, -- 1-bit input: Clock signal for the destination clock domain.
                dest_out => TxResetDone_clk40(i)  -- 1-bit output: src_in synchronized to the destination clock domain. This output
            );

        xpm_cdc_alignment_done_f : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4, -- DECIMAL; range: 2-10
                INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
                SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
                SRC_INPUT_REG => 1 -- DECIMAL; 0=do not register input, 1=register input
            )
            port map (
                src_clk => GT_RX_WORD_CLK(i), -- 1-bit input: optional; required when SRC_INPUT_REG = 1
                src_in => alignment_done_f(i),-- 1-bit input: Input signal to be synchronized to dest_clk domain.
                -- is registered.
                dest_clk => clk40_in, -- 1-bit input: Clock signal for the destination clock domain.
                dest_out => alignment_done_f_clk40(i)  -- 1-bit output: src_in synchronized to the destination clock domain. This output
            );

        xpm_cdc_fec_error : xpm_cdc_single
            generic map (
                DEST_SYNC_FF => 4, -- DECIMAL; range: 2-10
                INIT_SYNC_FF => 0, -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
                SIM_ASSERT_CHK => 0, -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
                SRC_INPUT_REG => 1 -- DECIMAL; 0=do not register input, 1=register input
            )
            port map (
                src_clk => GT_RX_WORD_CLK(i),
                src_in => fec_error_i(i),
                dest_clk => clk40_in,
                dest_out => MON_FEC_ERROR(i)
            );


        --xpm_cdc_fec_err_cnt : xpm_cdc_gray
        --    generic map (
        --        DEST_SYNC_FF => 2,          -- DECIMAL; range: 2-10
        --        INIT_SYNC_FF => 0,          -- DECIMAL; 0=disable simulation init values, 1=enable simulation init values
        --        REG_OUTPUT => 1,            -- DECIMAL; 0=disable registered output, 1=enable registered output
        --        SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
        --        SIM_LOSSLESS_GRAY_CHK => 0, -- DECIMAL; 0=disable lossless check, 1=enable lossless check
        --        WIDTH => 32                 -- DECIMAL; range: 2-32
        --    )
        --    port map (
        --        src_clk => GT_RX_WORD_CLK(i),
        --        src_in_bin => fec_err_cnt_i(i),
        --        dest_clk => clk40_in,
        --        dest_out_bin => MON_FEC_ERR_CNT(i)
        --    );
        MON_FEC_ERR_CNT(i) <= fec_err_cnt_i(i); --FEC Error count is now registered at 40 MHz, so no synchronization required anymore

    end generate; --channel_cdc

    GTH_inst : for i in 0 to (GBT_NUM-1)/4 generate
        signal CTRL_RXPOLARITY_rxusrclk: std_logic_vector(3 downto 0);
        signal CTRL_TXPOLARITY_txusrclk: std_logic_vector(3 downto 0);
    begin
        drpclk_vec(i)       <= clk40_in;
        RX_DATA_32b(4*i+0)  <= RX_DATA_128b(i)(31 downto 0);
        RX_DATA_32b(4*i+1)  <= RX_DATA_128b(i)(63 downto 32);
        RX_DATA_32b(4*i+2)  <= RX_DATA_128b(i)(95 downto 64);
        RX_DATA_32b(4*i+3)  <= RX_DATA_128b(i)(127 downto 96);
        TX_DATA_64b(i)      <= TX_DATA_16b(4*i+3) & TX_DATA_16b(4*i+2) & TX_DATA_16b(4*i+1) & TX_DATA_16b(4*i+0);

        g_sync_POLARITY: for pol in 0 to 3 generate
            xpm_cdc_RXPOLARITY: xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map (
                    src_clk => '0',
                    src_in => CTRL_RXPOLARITY(4*i+pol),
                    dest_clk => GT_RX_WORD_CLK(4*i+pol),
                    dest_out => CTRL_RXPOLARITY_rxusrclk(pol)
                );
            xpm_cdc_TXPOLARITY: xpm_cdc_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map (
                    src_clk => '0',
                    src_in => CTRL_TXPOLARITY(4*i+pol),
                    dest_clk => GT_TX_WORD_CLK(4*i+pol),
                    dest_out => CTRL_TXPOLARITY_txusrclk(pol)
                );
        end generate;

        g_712: if CARD_TYPE = 712 or CARD_TYPE = 128 or CARD_TYPE = 800 generate
            signal RxCdrLock_quad_s: std_logic_vector(3 downto 0);
            signal txresetdone_quad_s: std_logic_vector(3 downto 0);
            signal rxresetdone_quad_s: std_logic_vector(3 downto 0);
        begin
            GTH_TOP712_INST: entity work.FLX_LpGBT_GTH_BE
                generic map(
                    KCU_LOWER_LATENCY => KCU_LOWER_LATENCY,
                    CARD_TYPE => CARD_TYPE
                )
                Port map
        (
                    gt_rxusrclk_in => GT_RX_WORD_CLK(4*i+3 downto 4*i),
                    gt_txusrclk_in => GT_TX_WORD_CLK(4*i+3 downto 4*i),
                    gt_rxoutclk_out => GT_RXOUTCLK(4*i+3 downto 4*i),
                    gt_txoutclk_out => GT_TXOUTCLK(4*i+3 downto 4*i),
                    gthrxn_in => RX_N_i(4*i+3 downto 4*i),
                    gthrxp_in => RX_P_i(4*i+3 downto 4*i),
                    gthtxn_out => TX_N_i(4*i+3 downto 4*i),
                    gthtxp_out => TX_P_i(4*i+3 downto 4*i),
                    drpclk_in => drpclk_vec(i downto i),
                    gtrefclk0_in => GTHREFCLK(4*i downto 4*i),

                    rxpolarity_in => CTRL_RXPOLARITY_rxusrclk,
                    txpolarity_in => CTRL_TXPOLARITY_txusrclk,
                    loopback_in => tied_to_ground_vec_i,
                    rxcdrhold_in => tied_to_ground_i,
                    userdata_tx_in => TX_DATA_64b(i),
                    userdata_rx_out => RX_DATA_128b(i),

                    userclk_rx_reset_in => userclk_rx_reset_in(i downto i),
                    userclk_tx_reset_in => userclk_tx_reset_in(i downto i),
                    reset_all_in => CTRL_SOFT_RESET(i downto i),
                    reset_tx_pll_and_datapath_in => CTRL_TXPLL_DATAPATH_RESET(i downto i),
                    reset_tx_datapath_in => CTRL_TX_DATAPATH_RESET(i downto i),
                    reset_rx_pll_and_datapath_in => CTRL_RXPLL_DATAPATH_RESET(i downto i),
                    reset_rx_datapath_in => RX_DATAPATH_RESET_CHANNEL(4*i+3 downto 4*i),
                    qpll1lock_out => MON_QPLL_LCK_reg(i downto i),
                    qpll1fbclklost_out => open, --
                    qpll0lock_out => open,
                    qpll0fbclklost_out => open,
                    rxslide_in => RxSlide(4*i+3 downto 4*i),
                    txresetdone_out => TxResetDone(4*i+3 downto 4*i),
                    txpmaresetdone_out => txpmaresetdone(4*i+3 downto 4*i),
                    rxresetdone_out => rxresetdone(4*i+3 downto 4*i),
                    rxpmaresetdone_out => rxpmaresetdone(4*i+3 downto 4*i),
                    reset_tx_done_out => txresetdone_quad_s,
                    reset_rx_done_out => rxresetdone_quad_s,
                    reset_rx_cdr_stable_out => RxCdrLock_quad_s,
                    rxcdrlock_out => rxcdrlock_out(4*i+3 downto 4*i),
                    cplllock_out => MON_CPLL_LCK_reg(4*i+3 downto 4*i)
                );
            --zeroing to avoid glitches for auto_gth_rxrst
            rxfsmresetdone(4*i+3 downto 4*i) <= "0000";

            MON_RXRSTDONE_QUAD(i downto i)              <= rxresetdone_quad_clk40(i downto i);
            MON_TXRSTDONE_QUAD(i downto i)              <= txresetdone_quad_clk40(i downto i);
            MON_RXPMARSTDONE(4*i+3 downto 4*i)          <= rxpmaresetdone(4*i+3 downto 4*i);
            MON_TXPMARSTDONE(4*i+3 downto 4*i)          <= txpmaresetdone(4*i+3 downto 4*i);
            MON_RXCDR_LCK_QUAD(i downto i)              <= RxCdrLock_quad(i downto i);

            RxCdrLock_quad(i) <= RxCdrLock_quad_s(3) and RxCdrLock_quad_s(2) and  RxCdrLock_quad_s(1) and RxCdrLock_quad_s(0);
            txresetdone_quad(i) <= txresetdone_quad_s(3) and txresetdone_quad_s(2) and txresetdone_quad_s(1) and txresetdone_quad_s(0);
            rxresetdone_quad(i) <= rxresetdone_quad_s(3) and rxresetdone_quad_s(2) and rxresetdone_quad_s(1) and rxresetdone_quad_s(0);

        end generate; --g_712: if CARD_TYPE = 712 generate

        --to DO: implement CPLL
        g_709: if CARD_TYPE = 709 generate

            GTH_TOP709_INST: entity work.FLX_LpGBT_GTH_BE_VC7
                Port map
      (
                    GTH_RefClk => GTHREFCLK(4*i),
                    drpclk_in => drpclk_vec(i),
                    gt_rxusrclk_in => GT_RX_WORD_CLK(4*i+3 downto 4*i),
                    gt_txusrclk_in => GT_TXUSRCLK(4*i downto 4*i),
                    gt_rxoutclk_out => GT_RXOUTCLK(4*i+3 downto 4*i),
                    gt_txoutclk_out => GT_TXOUTCLK(4*i+3 downto 4*i),
                    gthrxn_in => RX_N_i(4*i+3 downto 4*i),
                    gthrxp_in => RX_P_i(4*i+3 downto 4*i),
                    gthtxn_out => TX_N_i(4*i+3 downto 4*i),
                    gthtxp_out => TX_P_i(4*i+3 downto 4*i),
                    rxpolarity_in => CTRL_RXPOLARITY_rxusrclk,
                    txpolarity_in => CTRL_TXPOLARITY_txusrclk,
                    userdata_tx_in => TX_DATA_64b(i),
                    userdata_rx_out => RX_DATA_128b(i),
                    gt_txresetdone_out => TxResetDone(4*i+3 downto 4*i),
                    gt_rxresetdone_out => rxresetdone(4*i+3 downto 4*i),
                    gt_txfsmresetdone_out => txfsmresetdone(4*i+3 downto 4*i),
                    gt_rxfsmresetdone_out => rxfsmresetdone(4*i+3 downto 4*i),
                    gt_cplllock_out => MON_CPLL_LCK_reg(4*i+3 downto 4*i),
                    gt_rxcdrlock_out => rxcdrlock_out(4*i+3 downto 4*i),
                    gt_rxcdrhold_in => "0000",
                    gt_qplllock_out => MON_QPLL_LCK_reg(i downto i),
                    gt_rxslide_in => RxSlide(4*i+3 downto 4*i),
                    gt_txuserrdy_in => "1111", --TxUsrRdy(4*i+3 downto 4*i),
                    --gt_rxuserrdy_in => "1111", --RxUsrRdy(4*i+3 downto 4*i),
                    --SOFT_RESET_IN => soft_reset(i) or rst_hw,
                    GTTX_RESET_IN => gttx_reset_i(4*i+3 downto 4*i), -- or rst_hw,
                    GTRX_RESET_IN => gtrx_reset_i(4*i+3 downto 4*i),
                    --CPLL_RESET_IN => cpll_reset(4*i+3 downto 4*i),
                    QPLL_RESET_IN => QPLL_RESET(i),
                    SOFT_TXRST_ALL => SOFT_TXRST_ALL(i) or rst_hw,
                    SOFT_RXRST_ALL => SOFT_RXRST_ALL(i) or rst_hw
                );
            --zeroing to avoid glitches for auto_gth_rxrst
            rxresetdone_quad(i) <= '0';
            MON_TXFSMRESETDONE(i) <= txfsmresetdone(i); --_clk40(i);
            MON_RXFSMRESETDONE(i) <= rxfsmresetdone(i); --_clk40(i);

        end generate; --g_709: if CARD_TYPE = 709 generate

        g_182: if CARD_TYPE = 180 or CARD_TYPE = 181 or CARD_TYPE = 182 or CARD_TYPE = 155 generate
            signal ch0_rxdata: std_logic_vector(127 downto 0);
            signal ch1_rxdata: std_logic_vector(127 downto 0);
            signal ch2_rxdata: std_logic_vector(127 downto 0);
            signal ch3_rxdata: std_logic_vector(127 downto 0);
            signal ch0_txdata: std_logic_vector(127 downto 0);
            signal ch1_txdata: std_logic_vector(127 downto 0);
            signal ch2_txdata: std_logic_vector(127 downto 0);
            signal ch3_txdata: std_logic_vector(127 downto 0);
            signal lcpll_lock: std_logic;
            --signal rxusrclk_quad : std_logic;
            --signal txusrclk_quad : std_logic;
            component transceiver_versal_lpgbt_wrapper is
                port (
                    GT_REFCLK0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_REFCLK0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_REFCLK1_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_REFCLK1_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_Serial_grx_n : in STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_grx_p : in STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_gtx_n : out STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_gtx_p : out STD_LOGIC_VECTOR ( 3 downto 0 );
                    apb3clk_gt_bridge_ip_0 : in STD_LOGIC;
                    apb3clk_quad : in STD_LOGIC;
                    ch0_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch0_rxbyteisaligned_ext_0 : out STD_LOGIC;
                    ch0_rxcdrlock_ext_0 : out STD_LOGIC;
                    ch0_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch0_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch0_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch0_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch0_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch0_rxpmaresetdone_ext_0 : out STD_LOGIC;
                    ch0_rxpolarity_ext_0 : in STD_LOGIC;
                    ch0_rxresetdone_ext_0 : out STD_LOGIC;
                    ch0_rxslide_ext_0 : in STD_LOGIC;
                    ch0_rxvalid_ext_0 : out STD_LOGIC;
                    ch0_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch0_txheader_ext_0 : in STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch0_txpolarity_ext_0 : in STD_LOGIC;
                    ch0_txresetdone_ext_0 : out STD_LOGIC;
                    ch0_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch1_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch1_rxbyteisaligned_ext_0 : out STD_LOGIC;
                    ch1_rxcdrlock_ext_0 : out STD_LOGIC;
                    ch1_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch1_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch1_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch1_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch1_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch1_rxpmaresetdone_ext_0 : out STD_LOGIC;
                    ch1_rxpolarity_ext_0 : in STD_LOGIC;
                    ch1_rxresetdone_ext_0 : out STD_LOGIC;
                    ch1_rxslide_ext_0 : in STD_LOGIC;
                    ch1_rxvalid_ext_0 : out STD_LOGIC;
                    ch1_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch1_txpolarity_ext_0 : in STD_LOGIC;
                    ch1_txresetdone_ext_0 : out STD_LOGIC;
                    ch1_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch2_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch2_rxbyteisaligned_ext_0 : out STD_LOGIC;
                    ch2_rxcdrlock_ext_0 : out STD_LOGIC;
                    ch2_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch2_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch2_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch2_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch2_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch2_rxpmaresetdone_ext_0 : out STD_LOGIC;
                    ch2_rxpolarity_ext_0 : in STD_LOGIC;
                    ch2_rxresetdone_ext_0 : out STD_LOGIC;
                    ch2_rxslide_ext_0 : in STD_LOGIC;
                    ch2_rxvalid_ext_0 : out STD_LOGIC;
                    ch2_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch2_txpolarity_ext_0 : in STD_LOGIC;
                    ch2_txresetdone_ext_0 : out STD_LOGIC;
                    ch2_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch3_loopback_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch3_rxbyteisaligned_ext_0 : out STD_LOGIC;
                    ch3_rxcdrlock_ext_0 : out STD_LOGIC;
                    ch3_rxctrl0_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_rxctrl1_ext_0 : out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_rxctrl2_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_rxctrl3_ext_0 : out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_rxdata_ext_0 : out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch3_rxdatavalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch3_rxgearboxslip_ext_0 : in STD_LOGIC;
                    ch3_rxheader_ext_0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch3_rxheadervalid_ext_0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch3_rxpmaresetdone_ext_0 : out STD_LOGIC;
                    ch3_rxpolarity_ext_0 : in STD_LOGIC;
                    ch3_rxresetdone_ext_0 : out STD_LOGIC;
                    ch3_rxslide_ext_0 : in STD_LOGIC;
                    ch3_rxvalid_ext_0 : out STD_LOGIC;
                    ch3_txctrl0_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_txctrl1_ext_0 : in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_txctrl2_ext_0 : in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_txdata_ext_0 : in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch3_txpolarity_ext_0 : in STD_LOGIC;
                    ch3_txresetdone_ext_0 : out STD_LOGIC;
                    ch3_txsequence_ext_0 : in STD_LOGIC_VECTOR ( 6 downto 0 );
                    gt_reset_gt_bridge_ip_0 : in STD_LOGIC;
                    lcpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
                    link_status_gt_bridge_ip_0 : out STD_LOGIC;
                    rate_sel_gt_bridge_ip_0 : in STD_LOGIC_VECTOR ( 3 downto 0 );
                    reset_rx_datapath_in_0 : in STD_LOGIC;
                    reset_rx_pll_and_datapath_in_0 : in STD_LOGIC;
                    reset_tx_datapath_in_0 : in STD_LOGIC;
                    reset_tx_pll_and_datapath_in_0 : in STD_LOGIC;
                    rpll_lock_gt_bridge_ip_0 : out STD_LOGIC;
                    rx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                    rxusrclk0 : out STD_LOGIC;
                    rxusrclk1 : out STD_LOGIC;
                    rxusrclk2 : out STD_LOGIC;
                    rxusrclk3 : out STD_LOGIC;
                    tx_resetdone_out_gt_bridge_ip_0 : out STD_LOGIC;
                    txusrclk0 : out STD_LOGIC;
                    txusrclk1 : out STD_LOGIC;
                    txusrclk2 : out STD_LOGIC;
                    txusrclk3 : out STD_LOGIC
                );
            end component transceiver_versal_lpgbt_wrapper;
        begin
            RX_DATA_128b(i) <= ch3_rxdata(31 downto 0) &
                               ch2_rxdata(31 downto 0) &
                               ch1_rxdata(31 downto 0) &
                               ch0_rxdata(31 downto 0);

            ch0_txdata(15 downto 0) <= TX_DATA_64b(i)(15 downto 0);
            ch1_txdata(15 downto 0) <= TX_DATA_64b(i)(31 downto 16);
            ch2_txdata(15 downto 0) <= TX_DATA_64b(i)(47 downto 32);
            ch3_txdata(15 downto 0) <= TX_DATA_64b(i)(63 downto 48);

            MON_CPLL_LCK_reg(4*i+3 downto 4*i) <= (others => lcpll_lock);

            --GT_RX_WORD_CLK(4*i+3 downto 4*i) <= (others => rxusrclk_quad);
            --GT_TX_WORD_CLK(4*i+3 downto 4*i) <= (others => txusrclk_quad);

            GTH_TOP182_INST: transceiver_versal_lpgbt_wrapper
                port map(
                    GT_REFCLK0_clk_n => GTREFCLK_n(i downto i), --in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_REFCLK0_clk_p => GTREFCLK_p(i downto i), --in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_REFCLK1_clk_n => "0", --in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_REFCLK1_clk_p => "0", --in STD_LOGIC_VECTOR ( 0 to 0 );
                    GT_Serial_grx_n => RX_N_i(4*i+3 downto 4*i), --in STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_grx_p => RX_P_i(4*i+3 downto 4*i), --in STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_gtx_n => TX_N_i(4*i+3 downto 4*i), --out STD_LOGIC_VECTOR ( 3 downto 0 );
                    GT_Serial_gtx_p => TX_P_i(4*i+3 downto 4*i), --out STD_LOGIC_VECTOR ( 3 downto 0 );
                    apb3clk_gt_bridge_ip_0 => clk100_in, --in STD_LOGIC;
                    apb3clk_quad => clk100_in, --in STD_LOGIC;
                    ch0_loopback_0 => "000", --in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch0_rxbyteisaligned_ext_0 => open, --out STD_LOGIC;
                    ch0_rxcdrlock_ext_0 => rxcdrlock_out(4*i+0), --out STD_LOGIC;
                    ch0_rxctrl0_ext_0 => open, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_rxctrl1_ext_0 => open, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_rxctrl2_ext_0 => open, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_rxctrl3_ext_0 => open, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_rxdata_ext_0 => ch0_rxdata, --out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch0_rxdatavalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch0_rxgearboxslip_ext_0 => '0', --in STD_LOGIC;
                    ch0_rxheader_ext_0 => open, --out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch0_rxheadervalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch0_rxpmaresetdone_ext_0 => rxpmaresetdone(4*i+0), --out STD_LOGIC;
                    ch0_rxpolarity_ext_0 => CTRL_RXPOLARITY_rxusrclk(0), --in STD_LOGIC;
                    ch0_rxresetdone_ext_0 => rxresetdone(4*i+0), --out STD_LOGIC;
                    ch0_rxslide_ext_0 => RxSlide(4*i+0), --in STD_LOGIC;
                    ch0_rxvalid_ext_0 => open, --out STD_LOGIC;
                    ch0_txctrl0_ext_0 => x"0000", --in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_txctrl1_ext_0 => x"0000", --in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch0_txctrl2_ext_0 => x"00", --in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch0_txdata_ext_0 => ch0_txdata, --in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch0_txheader_ext_0 => "000000", --in STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch0_txpolarity_ext_0 => CTRL_TXPOLARITY_txusrclk(0), --in STD_LOGIC;
                    ch0_txresetdone_ext_0 => TxResetDone(4*i+0), --out STD_LOGIC;
                    ch0_txsequence_ext_0 => "0000000", --in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch1_loopback_0 => "000", --in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch1_rxbyteisaligned_ext_0 => open, --out STD_LOGIC;
                    ch1_rxcdrlock_ext_0 => rxcdrlock_out(4*i+1) , --out STD_LOGIC;
                    ch1_rxctrl0_ext_0 => open, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_rxctrl1_ext_0 => open, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_rxctrl2_ext_0 => open, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_rxctrl3_ext_0 => open, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_rxdata_ext_0 => ch1_rxdata, --out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch1_rxdatavalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch1_rxgearboxslip_ext_0 => '0', --in STD_LOGIC;
                    ch1_rxheader_ext_0 => open, --out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch1_rxheadervalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch1_rxpmaresetdone_ext_0 => rxpmaresetdone(4*i+1), --out STD_LOGIC;
                    ch1_rxpolarity_ext_0 => CTRL_RXPOLARITY_rxusrclk(1), --in STD_LOGIC;
                    ch1_rxresetdone_ext_0 => rxresetdone(4*i+1), --out STD_LOGIC;
                    ch1_rxslide_ext_0 => RxSlide(4*i+1), --in STD_LOGIC;
                    ch1_rxvalid_ext_0 => open, --out STD_LOGIC;
                    ch1_txctrl0_ext_0 => x"0000", --in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_txctrl1_ext_0 => x"0000", --in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch1_txctrl2_ext_0 => x"00", --in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch1_txdata_ext_0 => ch1_txdata, --in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch1_txpolarity_ext_0 => CTRL_TXPOLARITY_txusrclk(1), --in STD_LOGIC;
                    ch1_txresetdone_ext_0 => TxResetDone(4*i+1), --out STD_LOGIC;
                    ch1_txsequence_ext_0 => "0000000", --in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch2_loopback_0 => "000", --in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch2_rxbyteisaligned_ext_0 => open, --out STD_LOGIC;
                    ch2_rxcdrlock_ext_0 => rxcdrlock_out(4*i+2), --out STD_LOGIC;
                    ch2_rxctrl0_ext_0 => open, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_rxctrl1_ext_0 => open, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_rxctrl2_ext_0 => open, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_rxctrl3_ext_0 => open, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_rxdata_ext_0 => ch2_rxdata, --out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch2_rxdatavalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch2_rxgearboxslip_ext_0 => '0', --in STD_LOGIC;
                    ch2_rxheader_ext_0 => open, --out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch2_rxheadervalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch2_rxpmaresetdone_ext_0 => rxpmaresetdone(4*i+2), --out STD_LOGIC;
                    ch2_rxpolarity_ext_0 => CTRL_RXPOLARITY_rxusrclk(2), --in STD_LOGIC;
                    ch2_rxresetdone_ext_0 => rxresetdone(4*i+2), --out STD_LOGIC;
                    ch2_rxslide_ext_0 => RxSlide(4*i+2), --in STD_LOGIC;
                    ch2_rxvalid_ext_0 => open, --out STD_LOGIC;
                    ch2_txctrl0_ext_0 => x"0000", --in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_txctrl1_ext_0 => x"0000", --in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch2_txctrl2_ext_0 => x"00", --in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch2_txdata_ext_0 => ch2_txdata, --in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch2_txpolarity_ext_0 => CTRL_TXPOLARITY_txusrclk(2), --in STD_LOGIC;
                    ch2_txresetdone_ext_0 => TxResetDone(4*i+2), --out STD_LOGIC;
                    ch2_txsequence_ext_0 => "0000000", --in STD_LOGIC_VECTOR ( 6 downto 0 );
                    ch3_loopback_0 => "000", --in STD_LOGIC_VECTOR ( 2 downto 0 );
                    ch3_rxbyteisaligned_ext_0 => open, --out STD_LOGIC;
                    ch3_rxcdrlock_ext_0 => rxcdrlock_out(4*i+3), --out STD_LOGIC;
                    ch3_rxctrl0_ext_0 => open, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_rxctrl1_ext_0 => open, --out STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_rxctrl2_ext_0 => open, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_rxctrl3_ext_0 => open, --out STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_rxdata_ext_0 => ch3_rxdata, --out STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch3_rxdatavalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch3_rxgearboxslip_ext_0 => '0', --in STD_LOGIC;
                    ch3_rxheader_ext_0 => open, --out STD_LOGIC_VECTOR ( 5 downto 0 );
                    ch3_rxheadervalid_ext_0 => open, --out STD_LOGIC_VECTOR ( 1 downto 0 );
                    ch3_rxpmaresetdone_ext_0 => rxpmaresetdone(4*i+3), --out STD_LOGIC;
                    ch3_rxpolarity_ext_0 => CTRL_RXPOLARITY_rxusrclk(3), --in STD_LOGIC;
                    ch3_rxresetdone_ext_0 => rxresetdone(4*i+3), --out STD_LOGIC;
                    ch3_rxslide_ext_0 => RxSlide(4*i+3), --in STD_LOGIC;
                    ch3_rxvalid_ext_0 => open, --out STD_LOGIC;
                    ch3_txctrl0_ext_0 => x"0000", --in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_txctrl1_ext_0 => x"0000", --in STD_LOGIC_VECTOR ( 15 downto 0 );
                    ch3_txctrl2_ext_0 => x"00", --in STD_LOGIC_VECTOR ( 7 downto 0 );
                    ch3_txdata_ext_0 => ch3_txdata, --in STD_LOGIC_VECTOR ( 127 downto 0 );
                    ch3_txpolarity_ext_0 => CTRL_TXPOLARITY_txusrclk(3), --in STD_LOGIC;
                    ch3_txresetdone_ext_0 => TxResetDone(4*i+3), --out STD_LOGIC;
                    ch3_txsequence_ext_0 => "0000000", --in STD_LOGIC_VECTOR ( 6 downto 0 );
                    gt_reset_gt_bridge_ip_0 => CTRL_SOFT_RESET(i), --in STD_LOGIC;
                    lcpll_lock_gt_bridge_ip_0 => lcpll_lock, --out STD_LOGIC;
                    link_status_gt_bridge_ip_0 => open, --out STD_LOGIC;
                    rate_sel_gt_bridge_ip_0 => "0000", --in STD_LOGIC_VECTOR ( 3 downto 0 );
                    reset_rx_datapath_in_0 => RX_DATAPATH_RESET_FINL(i), --in STD_LOGIC;
                    reset_rx_pll_and_datapath_in_0 => CTRL_RXPLL_DATAPATH_RESET(i), --in STD_LOGIC;
                    reset_tx_datapath_in_0 => CTRL_TX_DATAPATH_RESET(i), --in STD_LOGIC;
                    reset_tx_pll_and_datapath_in_0 => CTRL_TXPLL_DATAPATH_RESET(i), --in STD_LOGIC;
                    rpll_lock_gt_bridge_ip_0 => open, --out STD_LOGIC;
                    rx_resetdone_out_gt_bridge_ip_0 => rxresetdone_quad(i), --out STD_LOGIC;
                    --rxusrclk_gt_bridge_ip_0 => rxusrclk_quad, --out STD_LOGIC;
                    tx_resetdone_out_gt_bridge_ip_0 => txresetdone_quad(i), --: out STD_LOGIC;
                    --txusrclk_gt_bridge_ip_0 =>  txusrclk_quad  --: out STD_LOGIC
                    rxusrclk0 => GT_RX_WORD_CLK(4*i+0),
                    rxusrclk1 => GT_RX_WORD_CLK(4*i+1),
                    rxusrclk2 => GT_RX_WORD_CLK(4*i+2),
                    rxusrclk3 => GT_RX_WORD_CLK(4*i+3),
                    txusrclk0 => GT_TX_WORD_CLK(4*i+0),
                    txusrclk1 => GT_TX_WORD_CLK(4*i+1),
                    txusrclk2 => GT_TX_WORD_CLK(4*i+2),
                    txusrclk3 => GT_TX_WORD_CLK(4*i+3)
                );



            --zeroing to avoid glitches for auto_gth_rxrst
            rxfsmresetdone(4*i+3 downto 4*i) <= "0000";

            MON_RXRSTDONE_QUAD(i downto i)              <= rxresetdone_quad_clk40(i downto i);
            MON_TXRSTDONE_QUAD(i downto i)              <= txresetdone_quad_clk40(i downto i);
            MON_RXPMARSTDONE(4*i+3 downto 4*i)          <= rxpmaresetdone(4*i+3 downto 4*i);
            MON_TXPMARSTDONE(4*i+3 downto 4*i)          <= txpmaresetdone(4*i+3 downto 4*i);
            MON_RXCDR_LCK_QUAD(i downto i)              <= RxCdrLock_quad(i downto i);

        end generate; --g_182: if CARD_TYPE = 180 or CARD_TYPE = 181 or CARD_TYPE = 182 generate

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if cdr_cnt ="00000000000000000000" then
                    RxCdrLock_a(4*i)     <= rxcdrlock_out(4*i);
                    RxCdrLock_a(4*i+1)   <= rxcdrlock_out(4*i+1);
                    RxCdrLock_a(4*i+2)   <= rxcdrlock_out(4*i+2);
                    RxCdrLock_a(4*i+3)   <= rxcdrlock_out(4*i+3);
                else
                    RxCdrLock_a(4*i) <= RxCdrLock_a(4*i) and rxcdrlock_out(4*i);
                    RxCdrLock_a(4*i+1) <= RxCdrLock_a(4*i+1) and rxcdrlock_out(4*i+1);
                    RxCdrLock_a(4*i+2) <= RxCdrLock_a(4*i+2) and rxcdrlock_out(4*i+2);
                    RxCdrLock_a(4*i+3) <= RxCdrLock_a(4*i+3) and rxcdrlock_out(4*i+3);
                end if;
                if cdr_cnt="00000000000000000000" then
                    RxCdrLock_int(4*i) <=RxCdrLock_a(4*i);
                    RxCdrLock_int(4*i+1) <=RxCdrLock_a(4*i+1);
                    RxCdrLock_int(4*i+2) <=RxCdrLock_a(4*i+2);
                    RxCdrLock_int(4*i+3) <=RxCdrLock_a(4*i+3);
                end if;
            end if;
        end process;

        RxCdrLock(4*i)      <= (not CTRL_CHANNEL_DISABLE(4*i)) and RxCdrLock_int(4*i);
        RxCdrLock(4*i+1)    <= (not CTRL_CHANNEL_DISABLE(4*i+1)) and RxCdrLock_int(4*i+1);
        RxCdrLock(4*i+2)    <= (not CTRL_CHANNEL_DISABLE(4*i+2)) and RxCdrLock_int(4*i+2);
        RxCdrLock(4*i+3)    <= (not CTRL_CHANNEL_DISABLE(4*i+3)) and RxCdrLock_int(4*i+3);

        MON_RXRSTDONE(4*i+3 downto 4*i)             <= rxresetdone_clk40(4*i+3 downto 4*i);
        MON_TXRSTDONE(4*i+3 downto 4*i)             <= TxResetDone_clk40(4*i+3 downto 4*i);
        MON_RXCDR_LCK(4*i+3 downto 4*i)             <= RxCdrLock(4*i+3 downto 4*i);

    end generate; --GTH_inst

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            cdr_cnt <=cdr_cnt+'1';
        end if;
    end process;


    rst2_712: if CARD_TYPE = 712 or CARD_TYPE=182 or CARD_TYPE=181 or CARD_TYPE = 180 generate
        GTH_inst2 : for i in 0 to (GBT_NUM-1)/4 generate
            userclk_rx_reset_in(i) <=not (rxpmaresetdone(4*i+0) or rxpmaresetdone(4*i+1) or rxpmaresetdone(4*i+2) or rxpmaresetdone(4*i+3));
            userclk_tx_reset_in(i) <=not (txpmaresetdone(4*i+0) or txpmaresetdone(4*i+1) or txpmaresetdone(4*i+2) or txpmaresetdone(4*i+3));

            RX_DATAPATH_RESET_FINL(i) <= CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i) and RxCdrLock(4*i))
                                         or (auto_gth_rxrst(4*i+1) and RxCdrLock(4*i+1))
                                         or (auto_gth_rxrst(4*i+2) and RxCdrLock(4*i+2))
                                         or (auto_gth_rxrst(4*i+3) and RxCdrLock(4*i+3)) ;

            RX_DATAPATH_RESET_CHANNEL(4*i+3 downto 4*i) <= (CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i+3) and RxCdrLock(4*i+3))) &
                                                           (CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i+2) and RxCdrLock(4*i+2))) &
                                                           (CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i+1) and RxCdrLock(4*i+1))) &
                                                           (CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i+0) and RxCdrLock(4*i+0)));
        end generate; --GTH_inst2
    end generate; --rst2_712

    rx_rst_709: if CARD_TYPE = 709 generate
        rx_reset_l: for i in 0 to GBT_NUM-1 generate
            gtrx_reset_i(i) <= CTRL_GBTRXRST(i) or auto_gth_rxrst(i);
            gttx_reset_i(i) <= CTRL_GBTTXRST(i);
        end generate;
    end generate; --rx_rst_709

end Behavioral;
