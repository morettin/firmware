--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Kai Chen
--!               dmatakia
--!               Frans Schreuder
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:43:14 PM
-- Design Name: FELIX LpGBT Wrapper
-- Module Name: FELIX LpGBT Wrapper - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX LpGBT
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;

entity FELIX_LpGBT_Wrapper is
    Generic (
        CARD_TYPE                   : integer   := 712;
        GBT_NUM                     : integer   := 24;
        GTREFCLKS                   : integer := 5;
        --VC709 only
        -- CPLL : '0'
        -- QPLL : '1'
        --FLX712 only
        CLK_CHIP_SEL                : integer   := 1; --tested only with =1
        -- SI5345               : integer   := 0;
        -- LMK03200             : integer   := 1;

        KCU_LOWER_LATENCY           : integer := 1

    );
    Port (
        rst_hw                      : in std_logic;
        register_map_control        : in register_map_control_type;
        register_map_link_monitor    : out register_map_link_monitor_type;
        --VC709 only
        --        GREFCLK_IN                  : in std_logic; --not used since
        --        GTHREFCLK_SEL=0 in RefClkGen
        --
        GTREFCLK_N_IN                : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_P_IN                : in std_logic_vector(GTREFCLKS-1 downto 0);
        CLK40_IN                    : in std_logic;
        clk100_in                   : in std_logic;
        LMK_P                       : in std_logic_vector(NUM_LMK(CARD_TYPE)*6-1 downto 0);
        LMK_N                       : in std_logic_vector(NUM_LMK(CARD_TYPE)*6-1 downto 0);

        FELIX_DOWNLINK_USER_DATA    : in txrx32b_type(0 to GBT_NUM-1);
        FELIX_DOWNLINK_IC_DATA      : in txrx2b_type(0 to GBT_NUM-1);
        FELIX_DOWNLINK_EC_DATA      : in txrx2b_type(0 to GBT_NUM-1);

        FELIX_UPLINK_USER_DATA      : out txrx224b_type(0 to GBT_NUM-1);
        FELIX_UPLINK_EC_DATA        : out txrx2b_type(0 to GBT_NUM-1);
        FELIX_UPLINK_IC_DATA        : out txrx2b_type(0 to GBT_NUM-1);

        LinkAligned                 : out std_logic_vector(GBT_NUM-1 downto 0); --MT added different ch order wrt local

        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                        : in  std_logic_vector(GBT_NUM-1 downto 0);
        RX_N                        : in  std_logic_vector(GBT_NUM-1 downto 0);
        RXUSRCLK_OUT                : out std_logic_vector(GBT_NUM-1 downto 0)

    );
end FELIX_LpGBT_Wrapper;


architecture Behavioral of FELIX_LpGBT_Wrapper is

    --signal FE_SIDE_RX40MCLK         : std_logic;
    --signal FELIX_SIDE_RX40MCLK      : std_logic;
    --signal PRBS_ERR_CLR             : std_logic;

    signal GTH_REFCLK_OUT           : std_logic_vector(GBT_NUM-1 downto 0);
    --signal GTH_EMU_REFCLK_OUT       : std_logic_vector(GBT_NUM-1 downto 0);

    signal CTRL_TXPOLARITY          : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_RXPOLARITY          : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_GBTTXRST          : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_GBTRXRST          : std_logic_vector(GBT_NUM-1 downto 0);
    --signal CTRL_DATARATE            : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_FECMODE            : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_CHANNEL_DISABLE     : std_logic_vector(GBT_NUM-1 downto 0);
    signal CTRL_MULTICYCLE_DELAY : std_logic_vector(2 downto 0);
    signal MON_RXRSTDONE          : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_TXRSTDONE          : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_RXPMARSTDONE          : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_TXPMARSTDONE          : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_RXCDR_LCK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_CPLL_LCK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_ALIGNMENT_DONE   : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_FEC_ERROR         : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_FEC_ERR_CNT       : array_32b(0 to GBT_NUM-1);
    signal MON_QPLL_LCK          : std_logic_vector(GBT_NUM/4-1 downto 0);


    signal CTRL_SOFT_RESET          : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_TXPLL_DATAPATH_RESET: std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_RXPLL_DATAPATH_RESET: std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_TX_DATAPATH_RESET   : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal CTRL_RX_DATAPATH_RESET   : std_logic_vector(GBT_NUM/4-1 downto 0);

    signal CTRL_GBT_General_ctrl    : std_logic_vector(63 downto 0);
    signal LMK_P_s, LMK_N_s : std_logic_vector(5 downto 0);

    signal SOFT_TXRST_ALL : std_logic_vector(11 downto 0);
    signal SOFT_RXRST_ALL : std_logic_vector(11 downto 0);
    signal QPLL_RESET : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal MON_TXFSMRESETDONE : std_logic_vector(GBT_NUM-1 downto 0);
    signal MON_RXFSMRESETDONE : std_logic_vector(GBT_NUM-1 downto 0);
    signal GTREFCLK_P_IN_s: std_logic_vector(4 downto 0);
    signal GTREFCLK_N_IN_s: std_logic_vector(4 downto 0);
    signal MON_AUTO_RX_RESET_CNT        :  array_32b(0 to GBT_NUM-1);
    signal CTRL_AUTO_RX_RESET_CNT_CLEAR :  std_logic_vector(GBT_NUM-1 downto 0);


begin

    g_gtrefclks_s: for i in 0 to 4 generate
        g_gt: if GTREFCLKS>i generate
            GTREFCLK_P_IN_s(i) <= GTREFCLK_P_IN(i);
            GTREFCLK_N_IN_s(i) <= GTREFCLK_N_IN(i);
        end generate;
    end generate;

    g_lmk: for i in 0 to NUM_LMK(CARD_TYPE)*6 -1 generate
        LMK_P_s(i) <= LMK_P(i);
        LMK_N_s(i) <= LMK_N(i);
    end generate;

    Reference_Clk_Gen : entity work.RefClk_Gen
        Generic Map(
            GBT_NUM                     => GBT_NUM,
            GTREFCLKS                   => GTREFCLKS,
            --VC709 only
            GTHREFCLK_SEL               => '0',
            -- GREFCLK              : std_logic := '1';
            -- MGTREFCLK            : std_logic := '0';
            --
            CARD_TYPE                   => CARD_TYPE,
            CLK_CHIP_SEL                => CLK_CHIP_SEL
        -- SI5345               : integer   := 0;
        -- LMK03200             : integer   := 1
        )
        Port Map(
            --            GREFCLK_IN                  => GREFCLK_IN,

            SI53XX_0_P                  => GTREFCLK_P_IN_s(0),
            SI53XX_0_N                  => GTREFCLK_N_IN_s(0),
            SI53XX_2_P                  => GTREFCLK_P_IN_s(1),
            SI53XX_2_N                  => GTREFCLK_N_IN_s(1),
            SI53XX_4_P                  => GTREFCLK_P_IN_s(3),
            SI53XX_4_N                  => GTREFCLK_N_IN_s(3),
            SI53XX_5_P                  => GTREFCLK_P_IN_s(2),
            SI53XX_5_N                  => GTREFCLK_N_IN_s(2),
            SI53XX_8_P                  => GTREFCLK_P_IN_s(4),
            SI53XX_8_N                  => GTREFCLK_N_IN_s(4),

            LMK0_P                      => LMK_P_s(0),
            LMK0_N                      => LMK_N_s(0),
            LMK1_P                      => LMK_P_s(1),
            LMK1_N                      => LMK_N_s(1),
            LMK2_P                      => LMK_P_s(2),
            LMK2_N                      => LMK_N_s(2),
            LMK3_P                      => LMK_P_s(3),
            LMK3_N                      => LMK_N_s(3),
            LMK4_P                      => LMK_P_s(4),
            LMK4_N                      => LMK_N_s(4),
            LMK5_P                      => LMK_P_s(5),
            LMK5_N                      => LMK_N_s(5),
            LMK6_P                      => '0',--LMK_P_s(6),
            LMK6_N                      => '0',--LMK_N_s(6),
            LMK7_P                      => '0',--LMK_P_s(7),
            LMK7_N                      => '0',--LMK_N_s(7),

            GTREFCLK_P_IN => GTREFCLK_P_IN,
            GTREFCLK_N_IN => GTREFCLK_N_IN,

            GTH_REFCLK_OUT              => GTH_REFCLK_OUT

        );

    REGS_INTERFACE : entity work.Regs_RW
        Generic map(
            GBT_NUM                     => GBT_NUM,
            CARD_TYPE                   => CARD_TYPE
        )
        Port map(

            --VC709
            SOFT_TXRST_ALL              => SOFT_TXRST_ALL,
            SOFT_RXRST_ALL              => SOFT_RXRST_ALL,
            QPLL_RESET                  => QPLL_RESET,
            --

            CTRL_SOFT_RESET             => CTRL_SOFT_RESET,
            CTRL_TXPLL_DATAPATH_RESET   => CTRL_TXPLL_DATAPATH_RESET,
            CTRL_RXPLL_DATAPATH_RESET   => CTRL_RXPLL_DATAPATH_RESET,
            CTRL_TX_DATAPATH_RESET      => CTRL_TX_DATAPATH_RESET,
            CTRL_RX_DATAPATH_RESET      => CTRL_RX_DATAPATH_RESET,
            CTRL_TXPOLARITY             => CTRL_TXPOLARITY,
            CTRL_RXPOLARITY             => CTRL_RXPOLARITY,
            CTRL_GBTTXRST               => CTRL_GBTTXRST,
            CTRL_GBTRXRST               => CTRL_GBTRXRST,
            CTRL_CHANNEL_DISABLE        => CTRL_CHANNEL_DISABLE,
            CTRL_FECMODE                => CTRL_FECMODE,
            --            CTRL_DATARATE               => CTRL_DATARATE,
            CTRL_GBT_General_ctrl       => CTRL_GBT_General_ctrl,
            CTRL_MULTICYCLE_DELAY       => CTRL_MULTICYCLE_DELAY,
            MON_RXRSTDONE               => MON_RXRSTDONE,
            MON_TXRSTDONE               => MON_TXRSTDONE,
            --VC709
            MON_TXFSMRESETDONE         => MON_TXFSMRESETDONE,
            MON_RXFSMRESETDONE         => MON_RXFSMRESETDONE,
            MON_RXPMARSTDONE            => MON_RXPMARSTDONE,
            MON_TXPMARSTDONE            => MON_TXPMARSTDONE,
            --
            MON_RXCDR_LCK               => MON_RXCDR_LCK,
            MON_QPLL_LCK                => MON_QPLL_LCK,
            MON_CPLL_LCK                => MON_CPLL_LCK,
            MON_ALIGNMENT_DONE          => MON_ALIGNMENT_DONE,
            MON_FEC_ERROR               => MON_FEC_ERROR,
            MON_FEC_ERR_CNT             => MON_FEC_ERR_CNT,
            MON_AUTO_RX_RESET_CNT       => MON_AUTO_RX_RESET_CNT,
            CTRL_AUTO_RX_RESET_CNT_CLEAR => CTRL_AUTO_RX_RESET_CNT_CLEAR,
            clk40_in                    => CLK40_IN,
            register_map_control        => register_map_control,
            register_map_link_monitor    => register_map_link_monitor

        );


    FLX_LpGBT_BE_INST: entity work.FLX_LpGBT_BE_Wrapper
        Generic map(
            CARD_TYPE                   => CARD_TYPE,
            GBT_NUM                     => GBT_NUM,
            KCU_LOWER_LATENCY           => KCU_LOWER_LATENCY,
            GTREFCLKS                   => GTREFCLKS
        )
        Port map(
            FELIX_DOWNLINK_USER_DATA    => FELIX_DOWNLINK_USER_DATA,
            FELIX_DOWNLINK_EC_DATA      => FELIX_DOWNLINK_EC_DATA,
            FELIX_DOWNLINK_IC_DATA      => FELIX_DOWNLINK_IC_DATA,
            FELIX_UPLINK_USER_DATA      => FELIX_UPLINK_USER_DATA,
            FELIX_UPLINK_EC_DATA        => FELIX_UPLINK_EC_DATA,
            FELIX_UPLINK_IC_DATA        => FELIX_UPLINK_IC_DATA,
            clk40_in                    => CLK40_IN,
            clk100_in                   => clk100_in,
            rst_hw                      => rst_hw,
            RX_P                        => RX_P(GBT_NUM-1 downto 0),
            RX_N                        => RX_N(GBT_NUM-1 downto 0),
            TX_P                        => TX_P(GBT_NUM-1 downto 0),
            TX_N                        => TX_N(GBT_NUM-1 downto 0),
            GTHREFCLK                   => GTH_REFCLK_OUT,
            GTREFCLK_p                  => GTREFCLK_P_IN,
            GTREFCLK_n                  => GTREFCLK_N_IN,
            CTRL_SOFT_RESET             => CTRL_SOFT_RESET(GBT_NUM/4-1 downto 0),
            CTRL_TXPLL_DATAPATH_RESET   => CTRL_TXPLL_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_RXPLL_DATAPATH_RESET   => CTRL_RXPLL_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_TX_DATAPATH_RESET      => CTRL_TX_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_RX_DATAPATH_RESET      => CTRL_RX_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_TXPOLARITY             => CTRL_TXPOLARITY(GBT_NUM-1 downto 0),
            CTRL_RXPOLARITY             => CTRL_RXPOLARITY(GBT_NUM-1 downto 0),
            CTRL_GBTTXRST               => CTRL_GBTTXRST(GBT_NUM-1 downto 0),
            CTRL_GBTRXRST               => CTRL_GBTRXRST(GBT_NUM-1 downto 0),
            --            CTRL_DATARATE               => CTRL_DATARATE(GBT_NUM-1 downto 0),
            SOFT_TXRST_ALL              => SOFT_TXRST_ALL,
            SOFT_RXRST_ALL              => SOFT_RXRST_ALL,
            QPLL_RESET                  => QPLL_RESET,
            CTRL_FECMODE                => CTRL_FECMODE(GBT_NUM-1 downto 0),
            CTRL_CHANNEL_DISABLE        => CTRL_CHANNEL_DISABLE(GBT_NUM-1 downto 0),
            CTRL_GBT_General_ctrl       => CTRL_GBT_General_ctrl,
            CTRL_MULTICYCLE_DELAY       => CTRL_MULTICYCLE_DELAY,
            MON_RXRSTDONE               => MON_RXRSTDONE(GBT_NUM-1 downto 0),
            MON_TXRSTDONE               => MON_TXRSTDONE(GBT_NUM-1 downto 0),
            MON_RXRSTDONE_QUAD          => open, --MON_RXRSTDONE_QUAD(GBT_NUM/4-1 downto 0),
            MON_TXRSTDONE_QUAD          => open, --MON_TXRSTDONE_QUAD(GBT_NUM/4-1 downto 0),
            MON_RXPMARSTDONE            => MON_RXPMARSTDONE(GBT_NUM-1 downto 0),
            MON_TXPMARSTDONE            => MON_TXPMARSTDONE(GBT_NUM-1 downto 0),
            MON_TXFSMRESETDONE         => MON_TXFSMRESETDONE(GBT_NUM-1 downto 0),
            MON_RXFSMRESETDONE         => MON_RXFSMRESETDONE(GBT_NUM-1 downto 0),
            MON_RXCDR_LCK               => MON_RXCDR_LCK(GBT_NUM-1 downto 0),
            MON_RXCDR_LCK_QUAD          => open, --open, --MON_RXCDR_LCK_QUAD(GBT_NUM/4-1 downto 0),
            MON_QPLL_LCK                => MON_QPLL_LCK(GBT_NUM/4-1 downto 0),
            MON_CPLL_LCK                => MON_CPLL_LCK(GBT_NUM-1 downto 0),
            MON_ALIGNMENT_DONE          => MON_ALIGNMENT_DONE(GBT_NUM-1 downto 0),
            MON_FEC_ERROR               => MON_FEC_ERROR(GBT_NUM-1 downto 0),
            MON_FEC_ERR_CNT             => MON_FEC_ERR_CNT,
            MON_AUTO_RX_RESET_CNT       => MON_AUTO_RX_RESET_CNT,
            CTRL_AUTO_RX_RESET_CNT_CLEAR => CTRL_AUTO_RX_RESET_CNT_CLEAR,
            RXUSRCLK_OUT                => RXUSRCLK_OUT

        );

    LinkAligned <= MON_ALIGNMENT_DONE;




end Behavioral;
