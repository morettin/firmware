--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen      (kchen@bnl.gov)
--! modified:    Marco Trovato (mtrovato@anl.gov)
--!              Ricardo Luz   (rluz@anl.gov)      (based on lpgbtemul_top.vhd)
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:43:14 PM
-- Design Name: LpGBT FE Top
-- Module Name: LpGBT FE Top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The LpGBT Front-End Top
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------


LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;-- @suppress "Deprecated package"
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;-- @suppress "Deprecated package"

library XPM;
    use xpm.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
    use work.lpgbtfpga_package.all;
entity FLX_LpGBT_FE is
    Port
    (
        TXCLK40                         : in std_logic;
        RXCLK40                         : in std_logic;
        TXCLK320                        : in std_logic;
        RXCLK320                        : in std_logic;
        rst_uplink_i                    : in std_logic;
        --uplinkClkEn_i                   : in std_logic;
        ctr_clkSlip_s                   : out std_logic;
        aligned                         : out std_logic;
        --uplinkRdy_o                     : out std_logic;
        --downlinkRdy_o                   : out std_logic;
        sta_headerFlag_o                : out std_logic;
        sta_headerFlag_shift            : out std_logic;
        --sta_rxgbxRdy_o                  : out std_logic;
        clk_dataFlag_rxGb_s_o           : out std_logic;
        dat_upLinkWord_fromGb_s         : out std_logic_vector(31 downto 0);
        dat_downLinkWord_fromMgt_s16    : in std_logic_vector(15 downto 0);
        --sta_mgtTxRdy_s                  : in std_logic;
        rst_dnlink_i                    : in std_logic;
        sta_mgtRxRdy_s                  : in std_logic;

        downLinkBypassDeinterleaver     : in std_logic;
        downLinkBypassFECDecoder        : in std_logic;
        downLinkBypassDescsrambler      : in std_logic;

        enableFECErrCounter             : in std_logic;

        upLinkScramblerBypass           : in std_logic;

        upLinkFecBypass                 : in std_logic;
        upLinkInterleaverBypass         : in std_logic;
        fecMode                         : in std_logic;
        txDataRate                      : in std_logic;
        phase_sel                       : in std_logic_vector(2 downto 0);
        upLinkData                      : in std_logic_vector(223 downto 0);
        upLinkDataIC                    : in std_logic_vector(1 downto 0);
        upLinkDataEC                    : in std_logic_vector(1 downto 0);
        downLinkData                    : out std_logic_vector(31 downto 0);
        downLinkDataIC                  : out std_logic_vector(1 downto 0);
        downLinkDataEC                  : out std_logic_vector(1 downto 0);
        --MT added: needed by FELIG to latch tx payload to generated data. Need to
        --be synchronized with emulator logic
        tx_flag_out                     : out std_logic


    );
end FLX_LpGBT_FE;

architecture Behavioral of FLX_LpGBT_FE is

    --signal clk_mgtTxUsrclkToEmul_s              : std_logic;
    --signal clk_mgtRxUsrclkToEmul_s              : std_logic;


    signal rst_pattsearch_s                     : std_logic;
    signal sta_headeLocked_s                    : std_logic;
    signal sta_headerFlag_s                     : std_logic;
    signal sta_headerFlag_s_r1                  : std_logic;
    signal sta_headerFlag_s_r2                  : std_logic;

    --signal sta_rxgbxRdy_s                       : std_logic;
    signal sta_headerFlag_s_r3                  : std_logic;
    signal sta_headerFlag_s_r4                  : std_logic;

    --signal rstn_datapath_s                      : std_logic;
    --signal rst_datapath_s                       : std_logic;

    signal dat_downLinkWord_fromGb_s            : std_logic_vector(63 downto 0);
    --signal dat_downLinkWord_fromGbInv_s         : std_logic_vector(63 downto 0);
    signal dat_downLinkWord_toPattSrch_s        : std_logic_vector(15 downto 0);

    --signal downlinkRdy_s0                       : std_logic;
    --signal downlinkRdy_s1                       : std_logic;

    --signal uplinkClkEn_sh_s                     : std_logic;
    --signal uplinkClkEn_shgb_s                   : std_logic;
    --signal sta_txGbRdy_s                        : std_logic;

    signal dat_upLinkWord_fromLpGBT_s           : std_logic_vector(255 downto 0);
    --signal dat_upLinkWord_fromLpGBT_pipeline_s  : std_logic_vector(255 downto 0);
    --signal dat_upLinkWord_toGb_s                : std_logic_vector(255 downto 0);
    signal dat_upLinkWord_toGb_pipeline_s       : std_logic_vector(255 downto 0);
    signal dat_upLinkWord_toGb_pipeline_s_r     : std_logic_vector(255 downto 0);
    signal dat_upLinkWord_toGb_pipeline_s_inv   : std_logic_vector(255 downto 0);

    --signal rst_uplinkGb_s                       : std_logic;
    signal RXCLK40_r                            : std_logic;
    --signal rst_uplinkGb_synch_s                 : std_logic;
    --signal rst_uplinkMgt_s                      : std_logic;
    signal sel                                  : std_logic;
    --signal rst_uplinkInitDone_s                 : std_logic;
    --signal rst_downlinkInitDone_s               : std_logic;

    --signal RX_CLKEn_s                           : std_logic;
    --signal RX_CLK40_s                           : std_logic;
    signal TXCLK40_r                            : std_logic;

    signal downLinkDataIc_s                     : std_logic_vector(1 downto 0);
    signal downLinkDataEc_s                     : std_logic_vector(1 downto 0);
    signal downLinkDataGroup1_s                 : std_logic_vector(15 downto 0);
    signal downLinkDataGroup0_s                 : std_logic_vector(15 downto 0);

    constant clk_dataFlag_rxGb_s                  : std_logic := '0';
    signal txcnt                                : std_logic_vector(2 downto 0);
    signal count_rx                             : std_logic_vector(2 downto 0);
    signal upLinkData0_s                        : std_logic_vector(31 downto 0);
    signal upLinkData1_s                        : std_logic_vector(31 downto 0);
    signal upLinkData2_s                        : std_logic_vector(31 downto 0);
    signal upLinkData3_s                        : std_logic_vector(31 downto 0);
    signal upLinkData4_s                        : std_logic_vector(31 downto 0);
    signal upLinkData5_s                        : std_logic_vector(31 downto 0);
    signal dat_downLinkWord_fromMgt_s           : std_logic_vector(31 downto 0);
    signal upLinkData6_s                        : std_logic_vector(31 downto 0);
    --signal upLinkDataIC_s                       : std_logic_vector(1 downto 0);
    --signal upLinkDataEC_s                       : std_logic_vector(1 downto 0);
    signal dat_downLinkWord_fromMgt_s64         : std_logic_vector(63 downto 0);
    signal dat_downLinkWord_fromGb_s_buf        : std_logic_vector(63 downto 0);
    signal dat_downLinkWord_fromMgt_s8          : std_logic_vector(7 downto 0);

    signal sta_headerFlag_s_vec: std_logic_vector(9 downto 0);
    signal sta_headerFlag_s0: std_logic;

    signal TXCLK40_320: std_logic; --CDC synchronized version of TXCLK40 in 8 320MHz clocks
    signal RXCLK40_320: std_logic; --CDC synchronized version of RXCLK40 in 8 320MHz clocks

    component upLinkTxDataPath
        port (
            clk                 : in  std_logic;
            dataEnable          : in  std_logic;
            txDataGroup0        : in  std_logic_vector(31 downto 0);
            txDataGroup1        : in  std_logic_vector(31 downto 0);
            txDataGroup2        : in  std_logic_vector(31 downto 0);
            txDataGroup3        : in  std_logic_vector(31 downto 0);
            txDataGroup4        : in  std_logic_vector(31 downto 0);
            txDataGroup5        : in  std_logic_vector(31 downto 0);
            txDataGroup6        : in  std_logic_vector(31 downto 0);

            txIC                : in  std_logic_vector(  1 downto 0);
            txEC                : in  std_logic_vector(  1 downto 0);
            txDummyFec5         : in  std_logic_vector(  5 downto 0);
            txDummyFec12        : in  std_logic_vector(  9 downto 0);
            scramblerBypass     : in  std_logic;
            interleaverBypass   : in  std_logic;
            fecMode             : in  std_logic;
            txDataRate          : in  std_logic;
            fecDisable          : in  std_logic;
            scramblerReset      : in  std_logic;
            upLinkFrame         : out std_logic_vector(255 downto 0)
        );
    end component;

    component downLinkRxDataPath
        port (
            clk                 : in  std_logic;
            downLinkFrame       : in  std_logic_vector( 63 downto 0);
            dataStrobe          : out std_logic;
            dataOut             : out std_logic_vector( 31 downto 0);
            dataEC              : out std_logic_vector(  1 downto 0);
            dataIC              : out std_logic_vector(  1 downto 0);
            header              : out std_logic_vector(  3 downto 0);
            dataEnable          : in  std_logic;
            bypassDeinterleaver : in  std_logic;
            bypassFECDecoder    : in  std_logic;
            bypassDescrambler   : in  std_logic;
            fecCorrectionCount  : out std_logic_vector( 15 downto 0)
        );
    end component;

    COMPONENT mgt_framealigner IS
        GENERIC (
            c_wordRatio                      : integer;             --! Word ration: frameclock / mgt_wordclock
            c_wordSize                       : integer;             --! Size of the mgt word
            c_headerPattern                  : std_logic_vector;    --! Header pattern specified by the standard
            c_allowedFalseHeader             : integer;             --! Number of false header allowed to avoid unlock on frame error
            c_allowedFalseHeaderOverN        : integer;             --! Number of header checked to know wether the lock is lost or not
            c_requiredTrueHeader             : integer;             --! Number of true header required to go in locked state

            c_bitslip_mindly                 : integer := 1;        --! Number of clock cycle required WHEN asserting the bitslip SIGNAL
            c_bitslip_waitdly                : integer := 40        --! Number of clock cycle required before being back in a stable state
        );
        PORT (
            -- Clock(s)
            clk_pcsRx_i                      : in  std_logic;       --! MGT Wordclock

            -- Reset(s)
            rst_pattsearch_i                 : in  std_logic;       --! Rst the pattern search state machines

            -- Control
            cmd_bitslipCtrl_o                : out std_logic;       --! Bitslip SIGNAL to shift the parrallel word

            -- Status
            sta_headerLocked_o               : out std_logic;       --! Status: header is locked
            sta_headerFlag_o                 : out std_logic;       --! Status: header flag (1 pulse over c_wordRatio)
            sta_bitSlipEven_o                : out std_logic;       --!  Status: number of bit slips is even

            -- Data
            dat_word_i                       : in  std_logic_vector(c_headerPattern'length-1 downto 0)  --! Header bits from the MGT word (compared with c_headerPattern)
        );
    END COMPONENT;

begin

    --MTcomment: doubling bits to compensate oversamplngratio=2 (all modules in
    --LpGBT_CERN meant for 10g24 rate...see lpgbtemul_top in lpgbt-fpga-kcu105
    --v1.1.0 tag)
    dat_downLinkWord_fromMgt_s <=
                                  dat_downLinkWord_fromMgt_s16(15) & dat_downLinkWord_fromMgt_s16(15) &
                                  dat_downLinkWord_fromMgt_s16(14) & dat_downLinkWord_fromMgt_s16(14) &
                                  dat_downLinkWord_fromMgt_s16(13) & dat_downLinkWord_fromMgt_s16(13) &
                                  dat_downLinkWord_fromMgt_s16(12) & dat_downLinkWord_fromMgt_s16(12) &
                                  dat_downLinkWord_fromMgt_s16(11) & dat_downLinkWord_fromMgt_s16(11) &
                                  dat_downLinkWord_fromMgt_s16(10) & dat_downLinkWord_fromMgt_s16(10) &
                                  dat_downLinkWord_fromMgt_s16(9) & dat_downLinkWord_fromMgt_s16(9) &
                                  dat_downLinkWord_fromMgt_s16(8) & dat_downLinkWord_fromMgt_s16(8) &
                                  dat_downLinkWord_fromMgt_s16(7) & dat_downLinkWord_fromMgt_s16(7) &
                                  dat_downLinkWord_fromMgt_s16(6) & dat_downLinkWord_fromMgt_s16(6) &
                                  dat_downLinkWord_fromMgt_s16(5) & dat_downLinkWord_fromMgt_s16(5) &
                                  dat_downLinkWord_fromMgt_s16(4) & dat_downLinkWord_fromMgt_s16(4) &
                                  dat_downLinkWord_fromMgt_s16(3) & dat_downLinkWord_fromMgt_s16(3) &
                                  dat_downLinkWord_fromMgt_s16(2) & dat_downLinkWord_fromMgt_s16(2) &
                                  dat_downLinkWord_fromMgt_s16(1) & dat_downLinkWord_fromMgt_s16(1) &
                                  dat_downLinkWord_fromMgt_s16(0) & dat_downLinkWord_fromMgt_s16(0);

    aligned                 <= sta_headeLocked_s;
    sta_headerFlag_o        <= sta_headerFlag_s0;
    sta_headerFlag_shift    <= sta_headerFlag_s;
    --sta_rxgbxRdy_o          <= sta_rxgbxRdy_s;
    clk_dataFlag_rxGb_s_o   <= clk_dataFlag_rxGb_s;

    downLinkData(15 downto 0)       <= downLinkDataGroup0_s;
    downLinkData(31 downto 16)      <= downLinkDataGroup1_s;
    downLinkDataEC                  <= downLinkDataEc_s;
    downLinkDataIC                  <= downLinkDataIc_s;

    rst_pattsearch_s                <= not(sta_mgtRxRdy_s);
    --rst_datapath_s                  <= not(sta_headeLocked_s);
    --rst_uplinkGb_s                  <= rst_uplink_i or not(sta_mgtTxRdy_s);-- or not(sta_headeLocked_s);
    --uplinkRdy_o                     <= sta_txGbRdy_s;

    -- RL: updated mgt_framealigner with version 2.0
    -- Pattern aligner
    mgt_framealigner_inst: entity work.mgt_framealigner
        generic map
    (
            c_wordRatio                      => 8,
            c_headerPattern                  => x"F00F",
            c_wordSize                       => 32,
            c_allowedFalseHeader             => 32,
            c_allowedFalseHeaderOverN        => 40,
            c_requiredTrueHeader             => 30,
            -- c_resetOnEven                    => 0,
            c_bitslip_mindly                 => 1, --RL: to match old implementation
            c_bitslip_waitdly                => 40 --RL: to match old implementation
        )
        port map (
            -- Clock(s)
            clk_pcsRx_i                      => RXCLK320,
            --clk_freeRunningClk_i             => '0',                  -- Not used: rst on even is not enabled

            -- Reset(s)
            rst_pattsearch_i                 => rst_pattsearch_s,
            --rst_mgtctrler_i                  => '0',                  -- Not used: rst on even is not enabled
            --rst_rstoneven_o                  => open,                 -- Not used: rst on even is not enabled

            -- Control
            cmd_bitslipCtrl_o                => ctr_clkSlip_s,
            --cmd_rstonevenoroddsel_i          => '0',                  -- Not used: rst on even is not enabled

            -- Status
            sta_headerLocked_o               => sta_headeLocked_s,
            sta_headerFlag_o                 => sta_headerFlag_s0,
            sta_bitSlipEven_o => open,
            -- Data
            dat_word_i                       => dat_downLinkWord_toPattSrch_s
        );

    dat_downLinkWord_toPattSrch_s <=
                                     dat_downLinkWord_fromMgt_s(24) & dat_downLinkWord_fromMgt_s(25) & dat_downLinkWord_fromMgt_s(26) & dat_downLinkWord_fromMgt_s(27) &
                                     dat_downLinkWord_fromMgt_s(16) & dat_downLinkWord_fromMgt_s(17) & dat_downLinkWord_fromMgt_s(18) & dat_downLinkWord_fromMgt_s(19) &
                                     dat_downLinkWord_fromMgt_s(8) & dat_downLinkWord_fromMgt_s(9) & dat_downLinkWord_fromMgt_s(10) & dat_downLinkWord_fromMgt_s(11) &
                                     dat_downLinkWord_fromMgt_s(3) & dat_downLinkWord_fromMgt_s(2) & dat_downLinkWord_fromMgt_s(1) & dat_downLinkWord_fromMgt_s(0);


    --MT comment : throwing out 3 bits out 4 to ramp down from 10.12 Gbps to
    --2.56 Gbps
    dat_downLinkWord_fromMgt_s8 <=
                                   dat_downLinkWord_fromMgt_s(3) & dat_downLinkWord_fromMgt_s(7) & dat_downLinkWord_fromMgt_s(11) & dat_downLinkWord_fromMgt_s(15)
                                   & dat_downLinkWord_fromMgt_s(19) & dat_downLinkWord_fromMgt_s(23) &dat_downLinkWord_fromMgt_s(27) & dat_downLinkWord_fromMgt_s(31);

    xpm_cdc_single_inst_RX : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 8, --equivalent to 1 40 MHz clock cycle at 320 MHz
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0 --Cannot sync a clock to itself
        )
        port map (
            dest_out => RXCLK40_320,
            dest_clk => RXCLK320, --TXCLK320
            src_clk => '0',
            src_in => RXCLK40
        );

    process(RXCLK320)
    begin
        if RXCLK320'event and RXCLK320='1' then
            sta_headerFlag_s_vec <=sta_headerFlag_s_vec(8 downto 0) & sta_headerFlag_s0;
            case phase_sel is
                when "000" =>
                    sta_headerFlag_s <= sta_headerFlag_s_vec(0);
                when "001" =>
                    sta_headerFlag_s <= sta_headerFlag_s_vec(1);
                when "010" =>
                    sta_headerFlag_s <= sta_headerFlag_s_vec(2);
                when "011" =>
                    sta_headerFlag_s <= sta_headerFlag_s_vec(3);
                when "100" =>
                    sta_headerFlag_s <= sta_headerFlag_s_vec(4);
                when "101" =>
                    sta_headerFlag_s <= sta_headerFlag_s_vec(5);
                when "110" =>
                    sta_headerFlag_s <= sta_headerFlag_s_vec(6);
                when others =>
                    sta_headerFlag_s <= sta_headerFlag_s_vec(7);
            end case;
            RXCLK40_r <= RXCLK40_320;
            dat_downLinkWord_fromMgt_s64 <= dat_downLinkWord_fromMgt_s64(55 downto 0) & dat_downLinkWord_fromMgt_s8;
            --MTcomment sta_headerFlag_s=1 1 pulse every 8 (=c_wordration in mgtframealigner) clks.
            --This is because
            if sta_headerFlag_s='1' then
                dat_downLinkWord_fromGb_s_buf <= dat_downLinkWord_fromMgt_s64;
            end if;
            if sta_headeLocked_s='0' or rst_dnlink_i='1' then
                if RXCLK40_320 ='1' and RXCLK40_r='0' then
                    count_rx <="000";
                else
                    count_rx <= count_rx + '1';
                end if;
            else
                count_rx <= count_rx+'1';
            end if;
            if sta_headerFlag_s='1' then
                if count_rx="000" or count_rx="001" or count_rx="010" or count_rx="011"  then
                    sel<='0';
                else
                    sel <='1';
                end if;
            end if;
            if sta_headerFlag_s='1'  and  sel='0' then
                dat_downLinkWord_fromGb_s <=  dat_downLinkWord_fromMgt_s64;
            elsif sta_headerFlag_s_r4='1'  and  sel='1' then
                dat_downLinkWord_fromGb_s <= dat_downLinkWord_fromGb_s_buf;
            end if;
            sta_headerFlag_s_r1       <= sta_headerFlag_s;
            sta_headerFlag_s_r2       <= sta_headerFlag_s_r1;
            sta_headerFlag_s_r3       <= sta_headerFlag_s_r2;
            sta_headerFlag_s_r4       <= sta_headerFlag_s_r3;

        end if;
    end process;

    --------------------------------------------------------------------------------------
    --------------------------------------------------------------------------------------
    --------------------------------------------------------------------------------------
    ------------------------------------------TX------------------------------------------
    --------------------------------------------------------------------------------------
    --------------------------------------------------------------------------------------
    --------------------------------------------------------------------------------------

    -- Comment: Bits are inverted to transmit the MSB first on the MGT.
    frameInverter: for i in 255 downto 0 generate
        dat_upLinkWord_toGb_pipeline_s_inv(i)             <= dat_upLinkWord_toGb_pipeline_s(255-i);
    end generate;

    xpm_cdc_single_inst_TX : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 8, --equivalent to 1 40 MHz clock cycle at 320 MHz
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0 --Cannot sync a clock to itself
        )
        port map (
            dest_out => TXCLK40_320,
            dest_clk => TXCLK320, --TXCLK320
            src_clk => '0',
            src_in => TXCLK40
        );

    process(TXCLK320)
    begin
        if TXCLK320'event and TXCLK320='1' then
            TXCLK40_r <= TXCLK40_320;
            if rst_uplink_i= '1' THEN
                if TXCLK40_320 ='0' and TXCLK40_r='1' then
                    txcnt <="000";
                else
                    txcnt <=txcnt+ '1';
                end if;
            else
                txcnt <=txcnt+ '1';
            end if;

            if txcnt="010" then
                dat_upLinkWord_toGb_pipeline_s_r <=dat_upLinkWord_toGb_pipeline_s_inv;
                --MT added: needed by FELIG to latch tx payload to generated data. Need to
                --be synchronized with emulator logic
                tx_flag_out <= '1';
            --
            else
                dat_upLinkWord_toGb_pipeline_s_r <= dat_upLinkWord_toGb_pipeline_s_r;
                --MT added: needed by FELIG
                tx_flag_out <= '0';
            --
            end if;
            case txcnt is
                when "011" =>
                    dat_upLinkWord_fromGb_s <= dat_upLinkWord_toGb_pipeline_s_r(31 downto 0);
                when "100" =>
                    dat_upLinkWord_fromGb_s <= dat_upLinkWord_toGb_pipeline_s_r(63 downto 32);
                when "101" =>
                    dat_upLinkWord_fromGb_s <= dat_upLinkWord_toGb_pipeline_s_r(95 downto 64);
                when "110" =>
                    dat_upLinkWord_fromGb_s <= dat_upLinkWord_toGb_pipeline_s_r(127 downto 96);
                when "111" =>
                    dat_upLinkWord_fromGb_s <= dat_upLinkWord_toGb_pipeline_s_r(159 downto 128);
                when "000" =>
                    dat_upLinkWord_fromGb_s <= dat_upLinkWord_toGb_pipeline_s_r(191 downto 160);
                when "001" =>
                    dat_upLinkWord_fromGb_s <= dat_upLinkWord_toGb_pipeline_s_r(223 downto 192);
                when others =>
                    dat_upLinkWord_fromGb_s <= dat_upLinkWord_toGb_pipeline_s_r(255 downto 224);
            end case;
        end if;
    end process;


    upLinkData0_s       <= upLinkData(31 downto 0);
    upLinkData1_s       <= upLinkData(63 downto 32);
    upLinkData2_s       <= upLinkData(95 downto 64);
    upLinkData3_s       <= upLinkData(127 downto 96);
    upLinkData4_s       <= upLinkData(159 downto 128);
    upLinkData5_s       <= upLinkData(191 downto 160);
    upLinkData6_s       <= upLinkData(223 downto 192);
    --upLinkDataIC_s      <= upLinkDataIC;
    --upLinkDataEC_s      <= upLinkDataEC;

    -- RL replaced LpGBT_Model_dataPath with downLinkRxDataPath and upLinkTxDataPath, as in mgt_framealigner
    rxdatapath_inst : downLinkRxDataPath
        port map (
            clk                   => RXCLK40, --RXCLK320,
            downLinkFrame         => dat_downLinkWord_fromGb_s(63 downto 0),
            dataStrobe            => open,
            dataOut(15 downto  0) => downLinkDataGroup0_s,
            dataOut(31 downto 16) => downLinkDataGroup1_s,
            dataEC                => downLinkDataEc_s,
            dataIC                => downLinkDataIc_s,
            header                => open,
            dataEnable            => '1',--RX_CLKEn_s,
            bypassDeinterleaver   => downLinkBypassDeinterleaver,--'0',
            bypassFECDecoder      => downLinkBypassFECDecoder,--'0',
            bypassDescrambler     => downLinkBypassDescsrambler,--'0',
            fecCorrectionCount    => open
        );

    txdatapath_inst : upLinkTxDataPath
        port map (
            clk                   => TXCLK40,--TXCLK320
            dataEnable            => '1',--uplinkClkEn_i,
            txDataGroup0          => upLinkData0_s,
            txDataGroup1          => upLinkData1_s,
            txDataGroup2          => upLinkData2_s,
            txDataGroup3          => upLinkData3_s,
            txDataGroup4          => upLinkData4_s,
            txDataGroup5          => upLinkData5_s,
            txDataGroup6          => upLinkData6_s,
            txIC                  => upLinkDataIC,--upLinkDataIC_s,
            txEC                  => upLinkDataEC,--_s,
            txDummyFec5           => (others =>'0'),--"001100",
            txDummyFec12          => (others =>'0'),--"1001110011",
            scramblerBypass       => upLinkScramblerBypass,--'0',
            interleaverBypass     => upLinkInterleaverBypass,--'0',
            fecMode               => fecMode,
            txDataRate            => txDataRate,
            fecDisable            => '0',
            scramblerReset        => rst_uplink_i,--rst_datapath_s, rst_uplinkGb_s,
            upLinkFrame           => dat_upLinkWord_fromLpGBT_s
        );

    -- Uplink oversampler
    oversampler_gen: for i in 0 to 127 generate
        oversampler_ph_gen: for j in 0 to 1 generate
            dat_upLinkWord_toGb_pipeline_s((i*2)+j) <=  dat_upLinkWord_fromLpGBT_s(i) when txDataRate = '0' else
                                                       dat_upLinkWord_fromLpGBT_s((i*2)+j);
        end generate;
    end generate;

end Behavioral;
