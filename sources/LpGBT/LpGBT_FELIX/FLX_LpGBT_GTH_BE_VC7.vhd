--!-----------------------------------------------------------------------------
--!                                                                           --
--!           ANL - Argonne National Lboratory                                --
--!                       High Energy Physics Department                      --
--!-----------------------------------------------------------------------------
--|
--! author:      Marco Trovato (mtrovato@anl.gov)
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2021/12/07 12:40:00 PM
-- Design Name: LpGBT BE GTH Top
-- Module Name: LpGBT BE GTH Top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The LpGBT Back-End GTH Top for VC709
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
    use UNISIM.VComponents.all;

entity FLX_LpGBT_GTH_BE_VC7 is
    Port (
        GTH_RefClk                             : in std_logic;
        drpclk_in                              : in std_logic;
        gt_rxusrclk_in                         : in std_logic_vector(3 downto 0);
        gt_txusrclk_in                         : in std_logic_vector(0 downto 0);
        gt_rxoutclk_out                        : out std_logic_vector(3 downto 0);
        gt_txoutclk_out                        : out std_logic_vector(3 downto 0);
        gthrxn_in                              : in std_logic_vector(3 downto 0);
        gthrxp_in                              : in std_logic_vector(3 downto 0);
        gthtxn_out                             : out std_logic_vector(3 downto 0);
        gthtxp_out                             : out std_logic_vector(3 downto 0);

        rxpolarity_in                          : in std_logic_vector(3 downto 0);
        txpolarity_in                          : in std_logic_vector(3 downto 0);
        userdata_tx_in                         : in std_logic_vector(63 downto 0);
        userdata_rx_out                        : out std_logic_vector(127 downto 0);

        gt_txresetdone_out                         : out std_logic_vector(3 downto 0);
        gt_rxresetdone_out                      : out std_logic_vector(3 downto 0);
        gt_txfsmresetdone_out                   : out std_logic_vector(3 downto 0);
        gt_rxfsmresetdone_out                   : out std_logic_vector(3 downto 0);
        gt_cplllock_out                         : out std_logic_vector(3 downto 0);
        --toconnect once cpll implemented
        gt_rxcdrlock_out                           : out std_logic_vector(3 downto 0);
        --to connect
        gt_rxcdrhold_in                  : in std_logic_vector(3 downto 0);
        gt_qplllock_out                         : out std_logic_vector(0 downto 0);
        gt_rxslide_in                           : in std_logic_vector(3 downto 0);
        gt_txuserrdy_in                           : in std_logic_vector(3 downto 0);
        --gt_rxuserrdy_in                           : in std_logic_vector(3 downto 0);
        GTTX_RESET_IN                           : in   std_logic_vector(3 downto 0);
        GTRX_RESET_IN                             : in   std_logic_vector(3 downto 0);
        QPLL_RESET_IN                             : in   std_logic;
        SOFT_TXRST_ALL                            : in std_logic;
        SOFT_RXRST_ALL                            : in std_logic

    --compare with kai and check if all signals are connected




    );
end FLX_LpGBT_GTH_BE_VC7;

architecture Behavioral of FLX_LpGBT_GTH_BE_VC7 is

    component VC7_PMA_QPLL_4CH_LPGBT
        port
(
            SYSCLK_IN                               : in   std_logic;
            SOFT_RESET_TX_IN                        : in   std_logic;
            SOFT_RESET_RX_IN                        : in   std_logic;
            DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
            GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
            GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
            GT0_DATA_VALID_IN                       : in   std_logic;
            GT1_TX_FSM_RESET_DONE_OUT               : out  std_logic;
            GT1_RX_FSM_RESET_DONE_OUT               : out  std_logic;
            GT1_DATA_VALID_IN                       : in   std_logic;
            GT2_TX_FSM_RESET_DONE_OUT               : out  std_logic;
            GT2_RX_FSM_RESET_DONE_OUT               : out  std_logic;
            GT2_DATA_VALID_IN                       : in   std_logic;
            GT3_TX_FSM_RESET_DONE_OUT               : out  std_logic;
            GT3_RX_FSM_RESET_DONE_OUT               : out  std_logic;
            GT3_DATA_VALID_IN                       : in   std_logic;

            --_________________________________________________________________________
            --GT0  (X0Y4)
            --____________________________CHANNEL PORTS________________________________
            ---------------------------- Channel - DRP Ports  --------------------------
            gt0_drpaddr_in                          : in   std_logic_vector(8 downto 0);
            gt0_drpclk_in                           : in   std_logic;
            gt0_drpdi_in                            : in   std_logic_vector(15 downto 0);
            gt0_drpdo_out                           : out  std_logic_vector(15 downto 0);
            gt0_drpen_in                            : in   std_logic;
            gt0_drprdy_out                          : out  std_logic;
            gt0_drpwe_in                            : in   std_logic;
            --------------------- RX Initialization and Reset Ports --------------------
            gt0_eyescanreset_in                     : in   std_logic;
            gt0_rxuserrdy_in                        : in   std_logic;
            -------------------------- RX Margin Analysis Ports ------------------------
            gt0_eyescandataerror_out                : out  std_logic;
            gt0_eyescantrigger_in                   : in   std_logic;
            ------------------------- Receive Ports - CDR Ports ------------------------
            gt0_rxcdrhold_in                        : in   std_logic;
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt0_rxslide_in                          : in   std_logic;
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt0_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt0_rxusrclk_in                         : in   std_logic;
            gt0_rxusrclk2_in                        : in   std_logic;
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt0_rxdata_out                          : out  std_logic_vector(31 downto 0);
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt0_gthrxn_in                           : in   std_logic;
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            gt0_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
            gt0_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt0_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
            gt0_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt0_rxoutclk_out                        : out  std_logic;
            gt0_rxoutclkfabric_out                  : out  std_logic;
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt0_gtrxreset_in                        : in   std_logic;
            ----------------- Receive Ports - RX Polarity Control Ports ----------------
            gt0_rxpolarity_in                       : in   std_logic;
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt0_gthrxp_in                           : in   std_logic;
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt0_rxresetdone_out                     : out  std_logic;
            --------------------- TX Initialization and Reset Ports --------------------
            gt0_gttxreset_in                        : in   std_logic;
            gt0_txuserrdy_in                        : in   std_logic;
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt0_txusrclk_in                         : in   std_logic;
            gt0_txusrclk2_in                        : in   std_logic;
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt0_txdata_in                           : in   std_logic_vector(15 downto 0);
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt0_gthtxn_out                          : out  std_logic;
            gt0_gthtxp_out                          : out  std_logic;
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt0_txoutclk_out                        : out  std_logic;
            gt0_txoutclkfabric_out                  : out  std_logic;
            gt0_txoutclkpcs_out                     : out  std_logic;
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt0_txresetdone_out                     : out  std_logic;
            ----------------- Transmit Ports - TX Polarity Control Ports ---------------
            gt0_txpolarity_in                       : in   std_logic;

            --GT1  (X0Y5)
            --____________________________CHANNEL PORTS________________________________
            ---------------------------- Channel - DRP Ports  --------------------------
            gt1_drpaddr_in                          : in   std_logic_vector(8 downto 0);
            gt1_drpclk_in                           : in   std_logic;
            gt1_drpdi_in                            : in   std_logic_vector(15 downto 0);
            gt1_drpdo_out                           : out  std_logic_vector(15 downto 0);
            gt1_drpen_in                            : in   std_logic;
            gt1_drprdy_out                          : out  std_logic;
            gt1_drpwe_in                            : in   std_logic;
            --------------------- RX Initialization and Reset Ports --------------------
            gt1_eyescanreset_in                     : in   std_logic;
            gt1_rxuserrdy_in                        : in   std_logic;
            -------------------------- RX Margin Analysis Ports ------------------------
            gt1_eyescandataerror_out                : out  std_logic;
            gt1_eyescantrigger_in                   : in   std_logic;
            ------------------------- Receive Ports - CDR Ports ------------------------
            gt1_rxcdrhold_in                        : in   std_logic;
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt1_rxslide_in                          : in   std_logic;
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt1_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt1_rxusrclk_in                         : in   std_logic;
            gt1_rxusrclk2_in                        : in   std_logic;
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt1_rxdata_out                          : out  std_logic_vector(31 downto 0);
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt1_gthrxn_in                           : in   std_logic;
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            gt1_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
            gt1_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt1_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
            gt1_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt1_rxoutclk_out                        : out  std_logic;
            gt1_rxoutclkfabric_out                  : out  std_logic;
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt1_gtrxreset_in                        : in   std_logic;
            ----------------- Receive Ports - RX Polarity Control Ports ----------------
            gt1_rxpolarity_in                       : in   std_logic;
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt1_gthrxp_in                           : in   std_logic;
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt1_rxresetdone_out                     : out  std_logic;
            --------------------- TX Initialization and Reset Ports --------------------
            gt1_gttxreset_in                        : in   std_logic;
            gt1_txuserrdy_in                        : in   std_logic;
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt1_txusrclk_in                         : in   std_logic;
            gt1_txusrclk2_in                        : in   std_logic;
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt1_txdata_in                           : in   std_logic_vector(15 downto 0);
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt1_gthtxn_out                          : out  std_logic;
            gt1_gthtxp_out                          : out  std_logic;
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt1_txoutclk_out                        : out  std_logic;
            gt1_txoutclkfabric_out                  : out  std_logic;
            gt1_txoutclkpcs_out                     : out  std_logic;
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt1_txresetdone_out                     : out  std_logic;
            ----------------- Transmit Ports - TX Polarity Control Ports ---------------
            gt1_txpolarity_in                       : in   std_logic;

            --GT2  (X0Y6)
            --____________________________CHANNEL PORTS________________________________
            ---------------------------- Channel - DRP Ports  --------------------------
            gt2_drpaddr_in                          : in   std_logic_vector(8 downto 0);
            gt2_drpclk_in                           : in   std_logic;
            gt2_drpdi_in                            : in   std_logic_vector(15 downto 0);
            gt2_drpdo_out                           : out  std_logic_vector(15 downto 0);
            gt2_drpen_in                            : in   std_logic;
            gt2_drprdy_out                          : out  std_logic;
            gt2_drpwe_in                            : in   std_logic;
            --------------------- RX Initialization and Reset Ports --------------------
            gt2_eyescanreset_in                     : in   std_logic;
            gt2_rxuserrdy_in                        : in   std_logic;
            -------------------------- RX Margin Analysis Ports ------------------------
            gt2_eyescandataerror_out                : out  std_logic;
            gt2_eyescantrigger_in                   : in   std_logic;
            ------------------------- Receive Ports - CDR Ports ------------------------
            gt2_rxcdrhold_in                        : in   std_logic;
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt2_rxslide_in                          : in   std_logic;
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt2_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt2_rxusrclk_in                         : in   std_logic;
            gt2_rxusrclk2_in                        : in   std_logic;
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt2_rxdata_out                          : out  std_logic_vector(31 downto 0);
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt2_gthrxn_in                           : in   std_logic;
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            gt2_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
            gt2_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt2_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
            gt2_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt2_rxoutclk_out                        : out  std_logic;
            gt2_rxoutclkfabric_out                  : out  std_logic;
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt2_gtrxreset_in                        : in   std_logic;
            ----------------- Receive Ports - RX Polarity Control Ports ----------------
            gt2_rxpolarity_in                       : in   std_logic;
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt2_gthrxp_in                           : in   std_logic;
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt2_rxresetdone_out                     : out  std_logic;
            --------------------- TX Initialization and Reset Ports --------------------
            gt2_gttxreset_in                        : in   std_logic;
            gt2_txuserrdy_in                        : in   std_logic;
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt2_txusrclk_in                         : in   std_logic;
            gt2_txusrclk2_in                        : in   std_logic;
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt2_txdata_in                           : in   std_logic_vector(15 downto 0);
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt2_gthtxn_out                          : out  std_logic;
            gt2_gthtxp_out                          : out  std_logic;
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt2_txoutclk_out                        : out  std_logic;
            gt2_txoutclkfabric_out                  : out  std_logic;
            gt2_txoutclkpcs_out                     : out  std_logic;
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt2_txresetdone_out                     : out  std_logic;
            ----------------- Transmit Ports - TX Polarity Control Ports ---------------
            gt2_txpolarity_in                       : in   std_logic;

            --GT3  (X0Y7)
            --____________________________CHANNEL PORTS________________________________
            ---------------------------- Channel - DRP Ports  --------------------------
            gt3_drpaddr_in                          : in   std_logic_vector(8 downto 0);
            gt3_drpclk_in                           : in   std_logic;
            gt3_drpdi_in                            : in   std_logic_vector(15 downto 0);
            gt3_drpdo_out                           : out  std_logic_vector(15 downto 0);
            gt3_drpen_in                            : in   std_logic;
            gt3_drprdy_out                          : out  std_logic;
            gt3_drpwe_in                            : in   std_logic;
            --------------------- RX Initialization and Reset Ports --------------------
            gt3_eyescanreset_in                     : in   std_logic;
            gt3_rxuserrdy_in                        : in   std_logic;
            -------------------------- RX Margin Analysis Ports ------------------------
            gt3_eyescandataerror_out                : out  std_logic;
            gt3_eyescantrigger_in                   : in   std_logic;
            ------------------------- Receive Ports - CDR Ports ------------------------
            gt3_rxcdrhold_in                        : in   std_logic;
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt3_rxslide_in                          : in   std_logic;
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt3_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt3_rxusrclk_in                         : in   std_logic;
            gt3_rxusrclk2_in                        : in   std_logic;
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt3_rxdata_out                          : out  std_logic_vector(31 downto 0);
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt3_gthrxn_in                           : in   std_logic;
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            gt3_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
            gt3_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt3_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
            gt3_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt3_rxoutclk_out                        : out  std_logic;
            gt3_rxoutclkfabric_out                  : out  std_logic;
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt3_gtrxreset_in                        : in   std_logic;
            ----------------- Receive Ports - RX Polarity Control Ports ----------------
            gt3_rxpolarity_in                       : in   std_logic;
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt3_gthrxp_in                           : in   std_logic;
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt3_rxresetdone_out                     : out  std_logic;
            --------------------- TX Initialization and Reset Ports --------------------
            gt3_gttxreset_in                        : in   std_logic;
            gt3_txuserrdy_in                        : in   std_logic;
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt3_txusrclk_in                         : in   std_logic;
            gt3_txusrclk2_in                        : in   std_logic;
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt3_txdata_in                           : in   std_logic_vector(15 downto 0);
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt3_gthtxn_out                          : out  std_logic;
            gt3_gthtxp_out                          : out  std_logic;
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt3_txoutclk_out                        : out  std_logic;
            gt3_txoutclkfabric_out                  : out  std_logic;
            gt3_txoutclkpcs_out                     : out  std_logic;
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt3_txresetdone_out                     : out  std_logic;
            ----------------- Transmit Ports - TX Polarity Control Ports ---------------
            gt3_txpolarity_in                       : in   std_logic;


            --____________________________COMMON PORTS________________________________
            GT0_QPLLLOCK_IN : in std_logic;
            GT0_QPLLREFCLKLOST_IN  : in std_logic;
            GT0_QPLLRESET_OUT  : out std_logic;
            GT0_QPLLOUTCLK_IN  : in std_logic;
            GT0_QPLLOUTREFCLK_IN : in std_logic

        );
    end component;


    --signal rxusrclk               : std_logic_vector(3 downto 0);
    --signal rxusrclk_int           : std_logic_vector(3 downto 0);
    --signal rxusrclk2_int          : std_logic_vector(3 downto 0);
    --signal txoutclk_int           : std_logic_vector(3 downto 0);
    --signal rxoutclk_int           : std_logic_vector(3 downto 0);
    --signal txusrclk_int           : std_logic_vector(3 downto 0);
    --signal txusrclk2_int          : std_logic_vector(3 downto 0);
    --signal txusrclk               : std_logic;
    --signal gndvec                 : std_logic_vector(0 downto 0);
    --  signal drpclk_i               : std_logic_vector(3 downto 0);
    signal rxcdrhold_i            : std_logic_vector(3 downto 0); -- @suppress "signal rxcdrhold_i is never read"
    signal gt_gttxreset_i         : std_logic_vector(3 downto 0);
    signal gt_gtrxreset_i         : std_logic_vector(3 downto 0);
    signal GT0_QPLLLOCK_I         : std_logic;
    signal GT0_QPLLOUTCLK_I       : std_logic;
    signal GT0_QPLLOUTREFCLK_I    : std_logic;
    signal GT0_QPLLREFCLKLOST_I   : std_logic;
    signal GT0_QPLLRESET_I        : std_logic;

    signal  tied_to_vcc_i                : std_logic;
    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(15 downto 0);

begin

    tied_to_vcc_i                                <= '1';
    tied_to_ground_i                             <= '0';
    tied_to_ground_vec_i                         <= x"0000";

    gt_qplllock_out(0) <= GT0_QPLLLOCK_I;

    -- drpclk_i <= drpclk_in & drpclk_in &  drpclk_in & drpclk_in;
    rxcdrhold_i <= gt_rxcdrhold_in;
    gt_cplllock_out  <= (others => GT0_QPLLLOCK_I);
    gt_rxcdrlock_out <= (others => GT0_QPLLLOCK_I);



    gt_gttxreset_i(0) <= GTTX_RESET_IN(0) or (not GT0_QPLLLOCK_I);
    gt_gttxreset_i(1) <= GTTX_RESET_IN(1) or (not GT0_QPLLLOCK_I);
    gt_gttxreset_i(2) <= GTTX_RESET_IN(2) or (not GT0_QPLLLOCK_I);
    gt_gttxreset_i(3) <= GTTX_RESET_IN(3) or (not GT0_QPLLLOCK_I);

    gt_gtrxreset_i(0) <= GTRX_RESET_IN(0) or (not GT0_QPLLLOCK_I);
    gt_gtrxreset_i(1) <= GTRX_RESET_IN(1) or (not GT0_QPLLLOCK_I);
    gt_gtrxreset_i(2) <= GTRX_RESET_IN(2) or (not GT0_QPLLLOCK_I);
    gt_gtrxreset_i(3) <= GTRX_RESET_IN(3) or (not GT0_QPLLLOCK_I);

    --missing IS_DRPCLK_INVERTED, IS_GTGREFCLK_INVERTED, IS_QPLLLOCKDETCLK_INVERTED probably only for simu. Anyhow ug476 doesn't specify it in appendix D, so it's probably goo to leave as defaults
    --missing RSVD_ATTR0, RSVD_ATTR1. Probably ok, since xilinx will use defaults
    gthe2_common_i : GTHE2_COMMON
        generic map -- @suppress "Generic map uses default values. Missing optional actuals: IS_DRPCLK_INVERTED, IS_GTGREFCLK_INVERTED, IS_QPLLLOCKDETCLK_INVERTED"
    (
            -- Simulation attributes
            SIM_RESET_SPEEDUP    => ("FALSE"),
            SIM_QPLLREFCLK_SEL   => ("010"), --same as QPLLREFCLKSEL
            SIM_VERSION          => ("2.0"),

            ------------------COMMON BLOCK Attributes---------------
            BIAS_CFG                                =>     (x"0000040000001050"),
            COMMON_CFG                              =>     (x"0000005C"), --reserved
            QPLL_CFG                                =>     (x"04801C7"),
            QPLL_CLKOUT_CFG                         =>     ("1111"),
            QPLL_COARSE_FREQ_OVRD                   =>     ("010000"),
            QPLL_COARSE_FREQ_OVRD_EN                =>     ('0'),
            QPLL_CP                                 =>     ("0000011111"),
            QPLL_CP_MONITOR_EN                      =>     ('0'),
            QPLL_DMONITOR_SEL                       =>     ('0'),
            QPLL_FBDIV                              =>     "0001100000",
            --"0010000000" kai which is for 40b while IP is for 20: why?
            QPLL_FBDIV_MONITOR_EN                   =>     ('0'),
            QPLL_FBDIV_RATIO                        =>     '1',
            QPLL_INIT_CFG                           =>     (x"000006"),
            QPLL_LOCK_CFG                           =>     (x"05E8"),
            QPLL_LPF                                =>     ("1111"),
            QPLL_REFCLK_DIV                         =>     (1),
            RSVD_ATTR0                              =>     (x"0000"),
            RSVD_ATTR1                              =>     (x"0000"),
            QPLL_RP_COMP                            =>     ('0'),
            QPLL_VTRL_RESET                         =>     ("00"),
            RCAL_CFG                                =>     ("00")
        )
        port map
    (
            DRPDO => open,
            DRPRDY => open,
            PMARSVDOUT => open,
            ------------------------- Common Block -  QPLL Ports -----------------------
            QPLLDMONITOR => open,
            QPLLFBCLKLOST => open,
            QPLLLOCK => GT0_QPLLLOCK_I,
            ----------------------- Common Block - Clocking Ports ----------------------
            QPLLOUTCLK => GT0_QPLLOUTCLK_I,
            QPLLOUTREFCLK => GT0_QPLLOUTREFCLK_I,
            QPLLREFCLKLOST => GT0_QPLLREFCLKLOST_I,
            REFCLKOUTMONITOR => open,
            --------------------------------- QPLL Ports -------------------------------
            BGBYPASSB => tied_to_vcc_i,
            BGMONITORENB => tied_to_vcc_i,
            BGPDB => tied_to_vcc_i,
            BGRCALOVRD => "11111", --reserved must be set--to "11111"
            ------------------------- Common Block - QPLL Ports ------------------------
            BGRCALOVRDENB => tied_to_vcc_i,
            ------------- Common Block  - Dynamic Reconfiguration Port (DRP) -----------
            DRPADDR => tied_to_ground_vec_i(7 downto 0),
            DRPCLK => tied_to_ground_i,
            DRPDI => tied_to_ground_vec_i(15 downto 0),
            DRPEN => tied_to_ground_i,
            DRPWE => tied_to_ground_i,
            ---------------------- Common Block  - Ref Clock Ports ---------------------
            GTGREFCLK => tied_to_ground_i,
            GTNORTHREFCLK0 => tied_to_ground_i,
            GTNORTHREFCLK1 => tied_to_ground_i,
            GTREFCLK0 => tied_to_ground_i,
            GTREFCLK1 => GTH_RefClk, --different than kai--compare against--direct reodout 64b66b
            GTSOUTHREFCLK0 => tied_to_ground_i,
            GTSOUTHREFCLK1 => tied_to_ground_i,
            PMARSVD => "00000000",
            QPLLLOCKDETCLK => drpclk_in,
            QPLLLOCKEN => tied_to_vcc_i,
            QPLLOUTRESET => tied_to_ground_i,
            QPLLPD => tied_to_ground_i,
            QPLLREFCLKSEL => "010", --GTREFCLK1 as in .xdc
            QPLLRESET => GT0_QPLLRESET_I or QPLL_RESET_IN,
            QPLLRSVD1 => "0000000000000000",
            QPLLRSVD2 => "11111",
            RCALENB => tied_to_vcc_i
        );

    VC7_PMA_QPLL_4CH_LPGBT_inst: VC7_PMA_QPLL_4CH_LPGBT
        port map
(
            SYSCLK_IN                       => drpclk_in,
            SOFT_RESET_TX_IN                => SOFT_TXRST_ALL,
            SOFT_RESET_RX_IN                => SOFT_RXRST_ALL,
            DONT_RESET_ON_DATA_ERROR_IN     => '1',
            GT0_TX_FSM_RESET_DONE_OUT       => gt_txfsmresetdone_out(0),
            GT0_RX_FSM_RESET_DONE_OUT       => gt_rxfsmresetdone_out(0),
            GT0_DATA_VALID_IN               => '1',
            GT1_TX_FSM_RESET_DONE_OUT       => gt_txfsmresetdone_out(1),
            GT1_RX_FSM_RESET_DONE_OUT       => gt_rxfsmresetdone_out(1),
            GT1_DATA_VALID_IN               => '1',
            GT2_TX_FSM_RESET_DONE_OUT       => gt_txfsmresetdone_out(2),
            GT2_RX_FSM_RESET_DONE_OUT       => gt_rxfsmresetdone_out(2),
            GT2_DATA_VALID_IN               => '1',
            GT3_TX_FSM_RESET_DONE_OUT       => gt_txfsmresetdone_out(3),
            GT3_RX_FSM_RESET_DONE_OUT       => gt_rxfsmresetdone_out(3),
            GT3_DATA_VALID_IN               => '1',

            --_________________________________________________________________________
            --GT0  (X0Y4)
            --____________________________CHANNEL PORTS________________________________
            ---------------------------- Channel - DRP Ports  --------------------------
            gt0_drpaddr_in                  => (others => '0'),
            gt0_drpclk_in                   => drpclk_in,
            gt0_drpdi_in                    => (others => '0'),
            gt0_drpdo_out                   => open,
            gt0_drpen_in                    => '0',
            gt0_drprdy_out                  => open,
            gt0_drpwe_in                    => '0',
            --------------------- RX Initialization and Reset Ports --------------------
            gt0_eyescanreset_in             => '0',
            gt0_rxuserrdy_in                => '1', --TO DO: as in GTH_QPLL_Wrapper_V7 double check
            -------------------------- RX Margin Analysis Ports ------------------------
            gt0_eyescandataerror_out        => open,
            gt0_eyescantrigger_in           => '0',
            ------------------------- Receive Ports - CDR Ports ------------------------
            gt0_rxcdrhold_in                => gt_rxcdrhold_in(0),
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt0_rxslide_in                  => gt_rxslide_in(0),
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt0_dmonitorout_out             => open,
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt0_rxusrclk_in                 => gt_rxusrclk_in(0),
            gt0_rxusrclk2_in                => gt_rxusrclk_in(0),
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt0_rxdata_out                  => userdata_rx_out(31 downto 0),
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt0_gthrxn_in                   => gthrxn_in(0),
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            gt0_rxphmonitor_out             => open,
            gt0_rxphslipmonitor_out         => open,
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt0_rxmonitorout_out            => open,
            gt0_rxmonitorsel_in             => "00",
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt0_rxoutclk_out                => gt_rxoutclk_out(0),
            gt0_rxoutclkfabric_out          => open,
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt0_gtrxreset_in                => gt_gtrxreset_i(0),
            ----------------- Receive Ports - RX Polarity Control Ports ----------------
            gt0_rxpolarity_in               => rxpolarity_in(0),
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt0_gthrxp_in                   => gthrxp_in(0),
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt0_rxresetdone_out             => gt_rxresetdone_out(0),
            --------------------- TX Initialization and Reset Ports --------------------
            gt0_gttxreset_in                => gt_gttxreset_i(0),
            gt0_txuserrdy_in                => gt_txuserrdy_in(0),
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt0_txusrclk_in                 => gt_txusrclk_in(0),
            gt0_txusrclk2_in                => gt_txusrclk_in(0),
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt0_txdata_in                   => userdata_tx_in(15 downto 0),
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt0_gthtxn_out                  => gthtxn_out(0),
            gt0_gthtxp_out                  => gthtxp_out(0),
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt0_txoutclk_out                => gt_txoutclk_out(0),
            gt0_txoutclkfabric_out          => open,
            gt0_txoutclkpcs_out             => open,
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt0_txresetdone_out             => gt_txresetdone_out(0),
            ----------------- Transmit Ports - TX Polarity Control Ports ---------------
            gt0_txpolarity_in               => txpolarity_in(0),
            --GT1  (X0Y5)
            --____________________________CHANNEL PORTS________________________________
            ---------------------------- Channel - DRP Ports  --------------------------
            gt1_drpaddr_in                  => (others => '0'),
            gt1_drpclk_in                   => drpclk_in,
            gt1_drpdi_in                    => (others => '0'),
            gt1_drpdo_out                   => open,
            gt1_drpen_in                    => '0',
            gt1_drprdy_out                  => open,
            gt1_drpwe_in                    => '0',
            --------------------- RX Initialization and Reset Ports --------------------
            gt1_eyescanreset_in             => '0',
            gt1_rxuserrdy_in                => '1',
            -------------------------- RX Margin Analysis Ports ------------------------
            gt1_eyescandataerror_out        => open,
            gt1_eyescantrigger_in           => '0',
            ------------------------- Receive Ports - CDR Ports ------------------------
            gt1_rxcdrhold_in                => gt_rxcdrhold_in(1),
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt1_rxslide_in                  => gt_rxslide_in(1),
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt1_dmonitorout_out             => open,
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt1_rxusrclk_in                 => gt_rxusrclk_in(1),
            gt1_rxusrclk2_in                => gt_rxusrclk_in(1),
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt1_rxdata_out                  => userdata_rx_out(63 downto 32),
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt1_gthrxn_in                   => gthrxn_in(1),
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            gt1_rxphmonitor_out             => open,
            gt1_rxphslipmonitor_out         => open,
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt1_rxmonitorout_out            => open,
            gt1_rxmonitorsel_in             => "00",
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt1_rxoutclk_out                => gt_rxoutclk_out(1),
            gt1_rxoutclkfabric_out          => open,
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt1_gtrxreset_in                => gt_gtrxreset_i(1),
            ----------------- Receive Ports - RX Polarity Control Ports ----------------
            gt1_rxpolarity_in               => rxpolarity_in(1),
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt1_gthrxp_in                   => gthrxp_in(1),
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt1_rxresetdone_out             => gt_rxresetdone_out(1),
            --------------------- TX Initialization and Reset Ports --------------------
            gt1_gttxreset_in                => gt_gttxreset_i(1),
            gt1_txuserrdy_in                => gt_txuserrdy_in(1),
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt1_txusrclk_in                 => gt_txusrclk_in(0),
            gt1_txusrclk2_in                => gt_txusrclk_in(0),
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt1_txdata_in                   => userdata_tx_in(31 downto 16),
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt1_gthtxn_out                  => gthtxn_out(1),
            gt1_gthtxp_out                  => gthtxp_out(1),
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt1_txoutclk_out                => gt_txoutclk_out(1),
            gt1_txoutclkfabric_out          => open,
            gt1_txoutclkpcs_out             => open,
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt1_txresetdone_out             => gt_txresetdone_out(1),
            ----------------- Transmit Ports - TX Polarity Control Ports ---------------
            gt1_txpolarity_in               => txpolarity_in(1),

            --GT2  (X0Y6)
            --____________________________CHANNEL PORTS________________________________
            ---------------------------- Channel - DRP Ports  --------------------------
            gt2_drpaddr_in                  => (others => '0'),
            gt2_drpclk_in                   => drpclk_in,
            gt2_drpdi_in                    => (others => '0'),
            gt2_drpdo_out                   => open,
            gt2_drpen_in                    => '0',
            gt2_drprdy_out                  => open,
            gt2_drpwe_in                    => '0',
            --------------------- RX Initialization and Reset Ports --------------------
            gt2_eyescanreset_in             => '0',
            gt2_rxuserrdy_in                => '1',
            -------------------------- RX Margin Analysis Ports ------------------------
            gt2_eyescandataerror_out        => open,
            gt2_eyescantrigger_in           => '0',
            ------------------------- Receive Ports - CDR Ports ------------------------
            gt2_rxcdrhold_in                => gt_rxcdrhold_in(2),
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt2_rxslide_in                  => gt_rxslide_in(2),
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt2_dmonitorout_out             => open,
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt2_rxusrclk_in                 => gt_rxusrclk_in(2),
            gt2_rxusrclk2_in                => gt_rxusrclk_in(2),
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt2_rxdata_out                  => userdata_rx_out(95 downto 64),
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt2_gthrxn_in                   => gthrxn_in(2),
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            gt2_rxphmonitor_out             => open,
            gt2_rxphslipmonitor_out         => open,
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt2_rxmonitorout_out            => open,
            gt2_rxmonitorsel_in             => "00",
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt2_rxoutclk_out                => gt_rxoutclk_out(2),
            gt2_rxoutclkfabric_out          => open,
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt2_gtrxreset_in                => gt_gtrxreset_i(2),
            ----------------- Receive Ports - RX Polarity Control Ports ----------------
            gt2_rxpolarity_in               => rxpolarity_in(2),
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt2_gthrxp_in                   => gthrxp_in(2),
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt2_rxresetdone_out             => gt_rxresetdone_out(2),
            --------------------- TX Initialization and Reset Ports --------------------
            gt2_gttxreset_in                => gt_gttxreset_i(2),
            gt2_txuserrdy_in                => gt_txuserrdy_in(2),
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt2_txusrclk_in                 => gt_txusrclk_in(0),
            gt2_txusrclk2_in                => gt_txusrclk_in(0),
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt2_txdata_in                   => userdata_tx_in(47 downto 32),
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt2_gthtxn_out                  => gthtxn_out(2),
            gt2_gthtxp_out                  => gthtxp_out(2),
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt2_txoutclk_out                => gt_txoutclk_out(2),
            gt2_txoutclkfabric_out          => open,
            gt2_txoutclkpcs_out             => open,
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt2_txresetdone_out             => gt_txresetdone_out(2),
            ----------------- Transmit Ports - TX Polarity Control Ports ---------------
            gt2_txpolarity_in               => txpolarity_in(2),

            --GT3  (X0Y7)
            --____________________________CHANNEL PORTS________________________________
            ---------------------------- Channel - DRP Ports  --------------------------
            gt3_drpaddr_in                  => (others => '0'),
            gt3_drpclk_in                   => drpclk_in,
            gt3_drpdi_in                    => (others => '0'),
            gt3_drpdo_out                   => open,
            gt3_drpen_in                    => '0',
            gt3_drprdy_out                  => open,
            gt3_drpwe_in                    => '0',
            --------------------- RX Initialization and Reset Ports --------------------
            gt3_eyescanreset_in             => '0',
            gt3_rxuserrdy_in                => '1',
            -------------------------- RX Margin Analysis Ports ------------------------
            gt3_eyescandataerror_out        => open,
            gt3_eyescantrigger_in           => '0',
            ------------------------- Receive Ports - CDR Ports ------------------------
            gt3_rxcdrhold_in                => gt_rxcdrhold_in(3),
            --------------- Receive Ports - Comma Detection and Alignment --------------
            gt3_rxslide_in                  => gt_rxslide_in(3),
            ------------------- Receive Ports - Digital Monitor Ports ------------------
            gt3_dmonitorout_out             => open,
            ------------------ Receive Ports - FPGA RX Interface Ports -----------------
            gt3_rxusrclk_in                 => gt_rxusrclk_in(3),
            gt3_rxusrclk2_in                => gt_rxusrclk_in(3),
            ------------------ Receive Ports - FPGA RX interface Ports -----------------
            gt3_rxdata_out                  => userdata_rx_out(127 downto 96),
            ------------------------ Receive Ports - RX AFE Ports ----------------------
            gt3_gthrxn_in                   => gthrxn_in(3),
            ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
            gt3_rxphmonitor_out             => open,
            gt3_rxphslipmonitor_out         => open,
            --------------------- Receive Ports - RX Equalizer Ports -------------------
            gt3_rxmonitorout_out            => open,
            gt3_rxmonitorsel_in             => "00",
            --------------- Receive Ports - RX Fabric Output Control Ports -------------
            gt3_rxoutclk_out                => gt_rxoutclk_out(3),
            gt3_rxoutclkfabric_out          => open,
            ------------- Receive Ports - RX Initialization and Reset Ports ------------
            gt3_gtrxreset_in                => gt_gtrxreset_i(3),
            ----------------- Receive Ports - RX Polarity Control Ports ----------------
            gt3_rxpolarity_in               => rxpolarity_in(3),
            ------------------------ Receive Ports -RX AFE Ports -----------------------
            gt3_gthrxp_in                   => gthrxp_in(3),
            -------------- Receive Ports -RX Initialization and Reset Ports ------------
            gt3_rxresetdone_out             => gt_rxresetdone_out(3),
            --------------------- TX Initialization and Reset Ports --------------------
            gt3_gttxreset_in                => gt_gttxreset_i(3),
            gt3_txuserrdy_in                => gt_txuserrdy_in(3),
            ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
            gt3_txusrclk_in                 => gt_txusrclk_in(0),
            gt3_txusrclk2_in                => gt_txusrclk_in(0),
            ------------------ Transmit Ports - TX Data Path interface -----------------
            gt3_txdata_in                   => userdata_tx_in(63 downto 48),
            ---------------- Transmit Ports - TX Driver and OOB signaling --------------
            gt3_gthtxn_out                  => gthtxn_out(3),
            gt3_gthtxp_out                  => gthtxp_out(3),
            ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
            gt3_txoutclk_out                => gt_txoutclk_out(3),
            gt3_txoutclkfabric_out          => open,
            gt3_txoutclkpcs_out             => open,
            ------------- Transmit Ports - TX Initialization and Reset Ports -----------
            gt3_txresetdone_out             => gt_txresetdone_out(3),
            ----------------- Transmit Ports - TX Polarity Control Ports ---------------
            gt3_txpolarity_in               => txpolarity_in(3),


            --____________________________COMMON PORTS________________________________
            GT0_QPLLLOCK_IN => GT0_QPLLLOCK_I,
            GT0_QPLLREFCLKLOST_IN => GT0_QPLLREFCLKLOST_I,
            GT0_QPLLRESET_OUT => GT0_QPLLRESET_I,
            GT0_QPLLOUTCLK_IN  => GT0_QPLLOUTCLK_I,
            GT0_QPLLOUTREFCLK_IN => GT0_QPLLOUTREFCLK_I


        );


end Behavioral;
