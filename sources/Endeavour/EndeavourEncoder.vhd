--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Jacopo Pinzino
--!               jacopo pinzino
--!               Elena Zhivun
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: jacopo pinzino, Elena Zhivun <ezhivun@bnl.gov>
--
-- Create Date: 11/06/2019 11:51:07 AM
-- Design Name:
-- Module Name: EndeavourEncoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

    use work.endeavour_package.all;
    use work.pcie_package.all;
    use work.centralRouter_package.all;

entity EndeavourEncoder is
    generic (
        DEBUG_en : boolean := false;
        DISTR_RAM : boolean := false;
        USE_BUILT_IN_FIFO : std_logic := '0'
    );
    port (
        clk40 : in std_logic;
        LinkAligned : in std_logic; -- @suppress "Unused port: LinkAligned is not used in work.EndeavourEncoder(Behavioral)"
        s_axis_aclk : in std_logic;
        aresetn : in std_logic;
        rst     : in std_logic;
        s_axis : in axis_8_type;
        s_axis_tready : out std_logic;
        invert_polarity : in std_logic; -- invert link polatiry
        amac_signal : out std_logic;
        almost_full : out std_logic
    );
end EndeavourEncoder;

architecture Behavioral of EndeavourEncoder is

    type t_state is (s_idle, s_next_bit, s_send_bit, s_bit_gap, s_word_gap);

    signal state : t_state := s_idle;

    signal m_axis, s_axis_in, m_axis_out : axis_8_type;   -- @suppress "signal m_axis_out is never read"
    signal m_axis_tready, amac_out : std_logic;

    signal bits_to_send: std_logic_vector(7 downto 0) := (others => '0');
    signal bits_left : integer range 0 to 8 := 0;
    signal ticks_left : integer range 0 to 127 := 0;
    signal end_of_chunk : boolean := false;

    signal rst_probe : std_logic_vector(0 downto 0); -- @suppress "signal rst_probe is never read"
    signal amac_signal_probe : std_logic_vector(0 downto 0); -- @suppress "signal amac_signal_probe is never read"
    signal m_axis_tvalid_probe : std_logic_vector(0 downto 0); -- @suppress "signal m_axis_tvalid_probe is never read"
    signal m_axis_tready_probe : std_logic_vector(0 downto 0); -- @suppress "signal m_axis_tready_probe is never read"
    signal amac_signal_s : std_logic;

--component ila_endeavour_encoder
--  PORT(
--    clk    : IN std_logic;
--    probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
--    probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
--  );
--END component ila_endeavour_encoder;

begin

    ila_probes_gen : if DEBUG_en generate
        rst_probe(0) <= rst;
        amac_signal_probe(0) <= amac_signal_s;
        m_axis_tvalid_probe(0) <= m_axis.tvalid;
        m_axis_tready_probe(0) <= m_axis_tready;
    --ila_probe: ila_endeavour_encoder
    --port map (
    --  clk => clk40,
    --  probe0 => rst_probe,
    --  probe1 => amac_signal_probe,
    --  probe2 => m_axis_out.tdata,
    --  probe3 => m_axis_tvalid_probe,
    --  probe4 => m_axis_tready_probe
    --);
    end generate ila_probes_gen;


    fifoaxi8: entity work.Axis8Fifo
        generic map(
            --DEPTH => 512,
            --CLOCKING_MODE => "independent_clocks",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            DISTR_RAM => DISTR_RAM
        )
        port map(
            s_axis_aresetn => aresetn,
            s_axis_aclk => s_axis_aclk,
            s_axis => s_axis_in,
            s_axis_tready => s_axis_tready,
            m_axis_aclk => clk40,
            m_axis => m_axis,
            m_axis_tready => m_axis_tready,
            almost_full => almost_full
        );

    amac_signal <= amac_signal_s;
    amac_signal_s <= not amac_out when invert_polarity = '1' else amac_out;
    s_axis_in <= s_axis;
    m_axis_out <= m_axis;


    state_update : process(clk40) is
    begin
        if rising_edge(clk40) then
            if rst = '1' then
                state <= s_idle;
            else
                case state is
                    when s_idle =>
                        if m_axis.tvalid = '1' and m_axis_tready = '1' then
                            state <= s_next_bit;
                        else
                            state <= s_idle;
                        end if;

                    when s_next_bit =>
                        state <= s_send_bit;

                    when s_send_bit =>
                        if ticks_left = 0 then
                            if bits_left = 0 then
                                if end_of_chunk then
                                    state <= s_word_gap;
                                else
                                    state <= s_bit_gap;
                                end if;
                            else
                                state <= s_bit_gap;
                            end if;
                        end if;

                    when s_bit_gap =>
                        if ticks_left = 0 then
                            if bits_left = 0 then
                                state <= s_idle;
                            else
                                state <= s_next_bit;
                            end if;
                        else
                            state <= s_bit_gap;
                        end if;

                    when s_word_gap =>
                        if ticks_left = 0 then
                            state <= s_idle;
                        else
                            state <= s_word_gap;
                        end if;

                    when others =>
                        -- pragma synthesis off
                        report "Invalid state of AMAC signal encoder (state update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;


    variable_update : process(clk40) is
    begin
        if rising_edge(clk40) then
            if rst = '1' then
                amac_out <= '0';
                m_axis_tready <= '1';
                ticks_left <= 0;
                bits_left <= 0;
                end_of_chunk <= false;
            else
                case state is
                    when s_idle =>
                        m_axis_tready <= '1';
                        amac_out <= '0';
                        ticks_left <= 0;
                        bits_left <= 0;
                        end_of_chunk <= false;

                        if m_axis.tvalid = '1' and m_axis_tready = '1' then
                            end_of_chunk <= m_axis.tlast = '1';
                            bits_to_send <= m_axis.tdata;
                            m_axis_tready <= '0';
                            bits_left <= 8;
                        end if;

                    when s_next_bit =>
                        amac_out <= '1';
                        bits_left <= bits_left - 1;
                        bits_to_send <= bits_to_send(bits_to_send'high-1 downto bits_to_send'low) & '0';
                        if bits_to_send(bits_to_send'left) = '1' then
                            ticks_left <= TICKS_DAH_MID;
                        else
                            ticks_left <= TICKS_DIT_MID;
                        end if;

                    when s_send_bit =>
                        if ticks_left = 0 then
                            if bits_left = 0 then
                                if end_of_chunk then
                                    ticks_left <= TICKS_QUIESCENT;
                                else
                                    ticks_left <= TICKS_BITGAP_MID;
                                end if;
                            else
                                ticks_left <= TICKS_BITGAP_MID;
                            end if;
                            amac_out <= '0';
                        else
                            amac_out <= '1';
                            ticks_left <= ticks_left - 1;
                        end if;

                    when s_bit_gap | s_word_gap =>
                        amac_out <= '0';
                        if ticks_left = 0 then
                            null;
                        else
                            ticks_left <= ticks_left - 1;
                        end if;

                    when others =>
                        -- pragma synthesis off
                        report "Invalid state of AMAC signal encoder (variable update)" severity failure;
                -- pragma synthesis on
                end case;
            end if;
        end if;
    end process;

end Behavioral;
