--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;

package endeavour_package is

    constant TICKS_QUIESCENT : integer := 100;
    constant TICKS_DIT_MIN : integer := 6;     -- bit0 min
    constant TICKS_DIT_MAX : integer := 22;    -- bit0 max
    constant TICKS_DIT_MID : integer := (TICKS_DIT_MIN + TICKS_DIT_MAX)/2; --bit0
    constant TICKS_DAH_MIN : integer := 30;     --bit1 min
    constant TICKS_DAH_MAX : integer := 124;    --bit1 max
    constant TICKS_DAH_MID : integer := (TICKS_DAH_MIN + TICKS_DAH_MAX)/2; --bit1
    constant TICKS_BITGAP_MIN : integer := 11;   -- intraword gap min
    constant TICKS_BITGAP_MAX : integer := 75;   -- intraword gap max
    constant TICKS_BITGAP_MID : integer := (TICKS_BITGAP_MIN + TICKS_BITGAP_MAX)/2; --  intraword gap

end package endeavour_package;

package body endeavour_package is

end package body endeavour_package;
