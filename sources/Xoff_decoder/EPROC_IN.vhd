--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Mesfin Gebyehu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: Nikhef
-- Engineer: Frans Schreuder
-- 
-- Create Date: 01/05/2017 04:24:14 PM
-- Design Name: InputShifterNb
-- Module Name: InputShifterNb - Behavioral
-- Project Name: FELIX
-- Target Devices: Virtex7, Kintex Ultrascale
-- Tool Versions: 
-- Description: Universal input shift register, takes 2, 4, or 8 bit Elink data
--              and shifts it into 10b data (target direct data or 8b10b decoder)
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity EPROC_IN is
    generic (
        do_generate : boolean := true;
        generate_8b10b: boolean := true;
        generate_hdlc: boolean := true;
        DinWidth : integer range 0 to 16 := 16; --maximum width of the elink
        includeDirectMode   : boolean := true
        );
    Port (
        bitCLK : in std_logic;
        bitCLKx2 : in std_logic;
        rst    : in std_logic;
        ENA    : in std_logic;
        swap_inputbits : in std_logic;
        thCR_REVERSE_10B : in std_logic;
        ENCODING : in std_logic_vector(1 downto 0);
        EDATA_IN : in std_logic_vector(DinWidth-1 downto 0);
        DATA_OUT : out std_logic_vector (9 downto 0);
        DATA_RDY : out std_logic;
        InputWidth: in integer range 0 to 16; 
        busyOut  : out std_logic
    );
end EPROC_IN;

architecture Behavioral of EPROC_IN is
    signal edata_in_s : std_logic_vector(DinWidth-1 downto 0);
    signal direct_data_s : std_logic_vector(9 downto 0);
    signal direct_data_valid_s : std_logic;
    
    signal DATA_RDY_sig , DATA_RDY_8b10b_case, DATA_RDY_HDLC_case : std_logic;
    signal DATA_OUT_s, DATA_OUT_8b10b_case, DATA_OUT_HDLC_case : std_logic_vector(9 downto 0);
    signal RESTART_sig, rst_case00, rst_case01, rst_case10 : std_logic;
    signal Mode8b: std_logic;
    --signal dataCLK: std_logic; --can run at 40MHz or 80MHz (16 bit elink only), selected by generic / generate.
begin

g_generate: if do_generate = true generate
    with ENCODING select Mode8b <=
    '0' when "01",
    '1' when others;
    
    in_sel: process(swap_inputbits, EDATA_IN)
    begin
        if swap_inputbits = '1' then
            case InputWidth is
                when 2 => edata_in_s(1 downto 0) <= EDATA_IN(0) & EDATA_IN(1);
                          edata_in_s(DinWidth-1 downto 2) <= EDATA_IN(DinWidth-1 downto 2);
--                when 4 => 
--                          if (DinWidth >= 4) then
--                            edata_in_s(3 downto 0) <= EDATA_IN(0) & EDATA_IN(1) & EDATA_IN(2) & EDATA_IN(3);
--                            edata_in_s(DinWidth-1 downto 4) <= EDATA_IN(DinWidth-1 downto 4);
--                          else
--                            edata_in_s(DinWidth-1 downto 0) <= EDATA_IN(DinWidth-1 downto 0);
--                          end if;
                          
--                when 8 => if (DinWidth >= 8) then
--                            edata_in_s(7 downto 0) <= EDATA_IN(0) & EDATA_IN(1) & EDATA_IN(2) & EDATA_IN(3) & EDATA_IN(4) & EDATA_IN(5) & EDATA_IN(6) & EDATA_IN(7);
--                            edata_in_s(DinWidth-1 downto 8) <= EDATA_IN(DinWidth-1 downto 8);
--                          else
--                            edata_in_s(DinWidth-1 downto 0) <= EDATA_IN(DinWidth-1 downto 0);
--                          end if;
                            
--                when 16 =>
--                          if (DinWidth >= 16) then
--                            edata_in_s <= EDATA_IN(0) & EDATA_IN(1) & EDATA_IN(2) & EDATA_IN(3) & EDATA_IN(4) & EDATA_IN(5) & EDATA_IN(6) & EDATA_IN(7) 
--                            & EDATA_IN(8) & EDATA_IN(9) & EDATA_IN(10) & EDATA_IN(11) & EDATA_IN(12) & EDATA_IN(13) & EDATA_IN(14) & EDATA_IN(15);
--                          else
--                            edata_in_s(DinWidth-1 downto 0) <= EDATA_IN(DinWidth-1 downto 0);
--                          end if;
                when others =>
                    edata_in_s <= EDATA_IN(DinWidth-1 downto 0);
            
            end case;
        else
            edata_in_s <= EDATA_IN(DinWidth-1 downto 0);
        end if;	
    end process;

    RESTART_sig <= rst or (not ENA); -- comes from clk40 domain 

g8b: if(DinWidth < 16) generate
    shiftIn: entity work.InputShifterNb 
    Generic map(
        DinWidth => DinWidth, includeDirectMode => includeDirectMode
        )
    port map ( 
        DIN => EDATA_IN,
        DOUT => direct_data_s,
        DValid => direct_data_valid_s,
        Clk => bitCLK,
        Reset => RESTART_sig,
        InputWidth => InputWidth,
        Mode8b => Mode8b
    );
    
    g_8b10b: if generate_8b10b = true generate
    dec_8b10b: entity work.eproc_in_dec8b10b
    port map (
        bitClk => bitCLK,
        rst    => rst_case01,
        DATA_IN => direct_data_s,
        DATA_IN_VALID => direct_data_valid_s,
        DATA_OUT => DATA_OUT_8b10b_case,
        DATA_OUT_VALID => DATA_RDY_8b10b_case,
        busyOut => busyOut,
        thCR_REVERSE_10B => thCR_REVERSE_10B
    );
    end generate;
    g_no_8b10b: if generate_8b10b = false generate
        DATA_OUT_8b10b_case <= (others => '0');
        DATA_RDY_8b10b_case <= '0';
        busyOut <= '0';
    end generate;
end generate;
    
g16b: if(DinWidth = 16) generate
    signal DOUTa, DOUTb, DATA_OUT_8b10b_case_a, DATA_OUT_8b10b_case_b: std_logic_vector(9 downto 0);
    signal Dvalida, Dvalidb, DATA_RDY_8b10b_case_a, DATA_RDY_8b10b_case_b: std_logic;

begin

    shiftIn: entity work.InputShifter16b 
    Generic map(
        DinWidth => DinWidth, includeDirectMode => includeDirectMode
        )
    port map ( 
        DIN => EDATA_IN,
        DOUTa => DOUTa,
        DValida => DValida,
        DOUTb => DOUTb,
        DValidb => DValidb,
        Clk => bitCLK,
        Reset => RESTART_sig,
        InputWidth => InputWidth,
        Mode8b => Mode8b
    );
    
   
    mux0: entity work.mux40to80MHz
    port map (
        DINa => DOUTa,
        DValida => DValida,
        DINb => DOUTb,
        DValidb => DValidb,
        clk => bitCLKx2,
        Reset => RESTART_sig,
        DOUT => direct_data_s,
        DValid => direct_data_valid_s
    );
    
    dec_8b10b: entity work.eproc_in_dec8b10b
    port map (
        bitClk => bitCLKx2,
        rst    => rst_case01,
        DATA_IN => direct_data_s,
        DATA_IN_VALID => direct_data_valid_s,
        DATA_OUT => DATA_OUT_8b10b_case,
        DATA_OUT_VALID => DATA_RDY_8b10b_case,
        busyOut => busyOut,
        thCR_REVERSE_10B => thCR_REVERSE_10B
    );
end generate;
    
    
    -------------------------------------------------------------------------------------------
    -- ENCODING case "01": DEC8b10b
    -------------------------------------------------------------------------------------------
    rst_case01 <= '0' when ((RESTART_sig = '0') and (ENCODING = "01")) else '1';
    

g_hdlc: if generate_hdlc = true generate
    -------------------------------------------------------------------------------------------
    -- ENCODING case "10": HDLC
    -------------------------------------------------------------------------------------------
    rst_case10 <= '0' when ((RESTART_sig = '0') and (ENCODING = "10")) else '1';
    dec_HDLC: entity work.eproc_in_hdlc
    generic map (
        DinWidth => DinWidth
        )
    port map (
        bitClk => bitClk,
        bitCLKx2 => bitCLKx2,
        rst    => rst_case10,
        edataIN(1) => EDATA_IN(0),
        edataIN(0) => EDATA_IN(1),
        
        dataOUT   => DATA_OUT_HDLC_case,
        dataOUTrdy => DATA_RDY_HDLC_case
    );
end generate;

g_nohdlc: if generate_hdlc = false generate
    DATA_OUT_HDLC_case <= (others => '0');
    DATA_RDY_HDLC_case <= '0';
end generate;


    -------------------------------------------------------------------------------------------
    -- output data/rdy according to the encoding settings
    -------------------------------------------------------------------------------------------
    DATA_OUT_MUX4_10bit: entity work.MUX4_Nbit 
    generic map(N=>10)
    port map(
        data0 => direct_data_s,
        data1 => DATA_OUT_8b10b_case,
        data2 => DATA_OUT_HDLC_case,
        data3 => "0000000000", 
        sel   => ENCODING,
        data_out => DATA_OUT_s
    );

    DATA_RDY_MUX4: entity work.MUX4 
    port map(
        data0 => direct_data_valid_s,
        data1 => DATA_RDY_8b10b_case,
        data2 => DATA_RDY_HDLC_case,
        data3 => '0', 
        sel   => ENCODING,
        data_out => DATA_RDY_sig
    );


    DATA_RDY <= DATA_RDY_sig;
    DATA_OUT <= DATA_OUT_s;


end generate;
g_disabled: if do_generate = false generate
    DATA_OUT  <= (others => '0');
    DATA_RDY <= '0';
    busyOut  <= '0';
end generate;

end Behavioral;
