--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Mesfin Gebyehu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: Nikhef
-- Engineer: Frans Schreuder
-- 
-- Create Date: 01/05/2017 04:24:14 PM
-- Design Name: EPROC_IN_dec8b10b
-- Module Name: EPROC_IN_dec8b10b - Behavioral
-- Project Name: FELIX
-- Target Devices: Virtex7, Kintex Ultrascale
-- Tool Versions: 
-- Description: shifts 10b data until it detects 2x K28.5, then decodes.
--              
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library WORK;
use work.centralRouter_package.all;

entity EPROC_IN_dec8b10b is
    generic (do_generate : boolean := true);
    Port (
        bitCLK : in std_logic;
        rst    : in std_logic;
        DATA_IN : in std_logic_vector(9 downto 0);
        DATA_IN_VALID : in std_logic;
        DATA_OUT : out std_logic_vector (9 downto 0);
        DATA_OUT_VALID : out std_logic;
        busyOut  : out std_logic;
        thCR_REVERSE_10B : in std_logic
    );
end EPROC_IN_dec8b10b;


    

architecture Behavioral of EPROC_IN_dec8b10b is
    signal DATA_IN_p1: std_logic_vector(9 downto 0);
    signal DATA_IN_VALID_p1: std_logic;
    
    signal comma_detected, comma_detected_p1: std_logic_vector(9 downto 0); --10 possible shifts to detect
    signal decoder_din: std_logic_vector(9 downto 0);
    
    signal comma_found : std_logic := '0';
    
    type slv10_array is array (0 to 9) of std_logic_vector(9 downto 0);
    signal shifted_din_s: slv10_array;
    signal DATA_IN_p1_sw : std_logic_vector(9 downto 0);
    signal DATA_IN_sw : std_logic_vector(9 downto 0);

begin
swap: for i in 0 to 9 generate
      DATA_IN_p1_sw(i) <= DATA_IN_p1(9-i);
      DATA_IN_sw(i) <= DATA_IN(9-i);
end generate;
                    
  
    kDetect: process(bitCLK, rst)
        variable shifted_din: slv10_array;
        variable shift: integer range 0 to 9:= 0;
        variable comma_check: std_logic_vector(9 downto 0);
        
        
    begin
        if(rst = '1') then
            shift := 0;
            comma_found <= '0';
            DATA_OUT_VALID <= '0';
            comma_detected <= "0000000000"; --default is no comma detected.
            comma_detected_p1 <= "0000000000"; --default is no comma detected.
            DATA_IN_p1 <= (others => '0');
        elsif rising_edge(bitClk) then
            DATA_IN_VALID_p1 <= DATA_IN_VALID;
            DATA_OUT_VALID <= comma_found and DATA_IN_VALID_p1;
            if(DATA_IN_VALID = '1') then
                if thCR_REVERSE_10B = '0' then
                    for i in 0 to 9 loop
                        shifted_din(i) := DATA_IN(9-i downto 0) & DATA_IN_p1(9 downto 10-i);
                    end loop;
                else
                    for i in 0 to 9 loop
                        shifted_din(i) := DATA_IN_p1_sw(i downto 0)&DATA_IN_sw(9 downto i+1);
                        
                        --(9-i downto 0) & DATA_IN_p1(9 downto 10-i);
                    end loop;
                end if;
                
                DATA_IN_p1 <= DATA_IN;
                comma_detected_p1 <= comma_detected;
                comma_detected <= "0000000000"; --default is no comma detected.
                for i in 0 to 9 loop
                    comma_check := "0000000000";
                    comma_check(i) := '1';
                    if shifted_din(i) = COMMAp or shifted_din(i) = COMMAn then
                        comma_detected <= comma_check;
                    end if;
                    if (comma_detected = comma_check) and (comma_detected_p1 = comma_check) then
                        shift := i;
                        comma_found <= '1';
                    end if;
                end loop;
                decoder_din <= shifted_din(shift);
                shifted_din_s <= shifted_din;


                    
            end if;
            
        end if;
    end process;

    dec_8b10: entity work.dec_8b10_wrap
    port map(
        RESET         => rst,
        RBYTECLK      => bitCLK,
        ABCDEIFGHJ_IN => decoder_din,
        HGFEDCBA      => DATA_OUT(7 downto 0),
        ISK           => DATA_OUT(9 downto 8),
        BUSY          => busyOut
    );


end Behavioral;

