--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library work, ieee, UNISIM;
use work.FELIX_gbt_package.all;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.pcie_package.all;
use work.centralRouter_package.all;

entity housekeeping_control is
  port (
    RST                  : in     std_logic;
    i2c_clkfreq_sel      : out    std_logic;
    i2c_config_trig      : out    std_logic;
    i2c_rst              : out    std_logic;
    i2cmux_rst           : out    std_logic;
    register_map_control : in     register_map_control_type;
    rst_soft             : in     std_logic);
end entity housekeeping_control;



architecture rtl of housekeeping_control is

  signal reset: std_logic;

begin

  reset <= RST or rst_soft;
  
  i2c_rst <= reset;
  i2cmux_rst <= reset;

  -- Control Register mapping for Housekeeping
  i2c_clkfreq_sel    <= register_map_control.HK_CTRL(0);
  i2c_config_trig    <= register_map_control.HK_CTRL(1);
  
end architecture rtl ; -- of housekeeping_control

