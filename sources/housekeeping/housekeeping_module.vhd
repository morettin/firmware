--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Israel Grayzman
--!               Kai Chen
--!               Enrico Gamberini
--!               RHabraken
--!               Rene
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.



library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.numeric_std_unsigned.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.I2C.all;
    use work.FELIX_package.all;

library xpm;
    use xpm.vcomponents.all;

entity housekeeping_module is
    generic(
        CARD_TYPE                       : integer := 710;
        GBT_NUM                         : integer := 24;
        ENDPOINTS                       : integer := 1; --Number of PCIe Endpoints in the design, GBT_NUM has to be multiplied by this number in some cases.
        generateTTCemu                  : boolean := false;
        AUTOMATIC_CLOCK_SWITCH          : boolean := false;
        FIRMWARE_MODE                   : integer := 0;
        USE_Si5324_RefCLK               : boolean := false;
        GENERATE_XOFF                   : boolean := true; -- FromHost Xoff transmission enabled on Full mode busy
        IncludeDecodingEpath2_HDLC      : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath2_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath4_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath8_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath16_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeDecodingEpath32_8b10b    : std_logic_vector(6 downto 0) := "0000000";
        IncludeDirectDecoding           : std_logic_vector(6 downto 0) := "0000000";
        IncludeEncodingEpath2_HDLC      : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath2_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath4_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath8_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeDirectEncoding           : std_logic_vector(4 downto 0) := "00000";
        BLOCKSIZE                       : integer := 1024;
        DATA_WIDTH                      : integer := 256;
        FULL_HALFRATE                   : boolean := false;
        SUPPORT_HDLC_DELAY              : boolean := false;
        TTC_SYS_SEL                     : std_logic := '0';
        USE_VERSAL_CPM                  : boolean := false
    );
    port (
        MMCM_Locked_in              : in     std_logic;
        MMCM_OscSelect_in           : in     std_logic;
        SCL                         : inout  std_logic;
        SDA                         : inout  std_logic;
        SI5345_A                    : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_INSEL                : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_OE                   : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_RSTN                 : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_SEL                  : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_nLOL                 : in     std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_FINC_B               : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_FDEC_B               : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_INTR_B               : in     std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        appreg_clk                  : in     std_logic;
        emcclk                      : in     std_logic_vector(NUM_EMCCLK(CARD_TYPE)-1 downto 0);
        flash_SEL                   : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_a                     : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*25-1 downto 0);
        flash_a_msb                 : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*2-1 downto 0);
        flash_adv                   : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_cclk                  : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_ce                    : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_d                     : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*16-1 downto 0);
        flash_re                    : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_we                    : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        i2cmux_rst                  : out    std_logic_vector(NUM_I2C_MUXES(CARD_TYPE)-1 downto 0);
        TACH                        : in     std_logic_vector(NUM_TACH(CARD_TYPE)-1 downto 0);
        FAN_FAIL_B                  : in     std_logic_vector(NUM_FAN_FAIL(CARD_TYPE)-1 downto 0);
        FAN_FULLSP                  : in     std_logic_vector(NUM_FAN_FAIL(CARD_TYPE)-1 downto 0);
        FAN_OT_B                    : in     std_logic_vector(NUM_FAN_FAIL(CARD_TYPE)-1 downto 0);
        FAN_PWM                     : out    std_logic_vector(NUM_FAN_PWM(CARD_TYPE)-1 downto 0);
        FF3_PRSNT_B                 : in     std_logic_vector(NUM_FF3_PRSTN(CARD_TYPE)-1 downto 0);
        IOEXPAN_INTR_B              : in     std_logic_vector(NUM_IOEXP(CARD_TYPE)-1 downto 0);
        IOEXPAN_RST_B               : out    std_logic_vector(NUM_IOEXP(CARD_TYPE)-1 downto 0);
        clk10_xtal                  : in     std_logic;
        clk40_xtal                  : in     std_logic;
        leds                        : out    std_logic_vector(NUM_LEDS(CARD_TYPE)-1 downto 0);
        opto_inhibit                : out    std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        register_map_control        : in     register_map_control_type;
        register_map_gen_board_info : out    register_map_gen_board_info_type;
        register_map_hk_monitor     : out    register_map_hk_monitor_type;
        rst_soft                    : in     std_logic;
        sys_reset_n                 : in     std_logic;
        PCIE_PWRBRK                 : in     std_logic_vector(NUM_PCIE_PWRBRK(CARD_TYPE)-1 downto 0);
        PCIE_WAKE_B                 : in     std_logic_vector(NUM_PCIE_PWRBRK(CARD_TYPE)-1 downto 0);
        QSPI_RST_B                  : out    std_logic_vector(NUM_QSPI_RST(CARD_TYPE)-1 downto 0);
        rst_hw                      : in std_logic;
        CLK40_FPGA2LMK_P            : out std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        CLK40_FPGA2LMK_N            : out std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_DATA                    : out std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_CLK                     : out std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LE                      : out std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_GOE                     : out std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LD                      : in std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_SYNCn                   : out std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        I2C_SMB                   : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        I2C_SMBUS_CFG_nEN         : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        MGMT_PORT_EN              : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PCIE_PERSTn_out           : out    std_logic_vector(NUM_PEX(CARD_TYPE)*2-1 downto 0);
        PEX_PERSTn                : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SCL                   : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SDA                   : inout  std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PORT_GOOD                 : in     std_logic_vector(NUM_PEX(CARD_TYPE)*8-1 downto 0);
        SHPC_INT                  : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        lnk_up                    : in     std_logic_vector(1 downto 0);
        RXUSRCLK_IN               : in     std_logic_vector((GBT_NUM*ENDPOINTS)-1 downto 0);
        versal_sys_reset_n_out    : out    std_logic;
        WupperToCPM: in WupperToCPM_array_type(0 to 1);
        CPMToWupper: out CPMToWupper_array_type(0 to 1);
        clk100_out : out std_logic;
        DDR_in : in DDR_in_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_out : out DDR_out_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_inout : inout DDR_inout_array_type(0 to NUM_DDR(CARD_TYPE)-1)
    );
end entity housekeeping_module;


architecture structure of housekeeping_module is

    signal RST                            : std_logic;
    signal nReset                         : std_logic;
    signal clk                            : std_logic;
    signal cmd_ack                        : std_logic;
    --signal ack_out                        : std_logic;
    signal Dout                           : std_logic_vector(7 downto 0);
    signal Din                            : std_logic_vector(7 downto 0);
    signal ack_in                         : std_logic;
    signal write                          : std_logic;
    signal read                           : std_logic;
    signal stop                           : std_logic;
    signal start                          : std_logic;
    signal ena                            : std_logic;
    signal reset                          : std_logic;
    signal AUTOMATIC_CLOCK_SWITCH_ENABLED : std_logic_vector(0 downto 0);
    signal TACH_CNT                       : std_logic_vector(19 downto 0);
    signal TACH_CNT_LATCHED               : std_logic_vector(19 downto 0);
    signal TACH_FLAG                      : std_logic:='0';
    signal TACH_R                         : std_logic:='0';
    signal TACH_2R                        : std_logic:='0';
    signal TACH_3R                        : std_logic:='0';

    signal dna_out_data                   : std_logic_vector(95 downto 0); --IG: get the entire output vector data and assign the relevant bits only
    signal LMK_locked                     : std_logic;
    signal gpio2_o                        : std_logic_vector(20 downto 0);
    signal gpio_o                         : std_logic_vector(20 downto 0);
    signal gpio2_i                        : std_logic_vector(20 downto 0);
    signal gpio_i                         : std_logic_vector(20 downto 0);

    signal scl_cips_o                     : std_logic := '1';
    signal sda_cips_o                     : std_logic := '1';
    signal scl_opencores_o                : std_logic := '1';
    signal sda_opencores_o                : std_logic := '1';
    signal sda_opencores_cips_clk_o       : std_logic := '1';
    signal scl_opencores_cips_clk_o       : std_logic := '1';
    signal SDA_i                          : std_logic := '1';
    signal SCL_i                          : std_logic := '1';
    signal SDA_o                          : std_logic := '1';
    signal SCL_o                          : std_logic := '1';
    --attribute IOB                         : string;
    --attribute IOB of SDA_o, SCL_o         : signal is "TRUE";

    signal pl0_ref_clk                    : std_logic;

    signal si5345_lol_latched: std_logic_vector(0 downto 0);
    signal mmcm_main_lol_latched: std_logic_vector(0 downto 0);


begin

    register_map_hk_monitor.LMK_LOCKED(0) <= LMK_locked;

    i2c0: entity work.simple_i2c
        port map(
            clk     => clk,
            ena     => ena,
            nReset  => nReset,
            clk_cnt => "01100100",
            start   => start,
            stop    => stop,
            read    => read,
            write   => write,
            ack_in  => ack_in,
            Din     => Din,
            cmd_ack => cmd_ack,
            ack_out => open, --ack_out,
            Dout    => Dout,
            SCL_i    => SCL_i,
            SDA_i    => SDA_i,
            SCL_o    => scl_opencores_o,
            SDA_o    => sda_opencores_o);

    SCL_iobuf: IOBUF  -- @suppress "Generic map uses default values. Missing optional actuals: DRIVE, IBUF_LOW_PWR, IOSTANDARD, SLEW"
        port map(
            O => SCL_i,
            IO => SCL,
            I => '0',
            T => SCL_o
        );

    SDA_iobuf: IOBUF  -- @suppress "Generic map uses default values. Missing optional actuals: DRIVE, IBUF_LOW_PWR, IOSTANDARD, SLEW"
        port map(
            O => SDA_i,
            IO => SDA,
            I => '0',
            T => SDA_o
        );


    g_182_i2c: if (CARD_TYPE = 182 or CARD_TYPE = 155) generate
    begin
        xpm_cdc_sda : xpm_cdc_single
            generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1
            )

            port map(
                src_clk => clk,
                src_in => sda_opencores_o,
                dest_clk => pl0_ref_clk,
                dest_out => sda_opencores_cips_clk_o
            );

        xpm_cdc_scl : xpm_cdc_single
            generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 1
            )

            port map(
                src_clk => clk,
                src_in => scl_opencores_o,
                dest_clk => pl0_ref_clk,
                dest_out => scl_opencores_cips_clk_o
            );

        process (pl0_ref_clk)
        begin
            if rising_edge(pl0_ref_clk) then
                SDA_o  <=  sda_opencores_cips_clk_o and sda_cips_o;
                SCL_o  <=  scl_opencores_cips_clk_o and scl_cips_o;
            end if;
        end process;

    else generate -- g_182_i2c
        process (clk)
        begin
            if rising_edge(clk) then
                SDA_o <= sda_opencores_o;
                SCL_o <= scl_opencores_o;
            end if;
        end process;

        scl_cips_o <= '1';
        sda_cips_o <= '1';
    end generate; -- g_182_i2c




    g_ila: if true generate

        COMPONENT i2c_ila
            PORT (
                clk : IN STD_LOGIC;
                probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
                probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
            );
        END COMPONENT;
    begin
        ila0: i2c_ila port map(
                clk => appreg_clk,
                probe0(0) => SDA_i,
                probe1(0) => SDA_o,
                probe2(0) => SCL_i,
                probe3(0) => SCL_o,
                probe4(0) => sda_opencores_o,
                probe5(0) => scl_opencores_o,
                probe6(0) => sda_opencores_cips_clk_o,
                probe7(0) => scl_opencores_cips_clk_o
            );
    end generate g_ila;


    i2cint0: entity work.i2c_interface
        port map(
            Din                  => Din,
            Dout                 => Dout,
            I2C_RD               => register_map_hk_monitor.I2C_RD,
            I2C_WR               => register_map_hk_monitor.I2C_WR,
            RST                  => RST,
            ack_in               => ack_in,
            --ack_out              => ack_out,
            appreg_clk           => appreg_clk,
            clk                  => clk,
            cmd_ack              => cmd_ack,
            ena                  => ena,
            nReset               => nReset,
            read                 => read,
            register_map_control => register_map_control,
            rst_soft             => rst_soft,
            start                => start,
            stop                 => stop,
            write                => write);

    const0: entity work.GenericConstantsToRegs
        generic map(
            GBT_NUM => GBT_NUM,
            ENDPOINTS => ENDPOINTS,
            OPTO_TRX => 2,
            generateTTCemu => generateTTCemu,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_Si5324_RefCLK => USE_Si5324_RefCLK,
            --CREnableFromHost                => CREnableFromHost,
            --includeDirectMode               => includeDirectMode,
            GENERATE_XOFF => GENERATE_XOFF,
            IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
            IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
            IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
            IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
            IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
            IncludeDecodingEpath32_8b10b => IncludeDecodingEpath32_8b10b,
            IncludeDirectDecoding => IncludeDirectDecoding,
            IncludeEncodingEpath2_HDLC => IncludeEncodingEpath2_HDLC,
            IncludeEncodingEpath2_8b10b => IncludeEncodingEpath2_8b10b,
            IncludeEncodingEpath4_8b10b => IncludeEncodingEpath4_8b10b,
            IncludeEncodingEpath8_8b10b => IncludeEncodingEpath8_8b10b,
            IncludeDirectEncoding => IncludeDirectEncoding,

            BLOCKSIZE => BLOCKSIZE,
            CHUNK_TRAILER_32B => true,
            DATA_WIDTH => DATA_WIDTH,
            FULL_HALFRATE => FULL_HALFRATE,
            SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY)
        port map(
            AUTOMATIC_CLOCK_SWITCH_ENABLED => AUTOMATIC_CLOCK_SWITCH_ENABLED,
            register_map_gen_board_info    => register_map_gen_board_info);

    g_VC709: if CARD_TYPE = 709 generate
        opto_inhibit     <= (others => '0');  -- SFP_TX_DISABLE_H: 1 = DISABLE
    end generate;

    g_notVC709: if CARD_TYPE /= 709 generate
        opto_inhibit    <= (others => '1');  -- MiniPOD_nRST: 0 = RESET
    end generate;

    xadc0: entity work.xadc_drp
        generic map(
            CARD_TYPE => CARD_TYPE)
        port map(
            clk40   => appreg_clk,
            reset   => reset,
            temp    => register_map_hk_monitor.FPGA_CORE_TEMP,
            vccint  => register_map_hk_monitor.FPGA_CORE_VCCINT,
            vccaux  => register_map_hk_monitor.FPGA_CORE_VCCAUX,
            vccbram => register_map_hk_monitor.FPGA_CORE_VCCBRAM);

    dna0: entity work.dna
        generic map(
            CARD_TYPE => CARD_TYPE)
        port map(
            clk40   => appreg_clk,
            reset   => reset,
            --IG      dna_out(63 downto 0) => register_map_hk_monitor.FPGA_DNA,
            --IG      dna_out(95 downto 64) => open);
            dna_out => dna_out_data);
    register_map_hk_monitor.FPGA_DNA  <= dna_out_data(63 downto 0);


    g_NUM_BPI_FLASH: for i in 0 to NUM_BPI_FLASH(CARD_TYPE)-1 generate
        u1: entity work.flash_wrapper
            port map(
                rst                  => RST,
                clk                  => emcclk(0),
                flash_a_msb          => flash_a_msb(i*2+1 downto i*2),
                flash_a              => flash_a(25*i+24 downto 25*i),
                flash_d              => flash_d(16*i+15 downto 16*i),
                flash_adv            => flash_adv(i),
                flash_cclk           => flash_cclk(i),
                flash_ce             => flash_ce(i),
                flash_we             => flash_we(i),
                flash_re             => flash_re(i),
                register_map_control => register_map_control,
                config_flash_rd      => register_map_hk_monitor.CONFIG_FLASH_RD,
                flash_SEL            => flash_SEL(i));
    end generate;

    g_180: if (CARD_TYPE = 180) generate
        component cips_bd_wrapper
            port (
                pl0_resetn : out STD_LOGIC;
                pl0_ref_clk_0 : out std_logic
            );
        end component;
    begin
        versal_cips_block0: cips_bd_wrapper
            port map(
                pl0_resetn => versal_sys_reset_n_out,
                pl0_ref_clk_0 => clk100_out
            );
    end generate g_180;

    g_181: if (CARD_TYPE = 181) generate
        component cips_bd_BNL181_wrapper
            port (
                pl0_resetn : out STD_LOGIC
            );
        end component;
    begin
        versal_cips_block0: cips_bd_BNL181_wrapper
            port map(
                pl0_resetn => versal_sys_reset_n_out
            );
    end generate g_181;


    g_182_nocpm: if (CARD_TYPE = 182 and not USE_VERSAL_CPM) generate
        component cips_bd_BNL182_wrapper
            port (
                CH0_DDR4_0_0_act_n : out STD_LOGIC_VECTOR ( 0 to 0 );
                CH0_DDR4_0_0_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
                CH0_DDR4_0_0_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_bg : out STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_ck_c : out STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_ck_t : out STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_cke : out STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_cs_n : out STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_dm_n : inout STD_LOGIC_VECTOR ( 7 downto 0 );
                CH0_DDR4_0_0_dq : inout STD_LOGIC_VECTOR ( 63 downto 0 );
                CH0_DDR4_0_0_dqs_c : inout STD_LOGIC_VECTOR ( 7 downto 0 );
                CH0_DDR4_0_0_dqs_t : inout STD_LOGIC_VECTOR ( 7 downto 0 );
                CH0_DDR4_0_0_odt : out STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_reset_n : out STD_LOGIC_VECTOR ( 0 to 0 );
                scl_i : in STD_LOGIC;
                scl_o : out STD_LOGIC;
                sda_i : in STD_LOGIC;
                sda_o : out STD_LOGIC;
                clk100_out : out STD_LOGIC;
                gpio2_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
                gpio2_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
                gpio_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
                gpio_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
                pl0_resetn : out STD_LOGIC_VECTOR ( 0 to 0 );
                sys_clk0_0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
                sys_clk0_0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
                pl0_ref_clk : out STD_LOGIC
            );
        end component;
    begin
        versal_cips_block0: cips_bd_BNL182_wrapper
            port map(
                pl0_resetn(0) => versal_sys_reset_n_out,
                clk100_out => clk100_out,
                CH0_DDR4_0_0_act_n   => DDR_out(0).act_n,
                CH0_DDR4_0_0_adr     => DDR_out(0).adr,
                CH0_DDR4_0_0_ba      => DDR_out(0).ba,
                CH0_DDR4_0_0_bg      => DDR_out(0).bg,
                CH0_DDR4_0_0_ck_c    => DDR_out(0).ck_c,
                CH0_DDR4_0_0_ck_t    => DDR_out(0).ck_t,
                CH0_DDR4_0_0_cke     => DDR_out(0).cke,
                CH0_DDR4_0_0_cs_n    => DDR_out(0).cs_n,
                CH0_DDR4_0_0_dm_n    => DDR_inout(0).dm_n(7 downto 0),
                CH0_DDR4_0_0_dq      => DDR_inout(0).dq(63 downto 0),
                CH0_DDR4_0_0_dqs_c   => DDR_inout(0).dqs_c(7 downto 0),
                CH0_DDR4_0_0_dqs_t   => DDR_inout(0).dqs_t(7 downto 0),
                CH0_DDR4_0_0_odt     => DDR_out(0).odt,
                CH0_DDR4_0_0_reset_n => DDR_out(0).reset_n,
                gpio2_io_o_0         => gpio2_o,
                gpio_io_o_0          => gpio_o,
                gpio2_io_i_0         => gpio2_i,
                gpio_io_i_0          => gpio_i,
                scl_i                => SCL_i,
                sda_i                => SDA_i,
                scl_o                => scl_cips_o,
                sda_o                => sda_cips_o,
                sys_clk0_0_clk_n     => DDR_in(0).sys_clk_n,
                sys_clk0_0_clk_p     => DDR_in(0).sys_clk_p,
                pl0_ref_clk          => pl0_ref_clk
            );
    end generate g_182_nocpm;

    g_182cpm: if (CARD_TYPE = 182  and USE_VERSAL_CPM) or CARD_TYPE = 155 generate
        component versal_cpm_pcie_endpoints
            generic(CARD_TYPE : integer);
            port(
                CH0_DDR4_0_0_act_n   : out   STD_LOGIC_VECTOR ( 0 to 0 );
                CH0_DDR4_0_0_adr     : out   STD_LOGIC_VECTOR ( 16 downto 0 );
                CH0_DDR4_0_0_ba      : out   STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_bg      : out   STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_ck_c    : out   STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_ck_t    : out   STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_cke     : out   STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_cs_n    : out   STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_dm_n    : inout STD_LOGIC_VECTOR ( 8 downto 0 );
                CH0_DDR4_0_0_dq      : inout STD_LOGIC_VECTOR ( 71 downto 0 );
                CH0_DDR4_0_0_dqs_c   : inout STD_LOGIC_VECTOR ( 8 downto 0 );
                CH0_DDR4_0_0_dqs_t   : inout STD_LOGIC_VECTOR ( 8 downto 0 );
                CH0_DDR4_0_0_odt     : out   STD_LOGIC_VECTOR ( 1 downto 0 );
                CH0_DDR4_0_0_reset_n : out   STD_LOGIC_VECTOR ( 0 to 0 );
                scl_i                : in    STD_LOGIC;
                scl_o                : out   STD_LOGIC;
                sda_i                : in    STD_LOGIC;
                sda_o                : out   STD_LOGIC;
                gpio2_io_i_0         : in    STD_LOGIC_VECTOR ( 20 downto 0 );
                gpio2_io_o_0         : out   STD_LOGIC_VECTOR ( 20 downto 0 );
                gpio_io_i_0          : in    STD_LOGIC_VECTOR ( 20 downto 0 );
                gpio_io_o_0          : out   STD_LOGIC_VECTOR ( 20 downto 0 );
                pl0_resetn           : out   STD_LOGIC_VECTOR ( 0 to 0 );
                sys_clk0_0_clk_n     : in    STD_LOGIC_VECTOR ( 0 to 0 );
                sys_clk0_0_clk_p     : in    STD_LOGIC_VECTOR ( 0 to 0 );
                pl0_ref_clk          : out   STD_LOGIC;
                WupperToCPM          : in    WupperToCPM_array_type(0 to 1);
                CPMToWupper          : out   CPMToWupper_array_type(0 to 1);
                clk100_out           : out   std_logic
            );
        end component versal_cpm_pcie_endpoints;
    begin
        cpm0: versal_cpm_pcie_endpoints
            generic map(
                CARD_TYPE => CARD_TYPE
            )
            Port map(
                CH0_DDR4_0_0_act_n   => DDR_out(0).act_n,
                CH0_DDR4_0_0_adr     => DDR_out(0).adr,
                CH0_DDR4_0_0_ba      => DDR_out(0).ba,
                CH0_DDR4_0_0_bg      => DDR_out(0).bg,
                CH0_DDR4_0_0_ck_c    => DDR_out(0).ck_c,
                CH0_DDR4_0_0_ck_t    => DDR_out(0).ck_t,
                CH0_DDR4_0_0_cke     => DDR_out(0).cke,
                CH0_DDR4_0_0_cs_n    => DDR_out(0).cs_n,
                CH0_DDR4_0_0_dm_n    => DDR_inout(0).dm_n,
                CH0_DDR4_0_0_dq      => DDR_inout(0).dq,
                CH0_DDR4_0_0_dqs_c   => DDR_inout(0).dqs_c,
                CH0_DDR4_0_0_dqs_t   => DDR_inout(0).dqs_t,
                CH0_DDR4_0_0_odt     => DDR_out(0).odt,
                CH0_DDR4_0_0_reset_n => DDR_out(0).reset_n,
                gpio2_io_o_0         => gpio2_o,
                gpio_io_o_0          => gpio_o,
                gpio2_io_i_0         => gpio2_i,
                gpio_io_i_0          => gpio_i,
                scl_i                => SCL_i,
                sda_i                => SDA_i,
                scl_o                => scl_cips_o,
                sda_o                => sda_cips_o,
                sys_clk0_0_clk_n     => DDR_in(0).sys_clk_n,
                sys_clk0_0_clk_p     => DDR_in(0).sys_clk_p,
                pl0_resetn(0) => versal_sys_reset_n_out,
                pl0_ref_clk => pl0_ref_clk,
                WupperToCPM => WupperToCPM,
                CPMToWupper => CPMToWupper,
                clk100_out => clk100_out
            );
    else generate

    end generate;

    g_NUM_LMK: for i in 0 to NUM_LMK(CARD_TYPE)-1 generate
        lmk_init0: entity work.LMK03200_wrapper
            generic map(
                freq => LMKFREQ(TTC_SYS_SEL)
            )
            port map(
                rst_lmk => '0',
                hw_rst => rst_hw,
                LMK_locked => LMK_locked,
                clk40m_in => clk40_xtal,
                --                clk40m_in => clk40, --MT LMK to be driven by same clk lpgbt mgt for downlink
                --w/ clk40_xtal downstream emulator doesn't sink to
                --flx712 data
                clk10m_in => clk10_xtal,
                CLK40_FPGA2LMK_P => CLK40_FPGA2LMK_P(i),
                CLK40_FPGA2LMK_N => CLK40_FPGA2LMK_N(i),
                LMK_DATA => LMK_DATA(i),
                LMK_CLK => LMK_CLK(i),
                LMK_LE => LMK_LE(i),
                LMK_GOE => LMK_GOE(i),
                LMK_LD => LMK_LD(i),
                LMK_SYNCn => LMK_SYNCn(i));

    end generate;

    g_NUM_PEX: for i in 0 to NUM_PEX(CARD_TYPE)-1 generate

        pex_init0: entity work.pex_init
            generic map(
                CARD_TYPE => CARD_TYPE)
            port map(
                I2C_SMB           => I2C_SMB(i),
                I2C_SMBUS_CFG_nEN => I2C_SMBUS_CFG_nEN(i),
                MGMT_PORT_EN      => MGMT_PORT_EN(i),
                PCIE_PERSTn1      => PCIE_PERSTn_out(i*2),
                PCIE_PERSTn2      => PCIE_PERSTn_out(i*2+1),
                PEX_PERSTn        => PEX_PERSTn(i),
                PEX_SCL           => PEX_SCL(i),
                PEX_SDA           => PEX_SDA(i),
                PORT_GOOD         => PORT_GOOD(i*8+7 downto i*8),
                SHPC_INT          => SHPC_INT(i),
                clk40             => clk40_xtal,
                lnk_up0           => lnk_up(0),
                lnk_up1           => lnk_up(1),
                --reset_pcie        => open,
                sys_reset_n       => sys_reset_n);

    end generate;

    freqmeter0: entity work.gc_multichannel_frequency_meter
        generic map(
            g_WITH_INTERNAL_TIMEBASE => true, --: boolean := true;
            g_CLK_SYS_FREQ           => 25000000, --: integer;
            g_COUNTER_BITS           => 32, --: integer := 32;
            g_CHANNELS               => GBT_NUM*ENDPOINTS)--: integer := 1)

        port map(
            clk_sys_i     => appreg_clk,--: in  std_logic;
            clk_in_i      => RXUSRCLK_IN,--: in  std_logic_vector(g_CHANNELS -1 downto 0);
            rst_n_i       => '1',--: in  std_logic;
            pps_p1_i      => '0',--: in  std_logic;
            channel_sel_i => register_map_control.RXUSRCLK_FREQ.CHANNEL,--: in  std_logic_vector(5 downto 0);
            freq_o        => register_map_hk_monitor.RXUSRCLK_FREQ.VAL,
            freq_valid_o  => register_map_hk_monitor.RXUSRCLK_FREQ.VALID(38)
        );

    reset <= rst_soft or RST;

    RST <= not sys_reset_n;

    --  type bitfield_hk_ctrl_fmc_type is record
    --    SI5345_INSEL                   : std_logic_vector(6 downto 5);    -- Selects the input clock source
    --    SI5345_A                       : std_logic_vector(4 downto 3);    -- Si5345 address select
    --    SI5345_OE                      : std_logic_vector(2 downto 2);    -- Si5345 output enable
    --    SI5345_RSTN                    : std_logic_vector(1 downto 1);    -- Si5345 reset,
    --    SI5345_SEL                     : std_logic_vector(0 downto 0);    -- Si5345 i2c select bit
    --  end record;
    g_FLX182_155: if CARD_TYPE = 182 or CARD_TYPE = 155 generate
        signal GPIO_SEL : std_logic;
        signal GPIO_SI5345_INSEL : std_logic_vector(3 downto 0);
        signal GPIO_SI5345_A : std_logic_vector(3 downto 0);
        signal GPIO_SI5345_OE : std_logic_vector(1 downto 0);
        signal GPIO_SI5345_RSTN : std_logic_vector(1 downto 0);
        signal GPIO_SI5345_FINC_B: std_logic_vector(1 downto 0);
        signal GPIO_SI5345_FDEC_B: std_logic_vector(1 downto 0);
        signal GPIO_LEDS: std_logic_vector(3 downto 0);
        signal GPIO_I2CMUX_RSTB: std_logic;

        signal GPIO_FAN_PWM: std_logic;
        signal GPIO_IOEXPAN_RST_B: std_logic_vector(1 downto 0);
        signal GPIO_QSPI_RST_B: std_logic_vector(NUM_QSPI_RST(CARD_TYPE)-1 downto 0);
    begin
        --GPIO0[20] controls whether Versal GPIO is used (1) or the register map (0).
        GPIO_SEL <= gpio_o(20);
        GPIO_SI5345_INSEL <= gpio2_o(15) & gpio2_o(14) & gpio2_o(19) & gpio2_o(16);
        GPIO_SI5345_A <= gpio2_o(10) & gpio2_o(4) &  gpio2_o(0) & gpio2_o(12);
        GPIO_SI5345_OE <= gpio2_o(13) & gpio2_o(9);
        GPIO_SI5345_RSTN <= gpio2_o(1) & gpio2_o(17);
        GPIO_SI5345_FINC_B <= gpio2_o(18) & gpio2_o(3);
        GPIO_SI5345_FDEC_B <= gpio2_o(6) & gpio2_o(5);

        GPIO_LEDS <= gpio_o(4) & gpio_o(0) & gpio_o(2) & gpio_o(3);
        GPIO_I2CMUX_RSTB <= gpio_o(1);
        GPIO_FAN_PWM <= gpio_o(11);
        GPIO_IOEXPAN_RST_B <= gpio_o(12) & gpio_o(16);
        g_qspi_rst3: if NUM_QSPI_RST(CARD_TYPE) = 3 generate
            GPIO_QSPI_RST_B <= gpio_o(18) & gpio_o(17) & gpio_o(19);
        else generate
            GPIO_QSPI_RST_B <= (others => gpio_o(19));
        end generate;

        with GPIO_SEL select
            SI5345_INSEL(3 downto 0) <= register_map_control.HK_CTRL_FMC.SI5345_INSEL(6 downto 5) & register_map_control.HK_CTRL_FMC.SI5345_INSEL(6 downto 5) when '0',
                GPIO_SI5345_INSEL when others;
        with GPIO_SEL select
            SI5345_A(3 downto 0) <= "0100" when '0',
                GPIO_SI5345_A when others;
        with GPIO_SEL select
            SI5345_OE(1 downto 0) <= register_map_control.HK_CTRL_FMC.SI5345_OE(2) & register_map_control.HK_CTRL_FMC.SI5345_OE(2) when '0',
                GPIO_SI5345_OE when others;
        with GPIO_SEL select
            SI5345_RSTN(1 downto 0) <= register_map_control.HK_CTRL_FMC.SI5345_RSTN(1) & register_map_control.HK_CTRL_FMC.SI5345_RSTN(1) when '0',
                GPIO_SI5345_RSTN when others;
        with GPIO_SEL select
            SI5345_FINC_B <= register_map_control.HK_CTRL_FMC.SI5345_FINC_B when '0',
                GPIO_SI5345_FINC_B when others;
        with GPIO_SEL select
            SI5345_FDEC_B <= register_map_control.HK_CTRL_FMC.SI5345_FDEC_B when '0',
                GPIO_SI5345_FDEC_B when others;
        SI5345_SEL(1 downto 0) <=  register_map_control.HK_CTRL_FMC.SI5345_SEL(0) & register_map_control.HK_CTRL_FMC.SI5345_SEL(0);
        gpio2_i(7) <= SI5345_nLOL(1);
        gpio2_i(2) <= SI5345_nLOL(0);
        register_map_hk_monitor.HK_CTRL_FMC.SI5345_LOL <= (others=> (not SI5345_nLOL(0)));

        gpio2_i(11) <= SI5345_INTR_B(1);
        gpio2_i(8) <= SI5345_INTR_B(0);
        gpio2_i(20) <= PCIE_PWRBRK(0);

        with GPIO_SEL select
            leds <= register_map_control.STATUS_LEDS(NUM_LEDS(CARD_TYPE)-1 downto 0) when '0',
                GPIO_LEDS when others;

        with GPIO_SEL select
            i2cmux_rst <= (others => '1') when '0',
                (others => GPIO_I2CMUX_RSTB) when others;

        gpio_i(5) <= FAN_FAIL_B(0);
        gpio_i(6) <= FAN_FULLSP(0);
        gpio_i(7) <= sys_reset_n;
        gpio_i(8) <= FF3_PRSNT_B(0);
        gpio_i(9) <= IOEXPAN_INTR_B(0);
        gpio_i(10) <= FAN_OT_B(0);

        with GPIO_SEL select
            FAN_PWM(0) <= '1' when '0',
                GPIO_FAN_PWM when others;

        with GPIO_SEL select
            IOEXPAN_RST_B <= (others => not reset) when '0',
                GPIO_IOEXPAN_RST_B when others;

        gpio_i(13) <= IOEXPAN_INTR_B(1);
        register_map_hk_monitor.HK_CTRL_FMC.SI5345_INTR_B <= SI5345_INTR_B;
        gpio_i(14) <= TACH(0);
        gpio_i(15) <= PCIE_WAKE_B(0);

        with GPIO_SEL select
            QSPI_RST_B <= (others => not reset) when '0',
                GPIO_QSPI_RST_B when others;

    else generate
        g_NUM_SI5345: for i in 0 to NUM_SI5345(CARD_TYPE)-1 generate

            SI5345_INSEL(i*2+1 downto i*2) <= register_map_control.HK_CTRL_FMC.SI5345_INSEL(6 downto 5);
            SI5345_A(i*2+1 downto i*2)     <= std_logic_vector(to_unsigned(i,2)); --register_map_control.HK_CTRL_FMC.SI5345_A(4 downto 3);
            SI5345_OE(i)                  <= register_map_control.HK_CTRL_FMC.SI5345_OE(2);
            SI5345_RSTN(i)                <= register_map_control.HK_CTRL_FMC.SI5345_RSTN(1);
            SI5345_SEL(i)                 <= register_map_control.HK_CTRL_FMC.SI5345_SEL(0);
            g_0: if i = 0 generate
                register_map_hk_monitor.HK_CTRL_FMC.SI5345_LOL <= (others=> (not SI5345_nLOL(0)));
            end generate;
            g_lt2: if i < 2 generate
                SI5345_FINC_B(i) <= register_map_control.HK_CTRL_FMC.SI5345_FINC_B(i+10);
                SI5345_FDEC_B(i) <= register_map_control.HK_CTRL_FMC.SI5345_FDEC_B(i+8);
                register_map_hk_monitor.HK_CTRL_FMC.SI5345_INTR_B(i+12) <= SI5345_INTR_B(i);
            end generate;
        end generate;
        leds <= register_map_control.STATUS_LEDS(NUM_LEDS(CARD_TYPE)-1 downto 0);
        g_HTG710: if CARD_TYPE = 710 generate
            i2cmux_rst      <= (others => reset);
        end generate;

        g_notHTG710: if CARD_TYPE /= 710 generate
            i2cmux_rst      <= (others => not reset);
        end generate;
    end generate;

    register_map_hk_monitor.HK_CTRL_FMC.SI5345_LOL_LATCHED <=  si5345_lol_latched;

    si5345_and_MMCM_lol_latch_proc: process(appreg_clk)
    begin
        if rising_edge(appreg_clk) then
            if register_map_control.MMCM_MAIN.CLEAR = "1" then
                mmcm_main_lol_latched <= "0";
            end if;
            if MMCM_Locked_in = '0' then
                mmcm_main_lol_latched <= "1";
            end if;
            if register_map_control.HK_CTRL_FMC.CLEAR = "1" then
                si5345_lol_latched <= "0";
            end if;
            if SI5345_nLOL(0) = '0' then
                si5345_lol_latched <= "1";
            end if;
        end if;
    end process;

    --Removed in RM 4.0
    --register_map_hk_monitor.MMCM_MAIN.AUTOMATIC_CLOCK_SWITCH_ENABLED <= AUTOMATIC_CLOCK_SWITCH_ENABLED;
    --register_map_hk_monitor.MMCM_MAIN.OSC_SEL <= (others => MMCM_OscSelect_in);

    --Main MMCM Oscillator Input
    --2: LCLK fixed
    --1: TTC fixed
    --0: selectable
    process(MMCM_OscSelect_in, AUTOMATIC_CLOCK_SWITCH_ENABLED)
    begin
        if(AUTOMATIC_CLOCK_SWITCH_ENABLED = "0") then
            register_map_hk_monitor.MMCM_MAIN.MAIN_INPUT<= "00";
        elsif(MMCM_OscSelect_in = '1') then --TTC Clock
            register_map_hk_monitor.MMCM_MAIN.MAIN_INPUT<= "01";
        else
            register_map_hk_monitor.MMCM_MAIN.MAIN_INPUT<= "10";
        end if;
    end process;

    register_map_hk_monitor.MMCM_MAIN.PLL_LOCK <= (others => MMCM_Locked_in);
    register_map_hk_monitor.MMCM_MAIN.LOL_LATCHED <= mmcm_main_lol_latched;





    -- for fan speed monitoring, latest FLX712
    -- counter how many cycles for one fan revolution

    process(clk10_xtal)
    begin
        if clk10_xtal'event and clk10_xtal='1' then
            if NUM_TACH(CARD_TYPE) /= 0 then
                TACH_R <= TACH(0);
            else
                TACH_R <= '0';
            end if;
            TACH_2R <= TACH_R;
            TACH_3R <= TACH_2R;
            if TACH_2R='1' and TACH_3R='0' and TACH_FLAG='0' then
                TACH_CNT <= x"00000";
                TACH_CNT_LATCHED <= TACH_CNT;
                TACH_FLAG <= not TACH_FLAG;
            elsif TACH_2R='1' and TACH_3R='0' and TACH_FLAG='1' then
                TACH_CNT <= TACH_CNT + '1';
                TACH_FLAG <= not TACH_FLAG;
                TACH_CNT_LATCHED <= TACH_CNT_LATCHED;
            else
                TACH_CNT <= TACH_CNT + '1';
                TACH_FLAG <= TACH_FLAG;
                TACH_CNT_LATCHED <= TACH_CNT_LATCHED;
            end if;
        end if;
    end process;

    register_map_hk_monitor.TACH_CNT <= TACH_CNT_LATCHED; -- 20 bits counter



end architecture structure ; -- of housekeeping_module

