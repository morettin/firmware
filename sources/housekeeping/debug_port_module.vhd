--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Frans Schreuder
--!               RHabraken
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_unsigned.all;
    use ieee.std_logic_1164.all;
    --use work.lpgbtfpga_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;

entity debug_port_module is
    generic(
        DEBUG_MODE : boolean := false;
        GBT_NUM    : integer := 1);
    port (
        CRoutData_array         : in     txrx120b_type(0 to (GBT_NUM-1));
        GBTdata_array           : in     txrx120b_type(0 to (GBT_NUM-1));
        REFCLK_CXP1             : in     std_logic;
        SmaOut_x3               : out    std_logic;
        SmaOut_x4               : out    std_logic;
        SmaOut_x5               : out    std_logic;
        SmaOut_x6               : out    std_logic;
        TTC_in                  : in     std_logic_vector;
        clk40                   : in     std_logic;
        clk_adn_160             : in     std_logic;
        clk_ttc_40_s            : in     std_logic;
        clk_ttcfx_mon1          : in     std_logic;
        clk_ttcfx_mon2          : in     std_logic;
        register_map_40_control : in     register_map_control_type);
end entity debug_port_module;



architecture rtl of debug_port_module is

    component BUFGMUX
        port(
            I0 : in std_logic;
            I1 : in std_logic;
            O  : out std_logic;
            S  : in  std_logic
        );
    end component;

begin

    g1a: if(DEBUG_MODE = true) generate

        -- going through logic with clocks is always a bad idea...
        --SmaOut_x6 <= clk_ttcfx_mon1;          -- From Si5345
        --SmaOut_x5 <= clk_ttcfx_mon2;
        --SmaOut_x5 <= clk_ttc_40_s;            -- From the TTC MMCM
        --SmaOut_x4 <= clk40;                   -- From Main MMCM
        --SmaOut_x3 <= clk_adn_160;             -- From ADN2814

        bufg3: BUFG port map(
                I => clk_ttcfx_mon1,
                O => SmaOut_x6);

        bufg2: BUFG port map(
                I => clk_ttc_40_s,
                O => SmaOut_x5);

        bufg1: BUFG port map(
                I => clk40,
                O => SmaOut_x4);

        bufg0: BUFG port map(
                I => clk_adn_160,
                O => SmaOut_x3);

    -- bufgmux0: BUFGMUX port map(
    --  I0 => clk_adn_160,
    --  I1 => REFCLK_CXP1,
    --  O => SmaOut_x4,
    --  S => register_map_40_control.DEBUG_PORT_CLK(0)
    -- );

    --selector: process(GBTdata_array, register_map_40_control)
    --   variable index_gbt: integer range 0 to 127;
    --   variable index_clk: integer range 0 to 3;
    --begin
    --   index_gbt := to_integer(unsigned(register_map_40_control.DEBUG_PORT_GBT(6 downto 0)));
    --   index_clk := to_integer(unsigned(register_map_40_control.DEBUG_PORT_CLK(3 downto 0)));
    --   if(index_gbt <= 119) then
    --       SmaOut_x3 <= GBTdata_array(0)(index_gbt);
    --   else
    --       SmaOut_x3 <= '0';
    --   end if;
    --   case index_clk is
    --     when 0 =>
    --       SmaOut_x4 <= clk40;
    --     when 1 =>
    --       SmaOut_x4 <= clk_adn_160;
    --     when 2 =>
    --       SmaOut_x4 <= clk_ttc_40_s;
    --     when 3 =>
    --       SmaOut_x4 <= TTC_in(0); --TTC_L1accept
    --     when 4 =>
    --       SmaOut_x4 <= REFCLK_CXP1;
    --     when others => null;
    --   end case;
    --
    --
    --end process;


    end generate;



--g1b: if(DEBUG_MODE = false) generate
--    SmaOut_x3 <= TTC_in(0); --TTC_L1accept
--    SmaOut_x4 <= CRoutData_array(0)(36); --CR Output L1Accept
--end generate;

end architecture rtl ; -- of debug_port_module

