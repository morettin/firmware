--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.pcie_package.all;

entity spi_interface is
  port (
    CDCE_PD              : out    std_logic;
    CDCE_PLL_LOCK        : in     std_logic;
    CDCE_REF_SEL         : out    std_logic;
    CDCE_SYNC            : out    std_logic;
    RST                  : in     std_logic;
    SPI_RD               : out    bitfield_spi_rd_r_type;
    SPI_WR               : out    bitfield_spi_wr_r_type;
    appreg_clk           : in     std_logic;
    di_i                 : out    std_logic_vector(31 downto 0) := (others => 'X');
    do_o                 : in     std_logic_vector(31 downto 0);
    do_valid_o           : in     std_logic;
    register_map_control : in     register_map_control_type;
    rst_soft             : in     std_logic;
    wr_ack_o             : in     std_logic;
    wren_i               : out    std_logic := 'X');
end entity spi_interface;


architecture rtl of spi_interface is

  COMPONENT SPI_Fifo
    PORT (
      rst : IN std_logic;
      wr_clk : IN std_logic;
      rd_clk : IN std_logic;
      din : IN std_logic_vector(31 DOWNTO 0);
      wr_en : IN std_logic;
      rd_en : IN std_logic;
      dout : OUT std_logic_vector(31 DOWNTO 0);
      full : OUT std_logic;
      empty : OUT std_logic
    );
  END COMPONENT;
  
  signal rdfifo_din    : std_logic_vector(31 downto 0);
  signal wrfifo_dout   : std_logic_vector(31 downto 0);
  
  signal rdfifo_wren   : std_logic;
  signal rdfifo_full   : std_logic;
  signal wrfifo_rden   : std_logic;
  signal wrfifo_empty  : std_logic;
  
  signal do_valid_pipe : std_logic;
  
  type spi_wr_state_type is(IDLE, WRT, ACK1, ACK2);
  signal spi_wr_state: spi_wr_state_type := IDLE;
  signal reset: std_logic;
  signal wrfifo_wren_pipe: std_logic;
  signal wrfifo_wren     : std_logic;
  
  signal rdfifo_rden_pipe: std_logic;
  signal rdfifo_rden     : std_logic;
  signal watchdog: integer range 0 to 511;

begin

  reset <= RST or rst_soft;

  -- register_map_hk_monitor.HK_MON.CDCE_PLL_LOCK(1) <= CDCE_PLL_LOCK;
    

fifo_rd : SPI_Fifo
  PORT MAP (
    rst => reset,
    wr_clk => appreg_clk,
    rd_clk => appreg_clk,
    din => rdfifo_din,
    wr_en => rdfifo_wren,
    rd_en => rdfifo_rden,
    dout => SPI_RD.SPI_DOUT,
    full => rdfifo_full,
    empty => SPI_RD.SPI_EMPTY(32)
  );

fifo_wr : SPI_Fifo
  PORT MAP (
    rst => reset,
    wr_clk => appreg_clk,
    rd_clk => appreg_clk,
    din =>   register_map_control.SPI_WR.SPI_DIN,
    wr_en => wrfifo_wren,
    rd_en => wrfifo_rden,
    dout => wrfifo_dout,
    full => SPI_WR.SPI_FULL(32),
    empty => wrfifo_empty
  );

swap0: for i in 0 to 31 generate  
    di_i(i) <= wrfifo_dout(31 -i); --Route fifo output to SPI data input, MSB to LSB swapped
end generate;

--done in housekeeping_monitor
--register_map_hk_monitor.HK_MON(1) <= CDCE_PLL_LOCK;

wrfifo_wren <= register_map_control.SPI_WR.SPI_WREN(64) and not wrfifo_wren_pipe;
rdfifo_rden <= register_map_control.SPI_RD.SPI_RDEN(64) and not rdfifo_rden_pipe;

--CDCE_PD <= register_map_control.HK_CTRL_CDCE.PD(1);
--CDCE_SYNC <= register_map_control.HK_CTRL_CDCE.SYNC(0);
--CDCE_REF_SEL <= register_map_control.HK_CTRL_CDCE.REF_SEL(2);



oneshot: process(appreg_clk)
begin
    if(rising_edge(appreg_clk)) then
        wrfifo_wren_pipe <= register_map_control.SPI_WR.SPI_WREN(64);
        rdfifo_rden_pipe <= register_map_control.SPI_RD.SPI_RDEN(64);
    end if;
end process;

u_spi_wr: process(appreg_clk, reset)
    
begin
    if(reset = '1') then
        wrfifo_rden <= '0';
        wren_i <= '0';
        spi_wr_state <= IDLE;
        watchdog <= 0;
    elsif(rising_edge(appreg_clk)) then
        wren_i <= '0';
        wrfifo_rden <= '0';
        if(watchdog = 511) then
            spi_wr_state <= IDLE;
            watchdog <= 0;
        end if;
        watchdog <= watchdog + 1;
        case(spi_wr_state) is
            when IDLE =>
                if(wrfifo_empty = '0') then
                    spi_wr_state <= WRT;
                    wrfifo_rden <= '1'; --pulse for one clockcycle.
                else
                    spi_wr_state <= IDLE;
                end if;
                watchdog <= 0;
            when WRT =>
                wren_i <= '1'; --write data into SPI module
                spi_wr_state <= ACK1;
            when ACK1 =>
                if(wr_ack_o = '1') then 
                    spi_wr_state <= ACK2;
                else
                    spi_wr_state <= ACK1;
                end if;
            when ACK2 =>
                if(wr_ack_o = '0') then 
                    spi_wr_state <= IDLE;
                else
                    spi_wr_state <= ACK2;
                end if;
            when others =>
                spi_wr_state <= IDLE;
                
        end case;
    end if;
end process;


swap1: for i in 0 to 31 generate
    rdfifo_din(i) <= do_o(31-i);  --Route fifo input to SPI data output, lsb to msb swapped
end generate;

u_spi_rd: process(appreg_clk, reset)
begin
    if(reset = '1') then
        rdfifo_wren <= '0';
    elsif(rising_edge(appreg_clk)) then
        do_valid_pipe <= do_valid_o;
        
        if((do_valid_o = '1' and do_valid_pipe = '1') and rdfifo_full = '0') then
            rdfifo_wren <= '1';
        else
            rdfifo_wren <= '0';
        end if;
    end if;
end process;


end architecture rtl ; -- of spi_interface

