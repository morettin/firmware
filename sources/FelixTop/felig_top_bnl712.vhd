--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Shelfali Saxena
--!               mtrovato
--!               Ricardo Luz
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------
--!
--!           NIKHEF - National Institute for Subatomic Physics
--!
--!                       Electronics Department
--!
--!-----------------------------------------------------------------------------
--! @class felix_top
--!
--!
--! @author      Andrea Borga    (andrea.borga@nikhef.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)
--!              adapted by Marco Trovato (mtrovato@anl.gov)
--!
--!
--! @date        07/01/2015    created
--!
--! @version     1.0
--!
--! @brief
--! Top level for the FELIX project, containing GBT, CentralRouter and PCIe DMA core
--!
--!
--!
--! @detail
--!
--!-----------------------------------------------------------------------------
--! @TODO
--!
--!
--! ------------------------------------------------------------------------------
--
--! @brief ieee



library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.centralRouter_package.all;
    use work.axi_stream_package.all;
    use work.type_lib.ALL;                  --MT added
    use work.interlaken_package.all;        --RL added

entity felig_top_bnl712 is
    generic(
        NUMBER_OF_INTERRUPTS            : integer := 8;
        NUMBER_OF_DESCRIPTORS           : integer := 2;
        APP_CLK_FREQ                    : integer := 200;
        BUILD_DATETIME                  : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GBT_NUM                         : integer := 8; -- number of GBT channels
        AUTOMATIC_CLOCK_SWITCH          : boolean := true;
        USE_BACKUP_CLK                  : boolean := true; -- true to use 100/200 mhz board crystal, false to use TTC clock
        CARD_TYPE                       : integer := 712;
        generateTTCemu                  : boolean := false;
        GIT_HASH                        : std_logic_vector(159 downto 0) := x"0000000000000000000000000000000000000000";
        GIT_TAG                         : std_logic_vector(127 downto 0) := x"00000000000000000000000000000000";
        GIT_COMMIT_NUMBER               : integer := 0;
        COMMIT_DATETIME                 : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GTHREFCLK_SEL                   : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
        toHostTimeoutBitn               : integer := 16;
        FIRMWARE_MODE                   : integer := 0;
        PLL_SEL                         : std_logic := '1'; -- 0: CPLL, 1: QPLL
        USE_Si5324_RefCLK               : boolean := false;
        GENERATE_XOFF                   : boolean := true; -- FromHost Xoff transmission enabled on Full mode busy
        --Generics for semistatic GBT mode configurations. 1 bit per e-group to disable this type of epath at build time
        --For normal GBT mode, only the 5 LSBs are used.
        IncludeDecodingEpath2_HDLC      : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath2_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath4_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath8_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath16_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeDecodingEpath32_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeDirectDecoding           : std_logic_vector(6 downto 0) := "0000000";
        IncludeEncodingEpath2_HDLC      : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath2_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath4_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath8_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeDirectEncoding           : std_logic_vector(4 downto 0) := "00000";
        INCLUDE_TTC                     : std_logic_vector(4 downto 0) := "00000";
        INCLUDE_RD53                    : std_logic_vector(4 downto 0) := "00000";
        DATA_WIDTH                      : integer := 256;
        PCIE_LANES                      : integer := 8;
        BLOCKSIZE                       : integer := 1024;
        CHUNK_TRAILER_32B               : boolean := false;
        ENDPOINTS                       : integer := 2;
        GTREFCLKS                       : integer := 5;
        LMK_CLKS                        : integer := 8;
        SIMULATION                      : boolean := false;
        LOCK_PERIOD                     : integer := 20480;--Maximum chunk size of 2048 bytes on slowest E-link supported, used for locking 8b10b decoders
        FULL_HALFRATE                   : boolean := false;
        CREnableFromHost                : boolean := true;
        KCU_LOWER_LATENCY               : integer := 0;
        AddFULLMODEForDUNE              : boolean := false; --Add an additional FULL mode decoder without superchunk factor for DUNE
        SUPPORT_HDLC_DELAY              : boolean := false; -- support for inter-packet delays in HDLC encoders
        INCLUDE_XOFF                    : boolean := true;
        ENABLE_XVC                      : boolean := false;
        TOP_ILA                         : integer := 0
    );
    port (
        BUSY_OUT              : out    std_logic_vector(NUM_BUSY_OUTPUTS(CARD_TYPE)-1 downto 0);
        CLK40_FPGA2LMK_N      : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        CLK40_FPGA2LMK_P      : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        CLK_TTC_N             : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        CLK_TTC_P             : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        DATA_TTC_N            : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        DATA_TTC_P            : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        GTREFCLK_Si5324_N_IN  : in     std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        GTREFCLK_Si5324_P_IN  : in     std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        OPTO_LOS              : in     std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        I2C_SMB               : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        I2C_SMBUS_CFG_nEN     : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        I2C_nRESET_PCIe       : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        LMK_CLK               : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_DATA              : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_GOE               : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LD                : in     std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LE                : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_SYNCn             : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LOL_ADN               : in     std_logic_vector(NUM_ADN(CARD_TYPE)-1 downto 0);
        LOS_ADN               : in     std_logic_vector(NUM_ADN(CARD_TYPE)-1 downto 0);
        MGMT_PORT_EN          : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        NT_PORTSEL            : out    std_logic_vector(NUM_NT_PORTSEL(CARD_TYPE)-1 downto 0);
        PCIE_PERSTn_out       : out    std_logic_vector(NUM_PEX(CARD_TYPE)*2-1 downto 0);
        PEX_PERSTn            : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SCL               : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SDA               : inout  std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PORT_GOOD             : in     std_logic_vector(NUM_PEX(CARD_TYPE)*8-1 downto 0);
        Perstn_open           : in     std_logic_vector(NUM_PEX(CARD_TYPE)*2-1 downto 0); -- @suppress "Unused port: Perstn_open is not used in work.felix_top(structure)"
        GTREFCLK_N_IN             : in     std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_P_IN             : in     std_logic_vector(GTREFCLKS-1 downto 0);
        LMK_N                 : in     std_logic_vector(NUM_LMK(CARD_TYPE)*8-1 downto 0);
        LMK_P                 : in     std_logic_vector(NUM_LMK(CARD_TYPE)*8-1 downto 0);
        RX_N                      : in     std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                      : in     std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                      : out    std_logic_vector(GBT_NUM-1 downto 0);
        TX_P                      : out    std_logic_vector(GBT_NUM-1 downto 0);
        SCL                       : inout  std_logic;
        SDA                       : inout  std_logic;
        SHPC_INT              : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        SI5345_A              : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_INSEL          : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_OE             : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_RSTN           : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_SEL            : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_nLOL           : in     std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        STN0_PORTCFG          : out    std_logic_vector(NUM_STN0_PORTCFG(CARD_TYPE)-1 downto 0);
        STN1_PORTCFG          : out    std_logic_vector(NUM_STN1_PORTCFG(CARD_TYPE)-1 downto 0);
        SmaOut                : out    std_logic_vector(NUM_SMA(CARD_TYPE)-1 downto 0);
        TACH                  : in     std_logic_vector(NUM_TACH(CARD_TYPE)-1 downto 0);
        TESTMODE              : out    std_logic_vector(NUM_TESTMODE(CARD_TYPE)-1 downto 0);
        UPSTREAM_PORTSEL      : out    std_logic_vector(NUM_UPSTREAM_PORTSEL(CARD_TYPE)-1 downto 0);
        app_clk_in_n              : in     std_logic;
        app_clk_in_p              : in     std_logic;
        clk_adn_160_out_n     : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk_adn_160_out_p     : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk40_ttc_ref_out_n   : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0); -- Towards Si5345 CLKIN
        clk40_ttc_ref_out_p   : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0); -- Towards Si5345 CLKIN
        emcclk                : in     std_logic_vector(NUM_EMCCLK(CARD_TYPE)-1 downto 0);
        i2cmux_rst            : out    std_logic_vector(NUM_I2C_MUXES(CARD_TYPE)-1 downto 0);
        leds                  : out    std_logic_vector(NUM_LEDS(CARD_TYPE)-1 downto 0);
        flash_SEL             : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_a               : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*25-1 downto 0);
        flash_a_msb           : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*2-1 downto 0);
        flash_adv             : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_cclk            : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_ce              : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_d               : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*16-1 downto 0);
        flash_re              : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_we              : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        opto_inhibit          : out    std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        si5324_resetn         : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        pcie_rxn                  : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_rxp                  : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txn                  : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txp                  : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0); --! PCIe link lanes
        sys_clk_n                 : in     std_logic_vector(ENDPOINTS-1 downto 0);
        sys_clk_p                 : in     std_logic_vector(ENDPOINTS-1 downto 0); --! 100MHz PCIe reference clock
        sys_reset_n               : in     std_logic; --! Active-low system reset from PCIe interface);
        uC_reset_N            : out    std_logic_vector(NUM_UC_RESET_N(CARD_TYPE)-1 downto 0));
end entity felig_top_bnl712;


architecture structure of felig_top_bnl712 is

    constant STREAMS_TOHOST    : integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE);
    constant NUMELINKmax       : integer := 112;
    constant NUMEGROUPmax      : integer := 7;
    signal rst_hw                              : std_logic;

    signal global_reset_soft_appreg_clk        : std_logic;
    signal global_rst_soft_40                  : std_logic;

    signal clk10_xtal                          : std_logic;
    signal clk40_xtal                          : std_logic;
    signal clk40_rxusrclk                      : std_logic;  --MT added
    signal clk40                               : std_logic;
    --signal clk80                               : std_logic;--not used
    signal clk160                              : std_logic;
    signal clk240                              : std_logic;
    signal clk250                              : std_logic;
    signal clk320                              : std_logic;
    signal clk_ttc_40_s                        : std_logic;
    signal clk_adn_160                         : std_logic;
    signal global_appreg_clk                   : std_logic;

    signal global_register_map_control_appreg_clk : register_map_control_type;
    signal global_register_map_40_control      : register_map_control_type;
    signal register_map_gen_board_info         : register_map_gen_board_info_type;
    signal register_map_link_monitor           : register_map_link_monitor_type;
    signal register_map_ttc_monitor            : register_map_ttc_monitor_type;
    signal register_map_ltittc_monitor          : register_map_ltittc_monitor_type;
    signal register_map_hk_monitor             : register_map_hk_monitor_type;
    signal register_map_generators             : register_map_generators_type;

    signal TTC_path                            : std_logic_vector(9 downto 0);
    signal BUSY_OUT_s                          : std_logic;
    signal ttc_TTC_BUSY_mon_array              : busyOut_array_type(23 downto 0);
    signal TTC_BUSY_mon_array                  : busyOut_array_type(GBT_NUM-1 downto 0);

    signal MMCM_Locked_out                     : std_logic;
    signal MMCM_OscSelect_out                  : std_logic;
    signal LinkAligned                         : std_logic_vector(GBT_NUM-1 downto 0);
    signal GBT_DOWNLINK_USER_DATA              : txrx120b_type(0 to (GBT_NUM-1));
    signal GBT_UPLINK_USER_DATA                : txrx120b_type(0 to (GBT_NUM-1));
    signal GTH_FM_RX_33b_out                   : txrx33b_type(0 to (GBT_NUM-1));
    signal lpGBT_DOWNLINK_USER_DATA            : txrx224b_type(0 to GBT_NUM-1);
    signal lpGBT_DOWNLINK_IC_DATA              : txrx2b_type(0 to GBT_NUM-1);
    signal lpGBT_DOWNLINK_EC_DATA              : txrx2b_type(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_USER_DATA              : txrx32b_type(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_EC_DATA                : txrx2b_type(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_IC_DATA                : txrx2b_type(0 to GBT_NUM-1);

    signal lpGBT_UPLINK_USER_DATA_FOSEL        : txrx224b_type(0 to GBT_NUM/ENDPOINTS-1);



    signal BUSY_REQUESTs                       : busyOut_array_type(0 to (GBT_NUM-1));
    signal TTC_ToHost_Data                     : TTC_ToHost_data_type;

    signal RXUSERCLK                           : std_logic_vector(GBT_NUM-1 downto 0);
    --signal cdrlocked_out                       : std_logic;
    signal lnk_up                              : std_logic_vector(1 downto 0);

    signal dma_busy_arr : std_logic_vector(ENDPOINTS-1 downto 0);
    signal fifo_busy_arr : std_logic_vector(ENDPOINTS-1 downto 0);
    signal dma_busy : std_logic;
    signal fifo_busy : std_logic;

    signal GTREFCLK_N_s : std_logic_vector(GTREFCLKS-1 downto 0);
    signal GTREFCLK_P_s : std_logic_vector(GTREFCLKS-1 downto 0);

    signal RXUSRCLK                            : std_logic_vector(GBT_NUM-1 downto 0);
    --signal opto_los_s : std_logic_vector(OPTO_TRX-1 downto 0);

    --signal GBTFrameLocked: std_logic_vector(GBT_NUM-1 downto 0);

    --MT added: usrclks from MGT (GTB wrapper) to FELIG logic
    signal lane_control      : array_of_lane_control_type(GBT_NUM-1 downto 0);
    signal lane_monitor      : array_of_lane_monitor_type(GBT_NUM-1 downto 0);
    --signal pcie0_register_map_40_control        : register_map_control_type;
    --signal pcie0_register_map_monitor           : register_map_monitor_type;

    signal register_map_lane_remapper_output   : register_map_monitor_type;
    signal linkValid_array                   : std_logic_vector(GBT_NUM-1 downto 0);
    --MT preserve the same order as in the LPGBT Wrappwe (0 to (GBT_NUM-1));

    --signal gt_txusrclk_i                        : std_logic_vector(GBT_NUM-1 downto 0);
    --signal gt_rxusrclk_i                        : std_logic_vector(GBT_NUM-1 downto 0);
    signal TXUSRCLK                            : std_logic_vector(GBT_NUM-1 downto 0);
    --  signal link_tx_data_256b_array_i           : txrx256b_type(0 to GBT_NUM-1);
    --  signal link_tx_data_256b_array_tmp         : txrx256b_type(0 to GBT_NUM-1);
    signal link_tx_data_228b_array_tmp         : txrx228b_type(0 to GBT_NUM-1);
    signal link_rx_data_36b_array_i            : txrx36b_type(0 to GBT_NUM-1);
    signal link_rx_data_120b_array_tmp         : txrx120b_type(0 to GBT_NUM-1);
    signal lpgbt_rx_data_120b_array_tmp        : txrx120b_type(0 to GBT_NUM-1);

    signal link_tx_flag_i : std_logic_vector(GBT_NUM-1 downto 0);
    signal link_rx_flag_i : std_logic_vector(GBT_NUM-1 downto 0);

    signal RESET_TO_LMK_i : std_logic;
    --
    --RL
    signal LINKSconfig    : std_logic_vector(2 downto 0);

    signal CLK40_FPGA2LMK_N_link : std_logic;
    signal CLK40_FPGA2LMK_P_link : std_logic;
    signal CLK40_FPGA2LMK_N_hk   : std_logic;
    signal CLK40_FPGA2LMK_P_hk   : std_logic;

    COMPONENT ila_downlink IS
        PORT (
            clk : IN STD_LOGIC;
            probe0 : IN STD_LOGIC_VECTOR(223 DOWNTO 0);
            probe1 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;

begin

    NT_PORTSEL <= "111";
    TESTMODE <= "000";
    UPSTREAM_PORTSEL <= "000";
    STN0_PORTCFG <= "0Z";
    STN1_PORTCFG <= "01";

    -- assuming SmaOut x3,x4,x5,x6 corresponds to [0],[1],[2],[3]
    SmaOut(0)           <= TXUSRCLK(0);
    SmaOut(3 downto 1)  <= (others => '0');
    I2C_nRESET_PCIe(0)  <= '1';
    uC_reset_N(0)       <= '1';
    --opto_los_s <= OPTO_LOS;

    --  g_refclk_select_0: if USE_Si5324_RefCLK generate
    --    GTREFCLK_N_s(0) <= GTREFCLK_Si5324_N_IN;
    --    GTREFCLK_P_s(0) <= GTREFCLK_Si5324_P_IN;
    --  end generate;

    --  g_refclk_select_1: if USE_Si5324_RefCLK = false generate
    --    GTREFCLK_N_s(0) <= GTREFCLK_N_IN(0);
    --    GTREFCLK_P_s(0) <= GTREFCLK_P_IN(0);
    --  end generate;

    --  GTREFCLK_N_s(GTREFCLKS-1 downto 1) <= GTREFCLK_N_IN(GTREFCLKS-1 downto 1);
    --  GTREFCLK_P_s(GTREFCLKS-1 downto 1) <= GTREFCLK_P_IN(GTREFCLKS-1 downto 1);

    GTREFCLK_N_s <= GTREFCLK_N_IN;
    GTREFCLK_P_s <= GTREFCLK_P_IN;

    si5324_resetn(0)    <= not rst_hw;
    CLK40_FPGA2LMK_N(0) <= CLK40_FPGA2LMK_N_link;
    CLK40_FPGA2LMK_P(0) <= CLK40_FPGA2LMK_P_link;

    linkwrapper0: entity work.link_wrapper_FELIG
        generic map(
            GBT_NUM => GBT_NUM,
            CARD_TYPE => CARD_TYPE,
            GTHREFCLK_SEL => GTHREFCLK_SEL,
            FIRMWARE_MODE => FIRMWARE_MODE,
            PLL_SEL => PLL_SEL,
            GTREFCLKS => GTREFCLKS,
            OPTO_TRX => NUM_OPTO_LOS(CARD_TYPE))
        port map(
            register_map_control => global_register_map_40_control, --its cool
            register_map_link_monitor => register_map_link_monitor, -- needs confirnation
            clk40 => clk40,
            clk240 => clk240,
            clk40_xtal => clk40_xtal,
            GTREFCLK_N_in => GTREFCLK_N_s,
            GTREFCLK_P_in => GTREFCLK_P_s,
            rst_hw => rst_hw,
            OPTO_LOS => OPTO_LOS,
            RXUSRCLK_OUT => RXUSRCLK,
            GBT_DOWNLINK_USER_DATA => GBT_DOWNLINK_USER_DATA,
            GBT_UPLINK_USER_DATA => GBT_UPLINK_USER_DATA,
            lpGBT_DOWNLINK_USER_DATA => lpGBT_DOWNLINK_USER_DATA,
            lpGBT_DOWNLINK_IC_DATA => lpGBT_DOWNLINK_IC_DATA,
            lpGBT_DOWNLINK_EC_DATA => lpGBT_DOWNLINK_EC_DATA,
            lpGBT_UPLINK_USER_DATA => lpGBT_UPLINK_USER_DATA,
            lpGBT_UPLINK_EC_DATA => lpGBT_UPLINK_EC_DATA,
            lpGBT_UPLINK_IC_DATA => lpGBT_UPLINK_IC_DATA,
            LinkAligned => linkValid_array,
            TX_P => TX_P,
            TX_N => TX_N,
            RX_P => RX_P,
            RX_N => RX_N,
            GTH_FM_RX_33b_out => GTH_FM_RX_33b_out,
            LMK_P => LMK_P,
            LMK_N => LMK_N,
            --GBT FELIG specific
            link_rx_flag_i=>link_rx_flag_i,
            link_tx_flag_i=>link_tx_flag_i,
            TXUSRCLK_OUT => TXUSRCLK,
            appreg_clk => global_appreg_clk,
            CLK40_FPGA2LMK_N => CLK40_FPGA2LMK_N_link,
            CLK40_FPGA2LMK_P => CLK40_FPGA2LMK_P_link,
            LMK_LD => LMK_LD(0),
            RESET_TO_LMK => RESET_TO_LMK_i,
            clk40_rxusrclk => clk40_rxusrclk,
            clk320_in => '0', --simulation only
            LINKSconfig => LINKSconfig);

    clk0: entity work.clock_and_reset
        generic map(
            CARD_TYPE              => CARD_TYPE,
            APP_CLK_FREQ           => APP_CLK_FREQ,
            USE_BACKUP_CLK         => USE_BACKUP_CLK,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH)
        port map(
            MMCM_Locked_out      => MMCM_Locked_out,
            MMCM_OscSelect_out   => MMCM_OscSelect_out,
            app_clk_in_n         => app_clk_in_n,
            app_clk_in_p         => app_clk_in_p,
            --cdrlocked_in         => cdrlocked_out, --commented in master
            clk10_xtal           => clk10_xtal,--open,
            clk160               => clk160,
            clk240               => clk240,
            clk250               => clk250,
            clk320                 => clk320,
            clk40                => clk40,
            clk40_xtal           => clk40_xtal,
            clk80                => open, --clk80, not used
            clk_adn_160          => clk_adn_160,
            clk_adn_160_out_n    => clk_adn_160_out_n,
            clk_adn_160_out_p    => clk_adn_160_out_p,
            clk_ttc_40           => clk_ttc_40_s,
            --            clk_ttcfx_mon1       => open,
            --            clk_ttcfx_mon2       => open,
            clk_ttcfx_ref_out_n  => clk40_ttc_ref_out_n,
            clk_ttcfx_ref_out_p  => clk40_ttc_ref_out_p,
            register_map_control => global_register_map_control_appreg_clk,
            reset_out            => rst_hw,
            sys_reset_n          => sys_reset_n);

    TXRXDATA_inst: for I in 0 to GBT_NUM - 1 generate
        link_rx_data_120b_array_tmp(I)  <= GBT_UPLINK_USER_DATA(I)         when FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT else
                                           lpgbt_rx_data_120b_array_tmp(I) when FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT;
        lpgbt_rx_data_120b_array_tmp(I)(119 downto 36) <= (others=>'0');
        lpgbt_rx_data_120b_array_tmp(I)( 35 downto  0) <= lpGBT_UPLINK_IC_DATA(I) & lpGBT_UPLINK_EC_DATA(I) & lpGBT_UPLINK_USER_DATA(I);

        GBT_DOWNLINK_USER_DATA(I)   <= link_tx_data_228b_array_tmp(I)(119 downto 0);
        lpGBT_DOWNLINK_USER_DATA(I) <= link_tx_data_228b_array_tmp(I)(223 downto 0);
        lpGBT_DOWNLINK_EC_DATA(I)   <= link_tx_data_228b_array_tmp(I)(225 downto 224);
        lpGBT_DOWNLINK_IC_DATA(I)   <= link_tx_data_228b_array_tmp(I)(227 downto 226);
    end generate TXRXDATA_inst;

    GEN_ILA: if TOP_ILA = 1 generate
        signal ila_link_tx_data_224b_array_tmp  : txrx224b_type (3 downto 0);
        signal ila_LINKSconfig                  : std_logic_vector(2 downto 0);
        signal ila_clk                          : std_logic;
        COMPONENT ila_downlink IS
            PORT (
                clk : IN STD_LOGIC;
                probe0 : IN STD_LOGIC_VECTOR(223 DOWNTO 0);
                probe1 : IN STD_LOGIC_VECTOR(223 DOWNTO 0);
                probe2 : IN STD_LOGIC_VECTOR(223 DOWNTO 0);
                probe3 : IN STD_LOGIC_VECTOR(223 DOWNTO 0);
                probe4 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
                probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
            );
        END COMPONENT;
    begin
        CLK_LPGBT: if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT generate
            ila_clk <= clk320;
        end generate CLK_LPGBT;
        CLK_GBT: if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT generate
            ila_clk <= clk240;
        end generate CLK_GBT;
        process(ila_clk)
        begin
            if (ila_clk'event and ila_clk='1') then
                ila_link_tx_data_224b_array_tmp(0) <= link_tx_data_228b_array_tmp(0)(223 downto 0);
                ila_link_tx_data_224b_array_tmp(1) <= link_tx_data_228b_array_tmp(1)(223 downto 0);
                ila_link_tx_data_224b_array_tmp(2) <= link_tx_data_228b_array_tmp(2)(223 downto 0);
                ila_link_tx_data_224b_array_tmp(3) <= link_tx_data_228b_array_tmp(3)(223 downto 0);
                ila_LINKSconfig <= LINKSconfig;
            end if;
        end process;

        ila_dlink : ila_downlink
            port map(
                clk       => ila_clk,
                probe0    => ila_link_tx_data_224b_array_tmp(0),
                probe1    => ila_link_tx_data_224b_array_tmp(1),
                probe2    => ila_link_tx_data_224b_array_tmp(2),
                probe3    => ila_link_tx_data_224b_array_tmp(3),
                probe4    => ila_LINKSconfig,
                probe5(0) => '1'
            );
    end generate GEN_ILA;

    emulatorwrapper_i : entity work.EmulatorWrapper
        generic map(
            GBT_NUM                     => GBT_NUM,
            NUMELINKmax                 => NUMELINKmax,
            NUMEGROUPmax                => NUMEGROUPmax)
        port map(
            clk_xtal_40            => clk40_xtal,
            gt_txusrclk_in              => TXUSRCLK,--gt_txusrclk_i,
            gt_rxusrclk_in              => RXUSRCLK,--gt_rxusrclk_i,
            gt_rx_clk_div_2_mon        => open,
            gt_tx_clk_div_2_mon      => open,
            l1a_trigger_out        => open,
            link_tx_data_228b_array_out       => link_tx_data_228b_array_tmp,
            --cern-phase1: use only the ls 120b
            --NB: TX: parallel uplink data out for emulator (or FE)  (for BE TX is parallel
            --downlink data)
            link_rx_data_120b_array_in        => link_rx_data_120b_array_tmp,
            --cern-phase2: use only the ls 36b
            --NB: RX : parallel downlink data in for emulator (or FE) (for BE RX is
            --parallel uplink data)
            --tx/rx_flag_i must come from lpgbtwrapper as in phase1
            --was comming from TX/RX_FLAG_O of the gbt_wrapper (RX_FLAG_O 1
            --every 6 clock to enable scrambler input scrambler 120b (need
            --6 clks to form120b))
            --
            link_tx_flag_in                   => link_tx_flag_i, --gbt_tx_flag_i,
            link_rx_flag_in                   => link_rx_flag_i, -- gbt_rx_flag_i,
            lane_control        => lane_control,
            lane_monitor        => lane_monitor,
            register_map_control_40xtal  => global_register_map_40_control,
            --MT must come from lpgbtwrapper as in phase1 was coming from frame_lock_o.
            --rx_link_lck equivalent to frame_lock_o?
            link_frame_locked_array_in => linkValid_array,
            --MT/SS (SWAP LSB MSB)
            --fhCR_REVERSE_10B            => to_sl(pcie0_register_map_40_control.CR_REVERSE_10B.FROMHOST)
            LINKSconfig       => LINKSconfig--,
        );

    comp_LaneRegisterRemapper : entity work.LaneRegisterRemapper
        generic map(
            GBT_NUM  => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE)
        port map (
            register_map_monitor  => register_map_lane_remapper_output,
            register_map_control  => global_register_map_40_control,
            lane_control      => lane_control,
            lane_monitor      => lane_monitor,
            LINKSconfig           => LINKSconfig--,
        --    clk                   => clk40
        );

    --sync to appreg clk : pcie0_register_map_monitor goes to wupper
    --appreg_sync: process(global_appreg_clk)
    --begin
    --  if(rising_edge(global_appreg_clk)) then
    --    pcie0_register_map_monitor.register_map_generators    <= register_map_lane_remapper_output.register_map_generators; --needs to be checked. this type is no longer a thing. frans commented when he updated felig wupper.
    --pcie0_register_map_monitor.register_map_gbt_monitor  <= register_map_lane_remapper_output.register_map_gbt_monitor;
    ----commented for now as it is already done in the
    ----register_map_sync (register_map_gbt_monitor from
    ----FELIX_gbt_wrapper_KCU). Am I missing anything that was saved
    ----in lane_monitor?
    --  end if;
    --end process;


    appreg_sync: process(global_appreg_clk)
    begin
        if(rising_edge(global_appreg_clk)) then
            register_map_generators <= register_map_lane_remapper_output.register_map_generators;
        end if;
    end process;

    g_endpoints: for pcie_endpoint in 0 to ENDPOINTS-1 generate
        --signal register_map_cr_monitor             : register_map_cr_monitor_type;--not in mater
        signal register_map_crtohost_monitor       : register_map_crtohost_monitor_type;--in master
        signal register_map_crfromhost_monitor     : register_map_crfromhost_monitor_type;--in master
        signal register_map_xoff_monitor           : register_map_xoff_monitor_type;
        signal register_map_gbtemu_monitor         : register_map_gbtemu_monitor_type;
        signal register_map_decoding_monitor       : register_map_decoding_monitor_type;
        signal register_map_encoding_monitor       : register_map_encoding_monitor_type;--in master
        signal toHostFifo_din                      : slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
        signal toHostFifo_wr_en                    : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        signal toHostFifo_prog_full                : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        signal toHostFifo_wr_clk                   : std_logic;
        signal toHostFifo_rst                      : std_logic;
        signal fromHostFifo_dout                   : std_logic_vector(DATA_WIDTH-1 downto 0);
        signal fromHostFifo_rd_en                  : std_logic;
        signal fromHostFifo_empty                  : std_logic;
        signal fromHostFifo_rd_clk                 : std_logic;
        signal fromHostFifo_rst                    : std_logic;
        signal wr_data_count                       : slv12_array(0 to NUMBER_OF_DESCRIPTORS-2);
        signal decoding_aclk                       : std_logic;
        signal decoding_axis                       : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal decoding_axis_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal decoding_axis_prog_empty            : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal decoding_axis_aux                   : axis_32_array_type(0 to 1);
        signal decoding_axis_aux_tready            : axis_tready_array_type(0 to 1);
        signal decoding_axis_aux_prog_empty        : axis_tready_array_type(0 to 1);
        signal Interlaken_RX_Data                  : slv_67_array(0 to GBT_NUM - 1); --RL added to comply with decoder inputs july 2023
        signal Interlaken_RX_Datavalid             : std_logic_vector(GBT_NUM - 1 downto 0); --RL added to comply with decoder inputs july 2023
        signal toHost_axis64_tready                : axis_tready_array_type(0 to GBT_NUM/ENDPOINTS - 1); --RL added to comply with decoder inputs july 2023
        --signal encoding_aclk                       : std_logic;--in master, not needed
        --signal encoding_axis                       : axis_8_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_FROMHOST-1);--in master, not needed
        --signal encoding_axis_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_FROMHOST-1);--in master, not needed
        signal rst_soft_40                         : std_logic;
        signal reset_soft_appreg_clk               : std_logic;
        signal interrupt_call                      : std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);
        signal emu_axis            : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal emu_axis_tready     : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal emu_axis_prog_empty : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal fanout_sel_axis                       : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal fanout_sel_axis_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal fanout_sel_axis_prog_empty            : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal appreg_clk: std_logic;
        signal register_map_control_appreg_clk       : register_map_control_type;
        signal register_map_40_control               : register_map_control_type;
        signal GBT_UPLINK_USER_DATA_FOSEL : txrx120b_type (0 to GBT_NUM/ENDPOINTS-1);
        signal GBT_DOWNLINK_USER_DATA_ENCODING : txrx120b_type (0 to GBT_NUM/ENDPOINTS-1);
        signal lpGBT_DOWNLINK_USER_DATA_ENCODING : txrx32b_type(0 to GBT_NUM/ENDPOINTS-1);--in master
        signal lpGBT_DOWNLINK_IC_DATA_ENCODING   : txrx2b_type(0 to GBT_NUM/ENDPOINTS-1);--in master
        signal lpGBT_DOWNLINK_EC_DATA_ENCODING   : txrx2b_type(0 to GBT_NUM/ENDPOINTS-1);--in master
        signal lpGBT_UPLINK_USER_DATA_FOSEL      : txrx224b_type(0 to GBT_NUM/ENDPOINTS-1);--in master
        signal lpGBT_UPLINK_EC_DATA_FOSEL        : txrx2b_type(0 to GBT_NUM/ENDPOINTS-1);--in master
        signal lpGBT_UPLINK_IC_DATA_FOSEL        : txrx2b_type(0 to GBT_NUM/ENDPOINTS-1);--in master
        signal LinkAligned_FOSEL : std_logic_vector(GBT_NUM/ENDPOINTS-1 downto 0);
        signal aresetn                             : std_logic;
        signal ElinkBusy : busyOut_array_type(0 to GBT_NUM/ENDPOINTS-1);
        signal clk250_out_pcie : std_logic;
        signal CPMToWupper : CPMToWupper_type;
        signal WupperToCPM : WupperToCPM_type;

    begin
        g_assign_endpoint0: if pcie_endpoint = 0 generate
            global_appreg_clk <= appreg_clk;
            global_register_map_control_appreg_clk <= register_map_control_appreg_clk;
            global_register_map_40_control <= register_map_40_control;
            global_reset_soft_appreg_clk <= reset_soft_appreg_clk;
            global_rst_soft_40 <= rst_soft_40;
        end generate;

        aresetn <= not(rst_hw or rst_soft_40);

        pcie0: entity work.wupper
            generic map(
                NUMBER_OF_INTERRUPTS => NUMBER_OF_INTERRUPTS,
                NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
                BUILD_DATETIME => BUILD_DATETIME,
                --SVN_VERSION => 0,--not in master
                CARD_TYPE => CARD_TYPE,
                GIT_HASH => GIT_HASH,
                COMMIT_DATETIME => COMMIT_DATETIME,
                GIT_TAG => GIT_TAG,
                GIT_COMMIT_NUMBER => GIT_COMMIT_NUMBER,
                --                GBT_GENERATE_ALL_REGS => true,
                --                EMU_GENERATE_REGS => true,
                --                MROD_GENERATE_REGS => false,--in master
                GBT_NUM => GBT_NUM,--in master
                FIRMWARE_MODE => FIRMWARE_MODE,--in master
                PCIE_ENDPOINT => pcie_endpoint,
                PCIE_LANES => PCIE_LANES,
                DATA_WIDTH => DATA_WIDTH,
                SIMULATION => SIMULATION,
                BLOCKSIZE => BLOCKSIZE,
                USE_ULTRARAM => false,
                USE_VERSAL_CPM => false,
                ENABLE_XVC => ENABLE_XVC)
            port map(
                appreg_clk => appreg_clk,
                sync_clk => clk40,
                flush_fifo => open,
                interrupt_call => (others => '0'),
                lnk_up => lnk_up(pcie_endpoint),
                pcie_rxn => pcie_rxn(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_rxp => pcie_rxp(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_txn => pcie_txn(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_txp => pcie_txp(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pll_locked => open,
                register_map_control_sync => register_map_40_control,
                register_map_control_appreg_clk => register_map_control_appreg_clk,
                register_map_gen_board_info => register_map_gen_board_info,
                register_map_crtohost_monitor => register_map_crtohost_monitor,
                register_map_crfromhost_monitor => register_map_crfromhost_monitor,
                register_map_decoding_monitor => register_map_decoding_monitor,
                register_map_encoding_monitor => register_map_encoding_monitor,
                register_map_gbtemu_monitor => register_map_gbtemu_monitor,
                register_map_link_monitor => register_map_link_monitor,
                register_map_ttc_monitor => register_map_ttc_monitor,
                register_map_ltittc_monitor => register_map_ltittc_monitor,
                register_map_xoff_monitor => register_map_xoff_monitor,
                register_map_hk_monitor => register_map_hk_monitor,
                register_map_generators => register_map_generators, --register_map_lane_remapper_output.register_map_generators,--register_map_generators_c,
                wishbone_monitor => wishbone_monitor_c,
                ipbus_monitor => ipbus_monitor_c,
                regmap_mrod_monitor => regmap_mrod_monitor_c,
                reset_hard => open,
                reset_soft => open,
                reset_soft_appreg_clk => reset_soft_appreg_clk,
                reset_hw_in => rst_hw,
                sys_clk_n => sys_clk_n(pcie_endpoint),
                sys_clk_p => sys_clk_p(pcie_endpoint),
                sys_reset_n => sys_reset_n,
                tohost_busy_out => open,
                fromHostFifo_dout => open,
                fromHostFifo_empty => open,
                fromHostFifo_rd_clk => clk250_out_pcie,
                fromHostFifo_rd_en => '0',
                fromHostFifo_rst => rst_hw,
                toHostFifo_din => (others=> (others=> '0')),
                toHostFifo_prog_full => open,
                toHostFifo_rst => rst_hw,
                toHostFifo_wr_clk => clk250_out_pcie,
                toHostFifo_wr_en => (others=> '0'),
                clk250_out => clk250_out_pcie,
                master_busy_in => '0',
                CPMToWupper => CPMToWupper,
                WupperToCPM => WupperToCPM
            );

        --    decoding0: entity work.decoding
        --      generic map(
        --        GBT_NUM => GBT_NUM/ENDPOINTS,
        --        FIRMWARE_MODE => FIRMWARE_MODE,
        --        STREAMS_TOHOST => STREAMS_TOHOST,
        --        BLOCKSIZE => BLOCKSIZE,
        --        LOCK_PERIOD => LOCK_PERIOD,
        --        IncludeDecodingEpath2_HDLC =>IncludeDecodingEpath2_HDLC,
        --        IncludeDecodingEpath2_8b10b =>IncludeDecodingEpath2_8b10b,
        --        IncludeDecodingEpath4_8b10b =>IncludeDecodingEpath4_8b10b,
        --        IncludeDecodingEpath8_8b10b =>IncludeDecodingEpath8_8b10b,
        --        IncludeDecodingEpath16_8b10b =>IncludeDecodingEpath16_8b10b,
        --        IncludeDecodingEpath32_8b10b =>IncludeDecodingEpath32_8b10b)
        --      port map(
        --        RXUSRCLK                      => RXUSRCLK(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint),
        --        FULL_UPLINK_USER_DATA         => GTH_FM_RX_33b_out((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),
        --        GBT_UPLINK_USER_DATA          => GBT_UPLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),--GBT_UPLINK_USER_DATA_FOSEL,
        --        lpGBT_UPLINK_USER_DATA        => (others => (others => '0')), --lpGBT_UPLINK_USER_DATA_FOSEL TODO: connect lpGBT data : in  txrx230b_type(GBT_NUM-1 downto 0); --lpGBT data input
        --        lpGBT_UPLINK_EC_DATA          => (others => (others => '0')), --lpGBT_UPLINK_EC_DATA_FOSEL TODO: connect lpGBT data : in  txrx2b_type(GBT_NUM-1 downto 0);   --lpGBT EC data input
        --        lpGBT_UPLINK_IC_DATA          => (others => (others => '0')), --lpGBT_UPLINK_IC_DATA_FOSEL TODO: connect lpGBT data : in  txrx2b_type(GBT_NUM-1 downto 0);   --lpGBT IC data input
        --        LinkAligned                   => LinkAligned(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint),--LinkAligned_FOSEL,
        --        clk160                        => clk160,
        --        clk250                        => clk250,
        --        clk40                         => clk40,
        --        aclk                          => decoding_aclk,
        --        aresetn                       => aresetn,
        --        m_axis                        => decoding_axis,
        --        m_axis_tready                 => decoding_axis_tready,
        --        m_axis_prog_empty             => decoding_axis_prog_empty,
        --        register_map_control          => register_map_40_control,
        --        register_map_decoding_monitor => register_map_decoding_monitor);

        decoding0: entity work.decoding
            generic map(
                GBT_NUM => GBT_NUM/ENDPOINTS,
                FIRMWARE_MODE => FIRMWARE_MODE,
                STREAMS_TOHOST => STREAMS_TOHOST,
                BLOCKSIZE => BLOCKSIZE,
                LOCK_PERIOD => LOCK_PERIOD,
                IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
                IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
                IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
                IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
                IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
                IncludeDecodingEpath32_8b10b => IncludeDecodingEpath32_8b10b,
                PCIE_ENDPOINT  => pcie_endpoint
            )
            Port map(
                RXUSRCLK                        => RXUSRCLK(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint),
                FULL_UPLINK_USER_DATA           => GTH_FM_RX_33b_out((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),
                GBT_UPLINK_USER_DATA            => GBT_UPLINK_USER_DATA_FOSEL,
                lpGBT_UPLINK_USER_DATA          => lpGBT_UPLINK_USER_DATA_FOSEL,
                lpGBT_UPLINK_EC_DATA            => lpGBT_UPLINK_EC_DATA_FOSEL,
                lpGBT_UPLINK_IC_DATA            => lpGBT_UPLINK_IC_DATA_FOSEL,
                LinkAligned                     => LinkAligned_FOSEL,
                clk160                          => clk160,
                clk240                          => '0', --
                clk250                          => clk250,
                clk40                           => clk40,
                clk365                          => '0', --
                aclk_out                        => decoding_aclk,
                aresetn                         => aresetn,
                m_axis                          => decoding_axis,
                m_axis_tready                   => decoding_axis_tready,
                m_axis_prog_empty               => decoding_axis_prog_empty,
                m_axis_noSC                     => open,
                m_axis_noSC_tready              => (others =>(others=>'0')),
                m_axis_noSC_prog_empty          => open,
                TTC_ToHost_Data_in              => TTC_ToHost_Data,
                ElinkBusyIn                     => ElinkBusy,
                DmaBusyIn                       => dma_busy_arr(pcie_endpoint),
                FifoBusyIn                      => fifo_busy_arr(pcie_endpoint),
                BusySumIn                       => BUSY_OUT_s,
                m_axis_aux                      => decoding_axis_aux,
                m_axis_aux_prog_empty           => decoding_axis_aux_prog_empty,
                m_axis_aux_tready               => decoding_axis_aux_tready,
                register_map_control            => register_map_40_control,
                register_map_decoding_monitor   => register_map_decoding_monitor,

                --RL added july2023
                Interlaken_RX_Data_In           => Interlaken_RX_Data(pcie_endpoint*(GBT_NUM/ENDPOINTS) to ((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1),
                Interlaken_RX_Datavalid         => Interlaken_RX_Datavalid(((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1 downto pcie_endpoint*(GBT_NUM/ENDPOINTS)),
                Interlaken_RX_Gearboxslip       => open,
                Interlaken_Decoder_Aligned_out  => open,
                m_axis64                        => open,
                m_axis64_tready                 => toHost_axis64_tready,
                m_axis64_prog_empty             => open,
                toHost_axis64_aclk_out          => open
            --                CBOPT                         => CBOPT_vio,
            --                DIS_LANE_IN                   => DIS_LANE_vio,
            --                mask_k_char                   => mask_k_char_vio
            );



        g_DisableFullModeEmulator: if FIRMWARE_MODE /= FIRMWARE_MODE_FULL generate
            fanout_sel_axis <= decoding_axis;
            decoding_axis_tready <= fanout_sel_axis_tready;
            fanout_sel_axis_prog_empty <= decoding_axis_prog_empty;
        end generate;

        g_busy0: for i in 0 to GBT_NUM/ENDPOINTS-1 generate
            g_elinks_busy: for j in 0 to STREAMS_TOHOST-1 generate
                BUSY_REQUESTs(i + pcie_endpoint* GBT_NUM/ENDPOINTS)(j) <= fanout_sel_axis(i,j).tuser(2);
            end generate;
            g_unimplemented: for j in STREAMS_TOHOST to 51 generate
                BUSY_REQUESTs(i + pcie_endpoint* GBT_NUM/ENDPOINTS)(j) <= '0';
            end generate;
        end generate;

    end generate;

    hk0: entity work.housekeeping_module_FELIG
        generic map(
            CARD_TYPE => CARD_TYPE,
            OPTO_TRX => NUM_OPTO_LOS(CARD_TYPE),
            GBT_NUM => GBT_NUM/ENDPOINTS,
            ENDPOINTS => ENDPOINTS,
            generateTTCemu => generateTTCemu,
            --GENERATE_GBT => false,--not in master
            --wideMode => false,--not in master
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            --generate_IC_EC_TTC_only => false,--not in master
            --GTHREFCLK_SEL => GTHREFCLK_SEL,--not in master
            USE_Si5324_RefCLK => USE_Si5324_RefCLK,
            --MT added: =1 to use RXUSRCKL scaled down to 40 MHZ for TX GTREFCLK, =0
            --to use xtal_40
            --USE_LMK_RXUSRCLK => true,--RL: removed not needed with protocol
            GENERATE_XOFF => GENERATE_XOFF,
            IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
            IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
            IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
            IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
            IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
            --IncludeDecodingEpath32_8b10b => IncludeDecodingEpath32_8b10b,
            IncludeEncodingEpath2_HDLC      => IncludeEncodingEpath2_HDLC,
            IncludeEncodingEpath2_8b10b     => IncludeEncodingEpath2_8b10b,
            IncludeEncodingEpath4_8b10b     => IncludeEncodingEpath4_8b10b,
            IncludeEncodingEpath8_8b10b     => IncludeEncodingEpath8_8b10b,
            --GENERATE_FEI4B => false,--not in master
            BLOCKSIZE => BLOCKSIZE,
            CHUNK_TRAILER_32B => CHUNK_TRAILER_32B)
        --SUPER_CHUNK_FACTOR => SUPER_CHUNK_FACTOR) --not in master
        port map(
            MMCM_Locked_in => MMCM_Locked_out,
            MMCM_OscSelect_in => MMCM_OscSelect_out,
            SCL => SCL,
            SDA => SDA,
            SI5345_A => SI5345_A,
            SI5345_INSEL => SI5345_INSEL,
            SI5345_OE => SI5345_OE(0),
            SI5345_RSTN => SI5345_RSTN(0),
            SI5345_SEL => SI5345_SEL(0),
            SI5345_nLOL => SI5345_nLOL(0),
            appreg_clk => global_appreg_clk,
            emcclk => emcclk(0),
            flash_SEL => flash_SEL(0),
            flash_a => flash_a,
            flash_a_msb => flash_a_msb,
            flash_adv => flash_adv(0),
            flash_cclk => flash_cclk(0),
            flash_ce => flash_ce(0),
            flash_d => flash_d,
            flash_re => flash_re(0),
            flash_we => flash_we(0),
            i2cmux_rst => i2cmux_rst(0),
            TACH => TACH(0),
            clk10_xtal => clk10_xtal,
            clk40_xtal => clk40_xtal,
            clk40 => clk40, --MT added
            --MT added
            clk40_rxusrclk => clk40_rxusrclk,--not in master
            RESET_TO_LMK => RESET_TO_LMK_i,--not in master
            --
            leds(6 downto 0) => leds,
            leds(7) => open,
            opto_inhibit => opto_inhibit,
            --opto_los => OPTO_LOS,--commented in master
            register_map_control => global_register_map_control_appreg_clk,
            register_map_gen_board_info => register_map_gen_board_info,
            register_map_hk_monitor => register_map_hk_monitor,
            rst_soft => global_reset_soft_appreg_clk,
            sys_reset_n => sys_reset_n,
            rst_hw => rst_hw,
            CLK40_FPGA2LMK_P => open, --RL from link_wrapper
            CLK40_FPGA2LMK_N => open, --RL from link_wrapper
            LMK_DATA => LMK_DATA(0),
            LMK_CLK => LMK_CLK(0),
            LMK_LE => LMK_LE(0),
            LMK_GOE => LMK_GOE(0),
            LMK_LD => LMK_LD(0),
            LMK_SYNCn => LMK_SYNCn(0),
            I2C_SMB => I2C_SMB(0),
            I2C_SMBUS_CFG_nEN => I2C_SMBUS_CFG_nEN(0),
            MGMT_PORT_EN => MGMT_PORT_EN(0),
            PCIE_PERSTn1 => PCIE_PERSTn_out(0),
            PCIE_PERSTn2 => PCIE_PERSTn_out(1),
            PEX_PERSTn => PEX_PERSTn(0),
            PEX_SCL => PEX_SCL(0),
            PEX_SDA => PEX_SDA(0),
            PORT_GOOD => PORT_GOOD,
            SHPC_INT => SHPC_INT(0),
            lnk_up => lnk_up,
            RXUSRCLK_IN => RXUSRCLK);



end architecture structure ; -- of felig_top_bnl712

