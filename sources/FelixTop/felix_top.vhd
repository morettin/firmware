--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Soo Ryu
--!               Alexander Paramonov
--!               Kai Chen
--!               RHabraken
--!               Israel Grayzman
--!               Mesfin Gebyehu
--!               Elena Zhivun
--!               Ricardo Luz
--!               Ohad Shaked
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------
--!
--!           NIKHEF - National Institute for Subatomic Physics
--!
--!                       Electronics Department
--!
--!-----------------------------------------------------------------------------
--! @class felix_top
--!
--!
--! @author      Andrea Borga    (andrea.borga@nikhef.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)
--!
--!
--! @date        07/01/2015    created
--!
--! @version     1.0
--!
--! @brief
--! Top level for the FELIX project, containing GBT, CentralRouter and PCIe DMA core
--!
--!
--!
--! @detail
--!
--!-----------------------------------------------------------------------------
--!
--! ------------------------------------------------------------------------------
--
--! @brief ieee



library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.centralRouter_package.all;
    use work.axi_stream_package.all;
    use work.interlaken_package.all;
library xpm;
    use xpm.vcomponents.all;

entity felix_top is
    generic(
        INTERLAKEN_VERSAL_RAW_MODE      : boolean := false;
        NUMBER_OF_INTERRUPTS            : integer := 8;
        NUMBER_OF_DESCRIPTORS           : integer := 2;
        APP_CLK_FREQ                    : integer := 200;
        BUILD_DATETIME                  : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GBT_NUM                         : integer := 4; -- number of GBT channels
        AUTOMATIC_CLOCK_SWITCH          : boolean := true;
        USE_BACKUP_CLK                  : boolean := true; -- true to use 100/200 mhz board crystal, false to use TTC clock
        CARD_TYPE                       : integer := 712;
        generateTTCemu                  : boolean := false;
        GIT_HASH                        : std_logic_vector(159 downto 0) := x"0000000000000000000000000000000000000000";
        GIT_TAG                         : std_logic_vector(127 downto 0) := x"00000000000000000000000000000000";
        GIT_COMMIT_NUMBER               : integer := 0;
        COMMIT_DATETIME                 : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GTHREFCLK_SEL                   : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
        toHostTimeoutBitn               : integer := 16;
        FIRMWARE_MODE                   : integer := 0;
        PLL_SEL                         : std_logic := '1'; -- 0: CPLL, 1: QPLL
        USE_Si5324_RefCLK               : boolean := false;
        GENERATE_XOFF                   : boolean := true; -- FromHost Xoff transmission enabled on Full mode busy
        --Generics for semistatic GBT mode configurations. 1 bit per e-group to disable this type of epath at build time
        --For normal GBT mode, only the 5 LSBs are used.
        IncludeDecodingEpath2_HDLC      : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath2_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath4_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath8_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath16_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeDecodingEpath32_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeDirectDecoding           : std_logic_vector(6 downto 0) := "0000000";
        IncludeEncodingEpath2_HDLC      : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath2_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath4_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath8_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeDirectEncoding           : std_logic_vector(4 downto 0) := "00000";
        INCLUDE_TTC                     : std_logic_vector(4 downto 0) := "00000";
        TTC_SYS_SEL                     : std_logic := '0'; -- 0: TTC, 1: LTITTC P2P
        INCLUDE_RD53                    : std_logic_vector(4 downto 0) := "00000";
        DEBUGGING_RD53                  : boolean := false;
        RD53Version                     : String := "A"; --A or B
        ISTESTBEAM                      : boolean := false;
        DATA_WIDTH                      : integer := 256;
        PCIE_LANES                      : integer := 8;
        BLOCKSIZE                       : integer := 1024;
        ENDPOINTS                       : integer := 1;
        GTREFCLKS                       : integer := 5;
        --LMK_CLKS                        : integer := 8;
        SIMULATION                      : boolean := false;
        LOCK_PERIOD                     : integer := 20480;--Maximum chunk size of 2048 bytes on slowest E-link supported, used for locking 8b10b decoders
        FULL_HALFRATE                   : boolean := false;
        CREnableFromHost                : boolean := true;
        KCU_LOWER_LATENCY               : integer := 0;
        AddFULLMODEForDUNE              : boolean := false; --Add an additional FULL mode decoder without superchunk factor for DUNE
        SUPPORT_HDLC_DELAY              : boolean := false; -- support for inter-packet delays in HDLC encoders
        INCLUDE_XOFF                    : boolean := true;
        USE_VERSAL_CPM                  : boolean := false; --set to true for BNL182
        ENABLE_XVC                      : boolean := false
    );
    port (
        BUSY_OUT                  : out    std_logic_vector(NUM_BUSY_OUTPUTS(CARD_TYPE)-1 downto 0);
        CLK40_FPGA2LMK_N          : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        CLK40_FPGA2LMK_P          : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        CLK_TTC_N                 : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        CLK_TTC_P                 : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        DATA_TTC_N                : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        DATA_TTC_P                : in     std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);
        GTREFCLK_Si5324_N_IN      : in     std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        GTREFCLK_Si5324_P_IN      : in     std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        --OPTO_LOS                  : in     std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        I2C_SMB                   : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        I2C_SMBUS_CFG_nEN         : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        I2C_nRESET_PCIe           : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        LMK_CLK                   : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_DATA                  : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_GOE                   : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LD                    : in     std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_LE                    : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LMK_SYNCn                 : out    std_logic_vector(NUM_LMK(CARD_TYPE)-1 downto 0);
        LOL_ADN                   : in     std_logic_vector(NUM_ADN(CARD_TYPE)-1 downto 0);
        LOS_ADN                   : in     std_logic_vector(NUM_ADN(CARD_TYPE)-1 downto 0);
        MGMT_PORT_EN              : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        NT_PORTSEL                : out    std_logic_vector(NUM_NT_PORTSEL(CARD_TYPE)-1 downto 0);
        PCIE_PERSTn_out           : out    std_logic_vector(NUM_PEX(CARD_TYPE)*2-1 downto 0);
        PEX_PERSTn                : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SCL                   : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PEX_SDA                   : inout  std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        PORT_GOOD                 : in     std_logic_vector(NUM_PEX(CARD_TYPE)*8-1 downto 0);
        Perstn_open               : in     std_logic_vector(NUM_PEX(CARD_TYPE)*2-1 downto 0); -- @suppress "Unused port: Perstn_open is not used in work.felix_top(structure)"
        GTREFCLK_N_IN             : in     std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_P_IN             : in     std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK1_N_IN            : in     std_logic_vector(NUM_GTREFCLK1S(GTREFCLKS,FIRMWARE_MODE)-1 downto 0);
        GTREFCLK1_P_IN            : in     std_logic_vector(NUM_GTREFCLK1S(GTREFCLKS,FIRMWARE_MODE)-1 downto 0);
        LMK_N                     : in     std_logic_vector(NUM_LMK(CARD_TYPE)*6-1 downto 0);
        LMK_P                     : in     std_logic_vector(NUM_LMK(CARD_TYPE)*6-1 downto 0);
        RX_N                      : in     std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                      : in     std_logic_vector(GBT_NUM-1 downto 0);
        RX_N_LTITTC               : in     std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
        RX_P_LTITTC               : in     std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
        TX_N_LTITTC               : out    std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
        TX_P_LTITTC               : out    std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
        GTREFCLK0_LTITTC_P        : in     std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
        GTREFCLK0_LTITTC_N        : in     std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
        GTREFCLK1_LTITTC_P        : in     std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
        GTREFCLK1_LTITTC_N        : in     std_logic_vector(NUM_LTITTC_INPUTS(CARD_TYPE,TTC_SYS_SEL)-1 downto 0);
        TX_N                      : out    std_logic_vector(GBT_NUM-1 downto 0);
        TX_P                      : out    std_logic_vector(GBT_NUM-1 downto 0);
        SCL                       : inout  std_logic;
        SDA                       : inout  std_logic;
        SHPC_INT                  : out    std_logic_vector(NUM_PEX(CARD_TYPE)-1 downto 0);
        SI5345_A                  : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_INSEL              : out    std_logic_vector(NUM_SI5345(CARD_TYPE)*2-1 downto 0);
        SI5345_OE                 : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_RSTN               : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_SEL                : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_nLOL               : in     std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_FINC_B             : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_FDEC_B             : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        SI5345_INTR_B             : in     std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0);
        STN0_PORTCFG              : out    std_logic_vector(NUM_STN0_PORTCFG(CARD_TYPE)-1 downto 0);
        STN1_PORTCFG              : out    std_logic_vector(NUM_STN1_PORTCFG(CARD_TYPE)-1 downto 0);
        SmaOut                    : out    std_logic_vector(NUM_SMA(CARD_TYPE)-1 downto 0);
        TACH                      : in     std_logic_vector(NUM_TACH(CARD_TYPE)-1 downto 0);
        FAN_FAIL_B                : in     std_logic_vector(NUM_FAN_FAIL(CARD_TYPE)-1 downto 0);
        FAN_FULLSP                : in     std_logic_vector(NUM_FAN_FAIL(CARD_TYPE)-1 downto 0);
        FAN_OT_B                  : in     std_logic_vector(NUM_FAN_FAIL(CARD_TYPE)-1 downto 0);
        FAN_PWM                   : out    std_logic_vector(NUM_FAN_PWM(CARD_TYPE)-1 downto 0);
        FF3_PRSNT_B               : in     std_logic_vector(NUM_FF3_PRSTN(CARD_TYPE)-1 downto 0);
        IOEXPAN_INTR_B            : in     std_logic_vector(NUM_IOEXP(CARD_TYPE)-1 downto 0);
        IOEXPAN_RST_B             : out    std_logic_vector(NUM_IOEXP(CARD_TYPE)-1 downto 0);
        TESTMODE                  : out    std_logic_vector(NUM_TESTMODE(CARD_TYPE)-1 downto 0);
        UPSTREAM_PORTSEL          : out    std_logic_vector(NUM_UPSTREAM_PORTSEL(CARD_TYPE)-1 downto 0);
        app_clk_in_n              : in     std_logic;
        app_clk_in_p              : in     std_logic;
        clk_adn_160_out_n         : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk_adn_160_out_p         : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        clk40_ttc_ref_out_n       : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0); -- Towards Si5345 CLKIN
        clk40_ttc_ref_out_p       : out    std_logic_vector(NUM_SI5345(CARD_TYPE)-1 downto 0); -- Towards Si5345 CLKIN
        emcclk                    : in     std_logic_vector(NUM_EMCCLK(CARD_TYPE)-1 downto 0);
        i2cmux_rst                : out    std_logic_vector(NUM_I2C_MUXES(CARD_TYPE)-1 downto 0);
        leds                      : out    std_logic_vector(NUM_LEDS(CARD_TYPE)-1 downto 0);
        flash_SEL                 : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_a                   : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*25-1 downto 0);
        flash_a_msb               : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*2-1 downto 0);
        flash_adv                 : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_cclk                : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_ce                  : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_d                   : inout  std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)*16-1 downto 0);
        flash_re                  : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        flash_we                  : out    std_logic_vector(NUM_BPI_FLASH(CARD_TYPE)-1 downto 0);
        opto_inhibit              : out    std_logic_vector(NUM_OPTO_LOS(CARD_TYPE)-1 downto 0);
        si5324_resetn             : out    std_logic_vector(NUM_SI5324(CARD_TYPE)-1 downto 0);
        pcie_rxn                  : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_rxp                  : in     std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txn                  : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0);
        pcie_txp                  : out    std_logic_vector((ENDPOINTS*PCIE_LANES)-1 downto 0); --! PCIe link lanes
        sys_clk_n                 : in     std_logic_vector(ENDPOINTS-1 downto 0);
        sys_clk_p                 : in     std_logic_vector(ENDPOINTS-1 downto 0); --! 100MHz PCIe reference clock
        sys_reset_n               : in     std_logic; --! Active-low system reset from PCIe interface);
        PCIE_PWRBRK               : in     std_logic_vector(NUM_PCIE_PWRBRK(CARD_TYPE)-1 downto 0);
        PCIE_WAKE_B               : in     std_logic_vector(NUM_PCIE_PWRBRK(CARD_TYPE)-1 downto 0);
        QSPI_RST_B                : out    std_logic_vector(NUM_QSPI_RST(CARD_TYPE)-1 downto 0);
        TB_trigger_P              : in     std_logic_vector(NUM_TB_TRIGGERS(CARD_TYPE)-1 downto 0);
        TB_trigger_N              : in     std_logic_vector(NUM_TB_TRIGGERS(CARD_TYPE)-1 downto 0);
        uC_reset_N                : out    std_logic_vector(NUM_UC_RESET_N(CARD_TYPE)-1 downto 0);
        DDR_in                    : in     DDR_in_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_out                   : out    DDR_out_array_type(0 to NUM_DDR(CARD_TYPE)-1);
        DDR_inout                 : inout  DDR_inout_array_type(0 to NUM_DDR(CARD_TYPE)-1)
    );

end entity felix_top;


architecture structure of felix_top is
    function USE_ULTRARAM(brd: integer) return boolean is
    begin
        if brd = 180 or brd = 181 or brd = 182 or brd = 128 or brd = 155 then
            return true;
        else
            return false;
        end if;
    end function;

    function USE_ENCODING_DISTR_RAM(brd: integer) return boolean is
    begin
        if brd = 180 or brd = 181 or brd = 182 or brd = 128 then
            return true;
        else
            return false;
        end if;
    end function;

    function IS_VERSAL(brd: integer) return boolean is
    begin
        if brd = 180 or brd = 181 or brd = 182 or brd = 155 then
            return true;
        else
            return false;
        end if;
    end function;

    function SUPPORT_BUILT_IN_FIFO(brd: integer) return std_logic is
    begin
        if IS_VERSAL(brd) then
            return '0';
        else
            return '1';
        end if;
    end function;

    constant STREAMS_TOHOST: integer := STREAMS_TOHOST_MODE(FIRMWARE_MODE);
    constant GROUP_CONFIG_FROMHOST : IntArray(0 to 7) := STREAMS_FROMHOST_MODE(FIRMWARE_MODE);
    constant STREAMS_FROMHOST: integer := sum(GROUP_CONFIG_FROMHOST);

    signal rst_hw                              : std_logic;

    signal global_reset_soft_appreg_clk        : std_logic;
    signal global_rst_soft_40                  : std_logic;

    signal clk10_xtal                          : std_logic;
    signal clk40_xtal                          : std_logic;
    signal clk40                               : std_logic;
    signal clk100                              : std_logic; --generated by Versal CIPS block
    signal clk160                              : std_logic;
    signal clk240                              : std_logic;
    signal clk250                              : std_logic;
    signal clk365                              : std_logic;
    signal clk_ttc_40_s                        : std_logic;
    signal clk_adn_160                         : std_logic;
    signal global_appreg_clk                   : std_logic;

    signal global_register_map_control_appreg_clk : register_map_control_type;
    signal global_register_map_40_control      : register_map_control_type;
    signal register_map_gen_board_info         : register_map_gen_board_info_type;
    signal register_map_link_monitor           : register_map_link_monitor_type;
    signal register_map_ttc_monitor            : register_map_ttc_monitor_type;
    signal register_map_ltittc_monitor         : register_map_ltittc_monitor_type;
    signal register_map_hk_monitor             : register_map_hk_monitor_type;

    signal TTC_path                            : TTC_data_type;
    signal BUSY_OUT_s                          : std_logic;
    signal ttc_TTC_BUSY_mon_array              : busyOut_array_type(47 downto 0);
    signal TTC_BUSY_mon_array                  : busyOut_array_type(GBT_NUM-1 downto 0);

    signal MMCM_Locked_out                     : std_logic;
    signal MMCM_OscSelect_out                  : std_logic;
    signal LinkAligned                         : std_logic_vector(GBT_NUM-1 downto 0);
    signal GBT_DOWNLINK_USER_DATA              : txrx120b_type(0 to (GBT_NUM-1));
    signal GBT_UPLINK_USER_DATA                : txrx120b_type(0 to (GBT_NUM-1));
    signal GTH_FM_RX_33b_out                   : txrx33b_type(0 to (GBT_NUM-1));
    signal lpGBT_DOWNLINK_USER_DATA            : txrx32b_type(0 to GBT_NUM-1);
    signal lpGBT_DOWNLINK_IC_DATA              : txrx2b_type(0 to GBT_NUM-1);
    signal lpGBT_DOWNLINK_EC_DATA              : txrx2b_type(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_USER_DATA              : txrx224b_type(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_EC_DATA                : txrx2b_type(0 to GBT_NUM-1);
    signal lpGBT_UPLINK_IC_DATA                : txrx2b_type(0 to GBT_NUM-1);

    signal BUSY_REQUESTs                       : busyOut_array_type(0 to (GBT_NUM-1));
    signal TTC_ToHost_Data                     : TTC_ToHost_data_type;

    --signal cdrlocked_out                       : std_logic;
    signal lnk_up                              : std_logic_vector(1 downto 0);

    signal dma_busy_arr : std_logic_vector(ENDPOINTS-1 downto 0);
    signal fifo_busy_arr : std_logic_vector(ENDPOINTS-1 downto 0);
    signal dma_busy : std_logic;
    signal fifo_busy : std_logic;

    signal GTREFCLK_N_s : std_logic_vector(GTREFCLKS-1 downto 0);
    signal GTREFCLK_P_s : std_logic_vector(GTREFCLKS-1 downto 0);

    signal RXUSRCLK                            : std_logic_vector(GBT_NUM-1 downto 0);
    signal versal_sys_reset_n : std_logic;
    signal PCIE_PERSTn : std_logic;
    signal xoff_global : std_logic_vector(GBT_NUM-1 downto 0);

    signal LTI_TXUSRCLK: std_logic_vector(GBT_NUM-1 downto 0);
    signal WupperToCPM: WupperToCPM_array_type(0 to 1);
    signal CPMToWupper: CPMToWupper_array_type(0 to 1);


    --signal GBTFrameLocked: std_logic_vector(GBT_NUM-1 downto 0);
    --COMPONENT vio_cbopt
    --PORT (
    --    clk : IN STD_LOGIC;
    --    probe_in0 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    --    probe_out0 : out STD_LOGIC_VECTOR(3 DOWNTO 0)
    --);
    --END COMPONENT  ;

    --signal aclk_s : std_logic;


    --signal CBOPT_vio, CBOPT_tmp : std_logic_vector(3 downto 0);
    --signal DIS_LANE_vio, DIS_LANE_tmp : std_logic_vector(3 downto 0);
    --signal mask_k_char_vio, mask_k_char_tmp : std_logic_vector(3 downto 0);

    signal tb_trigger_i : std_logic_vector(NUM_TB_TRIGGERS(CARD_TYPE)-1 downto 0);

    function get_trigtag_mode return integer is
    begin
        if FIRMWARE_MODE = FIRMWARE_MODE_STRIP then
            return 2;           -- ABC*
        elsif FIRMWARE_MODE = FIRMWARE_MODE_PIXEL then
            if RD53Version = "A" then
                return 0;       -- RD53A
            else
                return 1;       -- ITkPixV1 (RD53B) and newer
            end if;
        else
            return -1;
        end if;
    end function;
    signal LTI_TX_Data_Transceiver : array_32b(0 to GBT_NUM - 1);
    signal LTI_TX_TX_CharIsK : array_4b(0 to GBT_NUM-1);
    signal Interlaken_RX_Data : slv_67_array(0 to GBT_NUM-1);
    signal Interlaken_RX_Datavalid : std_logic_vector(GBT_NUM-1 downto 0);
    signal Interlaken_RX_Gearboxslip : std_logic_vector(GBT_NUM - 1 downto 0);
    signal Interlaken_Decoder_Aligned: std_logic_vector(GBT_NUM-1 downto 0);

begin

    NT_PORTSEL <= (others => '1');
    TESTMODE <= (others => '0');
    UPSTREAM_PORTSEL <= (others => '0');
    g_SNT0_PORTCFG: if NUM_STN0_PORTCFG(CARD_TYPE) = 2 generate
        STN0_PORTCFG <= "0Z";
    end generate;
    g_SNT1_PORTCFG: if NUM_STN1_PORTCFG(CARD_TYPE) = 2 generate
        STN1_PORTCFG <= "01";
    end generate;
    SmaOut <= (others => '0');
    I2C_nRESET_PCIe <= (others => '1');
    uC_reset_N <= (others => '1');

    g_versal: if CARD_TYPE = 180 or CARD_TYPE = 155 generate
        PCIE_PERSTn <= versal_sys_reset_n;
    else generate
        PCIE_PERSTn <= sys_reset_n;
    end generate;

    --opto_los_s <= OPTO_LOS;

    g_refclk_select_0: if USE_Si5324_RefCLK generate
        GTREFCLK_N_s(0) <= GTREFCLK_Si5324_N_IN(0);
        GTREFCLK_P_s(0) <= GTREFCLK_Si5324_P_IN(0);
    end generate;

    g_refclk_select_1: if USE_Si5324_RefCLK = false generate
        GTREFCLK_N_s(0) <= GTREFCLK_N_IN(0);
        GTREFCLK_P_s(0) <= GTREFCLK_P_IN(0);
    end generate;

    GTREFCLK_N_s(GTREFCLKS-1 downto 1) <= GTREFCLK_N_IN(GTREFCLKS-1 downto 1);
    GTREFCLK_P_s(GTREFCLKS-1 downto 1) <= GTREFCLK_P_IN(GTREFCLKS-1 downto 1);

    linkwrapper0: entity work.link_wrapper
        generic map(
            INTERLAKEN_VERSAL_RAW_MODE => INTERLAKEN_VERSAL_RAW_MODE,
            LINK_NUM => GBT_NUM,
            CARD_TYPE => CARD_TYPE,
            GTHREFCLK_SEL => GTHREFCLK_SEL,
            FIRMWARE_MODE => FIRMWARE_MODE,
            FULL_HALFRATE => FULL_HALFRATE,
            PLL_SEL => PLL_SEL,
            GTREFCLKS => GTREFCLKS,
            GTREFCLK1S => NUM_GTREFCLK1S(GTREFCLKS,FIRMWARE_MODE),
            KCU_LOWER_LATENCY => KCU_LOWER_LATENCY
        )
        port map(
            register_map_control => global_register_map_40_control,
            register_map_link_monitor => register_map_link_monitor,
            clk40 => clk40,
            clk100 => clk100,
            --clk240 => clk240,
            clk250 => clk250,
            clk40_xtal => clk40_xtal,
            GTREFCLK_N_in => GTREFCLK_N_s,
            GTREFCLK_P_in => GTREFCLK_P_s,
            GTREFCLK1_N_in => GTREFCLK1_N_IN,
            GTREFCLK1_P_in => GTREFCLK1_P_IN,
            rst_hw => rst_hw,
            TXUSRCLK_OUT => LTI_TXUSRCLK,
            --OPTO_LOS => OPTO_LOS,
            RXUSRCLK_OUT => RXUSRCLK,
            GBT_DOWNLINK_USER_DATA => GBT_DOWNLINK_USER_DATA,
            GBT_UPLINK_USER_DATA => GBT_UPLINK_USER_DATA,
            lpGBT_DOWNLINK_USER_DATA => lpGBT_DOWNLINK_USER_DATA,
            lpGBT_DOWNLINK_IC_DATA => lpGBT_DOWNLINK_IC_DATA,
            lpGBT_DOWNLINK_EC_DATA => lpGBT_DOWNLINK_EC_DATA,
            lpGBT_UPLINK_USER_DATA => lpGBT_UPLINK_USER_DATA,
            lpGBT_UPLINK_EC_DATA => lpGBT_UPLINK_EC_DATA,
            lpGBT_UPLINK_IC_DATA => lpGBT_UPLINK_IC_DATA,
            LinkAligned => LinkAligned,
            TX_P => TX_P,
            TX_N => TX_N,
            RX_P => RX_P,
            RX_N => RX_N,
            GTH_FM_RX_33b_out => GTH_FM_RX_33b_out,
            LMK_P => LMK_P,
            LMK_N => LMK_N,
            LTI_TX_Data_Transceiver_In      => LTI_TX_Data_Transceiver,
            Interlaken_Data_Transceiver_Out => Interlaken_RX_Data,
            Interlaken_RX_Datavalid_Out     => Interlaken_RX_Datavalid,
            Interlaken_RX_Gearboxslip       => Interlaken_RX_Gearboxslip,
            Interlaken_Decoder_Aligned_in => Interlaken_Decoder_Aligned,
            LTI_TX_TX_CharIsK_in            => LTI_TX_TX_CharIsK
        );

    clk0: entity work.clock_and_reset
        generic map(
            APP_CLK_FREQ           => APP_CLK_FREQ,
            USE_BACKUP_CLK         => USE_BACKUP_CLK,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH,
            CARD_TYPE              => CARD_TYPE,
            FIRMWARE_MODE          => FIRMWARE_MODE)
        port map(
            MMCM_Locked_out      => MMCM_Locked_out,
            MMCM_OscSelect_out   => MMCM_OscSelect_out,
            app_clk_in_n         => app_clk_in_n,
            app_clk_in_p         => app_clk_in_p,
            --cdrlocked_in         => cdrlocked_out,
            clk10_xtal           => clk10_xtal,--open,
            clk100               => open,
            clk160               => clk160,
            clk240               => clk240,
            clk250               => clk250,
            clk320               => open,
            clk365               => clk365,
            clk40                => clk40,
            clk40_xtal           => clk40_xtal,
            clk80                => open,
            clk_adn_160          => clk_adn_160,
            clk_adn_160_out_n    => clk_adn_160_out_n,
            clk_adn_160_out_p    => clk_adn_160_out_p,
            clk_ttc_40           => clk_ttc_40_s,
            clk_ttcfx_ref_out_n  => clk40_ttc_ref_out_n,
            clk_ttcfx_ref_out_p  => clk40_ttc_ref_out_p,
            register_map_control => global_register_map_control_appreg_clk,
            reset_out            => rst_hw,
            sys_reset_n          => PCIE_PERSTn);

    g_endpoints: for pcie_endpoint in 0 to ENDPOINTS-1 generate
        signal register_map_crtohost_monitor       : register_map_crtohost_monitor_type;
        signal register_map_crfromhost_monitor     : register_map_crfromhost_monitor_type;
        signal register_map_xoff_monitor           : register_map_xoff_monitor_type;
        signal register_map_gbtemu_monitor         : register_map_gbtemu_monitor_type;
        signal register_map_decoding_monitor       : register_map_decoding_monitor_type;
        signal register_map_encoding_monitor       : register_map_encoding_monitor_type;


        signal toHostFifo_din                      : slv_array(0 to NUMBER_OF_DESCRIPTORS-2);
        signal toHostFifo_wr_en                    : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        signal toHostFifo_prog_full                : std_logic_vector(NUMBER_OF_DESCRIPTORS-2 downto 0);
        signal toHostFifo_wr_clk                   : std_logic;
        signal toHostFifo_rst                      : std_logic;
        signal fromHostFifo_dout                   : std_logic_vector(DATA_WIDTH-1 downto 0);
        signal fromHostFifo_rd_en                  : std_logic;
        signal fromHostFifo_empty                  : std_logic;
        signal fromHostFifo_rd_clk                 : std_logic;
        signal fromHostFifo_rst                    : std_logic;
        signal decoding_aclk                       : std_logic;
        --signal decoding_aclk64                     : std_logic; --!TODO: Connect to 400MHz clock -- @suppress "signal decoding_aclk64 is never written"
        signal decoding_axis                       : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal decoding_axis_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal decoding_axis_prog_empty            : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal decoding_axis_noSC                  : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal decoding_axis_noSC_tready           : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal decoding_axis_noSC_prog_empty       : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal decoding_axis_aux                   : axis_32_array_type(0 to 1);
        signal decoding_axis_aux_tready            : axis_tready_array_type(0 to 1);
        signal decoding_axis_aux_prog_empty        : axis_tready_array_type(0 to 1);
        signal encoding_aclk                       : std_logic;
        signal encoding_axis                       : axis_8_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_FROMHOST-1);
        signal encoding_axis_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_FROMHOST-1);
        signal rst_soft_40                         : std_logic;
        signal reset_soft_appreg_clk               : std_logic;
        signal interrupt_call                      : std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);

        signal emu_axis            : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal emu_axis_tready     : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal emu_axis_prog_empty : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal fanout_sel_axis                       : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal fanout_sel_axis_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal fanout_sel_axis_prog_empty            : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal emu_axis_noSC            : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal emu_axis_noSC_tready     : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal emu_axis_noSC_prog_empty : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1);
        signal fanout_sel_axis_noSC                       : axis_32_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1); -- @suppress "signal fanout_sel_axis_noSC is never read"
        signal fanout_sel_axis_noSC_tready                : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1); -- @suppress "signal fanout_sel_axis_noSC_tready is never written"
        signal fanout_sel_axis_noSC_prog_empty            : axis_tready_2d_array_type(0 to GBT_NUM/ENDPOINTS-1, 0 to STREAMS_TOHOST-1); -- @suppress "signal fanout_sel_axis_noSC_prog_empty is never read"
        signal appreg_clk: std_logic;

        signal register_map_control_appreg_clk       : register_map_control_type;
        signal register_map_40_control               : register_map_control_type;

        signal GBT_UPLINK_USER_DATA_FOSEL : txrx120b_type (0 to GBT_NUM/ENDPOINTS-1);
        signal GBT_DOWNLINK_USER_DATA_ENCODING : txrx120b_type (0 to GBT_NUM/ENDPOINTS-1);
        signal lpGBT_DOWNLINK_USER_DATA_ENCODING : txrx32b_type(0 to GBT_NUM/ENDPOINTS-1);
        signal lpGBT_DOWNLINK_IC_DATA_ENCODING   : txrx2b_type(0 to GBT_NUM/ENDPOINTS-1);
        signal lpGBT_DOWNLINK_EC_DATA_ENCODING   : txrx2b_type(0 to GBT_NUM/ENDPOINTS-1);
        signal lpGBT_UPLINK_USER_DATA_FOSEL      : txrx224b_type(0 to GBT_NUM/ENDPOINTS-1);
        signal lpGBT_UPLINK_EC_DATA_FOSEL        : txrx2b_type(0 to GBT_NUM/ENDPOINTS-1);
        signal lpGBT_UPLINK_IC_DATA_FOSEL        : txrx2b_type(0 to GBT_NUM/ENDPOINTS-1);

        signal LinkAligned_FOSEL : std_logic_vector(GBT_NUM/ENDPOINTS-1 downto 0);
        signal aresetn                             : std_logic;
        signal ElinkBusy : busyOut_array_type(0 to GBT_NUM/ENDPOINTS-1);
        signal fromhost_aresetn, fromhost_aresetn_sync: std_logic; --Separate reset path for CRFromHost and Encoding
        signal dma_enable: std_logic_vector(NUMBER_OF_DESCRIPTORS-1 downto 0);
        --signal fromHost_axis64 : axis_64_array_type(0 to GBT_NUM/ENDPOINTS - 1);
        --signal fromHost_axis64_aclk : std_logic;
        --signal fromHost_axis64_tready : axis_tready_array_type(0 to GBT_NUM/ENDPOINTS - 1);
        signal toHost_axis64_aclk : std_logic;
        signal toHost_axis64 : axis_64_array_type(0 to GBT_NUM/ENDPOINTS - 1);
        signal toHost_axis64_tready : axis_tready_array_type(0 to GBT_NUM/ENDPOINTS - 1);
        signal toHost_axis64_prog_empty : axis_tready_array_type(0 to GBT_NUM/ENDPOINTS - 1);
        signal FE_BUSY_endpoint, FE_BUSY_emulator: busyOut_array_type(0 to GBT_NUM/ENDPOINTS-1);
    begin

        fromhost_aresetn <= not (to_sl(register_map_40_control.CRFROMHOST_RESET) or rst_hw);
        sync_fromhost_aresetn : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 0,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => fromhost_aresetn,
                dest_clk => fromHostFifo_rd_clk,
                dest_rst => fromhost_aresetn_sync
            );

        ElinkBusy <= BUSY_REQUESTs(pcie_endpoint*GBT_NUM/ENDPOINTS to pcie_endpoint*GBT_NUM/ENDPOINTS+(GBT_NUM/ENDPOINTS)-1);

        g_assign_endpoint0: if pcie_endpoint = 0 generate
            global_appreg_clk <= appreg_clk;
            global_register_map_control_appreg_clk <= register_map_control_appreg_clk;
            global_register_map_40_control <= register_map_40_control;
            global_reset_soft_appreg_clk <= reset_soft_appreg_clk;
            global_rst_soft_40 <= rst_soft_40;
        end generate;

        aresetn <= not(rst_hw or rst_soft_40);

        pcie0: entity work.wupper
            generic map(
                NUMBER_OF_INTERRUPTS => NUMBER_OF_INTERRUPTS,
                NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
                BUILD_DATETIME => BUILD_DATETIME,
                CARD_TYPE => CARD_TYPE,
                GIT_HASH => GIT_HASH,
                COMMIT_DATETIME => COMMIT_DATETIME,
                GIT_TAG => GIT_TAG,
                GIT_COMMIT_NUMBER => GIT_COMMIT_NUMBER,
                GBT_NUM => GBT_NUM,
                FIRMWARE_MODE => FIRMWARE_MODE,
                PCIE_ENDPOINT => pcie_endpoint,
                PCIE_LANES => PCIE_LANES,
                DATA_WIDTH => DATA_WIDTH,
                SIMULATION => SIMULATION,
                BLOCKSIZE => BLOCKSIZE,
                USE_ULTRARAM => USE_ULTRARAM(CARD_TYPE),
                USE_VERSAL_CPM => USE_VERSAL_CPM,
                ENABLE_XVC => ENABLE_XVC
            )
            port map(
                appreg_clk => appreg_clk,
                sync_clk => clk40,
                flush_fifo => open,
                interrupt_call => interrupt_call,
                lnk_up => lnk_up(pcie_endpoint),
                pcie_rxn => pcie_rxn(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_rxp => pcie_rxp(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_txn => pcie_txn(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pcie_txp => pcie_txp(pcie_endpoint*8+(PCIE_LANES-1) downto pcie_endpoint*8),
                pll_locked => open,
                register_map_control_sync => register_map_40_control,
                register_map_control_appreg_clk => register_map_control_appreg_clk,
                register_map_gen_board_info => register_map_gen_board_info,
                register_map_crtohost_monitor => register_map_crtohost_monitor,
                register_map_crfromhost_monitor => register_map_crfromhost_monitor,
                register_map_decoding_monitor => register_map_decoding_monitor,
                register_map_encoding_monitor => register_map_encoding_monitor,
                register_map_gbtemu_monitor => register_map_gbtemu_monitor,
                register_map_link_monitor => register_map_link_monitor,
                register_map_ttc_monitor => register_map_ttc_monitor,
                register_map_ltittc_monitor => register_map_ltittc_monitor,
                register_map_xoff_monitor => register_map_xoff_monitor,
                register_map_hk_monitor => register_map_hk_monitor,
                register_map_generators => register_map_generators_c,
                wishbone_monitor => wishbone_monitor_c,
                regmap_mrod_monitor => regmap_mrod_monitor_c,
                ipbus_monitor => ipbus_monitor_c,
                dma_enable_out => dma_enable,
                reset_hard => open,
                reset_soft => rst_soft_40,
                reset_soft_appreg_clk => reset_soft_appreg_clk,
                reset_hw_in => rst_hw,
                sys_clk_n => sys_clk_n(pcie_endpoint),
                sys_clk_p => sys_clk_p(pcie_endpoint),
                sys_reset_n => PCIE_PERSTn,
                tohost_busy_out => dma_busy_arr(pcie_endpoint),
                fromHostFifo_dout => fromHostFifo_dout,
                fromHostFifo_empty => fromHostFifo_empty,
                fromHostFifo_rd_clk => fromHostFifo_rd_clk,
                fromHostFifo_rd_en => fromHostFifo_rd_en,
                fromHostFifo_rst => fromHostFifo_rst,
                toHostFifo_din => toHostFifo_din,
                toHostFifo_prog_full => toHostFifo_prog_full,
                toHostFifo_rst => toHostFifo_rst,
                toHostFifo_wr_clk => toHostFifo_wr_clk,
                toHostFifo_wr_en => toHostFifo_wr_en,
                clk250_out => open,
                master_busy_in => BUSY_OUT_s,
                toHostFifoBusy_out => fifo_busy_arr(pcie_endpoint),
                CPMToWupper => CPMToWupper(pcie_endpoint),
                WupperToCPM => WupperToCPM(pcie_endpoint)
            );


        g_gbtEmu: if (FIRMWARE_MODE = FIRMWARE_MODE_GBT and (GBT_NUM < 48)) or
                     FIRMWARE_MODE = FIRMWARE_MODE_LTDB or
                     FIRMWARE_MODE = FIRMWARE_MODE_FEI4 or
                     FIRMWARE_MODE = FIRMWARE_MODE_PIXEL or
                     (FIRMWARE_MODE = FIRMWARE_MODE_STRIP and ((GBT_NUM < 24) or (CARD_TYPE /= 712))) or --Save some memory for 24ch strip firmware on FLX712
                     FIRMWARE_MODE = FIRMWARE_MODE_LPGBT or
                     FIRMWARE_MODE = FIRMWARE_MODE_BCM_PRIME generate

            signal emuToHost_GBTdata: std_logic_vector(119 downto 0);
            signal emuToHost_lpGBTdata: std_logic_vector(223 downto 0);
            signal emuToHost_lpGBTECdata: std_logic_vector(1 downto 0);
            signal emuToHost_lpGBTICdata: std_logic_vector(1 downto 0);
            signal emuToHost_GBTlinkValid : std_logic;
            signal emuToFrontEnd_GBTdata: std_logic_vector(119 downto 0);
            signal emuToFrontEnd_lpGBTdata: std_logic_vector(31 downto 0);
            signal emuToFrontEnd_lpGBTECdata: std_logic_vector(1 downto 0);
            signal emuToFrontEnd_lpGBTICdata: std_logic_vector(1 downto 0);
            signal emuToFrontEnd_linkValid : std_logic;
        begin
            gbtEmuToHost0: entity work.GBTdataEmulator
                generic map(
                    EMU_DIRECTION      => "ToHost",
                    FIRMWARE_MODE => FIRMWARE_MODE,
                    MEM_DEPTH => 16384,
                    LPGBT_TOHOST_WIDTH => 32,
                    LPGBT_TOFE_WIDTH => 8
                )
                port map(
                    clk40 => clk40,
                    wrclk => clk40,
                    rst_hw => rst_hw,
                    rst_soft => rst_soft_40,
                    xoff => '0',
                    register_map_control => register_map_40_control,
                    GBTdata => emuToHost_GBTdata,
                    lpGBTdataToFE => open,
                    lpGBTdataToHost => emuToHost_lpGBTdata,
                    lpGBTECdata => emuToHost_lpGBTECdata,
                    lpGBTICdata => emuToHost_lpGBTICdata,
                    GBTlinkValid => emuToHost_GBTlinkValid);

            gbtFoSelToHost0: entity work.GBT_fanout_selector
                generic map(
                    GBT_NUM            => GBT_NUM/ENDPOINTS)
                port map(
                    GBTDataIn => GBT_UPLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1), --: in     txrx120b_type(0 to GBT_NUM-1);
                    lpGBTDataToFEIn => (others => (others => '0')),
                    lpGBTDataToHostIn => lpGBT_UPLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),
                    lpGBTECDataIn => lpGBT_UPLINK_EC_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),
                    lpGBTICDataIn => lpGBT_UPLINK_IC_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),
                    GBTLinkValidIn => LinkAligned(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint), --: in     std_logic_vector(GBT_NUM-1 downto 0);
                    EMU_GBTlinkValidIn => emuToHost_GBTlinkValid, --: in     std_logic;
                    EMU_GBTDataIn => emuToHost_GBTdata, --: in     std_logic_vector(119 downto 0);
                    EMU_lpGBTDataToFEIn => (others => '0'),
                    EMU_lpGBTDataToHostIn => emuToHost_lpGBTdata,
                    EMU_lpGBTECDataIn => emuToHost_lpGBTECdata,
                    EMU_lpGBTICDataIn => emuToHost_lpGBTICdata,
                    GBTDataOut => GBT_UPLINK_USER_DATA_FOSEL, --: out    txrx120b_type(0 to (GBT_NUM-1));
                    lpGBTDataToFEOut => open,
                    lpGBTDataToHostOut => lpGBT_UPLINK_USER_DATA_FOSEL,
                    lpGBTECDataOut => lpGBT_UPLINK_EC_DATA_FOSEL,
                    lpGBTICDataOut => lpGBT_UPLINK_IC_DATA_FOSEL,
                    GBTLinkValidOut => LinkAligned_FOSEL, --: out    std_logic_vector(0 to (GBT_NUM-1));
                    clk40 => clk40, --: in     std_logic;
                    sel => register_map_40_control.GBT_TOHOST_FANOUT.SEL(GBT_NUM/ENDPOINTS-1 downto 0));

            gbtEmuToFrontEnd0: entity work.GBTdataEmulator
                generic map(
                    EMU_DIRECTION      => "ToFrontEnd",
                    FIRMWARE_MODE => FIRMWARE_MODE,
                    MEM_DEPTH => 16384,
                    LPGBT_TOHOST_WIDTH => 32,
                    LPGBT_TOFE_WIDTH => 8
                )
                port map(
                    clk40 => clk40,
                    wrclk => clk40,
                    rst_hw => rst_hw,
                    rst_soft => rst_soft_40,
                    xoff => '0',
                    register_map_control => register_map_40_control,
                    GBTdata => emuToFrontEnd_GBTdata,
                    lpGBTdataToFE => emuToFrontEnd_lpGBTdata,
                    lpGBTdataToHost => open,
                    lpGBTECdata => emuToFrontEnd_lpGBTECdata,
                    lpGBTICdata => emuToFrontEnd_lpGBTICdata,
                    GBTlinkValid => emuToFrontEnd_linkValid);

            gbtFoSelToFrontEnd0: entity work.GBT_fanout_selector
                generic map(
                    GBT_NUM            => GBT_NUM/ENDPOINTS)
                port map(
                    GBTDataIn               => GBT_DOWNLINK_USER_DATA_ENCODING,
                    lpGBTDataToFEIn         => lpGBT_DOWNLINK_USER_DATA_ENCODING,
                    lpGBTDataToHostIn       => (others => (others => '0')),
                    lpGBTECDataIn           => lpGBT_DOWNLINK_EC_DATA_ENCODING,
                    lpGBTICDataIn           => lpGBT_DOWNLINK_IC_DATA_ENCODING,
                    GBTLinkValidIn          => LinkAligned(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint),--: in     std_logic_vector(GBT_NUM-1 downto 0);
                    EMU_GBTlinkValidIn      => emuToFrontEnd_linkValid,--: in     std_logic;
                    EMU_GBTDataIn           => emuToFrontEnd_GBTdata,--: in     std_logic_vector(119 downto 0);
                    EMU_lpGBTDataToFEIn     => emuToFrontEnd_lpGBTdata,--: in     std_logic_vector(119 downto 0);
                    EMU_lpGBTDataToHostIn   => (others => '0'),--: in     std_logic_vector(119 downto 0);
                    EMU_lpGBTECDataIn       => emuToFrontEnd_lpGBTECdata,--: in     std_logic_vector(119 downto 0);
                    EMU_lpGBTICDataIn       => emuToFrontEnd_lpGBTICdata,--: in     std_logic_vector(119 downto 0);
                    GBTDataOut              => GBT_DOWNLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),--: out    txrx120b_type(0 to (GBT_NUM-1));
                    lpGBTDataToFEOut        => lpGBT_DOWNLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),--: out    txrx120b_type(0 to (GBT_NUM-1));
                    lpGBTDataToHostOut      => open,
                    lpGBTECDataOut          => lpGBT_DOWNLINK_EC_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),--: out    txrx120b_type(0 to (GBT_NUM-1));
                    lpGBTICDataOut          => lpGBT_DOWNLINK_IC_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),--: out    txrx120b_type(0 to (GBT_NUM-1));
                    GBTLinkValidOut         => open,--: out    std_logic_vector(0 to (GBT_NUM-1));
                    clk40                   => clk40,--: in     std_logic;
                    sel                     => register_map_40_control.GBT_TOFRONTEND_FANOUT.SEL(GBT_NUM/ENDPOINTS-1 downto 0));

        else generate
            LinkAligned_FOSEL <= LinkAligned(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint);
            GBT_UPLINK_USER_DATA_FOSEL <= GBT_UPLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1);

            GBT_DOWNLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1) <= GBT_DOWNLINK_USER_DATA_ENCODING;
            lpGBT_UPLINK_USER_DATA_FOSEL <= lpGBT_UPLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1);
            lpGBT_UPLINK_IC_DATA_FOSEL <= lpGBT_UPLINK_IC_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1);
            lpGBT_UPLINK_EC_DATA_FOSEL <= lpGBT_UPLINK_EC_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1);
            lpGBT_DOWNLINK_USER_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1) <= lpGBT_DOWNLINK_USER_DATA_ENCODING;
            lpGBT_DOWNLINK_IC_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1) <= lpGBT_DOWNLINK_IC_DATA_ENCODING;
            lpGBT_DOWNLINK_EC_DATA((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1) <= lpGBT_DOWNLINK_EC_DATA_ENCODING;
        --

        end generate;

        decoding0: entity work.decoding
            generic map(
                CARD_TYPE => CARD_TYPE,
                GBT_NUM => GBT_NUM/ENDPOINTS,
                FIRMWARE_MODE => FIRMWARE_MODE,
                STREAMS_TOHOST => STREAMS_TOHOST,
                BLOCKSIZE => BLOCKSIZE,
                LOCK_PERIOD => LOCK_PERIOD,
                IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
                IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
                IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
                IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
                IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
                IncludeDecodingEpath32_8b10b => IncludeDecodingEpath32_8b10b,
                IncludeDirectDecoding => IncludeDirectDecoding,
                RD53Version => RD53Version,
                PCIE_ENDPOINT  => pcie_endpoint,
                VERSAL => IS_VERSAL(CARD_TYPE),
                AddFULLMODEForDUNE => AddFULLMODEForDUNE
            )
            Port map(
                RXUSRCLK                      => RXUSRCLK(((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1 downto (GBT_NUM/ENDPOINTS)*pcie_endpoint),
                FULL_UPLINK_USER_DATA         => GTH_FM_RX_33b_out((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),
                GBT_UPLINK_USER_DATA          => GBT_UPLINK_USER_DATA_FOSEL,
                lpGBT_UPLINK_USER_DATA        => lpGBT_UPLINK_USER_DATA_FOSEL,
                lpGBT_UPLINK_EC_DATA          => lpGBT_UPLINK_EC_DATA_FOSEL,
                lpGBT_UPLINK_IC_DATA          => lpGBT_UPLINK_IC_DATA_FOSEL,
                LinkAligned                   => LinkAligned_FOSEL,
                clk160                        => clk160,
                clk240                        => clk240,
                clk250                        => clk250,
                clk40                         => clk40,
                clk365                        => clk365,
                aclk_out                      => decoding_aclk,
                aresetn                       => aresetn,
                m_axis                        => decoding_axis,
                m_axis_tready                 => decoding_axis_tready,
                m_axis_prog_empty             => decoding_axis_prog_empty,
                m_axis_noSC                   => decoding_axis_noSC,
                m_axis_noSC_tready            => decoding_axis_noSC_tready,
                m_axis_noSC_prog_empty        => decoding_axis_noSC_prog_empty,
                TTC_ToHost_Data_in            => TTC_ToHost_Data,
                FE_BUSY_out                   => FE_BUSY_endpoint,
                ElinkBusyIn                   => ElinkBusy,
                DmaBusyIn                     => dma_busy_arr(pcie_endpoint),
                FifoBusyIn                    => fifo_busy_arr(pcie_endpoint),
                BusySumIn                     => BUSY_OUT_s,
                m_axis_aux                    => decoding_axis_aux,
                m_axis_aux_prog_empty         => decoding_axis_aux_prog_empty,
                m_axis_aux_tready             => decoding_axis_aux_tready,
                register_map_control          => register_map_40_control,
                register_map_decoding_monitor => register_map_decoding_monitor,
                Interlaken_RX_Data_In         => Interlaken_RX_Data(pcie_endpoint*(GBT_NUM/ENDPOINTS) to ((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1),
                Interlaken_RX_Datavalid       => Interlaken_RX_Datavalid(((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1 downto pcie_endpoint*(GBT_NUM/ENDPOINTS)),
                Interlaken_RX_Gearboxslip     => Interlaken_RX_Gearboxslip(((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1 downto pcie_endpoint*(GBT_NUM/ENDPOINTS)),
                Interlaken_Decoder_Aligned_out => Interlaken_Decoder_Aligned(((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1 downto pcie_endpoint*(GBT_NUM/ENDPOINTS)),
                m_axis64                      => toHost_axis64,
                m_axis64_tready               => toHost_axis64_tready,
                m_axis64_prog_empty           => toHost_axis64_prog_empty,
                toHost_axis64_aclk_out        => toHost_axis64_aclk
            --                CBOPT                         => CBOPT_vio,
            --                DIS_LANE_IN                   => DIS_LANE_vio,
            --                mask_k_char                   => mask_k_char_vio
            );

        crth0: entity work.CRToHost
            generic map(
                NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
                NUMBER_OF_INTERRUPTS => NUMBER_OF_INTERRUPTS,
                LINK_NUM => GBT_NUM/ENDPOINTS,
                LINK_CONFIG => LINK_CONFIG_MODE(FIRMWARE_MODE)(0 to GBT_NUM/ENDPOINTS-1),
                toHostTimeoutBitn => toHostTimeoutBitn,
                STREAMS_TOHOST => STREAMS_TOHOST,
                BLOCKSIZE => BLOCKSIZE,
                DATA_WIDTH => DATA_WIDTH,
                FIRMWARE_MODE => FIRMWARE_MODE,
                USE_URAM => USE_ULTRARAM(CARD_TYPE))
            port map(
                clk40 => clk40,
                clk160 => clk160,
                clk250 => clk250,
                aclk_tohost => decoding_aclk,
                aclk64_tohost => toHost_axis64_aclk,
                aresetn => aresetn,
                register_map_control => register_map_40_control,
                register_map_xoff_monitor => register_map_xoff_monitor,
                register_map_crtohost_monitor => register_map_crtohost_monitor,
                interrupt_call => interrupt_call,
                xoff_out => xoff_global((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS)-1 downto (pcie_endpoint)*(GBT_NUM/ENDPOINTS)),
                s_axis => fanout_sel_axis,
                s_axis_tready => fanout_sel_axis_tready,
                s_axis_prog_empty => fanout_sel_axis_prog_empty,
                s_axis_aux => decoding_axis_aux,
                s_axis_aux_tready => decoding_axis_aux_tready,
                s_axis_aux_prog_empty => decoding_axis_aux_prog_empty,
                s_axis64 => toHost_axis64,
                s_axis64_tready => toHost_axis64_tready,
                s_axis64_prog_empty => toHost_axis64_prog_empty,
                dma_enable_in => dma_enable,
                toHostFifo_din => toHostFifo_din,
                toHostFifo_wr_en => toHostFifo_wr_en,
                toHostFifo_prog_full => toHostFifo_prog_full,
                toHostFifo_wr_clk => toHostFifo_wr_clk,
                toHostFifo_rst => toHostFifo_rst);

        fromHostFifo_rd_clk <= clk160;                                    -- TODO: use slower clock if less GBT links?
        encoding_aclk <= clk160;                                          -- regular encoders can run at 160 MHz
        g_enableFromHost: if CREnableFromHost generate
            signal toHostXoff_endpoint: std_logic_vector(23 downto 0);
        begin
            g_ep0: if pcie_endpoint = 0 generate --For Encoding in endpoint 0, distribute the complete XOFF for the whole card
                g_lt24: if GBT_NUM < 24 generate
                    toHostXoff_endpoint(GBT_NUM-1 downto 0) <= xoff_global(GBT_NUM-1 downto 0);
                else generate
                    toHostXoff_endpoint <= xoff_global(23 downto 0);
                end generate g_lt24;
            end generate;
            g_epN: if pcie_endpoint /= 0 generate --For Encoding in endpoint > 0, distribute only the XOFF bits for the current endpoint.
                toHostXoff_endpoint(pcie_endpoint*(GBT_NUM/ENDPOINTS)-1 downto 0) <= xoff_global(((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1 downto pcie_endpoint*(GBT_NUM/ENDPOINTS));
            end generate;
            crfh0: entity work.CRFromHostAxis
                generic map (
                    LINK_NUM                  => GBT_NUM / ENDPOINTS,
                    LINK_CONFIG               => LINK_CONFIG_MODE(0)(0 to GBT_NUM/ENDPOINTS-1), --Using FIRMWARE mode 0 because we want axis8
                    STREAMS_PER_LINK_FROMHOST => STREAMS_FROMHOST,
                    GROUP_CONFIG              => GROUP_CONFIG_FROMHOST,
                    DATA_WIDTH                => DATA_WIDTH,
                    SUPPORT_DELAY             => SUPPORT_HDLC_DELAY,
                    CARD_TYPE                 => CARD_TYPE
                )
                port map (
                    aresetn                   => fromhost_aresetn_sync,
                    fromHostFifo_clk          => fromHostFifo_rd_clk,
                    fromHostFifo_dout         => fromHostFifo_dout,
                    fromHostFifo_rd_en        => fromHostFifo_rd_en,
                    fromHostFifo_empty        => fromHostFifo_empty,
                    fromHostFifo_rst          => fromHostFifo_rst,
                    fhAxis_aclk               => encoding_aclk,
                    fhAxis                    => encoding_axis,
                    fhAxis_tready             => encoding_axis_tready,
                    fhAxis64_aclk             => '0',--fromHost_axis64_aclk,
                    fhAxis64                  => open, --fromHost_axis64((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),
                    fhAxis64_tready           => (others => '1'), --fromHost_axis64_tready((GBT_NUM/ENDPOINTS)*pcie_endpoint to((GBT_NUM/ENDPOINTS)*(pcie_endpoint+1))-1),
                    fifo_monitoring           => register_map_crfromhost_monitor.CRFROMHOST_FIFO_STATUS,
                    register_map_control      => register_map_40_control
                );

            encoding0: entity work.encoding
                generic map (
                    GBT_NUM                         => GBT_NUM / ENDPOINTS, --: integer := 4;
                    FIRMWARE_MODE                   => FIRMWARE_MODE, --: integer := FIRMWARE_MODE_GBT;
                    --BLOCKSIZE                       => BLOCKSIZE, --: integer := 1024;
                    STREAMS_FROMHOST                => STREAMS_FROMHOST, --: integer := 1;
                    IncludeEncodingEpath2_HDLC      => IncludeEncodingEpath2_HDLC, --: std_logic_vector(6 downto 0) := "1111111";
                    IncludeEncodingEpath2_8b10b     => IncludeEncodingEpath2_8b10b, --: std_logic_vector(6 downto 0) := "1111111";
                    IncludeEncodingEpath4_8b10b     => IncludeEncodingEpath4_8b10b, --: std_logic_vector(6 downto 0) := "1111111";
                    IncludeEncodingEpath8_8b10b     => IncludeEncodingEpath8_8b10b,  --: std_logic_vector(6 downto 0) := "1111111";
                    INCLUDE_DIRECT                  => IncludeDirectEncoding,
                    INCLUDE_TTC                     => INCLUDE_TTC,
                    INCLUDE_RD53                    => INCLUDE_RD53,
                    DEBUGGING_RD53                  => DEBUGGING_RD53,
                    RD53Version                     => RD53Version,
                    DISTR_RAM                       => USE_ENCODING_DISTR_RAM(CARD_TYPE), --No ultraram is used here, but  distributed ram for Virtex Ultrascale+ and Versal
                    SUPPORT_HDLC_DELAY              => SUPPORT_HDLC_DELAY,
                    USE_ULTRARAM_LCB                => USE_ULTRARAM(CARD_TYPE),    --Depending on availability of Ultraram, it can be used in the trickle memory inside the LCB encoder.
                    INCLUDE_XOFF                    => INCLUDE_XOFF,
                    USE_BUILT_IN_FIFO               => SUPPORT_BUILT_IN_FIFO(CARD_TYPE)
                )
                Port map(
                    clk40 => clk40,
                    aclk => encoding_aclk,
                    aresetn => fromhost_aresetn,
                    aresetn_ttc_lti => aresetn,
                    s_axis => encoding_axis,
                    s_axis_tready => encoding_axis_tready,
                    register_map_control => register_map_40_control,
                    register_map_encoding_monitor => register_map_encoding_monitor,
                    GBT_DOWNLINK_USER_DATA => GBT_DOWNLINK_USER_DATA_ENCODING,
                    lpGBT_DOWNLINK_USER_DATA => lpGBT_DOWNLINK_USER_DATA_ENCODING,
                    lpGBT_DOWNLINK_IC_DATA => lpGBT_DOWNLINK_IC_DATA_ENCODING,
                    lpGBT_DOWNLINK_EC_DATA => lpGBT_DOWNLINK_EC_DATA_ENCODING,
                    TTCin => TTC_path,
                    toHostXoff => toHostXoff_endpoint,
                    LTI_TX_Data_Transceiver => LTI_TX_Data_Transceiver(pcie_endpoint*(GBT_NUM/ENDPOINTS) to ((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1),
                    LTI_TX_TX_CharIsK => LTI_TX_TX_CharIsK(pcie_endpoint*(GBT_NUM/ENDPOINTS) to ((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1),
                    LTI_TXUSRCLK_in => LTI_TXUSRCLK(((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1 downto pcie_endpoint*(GBT_NUM/ENDPOINTS))
                );
        else generate
            lpGBT_DOWNLINK_EC_DATA_ENCODING <= (others => (others => '0'));
            lpGBT_DOWNLINK_IC_DATA_ENCODING <= (others => (others => '0'));
            lpGBT_DOWNLINK_USER_DATA_ENCODING <= (others => (others => '0'));
            GBT_DOWNLINK_USER_DATA_ENCODING <= (others => (others => '0'));
            encoding_axis_tready <= (others => (others => '1'));
            encoding_aclk <= '0';
            fromHostFifo_rst <= rst_hw or rst_soft_40;
            fromHostFifo_rd_en <= not fromHostFifo_empty;
        end generate g_enableFromHost;

        g_EnableFullModeEmulator: if FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
            emu0: entity work.FullModeDataEmulator
                generic map(
                    GBT_NUM => GBT_NUM/ENDPOINTS,
                    BLOCKSIZE => BLOCKSIZE,
                    AddFULLMODEForDUNE => AddFULLMODEForDUNE,
                    VERSAL => IS_VERSAL(CARD_TYPE))
                port map(
                    appreg_clk => appreg_clk,
                    register_map_control => register_map_40_control,
                    register_map_control_appreg_clk => register_map_control_appreg_clk,
                    register_map_gbtemu_monitor => register_map_gbtemu_monitor,
                    FE_BUSY_out => FE_BUSY_emulator,
                    clk40 => clk40,
                    clk240 => clk240,
                    aclk => decoding_aclk,
                    aresetn => aresetn,
                    L1A_IN => TTC_path.L1A,
                    m_axis => emu_axis, -- @suppress "Incorrect array size in assignment: expected (<(Unknown Scalar Value)>, <1>)(tdata((<32>)), tkeep((<4>)), tuser((<4>))) but was (<(Unknown Scalar Value)>, <STREAMS_TOHOST>)(tdata((<32>)), tkeep((<4>)), tuser((<4>)))"
                    m_axis_tready => emu_axis_tready, -- @suppress "Incorrect array size in assignment: expected (<(Unknown Scalar Value)>, <1>) but was (<(Unknown Scalar Value)>, <STREAMS_TOHOST>)"
                    m_axis_prog_empty => emu_axis_prog_empty, -- @suppress "Incorrect array size in assignment: expected (<(Unknown Scalar Value)>, <1>) but was (<(Unknown Scalar Value)>, <STREAMS_TOHOST>)"
                    m_axis_noSC => emu_axis_noSC, -- @suppress "Incorrect array size in assignment: expected (<(Unknown Scalar Value)>, <1>)(tdata((<32>)), tkeep((<4>)), tuser((<4>))) but was (<(Unknown Scalar Value)>, <STREAMS_TOHOST>)(tdata((<32>)), tkeep((<4>)), tuser((<4>)))"
                    m_axis_noSC_tready => emu_axis_noSC_tready, -- @suppress "Incorrect array size in assignment: expected (<(Unknown Scalar Value)>, <1>) but was (<(Unknown Scalar Value)>, <STREAMS_TOHOST>)"
                    m_axis_noSC_prog_empty => emu_axis_noSC_prog_empty -- @suppress "Incorrect array size in assignment: expected (<(Unknown Scalar Value)>, <1>) but was (<(Unknown Scalar Value)>, <STREAMS_TOHOST>)"
                );

            fosel0: entity work.axis_32_fanout_selector
                generic map(
                    GBT_NUM        => GBT_NUM/ENDPOINTS,
                    STREAMS_TOHOST => STREAMS_TOHOST
                )
                port map(
                    aclk                       => decoding_aclk,
                    emu_axis                   => emu_axis,
                    emu_axis_tready            => emu_axis_tready,
                    emu_axis_prog_empty        => emu_axis_prog_empty,
                    emu_FE_BUSY_in             => FE_BUSY_emulator,
                    decoding_axis              => decoding_axis,
                    decoding_axis_tready       => decoding_axis_tready,
                    decoding_axis_prog_empty   => decoding_axis_prog_empty,
                    decoding_FE_BUSY_in        => FE_BUSY_endpoint,
                    fanout_sel_axis            => fanout_sel_axis,
                    fanout_sel_axis_tready     => fanout_sel_axis_tready,
                    fanout_sel_axis_prog_empty => fanout_sel_axis_prog_empty,
                    fanout_sel_FE_BUSY_out     => BUSY_REQUESTs(pcie_endpoint*(GBT_NUM/ENDPOINTS) to ((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1),
                    register_map_control       => register_map_40_control
                );
            g_AddNoSCFullMode: if AddFULLMODEForDUNE generate
                fosel_noSC0: entity work.axis_32_fanout_selector
                    generic map(
                        GBT_NUM        => GBT_NUM/ENDPOINTS,
                        STREAMS_TOHOST => STREAMS_TOHOST
                    )
                    port map(
                        aclk                       => decoding_aclk,
                        emu_axis                   => emu_axis_noSC,
                        emu_axis_tready            => emu_axis_noSC_tready,
                        emu_axis_prog_empty        => emu_axis_noSC_prog_empty,
                        emu_FE_BUSY_in             => (others => (others => '0')),
                        decoding_axis              => decoding_axis_noSC,
                        decoding_axis_tready       => decoding_axis_noSC_tready,
                        decoding_axis_prog_empty   => decoding_axis_noSC_prog_empty,
                        decoding_FE_BUSY_in        => (others => (others => '0')),
                        fanout_sel_axis            => fanout_sel_axis_noSC,
                        fanout_sel_axis_tready     => fanout_sel_axis_noSC_tready,
                        fanout_sel_axis_prog_empty => fanout_sel_axis_noSC_prog_empty,
                        fanout_sel_FE_BUSY_out     => open,
                        register_map_control       => register_map_40_control
                    );
            end generate;

        else generate
            fanout_sel_axis <= decoding_axis;
            decoding_axis_tready <= fanout_sel_axis_tready;
            fanout_sel_axis_prog_empty <= decoding_axis_prog_empty;
            BUSY_REQUESTs(pcie_endpoint*(GBT_NUM/ENDPOINTS) to ((pcie_endpoint+1)*(GBT_NUM/ENDPOINTS))-1) <= FE_BUSY_endpoint;
        end generate;

    end generate;

    hk0: entity work.housekeeping_module
        generic map(
            CARD_TYPE => CARD_TYPE,
            GBT_NUM => GBT_NUM/ENDPOINTS,
            ENDPOINTS => ENDPOINTS,
            generateTTCemu => generateTTCemu,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_Si5324_RefCLK => USE_Si5324_RefCLK,
            GENERATE_XOFF => GENERATE_XOFF,
            IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
            IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
            IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
            IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
            IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
            IncludeDecodingEpath32_8b10b => IncludeDecodingEpath32_8b10b,
            IncludeDirectDecoding => IncludeDirectDecoding,
            IncludeEncodingEpath2_HDLC      => IncludeEncodingEpath2_HDLC,
            IncludeEncodingEpath2_8b10b     => IncludeEncodingEpath2_8b10b,
            IncludeEncodingEpath4_8b10b     => IncludeEncodingEpath4_8b10b,
            IncludeEncodingEpath8_8b10b     => IncludeEncodingEpath8_8b10b,
            IncludeDirectEncoding => IncludeDirectEncoding,
            BLOCKSIZE => BLOCKSIZE,
            DATA_WIDTH => DATA_WIDTH,
            FULL_HALFRATE => FULL_HALFRATE,
            SUPPORT_HDLC_DELAY => SUPPORT_HDLC_DELAY,
            TTC_SYS_SEL => TTC_SYS_SEL,
            USE_VERSAL_CPM => USE_VERSAL_CPM
        )
        port map(
            MMCM_Locked_in => MMCM_Locked_out,
            MMCM_OscSelect_in => MMCM_OscSelect_out,
            SCL => SCL,
            SDA => SDA,
            SI5345_A => SI5345_A,
            SI5345_INSEL => SI5345_INSEL,
            SI5345_OE => SI5345_OE,
            SI5345_RSTN => SI5345_RSTN,
            SI5345_SEL => SI5345_SEL,
            SI5345_nLOL => SI5345_nLOL,
            SI5345_FINC_B => SI5345_FINC_B,
            SI5345_FDEC_B => SI5345_FDEC_B,
            SI5345_INTR_B => SI5345_INTR_B,
            appreg_clk => global_appreg_clk,
            emcclk => emcclk,
            flash_SEL => flash_SEL,
            flash_a => flash_a,
            flash_a_msb => flash_a_msb,
            flash_adv => flash_adv,
            flash_cclk => flash_cclk,
            flash_ce => flash_ce,
            flash_d => flash_d,
            flash_re => flash_re,
            flash_we => flash_we,
            i2cmux_rst => i2cmux_rst,
            TACH => TACH,
            FAN_FAIL_B => FAN_FAIL_B,
            FAN_FULLSP => FAN_FULLSP,
            FAN_OT_B => FAN_OT_B,
            FAN_PWM => FAN_PWM,
            FF3_PRSNT_B => FF3_PRSNT_B,
            IOEXPAN_INTR_B => IOEXPAN_INTR_B,
            IOEXPAN_RST_B => IOEXPAN_RST_B,
            clk10_xtal => clk10_xtal,
            clk40_xtal => clk40_xtal,
            leds => leds,
            opto_inhibit => opto_inhibit,
            --opto_los => OPTO_LOS,
            register_map_control => global_register_map_control_appreg_clk,
            register_map_gen_board_info => register_map_gen_board_info,
            register_map_hk_monitor => register_map_hk_monitor,
            rst_soft => global_reset_soft_appreg_clk,
            sys_reset_n => PCIE_PERSTn,
            PCIE_PWRBRK => PCIE_PWRBRK,
            PCIE_WAKE_B => PCIE_WAKE_B,
            QSPI_RST_B => QSPI_RST_B,
            rst_hw => rst_hw,
            CLK40_FPGA2LMK_P => CLK40_FPGA2LMK_P,
            CLK40_FPGA2LMK_N => CLK40_FPGA2LMK_N,
            LMK_DATA => LMK_DATA,
            LMK_CLK => LMK_CLK,
            LMK_LE => LMK_LE,
            LMK_GOE => LMK_GOE,
            LMK_LD => LMK_LD,
            LMK_SYNCn => LMK_SYNCn,
            I2C_SMB => I2C_SMB,
            I2C_SMBUS_CFG_nEN => I2C_SMBUS_CFG_nEN,
            MGMT_PORT_EN => MGMT_PORT_EN,
            PCIE_PERSTn_out => PCIE_PERSTn_out,
            PEX_PERSTn => PEX_PERSTn,
            PEX_SCL => PEX_SCL,
            PEX_SDA => PEX_SDA,
            PORT_GOOD => PORT_GOOD,
            SHPC_INT => SHPC_INT,
            lnk_up => lnk_up,
            RXUSRCLK_IN => RXUSRCLK,
            versal_sys_reset_n_out => versal_sys_reset_n,
            WupperToCPM => WupperToCPM,
            CPMToWupper => CPMToWupper,
            clk100_out => clk100,
            DDR_in => DDR_in,
            DDR_out => DDR_out,
            DDR_inout => DDR_inout
        );

    g_TB_TRIGGER_IBUF: for i in 0 to NUM_TB_TRIGGERS(CARD_TYPE) - 1 generate
        ibufds_testbeam : IBUFDS
            generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, DIFF_TERM, DQS_BIAS, IBUF_DELAY_VALUE, IBUF_LOW_PWR, IFD_DELAY_VALUE" -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, CCIO_EN_M, CCIO_EN_S, DIFF_TERM, DQS_BIAS, IBUF_DELAY_VALUE, IBUF_LOW_PWR, IFD_DELAY_VALUE" -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, DIFF_TERM, and 4 more"
                IOSTANDARD => "LVDS"
            )
            port map
        (
                O               =>   tb_trigger_i(i),
                I               =>   TB_trigger_P(i),
                IB              =>   TB_trigger_N(i)
            );
    end generate;

    TTCLTI: if  TTC_SYS_SEL = '1' and (CARD_TYPE = 711 or CARD_TYPE = 712) generate
        ltittc0: entity work.ltittc_wrapper
            generic map(
                CARD_TYPE => CARD_TYPE,
                ITK_TRIGTAG_MODE => get_trigtag_mode
            )
            port map(
                reset_in                    => '0',
                register_map_control        => global_register_map_40_control,
                register_map_ltittc_monitor => register_map_ltittc_monitor,
                TTC_out                     => TTC_path,
                --
                clk40_ttc_out               => clk_ttc_40_s,
                clk40_in                    => clk40,
                clk40_xtal_in               => clk40_xtal,

                BUSY                        => open,
                cdrlocked_out               => open,
                TTC_ToHost_Data_out      => TTC_ToHost_Data,
                --MT temporary open : need to change CRtohost--
                --TTCMSG_ToHost_Data_out      => open,
                --USRMSG_ToHost_Data_out      => open,
                --
                TTC_BUSY_mon_array          => ttc_TTC_BUSY_mon_array(23 downto 0),
                BUSY_IN                     => BUSY_OUT_s,
                --SI5345 OUT5 connected to bank 232 TX REFCLK
                GTREFCLK0_P_IN         => GTREFCLK0_LTITTC_P(0),
                GTREFCLK0_N_IN         => GTREFCLK0_LTITTC_N(0),
                --LMK OUT6 connected to bank 231 to RX REFCLK
                GTREFCLK1_P_IN         => GTREFCLK1_LTITTC_P(0),
                GTREFCLK1_N_IN         => GTREFCLK1_LTITTC_N(0),
                --bank 232 from .xdc
                RX_P_LTITTC                    => RX_P_LTITTC(0),
                RX_N_LTITTC                    => RX_N_LTITTC(0),
                TX_P_LTITTC                    => TX_P_LTITTC(0),
                TX_N_LTITTC                    => TX_N_LTITTC(0)
            );
    end generate;

    TTC: if  TTC_SYS_SEL = '0' generate
        ttc0: entity work.ttc_wrapper
            generic map (
                CARD_TYPE                => CARD_TYPE,
                FIRMWARE_MODE            => FIRMWARE_MODE,
                ISTESTBEAM               => ISTESTBEAM,
                ITK_TRIGTAG_MODE         => get_trigtag_mode
            )
            port map(
                CLK_TTC_P                => CLK_TTC_P,
                CLK_TTC_N                => CLK_TTC_N,
                DATA_TTC_P               => DATA_TTC_P,
                DATA_TTC_N               => DATA_TTC_N,
                TB_trigger               => tb_trigger_i,

                LOL_ADN => LOL_ADN,
                LOS_ADN => LOS_ADN,
                --RESET_N                  => sys_reset_n,
                register_map_control => global_register_map_40_control,
                register_map_ttc_monitor => register_map_ttc_monitor,
                register_map_control_appreg_clk => global_register_map_control_appreg_clk,
                appreg_clk => global_appreg_clk,
                TTC_out                  => TTC_path,
                clk_adn_160              => clk_adn_160,
                clk_ttc_40               => clk_ttc_40_s,
                clk40                    => clk40,
                BUSY                     => open,
                cdrlocked_out            => open,
                TTC_ToHost_Data_out      => TTC_ToHost_Data,
                TTC_BUSY_mon_array => ttc_TTC_BUSY_mon_array(23 downto 0),
                BUSY_IN => BUSY_OUT_s);
    end generate;

    busy0: entity work.ttc_busy
        generic map(
            GBT_NUM => GBT_NUM)
        port map(
            mclk                 => clk40,
            mrst => global_rst_soft_40,
            register_map_control => global_register_map_40_control,
            BUSY_REQUESTs => BUSY_REQUESTs,
            ANY_BUSY_REQ_OUT => BUSY_OUT_s,
            TTC_BUSY_mon_array   => TTC_BUSY_mon_array,
            DMA_BUSY_in => dma_busy,
            FIFO_BUSY_in => fifo_busy,
            BUSY_INTERRUPT_out => open);

    BUSY_OUT <= (others => BUSY_OUT_s);
    dma_busy <= or_reduce(dma_busy_arr);

    fifo_busy <= or_reduce(fifo_busy_arr);

    si5324_resetn <= (others => (not rst_hw));


    g_busysplit_0: for i in 0 to GBT_NUM-1 generate
        ttc_TTC_BUSY_mon_array(i) <= TTC_BUSY_mon_array(i);
    end generate;



end architecture structure ; -- of felix_top

