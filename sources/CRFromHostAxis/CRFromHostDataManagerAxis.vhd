--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.MATH_REAL.LOG2;
    use IEEE.MATH_REAL.CEIL;

    use work.axi_stream_package.all;
    use work.FELIX_package.all;

entity CRFromHostDataManagerAxis is
    generic (
        DATA_WIDTH                      : integer := 256;
        STREAMS_PER_LINK_FROMHOST       : integer range 1 to 64 := 1;
        GROUP_CONFIG                    : IntArray(0 to MAX_GROUPS_PER_STREAM_FROMHOST-1) := (0 => 1, others => 0);
        SUPPORT_DELAY                   : boolean := false;                -- boolean to support delay packet generation (FLX-1862)
        CARD_TYPE                       : integer := 712
    );
    port (
        -- reset
        aresetn             : in std_logic;

        -- interface to link FIFO
        linkFifo_clk        : in  std_logic;
        linkFifo_dout       : in  std_logic_vector(DATA_WIDTH-1 downto 0);
        linkFifo_rden       : out std_logic;
        linkFifo_valid      : in  std_logic;
        linkFifo_empty      : in  std_logic;

        fhAxis              : out axis_8_array_type(0 to STREAMS_PER_LINK_FROMHOST-1);
        fhAxis_tready       : in  axis_tready_array_type(0 to STREAMS_PER_LINK_FROMHOST-1);

        -- configuration
        broadcastEnable     : in  std_logic_vector(STREAMS_PER_LINK_FROMHOST-1 downto 0) := (others => '0')
    );
end CRFromHostDataManagerAxis;

architecture rtl of CRFromHostDataManagerAxis is
    function get_group_num(constant x : IntArray) return integer is
        variable cnt : integer;
    begin
        cnt := 0;
        for i in x'range loop
            if x(i) /= 0 then
                cnt := cnt + 1;
            end if;
        end loop;
        return cnt;
    end function;

    function get_group_low_addr(constant x : integer) return integer is
        variable offs : integer;
    begin
        offs := 0;
        for i in GROUP_CONFIG'range loop
            if i = x then
                return offs;
            end if;
            offs := offs + GROUP_CONFIG(i);
        end loop;
        return -1;        -- group not found
    end function;

    function get_group_high_addr(constant x : integer) return integer is
    begin
        return get_group_low_addr(x) + GROUP_CONFIG(x) - 1;
    end function;

    function get_group_from_stream(constant x : integer) return integer is
        variable offs : integer;
    begin
        offs := 0;
        for i in GROUP_CONFIG'range loop
            if (x >= offs) and (x < (offs + GROUP_CONFIG(i))) then
                return i;
            end if;
            offs := offs + GROUP_CONFIG(i);
        end loop;
        return -1;        -- not found
    end function;

    function get_streamToGroupLUT return IntArray is
        variable ret : IntArray(0 to 255);
    begin
        for i in 0 to 255 loop
            ret(i) := get_group_from_stream(i);
        end loop;
        return ret;
    end function;

    signal areset : std_logic;

    type groupFifo_din_array is array (natural range <>) of std_logic_vector(DATA_WIDTH-1 downto 0);
    type groupFifo_dout_array is array (natural range <>) of std_logic_vector(31 downto 0);
    signal groupFifo_din : groupFifo_din_array(0 to get_group_num(GROUP_CONFIG)-1);
    signal groupFifo_dout : groupFifo_dout_array(0 to get_group_num(GROUP_CONFIG)-1);
    signal groupFifo_wren : std_logic_vector(0 to get_group_num(GROUP_CONFIG)-1);
    signal groupFifo_rden : std_logic_vector(0 to get_group_num(GROUP_CONFIG)-1);
    --signal groupFifo_valid : std_logic_vector(0 to get_group_num(GROUP_CONFIG)-1);
    --signal groupFifo_full : std_logic_vector(0 to get_group_num(GROUP_CONFIG)-1);
    signal groupFifo_pfull : std_logic_vector(0 to get_group_num(GROUP_CONFIG)-1);
    signal groupFifo_empty : std_logic_vector(0 to get_group_num(GROUP_CONFIG)-1);
    signal groupFifo_drop : std_logic_vector(0 to get_group_num(GROUP_CONFIG)-1);
    signal groupFifo_pfull_ored : std_logic;
    constant streamToGroupLUT : IntArray(0 to 255) := get_streamToGroupLUT;

    signal streamFifoAxis : axis_8_array_type(0 to STREAMS_PER_LINK_FROMHOST-1);
    signal streamFifoAxis_tready : axis_tready_array_type(0 to STREAMS_PER_LINK_FROMHOST-1);
begin

    -- async reset
    areset <= not aresetn;

    -- read data from link FIFO
    groupFifo_pfull_ored <= '0' when groupFifo_pfull = (groupFifo_pfull'range => '0') else '1';
    linkFifo_rden <= not linkFifo_empty and not groupFifo_pfull_ored;

    -- distribute data to group fifos
    process (linkFifo_clk) begin
        if rising_edge(linkFifo_clk) then
            if aresetn = '0' then
                groupFifo_din <= (others => (others => '0'));
                groupFifo_wren <= (others => '0');
            else
                if linkFifo_valid = '1' then
                    for I in 0 to get_group_num(GROUP_CONFIG)-1 loop
                        if streamToGroupLUT(to_integer(unsigned(linkFifo_dout(DATA_WIDTH-32+15 downto DATA_WIDTH-32+8)))) = I or
                       linkFifo_dout(DATA_WIDTH-32+15 downto DATA_WIDTH-32+8) = "11111111" then
                            groupFifo_din(I) <= linkFifo_dout;
                            groupFifo_wren(I) <= '1';
                        else
                            groupFifo_din(I) <= (others => '0');
                            groupFifo_wren(I) <= '0';
                        end if;
                    end loop;
                else
                    groupFifo_din <= (others => (others => '0'));
                    groupFifo_wren <= (others => '0');
                end if;
            end if;
        end if;
    end process;

    -- group FIFOs
    groupFifos: for I in 0 to get_group_num(GROUP_CONFIG)-1 generate
        groupFifo_I: entity work.CRFromHostGroupFifo
            generic map (
                DATA_WIDTH => DATA_WIDTH,
                --DEPTH => 16,
                --PROG_FULL_THRESHOLD => 11,
                CARD_TYPE => CARD_TYPE
            )
            port map (
                clk => linkFifo_clk,
                areset => areset,
                din => groupFifo_din(I),
                dout => groupFifo_dout(I),
                wren => groupFifo_wren(I),
                rden => groupFifo_rden(I),
                valid => open, --groupFifo_valid(I),
                full => open, --groupFifo_full(I),
                empty => groupFifo_empty(I),
                pfull => groupFifo_pfull(I),
                drop => groupFifo_drop(I)
            );
    end generate;

    -- transfer manager
    transfer_managers: for I in 0 to get_group_num(GROUP_CONFIG)-1 generate
        transfer_manager_I: entity work.CRFromHostTransferManager
            generic map (
                DATA_WIDTH => DATA_WIDTH,
                STREAMS_PER_GROUP_FROMHOST => GROUP_CONFIG(I),
                STREAM_ADDR_OFFSET => get_group_low_addr(I),
                SUPPORT_DELAY => SUPPORT_DELAY
            )
            port map (
                clk => linkFifo_clk,
                reset => areset,
                groupFifoData => groupFifo_dout(I),
                groupFifoEmpty => groupFifo_empty(I),
                groupFifoRead => groupFifo_rden(I),
                groupFifoDrop => groupFifo_drop(I),
                stream_out => streamFifoAxis(get_group_low_addr(I) to get_group_high_addr(I)),
                stream_out_tready => streamFifoAxis_tready(get_group_low_addr(I) to get_group_high_addr(I)),
                broadcastEnable => broadcastEnable(get_group_high_addr(I) downto get_group_low_addr(I))
            );
    end generate;


    fhAxis <= streamFifoAxis;
    streamFifoAxis_tready <= fhAxis_tready;

end architecture;
