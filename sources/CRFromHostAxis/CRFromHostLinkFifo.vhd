--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

library XPM;
    use XPM.VCOMPONENTS.ALL;

entity CRFromHostLinkFifo is
    generic (
        DATA_WIDTH           : integer := 256;
        DEPTH                : integer := 512;
        PROG_FULL_THRESHOLD  : integer := 480
    );
    port (
        wrclk                : in  std_logic;
        rdclk                : in  std_logic;
        areset               : in  std_logic;
        din                  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
        dout                 : out std_logic_vector(DATA_WIDTH-1 downto 0);
        wren                 : in  std_logic;
        rden                 : in  std_logic;
        valid                : out std_logic;
        full                 : out std_logic;
        empty                : out std_logic;
        pfull                : out std_logic
    );
end CRFromHostLinkFifo;

architecture rtl of CRFromHostLinkFifo is
    signal fifo_din : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal fifo_dout : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal fifo_wren : std_logic;
    signal fifo_rden : std_logic;
    signal fifo_full : std_logic;
    signal fifo_pfull : std_logic;
    signal fifo_empty : std_logic;
begin

    -- check programmable full threshold
    assert PROG_FULL_THRESHOLD < DEPTH report "PROG_FULL_THRESHOLD has to be less than the FIFO depth!" severity failure;

    -- FIFO writing
    fifo_din <= din;
    fifo_wren <= wren and not fifo_full;
    full <= fifo_full;
    pfull <= fifo_pfull;

    -- FIFO reading
    fifo_rden <= rden and not fifo_empty;
    empty <= fifo_empty;

    -- produce valid signal
    valid <= rden when rising_edge(rdclk);

    -- output data
    dout <= fifo_dout;

    -- XPM macro
    fifo: xpm_fifo_async
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            CDC_SYNC_STAGES => 2,
            FIFO_MEMORY_TYPE => "block",
            FIFO_WRITE_DEPTH => DEPTH,
            WRITE_DATA_WIDTH => DATA_WIDTH,
            READ_MODE => "std",
            FIFO_READ_LATENCY => 1,
            FULL_RESET_VALUE => 0,
            USE_ADV_FEATURES => "0002",
            READ_DATA_WIDTH => DATA_WIDTH,
            WR_DATA_COUNT_WIDTH => 1,
            PROG_FULL_THRESH => PROG_FULL_THRESHOLD,
            RD_DATA_COUNT_WIDTH => 1,
            PROG_EMPTY_THRESH => 10,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            SIM_ASSERT_CHK => 0,
            WAKEUP_TIME => 0
        )
        port map (
            sleep => '0',
            rst => areset,
            wr_clk => wrclk,
            wr_en => fifo_wren,
            din => fifo_din,
            full => fifo_full,
            prog_full => fifo_pfull,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => rdclk,
            rd_en => fifo_rden,
            dout => fifo_dout,
            empty => fifo_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );


end architecture;
