--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

    use work.axi_stream_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
library xpm;
    use xpm.vcomponents.all;

entity CRFromHostAxis is
    generic (
        LINK_NUM                        : integer range 1 to 32 := 1;
        LINK_CONFIG                     : IntArray;
        STREAMS_PER_LINK_FROMHOST       : integer range 1 to 64 := 1;
        GROUP_CONFIG                    : IntArray(0 to MAX_GROUPS_PER_STREAM_FROMHOST-1) := (0 => 1, others => 0);
        DATA_WIDTH                      : integer := 256;
        SUPPORT_DELAY                   : boolean := false;                -- boolean to support delay packet generation (FLX-1862)
        CARD_TYPE                       : integer := 712
    );
    port (
        -- reset
        aresetn              : in std_logic;

        -- FromHost FIFO interface
        fromHostFifo_clk     : in  std_logic;
        fromHostFifo_dout    : in  std_logic_vector(DATA_WIDTH-1 downto 0);
        fromHostFifo_rd_en   : out std_logic;
        fromHostFifo_empty   : in  std_logic;
        fromHostFifo_rst     : out std_logic;

        -- AXI streaming
        fhAxis_aclk          : in  std_logic;
        fhAxis               : out axis_8_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_PER_LINK_FROMHOST-1);
        fhAxis_tready        : in  axis_tready_2d_array_type(0 to LINK_NUM-1, 0 to STREAMS_PER_LINK_FROMHOST-1);
        fhAxis64_aclk        : in  std_logic;
        fhAxis64             : out axis_64_array_type(0 to LINK_NUM-1);
        fhAxis64_tready      : in  axis_tready_array_type(0 to LINK_NUM-1);

        -- configuration & status (should be synchronous to fromHostFifo_clk!)
        fifo_monitoring      : out bitfield_crfromhost_fifo_status_r_type;
        register_map_control : in  register_map_control_type
    );
end CRFromHostAxis;

architecture rtl of CRFromHostAxis is
    signal areset : std_logic;

    signal fromHostFifo_read : std_logic;
    signal fromHostFifo_valid : std_logic;

    --signal upstreamFifoPfull : std_logic_vector(0 to LINK_NUM-1);
    --signal upstreamFifoPfull_ored : std_logic;

    type linkFifo_data_array is array (natural range <>) of std_logic_vector(DATA_WIDTH-1 downto 0);
    signal linkFifo_din : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal linkFifo_dout : linkFifo_data_array(0 to LINK_NUM-1);
    signal linkFifo_wren : std_logic_vector(0 to LINK_NUM-1);
    signal linkFifo_rden : std_logic_vector(0 to LINK_NUM-1);
    signal linkFifo_valid : std_logic_vector(0 to LINK_NUM-1);
    signal linkFifo_full : std_logic_vector(0 to LINK_NUM-1);
    signal linkFifo_pfull : std_logic_vector(0 to LINK_NUM-1);
    signal linkFifo_empty : std_logic_vector(0 to LINK_NUM-1);
    signal linkFifo_pfull_ored : std_logic;
    --signal packet_empty : std_logic;
    signal CLEAR_LATCH_sync : std_logic;
begin

    -- check data width (can be only 256, 512 or 1024)
    assert ((DATA_WIDTH = 256) or (DATA_WIDTH = 512) or (DATA_WIDTH = 1024))
        report "DATA_WIDTH can be either 256 or 512"
        severity failure;

    -- check grouping
    assert sum(GROUP_CONFIG) = STREAMS_PER_LINK_FROMHOST
        report "GROUP_CONFIG does not match STREAMS_PER_LINK_FROMHOST!"
        severity failure;

    -- check length of LINK_CONFIG array
    assert LINK_CONFIG'length = LINK_NUM
        report "Every link must have a corresponding LINK_CONFIG, please check the LINK_CONFIG array length!"
        severity failure;

    -- async reset
    areset <= not aresetn;

    -- flush also PCIe FIFO
    fromHostFifo_rst <= not aresetn;

    -- read when data is available and we don't have to wait for it
    -- and also generate a valid signal one clock cycle later
    linkFifo_pfull_ored <= '0' when (linkFifo_pfull = (linkFifo_pfull'range => '0')) else '1';
    fromHostFifo_read <= not fromHostFifo_empty and not linkFifo_pfull_ored;
    fromHostFifo_valid <= fromHostFifo_read when rising_edge(fromHostFifo_clk);
    fromHostFifo_rd_en <= fromHostFifo_read;

    -- check length field and generate an empty flag
    --packet_empty <= '1' when (fromHostFifo_dout(DATA_WIDTH-32+5 downto DATA_WIDTH-32) = "000000") else '0';

    -- manage writing to linkFifo
    process (fromHostFifo_clk) begin
        if rising_edge(fromHostFifo_clk) then
            if aresetn = '0' then
                linkFifo_wren <= (others => '0');
            else
                linkFifo_wren <= (others => '0');
                if fromHostFifo_valid = '1' then
                    linkFifo_din <= fromHostFifo_dout;
                    for I in 0 to LINK_NUM-1 loop
                        if (fromHostFifo_dout(DATA_WIDTH-32+23 downto DATA_WIDTH-32+16) = std_logic_vector(to_unsigned(I, 8))) or
                           (fromHostFifo_dout(DATA_WIDTH-32+23 downto DATA_WIDTH-32+16) = "11111111") then
                            linkFifo_wren(I) <= not linkFifo_full(I); -- and not packet_empty;
                        end if;
                    end loop;
                end if;
            end if;
        end if;
    end process;

    -- generate all links
    links: for LINK in 0 to LINK_NUM-1 generate
        signal dataManager_clk : std_logic;
        signal channelFhAxis : axis_8_array_type(0 to STREAMS_PER_LINK_FROMHOST-1);
        signal channelFhAxis_tready : axis_tready_array_type(0 to STREAMS_PER_LINK_FROMHOST-1);
    begin
        -- check proper link configuration
        assert ((LINK_CONFIG(LINK) = 0) or (LINK_CONFIG(LINK) = 1))
            report "Invalid LINK_CONFIG for link " & integer'image(LINK)
            severity failure;

        -- link FIFO instance (2 kB)
        linkFifo_I: entity work.CRFromHostLinkFifo
            generic map (
                DATA_WIDTH => DATA_WIDTH,
                DEPTH => 32,
                PROG_FULL_THRESHOLD => 26
            )
            port map (
                wrclk => fromHostFifo_clk,
                rdclk => dataManager_clk,
                areset => areset,
                din => linkFifo_din,
                dout => linkFifo_dout(LINK),
                wren => linkFifo_wren(LINK),
                rden => linkFifo_rden(LINK),
                valid => linkFifo_valid(LINK),
                full => linkFifo_full(LINK),
                empty => linkFifo_empty(LINK),
                pfull => linkFifo_pfull(LINK)
            );

        dataManager_gbt: if LINK_CONFIG(LINK) = 0 generate
            -- use AXIS-64 clock

            signal BROADCAST_ENABLE_fromHostFifo_clk: std_logic_vector(STREAMS_PER_LINK_FROMHOST-1 downto 0);
        begin
            dataManager_clk <= fhAxis_aclk;
            chAssign: for STREAM in 0 to STREAMS_PER_LINK_FROMHOST-1 generate
                fhAxis(LINK, STREAM) <= channelFhAxis(STREAM);
                channelFhAxis_tready(STREAM) <= fhAxis_tready(LINK, STREAM);
            end generate;




            xpm_cdc_array_single_inst_BROADCAST_ENABLE : xpm_cdc_array_single
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0,
                    WIDTH => STREAMS_PER_LINK_FROMHOST
                )
                port map (
                    src_clk => '0',
                    src_in => register_map_control.BROADCAST_ENABLE(LINK)(STREAMS_PER_LINK_FROMHOST-1 downto 0),
                    dest_clk => fromHostFifo_clk,
                    dest_out => BROADCAST_ENABLE_fromHostFifo_clk
                );

            dataManager_I: entity work.CRFromHostDataManagerAxis
                generic map (
                    DATA_WIDTH => DATA_WIDTH,
                    STREAMS_PER_LINK_FROMHOST => STREAMS_PER_LINK_FROMHOST,
                    GROUP_CONFIG => GROUP_CONFIG,
                    SUPPORT_DELAY => SUPPORT_DELAY,
                    CARD_TYPE => CARD_TYPE
                )
                port map (
                    aresetn => aresetn,
                    linkFifo_clk => dataManager_clk,
                    linkFifo_dout => linkFifo_dout(LINK),
                    linkFifo_rden => linkFifo_rden(LINK),
                    linkFifo_valid => linkFifo_valid(LINK),
                    linkFifo_empty => linkFifo_empty(LINK),
                    fhAxis => channelFhAxis,
                    fhAxis_tready => channelFhAxis_tready,
                    broadcastEnable => BROADCAST_ENABLE_fromHostFifo_clk
                );

            -- for GBT links we don't use the fhAxis64 port
            fhAxis64(LINK).tdata <= (others => '0');
            fhAxis64(LINK).tkeep <= (others => '0');
            fhAxis64(LINK).tvalid <= '0';
            fhAxis64(LINK).tlast <= '0';
            fhAxis64(LINK).tid <= (others => '0');
            fhAxis64(LINK).tuser <= (others => '0');
        end generate;

        dataManager_25g: if LINK_CONFIG(LINK) = 1 generate
            signal aresetn_fhAxis64_aclk: std_logic;
        begin
            xpm_cdc_sync_rst_inst : xpm_cdc_sync_rst
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT => 0,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0
                )
                port map (
                    src_rst => aresetn,   -- 1-bit input: Source reset signal.
                    -- is registered.

                    dest_clk => dataManager_clk, -- 1-bit input: Destination clock.
                    dest_rst => aresetn_fhAxis64_aclk  -- 1-bit output: src_rst synchronized to the destination clock domain. This output
                );
            -- use AXIS-64 clock
            dataManager_clk <= fhAxis64_aclk;

            -- we don't use the fhAxis port for 25G links
            chAssign: for STREAM in 0 to STREAMS_PER_LINK_FROMHOST-1 generate
                fhAxis(LINK, STREAM).tdata <= (others => '0');
                fhAxis(LINK, STREAM).tvalid <= '0';
                fhAxis(LINK, STREAM).tlast <= '0';
            end generate;

            dataManager_I: entity work.CRFromHostDataManagerAxis64
                generic map (
                    DATA_WIDTH => DATA_WIDTH
                )
                port map (
                    aresetn => aresetn_fhAxis64_aclk,
                    linkFifo_clk => dataManager_clk,
                    linkFifo_dout => linkFifo_dout(LINK),
                    linkFifo_rden => linkFifo_rden(LINK),
                    --linkFifo_valid => linkFifo_valid(LINK),
                    linkFifo_empty => linkFifo_empty(LINK),
                    fhAxis => fhAxis64(LINK),
                    fhAxis_tready => fhAxis64_tready(LINK)
                );
        end generate;
    end generate;
    sync_CLEAR_LATCH : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            src_clk => '0',
            src_in => register_map_control.CRFROMHOST_FIFO_STATUS.CLEAR(64),
            dest_clk => fromHostFifo_clk,
            dest_out => CLEAR_LATCH_sync
        );

    -- latching FIFO full status
    process (fromHostFifo_clk) begin
        if rising_edge(fromHostFifo_clk) then
            if aresetn = '0' then
                fifo_monitoring.FULL_LATCHED <= (others => '0');
            else
                -- clear latch flags
                if CLEAR_LATCH_sync = '1' then
                    fifo_monitoring.FULL_LATCHED <= (others => '0');
                end if;

                for I in 0 to LINK_NUM-1 loop
                    if linkFifo_full(I) = '1' then          -- should we use pfull here???
                        fifo_monitoring.FULL_LATCHED(I) <= '1';
                    end if;
                end loop;
            end if;
        end if;
    end process;

    -- fifo full current status
    fifo_full_status: for I in 0 to 23 generate
        no_link_present: if I >= LINK_NUM generate
            fifo_monitoring.FULL(24+I) <= '0';
        end generate;

        link_present: if I < LINK_NUM generate
            fifo_monitoring.FULL(24+I) <= linkFifo_full(I);      -- should we use pfull here???
        end generate;
    end generate;


end architecture;
