--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

library XPM;
    use XPM.VCOMPONENTS.ALL;

entity CRFromHostGroupFifo is
    generic (
        DATA_WIDTH           : integer := 256;
        --DEPTH                : integer := 512;
        --PROG_FULL_THRESHOLD  : integer := 480;
        CARD_TYPE            : integer := 712
    );
    port (
        clk                  : in  std_logic;
        areset               : in  std_logic;
        din                  : in  std_logic_vector(DATA_WIDTH-1 downto 0);
        dout                 : out std_logic_vector(31 downto 0);
        wren                 : in  std_logic;
        rden                 : in  std_logic;
        valid                : out std_logic;
        full                 : out std_logic;
        empty                : out std_logic;
        pfull                : out std_logic;
        drop                 : in  std_logic
    );
end CRFromHostGroupFifo;

architecture rtl of CRFromHostGroupFifo is
    signal fifo_din : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal fifo_dout : std_logic_vector(DATA_WIDTH-1 downto 0);
    signal fifo_wren : std_logic;
    signal fifo_rden : std_logic;
    signal fifo_full : std_logic;
    signal fifo_pfull : std_logic;
    signal fifo_empty : std_logic;

    constant WORDCNT_MAX : integer range 0 to 31 := (DATA_WIDTH/32)-1;
    signal wordcnt : integer range 0 to WORDCNT_MAX;

    function GROUPFIFO_PROG_FULL_THRESHOLD (brd: integer) return integer is
    begin
        if brd = 155 or brd = 182 or brd = 181 or brd = 180 or brd = 128 then
            return 57;
        else
            return 11;
        end if;
    end function;

    function GROUPFIFO_DEPTH (brd: integer) return integer is
    begin
        if brd = 155 or brd = 182 or brd = 181 or brd = 180 or brd = 128 then
            return 64;
        else
            return 16;
        end if;
    end function;

    function GROUPFIFO_MEMORY_TYPE (brd: integer) return string is
    begin
        if brd = 155 or brd = 182 or brd = 181 or brd = 180 or brd = 128 then
            return "auto";
        else
            return "distributed";
        end if;
    end function;
begin

    -- check programmable full threshold
    --assert PROG_FULL_THRESHOLD < DEPTH report "PROG_FULL_THRESHOLD has to be less than the FIFO depth!" severity failure;

    -- data to FIFO
    fifo_din <= din;

    -- FIFO writing
    fifo_wren <= wren and not fifo_full;
    full <= fifo_full;
    pfull <= fifo_pfull;

    -- FIFO reading (not every rden is forwarded to the FIFO)
    fifo_rden <= (not fifo_empty) when drop = '1' else
                 (rden and not fifo_empty) when wordcnt = 0 else
                 '0';
    empty <= fifo_empty;

    -- data output
    process (clk) begin
        if rising_edge(clk) then
            if areset = '1' then
                wordcnt <= WORDCNT_MAX;
            else

                if drop = '1' then
                    wordcnt <= WORDCNT_MAX;
                elsif rden = '1' then
                    if wordcnt = 0 then
                        wordcnt <= WORDCNT_MAX;
                    else
                        wordcnt <= wordcnt - 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- produce valid signal
    valid <= rden when rising_edge(clk);

    -- select output This statement is not well understood by Vivado, replacing by with .. select statement.
    --dout <= fifo_dout(wordcnt*16+15 downto wordcnt*16);

    w1024: if DATA_WIDTH = 1024 generate
        with wordcnt select dout <=
    fifo_dout(31  downto 0  ) when 0,
    fifo_dout(63  downto 32 ) when 1,
    fifo_dout(95  downto 64 ) when 2,
    fifo_dout(127 downto 96 ) when 3,
    fifo_dout(159 downto 128) when 4,
    fifo_dout(191 downto 160) when 5,
    fifo_dout(223 downto 192) when 6,
    fifo_dout(255 downto 224) when 7,
    fifo_dout(287 downto 256) when 8,
    fifo_dout(319 downto 288) when 9,
    fifo_dout(351 downto 320) when 10,
    fifo_dout(383 downto 352) when 11,
    fifo_dout(415 downto 384) when 12,
    fifo_dout(447 downto 416) when 13,
    fifo_dout(479 downto 448) when 14,
    fifo_dout(511 downto 480) when 15,
    fifo_dout(543 downto 512) when 16,
    fifo_dout(575 downto 544) when 17,
    fifo_dout(607 downto 576) when 18,
    fifo_dout(639 downto 608) when 19,
    fifo_dout(671 downto 640) when 20,
    fifo_dout(703 downto 672) when 21,
    fifo_dout(735 downto 704) when 22,
    fifo_dout(767 downto 736) when 23,
    fifo_dout(799 downto 768) when 24,
    fifo_dout(831 downto 800) when 25,
    fifo_dout(863 downto 832) when 26,
    fifo_dout(895 downto 864) when 27,
    fifo_dout(927 downto 896) when 28,
    fifo_dout(959 downto 928) when 29,
    fifo_dout(991 downto 960) when 30,
    fifo_dout(1023 downto 992) when 31,
    (others => '0') when others;
    end generate;

    w512: if DATA_WIDTH = 512 generate
        with wordcnt select dout <=
    fifo_dout(31  downto 0  ) when 0,
    fifo_dout(63  downto 32 ) when 1,
    fifo_dout(95  downto 64 ) when 2,
    fifo_dout(127 downto 96 ) when 3,
    fifo_dout(159 downto 128) when 4,
    fifo_dout(191 downto 160) when 5,
    fifo_dout(223 downto 192) when 6,
    fifo_dout(255 downto 224) when 7,
    fifo_dout(287 downto 256) when 8,
    fifo_dout(319 downto 288) when 9,
    fifo_dout(351 downto 320) when 10,
    fifo_dout(383 downto 352) when 11,
    fifo_dout(415 downto 384) when 12,
    fifo_dout(447 downto 416) when 13,
    fifo_dout(479 downto 448) when 14,
    fifo_dout(511 downto 480) when 15,
    (others => '0') when others;
    end generate;

    w256: if DATA_WIDTH = 256 generate
        with wordcnt select dout <=
    fifo_dout(31  downto 0  ) when 0,
    fifo_dout(63  downto 32 ) when 1,
    fifo_dout(95  downto 64 ) when 2,
    fifo_dout(127 downto 96 ) when 3,
    fifo_dout(159 downto 128) when 4,
    fifo_dout(191 downto 160) when 5,
    fifo_dout(223 downto 192) when 6,
    fifo_dout(255 downto 224) when 7,
    (others => '0') when others;
    end generate;

    fifo0: entity work.SRL16_FIFO
        generic map(
            DATA_WIDTH        => DATA_WIDTH,
            FWFT              => true
        )
        port map(
            rst     => areset,
            wr_clk  => clk,
            rd_clk  => clk,
            din     => fifo_din,
            dout    => fifo_dout,
            wr_en   => fifo_wren,
            rd_en   => fifo_rden,
            full    => fifo_full,
            empty   => fifo_empty,
            pfull   => fifo_pfull
        );

---- XPM macro
--fifo: xpm_fifo_sync
--    generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
--        FIFO_MEMORY_TYPE => GROUPFIFO_MEMORY_TYPE(CARD_TYPE),
--        FIFO_WRITE_DEPTH => GROUPFIFO_DEPTH(CARD_TYPE),
--        WRITE_DATA_WIDTH => DATA_WIDTH,
--        READ_MODE => "fwft",
--        FIFO_READ_LATENCY => 0,
--        FULL_RESET_VALUE => 0,
--        USE_ADV_FEATURES => "0002",
--        READ_DATA_WIDTH => DATA_WIDTH,
--        WR_DATA_COUNT_WIDTH => 1,
--        PROG_FULL_THRESH => GROUPFIFO_PROG_FULL_THRESHOLD(CARD_TYPE),
--        RD_DATA_COUNT_WIDTH => 1,
--        PROG_EMPTY_THRESH => 10,
--        DOUT_RESET_VALUE => "0",
--        ECC_MODE => "no_ecc",
--        SIM_ASSERT_CHK => 0,
--        WAKEUP_TIME => 0
--    )
--    port map (
--        sleep => '0',
--        rst => areset,
--        wr_clk => clk,
--        wr_en => fifo_wren,
--        din => fifo_din,
--        full => fifo_full,
--        prog_full => fifo_pfull,
--        wr_data_count => open,
--        overflow => open,
--        wr_rst_busy => open,
--        almost_full => open,
--        wr_ack => open,
--        rd_en => fifo_rden,
--        dout => fifo_dout,
--        empty => fifo_empty,
--        prog_empty => open,
--        rd_data_count => open,
--        underflow => open,
--        rd_rst_busy => open,
--        almost_empty => open,
--        data_valid => open,
--        injectsbiterr => '0',
--        injectdbiterr => '0',
--        sbiterr => open,
--        dbiterr => open
--    );


end architecture;
