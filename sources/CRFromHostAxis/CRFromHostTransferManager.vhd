--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use IEEE.MATH_REAL.LOG2;
    use IEEE.MATH_REAL.CEIL;

    use work.axi_stream_package.all;
    use work.FELIX_package.all;

entity CRFromHostTransferManager is
    generic (
        DATA_WIDTH : integer := 256;
        STREAMS_PER_GROUP_FROMHOST : integer := 1;
        STREAM_ADDR_OFFSET : integer := 0;
        SUPPORT_DELAY : boolean := false        -- boolean to support delay packet generation (FLX-1862)
    );
    port (
        clk : in std_logic;
        reset : in std_logic;
        groupFifoData : in std_logic_vector(31 downto 0);
        groupFifoEmpty : in std_logic;
        groupFifoRead : out std_logic;
        groupFifoDrop : out std_logic;
        stream_out : out axis_8_array_type(0 to STREAMS_PER_GROUP_FROMHOST-1);
        stream_out_tready : in axis_tready_array_type(0 to STREAMS_PER_GROUP_FROMHOST-1);
        broadcastEnable : in std_logic_vector(STREAMS_PER_GROUP_FROMHOST-1 downto 0) := (others => '0')
    );
end CRFromHostTransferManager;

architecture rtl of CRFromHostTransferManager is
    signal groupFifoData_i : std_logic_vector(31 downto 0);
    signal groupFifoEmpty_i : std_logic; -- @suppress "signal groupFifoEmpty_i is never read"
    signal groupFifoRead_i : std_logic;
    signal groupFifoDrop_i : std_logic;

    --constant ADDR_BITS_STREAM : integer := integer(ceil(log2(real(STREAMS_PER_GROUP_FROMHOST))));
    signal message_len : integer range 0 to 124;
    signal last_block : std_logic;
    signal broadcast_acked : std_logic_vector(0 to STREAMS_PER_GROUP_FROMHOST-1);
    signal datasel : integer range 0 to 3;
    signal generate_delay : std_logic;

    constant STREAM_NULL : axis_8_type := (
                                            tdata => (others => '0'),
                                            tvalid => '0',
                                            tlast => '0'
                                          );
    signal stream : axis_8_type;
    signal stream_tready : std_logic;
    signal stream_sel : integer range 0 to STREAMS_PER_GROUP_FROMHOST;
    signal stream_out_i : axis_8_array_type(0 to STREAMS_PER_GROUP_FROMHOST-1);

    type fsm_t is (HEADER, DATA);
    signal Z : fsm_t := HEADER;

-- attribute MARK_DEBUG : string;
-- attribute DONT_TOUCH : string;
-- attribute MARK_DEBUG of groupFifoData_i, groupFifoEmpty_i, groupFifoRead_i, stream, stream_tready, stream_sel, Z, message_len, last_block : signal is "true";
-- attribute DONT_TOUCH of groupFifoData_i, groupFifoEmpty_i, groupFifoRead_i, stream, stream_tready, stream_sel, Z, message_len, last_block : signal is "true";
begin

    groupFifoData_i <= groupFifoData;
    groupFifoEmpty_i <= groupFifoEmpty;
    groupFifoRead <= groupFifoRead_i;
    groupFifoDrop <= groupFifoDrop_i;

    -- maintain acknowledge for broadcasts
    process (clk, reset)
        variable all_acked : boolean;
    begin
        if reset = '1' then
            broadcast_acked <= (others => '0');
        else
            if rising_edge(clk) then
                if stream_sel /= STREAMS_PER_GROUP_FROMHOST then
                    -- if we don't broadcast reset
                    broadcast_acked <= (others => '0');
                else
                    -- if we broadcast make sure all stream outputs have acknowledged the transfer
                    all_acked := true;
                    for I in 0 to STREAMS_PER_GROUP_FROMHOST-1 loop
                        if not ((broadcast_acked(I) = '1') or ((stream_out_i(I).tvalid = '1') and (stream_out_tready(I) = '1'))) and broadcastEnable(I) = '1' then
                            all_acked := false;
                        end if;
                    end loop;

                    -- reset if all have acknowledged
                    if all_acked then
                        broadcast_acked <= (others => '0');
                    else
                        for I in 0 to STREAMS_PER_GROUP_FROMHOST-1 loop
                            broadcast_acked(I) <= broadcast_acked(I) or (stream_out_i(I).tvalid and stream_out_tready(I)) or not broadcastEnable(I);
                        end loop;
                    end if;
                end if;
            end if;
        end if;
    end process;

    -- fanout streams
    stream_fanout_mux: process (stream, stream_sel, stream_out_tready, broadcastEnable, broadcast_acked)
        variable tready_anded : std_logic;
    begin
        if stream_sel = STREAMS_PER_GROUP_FROMHOST then
            tready_anded := '1';
            for I in 0 to STREAMS_PER_GROUP_FROMHOST-1 loop
                stream_out_i(I).tdata <= stream.tdata;
                stream_out_i(I).tlast <= stream.tlast;
                stream_out_i(I).tvalid <= stream.tvalid and not broadcast_acked(I) and broadcastEnable(I);
                tready_anded := tready_anded and (stream_out_tready(I) or broadcast_acked(I) or not broadcastEnable(I));
            end loop;
            stream_tready <= tready_anded;
        else
            for I in 0 to STREAMS_PER_GROUP_FROMHOST-1 loop
                if I = stream_sel then
                    stream_out_i(I) <= stream;
                else
                    stream_out_i(I) <= STREAM_NULL;
                end if;
            end loop;
            stream_tready <= stream_out_tready(stream_sel);
        end if;
    end process;
    stream_out <= stream_out_i;

    -- data output
    process (groupFifoEmpty, message_len, last_block, datasel, groupFifoData_i, generate_delay) begin
        if (groupFifoEmpty = '0') and (message_len > 0) then
            if generate_delay = '1' then
                if ((datasel = 0) and groupFifoData_i(31 downto 24) = x"FF") or
                           ((datasel = 1) and groupFifoData_i(23 downto 16) = x"FF") or
                           ((datasel = 2) and groupFifoData_i(15 downto 8) = x"FF") or
               ((datasel = 3) and groupFifoData_i(7 downto 0) = x"FF") then
                    stream.tvalid <= '1';
                else
                    stream.tvalid <= '0';
                end if;
            else
                stream.tvalid <= '1';
            end if;
        else
            stream.tvalid <= '0';
        end if;

        if ((message_len = 1) and (last_block = '1')) or (generate_delay = '1') then
            stream.tlast <= '1';
        else
            stream.tlast <= '0';
        end if;

        case datasel is
            when 0 => stream.tdata <= groupFifoData_i(31 downto 24);
            when 1 => stream.tdata <= groupFifoData_i(23 downto 16);
            when 2 => stream.tdata <= groupFifoData_i(15 downto 8);
            when 3 => stream.tdata <= groupFifoData_i(7 downto 0);
        end case;
    end process;

    process (groupFifoEmpty, Z, datasel, stream, stream_tready, message_len, generate_delay) begin
        if (groupFifoEmpty = '0') then
            if Z = HEADER then
                groupFifoRead_i <= '1';
            elsif (Z = DATA) and (datasel = 3) then
                if ((stream.tvalid = '1' or generate_delay = '1') and stream_tready = '1') or (message_len = 0) then
                    groupFifoRead_i <= '1';
                else
                    groupFifoRead_i <= '0';
                end if;
            else
                groupFifoRead_i <= '0';
            end if;
        else
            groupFifoRead_i <= '0';
        end if;
    end process;

    process (stream, stream_tready, message_len, generate_delay) begin
        if ((stream.tvalid = '1' or generate_delay = '1') and stream_tready = '1') and (message_len = 1) then
            groupFifoDrop_i <= '1';
        else
            groupFifoDrop_i <= '0';
        end if;
    end process;

    -- state machine
    process (clk) begin
        if rising_edge(clk) then
            if reset = '1' then
                last_block <= '0';
                message_len <= 0;
                datasel <= 0;
                generate_delay <= '0';
                Z <= HEADER;
            else
                case Z is
                    when HEADER =>
                        if groupFifoEmpty = '0' then
                            if groupFifoData_i(7 downto 0) = "11111111" then
                                last_block <= '0';
                                message_len <= (DATA_WIDTH-32)/8;
                                generate_delay <= '0';
                            elsif (groupFifoData_i(7 downto 0) = "00000000") and SUPPORT_DELAY then
                                last_block <= '1';
                                message_len <= (DATA_WIDTH-32)/8;
                                generate_delay <= '1';
                            else
                                last_block <= '1';
                                message_len <= to_integer(unsigned(groupFifoData_i(7 downto 0)));
                                generate_delay <= '0';
                            end if;
                            if groupFifoData_i(15 downto 8) = "11111111" then
                                stream_sel <= STREAMS_PER_GROUP_FROMHOST;
                            else
                                stream_sel <= to_integer(unsigned(groupFifoData_i(15 downto 8))) - STREAM_ADDR_OFFSET;
                            end if;
                            datasel <= 0;
                            Z <= DATA;
                        end if;
                    when DATA =>
                        if ((stream.tvalid = '1' or generate_delay = '1') and stream_tready = '1') or (message_len = 0) then
                            if message_len > 1 then
                                message_len <= message_len - 1;
                            else
                                message_len <= 0;
                                Z <= HEADER;
                            end if;
                            if datasel = 3 then
                                datasel <= 0;
                            else
                                datasel <= datasel + 1;
                            end if;
                        end if;
                end case;
            end if;
        end if;
    end process;


end architecture;
