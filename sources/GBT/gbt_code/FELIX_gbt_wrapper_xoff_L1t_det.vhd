--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               RHabraken
--!               Frans Schreuder
--!               Mesfin Gebyehu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: FELIX V7 (GTH) GBT Wrapper
-- Module Name: gbt_top - Behavioral
-- Project Name:
-- Target Devices: V7
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX GBT & GTH
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VCOMPONENTS.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;
use work.FMTransceiverPackage.all;

entity FELIX_gbt_rx_wrapper is
  Generic (
    GBT_NUM                     : integer := 1
    );
  Port (
    rst_hw                      : in std_logic;
    clk40_in                    : in std_logic;
    -- gbt moniforing reg
    --register_map_gbt_monitor    : out  register_map_gbt_monitor_type;
    -- for CentralRouter
    RX_DATA_20b_in              : in slv20_array(0 to GBT_NUM-1);
    RX_120b_out                 : out txrx120b_type(GBT_NUM-1 downto 0);
    FRAME_LOCKED_O              : out std_logic_vector(GBT_NUM-1 downto 0);
    RXSLIDE_out                 : out std_logic_vector(GBT_NUM-1 downto 0);
    RX_CDRLOCKED_IN             : in std_logic_vector(GBT_NUM-1 downto 0);
    RX_FSM_RESET_DONE           : in std_logic_vector(GBT_NUM-1 downto 0);
    RXOUTCLK_IN                 : in std_logic_vector(GBT_NUM-1 downto 0);
    GT_RXRESET                  : out std_logic_vector(GBT_NUM-1 downto 0)
    --TX_FRAME_CLK_I              : in std_logic_vector(GBT_NUM-1 downto 0)


    );
end FELIX_gbt_rx_wrapper;

architecture Behavioral of FELIX_gbt_rx_wrapper is
  component fifo_GBT2CR IS
    PORT (
      rd_rst    : IN STD_LOGIC;
      wr_clk    : IN STD_LOGIC;
      rd_clk    : IN STD_LOGIC;
      din       : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
      wr_en     : IN STD_LOGIC;
      rd_en     : IN STD_LOGIC;
      dout      : OUT STD_LOGIC_VECTOR(119 DOWNTO 0);
      full      : OUT STD_LOGIC;
      empty     : OUT STD_LOGIC;
--      prog_full : OUT STD_LOGIC;
      prog_empty : OUT STD_LOGIC
      );
  END component;

component ila_gbt_alig
  PORT (
      clk : IN STD_LOGIC;
      probe0 : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
      probe1 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      probe2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      probe3 : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
      probe4 : IN STD_LOGIC_VECTOR(11 DOWNTO 0)
  );
 end component ila_gbt_alig;
  

  -- constant QUAD_NUM : integer := GBT_NUM / 4;

  signal RxSlide_c              : std_logic_vector(GBT_NUM-1 downto 0);
  signal RxSlide_i              : std_logic_vector(GBT_NUM-1 downto 0);
  
  signal RxCdrLock_f            : std_logic_vector(GBT_NUM-1 downto 0);
--  signal qplllock               : std_logic_vector(5 downto 0);

--  signal tx_is_data             : std_logic_vector(GBT_NUM-1 downto 0);
--  signal TX_RESET               : std_logic_vector(GBT_NUM-1 downto 0);
--  signal TX_RESET_i             : std_logic_vector(GBT_NUM-1 downto 0);

  signal RX_RESET               : std_logic_vector(GBT_NUM-1 downto 0);
  signal RX_RESET_i             : std_logic_vector(GBT_NUM-1 downto 0);
--  signal gbt_data_format        : std_logic_vector(47 downto 0);

--  SIGNAL CXP1_TX_PLL_LOCKEd     : STD_LOGIC;
--  signal CXP2_TX_PLL_LOCKED     : STD_LOGIC;
--  signal cpu_rst                : STD_LOGIC;
--  signal RX_ALIGN_SW            : STD_LOGIC;
--  signal RX_ALIGN_TB_SW         : STD_logic;

--  signal rx_pll_locked          : std_logic_vector(GBT_NUM-1 downto 0);
  signal outsel_ii              : std_logic_vector(GBT_NUM-1 downto 0);
  signal outsel_o               : std_logic_vector(GBT_NUM-1 downto 0);

  signal rx_is_header           : std_logic_vector(GBT_NUM-1 downto 0);
  signal alignment_done         : std_logic_vector(GBT_NUM-1 downto 0);
  signal rx_is_data             : std_logic_vector(GBT_NUM-1 downto 0);
  signal RX_HEADER_FOUND        : std_logic_vector(GBT_NUM-1 downto 0);

--  signal cxp1_rx_bitslip_nbr    : std_logic_vector(71 downto 0);
--  signal cxp2_rx_bitslip_nbr    : std_logic_vector(71 downto 0);

  signal RxSlide                : std_logic_vector(GBT_NUM-1 downto 0);

--  signal GT_TX_WORD_CLK         : std_logic_vector(GBT_NUM-1 downto 0);
--  signal TX_TC_METHOD           : std_logic_vector(GBT_NUM-1 downto 0);
  signal TC_EDGE                : std_logic_vector(GBT_NUM-1 downto 0);

  

  signal GT_RX_WORD_CLK         : std_logic_vector(GBT_NUM-1 downto 0);
  signal alignment_chk_rst_c    : std_logic_vector(GBT_NUM-1 downto 0);
  signal alignment_chk_rst_c1   : std_logic_vector(GBT_NUM-1 downto 0);
  signal alignment_chk_rst      : std_logic_vector(GBT_NUM-1 downto 0);

--  signal rstframeclk            : STD_LOGIC;
  signal alignment_chk_rst_i    : STD_LOGIC;
--  signal rstframeclk1           : STD_LOGIC;
--  signal rx_frame_phase_ok_cxp1 : STD_LOGIC;
--  signal rx_frame_phase_ok_cxp2 : std_logic;

--  signal CXP2_GTH_REF_CLK_BUF   : std_logic;
--  signal CXP1_GTH_REF_CLK       : std_logic;
--  signal CXP2_GTH_REF_CLK       : std_logic;
--  signal CXP1_GTH_REF_CLK_BUF   : std_logic;
--  signal DESMUX_USE_SW          : std_logic;
--  signal counterbig             : std_logic_vector(26 downto 0);
--  signal counterbig1            : std_logic_vector(26 downto 0);

--  signal rstframeclk_3r         : std_logic;
--  signal rstframeclk_r          : std_logic;
--  signal rstframeclk_2r         : std_logic;
--  signal rstframeclk1_3r        : std_logic;
--  signal rstframeclk1_r         : std_logic;
--  signal rstframeclk1_2r        : std_logic;
  --signal cxp1_tx_pll_rst        : std_logic;
  --signal cxp2_tx_pll_rst        : std_logic;
 -- signal SOFT_TXRST_GT          : std_logic_vector(GBT_NUM-1 downto 0);
  signal TopBot                 : std_logic_vector(GBT_NUM-1 downto 0);
--  signal TopBot_C               : std_logic_vector(GBT_NUM-1 downto 0);
--  signal TopBot_i               : std_logic_vector(GBT_NUM-1 downto 0);
--  signal SOFT_RXRST_GT          : std_logic_vector(GBT_NUM-1 downto 0);
  --signal SOFT_TXRST_ALL         : std_logic_vector(5 downto 0);
--  signal SOFT_RXRST_ALL         : std_logic_vector(5 downto 0);
  signal RX_OPT                 : std_logic_vector(95 downto 0);
--  SIGNAL DATA_RXFORMAT          : std_logic_vector(47 downto 0);
  signal DATA_RXFORMAT_i        : std_logic_vector(95 downto 0);

  SIGNAL OddEven                : std_logic_vector(GBT_NUM-1 downto 0);
--  signal OddEven_i              : std_logic_vector(GBT_NUM-1 downto 0);
--  signal OddEven_c              : std_logic_vector(GBT_NUM-1 downto 0);
  signal ext_trig_realign       : std_logic_vector(GBT_NUM-1 downto 0);



--  signal General_ctrl           : std_logic_vector(63 downto 0);
  signal fifo_empty             : std_logic_vector(63 downto 0);


--  signal GT_RXUSRCLK            : std_logic;

--  type txrx4b_24ch_type         is array (GBT_NUM-1 downto 0) of std_logic_vector(3 downto 0);

  signal RX_120b_out_i          : txrx120b_type(0 to (GBT_NUM-1));
  signal RX_120b_out_ii         : txrx120b_type(0 to (GBT_NUM-1));

--  signal RxWordCnt_out          : txrx4b_24ch_type;

  
  signal RX_FLAG_Oi             : std_logic_vector(GBT_NUM-1 downto 0);
  
  signal pulse_cnt              : std_logic_vector(29 downto 0);
  signal pulse_lg               : std_logic;

  
  signal error_orig             : std_logic_vector(GBT_NUM-1 downto 0);
  signal FSM_RST                : std_logic_vector(GBT_NUM-1 downto 0);
  signal auto_gth_rxrst         : std_logic_vector(GBT_NUM-1 downto 0);
  signal auto_gbt_rxrst         : std_logic_vector(GBT_NUM-1 downto 0);
--  signal gbt_rx_reset_i         : std_logic_vector(GBT_NUM-1 downto 0);
--  signal gtrx_reset_i           : std_logic_vector(GBT_NUM-1 downto 0);
--  signal gbt_sel                : std_logic_vector(GBT_NUM-1 downto 0);

--  signal TX_LINERATE            : std_logic_vector(GBT_NUM-1 downto 0);
--  signal RX_LINERATE            : std_logic_vector(GBT_NUM-1 downto 0);
--  signal GT_RXOUTCLK            : std_logic_vector(GBT_NUM-1 downto 0);
--  signal GT_TXOUTCLK            : std_logic_vector(GBT_NUM-1 downto 0);

  signal BITSLIP_MANUAL_r       : std_logic_vector(GBT_NUM-1 downto 0);
  signal BITSLIP_MANUAL_2r      : std_logic_vector(GBT_NUM-1 downto 0);
--  signal error_f                : std_logic_vector(GBT_NUM-1 downto 0);
--  signal BITSLIP_MANUAL_3r      : std_logic_vector(GBT_NUM-1 downto 0);
  signal alignment_done_chk_cnt : std_logic_vector(12 downto 0);
  signal alignment_done_a       : std_logic_vector(GBT_NUM-1 downto 0);

  --signal RX_N_i                 : std_logic_vector(GBT_NUM-1 downto 0):=x"000000";
  --signal RX_P_i                 : std_logic_vector(GBT_NUM-1 downto 0):=x"000000";
  --signal TX_N_i                 : std_logic_vector(GBT_NUM-1 downto 0):=x"000000";
  --signal TX_P_i                 : std_logic_vector(GBT_NUM-1 downto 0):=x"000000";
  signal alignment_done_f       : std_logic_vector(GBT_NUM-1 downto 0);

begin

  FRAME_LOCKED_O <= alignment_done_f(GBT_NUM-1 downto 0);
--  alignment_done <= RxCdrLock_f;
  
  
  --error_gen : for i in GBT_NUM-1 downto 0 generate
  --  error_f(i) <= error_orig(i) and alignment_done_f(i);
  --end generate;
  
  alignment_chk_rst_i   <= '0';

  DATA_RXFORMAT_i <= GBT_DATA_RXFORMAT_PACKAGE;
  
  process(clk40_in)
  begin
    if clk40_in'event and clk40_in='1' then
      pulse_lg <= pulse_cnt(20);
      if pulse_cnt(20)='1' then
        pulse_cnt <=(others=>'0');
      else
        pulse_cnt <= pulse_cnt+'1';
      end if;
    end if;
  end process;

  process(clk40_in)
  begin
    if clk40_in'event and clk40_in='1' then
      alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';
    end if;
  end process;

  rxalign_auto : for i in GBT_NUM-1 downto 0 generate

    process(clk40_in)
    begin
      if clk40_in'event and clk40_in='1' then
        if alignment_done_chk_cnt="0000000000000" then
          alignment_done_a(i) <= alignment_done(i);
        else
          alignment_done_a(i) <= alignment_done(i) and alignment_done_a(i);
        end if;
        if alignment_done_chk_cnt="1000000000000" then
          alignment_done_f(i) <=  RxCdrLock_f(i) and alignment_done_a(i);
        end if;
      end if;
    end process;

        RxCdrLock_f(i) <= RX_CDRLOCKED_IN(i);

    RX_120b_out(i) <= RX_120b_out_ii(i) when alignment_done_f(i)='1' else
                      (others =>'0');

    auto_rxrst : entity work.FELIX_GBT_RX_AUTO_RST
      port map
      (
        FSM_CLK                 => clk40_in,
        pulse_lg                => pulse_lg,
        GTHRXRESET_DONE         => RX_FSM_RESET_DONE(i),
        alignment_chk_rst       => alignment_chk_rst_c1(i),
        GBT_LOCK                => alignment_done_f(i),--alignment_done(i),
        auto_gth_rxrst          => auto_gth_rxrst(i),
        ext_trig_realign        => ext_trig_realign(i),
        auto_gbt_rxrst          => auto_gbt_rxrst(i)
        );

    rafsm : entity work.FELIX_GBT_RXSLIDE_FSM
      port map
      (
        ext_trig_realign        => ext_trig_realign(i),

        FSM_RST                 => FSM_RST(i),
        FSM_CLK                 => clk40_in,

        GBT_LOCK                => alignment_done(i),
        RxSlide                 => RxSlide_c(i),

        alignment_chk_rst       => alignment_chk_rst_c(i)
        );
    RX_RESET(i)         <= rst_hw;
    FSM_RST(i)          <= RX_RESET(i);
    GT_RXRESET(i)       <= (auto_gth_rxrst(i));
    RX_RESET_i(i)       <= (RX_RESET(i) or auto_gbt_rxrst(i));
    alignment_chk_rst(i) <= (alignment_chk_rst_i or alignment_chk_rst_c(i) or alignment_chk_rst_c1(i));

    
    RxSlide_i(i)     <= RxCdrLock_f(i) and RxSlide_c(i);
--  end generate;






--gbtRxTx : for i in GBT_NUM-1 downto 0 generate
  process(GT_RX_WORD_CLK)
  begin
    if rising_edge(GT_RX_WORD_CLK(i)) then
      BITSLIP_MANUAL_r(i)       <= RxSlide_i(i);
      BITSLIP_MANUAL_2r(i)      <= BITSLIP_MANUAL_r(i);
--      BITSLIP_MANUAL_3r(i)      <= BITSLIP_MANUAL_2r(i);
      RxSlide(i)                <= BITSLIP_MANUAL_r(i) and (not BITSLIP_MANUAL_2r(i));
    end if;
  end process;
  
  RXSLIDE_out(i) <= RxSlide(i);

  gbtRx_inst: entity work.gbtRx_wrap_FELIX
    
    port map
    (
      error_o                 => error_orig(i),
      RX_FLAG                 => RX_FLAG_Oi(i),
      Rx_DATA_FORMAT          => DATA_RXFORMAT_i(2*i+1 downto 2*i),
      RX_LATOPT_DES           => RX_OPT(i),

      TC_EDGE                 => TC_EDGE(i),
      
      alignment_chk_rst       => alignment_chk_rst(i),
      alignment_done_O        => alignment_done(i),
      L40M                    => clk40_in,
      outsel_i                => outsel_ii(i),
      outsel_o                => outsel_o(i),

      OddEven			=> '0',--OddEven_i(i),
      TopBot                    => '0',--TopBot_i(i),
      data_sel                  => (others => '0'),

      RX_RESET_i  		=> RX_RESET_i(i),
      RX_FRAME_CLK_O 		=> open,--RX_FRAME_CLK_O(i),
      RX_WORD_IS_HEADER_O       => rx_is_header(i),
      RX_HEADER_FOUND	        => RX_HEADER_FOUND(i),
      RX_ISDATA_FLAG_O          => rx_is_data(i),
      RX_DATA_20b_I    	        => RX_DATA_20b_in(i),
      RX_DATA_120b_O    	=> RX_120b_out_i(i),
      des_rxusrclk              => GT_RX_WORD_CLK(i),
      RX_WORDCLK_I      	=> GT_RX_WORD_CLK(i)

      );

  fifo_inst: fifo_GBT2CR
    PORT MAP(
      rd_rst    => rst_hw,
      wr_clk    => GT_RX_WORD_CLK(i),
      rd_clk    => clk40_in,--FIFO_RD_CLK(i),
      din       => RX_120b_out_i(i),
      wr_en     => RX_FLAG_Oi(i),
      rd_en     => not fifo_empty(i),--'1',--FIFO_RD_EN(i),
      dout      => RX_120b_out_ii(i),
      full      => open,
      empty     => open,
 --     prog_full => open,--FIFO_FULL(i),
      prog_empty => fifo_empty(i)--FIFO_EMPTY(i)
      );

end generate;


  outsel_ii     <= outsel_o;

  GT_RX_WORD_CLK <= RXOUTCLK_IN;

--  register_map_gbt_monitor.GBT_RXCDR_LOCK(11 downto 0)          <= alignment_done; --alignment_done_a;
--  register_map_gbt_monitor.GBT_ALIGNMENT_DONE(11 downto 0)      <= RX_CDRLOCKED_IN;
--  register_map_gbt_monitor.GBT_TXRESET_DONE(11 downto 0)      <= TX_RESET_DONE;
--  register_map_gbt_monitor.GBT_RXRESET_DONE(11 downto 0)      <= RX_FSM_RESET_DONE;
  

--     -- GBTWrapperMonitors
--   type register_map_gbt_monitor_type is record
--     GBT_VERSION                    : bitfield_gbt_version_r_type;  
--     GBT_TXRESET_DONE               : std_logic_vector(47 downto 0);   -- TX Reset done [47:0]
--     GBT_RXRESET_DONE               : std_logic_vector(47 downto 0);   -- RX Reset done [47:0]
--     GBT_TXFSMRESET_DONE            : std_logic_vector(47 downto 0);   -- TX FSM Reset done [47:0]
--     GBT_RXFSMRESET_DONE            : std_logic_vector(47 downto 0);   -- RX FSM Reset done [47:0]
--     GBT_CPLL_FBCLK_LOST            : std_logic_vector(47 downto 0);   -- CPLL FBCLK LOST [47:0]
--     GBT_PLL_LOCK                   : bitfield_gbt_pll_lock_r_type; 
--     GBT_RXCDR_LOCK                 : std_logic_vector(47 downto 0);   -- RX CDR LOCK [47:0]
--     GBT_CLK_SAMPLED                : std_logic_vector(47 downto 0);   -- clk sampled [47:0]
--     GBT_RX_IS_HEADER               : std_logic_vector(47 downto 0);   -- RX IS HEADER [47:0]
--     GBT_RX_IS_DATA                 : std_logic_vector(47 downto 0);   -- RX IS DATA [47:0]
--     GBT_RX_HEADER_FOUND            : std_logic_vector(47 downto 0);   -- RX HEADER FOUND [47:0]
--     GBT_ALIGNMENT_DONE             : std_logic_vector(47 downto 0);   -- RX ALIGNMENT DONE [47:0]
--     GBT_OUT_MUX_STATUS             : std_logic_vector(47 downto 0);   -- GBT output mux status [47:0]
--     GBT_ERROR                      : std_logic_vector(47 downto 0);   -- Error flags [47:0]
--     GBT_GBT_TOPBOT_C               : std_logic_vector(47 downto 0);   -- TopBot_c [47:0]
--     GBT_FM_RX_DISP_ERROR1          : std_logic_vector(47 downto 0);   -- Rx disparity error [47:0]
--     GBT_FM_RX_DISP_ERROR2          : std_logic_vector(47 downto 0);   -- Rx disparity error [96:48]
--     GBT_FM_RX_NOTINTABLE1          : std_logic_vector(47 downto 0);   -- Rx not in table [47:0]
--     GBT_FM_RX_NOTINTABLE2          : std_logic_vector(47 downto 0);   -- Rx not in table [96:48]
-- end record;

   ila3: ila_gbt_alig
    port map(
      clk    => clk40_in,
      probe0 => RX_120b_out_ii(0), 
      probe1 => fifo_empty(11 downto 0), 
      probe2 => alignment_done,
      probe3 => alignment_chk_rst,
      probe4 => RX_HEADER_FOUND
      );    
  
end Behavioral;
