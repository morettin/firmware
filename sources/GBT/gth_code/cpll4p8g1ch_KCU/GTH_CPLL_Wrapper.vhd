--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Kai Chen
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2016/02/24 04:43:14 PM
-- Design Name: KCU CPLL GTH Wrapper
-- Module Name: GTH_CPLL_Wrapper_KCU - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE: KCU CPLL GTH (for GBT)
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.FELIX_gbt_package.all;

    use work.FELIX_gbt_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
    use UNISIM.VComponents.all;

entity GTH_CPLL_Wrapper is
    Port (


        --cpllreset_in                           : in std_logic_vector(0 downto 0);
        cpllfbclklost_out                          : out std_logic_vector(0 downto 0);
        cplllock_out                               : out std_logic_vector(0 downto 0);

        gt0_rxusrclk_in                            : in std_logic_vector(0 downto 0);
        gt0_txusrclk_in                            : in std_logic_vector(0 downto 0);
        gt0_rxoutclk_out                           : out std_logic_vector(0 downto 0);
        gt0_txoutclk_out                           : out std_logic_vector(0 downto 0);
        gthrxn_in                                  : in std_logic_vector(0 downto 0);
        gthrxp_in                                  : in std_logic_vector(0 downto 0);
        gthtxn_out                                 : out std_logic_vector(0 downto 0);
        gthtxp_out                                 : out std_logic_vector(0 downto 0);

        drpclk_in                                  : in std_logic_vector(0 downto 0);
        gtrefclk0_in                               : in std_logic_vector(0 downto 0);

        rxpolarity_in                              : in std_logic_vector(0 downto 0);
        txpolarity_in                              : in std_logic_vector(0 downto 0);
        loopback_in                                : in std_logic_vector(2 downto 0);
        rxcdrhold_in                               : in std_logic;
        userdata_tx_in                             : in std_logic_vector(19 downto 0);
        userdata_rx_out                            : out std_logic_vector(19 downto 0);


        userclk_rx_reset_in                        : in std_logic_vector(0 downto 0);
        userclk_tx_reset_in                        : in std_logic_vector(0 downto 0);

        -- reset_clk_freerun_in                    : in std_logic_vector(0 downto 0);
        reset_all_in                               : in std_logic_vector(0 downto 0);
        reset_tx_pll_and_datapath_in               : in std_logic_vector(0 downto 0);
        reset_tx_datapath_in                       : in std_logic_vector(0 downto 0);
        reset_rx_pll_and_datapath_in               : in std_logic_vector(0 downto 0);
        reset_rx_datapath_in                       : in std_logic_vector(0 downto 0);


        rxslide_in                                 : in std_logic_vector(0 downto 0);


        rxpmaresetdone_out                         : out std_logic_vector(0 downto 0);
        txpmaresetdone_out                         : out std_logic_vector(0 downto 0);

        reset_tx_done_out                          : out std_logic_vector(0 downto 0);
        reset_rx_done_out                          : out std_logic_vector(0 downto 0);
        reset_rx_cdr_stable_out                    : out std_logic_vector(0 downto 0)

    );
end GTH_CPLL_Wrapper;

architecture Behavioral of GTH_CPLL_Wrapper is


    component KCU_NORXBUF_PCS_CPLL_1CH
        port(
            gtwiz_userclk_tx_active_in         : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_userclk_rx_active_in         : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_buffbypass_tx_reset_in       : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_buffbypass_tx_start_user_in  : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_buffbypass_tx_done_out       : out STD_LOGIC_VECTOR(0 to 0);
            gtwiz_buffbypass_tx_error_out      : out STD_LOGIC_VECTOR(0 to 0);
            gtwiz_buffbypass_rx_reset_in       : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_buffbypass_rx_start_user_in  : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_buffbypass_rx_done_out       : out STD_LOGIC_VECTOR(0 to 0);
            gtwiz_buffbypass_rx_error_out      : out STD_LOGIC_VECTOR(0 to 0);
            gtwiz_reset_clk_freerun_in         : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_reset_all_in                 : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_reset_tx_pll_and_datapath_in : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_reset_tx_datapath_in         : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_reset_rx_pll_and_datapath_in : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_reset_rx_datapath_in         : in  STD_LOGIC_VECTOR(0 to 0);
            gtwiz_reset_rx_cdr_stable_out      : out STD_LOGIC_VECTOR(0 to 0);
            gtwiz_reset_tx_done_out            : out STD_LOGIC_VECTOR(0 to 0);
            gtwiz_reset_rx_done_out            : out STD_LOGIC_VECTOR(0 to 0);
            gtwiz_userdata_tx_in               : in  STD_LOGIC_VECTOR(19 downto 0);
            gtwiz_userdata_rx_out              : out STD_LOGIC_VECTOR(19 downto 0);
            cplllockdetclk_in                  : in  STD_LOGIC_VECTOR(0 to 0);
            cpllreset_in                       : in  STD_LOGIC_VECTOR(0 to 0);
            drpclk_in                          : in  STD_LOGIC_VECTOR(0 to 0);
            gtgrefclk_in                       : in  STD_LOGIC_VECTOR(0 to 0);
            gthrxn_in                          : in  STD_LOGIC_VECTOR(0 to 0);
            gthrxp_in                          : in  STD_LOGIC_VECTOR(0 to 0);
            gtrefclk0_in                       : in  STD_LOGIC_VECTOR(0 to 0);
            loopback_in                        : in  STD_LOGIC_VECTOR(2 downto 0);
            rxcdrhold_in                       : in  STD_LOGIC_VECTOR(0 to 0);
            rxcdrreset_in                      : in  STD_LOGIC_VECTOR(0 to 0);
            rxcommadeten_in                    : in  STD_LOGIC_VECTOR(0 to 0);
            rxmcommaalignen_in                 : in  STD_LOGIC_VECTOR(0 to 0);
            rxpcommaalignen_in                 : in  STD_LOGIC_VECTOR(0 to 0);
            rxpolarity_in                      : in  STD_LOGIC_VECTOR(0 to 0);
            rxslide_in                         : in  STD_LOGIC_VECTOR(0 to 0);
            rxusrclk_in                        : in  STD_LOGIC_VECTOR(0 to 0);
            rxusrclk2_in                       : in  STD_LOGIC_VECTOR(0 to 0);
            txpolarity_in                      : in  STD_LOGIC_VECTOR(0 to 0);
            txusrclk_in                        : in  STD_LOGIC_VECTOR(0 to 0);
            txusrclk2_in                       : in  STD_LOGIC_VECTOR(0 to 0);
            cpllfbclklost_out                  : out STD_LOGIC_VECTOR(0 to 0);
            cplllock_out                       : out STD_LOGIC_VECTOR(0 to 0);
            gthtxn_out                         : out STD_LOGIC_VECTOR(0 to 0);
            gthtxp_out                         : out STD_LOGIC_VECTOR(0 to 0);
            gtpowergood_out                    : out STD_LOGIC_VECTOR(0 to 0);
            rxbyteisaligned_out                : out STD_LOGIC_VECTOR(0 to 0);
            rxbyterealign_out                  : out STD_LOGIC_VECTOR(0 to 0);
            rxcdrlock_out                      : out STD_LOGIC_VECTOR(0 to 0);
            rxcommadet_out                     : out STD_LOGIC_VECTOR(0 to 0);
            rxdata_out                         : out STD_LOGIC_VECTOR(127 downto 0);
            rxoutclk_out                       : out STD_LOGIC_VECTOR(0 to 0);
            rxpmaresetdone_out                 : out STD_LOGIC_VECTOR(0 to 0);
            rxresetdone_out                    : out STD_LOGIC_VECTOR(0 to 0);
            txoutclk_out                       : out STD_LOGIC_VECTOR(0 to 0);
            txpmaresetdone_out                 : out STD_LOGIC_VECTOR(0 to 0);
            txresetdone_out                    : out STD_LOGIC_VECTOR(0 to 0)
        );
    end component KCU_NORXBUF_PCS_CPLL_1CH;

    COMPONENT KCU_RXBUF_PMA_CPLL_1CH
        PORT (
            gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
            gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(19 DOWNTO 0);
            cplllockdetclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            cpllreset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            drpclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtgrefclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxn_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxp_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtrefclk0_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            loopback_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            rxcdrhold_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcdrreset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcommadeten_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxmcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpolarity_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxslide_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxusrclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxusrclk2_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            txpolarity_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            txusrclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            txusrclk2_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            cpllfbclklost_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            cplllock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthtxn_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthtxp_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtpowergood_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxbyteisaligned_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxbyterealign_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcdrlock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcommadet_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxdata_out : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
            rxoutclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txoutclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;

    signal rxusrclk       : std_logic_vector(0 downto 0);
    signal txoutclk_int   : std_logic_vector(0 downto 0);
    signal rxoutclk_int   : std_logic_vector(0 downto 0);
    signal rxusrclk_int   : std_logic_vector(0 downto 0);
    signal rxusrclk2_int  : std_logic_vector(0 downto 0);
    signal txusrclk_int   : std_logic_vector(0 downto 0);
    signal txusrclk2_int  : std_logic_vector(0 downto 0);
    signal userclk_tx_active_out  : std_logic_vector(0 downto 0);
    signal userclk_rx_active_out  : std_logic_vector(0 downto 0);
    signal userclk_rx_active_out_p: std_logic_vector(0 downto 0);
    signal userclk_tx_active_out_p: std_logic_vector(0 downto 0);
    signal txusrclk       : std_logic;
    signal vccvec         : std_logic_vector(0 downto 0);
    signal gndvec         : std_logic_vector(0 downto 0);

begin

    vccvec(0)     <= '1';
    gndvec(0)     <= '0';
    -- RxUsrClk
    rxusrclk      <= gt0_rxusrclk_in;
    rxusrclk_int  <= rxusrclk;
    rxusrclk2_int <= rxusrclk;

    gt0_rxoutclk_out <= rxoutclk_int;
    gt0_txoutclk_out <= txoutclk_int;

    --  -- TxUsrClk
    --  tx_usrclk_bufg: bufg_gt
    --  port map(
    --    i => txoutclk_int(0),
    --    div =>"000",
    --    clr =>'0',--userclk_tx_reset_in,--'0',
    --    cemask =>'0',
    --    clrmask=>'0',
    --    ce=>'1',
    --    o => txusrclk
    --  );

    txusrclk              <= gt0_txusrclk_in(0);
    txusrclk2_int(0)      <= txusrclk;
    txusrclk_int(0)       <= txusrclk;

    process(userclk_tx_reset_in(0), txusrclk)
    begin
        if userclk_tx_reset_in(0) = '1' then
            userclk_tx_active_out(0)          <= '0';
        elsif txusrclk'event and txusrclk = '1' then
            userclk_tx_active_out_p(0)        <= '1';
            userclk_tx_active_out             <= userclk_tx_active_out_p;
        end if;
    end process;

    process(userclk_rx_reset_in(0), rxusrclk(0))
    begin
        if userclk_rx_reset_in(0) = '1' then
            userclk_rx_active_out(0)          <= '0';
        elsif rxusrclk(0)'event and rxusrclk(0) = '1' then
            userclk_rx_active_out_p(0)        <= '1';
            userclk_rx_active_out             <= userclk_rx_active_out_p;
        end if;
    end process;



    NORXBUF_GEN: if KCU_LOWER_LATENCY = '1' generate
        gtwizard_ultrascale_single_channel_cpll_inst:  KCU_NORXBUF_PCS_CPLL_1CH
            port map(
                gtpowergood_out => open,
                rxbyteisaligned_out => open,
                rxbyterealign_out => open,
                rxcommadet_out => open,
                rxmcommaalignen_in => "0",
                rxpcommaalignen_in => "0",
                cpllreset_in                            => gndvec,
                gtgrefclk_in                            => gndvec,
                rxcdrreset_in                           =>  gndvec,--
                rxcommadeten_in                         =>  vccvec,--
                cpllfbclklost_out                       => cpllfbclklost_out,
                cplllock_out                            => cplllock_out,
                cplllockdetclk_in                       => drpclk_in,
                rxcdrlock_out                           => open,--
                rxdata_out                              => open,--
                loopback_in                             => loopback_in,
                rxcdrhold_in(0)                         => rxcdrhold_in,

                gthrxn_in                               => gthrxn_in,
                gthrxp_in                               => gthrxp_in,
                gthtxn_out                              => gthtxn_out,
                gthtxp_out                              => gthtxp_out,

                gtwiz_userclk_tx_active_in              => userclk_tx_active_out,
                gtwiz_userclk_rx_active_in              => userclk_rx_active_out,
                gtwiz_buffbypass_tx_reset_in            => gndvec,--buffbypass_tx_reset_in,
                gtwiz_buffbypass_tx_start_user_in       => gndvec,--buffbypass_tx_start_user_in,
                gtwiz_buffbypass_tx_done_out            => open,--buffbypass_tx_done_out,
                gtwiz_buffbypass_tx_error_out           => open,--buffbypass_tx_error_out,

                gtwiz_buffbypass_rx_reset_in           => gndvec,--
                gtwiz_buffbypass_rx_start_user_in      => gndvec,--
                gtwiz_buffbypass_rx_done_out           => open,
                gtwiz_buffbypass_rx_error_out          => open,

                gtwiz_reset_clk_freerun_in              => drpclk_in,--gtwiz_reset_clk_freerun_in,

                gtwiz_reset_all_in                      => reset_all_in,

                gtwiz_reset_tx_pll_and_datapath_in      => reset_tx_pll_and_datapath_in,
                gtwiz_reset_tx_datapath_in              => reset_tx_datapath_in,
                gtwiz_reset_rx_pll_and_datapath_in      => reset_rx_pll_and_datapath_in,
                gtwiz_reset_rx_datapath_in              => reset_rx_datapath_in,

                gtwiz_reset_rx_cdr_stable_out           => reset_rx_cdr_stable_out,
                gtwiz_reset_tx_done_out                 => reset_tx_done_out,
                gtwiz_reset_rx_done_out                 => reset_rx_done_out,
                rxresetdone_out                         => open,
                txresetdone_out                         => open,
                rxpolarity_in                           => rxpolarity_in,
                txpolarity_in                           => txpolarity_in,
                gtwiz_userdata_tx_in                    => userdata_tx_in,
                gtwiz_userdata_rx_out                   => userdata_rx_out,

                drpclk_in                               => drpclk_in,
                gtrefclk0_in                            => gtrefclk0_in,
                rxusrclk_in                             => rxusrclk_int,
                rxusrclk2_in                            => rxusrclk2_int,
                txusrclk_in                             => txusrclk_int,
                txusrclk2_in                            => txusrclk2_int,
                rxoutclk_out                            => rxoutclk_int,
                rxslide_in                              => rxslide_in,
                rxpmaresetdone_out                      => rxpmaresetdone_out,
                txoutclk_out                            => txoutclk_int,
                txpmaresetdone_out                      => txpmaresetdone_out
            );

    end generate;

    RXBUF_GEN: if KCU_LOWER_LATENCY = '0' generate
        gtwizard_ultrascale_single_channel_cpll_inst:  KCU_RXBUF_PMA_CPLL_1CH
            PORT MAP (
                gtwiz_userclk_tx_active_in => userclk_tx_active_out,
                gtwiz_userclk_rx_active_in => userclk_rx_active_out,
                gtwiz_buffbypass_tx_reset_in => gndvec,
                gtwiz_buffbypass_tx_start_user_in => gndvec,
                gtwiz_buffbypass_tx_done_out => open,
                gtwiz_buffbypass_tx_error_out => open,
                gtwiz_reset_clk_freerun_in => drpclk_in,
                gtwiz_reset_all_in => reset_all_in,
                gtwiz_reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath_in,
                gtwiz_reset_tx_datapath_in => reset_tx_datapath_in,
                gtwiz_reset_rx_pll_and_datapath_in => reset_rx_pll_and_datapath_in,
                gtwiz_reset_rx_datapath_in => reset_rx_datapath_in,
                gtwiz_reset_rx_cdr_stable_out => reset_rx_cdr_stable_out,
                gtwiz_reset_tx_done_out => reset_tx_done_out,
                gtwiz_reset_rx_done_out => reset_rx_done_out,
                gtwiz_userdata_tx_in => userdata_tx_in,
                gtwiz_userdata_rx_out => userdata_rx_out,
                cplllockdetclk_in => drpclk_in,
                cpllreset_in => gndvec,
                drpclk_in => drpclk_in,
                gtgrefclk_in => gndvec,
                gthrxn_in => gthrxn_in,
                gthrxp_in => gthrxp_in,
                gtrefclk0_in => gtrefclk0_in,
                loopback_in => loopback_in,
                rxcdrhold_in(0) => rxcdrhold_in,
                rxcdrreset_in => gndvec,
                rxcommadeten_in => vccvec,
                rxmcommaalignen_in => gndvec,
                rxpcommaalignen_in => gndvec,
                rxpolarity_in => rxpolarity_in,
                rxslide_in => rxslide_in,
                rxusrclk_in => rxusrclk_int,
                rxusrclk2_in => rxusrclk2_int,
                txpolarity_in => txpolarity_in,
                txusrclk_in => txusrclk_int,
                txusrclk2_in => txusrclk2_int,
                cpllfbclklost_out => cpllfbclklost_out,
                cplllock_out => cplllock_out,
                gthtxn_out => gthtxn_out,
                gthtxp_out => gthtxp_out,
                gtpowergood_out => open,
                rxbyteisaligned_out => open,
                rxbyterealign_out => open,
                rxcdrlock_out => open,
                rxcommadet_out => open,
                rxdata_out => open,
                rxoutclk_out => rxoutclk_int,
                rxpmaresetdone_out => rxpmaresetdone_out,
                rxresetdone_out => open,
                txoutclk_out => txoutclk_int,
                txpmaresetdone_out => txpmaresetdone_out,
                txresetdone_out => open
            );
    end generate;

end Behavioral;
