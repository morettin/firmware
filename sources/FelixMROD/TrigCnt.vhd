--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.TrigCnt
--! =project    FELIX_MROD
--! @modified   Mon Nov  4 09:45:43 2019
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity TrigCnt is
  port (
    ECR       : in     std_logic;
    EndEvt    : in     std_logic;
    ForceIdle : out    std_logic;
    GTMode    : in     std_logic_vector(2 downto 0);
    MReset    : in     std_logic;
    SClk      : in     std_logic;
    Trigger   : in     std_logic);
end entity TrigCnt;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.TrigCnt.a0
--! =project    FELIX_MROD
--! @modified   Mon Nov  4 09:45:43 2019
--!-----------------------------------------------------------------------------


architecture a0 of TrigCnt is

  signal TCnt       : std_logic_vector(7 downto 0) ;
  signal CntBusy    : std_logic ;
  signal CntFull    : std_logic ;
  signal CntLast    : std_logic ;
  signal IdleRMode  : std_logic;
  signal EventDone  : std_logic;

  --GTMode(0) : 1/0 : enable / disable CSM testrun
  --GTMode(1) : 1/0 : circulate fifo / fill fifo from SHARC (replace BOT+EOT)
  --GTMode(2) : 1/0 : triggered / untriggered test mode

begin

  -- IdleRMode<='1';    -- stop running mode (possibly finishing last event)
  -- EventDone<='1';    -- finished last event (in circulate mode)
  -- ForceIdle<='1';    -- both flags set: force MGT transmitter into Idle.
  -- need to set ForceIdle one cycle earlier (using EndEvt=1)

  ForceIdle <= '1' when ((IdleRMode = '1' and EventDone = '1') or EndEvt = '1') else '0';

  CntBusy <= '1' when (TCnt /= "00000000") else '0';
  CntFull <= '1' when (TCnt  = "11111111") else '0';
  CntLast <= '1' when (TCnt  = "00000001") else '0';

  prCnt:
  process (SClk, MReset)
    variable count: unsigned(7 downto 0);
  begin
    if (MReset = '1') then
      IdleRMode <= '1';                 -- stop running mode
      EventDone <= '1';                 -- last event done
      count := (others => '0');         -- trigger counter
    elsif (rising_edge(SClk)) then
      case GTMode is
      when "000" => 
        IdleRMode <= '1';               -- stop running mode
        EventDone <= '1';               -- last event done
      when "001" | "011" =>
        -- untriggered mode, yes/no circulate events: run (send out data)
        IdleRMode <= '0';               -- run
        EventDone <= '0';               --
      when "110" | "010"  =>
        -- (un-)triggered mode, circulate events, but stopped:
        IdleRMode <= '1';               -- stop running (finishing last event)
        if (EndEvt = '1') then
          EventDone <= '1';             -- last event done: really stop
          count := (others => '0');
        end if;
      when "111" =>
        if (ECR = '1') then
          IdleRMode <= '1';             -- stop running mode
          EventDone <= '1';             -- last event done
          count := (others => '0');     -- trigger counter
        else
          if (Trigger = '1' and EndEvt = '1') then
            null;                       -- no count
          elsif (Trigger = '1') then
            if (CntFull = '0') then
              count := count + 1;       -- count up
            end if;
          elsif (EndEvt = '1') then
            if (CntBusy = '1') then
              count := count - 1;       -- count down
            end if;
          end if;
          if (EndEvt = '1' and CntBusy = '1' and CntLast = '1') then
            IdleRMode <= '1';           -- stop when last event done
            EventDone <= '1';           --
          else
            IdleRMode <= not CntBusy;   -- idle when not busy (tcnt/=0)
            EventDone <= not CntBusy;   -- clear this flag
          end if;
        end if;
      when others =>
        -- "100"|"101": triggered mode, transparent data is impractical.
        IdleRMode <= '1';               -- stop running mode
        EventDone <= '1';               -- last event done
      end case;
    end if;
    TCnt <= std_logic_vector(count);
  end process;

end architecture a0 ; -- of TrigCnt

