--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.EvtMux
--! =project    FELIX_MROD
--! @modified   Sat Mar 14 11:36:38 2020
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity EvtMux is
  generic(
    EnPar : boolean := true);
  port (
    DF         : in     std_logic_vector(31 downto 0);
    DValid     : in     std_logic;
    ECR        : in     std_logic;
    Empty      : in     std_logic;
    EndEvt     : out    std_logic;
    Even_Odd_n : in     std_logic;
    ForceIdle  : in     std_logic;
    Full       : in     std_logic;
    GTMode     : in     std_logic_vector(2 downto 0);
    MReset     : in     std_logic;
    QF         : out    std_logic_vector(31 downto 0);
    QValid     : out    std_logic;
    RClk       : in     std_logic;
    RdReq      : out    std_logic);
end entity EvtMux;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.EvtMux.a0
--! =project    FELIX_MROD
--! @modified   Sat Mar 14 11:36:38 2020
--!-----------------------------------------------------------------------------


architecture a0 of EvtMux is

  type states is (Idle, ReadF, CheckF);
  signal mstate : states;
  --
  signal EnRead     : std_logic;
  signal EnaRun     : std_logic;
  signal EndEvtInt  : std_logic;
  signal BOTEvt     : std_logic;
  signal EOTEvt     : std_logic;
  signal Hpar       : std_logic;
  signal ECnt       : std_logic_vector(11 downto 0);
  signal Fdata      : std_logic_vector(31 downto 0);
  --
  constant IDSEP    : std_logic_vector(3 downto 0) :=   "1101"; -- Separator
  constant IDEOE    : std_logic_vector(5 downto 0) := "001110"; -- End of Event !
  constant IDBOT    : std_logic_vector(2 downto 0) :=    "101"; -- BOT identifier
  constant IDEOT    : std_logic_vector(3 downto 0) :=   "1100"; -- EOT identifier

  --GTMode(0) : 1/0 : enable / disable CSM emulator run
  --GTMode(1) : 1/0 : circulate fifo / fill fifo from HOST SHARC (replace BOT+EOT)
  --GTMode(2) : 1/0 : triggered / untriggered test mode

  -- BOT:  31-29="101",  28-24="TDCnr", 23-12=ECnt, 11-00=BCnt
  -- EOT:  31-28="1100", 27-24="00x0" , 23-12=ECnt, 11-00=wcnt

begin

  EnaRun <= GTMode(0);  -- GTMode(0)='1': run the emulator;  when '0': load data into fifo
  EndEvt <= '1' when (DValid = '1' and EndEvtInt = '1') else '0';
  -- signal end of event (special separator xD0E0/xD4E0)

  -- For the circulating data: look for BOT and EOT and EndOfEvent.
  -- When EndOfEvent found: increment internal event counter (ECnt).
  -- When BOT or EOT found: replace eventcounter value by internal event counter.
  -- Ignore parity when checking for End of Event (xD0E).

  BOTEvt    <= '1' when (DF(31 downto 29) = IDBOT) else '0';    -- Begin TDC
  EOTEvt    <= '1' when (DF(31 downto 28) = IDEOT) else '0';    -- End TDC
  EndEvtInt <= '1' when (DF(31 downto 28) = IDSEP
                     and DF(25 downto 20) = IDEOE) else '0';    -- End of Event

  -- Select ECnt to overwrite parts of the BOT and EOT words in the data.
  -- Force bit 26 to zero before calculating the (horizontal) parity (odd parity goes to bit 26)

  Fdata(31 downto 12) <= DF(31 downto 27) & '0' & DF(25) & DF(24) & ECnt  -- insert new ECnt
    when ((GTMode(1) = '1') and (BOTEvt = '1' or EOTEvt = '1'))           -- when BOT or EOT
    else   DF(31 downto 27) & '0' & DF(25 downto 12);           -- any other data unchanged

  Fdata(11 downto 0)  <= x"BC" & ECnt(3 downto 0)   -- insert new BCnt: x"BC" & ECnt(3-0)
    when ((GTMode(1) = '1') and (BOTEvt = '1'))     -- when BOT
    else   DF(11 downto 0);                         -- any other data unchanged

  -- read FifoBuffer when (not EmptyBuffer and not FullSender and not ForceIdle)

  RdReq <= '1' when (EnRead = '1' and Empty = '0' and Full = '0' and ForceIdle = '0')
    else '0';

  ----------------------------------------------------------------------------

  pr0:
  process(RClk, MReset)
  begin
    if (MReset = '1') then
      EnRead <= '0';
      QValid <= '0';
      QF     <= (others => '0');
      mstate <= Idle;
    elsif (rising_edge(RClk)) then
      case mstate is
      when Idle =>
        QValid <= '0';
        QF     <= (others => '0');
        if (EnaRun = '1' and Empty = '0' and Full = '0') then
          EnRead <= '1';
          mstate <= CheckF;             -- start allowed if fifos not empty/full
        else
          EnRead <= '0';
          mstate <= Idle;
        end if;
      when ReadF =>
        if (DValid = '1') then
          QValid <= '1';                -- word valid
          if (EnPar = true) then
            QF <= Fdata(31 downto 27) & Hpar & Fdata(25 downto 0);  -- modified data
          else
            QF <= DF;                   -- data directly from fifo
          end if;
        else
          QValid <= '0';
        end if;
        if (EnaRun = '1' and Empty = '0' and Full = '0') then   -- continue if fifos not empty/full
          EnRead <= '1';
          mstate <= ReadF;
        else
          EnRead <= '0';                -- wait if fifos empty/full
          mstate <= CheckF;
        end if;
      when CheckF =>
        if (DValid = '1') then
          QValid <= '1';                -- "last" word valid
          if (EnPar = true) then
            QF <= Fdata(31 downto 27) & Hpar & Fdata(25 downto 0);  -- modified data
          else
            QF <= DF;                   -- data directly from fifo
          end if;
        else
          QValid <= '0';
        end if;
        if (EnaRun = '0') then          -- if channel turned off: stop
          EnRead <= '0';
          mstate <= Idle;
        elsif (Empty = '0' and Full = '0') then -- if fifos not empty/full: continue
          EnRead <= '1';
          mstate <= ReadF;
        else                            -- if empty/full: hold
          EnRead <= '0';
          mstate <= CheckF;
        end if;
      when others =>
        mstate <= Idle;
      end case;
    end if;
  end process;

  -- bit27 is the TDC serial parity error: should be 0, but leave as it is!
  -- bit26 is the GOL parity: should be calculated before sending the data.
  -- The outgoing MD (muxtiplexed data) gets new parity:
  -- The parity is calculated over 32 bits and inserted into bit 26.
  -- Note that bit Fdata(26) is "preset" zero before the calculation.
  -- Even_Odd_n selects whether the total parity is even or odd.

  prPar:
  process (Fdata, Even_Odd_n)
    variable par : std_logic;
  begin
    par := not Even_Odd_n;       -- Even_Odd_n=0: use odd parity
    for i in 31 downto 0 loop
      par := par xor Fdata(i);
    end loop;
    Hpar <= par;
  end process;

  prCnt:
  process (RClk, MReset)
    variable cnt : unsigned(11 downto 0);
  begin
    if (MReset = '1') then
      cnt := (others => '0');
    elsif (rising_edge(RClk)) then
      if (GTMode(2 downto 0) = "000" or ECR = '1') then
        cnt := (others => '0');
      elsif (DValid = '1' and EndEvtInt = '1') then
        cnt := cnt + 1;
      end if;
    end if;
    ECnt <= std_logic_vector(cnt);
  end process;

end architecture a0 ; -- of EvtMux

