--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.MakeBlocks
--! =project    FELIX_MROD
--! @modified   Sat May 15 10:59:38 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity MakeBlocks is
  generic(
    chid : integer := 0);
  port (
    Busy       : in     std_logic;
    CkEna100k  : in     std_logic;
    Empty      : out    std_logic;
    EmptySupp  : in     std_logic;
    EnaCh      : in     std_logic;
    EnaPassAll : in     std_logic;
    Full       : out    std_logic;
    MReset     : in     std_logic;
    MaxTimeOut : in     std_logic_vector(15 downto 0);
    QF         : out    std_logic_vector(32 downto 0);
    QValid     : out    std_logic;
    RxClk      : in     std_logic;
    RxData     : in     std_logic_vector(31 downto 0);
    RxValid    : in     std_logic;
    SetHPTDC   : in     std_logic;
    clk80      : in     std_logic);
end entity MakeBlocks;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.MakeBlocks.a0
--! =project    FELIX_MROD
--! @modified   Sat May 15 10:59:38 2021
--!-----------------------------------------------------------------------------

architecture a0 of MakeBlocks is

  signal Empty_net : std_logic;
  signal RdReq     : std_logic;
  signal u5_QValid : std_logic;
  signal u5_Q      : std_logic_vector(31 downto 0);
  signal WData     : std_logic_vector(31 downto 0);
  signal WValid    : std_logic;
  signal Full_net  : std_logic;

begin
  --
  --RxClk: 50MHz
  --
  --
  Empty <= Empty_net;
  Full <= Full_net;

  u1: entity work.EmptySup(a0)
    port map(
      EmptySupp  => EmptySupp,
      EnaCh      => EnaCh,
      EnaPassAll => EnaPassAll,
      Full       => Full_net,
      MReset     => MReset,
      RxClk      => RxClk,
      RxData     => RxData,
      RxValid    => RxValid,
      SetHPTDC   => SetHPTDC,
      WData      => WData,
      WValid     => WValid);

  u5: entity work.FifoCSMdata(a0)
    port map(
      D      => WData,
      WrReq  => WValid,
      WClk   => RxClk,
      MReset => MReset,
      Full   => Full_net,
      RClk   => clk80,
      RdReq  => RdReq,
      Empty  => Empty_net,
      Q      => u5_Q,
      QValid => u5_QValid);

  u6: entity work.PrepaChunk(a0)
    port map(
      Busy       => Busy,
      CkEna100k  => CkEna100k,
      DF         => u5_Q,
      DValid     => u5_QValid,
      Empty      => Empty_net,
      EnaCh      => EnaCh,
      MReset     => MReset,
      MaxTimeOut => MaxTimeOut,
      QF         => QF,
      QValid     => QValid,
      RClk       => clk80,
      RdReq      => RdReq);

end architecture a0 ; -- of MakeBlocks

