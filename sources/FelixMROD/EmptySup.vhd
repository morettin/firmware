--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.EmptySup
--! =project    FELIX_MROD
--! @modified   Mon Jun 21 15:19:59 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity EmptySup is
  port (
    EmptySupp  : in     std_logic;
    EnaCh      : in     std_logic;
    EnaPassAll : in     std_logic;
    Full       : in     std_logic;
    MReset     : in     std_logic;
    RxClk      : in     std_logic;
    RxData     : in     std_logic_vector(31 downto 0);
    RxValid    : in     std_logic;
    SetHPTDC   : in     std_logic;
    WData      : out    std_logic_vector(31 downto 0);
    WValid     : out    std_logic);
end entity EmptySup;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.EmptySup.a0
--! =project    FELIX_MROD
--! @modified   Mon Jun 21 15:19:59 2021
--!-----------------------------------------------------------------------------


architecture a0 of EmptySup is

  type std_logic_v32array is array (natural range <>) of std_logic_vector(31 downto 0);
  type std_logic_v18array is array (natural range <>) of std_logic_vector(17 downto 0);

  signal DataPipe     : std_logic_v32array(0 to 18);
  signal SepFlagPipe  : std_logic_vector(0 to 18);
  signal FileSep      : std_logic_v18array(0 to 3);
  signal WFlags       : std_logic_vector(17 downto 0);
  signal ECnt         : unsigned(4 downto 0);
  signal TCnt         : unsigned(4 downto 0);
  signal EmptySep     : std_logic;
  signal EmptySepD    : std_logic;                      -- empty separator done
  signal EmptySepR    : std_logic_vector(1 downto 0);   -- count max 2 separators
  signal doTCnt1      : std_logic;
  signal doTCnt2      : std_logic;
  signal doTRes1      : std_logic;
  signal doTRes2      : std_logic;

  function make_uint(sinput : unsigned) return integer is
  begin
    return to_integer (sinput);
  end make_uint;

  constant Even_Odd_n : std_logic := '0';       -- 0: odd parity mode
  constant TDCHeader  : std_logic_vector( 3 downto 0) := x"A";
  constant TDCErrCode : std_logic_vector( 3 downto 0) := x"9";
  constant GOLErrCode : std_logic_vector( 3 downto 0) := x"D";

  signal PassOne      : std_logic;      -- pass one valid TDC (will set PassAll)
  signal PassAll      : std_logic;      -- pass all data (while at least one TDC has data)
  signal QData        : std_logic_vector(31 downto 0);
  signal QValid       : std_logic;
  signal UData        : std_logic_vector(31 downto 0);
  signal UValid       : std_logic;

  constant ESdebug    : boolean := false;
  constant sup1stsep  : boolean := false;

begin

  -- UData/UValid is data/valid from the transceiver
  -- QData/QValid is data/valid from the empty suppress pipe

  WData  <= QData  when (EmptySupp = '1') else UData;
  WValid <= QValid when (EmptySupp = '1') else UValid;

  --------------------------------------------------------------------

  prPipe:
  process (RxClk, MReset)
    variable dFlag    : std_logic;
    variable dNum     : unsigned(1 downto 0);
    variable eNum     : unsigned(1 downto 0);
    variable amtD     : std_logic_vector(31 downto 0);
    variable repD     : std_logic_vector(31 downto 0);
    variable gParity  : std_logic;
    variable tdcHead  : std_logic;

    procedure ShiftDataPipe is
      -- Shift the data pipe of 32 bit CSM data towards output,
      -- while changing ID's (for HPTDC), checking parity;
      -- while modifying ID's into x9 (for tdcpar) and xD (for golpar);
      -- while adding the time-slot counter to TDC headers (as TDC number);
      -- while storing the data flag (dFlag) and shifting these flags (RFlags).
      begin
      DataPipe(0) <= RxData;                            -- copy input data
      if (SetHPTDC = '1') then
        -- if there are HPTDCs (not AMT) on the mezzanines
        if (DataPipe(0)(31 downto 28) = "0010") then
          amtD(31 downto 28) := "1010";                 -- replace HPTDC header ID (2 ->A)
          amtD(26) := not DataPipe(0)(26);              -- correct gol parity: invert 
        elsif (DataPipe(0)(31 downto 28) = "0011") then
          amtD(31 downto 28) := "1100";                 -- replace HPTDC trailer ID (3 ->C)
          amtD(26) := DataPipe(0)(26);                  -- keep gol parity
        else
          amtD(31 downto 28) := DataPipe(0)(31 downto 28);  -- others: keep as is
          amtD(26) := DataPipe(0)(26);                      -- keep gol parity
        end if;
      else
        amtD(31 downto 28) := DataPipe(0)(31 downto 28);    -- AMT keep as is
        amtD(26) := DataPipe(0)(26);                        -- keep gol parity
      end if;
      amtD(27)          := DataPipe(0)(27);             -- keep tdc parity error bit
      amtD(25 downto 0) := DataPipe(0)(25 downto 0);
      DataPipe(1) <= amtD;                              -- copy AMT/HPTDC data
      --
      if (DataPipe(1)(31 downto 28) = TDCHeader) then
        tdcHead := '1';
      else
        tdcHead := '0';
      end if;
      -- The parity bit sent with the 31 databits and it is located in bit '26' of
      -- the incoming data word.
      -- Parity is checked over the full 32 bits (including bit the parity bit '26').
      -- A parity error is fed back into bit '26'.
      gParity := not Even_Odd_n;  -- Set initial value for parity thus setting Even or Odd
      for k in 0 to 31 loop       -- Caculate parity over all 32 bits
        gParity := gParity xor DataPipe(1)(k);
      end loop;
      -- Note: Highest Priority has the GOL_Error Replacement code
      --       Second TDC_Error Replacement Code
      --       Last the TDC-ID replacement in the TDC-Header
      if (gParity = '1') then                               -- if GOL Parity Error
        repD(31 downto 28) := GOLErrCode;
        repD(27 downto 24) := DataPipe(1)(31 downto 28);    -- Bits 27-24 the original ID
      elsif (DataPipe(1)(27) = '1') then                    -- if TDCParErr
        repD(31 downto 28) := TDCErrCode;
        repD(27 downto 24) := DataPipe(1)(31 downto 28);    -- Bits 27-24 the original ID
      elsif (tdcHead = '1') then                            -- if TDC header
        repD(31 downto 29) := DataPipe(1)(31 downto 29);
        if (ESdebug = true) then
          repD(28 downto 24) := std_logic_vector(TCnt);     -- add TDC number (slot number)
        else
          repD(28 downto 24) := DataPipe(1)(28 downto 24);  -- feed original data
        end if;
      else
        repD(31 downto 24) := DataPipe(1)(31 downto 24);
      end iF;
      repD(23 downto 0) := DataPipe(1)(23 downto 0);
      DataPipe(2) <= repD;
      --
      for i in 2 to 17 loop
        DataPipe(i+1) <= DataPipe(i);           -- shift data up towards output (18)
      end loop;
      --
    end procedure ShiftDataPipe;
    --
    procedure ShiftFlagPipe is
      -- Shift the flag pipe towards the output,
      -- while entering a new flag at the beginning when necessary;
      -- while checking at the end, when a separator exits, and adding flags
      -- to the separator word before passing it to the output.
      -- Empty separators are suppressed from the third consecutive one.
      variable sdata : std_logic_vector(31 downto 0);
    begin
      -- note that SepFlagPipe(0) gets separator indicator from input RxData
      --
      SepFlagPipe(1 to 18) <= SepFlagPipe(0 to 17); -- shift sepeartor flag thru pipe
      if (SepFlagPipe(18) = '1') then           -- if separator at output of pipe
        --#UValid <= '1';                          -- valid unsuppressed data for debugging
        --                                      -- this ignores initial data before first separator
        UData <= x"D0000000";                   -- unsuppresed separator
        UValid <= PassOne;                      -- valid unsuppressed data for debugging
        if (PassOne = '1') then
          PassAll <= '1';                       -- set flag to pass whole cycle
        else
          PassAll <= '0';                       -- clr flag to NOT pass whole cycle
        end if;        
        --
        --if (ECnt = "10010") then
        --  sdata  := x"D00" & "00" & RFlags;     -- get current flag word (after 18 TDCs) 
        --  WFlags <= RFlags;                     -- copy pattern for sender
        --else                                    -- too early separator (add flags from store)
        sdata  := x"D00" & "00" & FileSep(make_uint(eNum)); -- copy separator plus flags
        WFlags <= FileSep(make_uint(eNum))(17 downto 0);  -- copy pattern for sender
        --end if;
        QData <= sdata;
        --ifold
        if (sup1stsep = true) then      -- suppress first empty separator
        if (sdata = x"D0000000") then
          if (EmptySep = '1') then
            QValid <= '0';
          else
            QValid <= '1';
            EmptySep <= '1';
          end if;
        else
          QValid <= '1';
          EmptySep <= '0';
        end if;
        end if;
        --endold
        if (sdata = x"D0000000") then
          if (EmptySepR = "11") then            -- double separator
            if (EmptySepD = '1') then
              QValid <= '0';                    -- done before: no write
            else
              QValid <= '1';                    -- first double: write
              EmptySepD <= '1';
            end if;
          else                                  -- single separator
            EmptySepR <= EmptySepR(0) & '1';    -- do not write
            QValid <= '0';
          end if;
        else
          EmptySepR <= "00";                    -- non zero separator
          EmptySepD <= '0';                     -- clr flags and write
          QValid <= '1';
        end if;
        --QValid <= '1';
        ECnt <= "00000";                        -- clear counter
        eNum := eNum + 1;                       -- adjust output ptr to filesep
      else                                      -- not a separator
        ECnt  <= ECnt  + "00001";
        UData <= DataPipe(18);                  -- unsuppresed data in pipe
        --#UValid <= '1';                         -- valid unsuppressed data for debugging
        UValid <= PassAll;                      -- set valid when passing whole cycle
        if (WFlags(0) = '1') then               -- if TDC data (non-empty data)
          QData <= DataPipe(18);                -- suppressed data
          QValid <= '1';
        else
          --QData <= (others => '0');             -- hold data
          QValid <= '0';                        -- empty data: do not send
        end if;
        WFlags <= '0' & WFlags(17 downto 1);    -- shift pattern bits right
      end if;
    end procedure ShiftFlagPipe;
    --
    procedure HoldDataPipe is
    -- Ignore the input data by clearing the data valid flag.
    -- At the same time, nothing can come out of the datapipe.
    begin
      dFlag := '0';                     -- ignore data
      QValid <= '0';                    -- no valid data
      UValid <= '0';                    -- no valid unsuppressed data for debugging
    end procedure HoldDataPipe;
    --
    --
    -- Shift the data words into the pipe. When there is no data or idle words,
    -- shift an invalid indicator in the pipe. Valid data comes with RxValid=1.
    --
  begin
    if (MReset = '1') then
      dFlag   := '0';                   -- flag for "data" word
      gParity := '0';
      tdcHead := '0';
      amtD    := (26 => '1', others => '0');
      repD    := (26 => '1', others => '0');
      EmptySep  <= '0';
      EmptySepD <= '0';
      EmptySepR <= "11";
      WFlags  <= (others => '0');
      doTCnt1 <= '0';
      doTCnt2 <= '0';
      doTRes1 <= '0';
      doTRes2 <= '0';
      ECnt    <= (others => '1');
      TCnt    <= (others => '0');
      QData  <= (others => '0');
      QValid <= '0';
      UData  <= (others => '0');
      UValid <= '0';
      PassOne <= '0';                   -- found valid TDC in this cycle
      PassAll <= '0';                   -- may suppress whole empty cycles
      for i in 0 to 18 loop
        DataPipe(i) <= x"04000000";     -- clear data pipe (0x04000000)
        SepFlagPipe(i) <= '0';          -- clear separator flag pipe
      end loop;
      dNum  := "11";                    -- input  ptr for FileSep (incr before use)
      eNum  := "00";                    -- output ptr for FileSep
      for i in 0 to 3 loop
        FileSep(i) <= (others => '0');  -- clear separator file
      end loop;
    elsif (rising_edge(RxClk)) then
      if (RxValid = '1') then
        doTCnt2 <= doTCnt1;
        doTRes2 <= doTRes1;
        if (doTRes2 = '1') then
          TCnt <= (others => '0');      -- TDC/slot counter
        elsif (doTCnt2 = '1') then
          TCnt <= TCnt + 1;
        end if;
        --if (RxData(31 downto 27) = "11010"    -- xD00 or xD4E (ignore bit 26 only)
        --and RxData(25 downto 24) = "00") then
        if (RxData(31 downto 27) = "11010") then  -- xD00 or xD4E (ignore bit 26 and 25,24)
          dNum := dNum + 1;             -- new separator: incr ptr to circular buffer for RFlags
          dFlag := '0';                 -- no data flag on separator
          FileSep(make_uint(dNum)) <= (others => '0'); -- clear data RFlags
          SepFlagPipe(0) <= '1';        -- put "set separator" in the pipe
          ShiftDataPipe;
          ShiftFlagPipe;
          PassOne <= '0';               -- clear flag (no TDC value yet)
          doTRes1 <= '1';
          doTCnt1 <= '0';
        elsif (RxData(31 downto 24) = x"04") then
          if (EmptySupp = '1') then
            dFlag := '0';               -- no Data present (empty slot)
          else
            dFlag := '1';               -- keep Data present (even in empty slot)
          end if;
          if (EnaPassAll = '1') then    -- if forced to pass all
            PassOne <= '1';             -- set  flag (TDC value found in this cycle)
          end if;
          -- store input data flag and shift the array of RFlags (to be used for sending data to output)
          FileSep(make_uint(dNum)) <= dFlag & FileSep(make_uint(dNum))(17 downto 1);
          SepFlagPipe(0) <= '0';        -- put "no separator" in the pipe
          ShiftDataPipe;
          ShiftFlagPipe;
          doTRes1 <= '0';
          doTCnt1 <= '1';
        elsif (TCnt > "10010") then
          FileSep(make_uint(dNum)) <= (others => '0'); -- clear data RFlags
          SepFlagPipe(0) <= '0';        -- put "no separator" in the pipe
          doTRes1 <= '1';
          doTCnt1 <= '0';
          HoldDataPipe;
        else
          dFlag := '1';                 -- some TDC data
          PassOne <= '1';               -- set  flag (TDC value found in this cycle)
          -- store input data flag and shift the array of flags (to be used for sending data to output)
          FileSep(make_uint(dNum)) <= dFlag & FileSep(make_uint(dNum))(17 downto 1);
          SepFlagPipe(0) <= '0';        -- put "no separator" in the pipe
          ShiftDataPipe;
          ShiftFlagPipe;
          doTRes1 <= '0';
          doTCnt1 <= '1';
        end if;
      else                              -- when no input data available (or Idle):
        HoldDataPipe;
      end if;
    end if;
  end process;

end architecture a0 ; -- of EmptySup

