--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.ReadFHFifo
--! =project    FELIX_MROD
--! @modified   Wed Dec  9 16:21:55 2020
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity ReadFHFifo is
  port (
    Clk         : in     std_logic;
    EnaReadHost : in     std_logic;
    FD          : in     std_logic_vector(31 downto 0);
    FEmpty      : in     std_logic;
    FHDValid    : out    std_logic;
    FHData      : out    std_logic_vector(31 downto 0);
    FREna       : out    std_logic;
    FValid      : in     std_logic;
    MReset      : in     std_logic);
end entity ReadFHFifo;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.ReadFHFifo.a0
--! =project    FELIX_MROD
--! @modified   Wed Dec  9 16:21:55 2020
--!-----------------------------------------------------------------------------


architecture a0 of ReadFHFifo is

  type states is (Idle, CheckF, LastW);
  signal mstate : states;

  signal data  : std_logic_vector(31 downto 0);
  signal QValid : std_logic;
  signal EnRead : std_logic;
  signal Empty  : std_logic;

begin

  Empty   <= FEmpty;
  FREna   <= '1' when (EnRead = '1' and Empty = '0') else '0';
  FHData  <= data;
  FHDValid <= QValid;

  pr1:
  process(Clk, MReset)
  begin
    if (MReset = '1') then
      EnRead <= '0';
      QValid <= '0';
      data   <= (others => '0');
      mstate <= Idle;
    elsif (rising_edge(Clk)) then
      case mstate is
      when Idle =>
        QValid <= '0';
        if (EnaReadHost = '1' and Empty = '0') then
          EnRead <= '1';
          mstate <= CheckF;             -- start allowed if fifo not empty
        else
          EnRead <= '0';                -- wait
          mstate <= Idle;
        end if;
      when CheckF =>
        if (FValid = '1') then          -- if read word valid:
          QValid <= '1';                -- copy valid
          data <= FD;                   -- copy data from fifo
        else
          QValid <= '0';                -- no data
        end if;
        if (Empty = '0') then           -- if fifo not empty:
          EnRead <= '1';                -- continue reading
          mstate <= CheckF;
        else                            -- else fifo empty:
          EnRead <= '0';                -- possibly read last word
          mstate <= LastW;
        end if;
      when LastW =>
        if (FValid = '1') then          -- if last read word valid:
          QValid <= '1';                -- copy valid
          data <= FD;                   -- copy data from Fifo
        else
          QValid <= '0';                -- no data
        end if;
        if (EnaReadHost = '0') then           -- if channel disabled:
          EnRead <= '0';
          mstate <= Idle;               -- abort operation
        elsif (Empty = '0') then        -- elsif fifo not empty:
          EnRead <= '1';                -- continue reading
          mstate <= CheckF;
        else                            -- else fifo empty:
          EnRead <= '0';                -- wait
          mstate <= LastW;
        end if;
      when others =>
        mstate <= Idle;
      end case;
    end if;
  end process;

end architecture a0 ; -- of ReadFHFifo

