--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.Transceiver
--! project     FELIX_MROD
--! modified    Wed Dec 09 16:14:23 2019
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
--use work.FELIX_gbt_package.all;
use work.pcie_package.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity Transceiver is
  generic(
    W_ENDPOINT : integer := 0;
    NUMCH      : integer := 2);
  port (
    EnChan                              : in   std_logic_vector(NUMCH-1 downto 0);
    MReset                              : in   std_logic;
    QX_GTREFCLK_N                       : in   std_logic_vector(4 downto 0);
    QX_GTREFCLK_P                       : in   std_logic_vector(4 downto 0);
    RX_CHxLocked                        : out  std_logic_vector(NUMCH-1 downto 0);
    RX_CHxReset                         : in   std_logic_vector(NUMCH-1 downto 0);
    --RX_FSM_RESET_DONE                   : out  std_logic_vector(NUMCH/4-1 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    RxClk                               : out  std_logic_vector(NUMCH-1 downto 0);
    RxData                              : out  slv32_array(0 to NUMCH-1);
    RxValid                             : out  std_logic_vector(NUMCH-1 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    TRXloopback                         : in   std_logic_vector(NUMCH-1 downto 0);
    TX_CHxLocked                        : in   std_logic_vector(NUMCH-1 downto 0);
    TX_CHxReset                        : in   std_logic_vector(NUMCH-1 downto 0);
    TxClk                               : out  std_logic_vector(NUMCH-1 downto 0);
    TxData                              : in   slv33_array(0 to NUMCH-1);
    TxValid                             : in   std_logic_vector(NUMCH-1 downto 0);
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    --Txprbssel_in                        : in   std_logic_vector(2 downto 0);
    clk50                               : in   std_logic;
    gtrxn_in                            : in   std_logic_vector(NUMCH-1 downto 0);
    gtrxp_in                            : in   std_logic_vector(NUMCH-1 downto 0);
    gttxn_out                           : out  std_logic_vector(NUMCH-1 downto 0);
    gttxp_out                           : out  std_logic_vector(NUMCH-1 downto 0);
    ------------------ 40 MHz system (DRP) clk
    prmap_app_control                       : in  register_map_control_type;
    --register_map_control                : in   register_map_control_type;
    sysclk_in                           : in   std_logic
    );

end entity Transceiver;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.Transceiver.a0
--! project     FELIX_MROD
--! modified    Tue Jun 04 12:14:23 2019
--!-----------------------------------------------------------------------------

architecture a0 of Transceiver is

--COMPONENT MRODtransceiver_core
--  PORT (
--    gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_tx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_tx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_tx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_tx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
--    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
--    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
--    drpclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    gthrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    gthrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    gtrefclk0_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
--    rx8b10ben_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    tx8b10ben_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--    txctrl0_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
--    txctrl1_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
--    txctrl2_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--    gthtxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--    gthtxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--    gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--    rxcdrlock_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--    rxctrl0_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
--    rxctrl1_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
--    rxctrl2_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
--    rxctrl3_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
--    rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
--    txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
--  );
--END COMPONENT;

COMPONENT MRODtransceiver_core
  PORT (
    gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    gtrefclk01_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    qpll1outrefclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    drpclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gthrxn_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gthrxp_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtrefclk0_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    loopback_in : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    rx8b10ben_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    tx8b10ben_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txctrl0_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl1_in : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    txctrl2_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    txusrclk_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txusrclk2_in : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gthtxn_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gthtxp_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxcdrlock_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxctrl0_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxctrl1_out : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    rxctrl2_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    rxctrl3_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    rxoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txoutclk_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END COMPONENT;


COMPONENT vio_0_quad
  PORT (
    clk : IN STD_LOGIC;
    probe_in0 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe_in1 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe_in2 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    probe_in3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_in5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out0 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out1 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out2 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out3 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out4 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out5 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe_out6 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;

COMPONENT ila_gth_quad

PORT (
	clk : IN STD_LOGIC;
	probe0 : IN STD_LOGIC_VECTOR(127 DOWNTO 0); 
	probe1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
	probe2 : IN STD_LOGIC_VECTOR(127 DOWNTO 0); 
	probe3 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
	probe4 : IN STD_LOGIC_VECTOR(63 DOWNTO 0); 
	probe5 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	probe6 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
);
END COMPONENT  ;

signal RESETslv: std_logic_vector (0 downto 0);
signal RX_FSM_RESET_DONE_sA,RX_FSM_RESET_DONE_sB: std_logic_vector(NUMCH/4-1 downto 0);
--signal RXSLIDE_S: std_logic_vector(NUMCH - 1 downto 0);
signal gtrefclk0_i, gtrefclk1_i, gtrefclk2_i : std_logic;
signal loopback_in_s : std_logic_vector(2 downto 0);
signal rxusrclk_sA, rxusrclk_sB : std_logic_vector(NUMCH-1 downto 0);
signal rxoutclk_sA, rxoutclk_sB : std_logic_vector(NUMCH-1 downto 0);
signal txusrclk_sA, txusrclk_sB : std_logic_vector(NUMCH-1 downto 0);
signal txoutclk_sA, txoutclk_sB : std_logic_vector(NUMCH-1 downto 0);
signal rxdata_out_sA, rxdata_out_sB : std_logic_vector((NUMCH*32)-1 downto 0);
signal txdata_in_sA, txdata_in_sB : std_logic_vector((NUMCH*32)-1 downto 0);
signal gtrxp_in_sA, gtrxn_in_sA, gttxp_out_sA, gttxn_out_sA : std_logic_vector(NUMCH-1 downto 0);
signal gtrxp_in_sB, gtrxn_in_sB, gttxp_out_sB, gttxn_out_sB : std_logic_vector(NUMCH-1 downto 0);
signal txctrl2_in_sA, txctrl2_in_sB : std_logic_vector((NUMCH*8)-1 downto 0);
signal rxctrl0_out_sA, rxctrl0_out_sB : std_logic_vector((NUMCH*16)-1 downto 0);
signal rxctrl1_out_sA, rxctrl1_out_sB : std_logic_vector((NUMCH*16)-1 downto 0);
signal rxctrl2_out_sA, rxctrl2_out_sB : std_logic_vector((NUMCH*8)-1 downto 0);
signal rxctrl3_out_sA, rxctrl3_out_sB : std_logic_vector((NUMCH*8)-1 downto 0);

signal rxcdrlock_out_sA, rxcdrlock_out_sB		: std_logic_vector(NUMCH-1 downto 0);

--Monitor signals (connect to VIO)
signal rxpmaresetdone_out_sA, rxpmaresetdone_out_sB     : std_logic_vector(NUMCH-1 downto 0);
signal txpmaresetdone_out_sA, txpmaresetdone_out_sB     : std_logic_vector(NUMCH-1 downto 0);
signal gtpowergood_out_sA, gtpowergood_out_sB  	        : std_logic_vector(NUMCH-1 downto 0);
signal gtwiz_reset_rx_done_out_sA                       : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_rx_done_out_sB                       : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_tx_done_out_sA                       : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_tx_done_out_sB                       : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_rx_cdr_stable_out_sA                 : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_rx_cdr_stable_out_sB                 : std_logic_vector(NUMCH/4-1 downto 0);

--Resets, connected to VIO
--signal gtwiz_reset_rx_datapath_in 			: std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_rx_datapath_in_sA                    : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_rx_datapath_in_sB   	       	        : std_logic_vector(NUMCH/4-1 downto 0);
--signal gtwiz_reset_rx_pll_and_datapath_in 		: std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_rx_pll_and_datapath_in_sA            : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_rx_pll_and_datapath_in_sB            : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_all_in                               : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_all_in_sA                            : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_all_in_sB                            : std_logic_vector(NUMCH/4-1 downto 0);
--signal gtwiz_reset_tx_pll_and_datapath_in 		: std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_tx_pll_and_datapath_in_sA            : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_tx_pll_and_datapath_in_sB            : std_logic_vector(NUMCH/4-1 downto 0);
--signal gtwiz_reset_tx_datapath_in 			: std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_tx_datapath_in_sA                    : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_reset_tx_datapath_in_sB                    : std_logic_vector(NUMCH/4-1 downto 0);
--signal gtwiz_userclk_tx_reset_in 			: std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_userclk_tx_reset_in_sA                     : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_userclk_tx_reset_in_sB                     : std_logic_vector(NUMCH/4-1 downto 0);
--signal gtwiz_userclk_rx_active_in 			: std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_userclk_rx_active_in_sA                    : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_userclk_rx_active_in_sB                    : std_logic_vector(NUMCH/4-1 downto 0);
--signal gtwiz_userclk_tx_active_out 			: std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_userclk_tx_active_out_sA                   : std_logic_vector(NUMCH/4-1 downto 0);
signal gtwiz_userclk_tx_active_out_sB                   : std_logic_vector(NUMCH/4-1 downto 0);

type drpclk_in_type is array (NUMCH/4-1 downto 0) of std_logic_vector(3 downto 0);
signal drpclk_in, gtrefclk0_in_sA, gtrefclk0_in_sB      : drpclk_in_type;
signal cesyncRX_s, clrsyncRX_s                              : std_logic_vector(NUMCH-1 downto 0);
signal cesyncTX_s, clrsyncTX_s                              : std_logic_vector(NUMCH-1 downto 0);

--signal rxdata_out       : slv32_array(0 to NUMCH-1);
signal rxcharisk_out    : slv04_array(0 to NUMCH -1);
signal txdata_in_bufA, txdata_in_bufB        : slv32_array(0 to NUMCH-1);
signal txcharisk_in     : slv04_array(0 to NUMCH-1);
signal RXUSRCLK_OUT     : std_logic_vector(NUMCH-1 downto 0);
signal TXUSRCLK_OUT     : std_logic_vector(NUMCH-1 downto 0);

begin

  loopback_in_s <= prmap_app_control.GTH_LOOPBACK_CONTROL;
  RESETslv <= (others => MReset);

-- generate once per PCIE end point
  genEP_0: if (W_ENDPOINT = 0) generate

-- generate once per channel
    g_CHx_A: for i in 0 to (NUMCH - 1) generate
      RX_CHxLocked(i) <= rxcdrlock_out_sA(i); --'1';

      gttxp_out(i)  <= gttxp_out_sA(i);
      gttxn_out(i)  <= gttxn_out_sA(i);
      gtrxp_in_sA(i) <= gtrxp_in(i);
      gtrxn_in_sA(i) <= gtrxn_in(i); 

      --RX data, ctrl and clk
      RxData(i) <= rxdata_out_sA(i*32+31 downto i*32);
      rxcharisk_out(i) <= rxctrl0_out_sA(i*16+3 downto i*16);
      RxValid(i) <= '1' when (rxcharisk_out(i) = "0000") else '0';
      RxClk(i) <= rxusrclk_sA(i);
      
      -- EP 0: TRX data, ctrl and clk   
      txdata_in_bufA(i) <= TxData(31 downto 0)(i);
      txdata_in_sA(i*32+31 downto i*32) <= txdata_in_bufA(i);
      txcharisk_in(i) <= (others => TxData(i)(32) );
      txctrl2_in_sA(i*8+3 downto i*8) <= txcharisk_in(i);      
      TxClk(i) <= txusrclk_sA(i);
     
      -- Rx and Tx clk buffers
      BUFG_GT_RXinst: BUFG_GT port map(
       O         => rxusrclk_sA(i),
       CE        => cesyncRX_s(i),
       CEMASK    => '1',
       CLR       => clrsyncRX_s(i),
       CLRMASK   => '1',
       DIV       => "000",
       I         => rxoutclk_sA(i)
      );
      
      BUFG_GT_SYNC_RXinst: BUFG_GT_SYNC port map(
       CESYNC     => cesyncRX_s(i),
       CLRSYNC    => clrsyncRX_s(i),
       CLK        => rxoutclk_sA(i),
       CE         => '1',
       CLR        => '0'
      );
       
      BUFG_GT_TXinst: BUFG_GT port map(
       O         => txusrclk_sA(i),
       CE        => cesyncTX_s(i),
       CEMASK    => '1',
       CLR       => clrsyncTX_s(i),
       CLRMASK   => '1',
       DIV       => "000",
       I         => txoutclk_sA(i)
      );
            
      BUFG_GT_SYNC_TXinst: BUFG_GT_SYNC port map(
       CESYNC     => cesyncTX_s(i),
       CLRSYNC    => clrsyncTX_s(i),
       CLK        => txoutclk_sA(i),
       CE         => '1',
       CLR        => '0'
      );        
    end generate g_CHx_A;

--gen XCVR CoreA: if (W_ENDPOINT = 0) generate
g_quadsA: for i in 0 to (NUMCH/4-1) generate -- channel A LOWER SLR generate in multiples of 4 channels

    -- RefClk
    IBUFDS_GTE3_inst0 : IBUFDS_GTE3
    generic map (
    REFCLK_EN_TX_PATH => '0', -- Refer to Transceiver User Guide
    REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
    REFCLK_ICNTL_RX => "00" -- Refer to Transceiver User Guide
    )
    port map (
    O => gtrefclk0_i, -- 1-bit output: Refer to Transceiver User Guide
    ODIV2 => open, -- 1-bit output: Refer to Transceiver User Guide
    CEB => '0', -- 1-bit input: Refer to Transceiver User Guide
    I => Qx_GTREFCLK_P(0), -- 1-bit input: Refer to Transceiver User Guide
    IB => Qx_GTREFCLK_N(0) -- 1-bit input: Refer to Transceiver User Guide
    );

    drpclk_in(i)     <=  (others => sysclk_in);
    gtrefclk0_in_sA(i) <= (others => gtrefclk0_i);
    --RX_FSM_RESET_DONE(i) <= RX_FSM_RESET_DONE_sA(i);

    u0 : MRODtransceiver_core
   PORT MAP (
      gtwiz_userclk_tx_active_in => (others => '1'),
      gtwiz_userclk_rx_active_in => gtwiz_userclk_rx_active_in_sA(i downto i),
      gtwiz_reset_clk_freerun_in(0) => sysclk_in,
      gtwiz_reset_all_in => gtwiz_reset_all_in_sA(i downto i),
      gtwiz_reset_tx_pll_and_datapath_in => gtwiz_reset_tx_pll_and_datapath_in_sA(i downto i) or RESETslv,
      gtwiz_reset_tx_datapath_in => gtwiz_reset_tx_datapath_in_sA(i downto i),
      gtwiz_reset_rx_pll_and_datapath_in => gtwiz_reset_rx_pll_and_datapath_in_sA(i downto i),
      gtwiz_reset_rx_datapath_in => gtwiz_reset_rx_datapath_in_sA(i downto i),
      gtwiz_reset_rx_cdr_stable_out => gtwiz_reset_rx_cdr_stable_out_sA(i downto i),
      gtwiz_reset_tx_done_out => gtwiz_reset_tx_done_out_sA(i downto i),
      gtwiz_reset_rx_done_out => RX_FSM_RESET_DONE_sA(i downto i),
      gtwiz_userdata_tx_in => txdata_in_sA((128*(i+1))-1 downto (128 * (i))),
      gtwiz_userdata_rx_out => rxdata_out_sA((128*(i+1))-1 downto (128 * (i))),
      gtrefclk01_in(0) => gtrefclk0_i,
      qpll1outclk_out => open,
      qpll1outrefclk_out => open,
      drpclk_in => drpclk_in(i),
      gthrxn_in => gtrxn_in_sA,
      gthrxp_in => gtrxp_in_sA,
      gtrefclk0_in => gtrefclk0_in_sA(i),
      loopback_in => (loopback_in_s & loopback_in_s & loopback_in_s & loopback_in_s),-- "1111",
      rx8b10ben_in => (others => '1'),
      rxusrclk_in => rxusrclk_sA(i*4+3 downto i*4),
      rxusrclk2_in => rxusrclk_sA(i*4+3 downto i*4),
      tx8b10ben_in => (others => '1'),
      txctrl0_in => (others => '0'),
      txctrl1_in => (others => '0'),
      txctrl2_in => txctrl2_in_sA((32*(i+1))-1 downto 32*(i)),
      txusrclk_in => txusrclk_sA(i*4+3 downto i*4),
      txusrclk2_in => txusrclk_sA(i*4+3 downto i*4),
      gthtxn_out => gttxn_out_sA,
      gthtxp_out => gttxp_out_sA,
      gtpowergood_out => gtpowergood_out_sA,
      rxcdrlock_out => rxcdrlock_out_sA((4*(i+1))-1 downto 4*(i)),
      rxctrl0_out => rxctrl0_out_sA,
      rxctrl1_out => rxctrl1_out_sA,
      rxctrl2_out => rxctrl2_out_sA,
      rxctrl3_out => rxctrl3_out_sA,
      rxoutclk_out => rxoutclk_sA(i*4+3 downto i*4),
      rxpmaresetdone_out => rxpmaresetdone_out_sA((4*(i+1))-1 downto 4*(i)),
      txoutclk_out => txoutclk_sA(i*4+3 downto i*4),
      txpmaresetdone_out => txpmaresetdone_out_sA((4*(i+1))-1 downto 4*(i)),
      txprgdivresetdone_out => open
      );

    transceiver_vioA: vio_0_quad
      PORT MAP (
        clk => sysclk_in,
        probe_in0 => rxpmaresetdone_out_sA,--4
        probe_in1 => txpmaresetdone_out_sA,--4
        probe_in2 => gtpowergood_out_sA, --4 
        probe_in3 => RX_FSM_RESET_DONE_sA, --1
        probe_in4 => gtwiz_reset_tx_done_out_sA, --1
        probe_in5 => gtwiz_reset_rx_cdr_stable_out_sA,--1
        probe_out0 => gtwiz_reset_rx_datapath_in_sA,--1
        probe_out1 => gtwiz_reset_rx_pll_and_datapath_in_sA,--1
        probe_out2 => gtwiz_reset_all_in_sA,--1
        probe_out3 => gtwiz_reset_tx_pll_and_datapath_in_sA,--1
        probe_out4 => gtwiz_reset_tx_datapath_in_sA,--1
        probe_out5 => gtwiz_userclk_tx_reset_in_sA,--1
        probe_out6 => gtwiz_userclk_rx_active_in_sA--1 Set to 1 in VIO.
      );
        
    transceiver_ilaA : ila_gth_quad
      PORT MAP (
        clk => rxusrclk_sA(0),
        probe0 => txdata_in_sA,
        probe1 => txctrl2_in_sA,
        probe2 => rxdata_out_sA,
        probe3 => rxctrl0_out_sA,
        probe4 => rxctrl1_out_sA,
        probe5 => rxctrl2_out_sA,
        probe6 => rxctrl3_out_sA
      );
 
    end generate g_quadsA; --A channel
  end generate genEP_0;


  genEP_1: if (W_ENDPOINT = 1) generate

-- generate once per channel
    g_CHx_B: for i in 0 to (NUMCH - 1) generate
      RX_CHxLocked(i) <= rxcdrlock_out_sB(i); --'1';
      
      gttxp_out(i)  <= gttxp_out_sB(i);
      gttxn_out(i)  <= gttxn_out_sB(i);
      gtrxp_in_sB(i) <= gtrxp_in(i);
      gtrxn_in_sB(i) <= gtrxn_in(i); 

  --RX data, ctrl and clk
  RxData(i) <= rxdata_out_sB(i*32+31 downto i*32);
  rxcharisk_out(i) <= rxctrl0_out_sB(i*16+3 downto i*16);
  RxValid(i) <= '1' when (rxcharisk_out(i) = "0000") else '0';
  
  RxClk(i) <= rxusrclk_sB(i);
 
  --EP 1: TRX data, ctrl and clk  
  txdata_in_bufB(i) <= TxData(31 downto 0)(i);
  txdata_in_sB(i*32+31 downto i*32) <= txdata_in_bufB(i);      
  txcharisk_in(i) <= (others => TxData(i)(32) );
  txctrl2_in_sB(i*8+3 downto i*8) <= txcharisk_in(i); 
  TxClk(i) <= txusrclk_sB(i);
 
  -- Rx and Tx clk buffers
      BUFG_GT_RXinst: BUFG_GT port map(
        O         => rxusrclk_sB(i),
        CE         => cesyncRX_s(i),
        CEMASK     => '1',
        CLR         => clrsyncRX_s(i),
        CLRMASK   => '1',
        DIV         => "000",
        I         => rxoutclk_sB(i)
      );
      
      BUFG_GT_SYNC_RXinst: BUFG_GT_SYNC port map(
       CESYNC     => cesyncRX_s(i),
       CLRSYNC    => clrsyncRX_s(i),
       CLK        => rxoutclk_sB(i),
       CE         => '1',
       CLR        => '0'
       );
       
      BUFG_GT_TXinst: BUFG_GT port map(
         O         => txusrclk_sB(i),
         CE         => cesyncTX_s(i),
         CEMASK     => '1',
         CLR         => clrsyncTX_s(i),
         CLRMASK   => '1',
         DIV         => "000",
         I         => txoutclk_sB(i)
       );
       
       BUFG_GT_SYNC_TXinst: BUFG_GT_SYNC port map(
        CESYNC     => cesyncTX_s(i),
        CLRSYNC    => clrsyncTX_s(i),
        CLK        => txoutclk_sB(i),
        CE         => '1',
        CLR        => '0'
        );       
    end generate g_CHx_B;


--gen XCVR_CoreB: if (W_ENDPOINT = 1) generate
g_quadsB: for i in 0 to (NUMCH/4-1) generate -- channel B UPPER SLR: generate in multiples of 4 channels

    IBUFDS_GTE3_inst1 : IBUFDS_GTE3
    generic map (
    REFCLK_EN_TX_PATH => '0', -- Refer to Transceiver User Guide
    REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
    REFCLK_ICNTL_RX => "00" -- Refer to Transceiver User Guide
    )
    port map (
    O => gtrefclk1_i, -- 1-bit output: Refer to Transceiver User Guide
    ODIV2 => open, -- 1-bit output: Refer to Transceiver User Guide
    CEB => '0', -- 1-bit input: Refer to Transceiver User Guide
    I => Qx_GTREFCLK_P(1), -- 1-bit input: Refer to Transceiver User Guide
    IB => Qx_GTREFCLK_N(1) -- 1-bit input: Refer to Transceiver User Guide
    );
    
    drpclk_in(i)     <=  (others => sysclk_in);
    gtrefclk0_in_sB(i) <= (others => gtrefclk1_i);
    --RX_FSM_RESET_DONE(i) <= RX_FSM_RESET_DONE_sB(i);
      
    u0 : MRODtransceiver_core
   PORT MAP (
      --gtwiz_userclk_tx_reset_in => gtwiz_userclk_tx_reset_in_sB(i downto i),
      --gtwiz_userclk_tx_srcclk_out => open,
      --gtwiz_userclk_tx_usrclk_out => txusrclk_sB(i*4+3 downto i*4),
      --gtwiz_userclk_tx_usrclk2_out => open,
      --gtwiz_userclk_tx_active_out => gtwiz_userclk_tx_active_out_sB(i downto i),
      gtwiz_userclk_tx_active_in => (others => '1'),
      gtwiz_userclk_rx_active_in => gtwiz_userclk_rx_active_in_sB(i downto i),
      gtwiz_reset_clk_freerun_in(0) => sysclk_in,
      gtwiz_reset_all_in => gtwiz_reset_all_in_sB(i downto i),
      gtwiz_reset_tx_pll_and_datapath_in => gtwiz_reset_tx_pll_and_datapath_in_sB(i downto i) or RESETslv,
      gtwiz_reset_tx_datapath_in => gtwiz_reset_tx_datapath_in_sB(i downto i),
      gtwiz_reset_rx_pll_and_datapath_in => gtwiz_reset_rx_pll_and_datapath_in_sB(i downto i),
      gtwiz_reset_rx_datapath_in => gtwiz_reset_rx_datapath_in_sB(i downto i),
      gtwiz_reset_rx_cdr_stable_out => gtwiz_reset_rx_cdr_stable_out_sB(i downto i),
      gtwiz_reset_tx_done_out => gtwiz_reset_tx_done_out_sB(i downto i),
      gtwiz_reset_rx_done_out => RX_FSM_RESET_DONE_sB(i downto i),
      gtwiz_userdata_tx_in => txdata_in_sB((128*(i+1))-1 downto (128 * (i))),
      gtwiz_userdata_rx_out => rxdata_out_sB((128*(i+1))-1 downto (128 * (i))),
      gtrefclk01_in(0) => gtrefclk1_i,
      qpll1outclk_out => open,
      qpll1outrefclk_out => open,
      drpclk_in => drpclk_in(i),
      gthrxn_in => gtrxn_in_sB,
      gthrxp_in => gtrxp_in_sB,
      gtrefclk0_in => gtrefclk0_in_sB(i),
      loopback_in => (loopback_in_s & loopback_in_s & loopback_in_s & loopback_in_s),-- "010010010010",
      rx8b10ben_in => (others => '1'),
      rxusrclk_in => rxusrclk_sB(i*4+3 downto i*4),
      rxusrclk2_in => rxusrclk_sB(i*4+3 downto i*4),
      tx8b10ben_in => (others => '1'),
      txctrl0_in => (others => '0'),
      txctrl1_in => (others => '0'),
      txctrl2_in => txctrl2_in_sB((32*(i+1))-1 downto 32*(i)),
      txusrclk_in => txusrclk_sB(i*4+3 downto i*4),
      txusrclk2_in => txusrclk_sB(i*4+3 downto i*4),
      gthtxn_out => gttxn_out_sB,
      gthtxp_out => gttxp_out_sB,
      gtpowergood_out => gtpowergood_out_sB,
      rxcdrlock_out => rxcdrlock_out_sB((4*(i+1))-1 downto 4*(i)),
      rxctrl0_out => rxctrl0_out_sB,
      rxctrl1_out => rxctrl1_out_sB,
      rxctrl2_out => rxctrl2_out_sB,
      rxctrl3_out => rxctrl3_out_sB,
      rxoutclk_out => rxoutclk_sB(i*4+3 downto i*4),
      rxpmaresetdone_out => rxpmaresetdone_out_sB((4*(i+1))-1 downto 4*(i)),
      txoutclk_out => txoutclk_sB(i*4+3 downto i*4),
      txpmaresetdone_out => txpmaresetdone_out_sB((4*(i+1))-1 downto 4*(i)),
      txprgdivresetdone_out => open
      );

    transceiver_vioB: vio_0_quad
        PORT MAP (
          clk => sysclk_in,
          probe_in0 => rxpmaresetdone_out_sB,--4
          probe_in1 => txpmaresetdone_out_sB,--4
          probe_in2 => gtpowergood_out_sB, --4 
          probe_in3 => RX_FSM_RESET_DONE_sB, --1
          probe_in4 => gtwiz_reset_tx_done_out_sB, --1
          probe_in5 => gtwiz_reset_rx_cdr_stable_out_sB,--1
          probe_out0 => gtwiz_reset_rx_datapath_in_sB,--1
          probe_out1 => gtwiz_reset_rx_pll_and_datapath_in_sB,--1
          probe_out2 => gtwiz_reset_all_in_sB,--1
          probe_out3 => gtwiz_reset_tx_pll_and_datapath_in_sB,--1
          probe_out4 => gtwiz_reset_tx_datapath_in_sB,--1
          probe_out5 => gtwiz_userclk_tx_reset_in_sB,--1
          probe_out6 => gtwiz_userclk_rx_active_in_sB--1
        );

    transceiver_ilaB : ila_gth_quad
      PORT MAP (
        clk => rxusrclk_sB(0),
        probe0 => txdata_in_sB,
        probe1 => txctrl2_in_sB,
        probe2 => rxdata_out_sB,
        probe3 => rxctrl0_out_sB,
        probe4 => rxctrl1_out_sB,
        probe5 => rxctrl2_out_sB,
        probe6 => rxctrl3_out_sB
      );

    end generate g_quadsB; -- B-CHANNEL
  end generate genEP_1;

end architecture a0 ; -- of Transceiver

