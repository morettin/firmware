--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.FSM_Align.vhd
--! project     FELIX_MROD
--! @modified   Fri Apr 30 17:23:45 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.pcie_package.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity FSM_Align is
  port (
    clk50_ila     : in std_logic;
    rxusrclk      : in std_logic;
    TCVRall_rst   : in std_logic_vector(0 downto 0);
    rxctrl0_out   : in std_logic_vector(15 downto 0);
    rxctrl1_out   : in std_logic_vector(15 downto 0);
    rxctrl2_out   : in std_logic_vector(7 downto 0);
    rxctrl3_out   : in std_logic_vector(7 downto 0);
    enManslide    : in std_logic;
    SlideWait     : in std_logic_vector(7 downto 0);
    FrameSize     : in std_logic_vector(7 downto 0);
    rxslide_VIO   : in std_logic_vector(0 downto 0);
    rxcommadet_out: in std_logic_vector(0 downto 0);
    RX_CHxAlignBsy: out std_logic_vector(0 downto 0);
    RX_CHxRecData : out std_logic_vector(0 downto 0);
    RX_CHxRecIdles: out std_logic_vector(0 downto 0);
    rxslide_toGTH : out std_logic_vector(0 downto 0)
  );
end FSM_Align;

-- INFO --
-- RXCTRL0 indicates a K char received
-- RXCTRL1 indicates disparity error.
-- RXCTRL2 indicates comma char received
-- RXCTRL3 represents that an invalid 8B/10B character error occurred

architecture Behavioral of FSM_Align is

  --Types and signals for FSM
  type states is (INIT, CHKBIT, CHKFRAME, MANRXSLIDE, CNT_SLIDE, TRIG_SLIDE
                , WAIT32, ALIGNED, RECIDLES);
  signal TxcvrState, TxcvrStateOut : states;

  signal rxslide_cnt                                    : unsigned(7 downto 0) := (others => '0');
  signal alignCnt                                       : unsigned(7 downto 0) := (others => '0');
  signal ClkCnt                                         : unsigned(7 downto 0) := (others => '0');
  signal IdleCnt                                        : unsigned(7 downto 0) := (others => '0');
  signal rxslide_in_d0, rxslide_in_d1, rxslide_in_d2    : std_logic;
  signal rxslide_VIO_d0, rxslide_VIO_d1                 : std_logic;
  signal data_activity, idle_activity                   : std_logic;
  signal rxcommadet_out_d0                              : std_logic_vector(0 downto 0);

  --Uncomment and add them to debug core after synthesis
  --attribute mark_debug : string;
  --attribute keep : string;
  --attribute mark_debug of TxcvrState     : signal is "true";
  --attribute mark_debug of rxslide_toGTH  : signal is "true";
  --attribute mark_debug of rxslide_in_d0  : signal is "true";
  --attribute mark_debug of rxslide_in_d1  : signal is "true";

begin

  pr0:
  process(rxusrclk, TCVRall_rst )
  begin
    if (TCVRall_rst = "1") then
      TxcvrState <= INIT;
      rxslide_in_d0 <= '0';
      rxslide_in_d1 <= '0';
      rxslide_in_d2 <= '0';
      rxslide_VIO_d0 <= '0';
      rxslide_VIO_d1 <= '0';
      rxcommadet_out_d0 <= "0";
      data_activity <= '0';
      idle_activity <= '0';
      IdleCnt <=  (others => '0');
      ClkCnt <= (others => '0');
      alignCnt <= (others => '0');
      rxslide_cnt <= (others => '0');
      RX_CHxAlignBsy <= "0";
      RX_CHxRecData  <= "0";
      RX_CHxRecIdles <= "0";
      rxslide_toGTH  <= "0";
    elsif (rising_edge(rxusrclk)) then
      TxcvrStateOut <= TxcvrState;
      --
      case TxcvrState is
      when INIT =>
        rxslide_in_d0 <= '0';
        rxslide_in_d1 <= '0';
        rxslide_in_d2 <= '0';
        rxslide_VIO_d0 <= '0';
        rxslide_VIO_d1 <= '0';
        rxcommadet_out_d0 <= "0";
        data_activity <= '0';
        idle_activity <= '0';
        IdleCnt <=  unsigned(FrameSize);
        ClkCnt <= (others => '0');
        alignCnt <= (others => '0');
        rxslide_cnt <= (others => '0');
        -- next state logic
        if (EnManSlide = '1') then
          TxcvrState <= MANRXSLIDE;
        else
          TxcvrState <= CHKBIT;
        end if;
        -- outputs
        RX_CHxAlignBsy <= "0";
        RX_CHxRecData  <= "0";
        RX_CHxRecIdles <= "0";
        rxslide_toGTH  <= "0";
      when CHKBIT =>
        -- check for disparity errors or invalid 8b/10b chars
        rxslide_cnt <= to_unsigned(1,rxslide_cnt'length); --shift frame with 1 bit
        alignCnt <= unsigned(FrameSize);
        -- next state logic
        if ( (rxctrl1_out = x"0000") and (rxctrl3_out = x"F0") ) then
          TxcvrState <= CHKFRAME;
        else
          TxcvrState <= TRIG_SLIDE;
        end if;
        -- outputs
        rxslide_in_d0 <= '0';
        rxslide_in_d1 <= '0';
        rxslide_in_d2 <= '0';
        RX_CHxAlignBsy <= "1";
        RX_CHxRecData  <= "0";
        RX_CHxRecIdles <= "0";
        rxslide_toGTH  <= "0";
      when CHKFRAME =>
        -- rxctrl has correct value but must be valid for a complete frame of 20 words
        -- Tranceiver is locked to incoming data without disparity errors but "waiting"
        -- for an idle word to check alignment w.r.t. two idle chars in one 40bit word.
        if (alignCnt > x"00") then
          alignCnt <= alignCnt - 1;
        else
          alignCnt <= alignCnt;
        end if;
        rxcommadet_out_d0 <= rxcommadet_out;
        if ( (rxcommadet_out = "0") and (rxcommadet_out_d0 = "1") ) then
          data_activity <= '1';
        else
          data_activity <= '0';
        end if;
        -- next state logic
        if ( (alignCnt = x"00") and (data_activity = '1') ) then
          if (rxctrl0_out = x"0005") then --(MROD) Idle char detected
            TxcvrState <= RECIDLES;
          elsif ((rxctrl0_out = x"0000") and (rxctrl1_out = x"0000")
             and (rxctrl3_out = x"F0")) then
            TxcvrState <= ALIGNED;
          end if;
        elsif ( ((rxctrl0_out = x"0000") or  (rxctrl0_out = x"0005"))
            and ((rxctrl1_out = x"0000") and (rxctrl3_out = x"F0")) ) then
          TxcvrState <= CHKFRAME;
        else --alignment not correct, "restart" with a new trig slide.
          TxcvrState <= TRIG_SLIDE;
        end if;
        -- outputs
        rxslide_in_d0 <= '0';
        rxslide_in_d1 <= '0';
        rxslide_in_d2 <= '0';
        RX_CHxAlignBsy <= "1";
        RX_CHxRecData  <= "0";
        RX_CHxRecIdles <= "0";
        rxslide_toGTH  <= "0";
      when ALIGNED =>
        rxslide_VIO_d0 <= '0';
        rxslide_VIO_d1 <= '0';
        if (rxctrl0_out = x"0005") then --(MROD) Idle char detected
          if (IdleCnt > x"00") then
            IdleCnt <= IdleCnt - 1;
          else
            IdleCnt <= IdleCnt;
          end if;
        else                            -- data detected
          IdleCnt <=  unsigned(FrameSize);
        end if;
        -- next state logic
        if (EnManSlide = '1') then
          TxcvrState <= MANRXSLIDE;
        elsif ((rxctrl0_out = x"0005") and (IdleCnt = x"00")) then
          -- Complete frame with only Idle chars detected
          TxcvrState <= RECIDLES;
        elsif ( ((rxctrl0_out = x"0000") or  (rxctrl0_out = x"0005"))
            and  (rxctrl1_out = x"0000") and (rxctrl3_out = x"F0")) then
          -- Correct data detected (or idle char separator between frames)
          TxcvrState <= ALIGNED;
        else
          -- something went wrong, start over
          TxcvrState <= INIT;
        end if;
        alignCnt <= unsigned(FrameSize);
        rxslide_cnt <= to_unsigned(16, rxslide_cnt'length); --shift 16
        -- outputs
        RX_CHxAlignBsy <= "0";
        RX_CHxRecData  <= "1";
        RX_CHxRecIdles <= "0";
        rxslide_toGTH  <= "0";
      when RECIDLES =>
        if (rxctrl0_out = x"0005") then
          TxcvrState <= RECIDLES;
        else
          TxcvrState <= ALIGNED;
        end if;
        -- outputs
        RX_CHxAlignBsy <= "0";
        RX_CHxRecData  <= "1";
        RX_CHxRecIdles <= "1";
        rxslide_toGTH  <= "0";
      when TRIG_SLIDE =>
        ClkCnt <= unsigned(SlideWait);
        rxslide_in_d2 <= rxslide_in_d1;
        rxslide_in_d1 <= rxslide_in_d0;
        rxslide_in_d0 <= '1';
        if ( rxslide_in_d2 = '1') then
          TxcvrState <= CNT_SLIDE;
          rxslide_in_d0 <= '0';
        else
          TxcvrState <= TRIG_SLIDE;
        end if;
         -- outputs
        RX_CHxAlignBsy <= "1";
        RX_CHxRecData  <= "0";
        RX_CHxRecIdles <= "0";
        rxslide_toGTH  <= "1";
      when CNT_SLIDE =>
        if (rxslide_cnt > x"00") then
          rxslide_cnt <= rxslide_cnt - 1;
        else
          rxslide_cnt <= rxslide_cnt;
        end if;
        -- next state logic
        TxcvrState <= WAIT32;
        -- outputs
        RX_CHxAlignBsy <= "1";
        RX_CHxRecData  <= "0";
        RX_CHxRecIdles <= "0";
        rxslide_toGTH  <= "0";
      when WAIT32 =>
        if (ClkCnt > x"00") then
          ClkCnt <= ClkCnt - 1;
        else
          ClkCnt <= ClkCnt;
        end if;
        rxslide_in_d0 <= '0';
        rxslide_in_d1 <= '0';
        rxslide_in_d2 <= '0';
        if (ClkCnt = x"00") then
          if (rxslide_cnt = x"00") then
            if (EnManSlide = '1') then
              TxcvrState <= MANRXSLIDE;
            else
              TxcvrState <= CHKBIT;
            end if;
          else
            TxcvrState <= TRIG_SLIDE;
          end if;
        else --ClkCnt/= 0
          TxcvrState <= WAIT32;
        end if;
        -- outputs
        RX_CHxAlignBsy <= "1";
        RX_CHxRecData  <= "0";
        RX_CHxRecIdles <= "0";
        rxslide_toGTH  <= "0";
      when MANRXSLIDE =>
        alignCnt <= unsigned(FrameSize);
        rxslide_cnt <= to_unsigned(1, rxslide_cnt'length); --shift 1
        rxslide_VIO_d0 <= rxslide_VIO(0);
        rxslide_VIO_d1 <= rxslide_VIO_d0;
        if (rxslide_VIO_d0 = '1' and rxslide_VIO_d1 = '0') then
          TxcvrState <= TRIG_SLIDE;
        else
          TxcvrState <= MANRXSLIDE;
        end if;
        RX_CHxAlignBsy <= "1";
        RX_CHxRecData  <= "0";
        RX_CHxRecIdles <= "0";
        rxslide_toGTH  <= "0";
      when others =>
        TxcvrState <= INIT;
      end case;
    end if;
  end process;

end Behavioral;

