--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.Busy_TTC
--! =project    FELIX_MROD
--! @modified   Fri Dec 18 16:33:59 2020
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity Busy_TTC is
  generic(
    GBT_NUM : integer := 4;
    NUMCH   : integer := 2);
  port (
    BUSY_INTERRUPT    : out    std_logic;
    CDRlocked         : out    std_logic;
    CLK_TTC_N         : in     std_logic;
    CLK_TTC_P         : in     std_logic;
    CR0BusyOut        : in     std_logic_vector(NUMCH-1 downto 0);
    CR0_FIFO_Busy     : in     std_logic;
    CR1BusyOut        : in     std_logic_vector(NUMCH-1 downto 0);
    CR1_FIFO_Busy     : in     std_logic;
    DATA_TTC_N        : in     std_logic;
    DATA_TTC_P        : in     std_logic;
    LOL_ADN           : in     std_logic;
    LOS_ADN           : in     std_logic;
    MasterBusy        : out    std_logic;
    TTC_ToHost_Data   : out    TTC_ToHost_data_type;
    TTC_out           : out    std_logic_vector(15 downto 0);
    clk40             : in     std_logic;
    clk_ttc_40_s      : out    std_logic;
    pcie0_DMA_Busy    : in     std_logic;
    pcie1_DMA_Busy    : in     std_logic;
    prmap_40_control  : in     register_map_control_type;
    prmap_ttc_monitor : out    register_map_ttc_monitor_type;
    rst_soft_40       : in     std_logic;
    sys_reset_n       : in     std_logic);
end entity Busy_TTC;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.Busy_TTC.a0
--! =project    FELIX_MROD
--! @modified   Fri Dec 18 16:33:59 2020
--!-----------------------------------------------------------------------------


architecture a0 of Busy_TTC is

  component ttc_wrapper
    port (
      CLK_TTC_P                 : in     std_logic;
      CLK_TTC_N                 : in     std_logic;
      DATA_TTC_P                : in     std_logic;
      DATA_TTC_N                : in     std_logic;
      RESET_N                   : in     std_logic;
      TTC_out                   : out    std_logic_vector(15 downto 0);
      clk_adn_160               : out    std_logic;
      clk_ttc_40                : out    std_logic;
      clk40                     : in     std_logic;
      BUSY                      : out    std_logic;
      cdrlocked_out             : out    std_logic;
      TTC_ToHost_Data_out       : out    TTC_ToHost_data_type;
      register_map_control      : in     register_map_control_type;
      LOL_ADN                   : in     std_logic;
      LOS_ADN                   : in     std_logic;
      register_map_ttc_monitor  : out    register_map_ttc_monitor_type;
      TTC_BUSY_mon_array        : in     busyOut_array_type(23 downto 0);
      BUSY_IN                   : in     std_logic);
  end component ttc_wrapper;

  component ttc_busy
    generic(
      GBT_NUM : integer := 4);
    port (
      mclk                      : in     std_logic;
      mrst                      : in     std_logic;
      BUSY_REQUESTs             : in     busyOut_array_type(0 to (GBT_NUM-1));
      ANY_BUSY_REQ_OUT          : out    std_logic;
      register_map_control      : in     register_map_control_type;
      TTC_BUSY_mon_array        : out    busyOut_array_type(GBT_NUM-1 downto 0);
      DMA_BUSY_in               : in     std_logic;
      FIFO_BUSY_in              : in     std_logic;
      BUSY_INTERRUPT_out        : out    std_logic);
    end component ttc_busy;

  signal BUSY_REQUESTs          : busyOut_array_type(0 to 23);          --GBT_NUM-1
  signal ttc_TTC_BUSY_mon_array : busyOut_array_type(23 downto 0);      --GBT_NUM-1
  signal TTC_BUSY_mon_array     : busyOut_array_type(23 downto 0);      --GBT_NUM-1
  signal DMA_BUSY_in            : std_logic;
  signal FIFO_BUSY_in           : std_logic;
  signal BUSY_OUT_s             : std_logic;

begin

  -- DMA to host busy signal for TTC
  DMA_BUSY_in  <= '1' when (pcie0_DMA_Busy = '1' or pcie1_DMA_Busy = '1') else '0';
  -- Fifo to host busy signal for TTC
  FIFO_BUSY_in <= '1' when (CR0_FIFO_Busy = '1' or CR1_FIFO_Busy = '1') else '0';

  g0: for i in 0 to NUMCH-1 generate                    --GBT_NUM/2-1
      BUSY_REQUESTs(i)(0)<= CR0BusyOut(i);              --
      BUSY_REQUESTs(i)(1)<= CR1BusyOut(i);              --
      BUSY_REQUESTs(i)(56 downto 2) <= (others => '0');
  end generate g0;

  g1: for i in NUMCH to 23 generate                     --GBT_NUM/2 to GBT_NUM-1
      g2: if (i < 24) generate
          BUSY_REQUESTs(i)(0)<= '0';                    -- unused CR0BusyOut channels
          BUSY_REQUESTs(i)(1)<= '0';                    -- unused CR1BusyOut channels
          BUSY_REQUESTs(i)(56 downto 2) <= (others => '0');
      end generate g2;
  end generate g1;

  g3: for i in 0 to 23 generate                         --GBT_NUM-1
      ttc_TTC_BUSY_mon_array(i) <= TTC_BUSY_mon_array(i);
  end generate g3;

  MasterBusy <= BUSY_OUT_s;       -- any busy request from entity MasterBusy

  u2: ttc_wrapper
    port map(
      CLK_TTC_P                => CLK_TTC_P,
      CLK_TTC_N                => CLK_TTC_N,
      DATA_TTC_P               => DATA_TTC_P,
      DATA_TTC_N               => DATA_TTC_N,
      RESET_N                  => sys_reset_n,
      TTC_out                  => TTC_out,
      clk_adn_160              => open,
      clk_ttc_40               => clk_ttc_40_s,
      clk40                    => clk40,
      BUSY                     => open,
      cdrlocked_out            => CDRlocked,
      TTC_ToHost_Data_out      => TTC_ToHost_Data,
      register_map_control     => prmap_40_control,     -- from pcie0
      LOL_ADN                  => LOL_ADN,
      LOS_ADN                  => LOS_ADN,
      register_map_ttc_monitor => prmap_ttc_monitor,
      TTC_BUSY_mon_array       => ttc_TTC_BUSY_mon_array,
      BUSY_IN                  => BUSY_OUT_s);

  u3: ttc_busy
    generic map(
      GBT_NUM => 24)        --
    port map(
      mclk                  => clk40,
      mrst                  => rst_soft_40,
      BUSY_REQUESTs         => BUSY_REQUESTs,
      ANY_BUSY_REQ_OUT      => BUSY_OUT_s,
      register_map_control  => prmap_40_control,
      TTC_BUSY_mon_array    => TTC_BUSY_mon_array,
      DMA_BUSY_in           => DMA_BUSY_in,
      FIFO_BUSY_in          => FIFO_BUSY_in,
      BUSY_INTERRUPT_out    => BUSY_INTERRUPT);

end architecture a0 ; -- of Busy_TTC

