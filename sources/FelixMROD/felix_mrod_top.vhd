--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Frans Schreuder
--!               Thei Wijnen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.felix_mrod_top
--! =project    FELIX_MROD
--! @modified   Sat May 15 11:00:40 2021
--!-----------------------------------------------------------------------------

library ieee, work;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.felix_mrod_package.all;
    use work.centralRouter_package.all;
    use work.FELIX_gbt_package.all;
    use work.pcie_package.all;

entity felix_mrod_top is
    generic(
        NUMBER_OF_INTERRUPTS            : integer := 8;
        NUMBER_OF_DESCRIPTORS           : integer := 2;
        OPTO_TRX                        : integer := 4;
        APP_CLK_FREQ                    : integer := 200;
        GBT_NUM                         : integer := 4; -- number of GBT channels
        NUMCH                           : integer := 2;
        MROD_GENERATE_REGS              : boolean := true;
        CARD_TYPE                       : integer := 712;
        USE_BACKUP_CLK                  : boolean := true; -- true to use 100/200 mhz board crystal, false to use TTC clock
        DEBUG_MODE                      : boolean := false; -- true debug ON
        GENERATE_GBT                    : boolean := false;
        GBT_MAPPING                     : integer := 0; -- GBT mapping: 0 NORMAL CXP1 -> GBT1-12 | 1 ALTERNATE CXP1 -> GBT 1-4,9-12,17-20
        GENERATE_XOFF                   : boolean := true; -- FromHost Xoff transmission enabled on Full mode busy
        GTHREFCLK_SEL                   : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
        STATIC_CENTRALROUTER            : boolean := true; -- removes update process from central router register map, only initial constant values are used
        PLL_SEL                         : std_logic := '1'; -- 0: CPLL, 1: QPLL
        AUTOMATIC_CLOCK_SWITCH          : boolean := true;
        toHostTimeoutBitn               : integer := 10;
        crInternalLoopbackMode          : boolean := false;
        serialConfig_mode               : boolean := false;
        useToFrontendGBTdataEmulator    : boolean := false;
        useToHostGBTdataEmulator        : boolean := false;
        TTC_test_mode                   : boolean := false;
        CREnableFromHost                : boolean := true;
        ENDPOINTS                       : integer := 2;
        FIRMWARE_MODE                   : integer := 1;
        BUILD_DATETIME                  : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GIT_HASH                        : std_logic_vector(159 downto 0) := x"0000000000000000000000000000000000000000";
        GIT_TAG                         : std_logic_vector(127 downto 0) := x"00000000000000000000000000000000";
        GIT_COMMIT_NUMBER               : integer := 0;
        COMMIT_DATETIME                 : std_logic_vector(39 downto 0) := x"0000FE71CE";
        includeDirectMode               : boolean := false;
        USE_Si5324_RefCLK               : boolean := false;
        generateTTCemu                  : boolean := false;
        generate_IC_EC_TTC_only         : boolean := false;
        GENERATE_FEI4B                  : boolean := false;
        GENERATE_TRUNCATION_MECHANISM   : boolean := false;
        GENERATE_FM_WRAP                : boolean := false;
        BLOCKSIZE                       : integer := 1024;
        CHUNK_TRAILER_32B               : boolean := false;
        SUPER_CHUNK_FACTOR              : integer := 1;
        EnableFrHo_Egroup0Eproc2_HDLC   : boolean := false;
        EnableFrHo_Egroup0Eproc2_8b10b  : boolean := false;
        EnableFrHo_Egroup0Eproc4_8b10b  : boolean := false;
        EnableFrHo_Egroup0Eproc8_8b10b  : boolean := false;
        EnableFrHo_Egroup1Eproc2_HDLC   : boolean := false;
        EnableFrHo_Egroup1Eproc2_8b10b  : boolean := false;
        EnableFrHo_Egroup1Eproc4_8b10b  : boolean := false;
        EnableFrHo_Egroup1Eproc8_8b10b  : boolean := false;
        EnableFrHo_Egroup2Eproc2_HDLC   : boolean := false;
        EnableFrHo_Egroup2Eproc2_8b10b  : boolean := false;
        EnableFrHo_Egroup2Eproc4_8b10b  : boolean := false;
        EnableFrHo_Egroup2Eproc8_8b10b  : boolean := false;
        EnableFrHo_Egroup3Eproc2_HDLC   : boolean := false;
        EnableFrHo_Egroup3Eproc2_8b10b  : boolean := false;
        EnableFrHo_Egroup3Eproc4_8b10b  : boolean := false;
        EnableFrHo_Egroup3Eproc8_8b10b  : boolean := false;
        EnableFrHo_Egroup4Eproc2_HDLC   : boolean := false;
        EnableFrHo_Egroup4Eproc2_8b10b  : boolean := false;
        EnableFrHo_Egroup4Eproc4_8b10b  : boolean := false;
        EnableFrHo_Egroup4Eproc8_8b10b  : boolean := false;
        EnableToHo_Egroup0Eproc2_HDLC   : boolean := false;
        EnableToHo_Egroup0Eproc2_8b10b  : boolean := false;
        EnableToHo_Egroup0Eproc4_8b10b  : boolean := false;
        EnableToHo_Egroup0Eproc8_8b10b  : boolean := false;
        EnableToHo_Egroup0Eproc16_8b10b : boolean := false;
        EnableToHo_Egroup1Eproc2_HDLC   : boolean := false;
        EnableToHo_Egroup1Eproc2_8b10b  : boolean := false;
        EnableToHo_Egroup1Eproc4_8b10b  : boolean := false;
        EnableToHo_Egroup1Eproc8_8b10b  : boolean := false;
        EnableToHo_Egroup1Eproc16_8b10b : boolean := false;
        EnableToHo_Egroup2Eproc2_HDLC   : boolean := false;
        EnableToHo_Egroup2Eproc2_8b10b  : boolean := false;
        EnableToHo_Egroup2Eproc4_8b10b  : boolean := false;
        EnableToHo_Egroup2Eproc8_8b10b  : boolean := false;
        EnableToHo_Egroup2Eproc16_8b10b : boolean := false;
        EnableToHo_Egroup3Eproc2_HDLC   : boolean := false;
        EnableToHo_Egroup3Eproc2_8b10b  : boolean := false;
        EnableToHo_Egroup3Eproc4_8b10b  : boolean := false;
        EnableToHo_Egroup3Eproc8_8b10b  : boolean := false;
        EnableToHo_Egroup3Eproc16_8b10b : boolean := false;
        EnableToHo_Egroup4Eproc2_HDLC   : boolean := false;
        EnableToHo_Egroup4Eproc2_8b10b  : boolean := false;
        EnableToHo_Egroup4Eproc4_8b10b  : boolean := false;
        EnableToHo_Egroup4Eproc8_8b10b  : boolean := false;
        EnableToHo_Egroup4Eproc16_8b10b : boolean := false;
        ENABLE_XVC                      : boolean := false);
    port (
        BUSY_OUT                  : out    std_logic;
        CLK40_FPGA2LMK_N          : out    std_logic;
        CLK40_FPGA2LMK_P          : out    std_logic;
        CLK_TTC_N                 : in     std_logic;
        CLK_TTC_P                 : in     std_logic;
        DATA_TTC_N                : in     std_logic;
        DATA_TTC_P                : in     std_logic;
        I2C_SMB                   : out    std_logic;
        I2C_SMBUS_CFG_nEN         : out    std_logic;
        I2C_nRESET                : out    std_logic;
        I2C_nRESET_PCIe           : out    std_logic;
        LMK_CLK                   : out    std_logic;
        LMK_DATA                  : out    std_logic;
        LMK_GOE                   : out    std_logic;
        LMK_LD                    : in     std_logic;
        LMK_LE                    : out    std_logic;
        LMK_SYNCn                 : out    std_logic;
        LOL_ADN                   : in     std_logic;
        LOS_ADN                   : in     std_logic;
        MGMT_PORT_EN              : out    std_logic;
        NT_PORTSEL                : out    std_logic_vector(2 downto 0);
        PCIE_PERSTn1              : out    std_logic;
        PCIE_PERSTn2              : out    std_logic;
        PEX_PERSTn                : out    std_logic;
        PEX_SCL                   : out    std_logic;
        PEX_SDA                   : inout  std_logic;
        PORT_GOOD                 : in     std_logic_vector(7 downto 0);
        Perstn1_open              : in     std_logic;
        Perstn2_open              : in     std_logic;
        Q2_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
        Q2_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
        Q4_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
        Q4_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
        Q5_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
        Q5_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
        Q6_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
        Q6_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
        Q8_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
        Q8_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
        RX_N                      : in     std_logic_vector(NUMCH*2-1 downto 0);
        RX_P                      : in     std_logic_vector(NUMCH*2-1 downto 0);
        SCL                       : inout  std_logic;
        SDA                       : inout  std_logic;
        SHPC_INT                  : out    std_logic;
        SI5345_A                  : out    std_logic_vector(1 downto 0);
        SI5345_INSEL              : out    std_logic_vector(1 downto 0);
        SI5345_OE                 : out    std_logic;
        SI5345_SEL                : out    std_logic;
        SI5345_nLOL               : in     std_logic;
        STN0_PORTCFG              : out    std_logic_vector(1 downto 0);
        STN1_PORTCFG              : out    std_logic_vector(1 downto 0);
        SmaOut_x3                 : out    std_logic;
        SmaOut_x4                 : out    std_logic;
        SmaOut_x5                 : out    std_logic;
        SmaOut_x6                 : out    std_logic;
        TACH                      : in     std_logic;
        TESTMODE                  : out    std_logic_vector(2 downto 0);
        TX_N                      : out    std_logic_vector(NUMCH*2-1 downto 0);
        TX_P                      : out    std_logic_vector(NUMCH*2-1 downto 0);
        UPSTREAM_PORTSEL          : out    std_logic_vector(2 downto 0);
        app_clk_in_n              : in     std_logic;
        app_clk_in_p              : in     std_logic;
        clk40_ttc_ref_out_n       : out    std_logic;
        clk40_ttc_ref_out_p       : out    std_logic;
        clk_ttcfx_ref1_in_n       : in     std_logic;
        clk_ttcfx_ref1_in_p       : in     std_logic;
        clk_ttcfx_ref2_in_n       : in     std_logic;
        clk_ttcfx_ref2_in_p       : in     std_logic;
        emcclk                    : in     std_logic;
        flash_SEL                 : out    std_logic;
        flash_a                   : out    std_logic_vector(24 downto 0);
        flash_a_msb               : inout  std_logic_vector(1 downto 0);
        flash_adv                 : out    std_logic;
        flash_cclk                : out    std_logic;
        flash_ce                  : out    std_logic;
        flash_d                   : inout  std_logic_vector(15 downto 0);
        flash_re                  : out    std_logic;
        flash_we                  : out    std_logic;
        opto_inhibit              : out    std_logic_vector(OPTO_TRX-1 downto 0);
        pcie_rxn                  : in     std_logic_vector(15 downto 0);
        pcie_rxp                  : in     std_logic_vector(15 downto 0);
        pcie_txn                  : out    std_logic_vector(15 downto 0);
        pcie_txp                  : out    std_logic_vector(15 downto 0);
        sys_clk0_n                : in     std_logic;
        sys_clk0_p                : in     std_logic;
        sys_clk1_n                : in     std_logic;
        sys_clk1_p                : in     std_logic;
        sys_reset_n               : in     std_logic;
        uC_reset_N                : out    std_logic);
end entity felix_mrod_top;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.felix_mrod_top.a0
--! =project    FELIX_MROD
--! @modified   Sat May 15 11:00:40 2021
--!-----------------------------------------------------------------------------

architecture a0 of felix_mrod_top is

    signal pcie_txlon        : std_logic_vector(7 downto 0);
    signal pcie_txlop        : std_logic_vector(7 downto 0);
    signal pcie_txhip        : std_logic_vector(7 downto 0);
    signal pcie_txhin        : std_logic_vector(7 downto 0);
    signal pcie_rxlon        : std_logic_vector(7 downto 0);
    signal pcie_rxlop        : std_logic_vector(7 downto 0);
    signal pcie_rxhin        : std_logic_vector(7 downto 0);
    signal pcie_rxhip        : std_logic_vector(7 downto 0);
    signal LMK_Locked        : std_logic;
    signal rst_hw            : std_logic;
    signal clk80             : std_logic;
    signal clk160            : std_logic;
    signal clk250            : std_logic;
    signal clk_ttc_40_s      : std_logic;
    signal CDRlocked         : std_logic;
    signal clk10_xtal        : std_logic;
    signal clk40_xtal        : std_logic;
    signal MMCM_Locked       : std_logic;
    signal MMCM_OscSelect    : std_logic;
    signal rst0_soft_40      : std_logic;
    signal CR0_FIFO_Busy     : std_logic;
    signal CR1_FIFO_Busy     : std_logic;
    signal TTC_ToHost_Data   : TTC_ToHost_data_type;
    signal clk40             : std_logic;
    signal TTC_out           : std_logic_vector(15 downto 0);
    signal prmap_ttc_monitor : register_map_ttc_monitor_type;
    signal pcie0_appreg_clk  : std_logic;
    signal pcie0_soft_reset  : std_logic;
    signal prmap_hk_monitor  : register_map_hk_monitor_type;
    signal prmap_board_info  : register_map_gen_board_info_type;
    signal prmap0_40_control : register_map_control_type;
    signal lnk0_up           : std_logic;
    signal pcie0_DMA_Busy    : std_logic;
    signal pcie1_DMA_Busy    : std_logic;
    signal lnk1_up           : std_logic;
    signal u22_ChValid       : std_logic_vector(NUMCH-1 downto 0);
    signal u12_ChValid       : std_logic_vector(NUMCH-1 downto 0);
    signal u11_RxValid       : std_logic_vector(NUMCH-1 downto 0);
    signal u21_RxValid       : std_logic_vector(NUMCH-1 downto 0);
    signal u12_TxValid       : std_logic_vector(NUMCH-1 downto 0);
    signal u22_TxValid       : std_logic_vector(NUMCH-1 downto 0);
    signal prmap_app_control : register_map_control_type;
    signal u12_ChData        : slv33_array(0 to NUMCH-1);
    signal u22_ChData        : slv33_array(0 to NUMCH-1);
    signal u13_ChBusy        : std_logic_vector(NUMCH-1 downto 0);
    signal u23_ChBusy        : std_logic_vector(NUMCH-1 downto 0);
    signal u12_fhFifoRE      : std_logic;
    signal u13_fhFifoEmpty   : std_logic;
    signal u22_fhFifoRE      : std_logic;
    signal u23_fhFifoEmpty   : std_logic;
    signal u12_TxData        : slv33_array(0 to NUMCH-1);
    signal u22_TxData        : slv33_array(0 to NUMCH-1);
    signal u11_RxData        : slv32_array(0 to NUMCH-1);
    signal u21_RxData        : slv32_array(0 to NUMCH-1);
    signal u13_fhFifoValid   : std_logic;
    signal u23_fhFifoValid   : std_logic;
    signal u13_fhFifoD       : std_logic_vector(31 downto 0);
    signal u23_fhFifoD       : std_logic_vector(31 downto 0);
    signal u11_RX_N          : std_logic_vector(NUMCH-1 downto 0);
    signal u11_RX_P          : std_logic_vector(NUMCH-1 downto 0);
    signal u21_RX_N          : std_logic_vector(NUMCH-1 downto 0);
    signal u21_RX_P          : std_logic_vector(NUMCH-1 downto 0);
    signal u11_TX_N          : std_logic_vector(NUMCH-1 downto 0);
    signal u11_TX_P          : std_logic_vector(NUMCH-1 downto 0);
    signal u21_TX_P          : std_logic_vector(NUMCH-1 downto 0);
    signal u21_TX_N          : std_logic_vector(NUMCH-1 downto 0);
    signal CR0BusyOut        : std_logic_vector(NUMCH-1 downto 0);
    signal CR1BusyOut        : std_logic_vector(NUMCH-1 downto 0);
    signal clk50             : std_logic;
    signal QX_GTREFCLK_N     : std_logic_vector(4 downto 0);
    signal QX_GTREFCLK_P     : std_logic_vector(4 downto 0);
    signal u21_TxClk         : std_logic_vector(NUMCH-1 downto 0);
    signal u11_TxClk         : std_logic_vector(NUMCH-1 downto 0);
    signal BUSY_INTERRUPT    : std_logic;
    signal u11_RxClk         : std_logic_vector(NUMCH-1 downto 0);
    signal u21_RxClk         : std_logic_vector(NUMCH-1 downto 0);
    signal MasterBusy        : std_logic;
    signal Trx0Monitor       : regs_trx_monitor;
    signal Trx1Monitor       : regs_trx_monitor;
    signal CSM0Monitor       : regs_csm_monitor;
    signal CSM1Monitor       : regs_csm_monitor;

begin
    BUSY_OUT <= MasterBusy;

    u11: entity work.TrxBuffer(a0)
        generic map(
            NUMCH      => NUMCH,
            W_ENDPOINT => 0)
        port map(
            QX_GTREFCLK_N     => QX_GTREFCLK_N,
            QX_GTREFCLK_P     => QX_GTREFCLK_P,
            RXtoGTH_N         => u11_RX_N,
            RXtoGTH_P         => u11_RX_P,
            RxClk             => u11_RxClk,
            RxData            => u11_RxData,
            RxValid           => u11_RxValid,
            TXfrGTH_N         => u11_TX_N,
            TXfrGTH_P         => u11_TX_P,
            TrxMonitor        => Trx0Monitor,
            TxClk             => u11_TxClk,
            TxData            => u12_TxData,
            TxValid           => u12_TxValid,
            clk40             => clk40,
            clk50             => clk50,
            prmap_app_control => prmap_app_control,
            sys_reset_n       => sys_reset_n);

    u21: entity work.TrxBuffer(a0)
        generic map(
            NUMCH      => NUMCH,
            W_ENDPOINT => 1)
        port map(
            QX_GTREFCLK_N     => QX_GTREFCLK_N,
            QX_GTREFCLK_P     => QX_GTREFCLK_P,
            RXtoGTH_N         => u21_RX_N,
            RXtoGTH_P         => u21_RX_P,
            RxClk             => u21_RxClk,
            RxData            => u21_RxData,
            RxValid           => u21_RxValid,
            TXfrGTH_N         => u21_TX_N,
            TXfrGTH_P         => u21_TX_P,
            TrxMonitor        => Trx1Monitor,
            TxClk             => u21_TxClk,
            TxData            => u22_TxData,
            TxValid           => u22_TxValid,
            clk40             => clk40,
            clk50             => clk50,
            prmap_app_control => prmap_app_control,
            sys_reset_n       => sys_reset_n);

    u12: entity work.CSMHandler(a0)
        generic map(
            NUMCH      => NUMCH,
            W_ENDPOINT => 0)
        port map(
            CSMMonitor        => CSM0Monitor,
            ChBusy            => u13_ChBusy,
            ChData            => u12_ChData,
            ChValid           => u12_ChValid,
            RxClk             => u11_RxClk,
            RxData            => u11_RxData,
            RxValid           => u11_RxValid,
            TTC_out           => TTC_out,
            TxClk             => u11_TxClk,
            TxData            => u12_TxData,
            TxValid           => u12_TxValid,
            clk40             => clk40,
            clk50             => clk50,
            clk80             => clk80,
            fhFifoD           => u13_fhFifoD,
            fhFifoEmpty       => u13_fhFifoEmpty,
            fhFifoRE          => u12_fhFifoRE,
            fhFifoValid       => u13_fhFifoValid,
            prmap_app_control => prmap_app_control,
            sys_reset_n       => sys_reset_n);

    u22: entity work.CSMHandler(a0)
        generic map(
            NUMCH      => NUMCH,
            W_ENDPOINT => 1)
        port map(
            CSMMonitor        => CSM1Monitor,
            ChBusy            => u23_ChBusy,
            ChData            => u22_ChData,
            ChValid           => u22_ChValid,
            RxClk             => u21_RxClk,
            RxData            => u21_RxData,
            RxValid           => u21_RxValid,
            TTC_out           => TTC_out,
            TxClk             => u21_TxClk,
            TxData            => u22_TxData,
            TxValid           => u22_TxValid,
            clk40             => clk40,
            clk50             => clk50,
            clk80             => clk80,
            fhFifoD           => u23_fhFifoD,
            fhFifoEmpty       => u23_fhFifoEmpty,
            fhFifoRE          => u22_fhFifoRE,
            fhFifoValid       => u23_fhFifoValid,
            prmap_app_control => prmap_app_control,
            sys_reset_n       => sys_reset_n);

    u31: entity work.Busy_TTC(a0)
        generic map(
            GBT_NUM => GBT_NUM,
            NUMCH   => NUMCH)
        port map(
            BUSY_INTERRUPT    => BUSY_INTERRUPT,
            CDRlocked         => CDRlocked,
            CLK_TTC_N         => CLK_TTC_N,
            CLK_TTC_P         => CLK_TTC_P,
            CR0BusyOut        => CR0BusyOut,
            CR0_FIFO_Busy     => CR0_FIFO_Busy,
            CR1BusyOut        => CR1BusyOut,
            CR1_FIFO_Busy     => CR1_FIFO_Busy,
            DATA_TTC_N        => DATA_TTC_N,
            DATA_TTC_P        => DATA_TTC_P,
            LOL_ADN           => LOL_ADN,
            LOS_ADN           => LOS_ADN,
            MasterBusy        => MasterBusy,
            TTC_ToHost_Data   => TTC_ToHost_Data,
            TTC_out           => TTC_out,
            clk40             => clk40,
            clk_ttc_40_s      => clk_ttc_40_s,
            pcie0_DMA_Busy    => pcie0_DMA_Busy,
            pcie1_DMA_Busy    => pcie1_DMA_Busy,
            prmap_40_control  => prmap0_40_control,
            prmap_ttc_monitor => prmap_ttc_monitor,
            rst_soft_40       => rst0_soft_40,
            sys_reset_n       => sys_reset_n);

    u32: entity work.Clks_Reset(a0)
        generic map(
            APP_CLK_FREQ           => APP_CLK_FREQ,
            USE_BACKUP_CLK         => USE_BACKUP_CLK,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH)
        port map(
            CDRlocked           => CDRlocked,
            CLK40_FPGA2LMK_N    => CLK40_FPGA2LMK_N,
            CLK40_FPGA2LMK_P    => CLK40_FPGA2LMK_P,
            LMK_CLK             => LMK_CLK,
            LMK_DATA            => LMK_DATA,
            LMK_GOE             => LMK_GOE,
            LMK_LD              => LMK_LD,
            LMK_LE              => LMK_LE,
            LMK_Locked          => LMK_Locked,
            LMK_SYNCn           => LMK_SYNCn,
            MMCM_Locked         => MMCM_Locked,
            MMCM_OscSelect      => MMCM_OscSelect,
            app_clk_in_n        => app_clk_in_n,
            app_clk_in_p        => app_clk_in_p,
            clk10_xtal          => clk10_xtal,
            clk160              => clk160,
            clk240              => open,
            clk250              => clk250,
            clk40               => clk40,
            clk40_ttc_ref_out_n => clk40_ttc_ref_out_n,
            clk40_ttc_ref_out_p => clk40_ttc_ref_out_p,
            clk40_xtal          => clk40_xtal,
            clk50               => clk50,
            clk80               => clk80,
            clk_ttc_40_s        => clk_ttc_40_s,
            clk_ttcfx_ref1_in_n => clk_ttcfx_ref1_in_n,
            clk_ttcfx_ref1_in_p => clk_ttcfx_ref1_in_p,
            clk_ttcfx_ref2_in_n => clk_ttcfx_ref2_in_n,
            clk_ttcfx_ref2_in_p => clk_ttcfx_ref2_in_p,
            prmap_app_control   => prmap_app_control,
            rst_hw              => rst_hw,
            sys_reset_n         => sys_reset_n);

    u33: entity work.CareTaker(a0)
        generic map(
            OPTO_TRX                        => OPTO_TRX,
            GBT_NUM                         => GBT_NUM,
            ENDPOINTS                       => ENDPOINTS,
            NUMCH                           => NUMCH,
            DEBUG_MODE                      => DEBUG_MODE,
            CARD_TYPE                       => CARD_TYPE,
            GENERATE_GBT                    => GENERATE_GBT,
            GBT_MAPPING                     => GBT_MAPPING,
            GENERATE_XOFF                   => GENERATE_XOFF,
            GTHREFCLK_SEL                   => GTHREFCLK_SEL,
            AUTOMATIC_CLOCK_SWITCH          => AUTOMATIC_CLOCK_SWITCH,
            FIRMWARE_MODE                   => FIRMWARE_MODE,
            CREnableFromHost                => CREnableFromHost,
            crInternalLoopbackMode          => crInternalLoopbackMode,
            includeDirectMode               => includeDirectMode,
            TTC_test_mode                   => TTC_test_mode,
            USE_Si5324_RefCLK               => USE_Si5324_RefCLK,
            generateTTCemu                  => generateTTCemu,
            generate_IC_EC_TTC_only         => generate_IC_EC_TTC_only,
            wideMode                        => false,
            EnableFrHo_Egroup0Eproc2_HDLC   => EnableFrHo_Egroup0Eproc2_HDLC,
            EnableFrHo_Egroup0Eproc2_8b10b  => EnableFrHo_Egroup0Eproc2_8b10b,
            EnableFrHo_Egroup0Eproc4_8b10b  => EnableFrHo_Egroup0Eproc4_8b10b,
            EnableFrHo_Egroup0Eproc8_8b10b  => EnableFrHo_Egroup0Eproc8_8b10b,
            EnableFrHo_Egroup1Eproc2_HDLC   => EnableFrHo_Egroup1Eproc2_HDLC,
            EnableFrHo_Egroup1Eproc2_8b10b  => EnableFrHo_Egroup1Eproc2_8b10b,
            EnableFrHo_Egroup1Eproc4_8b10b  => EnableFrHo_Egroup1Eproc4_8b10b,
            EnableFrHo_Egroup1Eproc8_8b10b  => EnableFrHo_Egroup1Eproc8_8b10b,
            EnableFrHo_Egroup2Eproc2_HDLC   => EnableFrHo_Egroup2Eproc2_HDLC,
            EnableFrHo_Egroup2Eproc2_8b10b  => EnableFrHo_Egroup2Eproc2_8b10b,
            EnableFrHo_Egroup2Eproc4_8b10b  => EnableFrHo_Egroup2Eproc4_8b10b,
            EnableFrHo_Egroup2Eproc8_8b10b  => EnableFrHo_Egroup2Eproc8_8b10b,
            EnableFrHo_Egroup3Eproc2_HDLC   => EnableFrHo_Egroup3Eproc2_HDLC,
            EnableFrHo_Egroup3Eproc2_8b10b  => EnableFrHo_Egroup3Eproc2_8b10b,
            EnableFrHo_Egroup3Eproc4_8b10b  => EnableFrHo_Egroup3Eproc4_8b10b,
            EnableFrHo_Egroup3Eproc8_8b10b  => EnableFrHo_Egroup3Eproc8_8b10b,
            EnableFrHo_Egroup4Eproc2_HDLC   => EnableFrHo_Egroup4Eproc2_HDLC,
            EnableFrHo_Egroup4Eproc2_8b10b  => EnableFrHo_Egroup4Eproc2_8b10b,
            EnableFrHo_Egroup4Eproc4_8b10b  => EnableFrHo_Egroup4Eproc4_8b10b,
            EnableFrHo_Egroup4Eproc8_8b10b  => EnableFrHo_Egroup4Eproc8_8b10b,
            EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
            EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
            EnableToHo_Egroup0Eproc4_8b10b  => EnableToHo_Egroup0Eproc4_8b10b,
            EnableToHo_Egroup0Eproc8_8b10b  => EnableToHo_Egroup0Eproc8_8b10b,
            EnableToHo_Egroup0Eproc16_8b10b => EnableToHo_Egroup0Eproc16_8b10b,
            EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
            EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
            EnableToHo_Egroup1Eproc4_8b10b  => EnableToHo_Egroup1Eproc4_8b10b,
            EnableToHo_Egroup1Eproc8_8b10b  => EnableToHo_Egroup1Eproc8_8b10b,
            EnableToHo_Egroup1Eproc16_8b10b => EnableToHo_Egroup1Eproc16_8b10b,
            EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
            EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
            EnableToHo_Egroup2Eproc4_8b10b  => EnableToHo_Egroup2Eproc4_8b10b,
            EnableToHo_Egroup2Eproc8_8b10b  => EnableToHo_Egroup2Eproc8_8b10b,
            EnableToHo_Egroup2Eproc16_8b10b => EnableToHo_Egroup2Eproc16_8b10b,
            EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
            EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
            EnableToHo_Egroup3Eproc4_8b10b  => EnableToHo_Egroup3Eproc4_8b10b,
            EnableToHo_Egroup3Eproc8_8b10b  => EnableToHo_Egroup3Eproc8_8b10b,
            EnableToHo_Egroup3Eproc16_8b10b => EnableToHo_Egroup3Eproc16_8b10b,
            EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
            EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
            EnableToHo_Egroup4Eproc4_8b10b  => EnableToHo_Egroup4Eproc4_8b10b,
            EnableToHo_Egroup4Eproc8_8b10b  => EnableToHo_Egroup4Eproc8_8b10b,
            EnableToHo_Egroup4Eproc16_8b10b => EnableToHo_Egroup4Eproc16_8b10b,
            GENERATE_FEI4B                  => GENERATE_FEI4B,
            SUPER_CHUNK_FACTOR              => SUPER_CHUNK_FACTOR,
            BLOCKSIZE                       => BLOCKSIZE,
            CHUNK_TRAILER_32B               => CHUNK_TRAILER_32B)
        port map(
            I2C_SMB           => I2C_SMB,
            I2C_SMBUS_CFG_nEN => I2C_SMBUS_CFG_nEN,
            I2C_nRESET        => I2C_nRESET,
            I2C_nRESET_PCIe   => I2C_nRESET_PCIe,
            LMK_Locked        => LMK_Locked,
            MGMT_PORT_EN      => MGMT_PORT_EN,
            MMCM_Locked       => MMCM_Locked,
            MMCM_OscSelect    => MMCM_OscSelect,
            NT_PORTSEL        => NT_PORTSEL,
            PCIE_PERSTn1      => PCIE_PERSTn1,
            PCIE_PERSTn2      => PCIE_PERSTn2,
            PEX_PERSTn        => PEX_PERSTn,
            PEX_SCL           => PEX_SCL,
            PEX_SDA           => PEX_SDA,
            PORT_GOOD         => PORT_GOOD,
            Perstn1_open      => Perstn1_open,
            Perstn2_open      => Perstn2_open,
            RxClk0F           => u11_RxClk,
            RxClk1F           => u21_RxClk,
            SCL               => SCL,
            SDA               => SDA,
            SHPC_INT          => SHPC_INT,
            SI5345_A          => SI5345_A,
            SI5345_INSEL      => SI5345_INSEL,
            SI5345_OE         => SI5345_OE,
            SI5345_SEL        => SI5345_SEL,
            SI5345_nLOL       => SI5345_nLOL,
            STN0_PORTCFG      => STN0_PORTCFG,
            STN1_PORTCFG      => STN1_PORTCFG,
            SmaOut_x3         => SmaOut_x3,
            SmaOut_x4         => SmaOut_x4,
            SmaOut_x5         => SmaOut_x5,
            SmaOut_x6         => SmaOut_x6,
            TACH              => TACH,
            TESTMODE          => TESTMODE,
            UPSTREAM_PORTSEL  => UPSTREAM_PORTSEL,
            clk10_xtal        => clk10_xtal,
            clk40_xtal        => clk40_xtal,
            emcclk            => emcclk,
            flash_SEL         => flash_SEL,
            flash_a           => flash_a,
            flash_a_msb       => flash_a_msb,
            flash_adv         => flash_adv,
            flash_cclk        => flash_cclk,
            flash_ce          => flash_ce,
            flash_d           => flash_d,
            flash_re          => flash_re,
            flash_we          => flash_we,
            lnk0_up           => lnk0_up,
            lnk1_up           => lnk1_up,
            opto_inhibit      => opto_inhibit,
            pcie_appreg_clk   => pcie0_appreg_clk,
            pcie_soft_reset   => pcie0_soft_reset,
            prmap_app_control => prmap_app_control,
            prmap_board_info  => prmap_board_info,
            prmap_hk_monitor  => prmap_hk_monitor,
            sys_reset_n       => sys_reset_n,
            uC_reset_N        => uC_reset_N);

    u34: entity work.GlueBox(a0)
        generic map(
            NUMCH => NUMCH)
        port map(
            Q2_CLK0_GTREFCLK_PAD_N_IN => Q2_CLK0_GTREFCLK_PAD_N_IN,
            Q2_CLK0_GTREFCLK_PAD_P_IN => Q2_CLK0_GTREFCLK_PAD_P_IN,
            Q4_CLK0_GTREFCLK_PAD_N_IN => Q4_CLK0_GTREFCLK_PAD_N_IN,
            Q4_CLK0_GTREFCLK_PAD_P_IN => Q4_CLK0_GTREFCLK_PAD_P_IN,
            Q5_CLK0_GTREFCLK_PAD_N_IN => Q5_CLK0_GTREFCLK_PAD_N_IN,
            Q5_CLK0_GTREFCLK_PAD_P_IN => Q5_CLK0_GTREFCLK_PAD_P_IN,
            Q6_CLK0_GTREFCLK_PAD_N_IN => Q6_CLK0_GTREFCLK_PAD_N_IN,
            Q6_CLK0_GTREFCLK_PAD_P_IN => Q6_CLK0_GTREFCLK_PAD_P_IN,
            Q8_CLK0_GTREFCLK_PAD_N_IN => Q8_CLK0_GTREFCLK_PAD_N_IN,
            Q8_CLK0_GTREFCLK_PAD_P_IN => Q8_CLK0_GTREFCLK_PAD_P_IN,
            QX_GTREFCLK_N             => QX_GTREFCLK_N,
            QX_GTREFCLK_P             => QX_GTREFCLK_P,
            RXA_N                     => u11_RX_N,
            RXA_P                     => u11_RX_P,
            RXB_N                     => u21_RX_N,
            RXB_P                     => u21_RX_P,
            RX_N                      => RX_N,
            RX_P                      => RX_P,
            TXA_N                     => u11_TX_N,
            TXA_P                     => u11_TX_P,
            TXB_N                     => u21_TX_N,
            TXB_P                     => u21_TX_P,
            TX_N                      => TX_N,
            TX_P                      => TX_P,
            pcie_rxhin                => pcie_rxhin,
            pcie_rxhip                => pcie_rxhip,
            pcie_rxlon                => pcie_rxlon,
            pcie_rxlop                => pcie_rxlop,
            pcie_rxn                  => pcie_rxn,
            pcie_rxp                  => pcie_rxp,
            pcie_txhin                => pcie_txhin,
            pcie_txhip                => pcie_txhip,
            pcie_txlon                => pcie_txlon,
            pcie_txlop                => pcie_txlop,
            pcie_txn                  => pcie_txn,
            pcie_txp                  => pcie_txp);

    u13: entity work.CR_Wupper(a0)
        generic map(
            NUMBER_OF_INTERRUPTS           => NUMBER_OF_INTERRUPTS,
            NUMBER_OF_DESCRIPTORS          => NUMBER_OF_DESCRIPTORS,
            GBT_NUM                        => GBT_NUM,
            FMCH_NUM                       => GBT_NUM/2,
            NUMCH                          => NUMCH,
            GBT_GENERATE_ALL_REGS          => false,
            EMU_GENERATE_REGS              => false,
            MROD_GENERATE_REGS             => MROD_GENERATE_REGS,
            CARD_TYPE                      => CARD_TYPE,
            W_ENDPOINT                     => 0,
            GENERATE_XOFF                  => GENERATE_XOFF,
            STATIC_CENTRALROUTER           => STATIC_CENTRALROUTER,
            CREnableFromHost               => CREnableFromHost,
            toHostTimeoutBitn              => toHostTimeoutBitn,
            BLOCKSIZE                      => BLOCKSIZE,
            CHUNK_TRAILER_32B              => CHUNK_TRAILER_32B,
            SUPER_CHUNK_FACTOR             => SUPER_CHUNK_FACTOR,
            wideMode                       => false,
            FIRMWARE_MODE                  => FIRMWARE_MODE,
            BUILD_DATETIME                 => BUILD_DATETIME,
            GIT_HASH                       => GIT_HASH,
            GIT_TAG                        => GIT_TAG,
            GIT_COMMIT_NUMBER              => GIT_COMMIT_NUMBER,
            COMMIT_DATETIME                => COMMIT_DATETIME,
            EnableFrHo_Egroup0Eproc2_HDLC  => EnableFrHo_Egroup0Eproc2_HDLC,
            EnableFrHo_Egroup0Eproc2_8b10b => EnableFrHo_Egroup0Eproc2_8b10b,
            EnableFrHo_Egroup0Eproc4_8b10b => EnableFrHo_Egroup0Eproc4_8b10b,
            EnableFrHo_Egroup0Eproc8_8b10b => EnableFrHo_Egroup0Eproc8_8b10b,
            EnableFrHo_Egroup1Eproc2_HDLC  => EnableFrHo_Egroup1Eproc2_HDLC,
            EnableFrHo_Egroup1Eproc2_8b10b => EnableFrHo_Egroup1Eproc2_8b10b,
            EnableFrHo_Egroup1Eproc4_8b10b => EnableFrHo_Egroup1Eproc4_8b10b,
            EnableFrHo_Egroup1Eproc8_8b10b => EnableFrHo_Egroup1Eproc8_8b10b,
            EnableFrHo_Egroup2Eproc2_HDLC  => EnableFrHo_Egroup2Eproc2_HDLC,
            EnableFrHo_Egroup2Eproc2_8b10b => EnableFrHo_Egroup2Eproc2_8b10b,
            EnableFrHo_Egroup2Eproc4_8b10b => EnableFrHo_Egroup2Eproc4_8b10b,
            EnableFrHo_Egroup2Eproc8_8b10b => EnableFrHo_Egroup2Eproc8_8b10b,
            EnableFrHo_Egroup3Eproc2_HDLC  => EnableFrHo_Egroup3Eproc2_HDLC,
            EnableFrHo_Egroup3Eproc2_8b10b => EnableFrHo_Egroup3Eproc2_8b10b,
            EnableFrHo_Egroup3Eproc4_8b10b => EnableFrHo_Egroup3Eproc4_8b10b,
            EnableFrHo_Egroup3Eproc8_8b10b => EnableFrHo_Egroup3Eproc8_8b10b,
            EnableFrHo_Egroup4Eproc2_HDLC  => EnableFrHo_Egroup4Eproc2_HDLC,
            EnableFrHo_Egroup4Eproc2_8b10b => EnableFrHo_Egroup4Eproc2_8b10b,
            EnableFrHo_Egroup4Eproc4_8b10b => EnableFrHo_Egroup4Eproc4_8b10b,
            EnableFrHo_Egroup4Eproc8_8b10b => EnableFrHo_Egroup4Eproc8_8b10b,
            ENABLE_XVC                     => ENABLE_XVC)
        port map(
            BUSY_INTERRUPT    => BUSY_INTERRUPT,
            CRBusyOut         => CR0BusyOut,
            CR_FIFO_Busy      => CR0_FIFO_Busy,
            CSM0Monitor       => CSM0Monitor,
            CSM1Monitor       => CSM1Monitor,
            ChBusy            => u13_ChBusy,
            ChData            => u12_ChData,
            ChValid           => u12_ChValid,
            MasterBusy        => MasterBusy,
            TTC_ToHost_Data   => TTC_ToHost_Data,
            TTC_out           => TTC_out,
            Trx0Monitor       => Trx0Monitor,
            Trx1Monitor       => Trx1Monitor,
            clk160            => clk160,
            clk250            => clk250,
            clk40             => clk40,
            clk80             => clk80,
            fhFifoD           => u13_fhFifoD,
            fhFifoEmpty       => u13_fhFifoEmpty,
            fhFifoRE          => u12_fhFifoRE,
            fhFifoValid       => u13_fhFifoValid,
            lnk_up            => lnk0_up,
            pcie_DMA_Busy     => pcie0_DMA_Busy,
            pcie_appreg_clk   => pcie0_appreg_clk,
            pcie_rxn          => pcie_rxlon,
            pcie_rxp          => pcie_rxlop,
            pcie_soft_reset   => pcie0_soft_reset,
            pcie_txn          => pcie_txlon,
            pcie_txp          => pcie_txlop,
            prmap_40_control  => prmap0_40_control,
            prmap_app_control => prmap_app_control,
            prmap_board_info  => prmap_board_info,
            prmap_hk_monitor  => prmap_hk_monitor,
            prmap_ttc_monitor => prmap_ttc_monitor,
            rst_hw            => rst_hw,
            rst_soft_40       => rst0_soft_40,
            sys_clk_n         => sys_clk0_n,
            sys_clk_p         => sys_clk0_p,
            sys_reset_n       => sys_reset_n);

    u23: entity work.CR_Wupper(a0)
        generic map(
            NUMBER_OF_INTERRUPTS           => NUMBER_OF_INTERRUPTS,
            NUMBER_OF_DESCRIPTORS          => NUMBER_OF_DESCRIPTORS,
            GBT_NUM                        => GBT_NUM,
            FMCH_NUM                       => GBT_NUM/2,
            NUMCH                          => NUMCH,
            GBT_GENERATE_ALL_REGS          => false,
            EMU_GENERATE_REGS              => false,
            MROD_GENERATE_REGS             => MROD_GENERATE_REGS,
            CARD_TYPE                      => CARD_TYPE,
            W_ENDPOINT                     => 1,
            GENERATE_XOFF                  => GENERATE_XOFF,
            STATIC_CENTRALROUTER           => STATIC_CENTRALROUTER,
            CREnableFromHost               => CREnableFromHost,
            toHostTimeoutBitn              => toHostTimeoutBitn,
            BLOCKSIZE                      => BLOCKSIZE,
            CHUNK_TRAILER_32B              => CHUNK_TRAILER_32B,
            SUPER_CHUNK_FACTOR             => SUPER_CHUNK_FACTOR,
            wideMode                       => false,
            FIRMWARE_MODE                  => FIRMWARE_MODE,
            BUILD_DATETIME                 => BUILD_DATETIME,
            GIT_HASH                       => GIT_HASH,
            GIT_TAG                        => GIT_TAG,
            GIT_COMMIT_NUMBER              => GIT_COMMIT_NUMBER,
            COMMIT_DATETIME                => COMMIT_DATETIME,
            EnableFrHo_Egroup0Eproc2_HDLC  => EnableFrHo_Egroup0Eproc2_HDLC,
            EnableFrHo_Egroup0Eproc2_8b10b => EnableFrHo_Egroup0Eproc2_8b10b,
            EnableFrHo_Egroup0Eproc4_8b10b => EnableFrHo_Egroup0Eproc4_8b10b,
            EnableFrHo_Egroup0Eproc8_8b10b => EnableFrHo_Egroup0Eproc8_8b10b,
            EnableFrHo_Egroup1Eproc2_HDLC  => EnableFrHo_Egroup1Eproc2_HDLC,
            EnableFrHo_Egroup1Eproc2_8b10b => EnableFrHo_Egroup1Eproc2_8b10b,
            EnableFrHo_Egroup1Eproc4_8b10b => EnableFrHo_Egroup1Eproc4_8b10b,
            EnableFrHo_Egroup1Eproc8_8b10b => EnableFrHo_Egroup1Eproc8_8b10b,
            EnableFrHo_Egroup2Eproc2_HDLC  => EnableFrHo_Egroup2Eproc2_HDLC,
            EnableFrHo_Egroup2Eproc2_8b10b => EnableFrHo_Egroup2Eproc2_8b10b,
            EnableFrHo_Egroup2Eproc4_8b10b => EnableFrHo_Egroup2Eproc4_8b10b,
            EnableFrHo_Egroup2Eproc8_8b10b => EnableFrHo_Egroup2Eproc8_8b10b,
            EnableFrHo_Egroup3Eproc2_HDLC  => EnableFrHo_Egroup3Eproc2_HDLC,
            EnableFrHo_Egroup3Eproc2_8b10b => EnableFrHo_Egroup3Eproc2_8b10b,
            EnableFrHo_Egroup3Eproc4_8b10b => EnableFrHo_Egroup3Eproc4_8b10b,
            EnableFrHo_Egroup3Eproc8_8b10b => EnableFrHo_Egroup3Eproc8_8b10b,
            EnableFrHo_Egroup4Eproc2_HDLC  => EnableFrHo_Egroup4Eproc2_HDLC,
            EnableFrHo_Egroup4Eproc2_8b10b => EnableFrHo_Egroup4Eproc2_8b10b,
            EnableFrHo_Egroup4Eproc4_8b10b => EnableFrHo_Egroup4Eproc4_8b10b,
            EnableFrHo_Egroup4Eproc8_8b10b => EnableFrHo_Egroup4Eproc8_8b10b,
            ENABLE_XVC                     => ENABLE_XVC)
        port map(
            BUSY_INTERRUPT    => BUSY_INTERRUPT,
            CRBusyOut         => CR1BusyOut,
            CR_FIFO_Busy      => CR1_FIFO_Busy,
            CSM0Monitor       => CSM0Monitor,
            CSM1Monitor       => CSM1Monitor,
            ChBusy            => u23_ChBusy,
            ChData            => u22_ChData,
            ChValid           => u22_ChValid,
            MasterBusy        => MasterBusy,
            TTC_ToHost_Data   => TTC_ToHost_Data,
            TTC_out           => TTC_out,
            Trx0Monitor       => Trx0Monitor,
            Trx1Monitor       => Trx1Monitor,
            clk160            => clk160,
            clk250            => clk250,
            clk40             => clk40,
            clk80             => clk80,
            fhFifoD           => u23_fhFifoD,
            fhFifoEmpty       => u23_fhFifoEmpty,
            fhFifoRE          => u22_fhFifoRE,
            fhFifoValid       => u23_fhFifoValid,
            lnk_up            => lnk1_up,
            pcie_DMA_Busy     => pcie1_DMA_Busy,
            pcie_appreg_clk   => open,
            pcie_rxn          => pcie_rxhin,
            pcie_rxp          => pcie_rxhip,
            pcie_soft_reset   => open,
            pcie_txn          => pcie_txhin,
            pcie_txp          => pcie_txhip,
            prmap_40_control  => open,
            prmap_app_control => open,
            prmap_board_info  => prmap_board_info,
            prmap_hk_monitor  => prmap_hk_monitor,
            prmap_ttc_monitor => prmap_ttc_monitor,
            rst_hw            => rst_hw,
            rst_soft_40       => open,
            sys_clk_n         => sys_clk1_n,
            sys_clk_p         => sys_clk1_p,
            sys_reset_n       => sys_reset_n);

end architecture a0 ; -- of felix_mrod_top

