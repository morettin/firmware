--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.DataEmu
--! =project    FELIX_MROD
--! @modified   Mon Apr 19 15:58:30 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity DataEmu is
  generic(
    chid : integer := 0);
  port (
    ChBusy     : in     std_logic;
    ECR        : in     std_logic;
    EnaTxCount : in     std_logic;
    FHDEna     : in     std_logic;
    FHDValid   : in     std_logic;
    FHData     : in     std_logic_vector(31 downto 0);
    GTMode     : in     std_logic_vector(2 downto 0);
    MReset     : in     std_logic;
    SClk       : in     std_logic;
    Trigger    : in     std_logic;
    TxClk      : in     std_logic;
    TxData     : out    std_logic_vector(32 downto 0);
    TxValid    : out    std_logic);
end entity DataEmu;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.DataEmu.a0
--! =project    FELIX_MROD
--! @modified   Mon Apr 19 15:58:30 2021
--!-----------------------------------------------------------------------------

architecture a0 of DataEmu is

  signal RdReq      : std_logic;
  signal QF         : std_logic_vector(31 downto 0);
  signal QValid     : std_logic;
  signal Empty      : std_logic;
  signal u15_Empty  : std_logic;
  signal EndEvt     : std_logic;
  signal ForceIdle  : std_logic;
  signal u16_RdReq  : std_logic;
  signal u15_Full   : std_logic;
  signal u15_Q      : std_logic_vector(31 downto 0);
  signal u15_QValid : std_logic;
  signal u13_QValid : std_logic;
  signal u13_Q      : std_logic_vector(31 downto 0);
  signal u11_QValid : std_logic;
  signal u11_Q      : std_logic_vector(31 downto 0);

begin
  --
  --The event data fifo u13 has no Full flags connected.
  --The user (HOST) should avoid overloading this fifo!
  --
  --GTMode(0) : 1/0 : enable / disable CSM testrun
  --GTMode(1) : 1/0 : circulate fifo / fill fifo from HOST 
  --GTMode(2) : 1/0 : triggered / untriggered test mode
  --
  --(EvtMux replaces ECnt in BOT and EOT)
  --(SClk: 80MHz, TxClk=Clk50: 50MHz)
  --

  u11: entity work.FSMux(a0)
    port map(
      FHDEna => FHDEna,
      GTMode => GTMode,
      MReset => MReset,
      Q      => u11_Q,
      QValid => u11_QValid,
      SClk   => SClk,
      WD     => FHData,
      WDEN   => FHDValid,
      WF     => QF,
      WFEN   => QValid);

  u12: entity work.TrigCnt(a0)
    port map(
      ECR       => ECR,
      EndEvt    => EndEvt,
      ForceIdle => ForceIdle,
      GTMode    => GTMode,
      MReset    => MReset,
      SClk      => SClk,
      Trigger   => Trigger);

  u13: entity work.Fifo4096w(a0)
    port map(
      D      => u11_Q,
      Empty  => Empty,
      Full   => open,
      MReset => MReset,
      Q      => u13_Q,
      QValid => u13_QValid,
      RdReq  => RdReq,
      WClk   => SClk,
      WrReq  => u11_QValid);

  u14: entity work.EvtMux(a0)
    generic map(
      EnPar => true)
    port map(
      DF         => u13_Q,
      DValid     => u13_QValid,
      ECR        => ECR,
      Empty      => Empty,
      EndEvt     => EndEvt,
      Even_Odd_n => '0',
      ForceIdle  => ForceIdle,
      Full       => u15_Full,
      GTMode     => GTMode,
      MReset     => MReset,
      QF         => QF,
      QValid     => QValid,
      RClk       => SClk,
      RdReq      => RdReq);

  u15: entity work.Fifo512x32s(a0)
    port map(
      D      => QF,
      Empty  => u15_Empty,
      Full   => u15_Full,
      MReset => MReset,
      Q      => u15_Q,
      QValid => u15_QValid,
      RClk   => TxClk,
      RdReq  => u16_RdReq,
      WClk   => SClk,
      WrReq  => QValid);

  u16: entity work.TxDriver(a0)
    generic map(
      TxOn => true)
    port map(
      DF         => u15_Q,
      DValid     => u15_QValid,
      Empty      => u15_Empty,
      EnaTxCount => EnaTxCount,
      MReset     => MReset,
      QF         => TxData,
      QValid     => TxValid,
      RClk       => TxClk,
      RdReq      => u16_RdReq,
      TBusy      => ChBusy);

end architecture a0 ; -- of DataEmu

