--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.PrepaChunk
--! =project    FELIX_MROD
--! @modified   Mon May  3 19:03:35 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity PrepaChunk is
  port (
    Busy       : in     std_logic;
    CkEna100k  : in     std_logic;
    DF         : in     std_logic_vector(31 downto 0);
    DValid     : in     std_logic;
    Empty      : in     std_logic;
    EnaCh      : in     std_logic;
    MReset     : in     std_logic;
    MaxTimeOut : in     std_logic_vector(15 downto 0);
    QF         : out    std_logic_vector(32 downto 0);
    QValid     : out    std_logic;
    RClk       : in     std_logic;
    RdReq      : out    std_logic);
end entity PrepaChunk;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.PrepaChunk.a0
--! =project    FELIX_MROD
--! @modified   Mon May  3 19:03:35 2021
--!-----------------------------------------------------------------------------


architecture a0 of PrepaChunk is

  type states is (Idle, FirstW, CheckF, Fread, LastW, Pause);
  signal mstate : states;

  signal dcnt   : unsigned(7 downto 0);
  signal EnRead : std_logic;
  signal EValid : std_logic;                            -- pending valid word
  signal data   : std_logic_vector(32 downto 0);
  signal EnaTimeOut : std_logic;                        -- enable timeout
  signal timect : unsigned(15 downto 0);                -- timeout counter
  -- CkEna100k is clock 100kHz -> timeoutcnt 16bits -> max. 65000*10us=0.65s

  --from centralRouter_package.vhd : the lowest byte has K-character
  --do not set bit 31 in Kchar_eop (sets sob)
  --constant Kchar_sop    : std_logic_vector (7 downto 0) := "00111100"; -- K28.1
  --constant Kchar_eop    : std_logic_vector (7 downto 0) := "11011100"; -- K28.6
  --constant Kchar_sot    : std_logic_vector (7 downto 0) := "10011100"; -- K28.4
  constant K_SOP : std_logic_vector(32 downto 0) := '1' & x"1234003C";  --K28.1
  constant K_EOP : std_logic_vector(32 downto 0) := '1' & x"001234DC";  --K28.6
  constant K_SOT : std_logic_vector(32 downto 0) := '1' & x"0012349C";  --K28.4

begin

  EnaTimeOut <= '1' when (MaxTimeOut /= x"0000") else '0';   -- enable when MaxTimeOut is not 0x0000
  RdReq <= '1' when (EnRead = '1' and Empty = '0' and Busy = '0') else '0';
  QF <= data;

  pr0:
  process(RClk, MReset)
  begin
    if (MReset = '1') then
      EnRead <= '0';
      QValid <= '0';
      EValid <= '0';
      data   <= (others => '0');
      dcnt   <= x"00";
      timect <= (others => '0');
      mstate <= Idle;
    elsif (rising_edge(RClk)) then
      case mstate is
      when Idle =>
        QValid <= '0';
        EnRead <= '0';
        if (EnaCh = '1' and Empty = '0') then
          mstate <= FirstW;             -- start allowed if fifo not empty
        else
          mstate <= Idle;
        end if;
      when FirstW =>
        EnRead <= '0';                  -- do not read fifo
        timect <= (others => '0');      -- clear timeout counter
        QValid <= '1';                  -- set word valid
        data <= K_SOP;                  -- start of packet/chunk
        dcnt  <= x"00";                 -- send soc
        mstate <= CheckF;
      when CheckF =>
        EnRead <= '1';                  -- request data from fifo
        if (DValid = '1' or EValid = '1') then
          QValid <= '1';                -- "last" word valid
          data <= '0' & DF;             -- data from Fifo (last word)
          dcnt <= dcnt + x"01";         -- count words
        else
          QValid <= '0';
        end if;
        EValid <= '0';
        mstate <= Fread;
      when Fread =>
        if (DValid = '1') then
          QValid <= '1';                -- last word valid
          data <= '0' & DF;             -- data from Fifo (last word)
          dcnt <= dcnt + x"01";         -- count words
          timect <= (others => '0');    -- clear timeout counter
          if (dcnt = x"fd") then
            EnRead <= '0';              -- stop readinf fifo
            mstate <= LastW;            -- reached end of chunk
          else
            EnRead <= '1';              -- request data from fifo
            mstate <= Fread;            -- stay in read loop
          end if;
        else
          if (EnaTimeOut = '1' and CkEna100k = '1') then  -- if timeout enabled (100 kHz)
            timect <= timect + 1;                       -- increment timeout counter
          end if;
          if (timect = unsigned(MaxTimeOut) or EnaCh = '0') then  -- if timeout reached or channel disabled
            EnRead <= '0';              -- do not read fifo
            dcnt <= dcnt + x"01";       -- count words
            QValid <= '1';              -- set word valid
            data <= K_SOT;              -- force timeout truncate of chunk and block
            mstate <= Pause;            -- force delay of 10 us before reading new data
          else
            EnRead <= '1';              -- request data from fifo
            QValid <= '0';              -- nothing available
            mstate <= Fread;            -- stay in read loop
          end if;
        end if;
      when LastW =>
        EnRead <= '0';                  -- do not read fifo
        dcnt <= dcnt + x"01";           -- should become x"ff"
        QValid <= '1';                  -- set word valid
        data <= K_EOP;                  -- end of packet/chunk
        if (DValid = '1') then
          EValid <= '1';                -- one pending word
          mstate <= FirstW;             -- do next chunk
        else
          EValid <= '0';
          mstate <= Idle;               -- start over ...
        end if;
      when Pause =>
        EnRead <= '0';
        QValid <= '0';                  -- nothing available
        if (DValid = '1') then
          EValid <= '1';                -- one pending word
        end if;
        --if (EnaTimeOut = '1' and CkEna100k = '1') then  -- if timeout enabled
          if (DValid = '1' or EValid = '1') then
            mstate <= FirstW;           -- restart, but one pending word
          else
            mstate <= Idle;             -- restart, but wait for new data
          end if;
        --else
        --  mstate <= Pause;              -- delay 10 us (to allow timeout chunk completion)
        --end if;
      when others =>
        mstate <= Idle;                 -- start over ...
      end case;
    end if;
  end process;

end architecture a0 ; -- of PrepaChunk

