--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.CSMHandler
--! =project    FELIX_MROD
--! @modified   Sat May 15 11:07:33 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity CSMHandler is
  generic(
    NUMCH      : integer := 2;
    W_ENDPOINT : integer := 0); -- select Wupper_Endpoint partition
  port (
    CSMMonitor        : out    regs_csm_monitor;
    ChBusy            : in     std_logic_vector(NUMCH-1 downto 0);
    ChData            : out    slv33_array(0 to NUMCH-1);
    ChValid           : out    std_logic_vector(NUMCH-1 downto 0);
    RxClk             : in     std_logic_vector(NUMCH-1 downto 0);
    RxData            : in     slv32_array(0 to NUMCH-1);
    RxValid           : in     std_logic_vector(NUMCH-1 downto 0);
    TTC_out           : in     std_logic_vector(15 downto 0);
    TxClk             : in     std_logic_vector(NUMCH-1 downto 0);
    TxData            : out    slv33_array(0 to NUMCH-1);
    TxValid           : out    std_logic_vector(NUMCH-1 downto 0);
    clk40             : in     std_logic;
    clk50             : in     std_logic;
    clk80             : in     std_logic;
    fhFifoD           : in     std_logic_vector(31 downto 0);
    fhFifoEmpty       : in     std_logic;
    fhFifoRE          : out    std_logic;
    fhFifoValid       : in     std_logic;
    prmap_app_control : in     register_map_control_type;
    sys_reset_n       : in     std_logic);
end entity CSMHandler;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.CSMHandler.a0
--! =project    FELIX_MROD
--! @modified   Sat May 15 11:07:33 2021
--!-----------------------------------------------------------------------------

architecture a0 of CSMHandler is

  signal MReset      : std_logic;
  signal FHData      : std_logic_vector(31 downto 0);
  signal Trigger     : std_logic;
  signal ECR         : std_logic;
  signal GTMode      : std_logic_vector(2 downto 0);
  signal FHDValid    : std_logic;
  signal EnaCSMch    : std_logic_vector(NUMCH-1 downto 0);
  signal EnaReadHost : std_logic;
  signal ClearCh     : std_logic_vector(NUMCH-1 downto 0);
  signal SetHPTDC    : std_logic_vector(NUMCH-1 downto 0);
  signal FHDEna      : std_logic_vector(NUMCH-1 downto 0);
  signal Empty       : std_logic_vector(NUMCH-1 downto 0);
  signal Full        : std_logic_vector(NUMCH-1 downto 0);
  signal EmptySupp   : std_logic_vector(NUMCH-1 downto 0);
  signal EnaPassAll  : std_logic;
  signal MaxTimeOut  : std_logic_vector(15 downto 0);
  signal CkEna100k   : std_logic;
  signal EnaTxCount  : std_logic;

begin
  --MakeBlocks
  --DataEmulator

  u1: for ch in 0 to NUMCH-1 generate
  begin

      u1: entity work.MakeBlocks(a0)
        generic map(
          chid => ch)
        port map(
          Busy       => ChBusy(ch),
          CkEna100k  => CkEna100k,
          Empty      => Empty(ch),
          EmptySupp  => EmptySupp(ch),
          EnaCh      => EnaCSMch(ch),
          EnaPassAll => EnaPassAll,
          Full       => Full(ch),
          MReset     => ClearCh(ch),
          MaxTimeOut => MaxTimeOut,
          QF         => ChData(ch),
          QValid     => ChValid(ch),
          RxClk      => RxClk(ch),
          RxData     => RxData(ch),
          RxValid    => RxValid(ch),
          SetHPTDC   => SetHPTDC(ch),
          clk80      => clk80);

  end generate u1;

  u2: for ch in 0 to NUMCH-1 generate
  begin

      u1: entity work.DataEmu(a0)
        generic map(
          chid => ch)
        port map(
          ChBusy     => ChBusy(ch),
          ECR        => ECR,
          EnaTxCount => EnaTxCount,
          FHDEna     => FHDEna(ch),
          FHDValid   => FHDValid,
          FHData     => FHData,
          GTMode     => GTMode,
          MReset     => ClearCh(ch),
          SClk       => clk80,
          Trigger    => Trigger,
          TxClk      => TxClk(ch),
          TxData     => TxData(ch),
          TxValid    => TxValid(ch));

  end generate u2;

  u3: entity work.ReadFHFifo(a0)
    port map(
      Clk         => clk80,
      EnaReadHost => EnaReadHost,
      FD          => fhFifoD,
      FEmpty      => fhFifoEmpty,
      FHDValid    => FHDValid,
      FHData      => FHData,
      FREna       => fhFifoRE,
      FValid      => fhFifoValid,
      MReset      => MReset);

  u4: entity work.SyncTTC(a0)
    port map(
      ECR     => ECR,
      MReset  => MReset,
      TTC_out => TTC_out,
      Trigger => Trigger,
      clk40   => clk40,
      clk80   => clk80);

  u5: entity work.GetControls(a0)
    generic map(
      NUMCH      => NUMCH,
      W_ENDPOINT => W_ENDPOINT)
    port map(
      CSMMonitor        => CSMMonitor,
      CkEna100k         => CkEna100k,
      ClearCh           => ClearCh,
      Empty             => Empty,
      EmptySupp         => EmptySupp,
      EnaCSMFake        => open,
      EnaCSMch          => EnaCSMch,
      EnaPassAll        => EnaPassAll,
      EnaReadHost       => EnaReadHost,
      EnaTxCount        => EnaTxCount,
      FHDEna            => FHDEna,
      Full              => Full,
      GTMode            => GTMode,
      MReset            => MReset,
      MaxTimeOut        => MaxTimeOut,
      SetHPTDC          => SetHPTDC,
      clk50             => clk50,
      clk80             => clk80,
      prmap_app_control => prmap_app_control,
      sys_reset_n       => sys_reset_n);

end architecture a0 ; -- of CSMHandler

