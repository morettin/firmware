--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.TXCVR.vhd
--! project     FELIX_MROD
--! @modified   Tue Jun 22 17:23:45 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.pcie_package.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity TransceiverGen1CH is
  generic(
    W_ENDPOINT : integer := 0;
    NUMCH      : integer := 2);
  port (
    EnChan                              : in   std_logic_vector(NUMCH-1 downto 0);
    EnManSlide                          : in   std_logic;
    MReset                              : in   std_logic;
    QX_GTREFCLK_N                       : in   std_logic_vector(4 downto 0);
    QX_GTREFCLK_P                       : in   std_logic_vector(4 downto 0);
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    RX_CHxAlignBsy                      : out  std_logic_vector(NUMCH-1 downto 0);
    RX_CHxRecData                       : out  std_logic_vector(NUMCH-1 downto 0);
    RX_CHxReset                         : in   std_logic_vector(NUMCH-1 downto 0);
    RX_CHxRecIdles                      : out  std_logic_vector(NUMCH-1 downto 0);
    RxClk                               : out  std_logic_vector(NUMCH-1 downto 0);
    RxData                              : out  slv32_array(0 to NUMCH-1);
    RxValid                             : out  std_logic_vector(NUMCH-1 downto 0);
    ------------------ Transmit Ports - TX Data Path interface -----------------
    SlideMax                            : in   std_logic_vector(7 downto 0);
    SlideWait                           : in   std_logic_vector(7 downto 0);
    FrameSize                           : in   std_logic_vector(7 downto 0);
    TRXloopback                         : in   std_logic_vector(NUMCH-1 downto 0);
    TX_CHxLocked                        : out  std_logic_vector(NUMCH-1 downto 0);
    TX_CHxReset                         : in   std_logic_vector(NUMCH-1 downto 0);
    TXCVR_ResetAll                      : in   std_logic_vector(NUMCH-1 downto 0);
    TxClk                               : out  std_logic_vector(NUMCH-1 downto 0);
    TxData                              : in   slv33_array(0 to NUMCH-1);
    TxValid                             : in   std_logic_vector(NUMCH-1 downto 0);
    ------------------ Transmit Ports - pattern Generator Ports ----------------
    clk50                               : in   std_logic;
    gtrxn_in                            : in   std_logic_vector(NUMCH-1 downto 0);
    gtrxp_in                            : in   std_logic_vector(NUMCH-1 downto 0);
    gttxn_out                           : out  std_logic_vector(NUMCH-1 downto 0);
    gttxp_out                           : out  std_logic_vector(NUMCH-1 downto 0);
    ------------------ 40 MHz system (DRP) clk
    prmap_app_control                   : in   register_map_control_type;
    sysclk_in                           : in   std_logic
    );

end entity TransceiverGen1CH;

architecture a0 of TransceiverGen1CH is

type slv00_array  is array (natural range <>) of std_logic_vector(0 downto 0);
type slv03_array  is array (natural range <>) of std_logic_vector(2 downto 0);
--type slv08_array  is array (natural range <>) of std_logic_vector(7 downto 0);
type usgd8_array  is array (natural range <>) of unsigned(7 downto 0);

COMPONENT MRODtxcvr_1ch_core
  PORT (
    gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    drpclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gthrxn_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gthrxp_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk0_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    loopback_in : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    rx8b10ben_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxcommadeten_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxmcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxpcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxslide_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxusrclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxusrclk2_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    tx8b10ben_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    txctrl0_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    txctrl1_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    txctrl2_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    txusrclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    txusrclk2_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gthtxn_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gthtxp_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxbyteisaligned_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxbyterealign_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxcommadet_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxctrl0_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    rxctrl1_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    rxctrl2_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxctrl3_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxoutclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    txoutclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;

COMPONENT FSM_Align
  PORT (
    clk50_ila     : IN STD_LOGIC;
    rxusrclk      : IN STD_LOGIC;
    TCVRall_rst   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxctrl0_out   : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    rxctrl1_out   : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    rxctrl2_out   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxctrl3_out   : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    EnManslide    : IN STD_LOGIC;
    rxslide_VIO   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    SlideWait     : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    FrameSize     : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxcommadet_out: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    RX_CHxAlignBsy: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    RX_CHxRecData : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    RX_CHxRecIdles: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxslide_toGTH : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;

COMPONENT ila_gth
  PORT (
    clk : IN STD_LOGIC;
    probe0 : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
    probe1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    probe3 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe4 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    probe5 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe6 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe7 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe10 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe11 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    probe12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    probe13 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;

signal MReset_buf                                    : std_logic_vector(NUMCH-1 downto 0);
signal loopback_in_sA, loopback_in_sB                : slv03_array(NUMCH-1 downto 0);
signal rxusrclk_sA, rxusrclk_sB                      : std_logic_vector(NUMCH-1 downto 0);
signal rxoutclk_sA, rxoutclk_sB                      : std_logic_vector(NUMCH-1 downto 0);
signal txusrclk_sA, txusrclk_sB                      : std_logic_vector(NUMCH-1 downto 0);
signal txoutclk_sA, txoutclk_sB                      : std_logic_vector(NUMCH-1 downto 0);
signal rxdata_out_sA, rxdata_out_sB                  : slv32_array(NUMCH-1 downto 0);
signal rxdata_out_sA_d0, rxdata_out_sB_d0            : slv32_array(NUMCH-1 downto 0);
signal txdata_in_bufA, txdata_in_bufB                : slv32_array(NUMCH-1 downto 0);
signal gtrxp_in_sA, gtrxn_in_sA                      : std_logic_vector(NUMCH-1 downto 0);
signal gttxp_out_sA, gttxn_out_sA                    : std_logic_vector(NUMCH-1 downto 0);
signal gtrxp_in_sB, gtrxn_in_sB                      : std_logic_vector(NUMCH-1 downto 0);
signal gttxp_out_sB, gttxn_out_sB                    : std_logic_vector(NUMCH-1 downto 0);
signal txctrl2_in_sA, txctrl2_in_sB                  : slv08_array(NUMCH-1 downto 0);
signal rxctrl2_out                                   : slv08_array(NUMCH-1 downto 0);
signal rxctrl3_out                                   : slv08_array(NUMCH-1 downto 0);
signal rxctrl0_out                                   : slv16_array(NUMCH-1 downto 0);
signal rxctrl1_out                                   : slv16_array(NUMCH-1 downto 0);
signal rxcharisk_out                                 : slv04_array(NUMCH-1 downto 0);
signal rxbyteisaligned_out_sA,rxbyteisaligned_out_sB : std_logic_vector(NUMCH-1 downto 0);
signal rxbyterealign_out_sA,rxbyterealign_out_sB     : std_logic_vector(NUMCH-1 downto 0);
signal rxcommadet_out_sA,rxcommadet_out_sB           : std_logic_vector(NUMCH-1 downto 0);
signal rxmcommaalignen_in_sA, rxmcommaalignen_in_sB  : std_logic_vector(NUMCH-1 downto 0);
signal rxpcommaalignen_in_sA, rxpcommaalignen_in_sB  : std_logic_vector(NUMCH-1 downto 0);
signal rxslide_in_d0, rxslide_in_d1                  : std_logic_vector(NUMCH-1 downto 0);
signal rxslide_in_d2, rxslide_in_d3                  : std_logic_vector(NUMCH-1 downto 0);
signal rxslide_VIO                                   : std_logic_vector(NUMCH-1 downto 0);

--Monitor signals (connect to VIO)
signal rxpmaresetdone_out_sA, rxpmaresetdone_out_sB  : std_logic_vector(NUMCH-1 downto 0);
signal txpmaresetdone_out_sA, txpmaresetdone_out_sB  : std_logic_vector(NUMCH-1 downto 0);
signal gtpowergood_out_sA, gtpowergood_out_sB        : std_logic_vector(NUMCH-1 downto 0);
signal gtwiz_reset_rx_done_out_sA                    : std_logic_vector(NUMCH-1 downto 0);
signal gtwiz_reset_rx_done_out_sB                    : std_logic_vector(NUMCH-1 downto 0);
signal gtwiz_reset_tx_done_out_sA                    : std_logic_vector(NUMCH-1 downto 0);
signal gtwiz_reset_tx_done_out_sB                    : std_logic_vector(NUMCH-1 downto 0);
signal gtwiz_reset_rx_cdr_stable_out_sA              : std_logic_vector(NUMCH-1 downto 0);
signal gtwiz_reset_rx_cdr_stable_out_sB              : std_logic_vector(NUMCH-1 downto 0);

--Resets, connected to VIO
signal TCVRall_rst                                   : std_logic_vector(NUMCH-1 downto 0);
signal TCVRrstFrmCnt                                 : std_logic_vector(NUMCH-1 downto 0);
signal RXdatapath_rst                                : std_logic_vector(NUMCH-1 downto 0);
signal RXdatapath_rst_sA, RXdatapath_rst_sB          : std_logic_vector(NUMCH-1 downto 0);
signal RXplldata_rst                                 : std_logic_vector(NUMCH-1 downto 0);
signal TXplldata_rst                                 : std_logic_vector(NUMCH-1 downto 0);
signal TXplldata_rst_sA, TXplldata_rst_sB            : std_logic_vector(NUMCH-1 downto 0);
signal TXdatapath_rst                                : std_logic_vector(NUMCH-1 downto 0);
signal TXdatapath_rst_sA, TXdatapath_rst_sB          : std_logic_vector(NUMCH-1 downto 0);
signal rxslide_toGTH, rxslide_toGTH_d1               : std_logic_vector(NUMCH-1 downto 0);
signal SlideCntILA                                   : usgd8_array (NUMCH-1 downto 0);
signal SlideCnt                                      : slv08_array(NUMCH-1 downto 0);
signal SlideMax_loc                                  : usgd8_array (NUMCH-1 downto 0);
signal SlideMax_act                                  : usgd8_array (NUMCH-1 downto 0);

signal gtrefclk0_in_Q2, gtrefclk0_in_Q4, gtrefclk0_in_Q5 : std_logic_vector(NUMCH-1 downto 0);
signal gtrefclk0_in_Q6, gtrefclk0_in_Q8              : std_logic_vector(NUMCH-1 downto 0);
signal cesyncRX_s, clrsyncRX_s                       : std_logic_vector(NUMCH-1 downto 0);
signal cesyncTX_s, clrsyncTX_s                       : std_logic_vector(NUMCH-1 downto 0);
signal gtrefclk_sel                                  : std_logic_vector(NUMCH-1 downto 0);
signal gtrefclk_Q2, gtrefclk_Q4, gtrefclk_Q5         : std_logic;
signal gtrefclk_Q6, gtrefclk_Q8                      : std_logic;
signal gtrefclk_sA, gtrefclk_sB                      : slv00_array(NUMCH-1 downto 0);
signal clk50_ila                                     : std_logic_vector(NUMCH-1 downto 0);
signal RX_CHxRecData_s                               : std_logic_vector(NUMCH-1 downto 0);

--Uncomment and add them to debug core after synthesis
attribute mark_debug : string;
attribute keep : string;
attribute mark_debug of rxdata_out_sA       : signal is "true";
attribute mark_debug of txdata_in_bufA      : signal is "true";
attribute mark_debug of rxdata_out_sB       : signal is "true";
attribute mark_debug of txdata_in_bufB      : signal is "true";
attribute mark_debug of rxctrl0_out         : signal is "true";
attribute mark_debug of TCVRrstFrmCnt       : signal is "true";
attribute mark_debug of SlideCnt            : signal is "true";
attribute mark_debug of rxcommadet_out_sA   : signal is "true";
attribute mark_debug of rxcommadet_out_sB   : signal is "true";
attribute mark_debug of rxslide_toGTH       : signal is "true";

begin

  rxmcommaalignen_in_sA <= (others=> '0');
  rxmcommaalignen_in_sB <= (others=> '0');
  rxpcommaalignen_in_sA <= (others=> '0');
  rxpcommaalignen_in_sB <= (others=> '0');

  clk50_ila <= (others => clk50);

  -------------------------------------
  -- generate once per PCIE endpoint --
  -------------------------------------

  g0_ref: if (W_ENDPOINT = 0) generate
    --
    -- Connections GTREF clk Q2 Quad 126, 127 and 128 --
    -- Channel 0 to 11
    IBUFDS_GTE3_Q2: IBUFDS_GTE3
      generic map (
       REFCLK_EN_TX_PATH => '0', -- Refer to Transceiver User Guide
       REFCLK_HROW_CK_SEL => "00",
       REFCLK_ICNTL_RX => "00"
      )
      port map (
       O => gtrefclk_Q2,      -- 1-bit output: Refer to Transceiver User Guide
       ODIV2 => open,
       CEB => '0',
       I => QX_GTREFCLK_P(0), -- 1-bit input
       IB => QX_GTREFCLK_N(0)
       );
    --
    gtrefclk0_in_Q2 <= (others => gtrefclk_Q2);
    --
    -- Connections GTREF clk Q5 Quad 228 --
    -- Channel 12 to 15
    IBUFDS_GTE3_Q5: IBUFDS_GTE3
      generic map (
       REFCLK_EN_TX_PATH => '0',  -- Refer to Transceiver User Guide
       REFCLK_HROW_CK_SEL => "00",
       REFCLK_ICNTL_RX => "00"
      )
      port map (
       O => gtrefclk_Q5,      -- 1-bit output: Refer to Transceiver User Guide
       ODIV2 => open,
       CEB => '0',
       I => QX_GTREFCLK_P(2), -- 1-bit input
       IB => QX_GTREFCLK_N(2)
       );
    --
    gtrefclk0_in_Q5 <= (others => gtrefclk_Q5);
    --
    -- Connections GTREF clk Q6 Quad 224 and 225 --
    -- channel 16-23
    IBUFDS_GTE3_Q6: IBUFDS_GTE3
      generic map (
       REFCLK_EN_TX_PATH => '0',  -- Refer to Transceiver User Guide
       REFCLK_HROW_CK_SEL => "00",
       REFCLK_ICNTL_RX => "00"
      )
      port map (
       O => gtrefclk_Q6,      -- 1-bit output: Refer to Transceiver User Guide
       ODIV2 => open,
       CEB => '0',
       I => QX_GTREFCLK_P(4), -- 1-bit input
       IB => QX_GTREFCLK_N(4)
       );
    --
    gtrefclk0_in_Q6 <= (others => gtrefclk_Q6);
    --
  end generate g0_ref;

  -- generate transceiver channels PCIE endpoint 0 --

  g0_all: for i in 0 to 23 generate
    g0_ch: if (W_ENDPOINT = 0 and i < NUMCH) generate
      --
      g0_r2: if (i < 12) generate
        gtrefclk_sA(i) <= gtrefclk0_in_Q2(i downto i);  -- select clock source Q2
      end generate g0_r2;
      g0_r5: if (i >= 12 and i < 16) generate
        gtrefclk_sA(i) <= gtrefclk0_in_Q5(i downto i);  -- select clock source Q5
      end generate g0_r5;
      g0_r6: if (i >= 16 and i < 24) generate
        gtrefclk_sA(i) <= gtrefclk0_in_Q6(i downto i);  -- select clock source Q6
      end generate g0_r6;
      --
      --------------------------------
      -- generate once per channel  --
      -- TXCVR Core and clk buffers --
      -- EP0 Channel 0 to 23        --
      --------------------------------
      --
      -- connect input and output
      gttxp_out(i)  <= gttxp_out_sA(i);
      gttxn_out(i)  <= gttxn_out_sA(i);
      gtrxp_in_sA(i) <= gtrxp_in(i);
      gtrxn_in_sA(i) <= gtrxn_in(i);
      --
      -- RX data, clk and charisk
      rxcharisk_out(i) <= rxctrl0_out(i)(3 downto 0);
      RxData(i) <= rxdata_out_sA(i);
      RxValid(i) <= '1' when ((rxcharisk_out(i) = "0000") and (RX_CHxRecData_s(i downto i) = "1")) else '0';
      RX_CHxRecData(i downto i) <= RX_CHxRecData_s(i downto i);
      RxClk(i) <= rxusrclk_sA(i);
      --
      -- TX data, clk and charisk
      txdata_in_bufA(i) <= TxData(i)(31 downto 0);
      txctrl2_in_sA(i) <= (others => TxData(i)(32));
      TxClk(i) <= txusrclk_sA(i);
      TX_CHxLocked(i downto i) <= gtwiz_reset_tx_done_out_sA(i downto i);
      --
      -- LOOPBACK
      loopback_in_sA(i) <= '0' & TRXloopback(i) & '0';

      -----------------------------------------
      -- RESET AND CONTROL --------------------
      -----------------------------------------
      MReset_buf(i) <= MReset;
      TCVRall_rst(i downto i) <= TXCVR_ResetAll(i downto i) or MReset_buf(i downto i); --or TCVRrstFrmCnt(i downto i);
      RXplldata_rst(i) <= TCVRall_rst(i) or RX_CHxReset(i) or TCVRrstFrmCnt(i); -- from RM or FSM or general rst
      TXplldata_rst(i downto i) <= TCVRall_rst(i downto i) or TX_CHxReset(i downto i); -- from general rst or RM

      pr0:
      process (clk50_ila(i), TCVRall_rst(i downto i))
      begin
        if ( (TCVRall_rst(i downto i) = "1") ) then
          SlideMax_act(i) <= (others => '0');
          TCVRrstFrmCnt(i) <= '0';
          rxslide_toGTH_d1(i) <= '0';
          SlideCntILA(i) <= (others => '0');
          SlideMax_loc(i) <= (others => '0');
        elsif (rising_edge(clk50_ila(i))) then
          rxslide_toGTH_d1(i) <= rxslide_toGTH(i);
          SlideMax_loc(i) <= unsigned(SlideMax);
          SlideCntILA(i) <= unsigned(SlideMax_loc(i)) - SlideMax_act(i);
          if (SlideMax_act(i) = x"00") then
            TCVRrstFrmCnt(i) <= '1';
            SlideMax_act(i) <= unsigned(SlideMax_loc(i));
            SlideCntILA(i) <= (others => '0');
          elsif (rxslide_toGTH(i) = '1') and (rxslide_toGTH_d1(i) = '0') then
            SlideMax_act(i) <= SlideMax_act(i) - 1;
            TCVRrstFrmCnt(i) <= '0';
          else
            SlideMax_act(i) <= SlideMax_act(i);
            TCVRrstFrmCnt(i) <= '0';
          end if;
        end if;
      end process;

      SlideCnt(i) <= std_logic_vector(SlideCntILA(i));
      --
      -- Rx clk buffers
      BUFG_GT_RXinst: BUFG_GT
      port map (
        O         => rxusrclk_sA(i),
        CE        => cesyncRX_s(i),
        CEMASK    => '1',
        CLR       => clrsyncRX_s(i),
        CLRMASK   => '1',
        DIV       => "000",
        I         => rxoutclk_sA(i)
      );
      -- Rx sync
      BUFG_GT_SYNC_RXinst: BUFG_GT_SYNC
      port map (
        CESYNC    => cesyncRX_s(i),
        CLRSYNC   => clrsyncRX_s(i),
        CLK       => rxoutclk_sA(i),
        CE        => '1',
        CLR       => '0'
      );
      --
      -- Tx clk buffers
      BUFG_GT_TXinst: BUFG_GT
      port map (
        O         => txusrclk_sA(i),
        CE        => cesyncTX_s(i),
        CEMASK    => '1',
        CLR       => clrsyncTX_s(i),
        CLRMASK   => '1',
        DIV       => "000",
        I         => txoutclk_sA(i)
      );
      -- Tx sync
      BUFG_GT_SYNC_TXinst: BUFG_GT_SYNC
      port map (
        CESYNC    => cesyncTX_s(i),
        CLRSYNC   => clrsyncTX_s(i),
        CLK       => txoutclk_sA(i),
        CE        => '1',
        CLR       => '0'
      );
      --
      -- Align the bytes to correct order in word
      u0: FSM_Align
      PORT MAP (
        clk50_ila   => clk50_ila(i),
        rxusrclk    => rxusrclk_sA(i),
        TCVRall_rst => TCVRall_rst(i downto i),
        rxctrl0_out => rxctrl0_out(i),
        rxctrl1_out => rxctrl1_out(i),
        rxctrl2_out => rxctrl2_out(i),
        rxctrl3_out => rxctrl3_out(i),
        EnManslide  => EnManSlide,
        rxslide_VIO => (others => '0'),
        SlideWait   => SlideWait,
        FrameSize   => FrameSize,
        RX_CHxAlignBsy => RX_CHxAlignBsy(i downto i),
        RX_CHxRecData  => RX_CHxRecData_s(i downto i),
        RX_CHxRecIdles => RX_CHxRecIdles(i downto i),
        rxslide_toGTH  => rxslide_toGTH(i downto i),
        rxcommadet_out => rxcommadet_out_sA(i downto i)
      );
      --
      -- instantiate the transceiver core
      u1: MRODtxcvr_1ch_core
      PORT MAP (
        gtwiz_userclk_tx_active_in => (others => '1'),
        gtwiz_userclk_rx_active_in => (others => '1'),
        gtwiz_reset_clk_freerun_in(0) => sysclk_in,
        gtwiz_reset_all_in => TCVRall_rst(i downto i),
        gtwiz_reset_tx_pll_and_datapath_in => TXplldata_rst(i downto i),
        gtwiz_reset_tx_datapath_in => TCVRall_rst(i downto i),
        gtwiz_reset_rx_pll_and_datapath_in => RXplldata_rst(i downto i), -- or TCVRrstFrmCnt(i downto i), -- TCVRall_rst(i downto i) or RX_CHxReset(i downto i);
        gtwiz_reset_rx_datapath_in => TCVRall_rst(i downto i),
        gtwiz_reset_rx_cdr_stable_out => gtwiz_reset_rx_cdr_stable_out_sA(i downto i),
        gtwiz_reset_tx_done_out => gtwiz_reset_tx_done_out_sA(i downto i),
        gtwiz_reset_rx_done_out => gtwiz_reset_rx_done_out_sA(i downto i),
        gtwiz_userdata_tx_in => txdata_in_bufA(i),
        gtwiz_userdata_rx_out => rxdata_out_sA(i),
        drpclk_in(0) => sysclk_in,
        gthrxn_in => gtrxn_in_sA(i downto i),
        gthrxp_in => gtrxp_in_sA(i downto i),
        gtrefclk0_in => gtrefclk_sA(i),
        loopback_in => loopback_in_sA(i),        --"010" near end pma loopback
        rx8b10ben_in => (others => '1'),
        rxcommadeten_in => (others => '1'),
        rxmcommaalignen_in => rxmcommaalignen_in_sA(i downto i),
        rxpcommaalignen_in => rxpcommaalignen_in_sA(i downto i),
        rxslide_in => rxslide_toGTH(i downto i),
        rxusrclk_in => rxusrclk_sA(i downto i),
        rxusrclk2_in => rxusrclk_sA(i downto i),
        tx8b10ben_in => (others => '1'),
        txctrl0_in => (others => '0'),
        txctrl1_in => (others => '0'),
        txctrl2_in => txctrl2_in_sA(i),
        txusrclk_in => txusrclk_sA(i downto i),
        txusrclk2_in => txusrclk_sA(i downto i),
        gthtxn_out => gttxn_out_sA(i downto i),
        gthtxp_out => gttxp_out_sA(i downto i),
        gtpowergood_out => gtpowergood_out_sA(i downto i),
        rxbyteisaligned_out => rxbyteisaligned_out_sA(i downto i),
        rxbyterealign_out => rxbyterealign_out_sA(i downto i),
        rxcommadet_out => rxcommadet_out_sA(i downto i),
        rxctrl0_out => rxctrl0_out(i),
        rxctrl1_out => rxctrl1_out(i),
        rxctrl2_out => rxctrl2_out(i),
        rxctrl3_out => rxctrl3_out(i),
        rxoutclk_out => rxoutclk_sA(i downto i),
        rxpmaresetdone_out => rxpmaresetdone_out_sA(i downto i),
        txoutclk_out => txoutclk_sA(i downto i),
        txpmaresetdone_out => txpmaresetdone_out_sA(i downto i),
        txprgdivresetdone_out => open
      );
      --
      g0_ila: if (i = 10 or i = 14 or i = 20) generate
       -- instantiate the ila for some channels
       ila_A: ila_gth
       PORT MAP (
         clk => rxusrclk_sA(i), -- 1
         probe0 => TxData(i),   -- 33
         probe1 => txctrl2_in_sA(i), -- 8
         probe2 => rxdata_out_sA(i), -- 32
         probe3 => rxctrl0_out(i), -- 16
         probe4 => rxctrl1_out(i), -- 16
         probe5 => rxctrl2_out(i), -- 8
         probe6 => rxctrl3_out(i), -- 8
         probe7 => rxbyteisaligned_out_sA(i downto i),
         probe8 => rxbyterealign_out_sA(i downto i),
         probe9 => rxcommadet_out_sA(i downto i),
         probe10 => rxslide_toGTH(i downto i),
         probe11 => std_logic_vector(SlideCntILA(i)),
         probe12 => gtwiz_reset_tx_done_out_sA(i downto i),
         probe13 => TCVRall_rst(i downto i)
       );
      end generate g0_ila;
    --
    end generate g0_ch;
  end generate g0_all;

  --------------------------------------------------------------------

  -------------------------------------
  -- generate once per PCIE endpoint --
  -------------------------------------

  g1_ref: if (W_ENDPOINT = 1) generate
    --
    -- Connections GTREF clk Q8 Quad 131, 132 and 133 --
    -- Channel 0 to 11
    IBUFDS_GTE3_Q8: IBUFDS_GTE3
      generic map (
        REFCLK_EN_TX_PATH => '0',  -- Refer to Transceiver User Guide
        REFCLK_HROW_CK_SEL => "00",
        REFCLK_ICNTL_RX => "00"
      )
      port map (
        O => gtrefclk_Q8,      -- 1-bit output: Refer to Transceiver User Guide
        ODIV2 => open,
        CEB => '0',
        I => QX_GTREFCLK_P(1), -- 1-bit input
        IB => QX_GTREFCLK_N(1)
      );
    --
    gtrefclk0_in_Q8 <= (others => gtrefclk_Q8);
    --
    -- Connections GTREF clk Q4 Quad 231, 232 and 233 --
    -- channel 12 to 23
    IBUFDS_GTE3_Q4: IBUFDS_GTE3
      generic map (
        REFCLK_EN_TX_PATH => '0',  -- Refer to Transceiver User Guide
        REFCLK_HROW_CK_SEL => "00",
        REFCLK_ICNTL_RX => "00"
      )
      port map (
        O => gtrefclk_Q4,      -- 1-bit output: Refer to Transceiver User Guide
        ODIV2 => open,
        CEB => '0',
        I => QX_GTREFCLK_P(3), -- 1-bit input
        IB => QX_GTREFCLK_N(3)
      );
    --
    gtrefclk0_in_Q4 <= (others => gtrefclk_Q4);
    --
  end generate g1_ref;

  -- generate transceiver channels PCIE endpoint 1 --

  g1_all: for i in 0 to 23 generate
    g1_ch: if (W_ENDPOINT = 1 and i < NUMCH) generate
      --
      g1_r8: if (i < 12) generate
        gtrefclk_sB(i) <= gtrefclk0_in_Q8(i downto i);  -- select clock source Q8
      end generate g1_r8;
      g1_r4: if (i >= 12 and i < 24) generate
        gtrefclk_sB(i) <= gtrefclk0_in_Q4(i downto i);  -- select clock source Q4
      end generate g1_r4;
      --
      --------------------------------
      -- generate once per channel  --
      -- TXCVR Core and clk buffers --
      -- EP1 Channel 0 to 23        --
      --------------------------------
      --
      -- connect input and output
      gttxp_out(i)  <= gttxp_out_sB(i);
      gttxn_out(i)  <= gttxn_out_sB(i);
      gtrxp_in_sB(i) <= gtrxp_in(i);
      gtrxn_in_sB(i) <= gtrxn_in(i);
      --
      -- RX data, clk and charisk
      rxcharisk_out(i) <= rxctrl0_out(i)(3 downto 0);
      RxData(i) <= rxdata_out_sB(i);
      RxValid(i) <= '1' when ((rxcharisk_out(i) = "0000") and (RX_CHxRecData_s(i downto i) = "1")) else '0';
      RX_CHxRecData(i downto i) <= RX_CHxRecData_s(i downto i);
      RxClk(i) <= rxusrclk_sB(i);
      --
      -- TX data, clk and charisk
      txdata_in_bufB(i) <= TxData(i)(31 downto 0);
      txctrl2_in_sB(i) <= (others => TxData(i)(32));
      TxClk(i) <= txusrclk_sB(i);
      TX_CHxLocked(i downto i) <= gtwiz_reset_tx_done_out_sB(i downto i);
      --
      -- LOOPBACK
      loopback_in_sB(i) <= '0' & TRXloopback(i) & '0';

      -----------------------------------------
      -- RESET AND CONTROL --------------------
      -----------------------------------------
      MReset_buf(i) <= MReset;
      TCVRall_rst(i downto i) <= TXCVR_ResetAll(i downto i) or MReset_buf(i downto i); --or TCVRrstFrmCnt(i downto i);
      RXplldata_rst(i) <= TCVRall_rst(i) or RX_CHxReset(i) or TCVRrstFrmCnt(i); -- from RM or FSM or general rst
      TXplldata_rst(i downto i) <= TCVRall_rst(i downto i) or TX_CHxReset(i downto i); -- from general rst or RM

      pr0:
      process (clk50_ila(i), TCVRall_rst(i downto i))
      begin
        if ( (TCVRall_rst(i downto i) = "1") ) then
          SlideMax_act(i) <= (others => '0');
          TCVRrstFrmCnt(i) <= '0';
          rxslide_toGTH_d1(i) <= '0';
          SlideCntILA(i) <= (others => '0');
          SlideMax_loc(i) <= (others => '0');
        elsif (rising_edge(clk50_ila(i))) then
          rxslide_toGTH_d1(i) <= rxslide_toGTH(i);
          SlideMax_loc(i) <= unsigned(SlideMax);
          SlideCntILA(i) <= unsigned(SlideMax_loc(i)) - SlideMax_act(i);
          if (SlideMax_act(i) = x"00") then
            TCVRrstFrmCnt(i) <= '1';
            SlideMax_act(i) <= unsigned(SlideMax_loc(i));
            SlideCntILA(i) <= (others => '0');
          elsif (rxslide_toGTH(i) = '1') and (rxslide_toGTH_d1(i) = '0') then
            SlideMax_act(i) <= SlideMax_act(i) - 1;
            TCVRrstFrmCnt(i) <= '0';
          else
            SlideMax_act(i) <= SlideMax_act(i);
            TCVRrstFrmCnt(i) <= '0';
          end if;
        end if;
      end process;

      SlideCnt(i) <= std_logic_vector(SlideCntILA(i));
      --
      -- Rx clk buffers
      BUFG_GT_RXinst: BUFG_GT
      port map (
        O         => rxusrclk_sB(i),
        CE        => cesyncRX_s(i),
        CEMASK    => '1',
        CLR       => clrsyncRX_s(i),
        CLRMASK   => '1',
        DIV       => "000",
        I         => rxoutclk_sB(i)
      );
      -- Rx sync
      BUFG_GT_SYNC_RXinst: BUFG_GT_SYNC
      port map (
        CESYNC    => cesyncRX_s(i),
        CLRSYNC   => clrsyncRX_s(i),
        CLK       => rxoutclk_sB(i),
        CE        => '1',
        CLR       => '0'
      );
      --
      -- Tx clk buffers
      BUFG_GT_TXinst: BUFG_GT
      port map (
        O         => txusrclk_sB(i),
        CE        => cesyncTX_s(i),
        CEMASK    => '1',
        CLR       => clrsyncTX_s(i),
        CLRMASK   => '1',
        DIV       => "000",
        I         => txoutclk_sB(i)
      );
      -- Tx sync
      BUFG_GT_SYNC_TXinst: BUFG_GT_SYNC
      port map (
        CESYNC    => cesyncTX_s(i),
        CLRSYNC   => clrsyncTX_s(i),
        CLK       => txoutclk_sB(i),
        CE        => '1',
        CLR       => '0'
      );
      --
      -- Align bytes to correct order in word
      u0: FSM_Align
      PORT MAP (
        clk50_ila => clk50_ila(i),
        rxusrclk => rxusrclk_sB(i),
        TCVRall_rst => TCVRall_rst(i downto i),
        rxctrl0_out => rxctrl0_out(i),
        rxctrl1_out => rxctrl1_out(i),
        rxctrl2_out => rxctrl2_out(i),
        rxctrl3_out => rxctrl3_out(i),
        EnManslide  => EnManSlide,
        rxslide_VIO => (others => '0'),
        SlideWait   => SlideWait,
        FrameSize   => FrameSize,
        RX_CHxAlignBsy => RX_CHxAlignBsy(i downto i),
        RX_CHxRecData  => RX_CHxRecData_s(i downto i),
        RX_CHxRecIdles => RX_CHxRecIdles(i downto i),
        rxslide_toGTH  => rxslide_toGTH(i downto i),
        rxcommadet_out => rxcommadet_out_sB(i downto i)
      );
      --
      -- instantiate the transceiver core
      u1 : MRODtxcvr_1ch_core
      PORT MAP (
        gtwiz_userclk_tx_active_in => (others => '1'),
        gtwiz_userclk_rx_active_in => (others => '1'),
        gtwiz_reset_clk_freerun_in(0) => sysclk_in,
        gtwiz_reset_all_in => TCVRall_rst(i downto i),
        gtwiz_reset_tx_pll_and_datapath_in => TXplldata_rst(i downto i),
        gtwiz_reset_tx_datapath_in => TCVRall_rst(i downto i),
        gtwiz_reset_rx_pll_and_datapath_in => RXplldata_rst(i downto i), -- or TCVRrstFrmCnt(i downto i), -- TCVRall_rst(i downto i) or RX_CHxReset(i downto i);
        gtwiz_reset_rx_datapath_in => TCVRall_rst(i downto i),
        gtwiz_reset_rx_cdr_stable_out => gtwiz_reset_rx_cdr_stable_out_sB(i downto i),
        gtwiz_reset_tx_done_out => gtwiz_reset_tx_done_out_sB(i downto i),
        gtwiz_reset_rx_done_out => gtwiz_reset_rx_done_out_sB(i downto i),
        gtwiz_userdata_tx_in => txdata_in_bufB(i),
        gtwiz_userdata_rx_out => rxdata_out_sB(i),
        drpclk_in(0) => sysclk_in,
        gthrxn_in => gtrxn_in_sB(i downto i),
        gthrxp_in => gtrxp_in_sB(i downto i),
        gtrefclk0_in => gtrefclk_sB(i),
        loopback_in => loopback_in_sB(i),        -- "010" near end pma loopback
        rx8b10ben_in => (others => '1'),
        rxcommadeten_in => (others => '1'),
        rxmcommaalignen_in => rxmcommaalignen_in_sB(i downto i),
        rxpcommaalignen_in => rxpcommaalignen_in_sB(i downto i),
        rxslide_in => rxslide_toGTH(i downto i),
        rxusrclk_in => rxusrclk_sB(i downto i),
        rxusrclk2_in => rxusrclk_sB(i downto i),
        tx8b10ben_in => (others => '1'),
        txctrl0_in => (others => '0'),
        txctrl1_in => (others => '0'),
        txctrl2_in => txctrl2_in_sB(i),
        txusrclk_in => txusrclk_sB(i downto i),
        txusrclk2_in => txusrclk_sB(i downto i),
        gthtxn_out => gttxn_out_sB(i downto i),
        gthtxp_out => gttxp_out_sB(i downto i),
        gtpowergood_out => gtpowergood_out_sB(i downto i),
        rxbyteisaligned_out => rxbyteisaligned_out_sB(i downto i),
        rxbyterealign_out => rxbyterealign_out_sB(i downto i),
        rxcommadet_out => rxcommadet_out_sB(i downto i),
        rxctrl0_out => rxctrl0_out(i),
        rxctrl1_out => rxctrl1_out(i),
        rxctrl2_out => rxctrl2_out(i),
        rxctrl3_out => rxctrl3_out(i),
        rxoutclk_out => rxoutclk_sB(i downto i),
        rxpmaresetdone_out => rxpmaresetdone_out_sB(i downto i),
        txoutclk_out => txoutclk_sB(i downto i),
        txpmaresetdone_out => txpmaresetdone_out_sB(i downto i),
        txprgdivresetdone_out => open
      );
      --
      g1_ila: if (i = 10 or i = 20) generate
       -- instantiate the ila for some channels
       ila_B: ila_gth
       PORT MAP (
         clk => rxusrclk_sB(i), -- 1
         probe0 => TxData(i),   -- 33
         probe1 => txctrl2_in_sB(i), -- 8
         probe2 => rxdata_out_sB(i), -- 32
         probe3 => rxctrl0_out(i), -- 16
         probe4 => rxctrl1_out(i), -- 16
         probe5 => rxctrl2_out(i), -- 8
         probe6 => rxctrl3_out(i), -- 8
         probe7 => rxbyteisaligned_out_sB(i downto i),
         probe8 => rxbyterealign_out_sB(i downto i),
         probe9 => rxcommadet_out_sB(i downto i),
         probe10 => rxslide_toGTH(i downto i),
         probe11 => std_logic_vector(SlideCntILA(i)),
         probe12 => gtwiz_reset_tx_done_out_sB(i downto i),
         probe13 => TCVRall_rst(i downto i)
       );
      end generate g1_ila;
      --
    end generate g1_ch;
  end generate g1_all;

end architecture a0 ; -- of TransceiverGen1CH

