--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Rene
--!               Thei Wijnen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.CR_Wupper
--! =project    FELIX_MROD
--! @modified   Fri Apr 30 17:28:27 2021
--!-----------------------------------------------------------------------------

library ieee, work;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.felix_mrod_package.all;
    use work.centralRouter_package.all;
    use work.FELIX_gbt_package.all;
    use work.pcie_package.all;

entity CR_Wupper is
    generic(
        NUMBER_OF_INTERRUPTS           : integer := 8;
        NUMBER_OF_DESCRIPTORS          : integer := 8;
        GBT_NUM                        : integer := 4;
        FMCH_NUM                       : integer := 2;
        NUMCH                          : integer := 2;
        GBT_GENERATE_ALL_REGS          : boolean := false;
        EMU_GENERATE_REGS              : boolean := false;
        MROD_GENERATE_REGS             : boolean := true;
        CARD_TYPE                      : integer := 712;
        W_ENDPOINT                     : integer := 0;
        GENERATE_XOFF                  : boolean := true;
        STATIC_CENTRALROUTER           : boolean := true;
        CREnableFromHost               : boolean := true;
        toHostTimeoutBitn              : integer := 10;
        BLOCKSIZE                      : integer := 1024;
        CHUNK_TRAILER_32B              : boolean := false;
        SUPER_CHUNK_FACTOR             : integer := 1;
        wideMode                       : boolean := false;
        FIRMWARE_MODE                  : integer := 1;
        BUILD_DATETIME                 : std_logic_vector(39 downto 0) := x"0000FE71CE";
        GIT_HASH                       : std_logic_vector(159 downto 0) := x"0000000000000000000000000000000000000000";
        GIT_TAG                        : std_logic_vector(127 downto 0) := x"00000000000000000000000000000000";
        GIT_COMMIT_NUMBER              : integer := 0;
        COMMIT_DATETIME                : std_logic_vector(39 downto 0) := x"0000FE71CE";
        EnableFrHo_Egroup0Eproc2_HDLC  : boolean := false;
        EnableFrHo_Egroup0Eproc2_8b10b : boolean := false;
        EnableFrHo_Egroup0Eproc4_8b10b : boolean := false;
        EnableFrHo_Egroup0Eproc8_8b10b : boolean := false;
        EnableFrHo_Egroup1Eproc2_HDLC  : boolean := false;
        EnableFrHo_Egroup1Eproc2_8b10b : boolean := false;
        EnableFrHo_Egroup1Eproc4_8b10b : boolean := false;
        EnableFrHo_Egroup1Eproc8_8b10b : boolean := false;
        EnableFrHo_Egroup2Eproc2_HDLC  : boolean := false;
        EnableFrHo_Egroup2Eproc2_8b10b : boolean := false;
        EnableFrHo_Egroup2Eproc4_8b10b : boolean := false;
        EnableFrHo_Egroup2Eproc8_8b10b : boolean := false;
        EnableFrHo_Egroup3Eproc2_HDLC  : boolean := false;
        EnableFrHo_Egroup3Eproc2_8b10b : boolean := false;
        EnableFrHo_Egroup3Eproc4_8b10b : boolean := false;
        EnableFrHo_Egroup3Eproc8_8b10b : boolean := false;
        EnableFrHo_Egroup4Eproc2_HDLC  : boolean := false;
        EnableFrHo_Egroup4Eproc2_8b10b : boolean := false;
        EnableFrHo_Egroup4Eproc4_8b10b : boolean := false;
        EnableFrHo_Egroup4Eproc8_8b10b : boolean := false;
        ENABLE_XVC : boolean := false);
    port (
        BUSY_INTERRUPT    : in     std_logic;
        CRBusyOut         : out    std_logic_vector(NUMCH-1 downto 0);
        CR_FIFO_Busy      : out    std_logic;
        CSM0Monitor       : in     regs_csm_monitor;
        CSM1Monitor       : in     regs_csm_monitor;
        ChBusy            : out    std_logic_vector(NUMCH-1 downto 0);
        ChData            : in     slv33_array(0 to NUMCH-1);
        ChValid           : in     std_logic_vector(NUMCH-1 downto 0);
        MasterBusy        : in     std_logic;
        TTC_ToHost_Data   : in     TTC_ToHost_data_type;
        TTC_out           : in     std_logic_vector(15 downto 0);
        Trx0Monitor       : in     regs_trx_monitor;
        Trx1Monitor       : in     regs_trx_monitor;
        clk160            : in     std_logic;
        clk250            : in     std_logic;
        clk40             : in     std_logic;
        clk80             : in     std_logic;
        fhFifoD           : out    std_logic_vector(31 downto 0);
        fhFifoEmpty       : out    std_logic;
        fhFifoRE          : in     std_logic;
        fhFifoValid       : out    std_logic;
        lnk_up            : out    std_logic;
        pcie_DMA_Busy     : out    std_logic;
        pcie_appreg_clk   : out    std_logic;
        pcie_rxn          : in     std_logic_vector(7 downto 0);
        pcie_rxp          : in     std_logic_vector(7 downto 0);
        pcie_soft_reset   : out    std_logic;
        pcie_txn          : out    std_logic_vector(7 downto 0);
        pcie_txp          : out    std_logic_vector(7 downto 0);
        prmap_40_control  : out    register_map_control_type;
        prmap_app_control : out    register_map_control_type;
        prmap_board_info  : in     register_map_gen_board_info_type;
        prmap_hk_monitor  : in     register_map_hk_monitor_type;
        prmap_ttc_monitor : in     register_map_ttc_monitor_type;
        rst_hw            : in     std_logic;
        rst_soft_40       : out    std_logic;
        sys_clk_n         : in     std_logic;
        sys_clk_p         : in     std_logic;
        sys_reset_n       : in     std_logic);
end entity CR_Wupper;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.CR_Wupper.a0
--! =project    FELIX_MROD
--! @modified   Fri Apr 30 17:28:27 2021
--!-----------------------------------------------------------------------------


architecture a0 of CR_Wupper is

    -- uses entities CRFM_felixmrod.vhd, wupper.vhd

    -- IP: xilinx.com:ip:fifo_generator:13.2.5, Vivado 2020.1
    -- read clock 80MHz, write clock 250MHz, prog_full: 508.
    -- write size: 512x256 bits, read size 4096x32 bits.

    component fifo_16KB_256to32 -- @suppress "Component declaration 'fifo_16KB_256to32' has none or multiple matching entity declarations"
        port (
            srst        : in std_logic;
            wr_clk      : in std_logic;
            rd_clk      : in std_logic;
            din         : in std_logic_vector(255 downto 0);
            wr_en       : in std_logic;
            rd_en       : in std_logic;
            dout        : out std_logic_vector(31 downto 0);
            full        : out std_logic;
            empty       : out std_logic;
            valid       : out std_logic;
            prog_full   : out std_logic;
            wr_rst_busy : out std_logic;
            rd_rst_busy : out std_logic
        );
    end component fifo_16KB_256to32;

    ------------

    signal appreg_clk                             : std_logic;
    signal pcie_register_map_40_control           : register_map_control_type;
    signal pcie_register_map_control_appreg_clk   : register_map_control_type;
    signal register_map_emu_monitor               : register_map_gbtemu_monitor_type;
    signal register_map_hk_monitor                : register_map_hk_monitor_type;
    signal register_map_cr_monitor                : register_map_cr_monitor_type;
    signal register_map_gbt_monitor               : register_map_gbt_monitor_type;
    signal register_map_gen_board_info            : register_map_gen_board_info_type;
    signal register_mrod_monitor                  : regmap_mrod_monitor_type;
    signal register_map_ttc_monitor               : register_map_ttc_monitor_type;
    signal cr_register_map_xoff_monitor           : register_map_xoff_monitor_type;
    signal interrupt_call                         : std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);
    signal interrupt_call_i                       : std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);
    signal pcie_tohost_busy_out                   : std_logic;
    --@signal CRoutData_array                        : txrx120b_type(0 to (GBT_NUM-1));
    --@signal cr_CRoutData_array                     : txrx120b_type(0 to (GBT_NUM/2-1));
    signal rst_soft_40_0                          : std_logic;
    signal reset_soft_pcie                        : std_logic;
    signal fromHostFifo_rst                       : std_logic;
    signal toHostFifo_din                         : std_logic_vector(255 downto 0);
    signal toHostFifo_wr_en                       : std_logic;
    signal toHostFifo_prog_full                   : std_logic;
    signal toHostFifo_wr_clk                      : std_logic;
    signal toHostFifo_rst                         : std_logic;
    signal toHostFifo_wr_data_count               : std_logic_vector(11 downto 0);
    signal thFMbusy_array                         : std_logic_vector(NUMCH-1 downto 0);
    signal thFMlinkValid_array                    : std_logic_vector(NUMCH-1 downto 0);
    signal thFMinDnT_array                        : txrx33b_type(0 to NUMCH-1);

    constant Zero : std_logic_vector(255 downto 0) := (others => '0');
    constant One  : std_logic := '1';
    signal fromHostFifo_dout : std_logic_vector(255 downto 0);
    signal fromHostFifo_empty : std_logic;
    signal fromHostFifo_rd_en : std_logic;
    signal fromHostFifo_rd_en_pipe : std_logic;
    signal fhFifoFull : std_logic;
    signal fromHostFifo_dout_swapped : std_logic_vector(255 downto 0);
    signal CPMToWupper : CPMToWupper_type;
    signal WupperToCPM : WupperToCPM_type;

begin

    thFMinDnT_array     <= txrx33b_type(ChData);  -- input data
    thFMlinkValid_array <= ChValid;               -- input valid
    ChBusy              <= thFMbusy_array;        -- FMCR busy

    CRBusyOut(NUMCH-1 downto 0) <= thFMbusy_array(NUMCH-1 downto 0);

    rst_soft_40         <= rst_soft_40_0;         -- reset output
    pcie_DMA_Busy       <= pcie_tohost_busy_out;
    pcie_soft_reset     <= reset_soft_pcie;
    pcie_appreg_clk     <= appreg_clk;            --appreg_clk output from pcie

    prmap_app_control   <= pcie_register_map_control_appreg_clk;  -- registers on appreg_clk
    prmap_40_control    <= pcie_register_map_40_control;          -- registers on clk40

    register_map_hk_monitor       <= prmap_hk_monitor;
    register_map_gen_board_info   <= prmap_board_info;
    register_map_ttc_monitor      <= prmap_ttc_monitor;

    register_mrod_monitor.MROD_EP0_RXALIGNBSY(NUMCH-1 downto 0) <= Trx0Monitor.RXALIGNBSY(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP0_RXRECDATA(NUMCH-1 downto 0)  <= Trx0Monitor.RXRECDATA(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP0_RXRECIDLES(NUMCH-1 downto 0) <= Trx0Monitor.RXRECIDLES(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP0_TXLOCKED(NUMCH-1 downto 0)   <= Trx0Monitor.TXLOCKED(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP0_CSMH_EMPTY(NUMCH-1 downto 0) <= CSM0Monitor.CSMH_EMPTY(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP0_CSMH_FULL(NUMCH-1 downto 0)  <= CSM0Monitor.CSMH_FULL(NUMCH-1 downto 0);

    register_mrod_monitor.MROD_EP1_RXALIGNBSY(NUMCH-1 downto 0) <= Trx1Monitor.RXALIGNBSY(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP1_RXRECDATA(NUMCH-1 downto 0)  <= Trx1Monitor.RXRECDATA(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP1_RXRECIDLES(NUMCH-1 downto 0) <= Trx1Monitor.RXRECIDLES(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP1_TXLOCKED(NUMCH-1 downto 0)   <= Trx1Monitor.TXLOCKED(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP1_CSMH_EMPTY(NUMCH-1 downto 0) <= CSM1Monitor.CSMH_EMPTY(NUMCH-1 downto 0);
    register_mrod_monitor.MROD_EP1_CSMH_FULL(NUMCH-1 downto 0)  <= CSM1Monitor.CSMH_FULL(NUMCH-1 downto 0);

    interrupt_call_i(7 downto 4) <= interrupt_call(7) & BUSY_INTERRUPT & interrupt_call(5 downto 4);

    ---------------------------------------------

    fromHostFifo_dout_swapped <=
                                 fromHostFifo_dout(31  downto 0)   &        -- swap 32bit word order
                                 fromHostFifo_dout(63  downto 32)  &
                                 fromHostFifo_dout(95  downto 64)  &
                                 fromHostFifo_dout(127 downto 96)  &
                                 fromHostFifo_dout(159 downto 128) &
                                 fromHostFifo_dout(191 downto 160) &
                                 fromHostFifo_dout(223 downto 192) &
                                 fromHostFifo_dout(255 downto 224);
    ---------------------------------------------

    u1: entity work.CRFM_felixmrod
        generic map(
            NUMBER_OF_INTERRUPTS           => NUMBER_OF_INTERRUPTS,
            GBT_NUM                        => GBT_NUM/2,
            EnableFrHo_Egroup0Eproc2_HDLC  => EnableFrHo_Egroup0Eproc2_HDLC,
            EnableFrHo_Egroup0Eproc2_8b10b => EnableFrHo_Egroup0Eproc2_8b10b,
            EnableFrHo_Egroup0Eproc4_8b10b => EnableFrHo_Egroup0Eproc4_8b10b,
            EnableFrHo_Egroup0Eproc8_8b10b => EnableFrHo_Egroup0Eproc8_8b10b,
            EnableFrHo_Egroup1Eproc2_HDLC  => EnableFrHo_Egroup1Eproc2_HDLC,
            EnableFrHo_Egroup1Eproc2_8b10b => EnableFrHo_Egroup1Eproc2_8b10b,
            EnableFrHo_Egroup1Eproc4_8b10b => EnableFrHo_Egroup1Eproc4_8b10b,
            EnableFrHo_Egroup1Eproc8_8b10b => EnableFrHo_Egroup1Eproc8_8b10b,
            EnableFrHo_Egroup2Eproc2_HDLC  => EnableFrHo_Egroup2Eproc2_HDLC,
            EnableFrHo_Egroup2Eproc2_8b10b => EnableFrHo_Egroup2Eproc2_8b10b,
            EnableFrHo_Egroup2Eproc4_8b10b => EnableFrHo_Egroup2Eproc4_8b10b,
            EnableFrHo_Egroup2Eproc8_8b10b => EnableFrHo_Egroup2Eproc8_8b10b,
            EnableFrHo_Egroup3Eproc2_HDLC  => EnableFrHo_Egroup3Eproc2_HDLC,
            EnableFrHo_Egroup3Eproc2_8b10b => EnableFrHo_Egroup3Eproc2_8b10b,
            EnableFrHo_Egroup3Eproc4_8b10b => EnableFrHo_Egroup3Eproc4_8b10b,
            EnableFrHo_Egroup3Eproc8_8b10b => EnableFrHo_Egroup3Eproc8_8b10b,
            EnableFrHo_Egroup4Eproc2_HDLC  => EnableFrHo_Egroup4Eproc2_HDLC,
            EnableFrHo_Egroup4Eproc2_8b10b => EnableFrHo_Egroup4Eproc2_8b10b,
            EnableFrHo_Egroup4Eproc4_8b10b => EnableFrHo_Egroup4Eproc4_8b10b,
            EnableFrHo_Egroup4Eproc8_8b10b => EnableFrHo_Egroup4Eproc8_8b10b,
            wideMode                       => wideMode,
            STATIC_CENTRALROUTER           => STATIC_CENTRALROUTER,
            FIRMWARE_MODE                  => FIRMWARE_MODE,
            FMCH_NUM                       => FMCH_NUM, -- number of input channels
            toHostTimeoutBitn              => toHostTimeoutBitn,
            GENERATE_XOFF                  => GENERATE_XOFF,
            CARD_TYPE                      => CARD_TYPE,
            CREnableFromHost               => CREnableFromHost,
            SUPER_CHUNK_FACTOR             => SUPER_CHUNK_FACTOR,
            BLOCKSIZE                      => BLOCKSIZE,
            CHUNK_TRAILER_32B              => CHUNK_TRAILER_32B
        )
        port map(
            clk40                           => clk40,
            clk80                           => clk80,
            clk160                          => clk160,
            clk250                          => clk250,
            rst                             => rst_soft_40_0,
            register_map_control            => pcie_register_map_40_control,
            register_map_cr_monitor         => register_map_cr_monitor,
            register_map_xoff_monitor       => cr_register_map_xoff_monitor,
            register_map_control_appreg_clk => pcie_register_map_control_appreg_clk,
            appreg_clk                      => appreg_clk,
            interrupt_call                  => interrupt_call,
            thFMlinkValid_array             => thFMlinkValid_array(FMCH_NUM-1 downto 0),
            thFMinDnT_array                 => thFMinDnT_array(0 to FMCH_NUM-1),
            toHostFifo_din                  => toHostFifo_din,
            toHostFifo_wr_en                => toHostFifo_wr_en,
            toHostFifo_prog_full            => toHostFifo_prog_full,
            toHostFifo_wr_clk               => toHostFifo_wr_clk,
            toHostFifo_rst                  => toHostFifo_rst,
            thFMXoff                        => open,
            thFMbusyOut                     => thFMbusy_array(FMCH_NUM-1 downto 0),        --@BusyOutCR0,
            thFIFObusyOut                   => CR_FIFO_Busy,
            TTCin                           => TTC_out,
            fromHostFifo_dout               => Zero,                  --@fromHostFifo_dout,
            fromHostFifo_rd_en              => open,                  --@fromHostFifo_rd_en,
            fromHostFifo_empty              => One,                   --@fromHostFifo_empty,
            fromHostFifo_rd_clk             => open,                  -- fixed to clk80 (@fromHostFifo_rd_clk,)
            fromHostFifo_rst                => fromHostFifo_rst,
            fhXoff                          => open,
            fhOutData_array                 => open,                  --@cr_CRoutData_array,
            TTC_ToHost_Data_in              => TTC_ToHost_Data,
            toHostFifo_wr_data_count        => toHostFifo_wr_data_count
        );

    u3: fifo_16KB_256to32    -- small fromHost fifo (512x256 to 4096x32)
        port map (
            srst        => fromHostFifo_rst,
            wr_clk      => clk80,
            rd_clk      => clk80,                     -- fixed to clk80 (@fromHostFifo_rd_clk,)
            din         => fromHostFifo_dout_swapped,
            wr_en       => fromHostFifo_rd_en_pipe,
            rd_en       => fhFifoRE,                  --fromHostFifo_rd_en,
            dout        => fhFifoD,                   --fromHostFifo_dout,
            full        => fhFifoFull,
            empty       => fhFifoEmpty,               --fromHostFifo_empty,
            valid       => fhFifoValid,
            prog_full   => open,
            wr_rst_busy => open,
            rd_rst_busy => open
        );

    fromHostFifo_rd_en <= '1' when (fromHostFifo_empty ='0' and fhFifoFull = '0') else '0';

    fhFifo_rd_proc: process(clk80)
    begin
        if (rising_edge(clk80)) then
            fromHostFifo_rd_en_pipe <= fromHostFifo_rd_en;
        end if;
    end process;


    u5: entity work.wupper            -- pcie interface
        generic map(
            NUMBER_OF_INTERRUPTS => NUMBER_OF_INTERRUPTS,
            NUMBER_OF_DESCRIPTORS => NUMBER_OF_DESCRIPTORS,
            BUILD_DATETIME => BUILD_DATETIME,
            CARD_TYPE => CARD_TYPE,
            GIT_HASH => GIT_HASH,
            COMMIT_DATETIME => COMMIT_DATETIME,
            GIT_TAG => GIT_TAG,
            GIT_COMMIT_NUMBER => GIT_COMMIT_NUMBER,
            GBT_GENERATE_ALL_REGS => GBT_GENERATE_ALL_REGS,
            EMU_GENERATE_REGS => EMU_GENERATE_REGS,
            MROD_GENERATE_REGS => MROD_GENERATE_REGS,
            GBT_NUM => GBT_NUM,
            FIRMWARE_MODE => FIRMWARE_MODE,
            PCIE_ENDPOINT => W_ENDPOINT, --0/1--!
            PCIE_LANES => 8,
            DATA_WIDTH => 256,
            SIMULATION => false,
            BLOCKSIZE => BLOCKSIZE,
            USE_VERSAL_CPM => false,
            ENABLE_XVC => ENABLE_XVC
        )
        port map(
            appreg_clk => appreg_clk,
            sync_clk => clk40,
            flush_fifo => open,
            interrupt_call => interrupt_call_i,
            lnk_up => lnk_up,
            pcie_rxn => pcie_rxn(7 downto 0),
            pcie_rxp => pcie_rxp(7 downto 0),
            pcie_txn => pcie_txn(7 downto 0),
            pcie_txp => pcie_txp(7 downto 0),
            pll_locked => open,
            register_map_control_sync => pcie_register_map_40_control,
            register_map_control_appreg_clk => pcie_register_map_control_appreg_clk,
            register_map_gen_board_info => register_map_gen_board_info,
            register_map_cr_monitor => register_map_cr_monitor,
            register_map_gbtemu_monitor => register_map_emu_monitor,
            register_map_gbt_monitor => register_map_gbt_monitor,
            register_map_ttc_monitor => register_map_ttc_monitor,
            register_map_xoff_monitor => cr_register_map_xoff_monitor,
            register_map_hk_monitor => register_map_hk_monitor,
            register_map_generators => register_map_generators_c,
            wishbone_monitor => wishbone_monitor_c,
            ipbus_monitor => ipbus_monitor_c,
            regmap_mrod_monitor => register_mrod_monitor,
            reset_hard => open,
            reset_soft => rst_soft_40_0,
            reset_soft_appreg_clk => reset_soft_pcie,
            reset_hw_in => rst_hw,
            sys_clk_n => sys_clk_n,
            sys_clk_p => sys_clk_p,
            sys_reset_n => sys_reset_n,
            tohost_busy_out => pcie_tohost_busy_out,
            fromHostFifo_dout => fromHostFifo_dout,
            fromHostFifo_empty => fromHostFifo_empty,
            fromHostFifo_rd_clk => clk80,
            fromHostFifo_rd_en => fromHostFifo_rd_en,
            fromHostFifo_rst => fromHostFifo_rst,
            toHostFifo_din(0) => toHostFifo_din,
            toHostFifo_prog_full(0) => toHostFifo_prog_full,
            toHostFifo_rst => toHostFifo_rst,
            toHostFifo_wr_clk => toHostFifo_wr_clk,
            wr_data_count(0) => toHostFifo_wr_data_count,
            toHostFifo_wr_en(0) => toHostFifo_wr_en,
            clk250_out => open,
            master_busy_in => MasterBusy,
            CPMToWupper => CPMToWupper,
            WupperToCPM => WupperToCPM
        );

    ---------------------------------------------

    -- drive unused registers emu_monitor...
    register_map_emu_monitor.GBT_EMU_CONFIG.RDDATA <= (others => '0');
    register_map_emu_monitor.GBT_FM_EMU_READ <= (others => '0');

    -- drive unused registers gbt_monitor...
    register_map_gbt_monitor.GBT_VERSION.DATE <= (others => '0');
    register_map_gbt_monitor.GBT_VERSION.GBT_VERSION <= (others => '0');
    register_map_gbt_monitor.GBT_VERSION.GTH_IP_VERSION <= (others => '0');
    register_map_gbt_monitor.GBT_VERSION.RESERVED <= (others => '0');
    register_map_gbt_monitor.GBT_VERSION.GTHREFCLK_SEL <= (others => '0');
    register_map_gbt_monitor.GBT_VERSION.RX_CLK_SEL <= (others => '0');
    register_map_gbt_monitor.GBT_VERSION.PLL_SEL <= (others => '0');
    register_map_gbt_monitor.GBT_TXRESET_DONE <= (others => '0');
    register_map_gbt_monitor.GBT_RXRESET_DONE <= (others => '0');
    register_map_gbt_monitor.GBT_TXFSMRESET_DONE <= (others => '0');
    register_map_gbt_monitor.GBT_RXFSMRESET_DONE <= (others => '0');
    register_map_gbt_monitor.GBT_CPLL_FBCLK_LOST <= (others => '0');
    register_map_gbt_monitor.GBT_PLL_LOCK.QPLL_LOCK <= (others => '0');
    register_map_gbt_monitor.GBT_PLL_LOCK.CPLL_LOCK <= (others => '0');
    register_map_gbt_monitor.GBT_RXCDR_LOCK <= (others => '0');
    register_map_gbt_monitor.GBT_CLK_SAMPLED <= (others => '0');
    register_map_gbt_monitor.GBT_RX_IS_HEADER <= (others => '0');
    register_map_gbt_monitor.GBT_RX_IS_DATA <= (others => '0');
    register_map_gbt_monitor.GBT_RX_HEADER_FOUND <= (others => '0');
    register_map_gbt_monitor.GBT_ALIGNMENT_DONE <= (others => '0');
    register_map_gbt_monitor.GBT_OUT_MUX_STATUS <= (others => '0');
    register_map_gbt_monitor.GBT_ERROR <= (others => '0');
    register_map_gbt_monitor.GBT_GBT_TOPBOT_C <= (others => '0');
    register_map_gbt_monitor.GBT_FM_RX_DISP_ERROR1 <= (others => '0');
    register_map_gbt_monitor.GBT_FM_RX_DISP_ERROR2 <= (others => '0');
    register_map_gbt_monitor.GBT_FM_RX_NOTINTABLE1 <= (others => '0');
    register_map_gbt_monitor.GBT_FM_RX_NOTINTABLE2 <= (others => '0');

end architecture a0 ; -- of CR_Wupper

