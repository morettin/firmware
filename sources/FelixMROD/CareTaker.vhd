--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.CareTaker
--! =project    FELIX_MROD
--! @modified   Thu Dec 17 23:32:01 2020
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity CareTaker is
  generic(
    OPTO_TRX                        : integer := 2;
    GBT_NUM                         : integer := 4;
    ENDPOINTS                       : integer := 2;
    NUMCH                           : integer := 2;
    DEBUG_MODE                      : boolean := false;
    CARD_TYPE                       : integer := 712;
    GENERATE_GBT                    : boolean := true;
    GBT_MAPPING                     : integer := 0;
    GENERATE_XOFF                   : boolean := true;
    GTHREFCLK_SEL                   : std_logic := '0';
    AUTOMATIC_CLOCK_SWITCH          : boolean := true;
    FIRMWARE_MODE                   : integer := 1;
    CREnableFromHost                : boolean := true;
    crInternalLoopbackMode          : boolean := false;
    includeDirectMode               : boolean := false;
    TTC_test_mode                   : boolean := false;
    USE_Si5324_RefCLK               : boolean := false;
    generateTTCemu                  : boolean := false;
    generate_IC_EC_TTC_only         : boolean := false;
    wideMode                        : boolean := false;
    EnableFrHo_Egroup0Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup0Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup0Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup0Eproc8_8b10b  : boolean := false;
    EnableFrHo_Egroup1Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup1Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup1Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup1Eproc8_8b10b  : boolean := false;
    EnableFrHo_Egroup2Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup2Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup2Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup2Eproc8_8b10b  : boolean := false;
    EnableFrHo_Egroup3Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup3Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup3Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup3Eproc8_8b10b  : boolean := false;
    EnableFrHo_Egroup4Eproc2_HDLC   : boolean := false;
    EnableFrHo_Egroup4Eproc2_8b10b  : boolean := false;
    EnableFrHo_Egroup4Eproc4_8b10b  : boolean := false;
    EnableFrHo_Egroup4Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup0Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup0Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup0Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup0Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup0Eproc16_8b10b : boolean := false;
    EnableToHo_Egroup1Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup1Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup1Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup1Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup1Eproc16_8b10b : boolean := false;
    EnableToHo_Egroup2Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup2Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup2Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup2Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup2Eproc16_8b10b : boolean := false;
    EnableToHo_Egroup3Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup3Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup3Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup3Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup3Eproc16_8b10b : boolean := false;
    EnableToHo_Egroup4Eproc2_HDLC   : boolean := false;
    EnableToHo_Egroup4Eproc2_8b10b  : boolean := false;
    EnableToHo_Egroup4Eproc4_8b10b  : boolean := false;
    EnableToHo_Egroup4Eproc8_8b10b  : boolean := false;
    EnableToHo_Egroup4Eproc16_8b10b : boolean := false;
    GENERATE_FEI4B                  : boolean := false;
    SUPER_CHUNK_FACTOR              : integer := 12;
    BLOCKSIZE                       : integer := 1024;
    CHUNK_TRAILER_32B               : boolean := false);
  port (
    I2C_SMB           : out    std_logic;
    I2C_SMBUS_CFG_nEN : out    std_logic;
    I2C_nRESET        : out    std_logic;
    I2C_nRESET_PCIe   : out    std_logic;
    LMK_Locked        : in     std_logic;
    MGMT_PORT_EN      : out    std_logic;
    MMCM_Locked       : in     std_logic;
    MMCM_OscSelect    : in     std_logic;
    NT_PORTSEL        : out    std_logic_vector(2 downto 0);
    PCIE_PERSTn1      : out    std_logic;
    PCIE_PERSTn2      : out    std_logic;
    PEX_PERSTn        : out    std_logic;
    PEX_SCL           : out    std_logic;
    PEX_SDA           : inout  std_logic;
    PORT_GOOD         : in     std_logic_vector(7 downto 0);
    Perstn1_open      : in     std_logic;
    Perstn2_open      : in     std_logic;
    RxClk0F           : in     std_logic_vector(NUMCH-1 downto 0);
    RxClk1F           : in     std_logic_vector(NUMCH-1 downto 0);
    SCL               : inout  std_logic;
    SDA               : inout  std_logic;
    SHPC_INT          : out    std_logic;
    SI5345_A          : out    std_logic_vector(1 downto 0);
    SI5345_INSEL      : out    std_logic_vector(1 downto 0);
    SI5345_OE         : out    std_logic;
    SI5345_SEL        : out    std_logic;
    SI5345_nLOL       : in     std_logic;
    STN0_PORTCFG      : out    std_logic_vector(1 downto 0);
    STN1_PORTCFG      : out    std_logic_vector(1 downto 0);
    SmaOut_x3         : out    std_logic;
    SmaOut_x4         : out    std_logic;
    SmaOut_x5         : out    std_logic;
    SmaOut_x6         : out    std_logic;
    TACH              : in     std_logic;
    TESTMODE          : out    std_logic_vector(2 downto 0);
    UPSTREAM_PORTSEL  : out    std_logic_vector(2 downto 0);
    clk10_xtal        : in     std_logic;
    clk40_xtal        : in     std_logic;
    emcclk            : in     std_logic;
    flash_SEL         : out    std_logic;
    flash_a           : out    std_logic_vector(24 downto 0);
    flash_a_msb       : inout  std_logic_vector(1 downto 0);
    flash_adv         : out    std_logic;
    flash_cclk        : out    std_logic;
    flash_ce          : out    std_logic;
    flash_d           : inout  std_logic_vector(15 downto 0);
    flash_re          : out    std_logic;
    flash_we          : out    std_logic;
    lnk0_up           : in     std_logic;
    lnk1_up           : in     std_logic;
    opto_inhibit      : out    std_logic_vector(OPTO_TRX-1 downto 0);
    pcie_appreg_clk   : in     std_logic;
    pcie_soft_reset   : in     std_logic;
    prmap_app_control : in     register_map_control_type;
    prmap_board_info  : out    register_map_gen_board_info_type;
    prmap_hk_monitor  : out    register_map_hk_monitor_type;
    sys_reset_n       : in     std_logic;
    uC_reset_N        : out    std_logic);
end entity CareTaker;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.CareTaker.a0
--! =project    FELIX_MROD
--! @modified   Thu Dec 17 23:32:01 2020
--!-----------------------------------------------------------------------------


architecture a0 of CareTaker is

  component housekeeping_module
    generic(
      CARD_TYPE                       : integer := 710;
      OPTO_TRX                        : integer := 2;
      GBT_MAPPING                     : integer := 0; -- GBT mapping: 0 NORMAL CXP1 -> GBT1-12 | 1 ALTERNATE CXP1 -> GBT 1-4,9-12,17-20
      GBT_NUM                         : integer := 24;
      ENDPOINTS                       : integer := 1; --Number of PCIe Endpoints in the design, GBT_NUM has to be multiplied by this number in some cases.
      TTC_test_mode                   : boolean := false;
      generateTTCemu                  : boolean := false;
      GENERATE_GBT                    : boolean := true;
      crInternalLoopbackMode          : boolean := false;
      wideMode                        : boolean := false;
      DEBUG_MODE                      : boolean := false;
      AUTOMATIC_CLOCK_SWITCH          : boolean := false;
      FIRMWARE_MODE                   : integer := 0;
      generate_IC_EC_TTC_only         : boolean := false;
      GTHREFCLK_SEL                   : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
      USE_Si5324_RefCLK               : boolean := false;
      includeDirectMode               : boolean := true;
      CREnableFromHost                : boolean := true;
      GENERATE_XOFF                   : boolean := true; -- FromHost Xoff transmission enabled on Full mode busy
      EnableToHo_Egroup0Eproc2_HDLC   : boolean := false;
      EnableToHo_Egroup0Eproc2_8b10b  : boolean := false;
      EnableToHo_Egroup0Eproc4_8b10b  : boolean := false;
      EnableToHo_Egroup0Eproc8_8b10b  : boolean := false;
      EnableToHo_Egroup0Eproc16_8b10b : boolean := false;
      EnableToHo_Egroup1Eproc2_HDLC   : boolean := false;
      EnableToHo_Egroup1Eproc2_8b10b  : boolean := false;
      EnableToHo_Egroup1Eproc4_8b10b  : boolean := false;
      EnableToHo_Egroup1Eproc8_8b10b  : boolean := false;
      EnableToHo_Egroup1Eproc16_8b10b : boolean := false;
      EnableToHo_Egroup2Eproc2_HDLC   : boolean := false;
      EnableToHo_Egroup2Eproc2_8b10b  : boolean := false;
      EnableToHo_Egroup2Eproc4_8b10b  : boolean := false;
      EnableToHo_Egroup2Eproc8_8b10b  : boolean := false;
      EnableToHo_Egroup2Eproc16_8b10b : boolean := false;
      EnableToHo_Egroup3Eproc2_HDLC   : boolean := false;
      EnableToHo_Egroup3Eproc2_8b10b  : boolean := false;
      EnableToHo_Egroup3Eproc4_8b10b  : boolean := false;
      EnableToHo_Egroup3Eproc8_8b10b  : boolean := false;
      EnableToHo_Egroup3Eproc16_8b10b : boolean := false;
      EnableToHo_Egroup4Eproc2_HDLC   : boolean := false;
      EnableToHo_Egroup4Eproc2_8b10b  : boolean := false;
      EnableToHo_Egroup4Eproc4_8b10b  : boolean := false;
      EnableToHo_Egroup4Eproc8_8b10b  : boolean := false;
      EnableToHo_Egroup4Eproc16_8b10b : boolean := false;
      EnableFrHo_Egroup0Eproc2_HDLC   : boolean := false;
      EnableFrHo_Egroup0Eproc2_8b10b  : boolean := false;
      EnableFrHo_Egroup0Eproc4_8b10b  : boolean := false;
      EnableFrHo_Egroup0Eproc8_8b10b  : boolean := false;
      EnableFrHo_Egroup1Eproc2_HDLC   : boolean := false;
      EnableFrHo_Egroup1Eproc2_8b10b  : boolean := false;
      EnableFrHo_Egroup1Eproc4_8b10b  : boolean := false;
      EnableFrHo_Egroup1Eproc8_8b10b  : boolean := false;
      EnableFrHo_Egroup2Eproc2_HDLC   : boolean := false;
      EnableFrHo_Egroup2Eproc2_8b10b  : boolean := false;
      EnableFrHo_Egroup2Eproc4_8b10b  : boolean := false;
      EnableFrHo_Egroup2Eproc8_8b10b  : boolean := false;
      EnableFrHo_Egroup3Eproc2_HDLC   : boolean := false;
      EnableFrHo_Egroup3Eproc2_8b10b  : boolean := false;
      EnableFrHo_Egroup3Eproc4_8b10b  : boolean := false;
      EnableFrHo_Egroup3Eproc8_8b10b  : boolean := false;
      EnableFrHo_Egroup4Eproc2_HDLC   : boolean := false;
      EnableFrHo_Egroup4Eproc2_8b10b  : boolean := false;
      EnableFrHo_Egroup4Eproc4_8b10b  : boolean := false;
      EnableFrHo_Egroup4Eproc8_8b10b  : boolean := false;
      GENERATE_FEI4B                  : boolean := false;
      SUPER_CHUNK_FACTOR              : integer := 12;
      BLOCKSIZE                       : integer := 1024;
      CHUNK_TRAILER_32B               : boolean := false);
    port (
      LMK_locked                  : in     std_logic_vector(0 downto 0);
      MMCM_Locked_in              : in     std_logic;
      MMCM_OscSelect_in           : in     std_logic;
      SCL                         : inout  std_logic;
      SDA                         : inout  std_logic;
      SI5345_A                    : out    std_logic_vector(1 downto 0);
      SI5345_INSEL                : out    std_logic_vector(1 downto 0);
      SI5345_OE                   : out    std_logic;
      SI5345_RSTN                 : out    std_logic;
      SI5345_SEL                  : out    std_logic;
      SI5345_nLOL                 : in     std_logic;
      appreg_clk                  : in     std_logic;
      emcclk                      : in     std_logic;
      flash_SEL                   : out    std_logic;
      flash_a                     : out    std_logic_vector(24 downto 0);
      flash_a_msb                 : inout  std_logic_vector(1 downto 0);
      flash_adv                   : out    std_logic;
      flash_cclk                  : out    std_logic;
      flash_ce                    : out    std_logic;
      flash_d                     : inout  std_logic_vector(15 downto 0);
      flash_re                    : out    std_logic;
      flash_we                    : out    std_logic;
      i2cmux_rst                  : out    std_logic;
      tach                        : in     std_logic;
      clk10_xtal                  : in     std_logic;
      leds                        : out    std_logic_vector(7 downto 0);
      opto_inhibit                : out    std_logic_vector(OPTO_TRX-1 downto 0);
      opto_los                    : in     std_logic_vector(OPTO_TRX-1 downto 0);
      opto_los_net                : out    std_logic_vector(OPTO_TRX-1 downto 0);
      register_map_control        : in     register_map_control_type;
      register_map_gen_board_info : out    register_map_gen_board_info_type;
      register_map_hk_monitor     : out    register_map_hk_monitor_type;
      rst_soft                    : in     std_logic;
      sys_reset_n                 : in     std_logic;
      RXUSRCLK_IN                 : in     std_logic_vector((GBT_NUM*ENDPOINTS)-1 downto 0));
  end component housekeeping_module;

  component pex_init
    generic(
      CARD_TYPE : integer := 712);
    port (
      I2C_SMB           : out    std_logic;
      I2C_SMBUS_CFG_nEN : out    std_logic;
      MGMT_PORT_EN      : out    std_logic;
      PCIE_PERSTn1      : out    std_logic;
      PCIE_PERSTn2      : out    std_logic;
      PEX_PERSTn        : out    std_logic;
      PEX_SCL           : out    std_logic;
      PEX_SDA           : inout  std_logic;
      PORT_GOOD         : in     std_logic_vector(7 downto 0);
      SHPC_INT          : out    std_logic;
      clk40             : in     std_logic;
      lnk_up0           : in     std_logic;
      lnk_up1           : in     std_logic;
      reset_pcie        : out    std_logic_vector(0 downto 0);
      sys_reset_n       : in     std_logic);
  end component pex_init;

  -------------

  signal appreg_clk             : std_logic;
  signal reset_soft_pcie        : std_logic;
  signal slv_LMK_locked         : std_logic_vector(0 downto 0);
  signal RXUSRCLKF              : std_logic_vector(NUMCH*2-1 downto 0);

begin

  NT_PORTSEL <= "111";
  TESTMODE <= "000";
  UPSTREAM_PORTSEL <= "000";
  STN0_PORTCFG <= "0Z";
  STN1_PORTCFG <= "01";
  SmaOut_x4 <= '0';
  SmaOut_x3 <= '0';
  SmaOut_x5 <= '0';
  SmaOut_x6 <= '0';
  I2C_nRESET_PCIe <= '1';
  uC_reset_N <= '1';

  slv_LMK_locked(0) <= LMK_Locked;
  appreg_clk        <= pcie_appreg_clk;   
  reset_soft_pcie   <= pcie_soft_reset;

  RXUSRCLKF(NUMCH*2-1 downto NUMCH) <= RxClk1F(NUMCH-1 downto 0);
  RXUSRCLKF(NUMCH-1 downto 0)       <= RxClk0F(NUMCH-1 downto 0);

  u1: housekeeping_module
    generic map(
      CARD_TYPE                       => CARD_TYPE,
      OPTO_TRX                        => OPTO_TRX,
      GBT_MAPPING                     => GBT_MAPPING,
      GBT_NUM                         => GBT_NUM/2,
      ENDPOINTS                       => ENDPOINTS,
      TTC_test_mode                   => TTC_test_mode,
      generateTTCemu                  => generateTTCemu,
      GENERATE_GBT                    => GENERATE_GBT,
      crInternalLoopbackMode          => crInternalLoopbackMode,
      wideMode                        => wideMode,
      DEBUG_MODE                      => DEBUG_MODE,
      AUTOMATIC_CLOCK_SWITCH          => AUTOMATIC_CLOCK_SWITCH,
      FIRMWARE_MODE                   => FIRMWARE_MODE,
      generate_IC_EC_TTC_only         => generate_IC_EC_TTC_only,
      GTHREFCLK_SEL                   => GTHREFCLK_SEL,
      USE_Si5324_RefCLK               => USE_Si5324_RefCLK,
      includeDirectMode               => includeDirectMode,
      CREnableFromHost                => CREnableFromHost,
      GENERATE_XOFF                   => GENERATE_XOFF,
      EnableToHo_Egroup0Eproc2_HDLC   => EnableToHo_Egroup0Eproc2_HDLC,
      EnableToHo_Egroup0Eproc2_8b10b  => EnableToHo_Egroup0Eproc2_8b10b,
      EnableToHo_Egroup0Eproc4_8b10b  => EnableToHo_Egroup0Eproc4_8b10b,
      EnableToHo_Egroup0Eproc8_8b10b  => EnableToHo_Egroup0Eproc8_8b10b,
      EnableToHo_Egroup0Eproc16_8b10b => EnableToHo_Egroup0Eproc16_8b10b,
      EnableToHo_Egroup1Eproc2_HDLC   => EnableToHo_Egroup1Eproc2_HDLC,
      EnableToHo_Egroup1Eproc2_8b10b  => EnableToHo_Egroup1Eproc2_8b10b,
      EnableToHo_Egroup1Eproc4_8b10b  => EnableToHo_Egroup1Eproc4_8b10b,
      EnableToHo_Egroup1Eproc8_8b10b  => EnableToHo_Egroup1Eproc8_8b10b,
      EnableToHo_Egroup1Eproc16_8b10b => EnableToHo_Egroup1Eproc16_8b10b,
      EnableToHo_Egroup2Eproc2_HDLC   => EnableToHo_Egroup2Eproc2_HDLC,
      EnableToHo_Egroup2Eproc2_8b10b  => EnableToHo_Egroup2Eproc2_8b10b,
      EnableToHo_Egroup2Eproc4_8b10b  => EnableToHo_Egroup2Eproc4_8b10b,
      EnableToHo_Egroup2Eproc8_8b10b  => EnableToHo_Egroup2Eproc8_8b10b,
      EnableToHo_Egroup2Eproc16_8b10b => EnableToHo_Egroup2Eproc16_8b10b,
      EnableToHo_Egroup3Eproc2_HDLC   => EnableToHo_Egroup3Eproc2_HDLC,
      EnableToHo_Egroup3Eproc2_8b10b  => EnableToHo_Egroup3Eproc2_8b10b,
      EnableToHo_Egroup3Eproc4_8b10b  => EnableToHo_Egroup3Eproc4_8b10b,
      EnableToHo_Egroup3Eproc8_8b10b  => EnableToHo_Egroup3Eproc8_8b10b,
      EnableToHo_Egroup3Eproc16_8b10b => EnableToHo_Egroup3Eproc16_8b10b,
      EnableToHo_Egroup4Eproc2_HDLC   => EnableToHo_Egroup4Eproc2_HDLC,
      EnableToHo_Egroup4Eproc2_8b10b  => EnableToHo_Egroup4Eproc2_8b10b,
      EnableToHo_Egroup4Eproc4_8b10b  => EnableToHo_Egroup4Eproc4_8b10b,
      EnableToHo_Egroup4Eproc8_8b10b  => EnableToHo_Egroup4Eproc8_8b10b,
      EnableToHo_Egroup4Eproc16_8b10b => EnableToHo_Egroup4Eproc16_8b10b,
      EnableFrHo_Egroup0Eproc2_HDLC   => EnableFrHo_Egroup0Eproc2_HDLC,
      EnableFrHo_Egroup0Eproc2_8b10b  => EnableFrHo_Egroup0Eproc2_8b10b,
      EnableFrHo_Egroup0Eproc4_8b10b  => EnableFrHo_Egroup0Eproc4_8b10b,
      EnableFrHo_Egroup0Eproc8_8b10b  => EnableFrHo_Egroup0Eproc8_8b10b,
      EnableFrHo_Egroup1Eproc2_HDLC   => EnableFrHo_Egroup1Eproc2_HDLC,
      EnableFrHo_Egroup1Eproc2_8b10b  => EnableFrHo_Egroup1Eproc2_8b10b,
      EnableFrHo_Egroup1Eproc4_8b10b  => EnableFrHo_Egroup1Eproc4_8b10b,
      EnableFrHo_Egroup1Eproc8_8b10b  => EnableFrHo_Egroup1Eproc8_8b10b,
      EnableFrHo_Egroup2Eproc2_HDLC   => EnableFrHo_Egroup2Eproc2_HDLC,
      EnableFrHo_Egroup2Eproc2_8b10b  => EnableFrHo_Egroup2Eproc2_8b10b,
      EnableFrHo_Egroup2Eproc4_8b10b  => EnableFrHo_Egroup2Eproc4_8b10b,
      EnableFrHo_Egroup2Eproc8_8b10b  => EnableFrHo_Egroup2Eproc8_8b10b,
      EnableFrHo_Egroup3Eproc2_HDLC   => EnableFrHo_Egroup3Eproc2_HDLC,
      EnableFrHo_Egroup3Eproc2_8b10b  => EnableFrHo_Egroup3Eproc2_8b10b,
      EnableFrHo_Egroup3Eproc4_8b10b  => EnableFrHo_Egroup3Eproc4_8b10b,
      EnableFrHo_Egroup3Eproc8_8b10b  => EnableFrHo_Egroup3Eproc8_8b10b,
      EnableFrHo_Egroup4Eproc2_HDLC   => EnableFrHo_Egroup4Eproc2_HDLC,
      EnableFrHo_Egroup4Eproc2_8b10b  => EnableFrHo_Egroup4Eproc2_8b10b,
      EnableFrHo_Egroup4Eproc4_8b10b  => EnableFrHo_Egroup4Eproc4_8b10b,
      EnableFrHo_Egroup4Eproc8_8b10b  => EnableFrHo_Egroup4Eproc8_8b10b,
      GENERATE_FEI4B                  => GENERATE_FEI4B,
      SUPER_CHUNK_FACTOR              => SUPER_CHUNK_FACTOR,
      BLOCKSIZE                       => BLOCKSIZE,
      CHUNK_TRAILER_32B               => CHUNK_TRAILER_32B)
    port map(
      LMK_locked                  => slv_LMK_locked,
      MMCM_Locked_in              => MMCM_Locked,
      MMCM_OscSelect_in           => MMCM_OscSelect,
      SCL                         => SCL,
      SDA                         => SDA,
      SI5345_A                    => SI5345_A,
      SI5345_INSEL                => SI5345_INSEL,
      SI5345_OE                   => SI5345_OE,
      SI5345_RSTN                 => open,
      SI5345_SEL                  => SI5345_SEL,
      SI5345_nLOL                 => SI5345_nLOL,
      appreg_clk                  => appreg_clk,
      emcclk                      => emcclk,
      flash_SEL                   => flash_SEL,
      flash_a                     => flash_a,
      flash_a_msb                 => flash_a_msb,
      flash_adv                   => flash_adv,
      flash_cclk                  => flash_cclk,
      flash_ce                    => flash_ce,
      flash_d                     => flash_d,
      flash_re                    => flash_re,
      flash_we                    => flash_we,
      i2cmux_rst                  => I2C_nRESET,
      tach                        => TACH,
      clk10_xtal                  => clk10_xtal,
      leds                        => open,
      opto_inhibit                => opto_inhibit,
      opto_los                    => (others => '0'),
      opto_los_net                => open,
      register_map_control        => prmap_app_control,
      register_map_gen_board_info => prmap_board_info,
      register_map_hk_monitor     => prmap_hk_monitor,
      rst_soft                    => reset_soft_pcie,
      sys_reset_n                 => sys_reset_n,
      RXUSRCLK_IN                 => RXUSRCLKF);

  u2: pex_init
    generic map(
      CARD_TYPE => CARD_TYPE)
    port map(
      I2C_SMB           => I2C_SMB,
      I2C_SMBUS_CFG_nEN => I2C_SMBUS_CFG_nEN,
      MGMT_PORT_EN      => MGMT_PORT_EN,
      PCIE_PERSTn1      => PCIE_PERSTn1,
      PCIE_PERSTn2      => PCIE_PERSTn2,
      PEX_PERSTn        => PEX_PERSTn,
      PEX_SCL           => PEX_SCL,
      PEX_SDA           => PEX_SDA,
      PORT_GOOD         => PORT_GOOD,
      SHPC_INT          => SHPC_INT,
      clk40             => clk40_xtal,
      lnk_up0           => lnk0_up,
      lnk_up1           => lnk1_up,
      reset_pcie        => open,
      sys_reset_n       => sys_reset_n);

end architecture a0 ; -- of CareTaker

