--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.GetTRXControl
--! =project    FELIX_MROD
--! @modified   Fri Apr 30 17:28:27 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity GetTRXControl is
  generic(
    NUMCH      : integer := 4;
    W_ENDPOINT : integer := 0);
  port (
    EnChan            : out    std_logic_vector(NUMCH-1 downto 0);
    EnManSlide        : out    std_logic;
    FrameSize         : out    std_logic_vector(7 downto 0);
    MReset            : out    std_logic;
    RX_CHxAlignBsy    : in     std_logic_vector(NUMCH-1 downto 0);
    RX_CHxRecData     : in     std_logic_vector(NUMCH-1 downto 0);
    RX_CHxRecIdles    : in     std_logic_vector(NUMCH-1 downto 0);
    RX_CHxReset       : out    std_logic_vector(NUMCH-1 downto 0);
    SlideMax          : out    std_logic_vector(7 downto 0);
    SlideWait         : out    std_logic_vector(7 downto 0);
    TRXloopback       : out    std_logic_vector(NUMCH-1 downto 0);
    TXCVR_ResetAll    : out    std_logic_vector(NUMCH-1 downto 0);
    TX_CHxLocked      : in     std_logic_vector(NUMCH-1 downto 0);
    TX_CHxReset       : out    std_logic_vector(NUMCH-1 downto 0);
    TrxMonitor        : out    regs_trx_monitor;
    clk40             : in     std_logic;
    clk50             : in     std_logic;
    prmap_app_control : in     register_map_control_type;
    sys_reset_n       : in     std_logic);
end entity GetTRXControl;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.GetTRXControl.a0
--! =project    FELIX_MROD
--! @modified   Fri Apr 30 17:28:27 2021
--!-----------------------------------------------------------------------------


architecture a0 of GetTRXControl is

begin

  -- synchronize registers into the clock domain where they are used

  gen0: if (W_ENDPOINT = 0) generate      ----------------------------------

  pr01:
  process (clk50, sys_reset_n)
  begin
    if (sys_reset_n = '0') then
      EnChan(NUMCH-1 downto 0)      <= (others => '0');
      RX_CHxReset(NUMCH-1 downto 0) <= (others => '0');
      TRXloopback(NUMCH-1 downto 0) <= (others => '0');
      TX_CHxReset(NUMCH-1 downto 0) <= (others => '0');
      TXCVR_ResetAll(NUMCH-1 downto 0) <= (others => '0');
      EnManSlide <= '0';
      SlideMax  <= (others => '0');
      SlideWait <= (others => '0');
      FrameSize <= (others => '0');
    elsif (rising_edge(clk50)) then
      EnChan(NUMCH-1 downto 0)      <= prmap_app_control.MROD_EP0_CSMENABLE(NUMCH-1 downto 0);
      RX_CHxReset(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP0_RXRESET(NUMCH-1 downto 0);
      TRXloopback(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP0_TRXLOOPBACK(NUMCH-1 downto 0);
      TX_CHxReset(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP0_TXRESET(NUMCH-1 downto 0);
      TXCVR_ResetAll(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP0_TXCVRRESET(NUMCH-1 downto 0);
      EnManSlide <= prmap_app_control.MROD_CTRL.ENAMANSLIDE(6);
      SlideMax  <= prmap_app_control.MROD_TCVRCTRL.SLIDEMAX;
      SlideWait <= prmap_app_control.MROD_TCVRCTRL.SLIDEWAIT;
      FrameSize <= prmap_app_control.MROD_TCVRCTRL.FRAMESIZE;
    end if;
  end process;

  pr02:
  process (clk50, sys_reset_n)
    variable delay1, delay2 : std_logic;
  begin
    if (sys_reset_n = '0') then
      MReset <= '1';
      delay1 := '1';
      delay2 := '1';
      --ClearCh <= (others => '1');
    elsif (rising_edge(clk50)) then
      MReset <= delay2;
      --for i in NUMCH-1 downto 0 loop
      --  if (prmap_app_control.MROD_EP0_CLRFIFOS(i) = '1' or delay2 = '1') then
      --    ClearCh(i) <= '1';
      --  else
      --    ClearCh(i) <= '0';
      --  end if;
      --end loop;
      delay2 := delay1;
      delay1 := '0';
    end if;
  end process;

  end generate gen0;


  gen1: if (W_ENDPOINT = 1) generate      ----------------------------------

  pr11:
  process (clk50, sys_reset_n)
  begin
    if (sys_reset_n = '0') then
      EnChan(NUMCH-1 downto 0)      <= (others => '0');
      RX_CHxReset(NUMCH-1 downto 0) <= (others => '0');
      TRXloopback(NUMCH-1 downto 0) <= (others => '0');
      TX_CHxReset(NUMCH-1 downto 0) <= (others => '0');
      TXCVR_ResetAll(NUMCH-1 downto 0) <= (others => '0');
      EnManSlide <= '0';
      SlideMax  <= (others => '0');
      SlideWait <= (others => '0');
      FrameSize <= (others => '0');
    elsif (rising_edge(clk50)) then
      EnChan(NUMCH-1 downto 0)      <= prmap_app_control.MROD_EP1_CSMENABLE(NUMCH-1 downto 0);
      RX_CHxReset(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP1_RXRESET(NUMCH-1 downto 0);
      TRXloopback(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP1_TRXLOOPBACK(NUMCH-1 downto 0);
      TX_CHxReset(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP1_TXRESET(NUMCH-1 downto 0);
      TXCVR_ResetAll(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP1_TXCVRRESET(NUMCH-1 downto 0);
      EnManSlide <= prmap_app_control.MROD_CTRL.ENAMANSLIDE(6);
      SlideMax  <= prmap_app_control.MROD_TCVRCTRL.SLIDEMAX;
      SlideWait <= prmap_app_control.MROD_TCVRCTRL.SLIDEWAIT;
      FrameSize <= prmap_app_control.MROD_TCVRCTRL.FRAMESIZE;
    end if;
  end process;

  pr12:
  process (clk50, sys_reset_n)
    variable delay1, delay2 : std_logic;
  begin
    if (sys_reset_n = '0') then
      MReset <= '1';
      delay1 := '1';
      delay2 := '1';
      --ClearCh   <= (others => '1');
    elsif (rising_edge(clk50)) then
      MReset <= delay2;
      --for i in NUMCH-1 downto 0 loop
      --  if (prmap_app_control.MROD_EP1_CLRFIFOS(i) = '1' or delay2 = '1') then
      --    ClearCh(i) <= '1';
      --  else
      --    ClearCh(i) <= '0';
      --  end if;
      --end loop;
      delay2 := delay1;
      delay1 := '0';
    end if;
  end process;

  end generate gen1;

  -- synchronize monitor registers into the clk40 domain (centralrouter)

  pr20:
  process (clk40, sys_reset_n)
  begin
    if (sys_reset_n = '0') then
      TrxMonitor.RXALIGNBSY(NUMCH-1 downto 0) <= (others => '0');
      TrxMonitor.RXRECDATA(NUMCH-1 downto 0) <= (others => '0');
      TrxMonitor.RXRECIDLES(NUMCH-1 downto 0) <= (others => '0');
      TrxMonitor.TXLOCKED(NUMCH-1 downto 0) <= (others => '0');
    elsif (rising_edge(clk40)) then
      TrxMonitor.RXALIGNBSY(NUMCH-1 downto 0) <= RX_CHxAlignBsy(NUMCH-1 downto 0);
      TrxMonitor.RXRECDATA(NUMCH-1 downto 0) <= RX_CHxRecData(NUMCH-1 downto 0);
      TrxMonitor.RXRECIDLES(NUMCH-1 downto 0) <= RX_CHxRecIdles(NUMCH-1 downto 0);
      TrxMonitor.TXLOCKED(NUMCH-1 downto 0) <= TX_CHxLocked(NUMCH-1 downto 0);
    end if;
  end process;

end architecture a0 ; -- of GetTRXControl

