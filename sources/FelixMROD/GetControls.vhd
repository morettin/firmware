--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.GetControls
--! =project    FELIX_MROD
--! @modified   Sat May 15 11:05:49 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity GetControls is
  generic(
    NUMCH      : integer := 2;
    W_ENDPOINT : integer := 0);
  port (
    CSMMonitor        : out    regs_csm_monitor;
    CkEna100k         : out    std_logic;
    ClearCh           : out    std_logic_vector(NUMCH-1 downto 0);
    Empty             : in     std_logic_vector(NUMCH-1 downto 0);
    EmptySupp         : out    std_logic_vector(NUMCH-1 downto 0);
    EnaCSMFake        : out    std_logic;
    EnaCSMch          : out    std_logic_vector(NUMCH-1 downto 0);
    EnaPassAll        : out    std_logic;
    EnaReadHost       : out    std_logic;
    EnaTxCount        : out    std_logic;
    FHDEna            : out    std_logic_vector(NUMCH-1 downto 0);
    Full              : in     std_logic_vector(NUMCH-1 downto 0);
    GTMode            : out    std_logic_vector(2 downto 0);
    MReset            : out    std_logic;
    MaxTimeOut        : out    std_logic_vector(15 downto 0);
    SetHPTDC          : out    std_logic_vector(NUMCH-1 downto 0);
    clk50             : in     std_logic;
    clk80             : in     std_logic;
    prmap_app_control : in     register_map_control_type;
    sys_reset_n       : in     std_logic);
end entity GetControls;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.GetControls.a0
--! =project    FELIX_MROD
--! @modified   Sat May 15 11:05:49 2021
--!-----------------------------------------------------------------------------


architecture a0 of GetControls is

begin

  --MaxTimeOut     <= prmap_app_control.TIMEOUT_CTRL.TIMEOUT;  -- maximum timeout count
  --
  --ENASPARE1  <= prmap_app_control.MROD_CTRL.ENASPARE1(7);
  --ENAMANSLIDE<= prmap_app_control.MROD_CTRL.ENAMANSLIDE(6);
  --EnaPassAll <= prmap_app_control.MROD_CTRL.ENAPASSALL(5);   -- enable PassAll in EmptySuppress
  --EnaTxCount <= prmap_app_control.MROD_CTRL.ENATXCOUNT(4);   -- enable simpleCnt in txdriver
  --EnaCSMFake <= prmap_app_control.MROD_CTRL.GOLTESTMODE(3);  -- enable CSM pattern generator
  --GTMode(2)  <= prmap_app_control.MROD_CTRL.GOLTESTMODE(2);  -- enable triggered mode
  --GTMode(1)  <= prmap_app_control.MROD_CTRL.GOLTESTMODE(1);  -- enable circulate buffer
  --GTMode(0)  <= prmap_app_control.MROD_CTRL.GOLTESTMODE(0);  -- load(0) or run(1) test mode

  -- synchronize registers into the clock domain where they are used

  gen0: if (W_ENDPOINT = 0) generate      ----------------------------------

  pr01:
  process (clk80, sys_reset_n)
    variable check : std_logic;
  begin
    if (sys_reset_n = '0') then
      EnaCSMch(NUMCH-1 downto 0) <= (others => '0');
      FHDEna(NUMCH-1 downto 0) <= (others => '0');
      check := '0';
      EnaReadHost <= '0';
    elsif (rising_edge(clk80)) then
      EnaCSMch(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP0_CSMENABLE(NUMCH-1 downto 0);
      FHDEna(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP0_EMULOADENA(NUMCH-1 downto 0);
      check := '0';
      for i in NUMCH-1 downto 0 loop
        if (prmap_app_control.MROD_EP0_EMULOADENA(i) = '1') then
          check := '1';
        end if;
      end loop;
      EnaReadHost <= check;     -- enable when one or more bits are set
    end if;
  end process;

  pr02:
  process (clk50, sys_reset_n)
    variable delay1, delay2 : std_logic;
  begin
    if (sys_reset_n = '0') then
      SetHPTDC(NUMCH-1 downto 0) <= (others => '0');
      EmptySupp(NUMCH-1 downto 0) <= (others => '0');
      ClearCh(NUMCH-1 downto 0)  <= (others => '1');
      MReset <= '1';
      delay1 := '1';
      delay2 := '1';
    elsif (rising_edge(clk50)) then
      SetHPTDC(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP0_HPTDCMODE(NUMCH-1 downto 0);
      EmptySupp(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP0_EMPTYSUPPR(NUMCH-1 downto 0);
      MReset <= delay2;
      for i in NUMCH-1 downto 0 loop
        if (prmap_app_control.MROD_EP0_CLRFIFOS(i) = '1' or delay2 = '1') then
          ClearCh(i) <= '1';
        else
          ClearCh(i) <= '0';
        end if;
      end loop;
      delay2 := delay1;
      delay1 := '0';
    end if;
  end process;

  end generate gen0;


  gen1: if (W_ENDPOINT = 1) generate      ----------------------------------

  pr11:
  process (clk80, sys_reset_n)
    variable check : std_logic;
  begin
    if (sys_reset_n = '0') then
      EnaCSMch(NUMCH-1 downto 0) <= (others => '0');
      FHDEna(NUMCH-1 downto 0) <= (others => '0');
      check := '0';
      EnaReadHost <= '0';
    elsif (rising_edge(clk80)) then
      EnaCSMch(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP1_CSMENABLE(NUMCH-1 downto 0);
      FHDEna(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP1_EMULOADENA(NUMCH-1 downto 0);
      check := '0';
      for i in NUMCH-1 downto 0 loop
        if (prmap_app_control.MROD_EP1_EMULOADENA(i) = '1') then
          check := '1';
        end if;
      end loop;
      EnaReadHost <= check;     -- enable when one or more bits are set
    end if;
  end process;

  pr12:
  process (clk50, sys_reset_n)
    variable delay1, delay2 : std_logic;
  begin
    if (sys_reset_n = '0') then
      SetHPTDC(NUMCH-1 downto 0) <= (others => '0');
      EmptySupp(NUMCH-1 downto 0) <= (others => '0');
      ClearCh(NUMCH-1 downto 0)  <= (others => '1');
      MReset <= '1';
      delay1 := '1';
      delay2 := '1';
    elsif (rising_edge(clk50)) then
      SetHPTDC(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP1_HPTDCMODE(NUMCH-1 downto 0);
      EmptySupp(NUMCH-1 downto 0) <= prmap_app_control.MROD_EP1_EMPTYSUPPR(NUMCH-1 downto 0);
      MReset <= delay2;
      for i in NUMCH-1 downto 0 loop
        if (prmap_app_control.MROD_EP1_CLRFIFOS(i) = '1' or delay2 = '1') then
          ClearCh(i) <= '1';
        else
          ClearCh(i) <= '0';
        end if;
      end loop;
      delay2 := delay1;
      delay1 := '0';
    end if;
  end process;

  end generate gen1;


  -- synchronize registers into clk50 domain for EmptySuppress and TxDriver

  pr20:
  process (clk50, sys_reset_n)
  begin
    if (sys_reset_n = '0') then
      EnaPassAll <= '0';
      EnaTxCount <= '0';
      EnaCSMFake <= '0';
    elsif (rising_edge(clk50)) then
      EnaPassAll <= prmap_app_control.MROD_CTRL.ENAPASSALL(5);   -- bit 5
      EnaTxCount <= prmap_app_control.MROD_CTRL.ENATXCOUNT(4);   -- bit 4
      EnaCSMFake <= prmap_app_control.MROD_CTRL.GOLTESTMODE(3);  -- bit 3
    end if;
  end process;

  -- synchronize GTMode bits into clk80 domain for DataEmu
  -- synchronize monitor registers into clk80 domain for CentralRouter
  -- synchronize timeout registers into clk80 domain for PrepaChunk

  pr30:
  process (clk80, sys_reset_n)
  begin
    if (sys_reset_n = '0') then
      GTMode(2 downto 0) <= (others => '0');
      CSMMonitor.CSMH_EMPTY(NUMCH-1 downto 0) <= (others => '0');
      CSMMonitor.CSMH_FULL(NUMCH-1 downto 0)  <= (others => '0');
      MaxTimeOut <= (others => '0');
    elsif (rising_edge(clk80)) then
      GTMode(2 downto 0) <= prmap_app_control.MROD_CTRL.GOLTESTMODE(2 downto 0); -- GOLTestMode
      CSMMonitor.CSMH_EMPTY(NUMCH-1 downto 0) <= Empty(NUMCH-1 downto 0);
      CSMMonitor.CSMH_FULL(NUMCH-1 downto 0)  <= Full(NUMCH-1 downto 0);
      MaxTimeOut <= prmap_app_control.TIMEOUT_CTRL.TIMEOUT(15 downto 0);  -- maximum timeout count
    end if;
  end process;

  -- for PrepaChunk: make clock_enable (for 100 kHz clock enable)

  pr40:
  process (clk80, sys_reset_n)
    variable counter : unsigned(9 downto 0);
  begin
    if (sys_reset_n = '0') then
      counter := (others => '0');
      CkEna100k <= '0';
    elsif (rising_edge(clk80)) then       -- 80 MHz
      if (counter = ("11" & x"1f")) then  -- if (counter = 799)
        counter := (others => '0');
        CkEna100k <= '1';
      else
        counter := counter + 1;
        CkEna100k <= '0';
      end if;
    end if;
  end process;

end architecture a0 ; -- of GetControls

