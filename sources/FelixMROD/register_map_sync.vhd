--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               RHabraken
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!------------------------------------------------------------------------------
--!           NIKHEF - National Institute for Subatomic Physics
--!                       Electronics Department
--!------------------------------------------------------------------------------
--! 01-mar-2020, RH,TW: added FELIX_MROD monitor registers, drive unused registers


library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_1164.all;
use work.pcie_package.all;

entity register_map_sync is
  generic(
    NUMBER_OF_INTERRUPTS : integer := 8);
  port (
    appreg_clk                  : in     std_logic;
    clk40                       : in     std_logic;
    interrupt_call              : out    std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);
    interrupt_call_cr           : in     std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);
    register_map_40_control     : out    register_map_control_type;
    register_map_control        : in     register_map_control_type;
    register_map_cr_monitor     : in     register_map_cr_monitor_type;
    register_map_emu_monitor    : in     register_map_gbtemu_monitor_type;
    register_map_gbt_monitor    : in     register_map_gbt_monitor_type;
    register_map_gen_board_info : in     register_map_gen_board_info_type;
    register_map_hk_monitor     : in     register_map_hk_monitor_type;
    register_map_monitor        : out    register_map_monitor_type;
    register_mrod_monitor       : in     regmap_mrod_monitor_type;
    register_map_ttc_monitor    : in     register_map_ttc_monitor_type;
    register_map_xoff_monitor   : in     register_map_xoff_monitor_type;
    rst_hw                      : in     std_logic;
    rst_soft_40                 : out    std_logic;
    rst_soft_appregclk          : in     std_logic;
    master_busy_in              : in     std_logic);
end entity register_map_sync;


architecture rtl of register_map_sync is

  --attribute ASYNC_REG     : string;

  --synchronization stages to 41.667MHz
  signal interrupt_call_p1                         : std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);
  signal register_map_hk_monitor_p1                : register_map_hk_monitor_type;
  signal register_map_cr_monitor_p1                : register_map_cr_monitor_type;
  signal register_map_gbt_monitor_p1               : register_map_gbt_monitor_type;
  signal register_map_emu_monitor_p1               : register_map_gbtemu_monitor_type;
  signal register_map_mrod_monitor_p1              : regmap_mrod_monitor_type;
  signal register_map_ttc_monitor_p1               : register_map_ttc_monitor_type;
  signal register_map_xoff_monitor_p1              : register_map_xoff_monitor_type;
  
  --attribute ASYNC_REG of interrupt_call_p1            : signal is "TRUE";
  --attribute ASYNC_REG of register_map_hk_monitor_p1   : signal is "TRUE";
  --attribute ASYNC_REG of register_map_cr_monitor_p1   : signal is "TRUE";
  --attribute ASYNC_REG of register_map_gbt_monitor_p1  : signal is "TRUE";
  
  --synchronization stages to 40 MHz
  signal register_map_control_p1                   : register_map_control_type;
  signal rst_soft_p1                  : std_logic;
  --attribute ASYNC_REG of register_map_control_p1   : signal is "TRUE";
  --attribute ASYNC_REG of rst_soft_p1                  : signal is "TRUE";

begin

    clk40_sync: process(clk40)
    begin
      if(rising_edge(clk40)) then
        register_map_40_control <= register_map_control_p1;
        register_map_control_p1 <= register_map_control;
        rst_soft_40 <= rst_soft_p1 or rst_hw;
        rst_soft_p1 <= rst_soft_appregclk;
      end if;
    end process;

    appreg_sync: process(appreg_clk)
      variable master_busy_p1, master_busy_p2: std_logic;
    begin
      if(rising_edge(appreg_clk)) then
        if master_busy_p1 /= master_busy_p2 then
            interrupt_call(6) <= '1';
        else 
            interrupt_call(6) <= '0';
        end if;
        
        master_busy_p2 := master_busy_p1;
        master_busy_p1 := master_busy_in;
        
        interrupt_call(7) <= interrupt_call_p1(7);
        interrupt_call(5) <= interrupt_call_p1(5);
        interrupt_call(4) <= interrupt_call_p1(4);
        interrupt_call_p1 <= interrupt_call_cr;
        register_map_monitor.register_map_gen_board_info  <= register_map_gen_board_info; --does not need synchronization as it contains only constants
        register_map_monitor.register_map_hk_monitor      <= register_map_hk_monitor_p1;
        register_map_hk_monitor_p1                        <= register_map_hk_monitor;
        register_map_monitor.register_map_cr_monitor      <= register_map_cr_monitor_p1;
        register_map_cr_monitor_p1                        <= register_map_cr_monitor;
        register_map_monitor.register_map_gbt_monitor     <= register_map_gbt_monitor_p1;
        register_map_gbt_monitor_p1                       <= register_map_gbt_monitor;
        register_map_monitor.register_map_gbtemu_monitor  <= register_map_emu_monitor_p1;
        register_map_emu_monitor_p1                       <= register_map_emu_monitor;
        register_map_monitor.regmap_mrod_monitor          <= register_map_mrod_monitor_p1;
        register_map_mrod_monitor_p1                      <= register_mrod_monitor;
        register_map_monitor.register_map_ttc_monitor     <= register_map_ttc_monitor_p1;
        register_map_ttc_monitor_p1                       <= register_map_ttc_monitor;
        register_map_monitor.register_map_xoff_monitor    <= register_map_xoff_monitor_p1;
        register_map_xoff_monitor_p1                      <= register_map_xoff_monitor;
      end if;
    end process;

  --do_g10: if (FIRMWARE_MODE = 8) generate  -- firmware for FelixMROD
    -- drive unused registers felig_mon_ttc...
    do_g0: for i in 0 to 23 generate
      register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).L1ID  <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).XL1ID <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).BCID  <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).RESERVED0 <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).LEN   <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).FMT   <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_TTC_1(i).RESERVED1    <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_TTC_1(i).TRIGGER_TYPE <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_TTC_1(i).ORBIT <= (others => '0');
      -- drive unused registers felig_mon_xxx...
      register_map_monitor.register_map_generators.FELIG_MON_COUNTERS(i).SLIDE_COUNT <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_COUNTERS(i).FC_ERROR_COUNT <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_FREQ(i).TX <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_FREQ(i).RX <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_L1A_ID(i) <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_PICXO(i).VLOT <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_PICXO(i).ERROR <= (others => '0');
      register_map_monitor.register_map_generators.FELIG_MON_ITK_STRIPS(i) <= (others => '0');
    end generate do_g0;
    --
    -- drive unused registers fmemu_control...
    register_map_monitor.register_map_generators.FMEMU_CONTROL.INT_STATUS_EMU <= (others => '0');
    -- drive unused registers wishbone_monitor...
    register_map_monitor.wishbone_monitor.WISHBONE_WRITE.FULL <= (others => '0');
    register_map_monitor.wishbone_monitor.WISHBONE_READ.EMPTY <= (others => '0');
    register_map_monitor.wishbone_monitor.WISHBONE_READ.DATA <= (others => '0');
    register_map_monitor.wishbone_monitor.WISHBONE_STATUS.INT <= (others => '0');
    register_map_monitor.wishbone_monitor.WISHBONE_STATUS.RETRY <= (others => '0');
    register_map_monitor.wishbone_monitor.WISHBONE_STATUS.STALL <= (others => '0');
    register_map_monitor.wishbone_monitor.WISHBONE_STATUS.ACKNOWLEDGE <= (others => '0');
    register_map_monitor.wishbone_monitor.WISHBONE_STATUS.ERROR <= (others => '0');
  --end generate do_g10;

end architecture rtl ; -- of register_map_sync
     
