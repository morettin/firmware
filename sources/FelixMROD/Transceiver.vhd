--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.Transceiver
--! =project    FELIX_MROD
--! @modified   Fri Apr 30 17:28:27 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity Transceiver is
  generic(
    NUMCH      : integer := 4;
    W_ENDPOINT : integer := 0);
  port (
    EnChan            : in     std_logic_vector(NUMCH-1 downto 0);
    EnManSlide        : in     std_logic;
    FrameSize         : in     std_logic_vector(7 downto 0);
    MReset            : in     std_logic;
    QX_GTREFCLK_N     : in     std_logic_vector(4 downto 0);
    QX_GTREFCLK_P     : in     std_logic_vector(4 downto 0);
    RX_CHxAlignBsy    : out    std_logic_vector(NUMCH-1 downto 0);
    RX_CHxRecData     : out    std_logic_vector(NUMCH-1 downto 0);
    RX_CHxRecIdles    : out    std_logic_vector(NUMCH-1 downto 0);
    RX_CHxReset       : in     std_logic_vector(NUMCH-1 downto 0);
    RxClk             : out    std_logic_vector(NUMCH-1 downto 0);
    RxData            : out    slv32_array(0 to NUMCH-1);
    RxValid           : out    std_logic_vector(NUMCH-1 downto 0);
    SlideMax          : in     std_logic_vector(7 downto 0);
    SlideWait         : in     std_logic_vector(7 downto 0);
    TRXloopback       : in     std_logic_vector(NUMCH-1 downto 0);
    TXCVR_ResetAll    : in     std_logic_vector(NUMCH-1 downto 0);
    TX_CHxLocked      : out    std_logic_vector(NUMCH-1 downto 0);
    TX_CHxReset       : in     std_logic_vector(NUMCH-1 downto 0);
    TxClk             : out    std_logic_vector(NUMCH-1 downto 0);
    TxData            : in     slv33_array(0 to NUMCH-1);
    TxValid           : in     std_logic_vector(NUMCH-1 downto 0);
    clk50             : in     std_logic;
    gtrxn_in          : in     std_logic_vector(NUMCH-1 downto 0);
    gtrxp_in          : in     std_logic_vector(NUMCH-1 downto 0);
    gttxn_out         : out    std_logic_vector(NUMCH-1 downto 0);
    gttxp_out         : out    std_logic_vector(NUMCH-1 downto 0);
    prmap_app_control : in     register_map_control_type;
    sysclk_in         : in     std_logic);
end entity Transceiver;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.Transceiver.a0
--! =project    FELIX_MROD
--! @modified   Fri Apr 30 17:28:27 2021
--!-----------------------------------------------------------------------------


architecture a0 of Transceiver is

  --signal loopback_s : std_logic_vector(2 downto 0);  -- "000" = no loopback
  --loopback_s <= prmap_app_control.GTH_LOOPBACK_CONTROL;

  signal loopback : std_logic_vector(23 downto 0);
  signal txDvalid : std_logic_vector(NUMCH-1 downto 0);

begin

  RxClk <= (others => clk50);
  TxClk <= (others => clk50);

  g0: if (W_ENDPOINT = 0) generate      --
    loopback <= prmap_app_control.MROD_EP0_TRXLOOPBACK;
  end generate g0;

  g1: if (W_ENDPOINT = 1) generate      --
    loopback <= prmap_app_control.MROD_EP1_TRXLOOPBACK;
  end generate g1;

  g2: for i in NUMCH-1 downto 0 generate
  begin
    txDvalid(i) <= '1' when (TxValid(i) = '1' and TxData(i)(32) = '0') else '0';
    RxValid(i)  <= txDvalid(i) when (loopback(i) = '1') else '0';
    RxData(i)   <= TxData(i)(31 downto 0) when (txDvalid(i) = '1') else x"04000000";
  end generate g2;

  g3: for i in NUMCH-1 downto 0 generate
  begin
    gttxn_out(i) <= '1';
    gttxp_out(i) <= '0';
    TX_CHxLocked(i) <= '1';
    RX_CHxAlignBsy(i) <= '0';
    RX_CHxRecData(i)  <= '1';
    RX_CHxRecIdles(i) <= '0';
  end generate g3;

end architecture a0 ; -- of Transceiver

