--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @file        felix_mrod_package.vhd
--!
--! @authors     R. Habraken, T. Wijnen
--! @company     Radboud University Nijmegen
--! @startdate   01-Feb-2019
--! @version     1.0
--!-----------------------------------------------------------------------------
--! @brief
--! Definitions for the FELIX_MROD design.
--!
--!-----------------------------------------------------------------------------

library ieee ;
use ieee.std_logic_1164.all ;
use ieee.numeric_std.all;

package felix_mrod_package is

  type slv00_array  is array (natural range <>) of std_logic_vector( 0 downto 0);
  type slv04_array  is array (natural range <>) of std_logic_vector( 3 downto 0);
  type slv08_array  is array (natural range <>) of std_logic_vector( 7 downto 0);
  type slv16_array  is array (natural range <>) of std_logic_vector(15 downto 0);
  type slv20_array  is array (natural range <>) of std_logic_vector(19 downto 0);
  type slv24_array  is array (natural range <>) of std_logic_vector(23 downto 0);
  --! Already declared in centralRouter_package type slv32_array  is array (natural range <>) of std_logic_vector(31 downto 0);
  type slv33_array  is array (natural range <>) of std_logic_vector(32 downto 0);
  type slv36_array  is array (natural range <>) of std_logic_vector(35 downto 0);

  type usgd8_array  is array (natural range <>) of unsigned(7 downto 0);

  type regs_csm_monitor is record
    CSMH_EMPTY  : std_logic_vector(23 downto 0);    -- CSM Handler FIFO Empty 23-0
    CSMH_FULL   : std_logic_vector(23 downto 0);    -- CSM Handler FIFO Full 23-0
  end record;
  type regs_trx_monitor is record
    RXALIGNBSY  : std_logic_vector(23 downto 0);    -- Receiver Aligned monitor 23-0
    RXRECDATA   : std_logic_vector(23 downto 0);    -- Receiving Data monitor 23-0
    RXRECIDLES  : std_logic_vector(23 downto 0);    -- Receiving Idles monitor 23-0
    TXLOCKED    : std_logic_vector(23 downto 0);    -- Transmitter Locked monitor 23-0
  end record;

  constant SLV32ZERO  : std_logic_vector(31 downto 0) := x"00000000";
  constant SLV36ZERO  : std_logic_vector(35 downto 0) := x"000000000";
  constant SLV33ZERO  : std_logic_vector(32 downto 0) := '0' & x"00000000";
  --constant SLV33AZERO : slv33_array                   := (others => '0' & x"00000000");

  constant ID_SEP     : std_logic_vector( 7 downto 0) := x"D0";       -- Separator identifier
  constant ID_NOD     : std_logic_vector( 7 downto 0) := x"00";       -- NoData identifier
  constant ID_TDCER   : std_logic_vector( 3 downto 0) := x"9";        -- TDC parity Error identifier
  constant ID_GOLER   : std_logic_vector( 3 downto 0) := x"D";        -- GOL parity Error identifier
  constant ER_REPLACE : std_logic_vector(31 downto 0) := x"9000D000"; -- error replace codes
 
  --constant NUMCH      : integer := 24;  -- number of input channels (GOL links)

end package; --of package felix_mrod_package

package body felix_mrod_package is

  --  nothing so far...

end package body; --of package body felix_mrod_package

