--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Andrea Borga
--!               Enrico Gamberini
--!               Rene
--!               Thei Wijnen
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS, Nikhef.  
--! Engineer: juna, fschreud
--! 
--! Create Date:    01/03/2015  
--! Module Name:    CRFM
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen  (Radboud University Nijmegen)
--! @version    FELIX_MROD: Interface to GOL links coming from the MDT Chambers.
--! This code replaces the "CRFM" entity, adding special code
--! to adapt to the readout of the MDT chambers (uses no GBT links to frontend).
--! @modified   Thu Dec 17 10:20:00 2020
--!-----------------------------------------------------------------------------

--! Use standard library
library work, ieee, unisim;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use work.all;
use work.pcie_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.FELIX_package.all;

--! top module for CRFM logic
entity CRFM_felixmrod is
generic (
    NUMBER_OF_INTERRUPTS    : integer := 8;
    GBT_NUM                 : integer := 1;
    EnableFrHo_Egroup0Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup0Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup0Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup0Eproc8_8b10b  : boolean := true;
    EnableFrHo_Egroup1Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup1Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup1Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup1Eproc8_8b10b  : boolean := true;
    EnableFrHo_Egroup2Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup2Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup2Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup2Eproc8_8b10b  : boolean := true;
    EnableFrHo_Egroup3Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup3Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup3Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup3Eproc8_8b10b  : boolean := true;
    EnableFrHo_Egroup4Eproc2_HDLC   : boolean := true;
    EnableFrHo_Egroup4Eproc2_8b10b  : boolean := true;
    EnableFrHo_Egroup4Eproc4_8b10b  : boolean := true;
    EnableFrHo_Egroup4Eproc8_8b10b  : boolean := true;
    wideMode                : boolean := false; -- [set in top module]
    STATIC_CENTRALROUTER    : boolean := false;  -- removes update process from central router register map, only initial constant values are used
    --
    FMCH_NUM                : integer := 1;
    toHostTimeoutBitn       : integer := 8;
    GENERATE_XOFF           : boolean := true;
    CARD_TYPE               : integer := 712;
    CREnableFromHost        : boolean := true;
    SUPER_CHUNK_FACTOR      : integer := 1;
    BLOCKSIZE               : integer := 1024;
    CHUNK_TRAILER_32B       : boolean := false;
    FIRMWARE_MODE           : integer := 8
    );
port  ( 
    clk40   : in  std_logic; 
    clk80   : in  std_logic;
    clk160  : in  std_logic;
    clk250  : in  std_logic;
    rst     : in  std_logic;
    -----
    register_map_control               : in  register_map_control_type; --! configuration settings, 64 bit per EGROUP (7 EGROUPS total)
    register_map_cr_monitor            : out register_map_cr_monitor_type;
    register_map_xoff_monitor          : out register_map_xoff_monitor_type;
    register_map_control_appreg_clk    : in  register_map_control_type; --! configuration settings, 64 bit per EGROUP (7 EGROUPS total)
    appreg_clk                         : in  std_logic;
    interrupt_call                     : out std_logic_vector(NUMBER_OF_INTERRUPTS-1 downto 4);
    --------------------------------------------------------
    --- to-Host direction
    --------------------------------------------------------
    -- transceiver side
    thFMlinkValid_array : in  std_logic_vector ((FMCH_NUM-1) downto 0);
    -- bit 32 = isK, indicates if the lowest byte is a K-character
    thFMinDnT_array     : in  txrx33b_type(0 to (FMCH_NUM-1)); -- {[32-bit data and 1-bit type] x FMCH_NUM} array
    -- wupper side
    toHostFifo_din      : out std_logic_vector(255 downto 0);
    toHostFifo_wr_en    : out std_logic;
    toHostFifo_prog_full : in std_logic;
    toHostFifo_wr_clk   : out std_logic;
    toHostFifo_rst      : out std_logic;
    
    thFMXoff            : out std_logic; -- one of the Eproc FIFOs got full. Can signal data source module (emulator), test use only.
    thFMbusyOut         : out std_logic_vector ((FMCH_NUM-1) downto 0); 
    thFIFObusyOut       : out std_logic;
    --------------------------------------------------------
    --- 'from-Host' direction: normal GBT mode
    --------------------------------------------------------
    TTCin               : in  std_logic_vector (15 downto 0); 
    fromHostFifo_dout   : in  std_logic_vector(255 downto 0);
    fromHostFifo_rd_en  : out std_logic;
    fromHostFifo_empty  : in  std_logic;
    fromHostFifo_rd_clk : out std_logic;
    fromHostFifo_rst    : out std_logic;
    fhXoff              : out std_logic;
    fhOutData_array     : out txrx120b_type(0 to (GBT_NUM-1));
    TTC_ToHost_Data_in  : in TTC_ToHost_data_type;
    toHostFifo_wr_data_count : in std_logic_vector(11 downto 0)
    );
end CRFM_felixmrod;

architecture Behavioral of CRFM_felixmrod is


constant zerosGBT_NUMarray  : std_logic_vector ((GBT_NUM-1) downto 0) := (others=>'0');
constant zerosFMCH_NUMarray : std_logic_vector ((FMCH_NUM-1) downto 0) := (others=>'0');
--
signal cr_fifo_flush,cr_rst : std_logic;
----    
signal TTCin_array,xTTCin_array  : TTCin_array_type(0 to (GBT_NUM-1));
----
signal to_GBTs_array    : cr_DOUT_array_type(0 to (GBT_NUM-1));
signal from_GBTs_array  : cr_DIN_array_type(0 to (FMCH_NUM-1));
signal from_GBTs_8MSbs_array   : cr_8MSbs_array_type(0 to (FMCH_NUM-1));
---
signal fhFifoDout_array   : GBTdm_data_array_type (0 to (GBT_NUM-1));
signal fmchFifo_dout_array : d256b_array_type (0 to (FMCH_NUM));
signal fmchFifo_dvalid_array: std_logic_vector ((FMCH_NUM) downto 0); 
----
signal thch_sel, thMUXsel, thMUXsel_s : std_logic_vector (4 downto 0);
signal fmchFifo_re : std_logic;
----
signal fhMUXdout : std_logic_vector (255 downto 0);
signal fhMUXdout_valid, thMUXdin_rdy, thMUXdin_rdy_s : std_logic := '0';

type gbtFHconfig_array_type is array (0 to (GBT_NUM-1)) of crUpstreamConfig_type;
signal gbtFHconfig_array : gbtFHconfig_array_type;

signal THtransferENA, DownFifoPEmpty_s : std_logic;
signal crOUTfifo_pfull, croutfifo_full, croutfifo_empty : std_logic;
signal fmch_monitor_array : fmch_monitor_array_type(0 to FMCH_NUM-1);
----
signal busyOut_array : busyOut_array_type (0 to (FMCH_NUM-1));
----
constant zerosFMCH_NUMbit : std_logic_vector ((FMCH_NUM-1) downto 0) := (others=>'0');
signal FIFO_EMPTY_s, DownXoff_array : std_logic_vector((FMCH_NUM-1) downto 0); 
signal fmchFifo_hasBlock_array: std_logic_vector((FMCH_NUM) downto 0);

signal fmchFifo_re_array  : std_logic_vector(FMCH_NUM downto 0); 
signal fmchXoff_array     : std_logic_vector(FMCH_NUM downto 0);

----
signal UpFifoPfull_array, fhFifoWE_array, fhXoff_array : std_logic_vector ((GBT_NUM-1) downto 0) := (others=>'0');

signal crINfifo_empty, crINfifo_re, UpFifoPfull_or : std_logic;
--signal FHicFIFOfull : std_logic_vector(GBT_NUM-1 downto 0);
--
type register_queue_array_type is array (0 to (FMCH_NUM)) of std_logic_vector(63 downto 0); 
signal register_queue_array : register_queue_array_type;

signal fhFifoDout_rdy : std_logic := '0';
signal fhFifoDout  : std_logic_vector (255 downto 0);
signal thFMbusyOut_s : std_logic_vector ((FMCH_NUM-1) downto 0); 

--

signal UpFifoFull_mon_array : UpFifoFull_mon_array_type(0 to 23);

type TTC_DELAY_array_type is array (0 to 23) of std_logic_vector(3 downto 0);
signal TTC_DELAY_array : TTC_DELAY_array_type;

signal auxUpstreamConfig : std_logic_vector (0 downto 0); 

signal thFMXoff_s : std_logic;

signal upfifoDoutClk : std_logic;
constant FMCH_NUM_ZEROS: std_logic_vector(FMCH_NUM-1 downto 0) := (others => '0');

function or_reduct(slv : in std_logic_vector) return std_logic is
  variable res_v : std_logic := '0';  -- Null slv vector will also return '0'
begin
  for i in slv'range loop
    res_v := res_v or slv(i);
  end loop;
  return res_v;
end function;


signal ExtendedTestPulse    : std_logic; -- NSW required a 32x 40MHz clock pulse, 
signal TestPulseCounter     : integer range 0 to 15; -- 32 states counter RL: changed time active from 32 to 16 clks

------------------------------------------------------------------------------------------

begin

do_g0: if (FIRMWARE_MODE = 8) generate  -- firmware for FelixMROD
    -- Assign clock (FromHost direction)
    upfifoDoutClk <= clk80;
end generate do_g0;

do_g1: if (FIRMWARE_MODE /= 8) generate     -- not for FelixMROD
    --Assign multiplexer clock according to the number of channels (FromHost direction), we need at least 12.5MHz per GBT_NUM
g_upfifoclk_40M: if GBT_NUM <= 3 generate
    upfifoDoutClk <= clk40;
end generate;
g_upfifoclk_80M: if GBT_NUM > 3 and GBT_NUM<= 6 generate
    upfifoDoutClk <= clk80;
end generate;
g_upfifoclk_160M: if GBT_NUM > 6  generate
    upfifoDoutClk <= clk160;
end generate;
end generate do_g1;


------------------------------------------------------------------------------------------
-- Interrupts calls
------------------------------------------------------------------------------------------
interrupt_call(4) <= '0'; --<= register_map_control.INT_TEST_4;
interrupt_call(5) <= thFMXoff_s;
interrupt_call(6) <= crOUTfifo_pfull;
interrupt_call(7) <= croutfifo_full;    -- place holder for the GBT loss of lock interrupt, now connected to croutfifo_full

------------------------------------------------------------------------------------------
-- resets and FIFO flush, rst is from appreg_clk
------------------------------------------------------------------------------------------
rst0: entity work.CRresetManager (Behavioral)
port map( 
    clk40           => clk40,
    rst             => rst,  -- appreg_clk domain
    -- 
    cr_rst          => cr_rst,
    cr_fifo_flush   => cr_fifo_flush
    );
---

------------------------------------------------------------------------------------------
-- CRFM monitor register connection module
------------------------------------------------------------------------------------------
CRmonitors: entity work.CRFMmonitorMUX (Behavioral)
generic map(
    GBT_NUM  => GBT_NUM,
    FMCH_NUM => FMCH_NUM,
    toHostTimeoutBitn => toHostTimeoutBitn
    )
port map( 
    clk => appreg_clk,
    -----
    register_map_control    => register_map_control_appreg_clk, --register_map_control,
    register_map_cr_monitor => register_map_cr_monitor,
    --
    UpFifoFull_mon_array    => UpFifoFull_mon_array,
    fmch_monitor_array      => fmch_monitor_array
    --FHicFIFOfull_array      => FHicFIFOfull
    );
--

------------------------------------------------------------------------------------------
--
-- from-Host data: GBT-like channels (without IC/EC)
--
------------------------------------------------------------------------------------------

------------------------------------------------------------
-- TTC fanout
------------------------------------------------------------
-- mapping of the TTC delay (per GBT channel)
g_enableFromHost: if CREnableFromHost generate

TTC_DELAY_array <= (
register_map_control.TTC_DELAY( 0),register_map_control.TTC_DELAY( 1),
register_map_control.TTC_DELAY( 2),register_map_control.TTC_DELAY( 3),
register_map_control.TTC_DELAY( 4),register_map_control.TTC_DELAY( 5),
register_map_control.TTC_DELAY( 6),register_map_control.TTC_DELAY( 7),
register_map_control.TTC_DELAY(08),register_map_control.TTC_DELAY(09),
register_map_control.TTC_DELAY(10),register_map_control.TTC_DELAY(11),
register_map_control.TTC_DELAY(12),register_map_control.TTC_DELAY(13),
register_map_control.TTC_DELAY(14),register_map_control.TTC_DELAY(15),
register_map_control.TTC_DELAY(16),register_map_control.TTC_DELAY(17),
register_map_control.TTC_DELAY(18),register_map_control.TTC_DELAY(19),
register_map_control.TTC_DELAY(20),register_map_control.TTC_DELAY(21),
register_map_control.TTC_DELAY(22),register_map_control.TTC_DELAY(23));
---

process (rst, clk40)
begin
    if (rst = '1') then
        ExtendedTestPulse   <= '0'; -- clear the extended test pulse signal
        TestPulseCounter    <= 0;   -- hold the extended test pulse counter
    elsif rising_edge(clk40) then    
        -- NSW Test Pulse bit location
        if (TTCin(4) = '1') then
            ExtendedTestPulse   <= '1'; -- set the extended test pulse signal
            TestPulseCounter    <= 15;  -- initial the extended test pulse counter  RL: changed time active from 32 to 16 clks
        -- 32x 40MHz clocks pulse width
        elsif (TestPulseCounter = 0) then
            ExtendedTestPulse   <= '0'; -- clear the extended test pulse signal
            TestPulseCounter    <= 0;   -- hold the extended test pulse counter
        else
            ExtendedTestPulse   <= ExtendedTestPulse;       -- keep the value
            TestPulseCounter    <= TestPulseCounter - 1;    -- the extended test pulse counter count down
        end if;
    end if;
end process;


crFH:  for I in 0 to (GBT_NUM-1) generate 
--signal FHicFIFOdin  : std_logic_vector (7 downto 0);    
--signal FHicFIFOwe,FHicPacketRdy : std_logic;

begin
--
GBTdmOUTPUT:  for J in 0 to 4 generate -- mapping to 120 bit
     fhOutData_array(I)((32+J*16+15) downto (32+J*16)) <= to_GBTs_array(I)(J);     
end generate GBTdmOUTPUT;
-- bits (119 downto 112) are unused
fhOutData_array(I)(119 downto 112) <= "0101" & "1111"; 
--
ttcn: process(clk40)                                                                      
begin                                                                                     
    if rising_edge(clk40) then                                                            
        xTTCin_array(I)(9 downto 0) <= TTCin(9 downto 0);                                 
        xTTCin_array(I)(16 downto 11) <= TTCin(15 downto 10); -- RL added latch bits      
    end if;                                                                               
end process;                                                                              

xTTCin_array(I)(10)    <= ExtendedTestPulse; -- the ExtendedTestPulse already set in one clock delay, like in the "ttcn" process above

-- If you want to generate a per-GBT (per front end) delay, that
--  can be done here, but only in steps of the 40MHz clock.
--
--  If used connect DlydTTCin_array to upstream instead of TTCin_array.

--IG ttcFanDly : for J in 0 to 9 generate
--RL ttcFanDly : for J in 0 to 10 generate --IG: add another bit to support the extended Test Pulse
ttcFanDly : for J in 0 to 16 generate --RL changed from 10 to 16 to accomodate latch bits
  TTC_DELAY_SRL : SRL16E
  port map (
      clk => clk40,
      ce => '1', --delay_en,     --input from control register
      D => xTTCin_array(I)(J), --latched copy of TTCin
      Q => TTCin_array(I)(J), --delayed copy of TTCin
      A0 => TTC_DELAY_array(I)(0),  --input from control register
      A1 => TTC_DELAY_array(I)(1),  --input from control register
      A2 => TTC_DELAY_array(I)(2),  --input from control register
      A3 => TTC_DELAY_array(I)(3)  --input from control register
      );
end generate;
--

--
------------------------------------------------------------
-- from-Host configuration 
------------------------------------------------------------
cfg0: entity work.crFHconfigMap (behavioral)
generic map(
    GBTid                   => I,
    wideMode                => false,
    crInternalLoopbackMode  => false,
    TTC_test_mode           => false,
    STATIC_CENTRALROUTER    => STATIC_CENTRALROUTER
    )
port map( 
    clk                                => clk40,
    register_map_control               => register_map_control,
    register_map_control_appreg_clk    => register_map_control_appreg_clk,
    appreg_clk                         => appreg_clk,
    --FHicFIFOwe                         => FHicFIFOwe,    -- out, comes from a trigger register  
    --FHicFIFOdin                        => FHicFIFOdin,   -- out 8 bit, comes from a register
    --FHicPacketRdy                      => FHicPacketRdy, -- out, comes from a register
    -----
    crUpstreamConfig        => gbtFHconfig_array(I),
    auxUpstreamConfig       => auxUpstreamConfig
    );
    
--
------------------------------------------------------------
-- GBT data manager: Upstream, to GBT (from-Host)
------------------------------------------------------------
GBTdmUp: entity work.GBTdmUpstream (Behavioral)
generic map(
    GBT_NUM             => GBT_NUM,
    GBTid               => I,
    EnableFrHo_Egroup0Eproc2_HDLC  => EnableFrHo_Egroup0Eproc2_HDLC,
    EnableFrHo_Egroup0Eproc2_8b10b => EnableFrHo_Egroup0Eproc2_8b10b,
    EnableFrHo_Egroup0Eproc4_8b10b => EnableFrHo_Egroup0Eproc4_8b10b,
    EnableFrHo_Egroup0Eproc8_8b10b => EnableFrHo_Egroup0Eproc8_8b10b,
    EnableFrHo_Egroup1Eproc2_HDLC  => EnableFrHo_Egroup1Eproc2_HDLC,
    EnableFrHo_Egroup1Eproc2_8b10b => EnableFrHo_Egroup1Eproc2_8b10b,
    EnableFrHo_Egroup1Eproc4_8b10b => EnableFrHo_Egroup1Eproc4_8b10b,
    EnableFrHo_Egroup1Eproc8_8b10b => EnableFrHo_Egroup1Eproc8_8b10b,
    EnableFrHo_Egroup2Eproc2_HDLC  => EnableFrHo_Egroup2Eproc2_HDLC,
    EnableFrHo_Egroup2Eproc2_8b10b => EnableFrHo_Egroup2Eproc2_8b10b,
    EnableFrHo_Egroup2Eproc4_8b10b => EnableFrHo_Egroup2Eproc4_8b10b,
    EnableFrHo_Egroup2Eproc8_8b10b => EnableFrHo_Egroup2Eproc8_8b10b,
    EnableFrHo_Egroup3Eproc2_HDLC  => EnableFrHo_Egroup3Eproc2_HDLC,
    EnableFrHo_Egroup3Eproc2_8b10b => EnableFrHo_Egroup3Eproc2_8b10b,
    EnableFrHo_Egroup3Eproc4_8b10b => EnableFrHo_Egroup3Eproc4_8b10b,
    EnableFrHo_Egroup3Eproc8_8b10b => EnableFrHo_Egroup3Eproc8_8b10b,
    EnableFrHo_Egroup4Eproc2_HDLC  => EnableFrHo_Egroup4Eproc2_HDLC,
    EnableFrHo_Egroup4Eproc2_8b10b => EnableFrHo_Egroup4Eproc2_8b10b,
    EnableFrHo_Egroup4Eproc4_8b10b => EnableFrHo_Egroup4Eproc4_8b10b,
    EnableFrHo_Egroup4Eproc8_8b10b => EnableFrHo_Egroup4Eproc8_8b10b,
    wideMode            => wideMode,
    GENERATE_XOFF       => GENERATE_XOFF,
    CARD_TYPE           => CARD_TYPE
    )
port map( 
    clk40      => clk40,
    clk80      => clk80,
    clk160     => clk160,
    clk240     => upfifoDoutClk,
    rst        => rst,
    -----
    register_map_control => register_map_control,
    gbtUpstreamConfig   => gbtFHconfig_array(I),
    auxUpstreamConfig   => auxUpstreamConfig,
    ----- from-host (upstream) IC e-link fifo write driver
    --FHicFIFOwclk        => appreg_clk,
    --FHicFIFOwe          => FHicFIFOwe,      -- in, comes from a trigger register  
    --FHicFIFOfull        => FHicFIFOfull(I),    -- out, goes to a register      
    --FHicFIFOdin         => FHicFIFOdin,     -- in 8 bit, comes from a register
    --FHicPacketRdy       => FHicPacketRdy,   -- in, comes from a register  
    -----
    TTCin               => TTCin_array(I),
    fhCR_REVERSE_10B    => to_sl(register_map_control.CR_REVERSE_10B.FROMHOST),
    -- TX side
    GBTdm_DOUT_array    => to_GBTs_array(I),
    ICnEC_uplinks       => open, 
    -- wupper side
    fifoWCLK    => upfifoDoutClk,
    fifoWE      => fhFifoWE_array(I),
    fifoDin     => fhFifoDout_array(I),   
    fifoPfull   => UpFifoPfull_array(I),  
    fifoFLUSH   => cr_fifo_flush,  
    -----
    xoff        => fhXoff_array(I),
    toHostXoff  => fmchXoff_array(GBT_NUM-1 downto 0),
    -----
    monitor_vec => UpFifoFull_mon_array(I)
    );
-- 
end generate crFH;
--
------------------------------------------------------------
-- from Host input fifo
------------------------------------------------------------
--fanout to all GBTs/Eprocs/Paths!!! (filtered locally)
--
UpFifoPfull_or  <= '0' when (UpFifoPfull_array = zerosGBT_NUMarray) else '1'; -- can't write to at least one GBT wm fifo -> stop reading from crINfifo
crINfifo_re     <= (not crINfifo_empty) and (not UpFifoPfull_or) and (not cr_rst);
--
fhFifoDout<= fromHostFifo_dout;
fromHostFifo_rd_en <= crINfifo_re;
crINfifo_empty <= fromHostFifo_empty;
fromHostFifo_rd_clk <= upfifoDoutClk;
fromHostFifo_rst <= cr_fifo_flush;
--
fhXoff  <= '0' when (fhXoff_array = zerosGBT_NUMarray) else '1';
--
end generate g_enableFromHost;

do_g3: if (CREnableFromHost = false and FIRMWARE_MODE = 8) generate     -- for FelixMROD

fhXoff_array   <= (others => '0');  -- no fhXoff_array output from GBTdmUpstream
UpFifoPfull_or <= '0';              -- no UpFifoPfull_array output from GBTdmUpstream
crINfifo_re    <= '1' when ((crINfifo_empty = '0') and (UpFifoPfull_or = '0') and (cr_rst = '0')) else '0';
-- if I can't write (UpFifoPfull/=0) to at least one GBT wm fifo, then stop reading from crINfifo

fhFifoDout<= fromHostFifo_dout;
fromHostFifo_rd_en <= crINfifo_re;
crINfifo_empty <= fromHostFifo_empty;
fromHostFifo_rd_clk <= upfifoDoutClk;
fromHostFifo_rst <= cr_fifo_flush;

fhXoff  <= '0' when (fhXoff_array = zerosGBT_NUMarray) else '1';

end generate do_g3;

--
--fhFifoPfull <= fhFifoPfull_s; -- goes to wupper
--
--
process(upfifoDoutClk) -- 1-clock pipeline 
begin
    if rising_edge(upfifoDoutClk) then
        fhFifoDout_rdy <= crINfifo_re;
    end if;
end process; 
--
UpFifoDout_fanout:  for I in 0 to (GBT_NUM-1) generate   
UpFifoDoutN: process(upfifoDoutClk) -- 1-clock pipeline, fanout to wm fifos
begin
    if rising_edge(upfifoDoutClk) then
        fhFifoDout_array(I) <= fhFifoDout;
        fhFifoWE_array(I)   <= fhFifoDout_rdy;
    end if;
end process;
end generate UpFifoDout_fanout;
--

------------------------------------------------------------------------------------------
--
-- to-Host data: Full Mode channels 
--
------------------------------------------------------------------------------------------
thFMdataManagers:  for I in 0 to (FMCH_NUM-1) generate 
begin
thFMdmN: entity work.thFMdm
generic map(
    FMCHid                  => I,
    STATIC_CENTRALROUTER    => STATIC_CENTRALROUTER,
    toHostTimeoutBitn       => toHostTimeoutBitn,
    SUPER_CHUNK_FACTOR      => SUPER_CHUNK_FACTOR,
    BLOCKSIZE               => BLOCKSIZE,
    CHUNK_TRAILER_32B       => CHUNK_TRAILER_32B,
    FIRMWARE_MODE           => FIRMWARE_MODE,
    Phase2CRToHost_FM       => false
    )
port map(
    clk40                      => clk40,
    clk250                     => clk250,
    clk240                     => '0', --only used if phase2 / axis implementation is used
    rxusrclk                   => '0', --only used if phase2 / axis implementation is used
    clk_wrth                   => clk80,    -- clock for write to host
    rst40                      => cr_rst,
    rst250                     => cr_rst,
    register_map_control       => register_map_control, 
    register_map_control_appreg_clk     => register_map_control_appreg_clk, --register_map_control,
    appreg_clk                          => appreg_clk,
    -- RX side
    FMCHdin         => thFMinDnT_array(I)(31 downto 0), -- 32 bit
    FMCHdtype       => thFMinDnT_array(I)(32), -- isK-array, indicates if the lowest byte is a K-character
    FM_Emu_dout     => (others => '0'), -- only used if phase2 / axis implementation is used
    FMCHlink_valid  => thFMlinkValid_array(I),       
    -- wupper side: 
    -- 1. read from specified channel
    fmchFifo_flush      => cr_fifo_flush,
    fmchFifo_re         => fmchFifo_re_array(I), 
    fmchFifo_dout       => fmchFifo_dout_array(I),
    fmchFifo_dvalid     => fmchFifo_dvalid_array(I),
    fmchFifo_hasBlock   => fmchFifo_hasBlock_array(I), -- out, 'block_ready' flag
    fmchXoffout         => fmchXoff_array(I), -- out test purposes only, flag to a data source to stop transmitting
    fmchHighThreshCrossed      => register_map_xoff_monitor.XOFF_FM_HIGH_THRESH.CROSS_LATCHED(I+24),
    fmchHighThreshCrossedLatch => register_map_xoff_monitor.XOFF_FM_HIGH_THRESH.CROSSED(I),
    fmchLowThreshCrossed       => register_map_xoff_monitor.XOFF_FM_LOW_THRESH_CROSSED(I),
    --fmchXoffin          => croutfifo_full, -- in, crOUTfifo is pfull 4060/3830 (out of 4096), stop writing flag 
    --
    fmch_monitor        => fmch_monitor_array(I), -- out
    fmch_busy           => thFMbusyOut_s(I) -- one busy bit per fm channel  
    );
end generate thFMdataManagers;
--
thFMbusyOut <= thFMbusyOut_s;

------------------------------------------------------------------------
-- Downstream direction : to Host
------------------------------------------------------------------------
TTCtoHost: entity work.TTCtoHost_channel
generic map(
    GBTid                   => FMCH_NUM,
    generate_IC_EC_TTC_only => false,
    TimeoutCounterBitNum    => 12 -- IG: number of timeout counter bits
    )
port  map(
    clk40       => clk40,
    clk240      => clk250,
    rst_clk40   => cr_rst,
    FifoFlush   => cr_fifo_flush,
    -----
    TTC_ToHost_ena      => register_map_control.CR_TTC_TOHOST.ENABLE(0),                            -- TTC_ToHost_ena,
    TTC_ToHost_Data_in  => TTC_ToHost_Data_in,
    -----
    TTC_ToHost_TO_ena   => register_map_control.CR_TTC_TOHOST.TIMEOUT_ENABLE(1),                    -- IG: enable TTC's timeout 
    TTC_ToHost_TO_max   => register_map_control.CR_TTC_TOHOST.TIMEOUT_VALUE(15 downto 4),           -- IG: set the maximum timeout counting
    -----
    TTC_ToHost_emu_ena  => register_map_control.CR_TTC_TOHOST.EMU_ENABLE(2),                        -- IG: enabling the emulator
    TTC_ToHost_Fake_ena => register_map_control.CR_TTC_TOHOST.EMU_FAKE_READY_ENABLE(63),            -- IG: enable the fake ready signals from the emulator
    TTC_ToHost_Fake_Val => register_map_control.CR_TTC_TOHOST.EMU_FAKE_READY_VALUE(60 downto 48),   -- IG: set the value of the fake ready signals counter (vector is one bit bigger then the TTC_ToHost_TO_max)
    -----
    FIFOdout      => fmchFifo_dout_array(FMCH_NUM),
    FIFOhasBlock  => fmchFifo_hasBlock_array(FMCH_NUM),
    FIFOre        => fmchFifo_re_array(FMCH_NUM),
    FIFOempty     => open,
    FIFOdvalid    => fmchFifo_dvalid_array(FMCH_NUM),
    -----
    xoff_in       => croutfifo_full
    );

------------------------------------------------------------
-- writing to the cr OUT FIFO / reading from channel fifos
------------------------------------------------------------
THPCIeM: entity work.thfmPCIeManager 
generic map(
    FMCH_NUM => FMCH_NUM+1,
    BLOCKSIZE => BLOCKSIZE
    )
port map(
    clk             => clk250,
    rst             => cr_rst,              -- reset is deasserted after fifo flush!
    thch_rdy_array  => fmchFifo_hasBlock_array, -- in, 'block_ready' flags from GBTdms
    PCIe_ena        => THtransferENA,       -- in, '1' when crOUTfifo is ready to accept 1 block (not fifo pfull)
    thch_sel        => thch_sel,            -- out, data mux select
    thch_re_array   => fmchFifo_re_array,   -- out, enables reading from single fmchannel at a time
    thch_re         => fmchFifo_re          -- out, re 1 bit
    );
--
process(clk250, cr_rst) -- write to crOUTfifo 2 clk pipeline before data mux, aligned with data
begin
    if cr_rst  = '1' then
        --thMUXdin_rdy_s  <= '0';
        --thMUXdin_rdy    <= '0';
        thMUXsel_s      <= (others=>'1'); 
        thMUXsel        <= (others=>'1'); 
    elsif rising_edge(clk250) then
        --thMUXdin_rdy_s  <= fmchFifo_re;
        --thMUXdin_rdy    <= thMUXdin_rdy_s; -- data is ready 2 clk250s after re
        thMUXsel_s      <= thch_sel;
        thMUXsel        <= thMUXsel_s;
    end if;
end process;


thMUXdin_rdy <= or_reduct(fmchFifo_dvalid_array);

--
dataMUXn: entity work.MUXn_d256b (Behavioral) 
  generic map(N        => FMCH_NUM+1,
              sel_bitn => 5)            -- max 24
  port map(
    clk             => clk250,
    data            => fmchFifo_dout_array,
    data_rdy        => thMUXdin_rdy,
    sel             => thMUXsel,
    data_out        => fhMUXdout,
    data_out_rdy    => fhMUXdout_valid
    );
--
------------------------------------------------------------
-- to Host output fifo
------------------------------------------------------------

toHostFifo_din <= fhMUXdout;
toHostFifo_wr_en <= fhMUXdout_valid;
crOUTfifo_pfull <= toHostFifo_prog_full;
toHostFifo_wr_clk <= clk250;
toHostFifo_rst <= cr_fifo_flush;

--
process(clk250, cr_rst) -- 
begin
    if cr_rst  = '1' then
        THtransferENA   <= '0';
        croutfifo_full  <= '0';
    elsif rising_edge(clk250) then
        if crOUTfifo_pfull = '1' then -- hardcoded hysteresis on pfull flag: 4060/3830 (out of 4096)
            THtransferENA   <= '0';
            croutfifo_full  <= '1';
        else --
            THtransferENA   <= '1';
            croutfifo_full  <= '0';
        end if;
    end if;
end process;
--
process(clk250, cr_rst) -- test purposes only, flag to a data source to stop transmitting
begin
    if cr_rst  = '1' then
        thFMXoff_s  <= '0';
    elsif rising_edge(clk250) then
        if fmchXoff_array = (fmchXoff_array'range => '0') and THtransferENA = '1' then
            thFMXoff_s  <= '0';
        else 
            thFMXoff_s  <= '1';
        end if;
    end if;
end process;

thFMXoff <= thFMXoff_s;

busymon_proc: process(clk40, cr_rst)
begin
    if cr_rst = '1' then
        register_map_xoff_monitor.FM_BUSY_CHANNEL_STATUS.BUSY_LATCHED <= (others => '0');
    elsif rising_edge(clk40) then
        for i in 0 to FMCH_NUM-1 loop
            if(thFMbusyOut_s(i) = '1') then
                register_map_xoff_monitor.FM_BUSY_CHANNEL_STATUS.BUSY_LATCHED(i+24) <= '1';
            end if;
            if(register_map_control.FM_BUSY_CHANNEL_STATUS.CLEAR_LATCH="1") then
                register_map_xoff_monitor.FM_BUSY_CHANNEL_STATUS.BUSY_LATCHED(i+24) <= '0';
            end if;
            register_map_xoff_monitor.FM_BUSY_CHANNEL_STATUS.BUSY(i) <= thFMbusyOut_s(i);
        end loop;
    end if;
end process;

busy_pciefifo_proc: process(clk40, cr_rst)
begin
    if cr_rst = '1' then
        register_map_xoff_monitor.BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED <= "0";
    elsif rising_edge(clk40) then
        if toHostFifo_wr_data_count < register_map_control.BUSY_MAIN_OUTPUT_FIFO_THRESH.LOW then
            register_map_xoff_monitor.BUSY_MAIN_OUTPUT_FIFO_STATUS.LOW_THRESH_CROSSED <= "0";
            thFIFObusyOut <= '0';
        else
            register_map_xoff_monitor.BUSY_MAIN_OUTPUT_FIFO_STATUS.LOW_THRESH_CROSSED <= "1";
        end if;
        if toHostFifo_wr_data_count > register_map_control.BUSY_MAIN_OUTPUT_FIFO_THRESH.HIGH then
            register_map_xoff_monitor.BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED <= "1";
            register_map_xoff_monitor.BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED <= "1";
            if register_map_control.BUSY_MAIN_OUTPUT_FIFO_THRESH.BUSY_ENABLE = "1" then
                thFIFObusyOut <= '1';
            end if;
        else
            register_map_xoff_monitor.BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED <= "0";
        end if;
        if register_map_control.BUSY_MAIN_OUTPUT_FIFO_STATUS.CLEAR_LATCHED = "1" then
            register_map_xoff_monitor.BUSY_MAIN_OUTPUT_FIFO_STATUS.HIGH_THRESH_CROSSED_LATCHED <= "0";
        end if;
    end if;
end process;


-- XOFF monitoring
xoff_monitoring_generate: for CH in 0 to FMCH_NUM-1 generate
    xoff_monitoring: entity work.XoffMonitoring
        generic map (
            DURATION_COUNTER_WIDTH => 64,
            XOFF_COUNTER_WIDTH => 64
        )
        port map (
            clk => clk40,
            reset => cr_rst,
            xoff => fmchXoff_array(CH),
            peak_duration => register_map_xoff_monitor.XOFF_PEAK_DURATION(CH),
            total_duration => register_map_xoff_monitor.XOFF_TOTAL_DURATION(CH),
            xoff_count => register_map_xoff_monitor.XOFF_COUNT(CH)
        );
end generate;

do_g10: if (FIRMWARE_MODE = 8) generate  -- firmware for FelixMROD
    --
    -- drive unused data from host to gbt...
    do_g11: for i in 0 to (GBT_NUM-1) generate
      fhOutData_array(i) <= (others => '0');        -- open output in CR_Wupper
      UpFifoFull_mon_array(i) <= (others => '0');   -- for CRFMmonitorMUX
    end generate do_g11;
    --
    -- drive unused registers xoff_monitor.DMA...
    --register_map_xoff_monitor.DMA_BUSY_STATUS.FROMHOST_BUSY <= (others => '0');
    --register_map_xoff_monitor.DMA_BUSY_STATUS.FROMHOST_BUSY_LATCHED <= (others => '0');
    register_map_xoff_monitor.DMA_BUSY_STATUS.TOHOST_BUSY   <= (others => '0');
    register_map_xoff_monitor.DMA_BUSY_STATUS.TOHOST_BUSY_LATCHED   <= (others => '0');
    --
end generate do_g10;

end Behavioral;

