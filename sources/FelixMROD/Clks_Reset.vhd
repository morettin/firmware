--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.Clks_Reset
--! =project    FELIX_MROD
--! @modified   Tue Jun  4 12:14:23 2019
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity Clks_Reset is
  generic(
    APP_CLK_FREQ           : integer := 200;
    USE_BACKUP_CLK         : boolean := true;
    AUTOMATIC_CLOCK_SWITCH : boolean := true);
  port (
    CDRlocked           : in     std_logic;
    CLK40_FPGA2LMK_N    : out    std_logic;
    CLK40_FPGA2LMK_P    : out    std_logic;
    LMK_CLK             : out    std_logic;
    LMK_DATA            : out    std_logic;
    LMK_GOE             : out    std_logic;
    LMK_LD              : in     std_logic;
    LMK_LE              : out    std_logic;
    LMK_Locked          : out    std_logic;
    LMK_SYNCn           : out    std_logic;
    MMCM_Locked         : out    std_logic;
    MMCM_OscSelect      : out    std_logic;
    app_clk_in_n        : in     std_logic;
    app_clk_in_p        : in     std_logic;
    clk10_xtal          : out    std_logic;
    clk160              : out    std_logic;
    clk240              : out    std_logic;
    clk250              : out    std_logic;
    clk40               : out    std_logic;
    clk40_ttc_ref_out_n : out    std_logic;
    clk40_ttc_ref_out_p : out    std_logic;
    clk40_xtal          : out    std_logic;
    clk50               : out    std_logic;
    clk80               : out    std_logic;
    clk_ttc_40_s        : in     std_logic;
    clk_ttcfx_ref1_in_n : in     std_logic;
    clk_ttcfx_ref1_in_p : in     std_logic;
    clk_ttcfx_ref2_in_n : in     std_logic;
    clk_ttcfx_ref2_in_p : in     std_logic;
    prmap_app_control   : in     register_map_control_type;
    rst_hw              : out    std_logic;
    sys_reset_n         : in     std_logic);
end entity Clks_Reset;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.Clks_Reset.a0
--! =project    FELIX_MROD
--! @modified   Tue Jun  4 12:14:23 2019
--!-----------------------------------------------------------------------------


architecture a0 of Clks_Reset is

  component clock_and_reset
    generic(
      APP_CLK_FREQ            : integer := 200;
      USE_BACKUP_CLK          : boolean := true; -- true to use 100/200 MHz board crystal, false to use TTC clock
      AUTOMATIC_CLOCK_SWITCH  : boolean := false);
    port (
      MMCM_Locked_out       : out    std_logic;
      MMCM_OscSelect_out    : out    std_logic;
      app_clk_in_n          : in     std_logic;
      app_clk_in_p          : in     std_logic;
      cdrlocked_in          : in     std_logic;
      clk10_xtal            : out    std_logic;
      clk160                : out    std_logic;
      clk240                : out    std_logic;
      clk250                : out    std_logic;
      clk320                : out    std_logic;
      clk40                 : out    std_logic;
      clk40_xtal            : out    std_logic;
      clk80                 : out    std_logic;
      clk50                 : out    std_logic;
      clk_adn_160           : in     std_logic;
      clk_adn_160_out_n     : out    std_logic;
      clk_adn_160_out_p     : out    std_logic;
      clk_ttc_40            : in     std_logic;
      clk_ttcfx_mon1        : out    std_logic;
      clk_ttcfx_mon2        : out    std_logic;
      clk_ttcfx_ref1_in_n   : in     std_logic;
      clk_ttcfx_ref1_in_p   : in     std_logic;
      clk_ttcfx_ref2_in_n   : in     std_logic;
      clk_ttcfx_ref2_in_p   : in     std_logic;
      clk_ttcfx_ref2_out_n  : out    std_logic;
      clk_ttcfx_ref2_out_p  : out    std_logic;
      clk_ttcfx_ref_out_n   : out    std_logic;
      clk_ttcfx_ref_out_p   : out    std_logic;
      register_map_control  : in     register_map_control_type;
      reset_out             : out    std_logic;  --! Active high reset out (synchronous to clk40)
      sys_reset_n           : in     std_logic); --! Active low reset input.
  end component clock_and_reset;

  component LMK03200_wrapper
    port (
      rst_lmk           : in     std_logic;
      hw_rst            : in     std_logic;
      LMK_locked        : out    std_logic;
      CLK40_FPGA2LMK_P  : out    std_logic;
      CLK40_FPGA2LMK_N  : out    std_logic;
      LMK_DATA          : out    std_logic;
      LMK_CLK           : out    std_logic;
      LMK_LE            : out    std_logic;
      LMK_GOE           : out    std_logic;
      LMK_LD            : in     std_logic;
      LMK_SYNCn         : out    std_logic;
      clk40m_in         : in     std_logic;
      clk10m_in         : in     std_logic);
  end component LMK03200_wrapper;

  -------------

  signal rst_hw_l       : std_logic;
  signal clk10_xtal_l   : std_logic;
  signal clk40_xtal_l   : std_logic;

begin

  rst_hw      <= rst_hw_l;      -- output
  clk10_xtal  <= clk10_xtal_l;  -- output
  clk40_xtal  <= clk40_xtal_l;  -- output

  clk0: clock_and_reset
    generic map(
      APP_CLK_FREQ           => APP_CLK_FREQ,
      USE_BACKUP_CLK         => USE_BACKUP_CLK,
      AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH)
    port map(
      MMCM_Locked_out      => MMCM_Locked,
      MMCM_OscSelect_out   => MMCM_OscSelect,
      app_clk_in_n         => app_clk_in_n,
      app_clk_in_p         => app_clk_in_p,
      cdrlocked_in         => CDRlocked,
      clk10_xtal           => clk10_xtal_l,
      clk160               => clk160,
      clk240               => clk240,
      clk250               => clk250,
      clk320               => open,
      clk40                => clk40,
      clk40_xtal           => clk40_xtal_l,
      clk80                => clk80,
      clk50                => clk50,
      clk_adn_160          => '0',
      clk_adn_160_out_n    => open,
      clk_adn_160_out_p    => open,
      clk_ttc_40           => clk_ttc_40_s,
      clk_ttcfx_mon1       => open,
      clk_ttcfx_mon2       => open,
      clk_ttcfx_ref1_in_n  => clk_ttcfx_ref1_in_n,
      clk_ttcfx_ref1_in_p  => clk_ttcfx_ref1_in_p,
      clk_ttcfx_ref2_in_n  => clk_ttcfx_ref2_in_n,
      clk_ttcfx_ref2_in_p  => clk_ttcfx_ref2_in_p,
      clk_ttcfx_ref2_out_n => open,
      clk_ttcfx_ref2_out_p => open,
      clk_ttcfx_ref_out_n  => clk40_ttc_ref_out_n,
      clk_ttcfx_ref_out_p  => clk40_ttc_ref_out_p,
      register_map_control => prmap_app_control,        -- to pcie0
      reset_out            => rst_hw_l,
      sys_reset_n          => sys_reset_n);

  lmk_init0: LMK03200_wrapper
    port map(
      rst_lmk          => '0',
      hw_rst           => rst_hw_l,
      LMK_locked       => LMK_Locked,       -- output to housekeeping
      CLK40_FPGA2LMK_P => CLK40_FPGA2LMK_P,
      CLK40_FPGA2LMK_N => CLK40_FPGA2LMK_N,
      LMK_DATA         => LMK_DATA,
      LMK_CLK          => LMK_CLK,
      LMK_LE           => LMK_LE,
      LMK_GOE          => LMK_GOE,
      LMK_LD           => LMK_LD,
      LMK_SYNCn        => LMK_SYNCn,
      clk40m_in        => clk40_xtal_l,
      clk10m_in        => clk10_xtal_l);

end architecture a0 ; -- of Clks_Reset

