--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.FifoCSMdata
--! =project    FELIX_MROD
--! @modified   Wed May 13 14:57:05 2020
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity FifoCSMdata is
  port (
    D      : in     std_logic_vector(31 downto 0);
    WrReq  : in     std_logic;
    WClk   : in     std_logic;
    MReset : in     std_logic;
    Full   : out    std_logic;
    RClk   : in     std_logic;
    RdReq  : in     std_logic;
    Empty  : out    std_logic;
    Q      : out    std_logic_vector(31 downto 0);
    QValid : out    std_logic);
end entity FifoCSMdata;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.FifoCSMdata.a0
--! =project    FELIX_MROD
--! @modified   Wed May 13 14:57:05 2020
--!-----------------------------------------------------------------------------


architecture a0 of FifoCSMdata is

  -- (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved. 
  -- IP VLNV: xilinx.com:ip:fifo_generator:13.2
  -- IP Revision: 2
  -- You must compile the wrapper file fifo_4096x36_80_50.vhd when simulating
  -- the core, fifo_4096x36_80_50. When compiling the wrapper file, be sure to
  -- reference the VHDL simulation library.
  -- read clock 80MHz, write clock 50MHz, prog_full: 4088.

  component fifo_4096x36_80_50
    port (
      srst : in std_logic;
      wr_clk : in std_logic;
      rd_clk : in std_logic;
      din : in std_logic_vector ( 35 downto 0 );
      wr_en : in std_logic;
      rd_en : in std_logic;
      dout : out std_logic_vector ( 35 downto 0 );
      full : out std_logic;
      empty : out std_logic;
      valid : out std_logic;
      prog_full : out std_logic;
      wr_rst_busy : out std_logic;
      rd_rst_busy : out std_logic
    );
  end component;

  signal wD : std_logic_vector(35 downto 0);
  signal wQ : std_logic_vector(35 downto 0);

begin

  wD <= "0000" & D;
  Q  <= wQ(31 downto 0);

  u1 : fifo_4096x36_80_50
  port map (
    srst   => MReset,
    wr_clk => WClk,
    rd_clk => RClk,
    din    => wD,
    wr_en  => WrReq,
    rd_en  => RdReq,
    dout   => wQ,
    full   => open,
    empty  => Empty,
    valid  => QValid,
    prog_full => Full,
    wr_rst_busy => open,
    rd_rst_busy => open
  );

end architecture a0 ; -- of FifoCSMdata

