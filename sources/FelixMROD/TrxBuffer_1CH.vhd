--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.TrxBuffer
--! =project    FELIX_MROD
--! @modified   Fri Apr 30 17:23:45 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity TrxBuffer is
  generic(
    NUMCH      : integer := 4;
    W_ENDPOINT : integer := 0); -- select Wupper_Endpoint partition
  port (
    QX_GTREFCLK_N     : in     std_logic_vector(4 downto 0);
    QX_GTREFCLK_P     : in     std_logic_vector(4 downto 0);
    RXtoGTH_N         : in     std_logic_vector(NUMCH-1 downto 0);
    RXtoGTH_P         : in     std_logic_vector(NUMCH-1 downto 0);
    RxClk             : out    std_logic_vector(NUMCH-1 downto 0);
    RxData            : out    slv32_array(0 to NUMCH-1);
    RxValid           : out    std_logic_vector(NUMCH-1 downto 0);
    TXfrGTH_N         : out    std_logic_vector(NUMCH-1 downto 0);
    TXfrGTH_P         : out    std_logic_vector(NUMCH-1 downto 0);
    TrxMonitor        : out    regs_trx_monitor;
    TxClk             : out    std_logic_vector(NUMCH-1 downto 0);
    TxData            : in     slv33_array(0 to NUMCH-1);
    TxValid           : in     std_logic_vector(NUMCH-1 downto 0);
    clk40             : in     std_logic;
    clk50             : in     std_logic;
    prmap_app_control : in     register_map_control_type;
    sys_reset_n       : in     std_logic);
end entity TrxBuffer;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.TrxBuffer.a0
--! =project    FELIX_MROD
--! @modified   Fri Apr 30 17:23:45 2021
--!-----------------------------------------------------------------------------

architecture a0 of TrxBuffer is

  signal MReset         : std_logic;
  signal RX_CHxReset    : std_logic_vector(NUMCH-1 downto 0);
  signal TX_CHxReset    : std_logic_vector(NUMCH-1 downto 0);
  signal TRXloopback    : std_logic_vector(NUMCH-1 downto 0);
  signal TX_CHxLocked   : std_logic_vector(NUMCH-1 downto 0);
  signal EnChan         : std_logic_vector(NUMCH-1 downto 0);
  signal TXCVR_ResetAll : std_logic_vector(NUMCH-1 downto 0);
  signal EnManSlide     : std_logic;
  signal SlideMax       : std_logic_vector(7 downto 0);
  signal SlideWait      : std_logic_vector(7 downto 0);
  signal FrameSize      : std_logic_vector(7 downto 0);
  signal RX_CHxAlignBsy : std_logic_vector(NUMCH-1 downto 0);
  signal RX_CHxRecData  : std_logic_vector(NUMCH-1 downto 0);
  signal RX_CHxRecIdles : std_logic_vector(NUMCH-1 downto 0);

begin

  u10: entity work.GetTRXControl(a0)
    generic map(
      NUMCH      => NUMCH,
      W_ENDPOINT => W_ENDPOINT)
    port map(
      EnChan            => EnChan,
      EnManSlide        => EnManSlide,
      FrameSize         => FrameSize,
      MReset            => MReset,
      RX_CHxAlignBsy    => RX_CHxAlignBsy,
      RX_CHxRecData     => RX_CHxRecData,
      RX_CHxRecIdles    => RX_CHxRecIdles,
      RX_CHxReset       => RX_CHxReset,
      SlideMax          => SlideMax,
      SlideWait         => SlideWait,
      TRXloopback       => TRXloopback,
      TXCVR_ResetAll    => TXCVR_ResetAll,
      TX_CHxLocked      => TX_CHxLocked,
      TX_CHxReset       => TX_CHxReset,
      TrxMonitor        => TrxMonitor,
      clk40             => clk40,
      clk50             => clk50,
      prmap_app_control => prmap_app_control,
      sys_reset_n       => sys_reset_n);

  u1: entity work.TransceiverGen1CH(a0)
    generic map(
      NUMCH      => NUMCH,
      W_ENDPOINT => W_ENDPOINT)
    port map(
      EnChan            => EnChan,
      EnManSlide        => EnManSlide,
      FrameSize         => FrameSize,
      MReset            => MReset,
      QX_GTREFCLK_N     => QX_GTREFCLK_N,
      QX_GTREFCLK_P     => QX_GTREFCLK_P,
      RX_CHxAlignBsy    => RX_CHxAlignBsy,
      RX_CHxRecData     => RX_CHxRecData,
      RX_CHxRecIdles    => RX_CHxRecIdles,
      RX_CHxReset       => RX_CHxReset,
      RxClk             => RxClk,
      RxData            => RxData,
      RxValid           => RxValid,
      SlideMax          => SlideMax,
      SlideWait         => SlideWait,
      TRXloopback       => TRXloopback,
      TXCVR_ResetAll    => TXCVR_ResetAll,
      TX_CHxLocked      => TX_CHxLocked,
      TX_CHxReset       => TX_CHxReset,
      TxClk             => TxClk,
      TxData            => TxData,
      TxValid           => TxValid,
      clk50             => clk50,
      gtrxn_in          => RXtoGTH_N,
      gtrxp_in          => RXtoGTH_P,
      gttxn_out         => TXfrGTH_N,
      gttxp_out         => TXfrGTH_P,
      prmap_app_control => prmap_app_control,
      sysclk_in         => clk50);

end architecture a0 ; -- of TrxBuffer

