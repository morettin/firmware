--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.Fifo4096w
--! =project    FELIX_MROD
--! @modified   Wed Dec  9 16:18:12 2020
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity Fifo4096w is
  port (
    D      : in     std_logic_vector(31 downto 0);
    Empty  : out    std_logic;
    Full   : out    std_logic;
    MReset : in     std_logic;
    Q      : out    std_logic_vector(31 downto 0);
    QValid : out    std_logic;
    RdReq  : in     std_logic;
    WClk   : in     std_logic;
    WrReq  : in     std_logic);
end entity Fifo4096w;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.Fifo4096w.a0
--! =project    FELIX_MROD
--! @modified   Wed Dec  9 16:18:12 2020
--!-----------------------------------------------------------------------------


architecture a0 of Fifo4096w is

  -- (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved.
  -- IP VLNV: xilinx.com:ip:fifo_generator:13.2
  -- IP Revision: 2
  -- You must compile the wrapper file fifo_4096x36_80s.vhd when simulating
  -- the core, fifo_4096x36_80s. When compiling the wrapper file, be sure to
  -- reference the VHDL simulation library.
  -- read/write clock 80MHz, prog_full: 4088.

  component fifo_4096x36_80s
    port (
      clk : in std_logic;
      srst : in std_logic;
      din : in std_logic_vector(35 DOWNTO 0);
      wr_en : in std_logic;
      rd_en : in std_logic;
      dout : out std_logic_vector(35 DOWNTO 0);
      full : out std_logic;
      empty : out std_logic;
      valid : out std_logic;
      prog_full : out std_logic;
      wr_rst_busy : out std_logic;
      rd_rst_busy : out std_logic
    );
  end component;

  signal Dinp: std_logic_vector(35 downto 0);
  signal DOut: std_logic_vector(35 downto 0);

begin

  Dinp(35 downto 0) <= "0000" & D(31 downto 0);
  Q(31 downto 0) <= DOut(31 downto 0);
  -- fifo uses only one clock (WClk) for write and read

  uc1 : fifo_4096x36_80s
  port map (
    clk    => WClk,
    srst   => MReset,
    din    => Dinp,
    wr_en  => WrReq,
    rd_en  => RdReq,
    dout   => DOut,
    full   => open,
    empty  => Empty,
    valid  => QValid,
    prog_full => Full,
    wr_rst_busy => open,
    rd_rst_busy => open
  );

end architecture a0 ; -- of Fifo4096w

