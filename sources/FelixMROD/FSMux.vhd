--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.FSMux
--! =project    FELIX_MROD
--! @modified   Mon Sep 30 15:13:41 2019
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity FSMux is
  port (
    FHDEna : in     std_logic;
    GTMode : in     std_logic_vector(2 downto 0);
    MReset : in     std_logic;
    Q      : out    std_logic_vector(31 downto 0);
    QValid : out    std_logic;
    SClk   : in     std_logic;
    WD     : in     std_logic_vector(31 downto 0);
    WDEN   : in     std_logic;
    WF     : in     std_logic_vector(31 downto 0);
    WFEN   : in     std_logic);
end entity FSMux;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.FSMux.a0
--! =project    FELIX_MROD
--! @modified   Mon Sep 30 15:13:41 2019
--!-----------------------------------------------------------------------------


architecture a0 of FSMux is

  -- GTMode(0) : 1/0 : enable / disable CSM testrun
  -- GTMode(1) : 1/0 : circulate fifo / fill fifo from processor (via fromhost fifo)
  --           :     : when circulating: replace BOT+EOT
  -- GTMode(2) : 1/0 : triggered / untriggered test mode

begin

  -- store the multiplexed data into the circulate fifo

  pr1:
  process (SClk, MReset)
  begin
    if (MReset = '1') then
      QValid <= '0';
      Q      <= (others => '0');
    elsif (rising_edge(SClk)) then
      if (GTMode(1) = '0') then         -- GTMode(1)=0: fill new data from host
        if (WDEN = '1' and FHDEna = '1') then
          QValid <= '1';
          Q      <= WD;                 -- fill this fifo with data from FromHostFifo
        else
          QValid <= '0';
        end if;
      else                              -- GTMode(1)=1: circulate data
        if (WFEN = '1') then
          QValid <= '1';
          Q      <= WF;                 -- fill this fifo with data from circulate fifo
        else
          QValid <= '0';
        end if;
      end if;
    end if;
  end process;

end architecture a0 ; -- of FSMux

