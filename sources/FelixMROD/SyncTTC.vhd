--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.SyncTTC
--! =project    FELIX_MROD
--! @modified   Sat May 15 11:11:25 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity SyncTTC is
  port (
    ECR     : out    std_logic;
    MReset  : in     std_logic;
    TTC_out : in     std_logic_vector(15 downto 0);
    Trigger : out    std_logic;
    clk40   : in     std_logic;
    clk80   : in     std_logic);
end entity SyncTTC;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.SyncTTC.a0
--! =project    FELIX_MROD
--! @modified   Sat May 15 11:11:25 2021
--!-----------------------------------------------------------------------------


architecture a0 of SyncTTC is

  signal Start1, Start2 : std_logic;        -- registered signals (40 MHz domain)
  signal Started1, Started2 : std_logic;    -- OR of input and registered signal
  signal Valid1, Valid2 : std_logic;        -- output signals (80 MHz domain)

  type states1 is (Idle1, Check1, Pause1);
  signal state1 :  states1;                 -- Current State
  type states2 is (Idle2, Check2, Pause2);
  signal state2 :  states2;                 -- Current State

begin

  Trigger <= Valid1;
  ECR     <= Valid2;

  pr1:
  process (clk40, MReset)
  begin
    if (MReset = '1') then
      Start1 <= '0';
      Start2 <= '0';
    elsif (rising_edge(clk40)) then
      Start1 <= TTC_out(0);         -- L1A from TTC
      Start2 <= TTC_out(3);         -- ECR from TTC
    end if;
  end process;

  pr2:
  process (clk80, MReset)
  begin
    if (MReset = '1') then
      Started1 <= '0';
      Started2 <= '0';
    elsif (rising_edge(clk80)) then
      if (TTC_out(0) = '1' or Start1 = '1') then
        Started1 <= '1';
      else
        Started1 <= '0';
      end if;
      if (TTC_out(3) = '1' or Start2 = '1') then
        Started2 <= '1';
      else
        Started2 <= '0';
      end if;
    end if;
  end process;

  pr3:
  process (clk80, MReset)
  begin
    if (MReset = '1') then
      Valid1 <= '0';
      state1 <= Idle1;
    elsif (rising_edge(clk80)) then
      case state1 is
      when Idle1 =>
        Valid1 <= '0';
        if (Started1 = '1') then        -- wait for Start
          state1 <= Check1;
        else
          state1 <= Idle1;
        end if;
      when Check1 =>
        if (Started1 = '1') then
          Valid1 <= '1';                -- set valid signal (one cylce)
          state1 <= Pause1;
        else
          Valid1 <= '0';
          state1 <= Idle1;
        end if;
      when Pause1 =>
        Valid1 <= '0';
        if (Started1 = '1') then        -- wait until released
          state1 <= Pause1;
        else
          state1 <= Idle1;
        end if;
      when others =>
        state1 <= Idle1;
      end case;
    end if;
  end process;

  pr4:
  process (clk80, MReset)
  begin
    if (MReset = '1') then
      Valid2 <= '0';
      state2 <= Idle2;
    elsif (rising_edge(clk80)) then
      case state2 is
      when Idle2 =>
        Valid2 <= '0';
        if (Started2 = '1') then        -- wait for Start
          state2 <= Check2;
        else
          state2 <= Idle2;
        end if;
      when Check2 =>
        if (Started2 = '1') then
          Valid2 <= '1';                -- set valid signal (one cylce)
          state2 <= Pause2;
        else
          Valid2 <= '0';
          state2 <= Idle2;
        end if;
      when Pause2 =>
        Valid2 <= '0';
        if (Started2 = '1') then
          state2 <= Pause2;
        else
          state2 <= Idle2;
        end if;
      when others =>
        state2 <= Idle2;
      end case;
    end if;
  end process;

end architecture a0 ; -- of SyncTTC

