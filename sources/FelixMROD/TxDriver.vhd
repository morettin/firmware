--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.TxDriver
--! =project    FELIX_MROD
--! @modified   Mon Apr 19 15:58:34 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity TxDriver is
  generic(
    TxOn : boolean := true);
  port (
    DF         : in     std_logic_vector(31 downto 0);
    DValid     : in     std_logic;
    Empty      : in     std_logic;
    EnaTxCount : in     std_logic;
    MReset     : in     std_logic;
    QF         : out    std_logic_vector(32 downto 0);
    QValid     : out    std_logic;
    RClk       : in     std_logic;
    RdReq      : out    std_logic;
    TBusy      : in     std_logic);
end entity TxDriver;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.TxDriver.a0
--! =project    FELIX_MROD
--! @modified   Mon Apr 19 15:58:34 2021
--!-----------------------------------------------------------------------------


architecture a0 of TxDriver is

  --GOL IDLE = <D16.2>,<K28.5>,<D16.2>,<K28.5>
  --constant IDLE_Data: std_logic_vector (31 downto 0) := x"50BC50BC";
  --note that the lowest byte (xBC) is sent first.

  type states is (Idle, FirstW, CheckF, FRead, LastW);
  signal mstate : states;

  signal dcnt   : unsigned(4 downto 0);
  signal ccnt   : unsigned(3 downto 0);
  signal idcode : std_logic_vector(7 downto 0);
  signal EnRead : std_logic;
  signal EValid : std_logic;
  signal data   : std_logic_vector(32 downto 0);

  constant K_idle : std_logic_vector(32 downto 0) := '1' & x"50BC50BC";

  -- input EnaTxCount : generate simple counter data (19 words + 2 idles) for test purposes

begin

  RdReq <= '1' when (EnRead = '1' and Empty = '0') else '0';  -- and TBusy = '0'?
  QF <= data;

  pr0:
  process(RClk, MReset)
  begin
    if (MReset = '1') then
      EnRead <= '0';
      QValid <= '0';
      EValid <= '0';
      data   <= (others => '0');
      dcnt   <= "00000";
      ccnt   <= "0000";
      idcode <= x"D0";
      mstate <= Idle;
    elsif (rising_edge(RClk)) then
      case mstate is
      when Idle =>
        if (TxOn = true) then
          QValid <= '1';
          data <= K_idle;               -- always send idles
        else
          QValid <= '0';
          data <= (others => '0');      -- off
        end if;
        dcnt   <= "00000";
        ccnt   <= "0000";
        idcode <= x"D0";
        EnRead <= '0';
        if (Empty = '0' or EnaTxCount = '1') then
          mstate <= FirstW;             -- start sending data
        else
          mstate <= Idle;
        end if;
      when FirstW =>
        QValid <= '1';                  -- start idle_1   (from Idle state)
        data <= K_idle;                 -- or send idle_2 (from LastW state)
        dcnt   <= "00000";              --
        idcode <= x"D0";                -- for begin of next cycle
        if (Empty = '0') then           -- if fifo not empty:
          EnRead <= '1';
        else
          EnRead <= '0';
        end if;
        mstate <= CheckF;
      when CheckF =>
        if (DValid = '1' or EValid = '1') then
          QValid <= '1';                -- "last" word valid
          data <= '0' & DF;             -- data from Fifo (last word)
          dcnt <= dcnt + "00001";       -- count words
        elsif (EnaTxCount = '1') then
          QValid <= '1';                -- word valid, set pattern
          if (idcode = x"D0") then
            data <= '0' & idcode & x"000000";
          else
            data <= '0' & idcode & "000" & std_logic_vector(dcnt)
            & std_logic_vector(ccnt) & std_logic_vector(ccnt) & "000" & std_logic_vector(dcnt);
          end if;
          dcnt <= dcnt + "00001";       -- count words
          idcode <= x"04";              -- next idcode
        else
          QValid <= '1';                -- send nothing is not an option
          data <= K_idle;               -- send another idle
        end if;
        EValid <= '0';
        if (Empty = '0') then           -- if fifo not empty:
          EnRead <= '1';
          mstate <= FRead;
        elsif (EnaTxCount = '1') then
          if (dcnt = "10010") then      -- if at word 18, this one is last
            mstate <= LastW;            -- reached end of cycle
          else
            mstate <= CheckF;
          end if;
        else
          EnRead <= '0';
          mstate <= CheckF;
        end if;
      when FRead =>
        if (DValid = '1') then
          QValid <= '1';                -- "last" word valid
          data <= '0' & DF;             -- data from Fifo (last word)
          if (dcnt = "10010") then      -- if at word 18, this one is last
            EnRead <= '0';              -- (doing separator + 18 TDCs)
            dcnt <= dcnt + "00001";     -- count words
            mstate <= LastW;            -- reached end of cycle
          else
            EnRead <= '1';
            dcnt <= dcnt + "00001";     -- count words
            mstate <= FRead;
          end if;
        else
          QValid <= '1';                -- send nothing is not an option
          data <= K_idle;               -- send another idle
          --dcnt <= dcnt + "00001";       -- do not count words on idles
          if (Empty = '1') then         -- if fifo is empty (or reached end)
            EnRead <= '0';
            mstate <= LastW;            -- truncate last cycle
          else
            EnRead <= '1';
            mstate <= FRead;
          end if;
        end if;
      when LastW =>
        dcnt <= dcnt + "00001";         -- should be "10011" when I get here
        ccnt <= ccnt + "0001";          -- count the number of cycles (sep+18tdc)
        QValid <= '1';                  --
        data <= K_idle;                 -- end of cycle; send idle_2
        EnRead <= '0';
        -- if there is valid data or if the simple count pattern is turned on:
        if (DValid = '1') then
          EValid <= '1';                -- one pending word
          mstate <= FirstW;             -- do next cycle
        elsif (EnaTxCount = '1') then
          EValid <= '0';
          mstate <= FirstW;             -- do next cycle
        else
          EValid <= '0';
          mstate <= Idle;               -- hold
        end if;
      when others =>
        mstate <= Idle;
      end case;
    end if;
  end process;

end architecture a0 ; -- of TxDriver

