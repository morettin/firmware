--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Rene
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--file: GlueBox_EMU.vhd
--!-----------------------------------------------------------------------------
--! @authors    R. Habraken, T. Wijnen
--! @company    Radboud University Nijmegen
--! @startdate  01-Feb-2019
--! @version    1.0
--! @project    FELIX_MROD: MROD functionality implemented on a FELIX board.
--!-----------------------------------------------------------------------------
--! @brief
--! Use a FELIX board to interface to GOL links coming from the MDT Chambers.
--! Provides a new type of interface to possibly replace the MROD system.
--!
--!-----------------------------------------------------------------------------

--!-----------------------------------------------------------------------------
--! @object     Entity design.GlueBox
--! =project    FELIX_MROD
--! @modified   Thu Jun 24 22:31:47 2021
--!-----------------------------------------------------------------------------

library ieee, work;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.felix_mrod_package.all;
use work.centralRouter_package.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity GlueBox is
  generic(
    NUMCH : integer := 2);
  port (
    Q2_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q2_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    Q4_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q4_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    Q5_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q5_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    Q6_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q6_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    Q8_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
    Q8_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
    QX_GTREFCLK_N             : out    std_logic_vector(4 downto 0);
    QX_GTREFCLK_P             : out    std_logic_vector(4 downto 0);
    --RXA_N                     : out    std_logic_vector(NUMCH-1 downto 0);
    --RXA_P                     : out    std_logic_vector(NUMCH-1 downto 0);
    --RXB_N                     : out    std_logic_vector(NUMCH-1 downto 0);
    --RXB_P                     : out    std_logic_vector(NUMCH-1 downto 0);
    --RX_N                      : in     std_logic_vector(NUMCH*2-1 downto 0);
    --RX_P                      : in     std_logic_vector(NUMCH*2-1 downto 0);
    TXA_N                     : in     std_logic_vector(NUMCH-1 downto 0);
    TXA_P                     : in     std_logic_vector(NUMCH-1 downto 0);
    TXB_N                     : in     std_logic_vector(NUMCH-1 downto 0);
    TXB_P                     : in     std_logic_vector(NUMCH-1 downto 0);
    --TX_N                      : out    std_logic_vector(NUMCH*2-1 downto 0);
    --TX_P                      : out    std_logic_vector(NUMCH*2-1 downto 0);
    pcie_rxhin                : out    std_logic_vector(7 downto 0);
    pcie_rxhip                : out    std_logic_vector(7 downto 0);
    pcie_rxlon                : out    std_logic_vector(7 downto 0);
    pcie_rxlop                : out    std_logic_vector(7 downto 0);
    pcie_rxn                  : in     std_logic_vector(15 downto 0);
    pcie_rxp                  : in     std_logic_vector(15 downto 0);
    pcie_txhin                : in     std_logic_vector(7 downto 0);
    pcie_txhip                : in     std_logic_vector(7 downto 0);
    pcie_txlon                : in     std_logic_vector(7 downto 0);
    pcie_txlop                : in     std_logic_vector(7 downto 0);
    pcie_txn                  : out    std_logic_vector(15 downto 0);
    pcie_txp                  : out    std_logic_vector(15 downto 0));
end entity GlueBox;

--!-----------------------------------------------------------------------------
--! @object     Architecture design.GlueBox.a0
--! =project    FELIX_MROD
--! @modified   Thu Jun 24 22:31:47 2021
--!-----------------------------------------------------------------------------


architecture a0 of GlueBox is

  constant MAXCH : natural := 24;

begin

  -- Split RX and combine TX for (CSM/GOL) transceiver input links. (max. 2x24)
  -- Mix and un-mix in steps of 4 channels to keep  channels at the same
  -- physical tranceiver no matter the NUMCH. See felix_minipod_BNL712xxx_MROD.xdc
  -- The result is that transceivers in EP0 are connected to minipod A/B 0-11
  -- and the transceivers in EP1 are connected to minipod C/D 0-11.
  -- The link numbers in both endpoints will be 0-n (0-3 or 0-11 or 0-23).

  --g_cha: for i in 0 to 5 generate
    --g_do: if (i < NUMCH/4) generate
      --RXA_N((i*4)+3 downto (i*4)) <= RX_N( (i*4)+3 downto (i*4));
      --RXA_P((i*4)+3 downto (i*4)) <= RX_P( (i*4)+3 downto (i*4));
      --RXB_N((i*4)+3 downto (i*4)) <= RX_N( (i*4)+3+MAXCH downto (i*4)+MAXCH);
      --RXB_P((i*4)+3 downto (i*4)) <= RX_P( (i*4)+3+MAXCH downto (i*4)+MAXCH);
      --
      --TX_N( (i*4)+3 downto (i*4)) <= TXA_N((i*4)+3 downto (i*4));
      --TX_P( (i*4)+3 downto (i*4)) <= TXA_P((i*4)+3 downto (i*4));
      --TX_N( (i*4)+3+MAXCH downto (i*4)+MAXCH) <= TXB_N((i*4)+3 downto (i*4));
      --TX_P( (i*4)+3+MAXCH downto (i*4)+MAXCH) <= TXB_P((i*4)+3 downto (i*4));
    --end generate g_do;
  --end generate g_cha;

  -- combine GTREFCLK
  QX_GTREFCLK_N(0) <= Q2_CLK0_GTREFCLK_PAD_N_IN;
  QX_GTREFCLK_P(0) <= Q2_CLK0_GTREFCLK_PAD_P_IN;
  QX_GTREFCLK_N(1) <= Q8_CLK0_GTREFCLK_PAD_N_IN;
  QX_GTREFCLK_P(1) <= Q8_CLK0_GTREFCLK_PAD_P_IN;
  QX_GTREFCLK_N(2) <= Q5_CLK0_GTREFCLK_PAD_N_IN;
  QX_GTREFCLK_P(2) <= Q5_CLK0_GTREFCLK_PAD_P_IN;
  QX_GTREFCLK_N(3) <= Q4_CLK0_GTREFCLK_PAD_N_IN;
  QX_GTREFCLK_P(3) <= Q4_CLK0_GTREFCLK_PAD_P_IN;
  QX_GTREFCLK_N(4) <= Q6_CLK0_GTREFCLK_PAD_N_IN;
  QX_GTREFCLK_P(4) <= Q6_CLK0_GTREFCLK_PAD_P_IN;

  -- split RX and combine TX for PCIE.
  pcie_rxhin  <= pcie_rxn(15 downto 8);
  pcie_rxhip  <= pcie_rxp(15 downto 8);
  pcie_rxlon  <= pcie_rxn(7 downto 0);
  pcie_rxlop  <= pcie_rxp(7 downto 0);
  pcie_txn    <= pcie_txhin & pcie_txlon;
  pcie_txp    <= pcie_txhip & pcie_txlop;

end architecture a0 ; -- of GlueBox

