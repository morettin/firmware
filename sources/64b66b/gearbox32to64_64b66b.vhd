--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

------------------------------------------------------------------------------------
----! Company:  Argonne National Laboratory
----! Engineer: Marco Trovato
----!
----! Create Date:    26/06/2019
----! Module Name:    aurora_64b66b_0_core
----! Project Name:   FELIX
------------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.package_64b66b.all;


entity  gearbox32to64_64b66b is
    Port (
        reset    : in std_logic;
        clk40_in : in std_logic;
        data_in  : in data32VHHV_64b66b_type;
        data_out : out data64VHHVE_64b66b_type
    );
end gearbox32to64_64b66b;

architecture Behavioral of gearbox32to64_64b66b is
    signal count_1b       : std_logic := '0';
    signal data_i         : data32VHHV_64b66b_type;
    signal data_new_i     : data64VHHVE_64b66b_type;

begin
    data_i <= data_in;

    count_proc: process(clk40_in)
        variable first_time : std_logic := '1';
    begin
        if rising_edge(clk40_in) then
            if(reset = '1') then
                count_1b         <= '0';
                first_time       := '1';
            elsif(data_i.valid = '1') then
                if (first_time = '1' and data_i.data(31 downto 24) = x"78") then
                    count_1b     <= '1';
                    first_time   := '0';
                elsif (first_time = '0') then
                    if (count_1b = '0') then
                        count_1b <= '1';
                    else
                        count_1b <= '0';
                    end if;
                else
                    count_1b     <= count_1b;
                end if;

            else
                count_1b         <= count_1b;
            end if;
        end if;
    end process;

    data_proc: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if(reset = '1') then
                data_new_i                        <= data64VHHVE_64b66b_type_zero;
            elsif (data_i.valid = '1') then
                if (count_1b = '0') then
                    data_new_i.data(63 downto 32) <= data_i.data;
                    data_new_i.valid              <= data_i.valid;
                    data_new_i.hdr                <= data_i.hdr;
                    data_new_i.hdr_valid          <= data_i.hdr_valid;
                    data_new_i.en                 <= '0';
                else
                    data_new_i.data(31 downto 0)  <= data_i.data;
                    data_new_i.en                 <=  '1';
                end if; -- if(count_1b = '0')
            else
                data_new_i.data                   <= data_new_i.data;
                data_new_i.valid                  <= '0';
                data_new_i.hdr                    <= data_new_i.hdr;
                data_new_i.hdr_valid              <= data_new_i.hdr_valid;
                data_new_i.en                     <= '0';
            end if;
        end if; --if rising_edge(clk40_in)
    end process;
    data_out <=  data_new_i;

end Behavioral;


