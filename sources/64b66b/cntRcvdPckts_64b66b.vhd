--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.ALL;
    use IEEE.numeric_std_unsigned.ALL;


library UNISIM;
    use UNISIM.VCOMPONENTS.all;


entity cntRcvdPckts_64b66b is
    port (
        rst              : in std_logic;
        clk40            : in std_logic;
        data_in          : in std_logic_vector(6 downto 0);
        datav_in         : in std_logic;
        ref_packet_in    : in std_logic_vector(6 downto 0);
        cnt_packets_out  : out std_logic_vector(31 downto 0)

    );
end cntRcvdPckts_64b66b;

architecture Behavioral of cntRcvdPckts_64b66b is
    signal cnt_packets_i: std_logic_vector(31 downto 0);
begin

    process(clk40)
    begin
        if rising_edge(clk40) then
            if( rst = '1' ) then
                cnt_packets_i <= (others => '0');
            elsif (data_in = ref_packet_in and datav_in = '1' ) then
                cnt_packets_i <= cnt_packets_i + x"00000001";
            end if;
        end if;
    end process;
    cnt_packets_out <= cnt_packets_i;


end Behavioral;
