--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  Nikhef, Argonne National Laboratory
--! Engineer: Frans Schreuder (initially written by), Marco Trovato
--(adapted by)
--!
--! Create Date:    26/06/2019
--! Module Name:    64b66b_DecodingGearBox
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--/////////////////////////////////////////////////////////////////////////////
--  Description:
--/////////////////////////////////////////////////////////////////////////////


-- Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;

entity decodingGearbox_64b66b is
    generic     (
        IN_WIDTH     : integer := 32;
        GB_OUT_WIDTH : integer := 66
    );
    port    (
        MsbFirst         : in std_logic;
        Reset            : in std_logic;
        clk40            : in std_logic;
        DataIn           : in std_logic_vector(IN_WIDTH-1 downto 0);
        LinkAligned      : in std_logic;
        DataOut          : out std_logic_vector(31 downto 0);
        DataOutValid     : out std_logic;
        HeaderOut        : out std_logic_vector(1 downto 0);
        HeaderOutValid   : out std_logic;
        BitSlip : in std_logic


    );
end decodingGearbox_64b66b;

architecture rtl of decodingGearbox_64b66b is
    signal buf_GB            : std_logic_vector((IN_WIDTH+GB_OUT_WIDTH)-1 downto 0);
    signal BitsInBuf_GB      : integer range 0 to (IN_WIDTH+GB_OUT_WIDTH);
    signal DataOut_GB        : std_logic_vector((GB_OUT_WIDTH-1) downto 0);
    signal DataOutValid_GB   : std_logic;

    signal Count_max         : integer range 0 to (GB_OUT_WIDTH-2)/IN_WIDTH;
    signal Count             : integer range 0 to (GB_OUT_WIDTH-2)/IN_WIDTH;
    signal BitsInBuf         : integer range 0 to (IN_WIDTH);
    signal Data_i            : std_logic_vector(IN_WIDTH-1 downto 0);
begin

    MSBfirstLoop: for I in IN_WIDTH-1 downto 0 generate
        Data_i(I) <= DataIn(I) when MsbFirst = '1' else
                     DataIn(IN_WIDTH-1-I);
    end generate;

    shift_proc: process(clk40, Reset)
        variable DataOut_v: std_logic_vector(GB_OUT_WIDTH-1 downto 0);
    begin
        if (Reset = '1') then
            buf_GB          <= (others => '0');
            DataOut_GB      <= (others => '0');
            DataOutValid_GB <= '0';
            BitsInBuf_GB    <= 0;
        elsif rising_edge(clk40) then
            DataOutValid_GB <= '0';
            if (LinkAligned = '1') then
                for i in buf_GB'high downto 0 loop
                    if(i>=IN_WIDTH) then
                        buf_GB(i)     <= buf_GB(i-IN_WIDTH);
                    else
                        if(i < IN_WIDTH) then
                            buf_GB(i) <= Data_i(i);
                        end if;
                    end if;
                end loop;

                if (BitsInBuf_GB < GB_OUT_WIDTH) then
                    if BitSlip = '1' then
                        BitsInBuf_GB  <= BitsInBuf_GB + IN_WIDTH - 1;
                    else
                        BitsInBuf_GB  <= BitsInBuf_GB + IN_WIDTH;
                    end if;
                    DataOutValid_GB   <= '0';
                else
                    for i in GB_OUT_WIDTH-1 downto 0 loop
                        DataOut_v(i)  := buf_GB(BitsInBuf_GB-((GB_OUT_WIDTH)-i));
                    end loop;
                    DataOut_GB        <= DataOut_v;
                    DataOutValid_GB   <= '1';
                    if BitSlip = '1' then
                        BitsInBuf_GB  <= BitsInBuf_GB + (IN_WIDTH - GB_OUT_WIDTH) - 1;
                    else
                        BitsInBuf_GB  <= BitsInBuf_GB + (IN_WIDTH - GB_OUT_WIDTH);
                    end if;
                end if;
            end if;
        end if;
    end process;

    Count_max <= (GB_OUT_WIDTH-2)/IN_WIDTH;
    preparedataout_proc: process(clk40, Reset)
    begin
        if (Reset = '1') then
            DataOut                <= (others => '0');
            DataOutValid           <= '0';
            HeaderOut              <=  (others => '0');
            HeaderOutValid         <=  '0';
            Count                  <= 0;
            BitsInBuf              <= 0;
        elsif rising_edge(clk40) then
            if (DataOutValid_GB = '1') then
                DataOut(IN_WIDTH-1 downto 0) <= DataOut_GB(GB_OUT_WIDTH-3 downto GB_OUT_WIDTH-3-(IN_WIDTH-1));
                if (IN_WIDTH = 32) then
                    DataOutValid   <= '1';
                    BitsInBuf      <= 0;
                else
                    DataOutValid   <= '0';
                    BitsInBuf      <= IN_WIDTH;
                end if;
                HeaderOut          <= DataOut_GB(GB_OUT_WIDTH-1 downto GB_OUT_WIDTH-2);
                HeaderOutValid     <= '1';
                Count              <= 1;
            elsif (Count /= Count_max) then
                DataOut            <= DataOut_GB(GB_OUT_WIDTH-3-Count*IN_WIDTH downto GB_OUT_WIDTH-3-((Count+1)*IN_WIDTH-1));
                if (BitsInBuf + IN_WIDTH = 32) then
                    DataOutValid   <= '1';
                    BitsInBuf      <= 0;
                else
                    DataOutValid   <= '0';
                    BitsInBuf      <= BitsInBuf + IN_WIDTH;
                end if;
                HeaderOut          <= (others => '0');
                HeaderOutValid     <= '0';
                Count              <= Count + 1;
            else
                DataOut            <= (others => '0');
                DataOutValid       <= '0';
                HeaderOut          <= (others => '0');
                HeaderOutValid     <= '0';
                Count              <= 0;
            end if;
        end if;
    end process;

end rtl;
