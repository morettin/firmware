library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    --use work.axi_stream_package.all;
    use work.centralRouter_package.all;
    use ieee.numeric_std.all;
    use IEEE.numeric_std_unsigned.ALL;
    use work.pcie_package.all;
    --use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;

library UNISIM;
    use UNISIM.VComponents.all;

entity descrambler_64b66b is
    generic (

        RX_DATA_WIDTH : integer := 32
    );
    port (
        clk40_in      : in std_logic;
        reset         : in std_logic;
        data_in       : in std_logic_vector(0 to RX_DATA_WIDTH-1);
        data_v_in     : in std_logic;
        data_out      : out std_logic_vector(0 to RX_DATA_WIDTH-1)
    );
end descrambler_64b66b;

architecture Behavioral of descrambler_64b66b is

    signal descrambler        : std_logic_vector(57 downto 0)               := (others => '0');
    signal poly_out           : std_logic_vector(57 downto 0)               := (others => '0');
    signal tempData_out       : std_logic_vector(0 to RX_DATA_WIDTH-1);
    signal unscrambled_data_i : std_logic_vector(RX_DATA_WIDTH-1 downto 0);
    signal xorBit_out         : std_logic; -- @suppress "signal xorBit_out is never read"

begin

    descr_proc : process(descrambler,data_in)
        variable poly       : std_logic_vector(57 downto 0) := (others => '0');
        variable tempData   : std_logic_vector(0 to RX_DATA_WIDTH-1);
        variable xorBit     : std_logic;
    begin
        poly := descrambler;
        for it in 0 to (RX_DATA_WIDTH-1) loop
            xorBit          := data_in(it) xor poly(38) xor poly(57);
            poly            := poly(56 downto 0) & data_in(it);
            tempData(it)    := xorBit;
        end loop;
        poly_out            <= poly;
        xorBit_out          <= xorBit;
        tempData_out        <= tempData;
    end process;

    retim_proc : process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            if (reset = '1') then
                unscrambled_data_i <= (others => '0');
                descrambler        <= "01" & x"55555555555555";
            elsif (data_v_in = '1') then
                unscrambled_data_i <= tempData_out;
                descrambler        <= poly_out;
            end if;
        end if;
    end process;
    data_out <= unscrambled_data_i;

end Behavioral;
