--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.package_64b66b.all;

--library UNISIM;
--use UNISIM.VComponents.all;

entity split64bword_64b66b is
    generic (
        RD53Version   : String := "A" --A or B
    );
    port (
        rst           : in std_logic;
        clk40         : in std_logic;
        data_in       : in std_logic_vector(63 downto 0);
        valid_in      : in std_logic;
        isSOPEOP_in   : in std_logic;
        data_out      : out data32VL_64b66b_type;
        byteskeep_out : out std_logic_vector(3 downto 0)

    );
end split64bword_64b66b;


architecture Behavioral of split64bword_64b66b is
begin

    g_outputdata_RD53A: if RD53Version = "A" generate
        signal data_i             : data32VL_64b66b_type;
    begin
        process(clk40)
            variable pending      : std_logic := '0';
            variable data_tmp     : std_logic_vector(31 downto 0);
            variable isSOPEOP_tmp : std_logic;
        begin
            if rising_edge(clk40) then
                if(valid_in = '1') then
                    data_tmp               := data_in(31 downto 0);
                    isSOPEOP_tmp           := isSOPEOP_in;
                    data_i.data            <= data_in(63 downto 32);
                    data_i.valid           <= '1';
                    data_i.tlast           <= '0';
                    pending                := '1';
                elsif (pending = '1') then
                    data_i.data            <= data_tmp;
                    data_i.valid           <= '1';
                    data_i.tlast           <= isSOPEOP_tmp;
                    pending                := '0';
                else
                    data_i                 <= data32VL_64b66b_type_zero; --(others => '0');
                    pending                := '0';
                end if;
            end if;
        end process;
        data_out      <= data_i;
        byteskeep_out <= "1111";
    end generate;

    g_outputdata_RD53B: if RD53Version = "B" generate
        type state_type is (idle,
            wait_tlast
        );

        signal data_in_dly         : std_logic_vector(63 downto 0);
        signal valid_in_dly        : std_logic;
        signal data_in_dlydly      : std_logic_vector(63 downto 0);
        signal isSOPEOP_in_dly     : std_logic;
        signal data_dlydly_i       : data32VL_64b66b_type;
        signal valid_dlydly_i      : std_logic;
        signal tlast_dlydly_i      : std_logic;
        signal tkeep_dlydly_i      : std_logic_vector(3 downto 0);
        signal tkeep_dlydly_out_i  : std_logic_vector(3 downto 0);
        signal cnt_err             : std_logic_vector(3 downto 0);

        signal state               : state_type := idle;
    begin
        data_out      <= data_dlydly_i;
        byteskeep_out <= tkeep_dlydly_out_i;

        --assumption: valid 64b input with tkeep=1111 every other clock
        --            valid input with tkeep=0000 can be followed by valid input with tkeep=1
        -- for each valid 64b input with tkeep=1111 will generate 2x32b
        -- for each valid 64b input with tkeep=0000 will generate 1x32b
        split32b: process(clk40)
            variable pending             : std_logic := '0';
            variable data_dlydly_tmp     : std_logic_vector(31 downto 0);
            variable isSOPEOP_dlydly_tmp : std_logic;
            variable tkeep_dlydly_tmp    : std_logic_vector(3 downto 0);
        begin
            if rising_edge(clk40) then
                if(valid_dlydly_i = '1' and tkeep_dlydly_i = "1111") then
                    data_dlydly_tmp               := data_in_dlydly(31 downto 0);
                    isSOPEOP_dlydly_tmp           := tlast_dlydly_i;
                    tkeep_dlydly_tmp              := tkeep_dlydly_i;
                    tkeep_dlydly_out_i            <= tkeep_dlydly_i;
                    data_dlydly_i.data            <= data_in_dlydly(63 downto 32);
                    data_dlydly_i.valid           <= '1';
                    data_dlydly_i.tlast           <= '0';
                    pending                       := '1';
                elsif (pending = '0' and valid_dlydly_i = '1' and tkeep_dlydly_i = "0000") then
                    data_dlydly_tmp               := data_dlydly_tmp;
                    isSOPEOP_dlydly_tmp           := isSOPEOP_dlydly_tmp;
                    tkeep_dlydly_tmp              := tkeep_dlydly_tmp;
                    tkeep_dlydly_out_i            <= "0000";
                    data_dlydly_i.data            <= (others => '0');--data_in_dlydly(31 downto 0);
                    data_dlydly_i.valid           <= '1';
                    data_dlydly_i.tlast           <= tlast_dlydly_i;
                    pending                       := '0';
                elsif (pending = '1') then
                    data_dlydly_tmp               := data_dlydly_tmp;
                    isSOPEOP_dlydly_tmp           := isSOPEOP_dlydly_tmp;
                    tkeep_dlydly_tmp              := tkeep_dlydly_tmp;
                    tkeep_dlydly_out_i            <= tkeep_dlydly_tmp;
                    data_dlydly_i.data            <= data_dlydly_tmp;
                    data_dlydly_i.valid           <= '1';
                    data_dlydly_i.tlast           <= tlast_dlydly_i or isSOPEOP_dlydly_tmp;
                    pending                       := '0';
                else
                    data_dlydly_i                 <= data32VL_64b66b_type_zero;
                    tkeep_dlydly_out_i            <= "0000";
                    pending                       := '0';
                end if;
            end if;
        end process;

        proc64b: process(clk40)
        begin
            if rising_edge(clk40) then
                data_in_dlydly     <= data_in_dly;
                data_in_dly        <= data_in;
                valid_in_dly       <= valid_in;
                isSOPEOP_in_dly    <= isSOPEOP_in;
                if (rst = '1') then
                    valid_dlydly_i <= '0';
                    tlast_dlydly_i <= '0';
                    tkeep_dlydly_i <= (others => '0');
                    state   <= idle;
                else
                    case state is
                        when idle =>
                            if valid_in_dly = '1' and data_in_dly(63) = '1' then
                                cnt_err            <= cnt_err;
                                if (data_in_dly(5 downto 0) = "000000") then
                                    valid_dlydly_i <= '1';
                                    tlast_dlydly_i <= '1';
                                    tkeep_dlydly_i <= "1111";
                                    state <= idle;
                                else
                                    valid_dlydly_i <= '1';
                                    tlast_dlydly_i <= '0';
                                    tkeep_dlydly_i <= "1111";
                                    state          <= wait_tlast;
                                end if;
                            else
                                if valid_in_dly = '1' then
                                    cnt_err <= cnt_err+x"1";
                                    valid_dlydly_i <= '1';
                                    tlast_dlydly_i <= '0';
                                    tkeep_dlydly_i <= "1111";
                                else
                                    cnt_err        <= cnt_err;
                                    valid_dlydly_i <= '0';
                                    tlast_dlydly_i <= '0';
                                    tkeep_dlydly_i <= "0000";
                                end if;
                                state              <= idle;
                            end if;
                        when wait_tlast =>
                            cnt_err                <= cnt_err;
                            if valid_in_dly = '1' then
                                if (data_in_dly(5 downto 0) = "000000") then-- or isSOPEOP_in_dly = '1' or (data_in(63) = '1' and valid_in = '1')) then
                                    valid_dlydly_i <= '1';
                                    tlast_dlydly_i <= '1';
                                    tkeep_dlydly_i <= "1111";
                                    state          <= idle;
                                else
                                    valid_dlydly_i <= '1';
                                    tlast_dlydly_i <= '0';
                                    tkeep_dlydly_i <= "1111";
                                    state          <= wait_tlast;
                                end if;
                            elsif (data_in(63) = '1' and valid_in = '1') then
                                valid_dlydly_i     <= '1';
                                tlast_dlydly_i     <= '1';
                                tkeep_dlydly_i     <= "0000";
                                state              <= idle;
                            else
                                valid_dlydly_i     <= '0';
                                tlast_dlydly_i     <= '0';
                                tkeep_dlydly_i     <= "0000";
                                state              <= wait_tlast;
                            end if;
                    end case; --the state machine
                end if; --reset
            end if; --clock
        end process;

    end generate;


end Behavioral;
