--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    --use ieee.std_logic_unsigned.all;
    use work.FELIX_package.all;
    use work.package_64b66b.all;

entity remapEpaths_64b66b is
    Port (
        CBOPT        : in  std_logic_vector(3 downto 0);
        DataIn       : in  array_data64VHHVE_64b66b_type(0 to 6);
        AlignedIn    : in  std_logic_vector(0 to 6);
        DataOut      : out array_data64VHHVE_64b66b_type(0 to 6);
        AlignedOut   : out  std_logic_vector(0 to 6)

    );
end remapEpaths_64b66b;


architecture Behavioral of remapEpaths_64b66b is

    type Integer_Array is array (0 to 6) of integer range 0 to 6;
    signal tonew : Integer_Array;

begin


    --par/opt nobond bond=3a*
    --CBOPT      0      3
    --*: assumption: different channels from the same chip will occupy contiguous egroups. Possible Permutations (chip Vs Egr #): a) chip1: 0-2, chip2 3-5, chip3 6; b) chip1: 0-2, chip3: 3, chip2: 4-6 c) chip3: 0, chip1: 1-3, chip2: 4-6; Permuting chip# is not relevant
    remap_proc: process(CBOPT)
    begin
        case CBOPT is
            when x"3" => --7(egroups)->2(segroups)x3(egroups)+1x1. TO implement the other permutations
                tonew(0)         <= 0;
                tonew(1)         <= 1;
                tonew(2)         <= 2;
                tonew(3)         <= 3;
                tonew(4)         <= 4;
                tonew(5)         <= 5;
                tonew(6)         <= 6;
            when others => --include nobond x"1"
                for egroup in 0 to 6 loop
                    tonew(egroup) <= egroup;
                end loop;
        end case;
    --  other options to be implemented
    end process;

    loop_proc: for egroup in 0 to 6 generate
    begin
        DataOut(egroup)    <= DataIn(tonew(egroup));
        AlignedOut(egroup) <= AlignedIn(tonew(egroup));
    end generate;

end Behavioral;
