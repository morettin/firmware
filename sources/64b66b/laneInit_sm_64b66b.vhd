library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VCOMPONENTS.all;

entity laneInit_sm_64b66b is
    port (
        CLK40_IN                 : in std_logic;
        reset                 : in std_logic;
        lossofsync         : in std_logic;
        enable_err_detect_out  : out std_logic;
        lane_up_out            : out std_logic
    );
end laneInit_sm_64b66b;

architecture Behavioral of laneInit_sm_64b66b is
    constant BEGIN_R_ST      : std_logic_vector(3 downto 0) := "1000";
    constant RST_R_ST        : std_logic_vector(3 downto 0) := "0100";
    constant ALIGN_R_ST      : std_logic_vector(3 downto 0) := "0010";
    constant READY_R_ST      : std_logic_vector(3 downto 0) := "0001";

    signal state             : std_logic_vector(3 downto 0) := (others => '0');
    signal counter1_r        : std_logic_vector(0 to 3)     := x"1";
    signal begin_r           : std_logic;
    signal rst_r             : std_logic;
    signal align_r           : std_logic;
    signal ready_r           : std_logic;
    signal reset_count_r     : std_logic;
    signal count_8d_done_r   : std_logic;
    signal next_begin_c      : std_logic;
    signal next_rst_c        : std_logic;
    signal next_align_c      : std_logic;
    signal next_ready_c      : std_logic;
    signal lane_up_i      : std_logic;
begin

    ----------------------------------------------------------------
    -----------Main state machine for managing initialization-------
    ----------------------------------------------------------------
    process(CLK40_IN)
    begin
        if rising_edge(CLK40_IN) then
            if(reset = '1') then
                begin_r <= '1';
                rst_r   <= '0';
                align_r <= '0';
                ready_r <= '0';
            else
                begin_r <=  next_begin_c;
                rst_r   <=  next_rst_c;
                align_r <=  next_align_c;
                ready_r <=  next_ready_c;
            end if;
        end if;
    end process;

    state <= begin_r & rst_r & align_r & ready_r;
    process(all)
        variable next_begin_c_v, next_rst_c_v, next_align_c_v ,next_ready_c_v : std_logic;
    begin
        next_begin_c_v             := '0';
        next_rst_c_v               := '0';
        next_align_c_v             := '0';
        next_ready_c_v             := '0';
        case state is
            when BEGIN_R_ST =>
                next_rst_c_v := '1';
            when RST_R_ST =>
                if(reset = '1') then
                    next_rst_c_v   := '1';
                elsif(count_8d_done_r = '1') then
                    next_align_c_v := '1';
                else
                    next_rst_c_v   := '1';
                end if;
            when ALIGN_R_ST =>
                if(reset = '1') then
                    next_begin_c_v := '1';
                elsif(lossofsync = '1') then
                    next_align_c_v := '1';
                else
                    next_ready_c_v := '1';
                end if;
            when READY_R_ST =>
                if(reset = '1') then
                    next_begin_c_v := '1';
                elsif(lossofsync = '1') then
                    next_begin_c_v := '1';
                else
                    next_ready_c_v := '1';
                end if;
            when others =>
                next_begin_c_v     := '1';
        end case;
        next_begin_c <= next_begin_c_v ;
        next_rst_c   <= next_rst_c_v   ;
        next_align_c <= next_align_c_v ;
        next_ready_c <= next_ready_c_v ;

    end process;

    process(CLK40_IN)
    begin
        if rising_edge(CLK40_IN) then
            if (reset = '1') then
                lane_up_i     <= '0';
            else
                lane_up_i     <= ready_r;
            end if;
        end if;
    end process;
    lane_up_out               <= lane_up_i;

    process(CLK40_IN)
    begin
        if rising_edge(CLK40_IN) then
            enable_err_detect_out <=  ready_r;
        end if;
    end process;

    ----------------------------------------------------------------
    -----------Counter 1, for reset cycles, align cycles and realign cycles
    ----------------------------------------------------------------
    process(CLK40_IN)
    begin
        if rising_edge(CLK40_IN) then
            if(reset_count_r = '1') then
                counter1_r   <=   x"1";
            else
                counter1_r   <=   counter1_r + x"1";
            end if;
        end if;
    end process;
    count_8d_done_r          <= counter1_r(0);

    process(CLK40_IN)
    begin
        if rising_edge(CLK40_IN) then
            reset_count_r    <= reset or not rst_r;
        end if;
    end process;

end Behavioral;
