#!/bin/sh
# This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
# Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
# Authors:
#               Mark Donszelmann
#               Mesfin Gebyehu
#               Thei Wijnen
# 
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#
# Script to rebuild the derived files from templates
#
prev_version=5.0
current_version=5.1
#next_version=4.10

# firmware directory:
firmware_dir=../..
# sources directory:
sources_dir=$firmware_dir/sources
# template directory:
template_dir=$sources_dir/templates
# generated sources directory
generated_dir=$template_dir/generated

# WupperCodeGen
echo $scriptdir
wuppercodegen_dir=$firmware_dir/WupperCodeGen
wuppercodegen=$wuppercodegen_dir/wuppercodegen/cli.py

prev_registers=$template_dir/registers-${prev_version}.yaml
current_registers=$template_dir/registers-${current_version}.yaml
#next_registers=$template_dir/registers-${next_version}.yaml
$wuppercodegen --version
echo "Previous version: $prev_version"
echo "Current  version: $current_version"
#echo "Next     version: $next_version"

prev_registers=$template_dir/registers-$1.yaml
current_registers=$template_dir/registers-$2.yaml
$wuppercodegen --version
echo "Generating diff between previous and current version..."
$wuppercodegen --diff $prev_registers $current_registers $wuppercodegen_dir/input/registers-diff.html.template registers-diff-$1-$2.html

regdoc=../../../documents/regmap
cp -p registers-diff-$1-$2.html $regdoc

#
