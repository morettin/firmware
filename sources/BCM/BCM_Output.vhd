----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 08/25/2022 02:16:24 PM
-- Design Name:
-- Module Name: BCM_Output - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
    use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity BCM_Output is
    Port ( ToT : in STD_LOGIC_VECTOR (5 downto 0);
        ToA : in STD_LOGIC_VECTOR (5 downto 0);
        BCID : in STD_LOGIC_VECTOR (11 downto 0);
        L1ID: in STD_LOGIC_VECTOR (3 downto 0);
        L1SubID: in STD_LOGIC_VECTOR (3 downto 0);
        reset : in STD_LOGIC;
        a_clk : in STD_LOGIC;
        Output : out STD_LOGIC_VECTOR (31 downto 0);
        DataValid : out STD_LOGIC;
        LastWord :  out STD_LOGIC;
        ForcePublish: in STD_LOGIC
    );

end BCM_Output;

architecture Behavioral of BCM_Output is

begin
    process(a_clk)
        variable count: Integer range 0 to 127;
    begin
        if rising_edge(a_clk) then -- There is no need for delay between the input block so rising edge works

            if reset='1' then
                Output <= (others=> '0');
                DataValid <= '0';
                LastWord <= '0';
                count := 0;
            else
                if (ToA /= "111111" and ToT /= "000000") or ForcePublish='1' then
                    Output <=  L1ID & L1SubID & STD_LOGIC_VECTOR(BCID)  & ToT & ToA;
                    DataValid<= '1';
                    if count = 127 then
                        LastWord <= '1';
                        count := 0;
                    else
                        LastWord <= '0';
                        count := count + 1;
                    end if;

                else
                    LastWord <= '0';
                    Output <= (others => '0' );
                    DataValid <= '0';
                end if;
            end if;
        end if;
    end process;



end Behavioral;
