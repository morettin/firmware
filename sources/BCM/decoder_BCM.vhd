
----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 08/02/2022 03:49:28 PM
-- Design Name:
-- Module Name: decoding_BCM - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
    use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
    use work.ToTExtracter;
    use work.ToAExtracter;
    use work.BCM_DataMasker_Serial;



entity decoding_BCM is
    generic ( AVALIABLE_CLK_CYCLES : integer := 6;
        DATA_SIZE : integer :=32;
        OTHER_DATA: integer :=12+4); --This is used for extra bits like BCID, L1A
    --BCID is 12, L1ID is 23 bits but we oly use 4


    Port (
        Input : in STD_LOGIC_VECTOR (31 downto 0);
        InPipe : in STD_LOGIC_VECTOR (OTHER_DATA -1 downto 0);


        OutPipe : out STD_LOGIC_VECTOR (OTHER_DATA-1 downto 0);
        ToT: out std_logic_vector(5 downto 0);
        ToA: out std_logic_vector(5 downto 0);

        a_clk : in STD_LOGIC;
        --clk : in STD_LOGIC;
        reset : in STD_LOGIC
    );
end decoding_BCM;

architecture Behavioral of decoding_BCM is

    signal DataToToAExt : std_logic_vector( OTHER_DATA+DATA_SIZE-1 downto 0);
    signal DataToToTExt : std_logic_vector( OTHER_DATA+DATA_SIZE-1 downto 0);
    signal ToA_ToToTExt:  std_logic_vector(5 downto 0);
    signal FinalOutput:  std_logic_vector( OTHER_DATA+DATA_SIZE-1 downto 0);
    signal DataSerialInput: std_logic_vector(OTHER_DATA+DATA_SIZE-1 downto 0);

begin

    -- This code takes the Input, if it's the first cycle, it passes as it is
    -- on other cycles it masks the first sequence of the ones in the input
    DataSerialInput(OTHER_DATA+31 downto 0) <= InPipe & Input;
    DataSerial : entity BCM_DataMasker_Serial
        generic map (
            DEBUG => false,
            AVALIABLE_CLK_CYCLES => AVALIABLE_CLK_CYCLES,
            DATA_SIZE => DATA_SIZE,
            OTHER_DATA => OTHER_DATA
        )
        port map (
            Input => DataSerialInput,
            Output=> DataToToAExt,
            a_clk => a_clk, reset => reset
        );



    -- It takes in the data passed by the DataSerial and extracts the ToA value
    ToA_Extract : entity ToAExtracter
        generic map ( DATA_SIZE => DATA_SIZE, OTHER_DATA => OTHER_DATA )
        port map (
            DataIn => DataToToAExt,
            DataOut=> DataToToTExt,
            ToA => ToA_ToToTExt,
            a_clk => a_clk,
            reset=> reset
        );
    -- It takes in the data passed by the ToAExtracter and extracts the ToT value
    ToT_Extract : entity ToTExtracter
        generic map ( DATA_SIZE => DATA_SIZE, OTHER_DATA => OTHER_DATA )
        port map (
            DataIn => DataToToTExt,
            DataOut => FinalOutput,
            ToA => ToA_ToToTExt,
            OutToA => ToA,
            ToT => ToT,
            a_clk => a_clk,
            reset=> reset
        );
    OutPipe <= FinalOutput(OTHER_DATA+DATA_SIZE-1 downto DATA_SIZE);

end Behavioral;
