----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 08/25/2022 02:20:59 PM
-- Design Name:
-- Module Name: BCMDec_GearBox - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
    use work.decoding_BCM;
    use work.BCM_Output;



entity BCMDec_GearBox is
    generic ( AVALIABLE_CLK_CYCLES : integer := 6;
        DATA_SIZE : integer :=32;
        OTHER_DATA: integer :=12+4+5+1 --This is used for extra bits like BCID,
    --L1ID, L1SubID, L1A

    ); --This is used for extra bits like BCID, L1A


    Port ( clk : in STD_LOGIC;
        reset : in STD_LOGIC;
        Input : in STD_LOGIC_VECTOR (DATA_SIZE-1 downto 0);
        Output : out STD_LOGIC_VECTOR (31 downto 0);

        a_clk : in STD_LOGIC;
        BCID : in STD_LOGIC_VECTOR (11 downto 0);

        -- L1A: in STD_LOGIC;
        L1ID: in STD_LOGIC_VECTOR(23 downto 0);
        L1SubID: in STD_LOGIC_VECTOR(4 downto 0);
        L1A: in STD_LOGIC;

        DataValid : out STD_LOGIC;
        LastWord: out STD_LOGIC;
        --ZeroFlag : out STD_LOGIC;
        --NoErrFlag : out STD_LOGIC;
        PublishZeros: in STD_LOGIC
    );
end BCMDec_GearBox;

architecture Behavioral of BCMDec_GearBox is

    signal ToT: STD_LOGIC_VECTOR( 5 downto 0);
    signal ToA: STD_LOGIC_VECTOR( 5 downto 0);

    -- 4 bits L1ID -- 12 Bits BCID
    signal SerInput : STD_LOGIC_VECTOR (DATA_SIZE-1 downto 0);

    signal SerOtherData: STD_LOGIC_VECTOR(OTHER_DATA-1 downto 0);
    signal SerOut : STD_LOGIC_VECTOR(OTHER_DATA-1 downto 0);


    function reverse_any_vector (a: in std_logic_vector)
    return std_logic_vector is
        variable result: std_logic_vector(a'RANGE);
        alias aa: std_logic_vector(a'REVERSE_RANGE) is a;
    begin
        for i in aa'RANGE loop
            result(i) := aa(i);
        end loop;
        return result;
    end; -- function reverse_any_vector







begin
    ---  Here we have the state machine where we control the data flow to our asnyc curcuits.

    process(clk)
    begin

        if rising_edge(clk) then
            if reset then
                SerInput <= (others=> '0');
                SerOtherData <= (others => '0');
            else
                if L1A then
                    SerOtherData(21 downto 0) <= L1A & L1SubID & L1ID(3 downto 0) & BCID;
                    SerInput <=  reverse_any_vector(Input);
                else
                    SerInput <= (others=> '0');
                    SerOtherData <= (others => '0');
                end if;

            end if;
        end if;
    end process;




    SyncToT: entity decoding_BCM
        generic map (
            AVALIABLE_CLK_CYCLES=>AVALIABLE_CLK_CYCLES,
            DATA_SIZE => 32,
            OTHER_DATA=> OTHER_DATA
        )
        port map(
            Input => SerInput(31 downto 0),
            InPipe => SerOtherData,
            OutPipe => SerOut,
            ToT => ToT,
            ToA => ToA,
            a_clk => a_clk,
            --clk => clk,
            reset => reset
        );
    e_Out: entity BCM_Output

        port map(  ToT => ToT,
            ToA => ToA,
            BCID=> SerOut(11 downto 0),
            L1ID => SerOut(15 downto 12),
            L1SubID => SerOut(19 downto 16),
            reset => reset,
            a_clk => a_clk,
            Output => Output,
            DataValid => DataValid,
            LastWord => LastWord,
            ForcePublish => (SerOut(21) and PublishZeros)

        );





end Behavioral;
