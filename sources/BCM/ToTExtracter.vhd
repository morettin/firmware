----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 07/29/2022 10:58:42 AM
-- Design Name:
-- Module Name: ToT-ToA-Extracter - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use work.ALL;


entity ToTExtracter is
    generic (DATA_SIZE : integer :=32;
        OTHER_DATA: integer :=0 --This is used for extra bits like BCID, L1A
    );



    Port ( DataIn : in STD_LOGIC_VECTOR  (OTHER_DATA+DATA_SIZE-1 downto 0);
        DataOut : out STD_LOGIC_VECTOR (OTHER_DATA+DATA_SIZE-1 downto 0);
        ToA : in STD_LOGIC_VECTOR (5 downto 0);
        OutToA : out STD_LOGIC_VECTOR (5 downto 0);
        ToT : out STD_LOGIC_VECTOR (5 downto 0);
        a_clk : in STD_LOGIC;
        reset : in STD_LOGIC
    );

end ToTExtracter;

architecture Behavioral of ToTExtracter is
    signal InputExpanded: std_logic_vector(DATA_SIZE downto 0);
begin

    InputExpanded <= '0' & DataIn (DATA_SIZE-1 downto 0);

    process(a_clk)
    begin
        if rising_edge(a_clk) then
            if reset then
                DataOut <= (others => '0');
                OutToA <= "111111";
                ToT <= (others => '0' );

            else

                DataOut<=DataIn;
                OutToA <= ToA;

                if ToA = "111111" then ToT <= "100000";
                elsif InputExpanded(to_integer(unsigned(ToA)))='0' then ToT <= "000000";
                elsif InputExpanded(to_integer(unsigned(ToA))+1)='0' then ToT <= "000001";
                elsif InputExpanded(to_integer(unsigned(ToA))+2)='0' then ToT <= "000010";
                elsif InputExpanded(to_integer(unsigned(ToA))+3)='0' then ToT <= "000011";
                elsif InputExpanded(to_integer(unsigned(ToA))+4)='0' then ToT <= "000100";
                elsif InputExpanded(to_integer(unsigned(ToA))+5)='0' then ToT <= "000101";
                elsif InputExpanded(to_integer(unsigned(ToA))+6)='0' then ToT <= "000110";
                elsif InputExpanded(to_integer(unsigned(ToA))+7)='0' then ToT <= "000111";
                elsif InputExpanded(to_integer(unsigned(ToA))+8)='0' then ToT <= "001000";
                elsif InputExpanded(to_integer(unsigned(ToA))+9)='0' then ToT <= "001001";
                elsif InputExpanded(to_integer(unsigned(ToA))+10)='0' then ToT <= "001010";
                elsif InputExpanded(to_integer(unsigned(ToA))+11)='0' then ToT <= "001011";
                elsif InputExpanded(to_integer(unsigned(ToA))+12)='0' then ToT <= "001100";
                elsif InputExpanded(to_integer(unsigned(ToA))+13)='0' then ToT <= "001101";
                elsif InputExpanded(to_integer(unsigned(ToA))+14)='0' then ToT <= "001110";
                elsif InputExpanded(to_integer(unsigned(ToA))+15)='0' then ToT <= "001111";
                elsif InputExpanded(to_integer(unsigned(ToA))+16)='0' then ToT <= "010000";
                elsif InputExpanded(to_integer(unsigned(ToA))+17)='0' then ToT <= "010001";
                elsif InputExpanded(to_integer(unsigned(ToA))+18)='0' then ToT <= "010010";
                elsif InputExpanded(to_integer(unsigned(ToA))+19)='0' then ToT <= "010011";
                elsif InputExpanded(to_integer(unsigned(ToA))+20)='0' then ToT <= "010100";
                elsif InputExpanded(to_integer(unsigned(ToA))+21)='0' then ToT <= "010101";
                elsif InputExpanded(to_integer(unsigned(ToA))+22)='0' then ToT <= "010110";
                elsif InputExpanded(to_integer(unsigned(ToA))+23)='0' then ToT <= "010111";
                elsif InputExpanded(to_integer(unsigned(ToA))+24)='0' then ToT <= "011000";
                elsif InputExpanded(to_integer(unsigned(ToA))+25)='0' then ToT <= "011001";
                elsif InputExpanded(to_integer(unsigned(ToA))+26)='0' then ToT <= "011010";
                elsif InputExpanded(to_integer(unsigned(ToA))+27)='0' then ToT <= "011011";
                elsif InputExpanded(to_integer(unsigned(ToA))+28)='0' then ToT <= "011100";
                elsif InputExpanded(to_integer(unsigned(ToA))+29)='0' then ToT <= "011101";
                elsif InputExpanded(to_integer(unsigned(ToA))+30)='0' then ToT <= "011110";
                elsif InputExpanded(to_integer(unsigned(ToA))+31)='0' then ToT <= "011111";
                else ToT <= "100000";
                end if;


            end if;
        end if;
    end process;




end Behavioral;





