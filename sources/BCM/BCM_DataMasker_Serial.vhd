----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 08/02/2022 03:49:28 PM
-- Design Name:
-- Module Name: decoding_BCM - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
    use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
    use work.FastMasker;



entity BCM_DataMasker_Serial is
    generic ( DEBUG : boolean := false;
        AVALIABLE_CLK_CYCLES : integer := 6;
        DATA_SIZE : integer :=32;
        OTHER_DATA: integer :=0 --This is used for extra bits like BCID, L1A
    );


    Port (
        Input :   in STD_LOGIC_VECTOR (OTHER_DATA+DATA_SIZE-1 downto 0);
        Output : out STD_LOGIC_VECTOR (OTHER_DATA+DATA_SIZE-1 downto 0);

        a_clk : in STD_LOGIC;
        reset : in STD_LOGIC

    );
end BCM_DataMasker_Serial;

architecture Behavioral of BCM_DataMasker_Serial is
    signal PassToDecoder : std_logic_vector(DATA_SIZE-1 downto 0);
    signal MaskedInput : std_logic_vector(DATA_SIZE-1 downto 0);
    signal OtherData     : std_logic_vector(OTHER_DATA-1 downto 0);
begin

    ---  Here we have the state machine where we control the data flow to our asnyc curcuits.
    process(a_clk)
        variable count : integer range 0 to (AVALIABLE_CLK_CYCLES-1) := 0;
    begin
        if rising_edge(a_clk) then

            -- A state machine, that takes in input from the stream if count is 0
            -- otherwise used masked data
            if reset then
                count:=0;
            else
                if count = (AVALIABLE_CLK_CYCLES-1) then
                    count:=0;
                else
                    count := count +1;
                end if;
            end if;

            if reset then
                PassToDecoder <= (others => '1');
                Output <= (others => '0');
                OtherData <= (others => '0');
            else
                if count=0 then
                    PassToDecoder<=Input(DATA_SIZE-1 downto 0);
                    Output <= Input;
                    OtherData <= Input( OTHER_DATA+DATA_SIZE-1 downto DATA_SIZE);
                else
                    Output <= OtherData & MaskedInput;
                    PassToDecoder<=MaskedInput;
                end if;
            end if;
        end if;

    end process;

    Masker : entity FastMasker port map ( Input => PassToDecoder(31 downto 0), Output => MaskedInput(31 downto 0) );


end Behavioral;
