--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library work, ieee, UNISIM;
use work.FELIX_gbt_package.all;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.pcie_package.all;
use work.centralRouter_package.all;

entity GbtSmaOutputSelector is
  generic(
    DEBUG_MODE : boolean := false);
  port (
    GBTdata_array           : in     txrx120b_type;
    GbtSmaOut               : out    std_logic;
    register_map_40_control : in     register_map_control_type);
end entity GbtSmaOutputSelector;



architecture rtl of GbtSmaOutputSelector is

begin

g1a: if(DEBUG_MODE = true) generate  
 selector: process(GBTdata_array, register_map_40_control)
    variable index: integer range 0 to 127;
 begin
    index := to_integer(unsigned(register_map_40_control.GBT_SMA_SEL));
    if(index <= 119) then
        GbtSmaOut <= GBTdata_array(0)(index);
    else
        GbtSmaOut <= '0';
    end if;
 end process;
end generate;

g1b: if(DEBUG_MODE = false) generate
    GbtSmaOut <= '0';
end generate;

end architecture rtl ; -- of GbtSmaOutputSelector

