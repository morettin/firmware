--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Marius Wensing
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    --use ieee.std_logic_unsigned.all;
    use ieee.std_logic_1164.all;
    use work.centralRouter_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;

entity GBT_fanout_selector is
    generic(
        GBT_NUM            : integer := 24);
    port (
        GBTDataIn               : in     txrx120b_type(0 to GBT_NUM-1);
        lpGBTDataToFEIn         : in     txrx32b_type(0 to GBT_NUM-1);
        lpGBTDataToHostIn       : in     txrx224b_type(0 to GBT_NUM-1);
        lpGBTECDataIn           : in     txrx2b_type(0 to GBT_NUM-1);
        lpGBTICDataIn           : in     txrx2b_type(0 to GBT_NUM-1);
        GBTLinkValidIn          : in     std_logic_vector(GBT_NUM-1 downto 0);
        EMU_GBTlinkValidIn      : in     std_logic;
        EMU_GBTDataIn           : in     std_logic_vector(119 downto 0);
        EMU_lpGBTDataToFEIn     : in     std_logic_vector(31 downto 0);
        EMU_lpGBTDataToHostIn   : in     std_logic_vector(223 downto 0);
        EMU_lpGBTECDataIn       : in     std_logic_vector(1 downto 0);
        EMU_lpGBTICDataIn       : in     std_logic_vector(1 downto 0);
        GBTDataOut              : out    txrx120b_type(0 to (GBT_NUM-1));
        lpGBTDataToFEOut        : out    txrx32b_type(0 to (GBT_NUM-1));
        lpGBTDataToHostOut      : out    txrx224b_type(0 to (GBT_NUM-1));
        lpGBTECDataOut          : out    txrx2b_type(0 to (GBT_NUM-1));
        lpGBTICDataOut          : out    txrx2b_type(0 to (GBT_NUM-1));
        GBTLinkValidOut         : out    std_logic_vector(GBT_NUM-1 downto 0);
        clk40                   : in     std_logic;
        sel                     : in     std_logic_vector(GBT_NUM-1 downto 0));
end entity GBT_fanout_selector;



architecture rtl of GBT_fanout_selector is

    signal GBTDataIn_f00  : txrx120b_type(0 to (GBT_NUM-1));
    signal GBTDataIn_f01  : txrx120b_type(0 to (GBT_NUM-1));

    signal lpGBTDataToFEIn_f00  : txrx32b_type(0 to (GBT_NUM-1));
    signal lpGBTDataToFEIn_f01  : txrx32b_type(0 to (GBT_NUM-1));

    signal lpGBTDataToHostIn_f00  : txrx224b_type(0 to (GBT_NUM-1));
    signal lpGBTDataToHostIn_f01  : txrx224b_type(0 to (GBT_NUM-1));

    signal lpGBTECDataIn_f00  : txrx2b_type(0 to (GBT_NUM-1));
    signal lpGBTECDataIn_f01  : txrx2b_type(0 to (GBT_NUM-1));

    signal lpGBTICDataIn_f00  : txrx2b_type(0 to (GBT_NUM-1));
    signal lpGBTICDataIn_f01  : txrx2b_type(0 to (GBT_NUM-1));

    signal GBTLinkValidIn_f00 : std_logic_vector(GBT_NUM-1 downto 0);
    signal GBTLinkValidIn_f01 : std_logic_vector(GBT_NUM-1 downto 0);


begin
    g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
        gbt_fo : process (clk40)
        begin
            if rising_edge(clk40) then
                if sel(i) = '1' then
                    GBTDataOut(i)      <= EMU_GBTDataIn;
                    lpGBTDataToFEOut(i)      <= EMU_lpGBTDataToFEIn;
                    lpGBTDataToHostOut(i)      <= EMU_lpGBTDataToHostIn;
                    lpGBTECDataOut(i)      <= EMU_lpGBTECDataIn;
                    lpGBTICDataOut(i)      <= EMU_lpGBTICDataIn;
                    GBTLinkValidOut(i) <= EMU_GBTlinkValidIn;
                else
                    GBTDataOut(i)      <= GBTDataIn_f01(i);
                    GBTLinkValidOut(i)    <= GBTLinkValidIn_f01(i);
                    lpGBTDataToFEOut(i)       <= lpGBTDataToFEIn_f01(i);
                    lpGBTDataToHostOut(i)       <= lpGBTDataToHostIn_f01(i);
                    lpGBTECDataOut(i)       <= lpGBTECDataIn_f01(i);
                    lpGBTICDataOut(i)       <= lpGBTICDataIn_f01(i);
                end if;
            end if;
        end process;
    end generate;

    GBT_rx_sync : process (clk40)
    begin  -- process
        if rising_edge (clk40) then
            g_GBT_rx_sync : for i in 0 to GBT_NUM-1 loop
                GBTDataIn_f00(i)  <= GBTDataIn(i);
                GBTDataIn_f01(i)  <= GBTDataIn_f00(i);
                lpGBTDataToFEIn_f00(i)  <= lpGBTDataToFEIn(i);
                lpGBTDataToFEIn_f01(i)  <= lpGBTDataToFEIn_f00(i);
                lpGBTDataToHostIn_f00(i)  <= lpGBTDataToHostIn(i);
                lpGBTDataToHostIn_f01(i)  <= lpGBTDataToHostIn_f00(i);
                lpGBTECDataIn_f00(i)  <= lpGBTECDataIn(i);
                lpGBTECDataIn_f01(i)  <= lpGBTECDataIn_f00(i);
                lpGBTICDataIn_f00(i)  <= lpGBTICDataIn(i);
                lpGBTICDataIn_f01(i)  <= lpGBTICDataIn_f00(i);
                GBTLinkValidIn_f00(i) <= GBTLinkValidIn(i);
                GBTLinkValidIn_f01(i) <= GBTLinkValidIn_f00(i);
            end loop;  -- i
        end if;
    end process   ;


end architecture rtl ; -- of downstream_fanout_selector

