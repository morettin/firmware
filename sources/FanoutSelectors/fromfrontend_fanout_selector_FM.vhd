--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               RHabraken
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.pcie_package.all;
use work.centralRouter_package.all;
Library xpm;
use xpm.vcomponents.all;

entity fromfrontend_fanout_selector_FM is
  generic(
    GBT_NUM          : integer := 12;
    do_implement     : boolean := true;
    GENERATE_FM_WRAP : boolean := true);
  port (
    Emu_Data_valid       : in     std_logic;
    FULL_Mode_data_valid : in     std_logic_vector(GBT_NUM-1 downto 0);
    GTH_FM_EMU_data      : in     std_logic_vector(32 downto 0);
    GTH_FM_RX_33b_out    : in     txrx33b_type(0 to (GBT_NUM-1));
    GTH_FM_Valid_array   : out    std_logic_vector((GBT_NUM-1) downto 0);
    GTH_FM_data_array    : out    txrx33b_type(0 to (GBT_NUM-1));
    clk250               : in     std_logic;
    register_map_control : in     register_map_control_type);
end entity fromfrontend_fanout_selector_FM;



architecture rtl of fromfrontend_fanout_selector_FM is

  signal s_rx_33b_out_f00  : txrx33b_type(0 to (GBT_NUM-1));
  signal s_rx_33b_out_f01  : txrx33b_type(0 to (GBT_NUM-1));
  signal s_frame_locked_f00 : std_logic_vector(GBT_NUM-1 downto 0);
  signal s_frame_locked_f01 : std_logic_vector(GBT_NUM-1 downto 0);
  signal fosel_240 : std_logic_vector(GBT_NUM-1 downto 0);
  signal GTH_FM_Valid_array_s   :   std_logic_vector((GBT_NUM-1) downto 0);
  signal GTH_FM_data_array_s    :   txrx33b_type(0 to (GBT_NUM-1));
  
begin

xpm_cdc_array_single_inst_fosel : xpm_cdc_array_single
   generic map (
      DEST_SYNC_FF => 2,
      INIT_SYNC_FF => 0,
      SIM_ASSERT_CHK => 0,
      SRC_INPUT_REG => 0,
      WIDTH => GBT_NUM
   )
   port map (
      dest_out => fosel_240, -- WIDTH-bit output: src_in synchronized to the destination clock domain. This
                            -- output is registered.

      dest_clk => clk250, -- 1-bit input: Clock signal for the destination clock domain.
      src_clk => '0',   -- 1-bit input: optional; required when SRC_INPUT_REG = 1
      src_in => register_map_control.GBT_TOHOST_FANOUT.SEL(GBT_NUM-1 downto 0)

   );
   
g_channel: for i in 0 to GBT_NUM-1 generate

    GTH_FM_rx_sync : process (clk250)
    begin  -- process 	
        if rising_edge (clk250) then
            s_rx_33b_out_f00(i)  <= GTH_FM_RX_33b_out(i);
            s_rx_33b_out_f01(i)  <= s_rx_33b_out_f00(i);
            s_frame_locked_f00(i) <= FULL_Mode_data_valid(i);
            s_frame_locked_f01(i) <= s_frame_locked_f00(i);
        end if;
    end process 	;

    gbt_fo : process (clk250)
    begin
        if rising_edge(clk250) then
            if fosel_240(i) = '1' then
                GTH_FM_data_array_s(i)      <= GTH_FM_EMU_data;
                GTH_FM_Valid_array_s(i) <= Emu_Data_valid;
            else
                GTH_FM_data_array_s(i)      <= s_rx_33b_out_f01(i);
                GTH_FM_Valid_array_s(i) <= s_frame_locked_f01(i);       
            end if;
        end if;
    end process;
    
    output_pipe : process (clk250)
    begin
        if rising_edge(clk250) then
            GTH_FM_data_array(i)  <= GTH_FM_data_array_s(i);
            GTH_FM_Valid_array(i) <= GTH_FM_Valid_array_s(i);
        end if;
    end process;
    
end generate;





end architecture rtl ; -- of fromfrontend_fanout_selector_FM

