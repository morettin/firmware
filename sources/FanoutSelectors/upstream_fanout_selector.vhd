--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.centralRouter_package.all;
use work.lpgbtfpga_package.all;
use work.pcie_package.all;

entity upstream_fanout_selector is
  generic(
    GBT_NUM                      : integer := 24;
    useToFrontendGBTdataEmulator : boolean := true;
    CREnableFromHost             : boolean := true);
  port (
    CRoutData_array         : in     txrx120b_type(0 to (GBT_NUM-1));
    GBTdata                 : in     std_logic_vector(119 downto 0);
    TX_120b_in              : out    txrx120b_type(0 to GBT_NUM-1);
    clk40                   : in     std_logic;
    register_map_40_control : in     register_map_control_type);
end entity upstream_fanout_selector;



architecture rtl of upstream_fanout_selector is

  signal s_rx_120b_out_f00  : txrx120b_type(0 to (GBT_NUM-1));
  signal s_rx_120b_out_f01  : txrx120b_type(0 to (GBT_NUM-1));
  signal s_frame_locked_f00 : std_logic_vector(GBT_NUM-1 downto 0);
  signal s_frame_locked_f01 : std_logic_vector(GBT_NUM-1 downto 0);

  
begin

g_emu: if useToFrontendGBTdataEmulator = true generate
 g_CrFromHost: if CREnableFromHost = true generate
    --Generate fanout selector
      g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
        GBT_tx_sync : process (clk40)
        begin  -- process 	
          if rising_edge (clk40) then
            if register_map_40_control.GBT_TOFRONTEND_FANOUT.SEL(i) = '1' then
              TX_120b_in(i)      <= GBTdata;
            else
              TX_120b_in(i)      <= CRoutData_array(i);   
            end if;
          end if;
        end process;
      end generate;
  end generate;
  --Connect only emu
  g_NCrFromHost: if CREnableFromHost = false generate
      g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
          TX_120b_in(i)      <= GBTdata;
      end generate;
  end generate;
end generate;

--Connect only CentralRouter
g_Nemu: if useToFrontendGBTdataEmulator = false generate
      TX_120b_in      <= CRoutData_array;   
end generate;
  


end architecture rtl ; -- of upstream_fanout_selector

