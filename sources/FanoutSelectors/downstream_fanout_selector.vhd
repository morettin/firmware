--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.centralRouter_package.all;
use work.lpgbtfpga_package.all;
use work.pcie_package.all;

entity downstream_fanout_selector is
  generic(
    GBT_NUM            : integer := 24;
    useGBTdataEmulator : boolean := true;
    GENERATE_GBT       : boolean := true);
  port (
    FRAME_LOCKED_O          : in     std_logic_vector(GBT_NUM-1 downto 0);
    GBTdata                 : in     std_logic_vector(119 downto 0);
    GBTdata_array           : out    txrx120b_type(0 to (GBT_NUM-1));
    GBTlinkValid            : in     std_logic;
    GBTlinkValid_array      : out    std_logic_vector(0 to (GBT_NUM-1));
    RX_120b_out             : in     txrx120b_type(0 to GBT_NUM-1);
    clk40                   : in     std_logic;
    register_map_40_control : in     register_map_control_type);
end entity downstream_fanout_selector;



architecture rtl of downstream_fanout_selector is

  signal s_rx_120b_out_f00  : txrx120b_type(0 to (GBT_NUM-1));
  signal s_rx_120b_out_f01  : txrx120b_type(0 to (GBT_NUM-1));
  signal s_frame_locked_f00 : std_logic_vector(GBT_NUM-1 downto 0);
  signal s_frame_locked_f01 : std_logic_vector(GBT_NUM-1 downto 0);

  
begin
g_emu: if ( useGBTdataEmulator = true ) generate
  g_GBT: if GENERATE_GBT = true generate
      g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
        gbt_fo : process (clk40)
        begin
          if rising_edge(clk40) then
            if register_map_40_control.GBT_TOHOST_FANOUT.SEL(i) = '1' then
              GBTdata_array(i)      <= GBTdata;
              GBTlinkValid_array(i) <= GBTlinkValid;
            else
              GBTdata_array(i)      <= s_rx_120b_out_f01(i);
              GBTlinkValid_array(i) <= s_frame_locked_f01(i);       
              --GBTdata_array(i)      <= RX_120b_out(i);
              --GBTlinkValid_array(i) <= FRAME_LOCKED_O(i);
            end if;
          end if;
        end process;
      end generate;

      GBT_rx_sync : process (clk40)
      begin  -- process 	
          if rising_edge (clk40) then
            g_GBT_rx_sync : for i in 0 to GBT_NUM-1 loop
              s_rx_120b_out_f00(i)  <= RX_120b_out(i);
              s_rx_120b_out_f01(i)  <= s_rx_120b_out_f00(i);
              s_frame_locked_f00(i) <= FRAME_LOCKED_O(i);
              s_frame_locked_f01(i) <= s_frame_locked_f00(i);
            end loop;  -- i
          end if;
      end process 	;
  end generate;
  g_NGBT: if GENERATE_GBT = false generate
      g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
          GBTdata_array(i)      <= GBTdata;
          GBTlinkValid_array(i) <= GBTlinkValid;
      end generate;
  end generate;
end generate;

g_Nemu: if ( useGBTdataEmulator = false ) generate
  g_GBT_emulator_fanout : for i in 0 to (GBT_NUM-1) generate
          GBTdata_array(i)      <= s_rx_120b_out_f01(i);
          GBTlinkValid_array(i) <= s_frame_locked_f01(i);       
  end generate;

  GBT_rx_sync : process (clk40)
  begin  -- process 	
      if rising_edge (clk40) then
        g_GBT_rx_sync : for i in 0 to GBT_NUM-1 loop
          s_rx_120b_out_f00(i)  <= RX_120b_out(i);
          s_rx_120b_out_f01(i)  <= s_rx_120b_out_f00(i);
          s_frame_locked_f00(i) <= FRAME_LOCKED_O(i);
          s_frame_locked_f01(i) <= s_frame_locked_f00(i);
        end loop;  -- i
      end if;
  end process 	;
end generate;

end architecture rtl ; -- of downstream_fanout_selector

