--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Weihao Wu
--!               Kai Chen
--!               Mesfin Gebyehu
--!               Rene
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer: Kai Chen
--
-- Create Date:    10/20/2016
-- Design Name:
-- Module Name:     FELIX_FM_gbt_wrapper  -- Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:   The TOP MODULE FOR FELIX GBT FULL mode
--                RX buffer is used in transceiver
--                The TTC 240M clock is used for rx_user_clk
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
LIBRARY IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL; -- @suppress "Deprecated package"
library xpm;
use xpm.vcomponents.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;
use work.centralRouter_package.all;

entity FELIX_FM_gbt_wrapper_ku is
    Generic (
        GBT_NUM : integer := 24;
        Phase2CRToHost_FM : boolean := false
    );
    Port (

        -------------------
        ---- For debug
        -------------------
        -- For Latency test
        RX_FLAG_O    : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_FLAG_O    : out std_logic_vector(GBT_NUM-1 downto 0);
        REFCLK_CXP1  : out std_logic;
        REFCLK_CXP2  : out std_logic;

        rst_hw       : in std_logic;

        register_map_control        : in    register_map_control_type;
        register_map_gbt_monitor    : out     register_map_gbt_monitor_type;

        -- GTH REFCLK, DRPCLK, GREFCLK
        DRP_CLK_IN                      : in std_logic;
        Q2_CLK0_GTREFCLK_PAD_N_IN       : in std_logic;
        Q2_CLK0_GTREFCLK_PAD_P_IN       : in std_logic;
        Q8_CLK0_GTREFCLK_PAD_N_IN       : in std_logic;
        Q8_CLK0_GTREFCLK_PAD_P_IN       : in std_logic;
        --Q4_CLK0_GTREFCLK_PAD_N_IN       : in std_logic;
        --Q4_CLK0_GTREFCLK_PAD_P_IN       : in std_logic;
        --Q5_CLK0_GTREFCLK_PAD_N_IN       : in std_logic;
        --Q5_CLK0_GTREFCLK_PAD_P_IN       : in std_logic;

        clk40_in                        : in std_logic;
        clk250_in                       : in std_logic;
        TXUSRCLK_OUT                    : out std_logic;

        -- for CentralRouter
        TX_120b_in                      : in  txrx120b_type;
        RX_DATA_33b                     : out txrx33b_type;
        RX_DATA_33b_rdy                 : out std_logic_vector(GBT_NUM-1 downto 0);

        TX_FRAME_CLK_I : in std_logic;

        -- GTH Data pins
        TX_P   : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N   : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_P   : in  std_logic_vector(GBT_NUM-1 downto 0);
        RX_N   : in  std_logic_vector(GBT_NUM-1 downto 0);
        RXUSRCLK_OUT                    : out std_logic_vector(GBT_NUM-1 downto 0)

    );
end FELIX_FM_gbt_wrapper_ku;

architecture Behavioral of FELIX_FM_gbt_wrapper_ku is

    type data20barray is array (0 to GBT_NUM-1) of std_logic_vector(19 downto 0);

    signal soft_reset          :std_logic_vector(23 downto 0);
    signal cpll_reset          :std_logic_vector(23 downto 0);
    signal qpll_reset          :std_logic_vector(5 downto 0);
    signal RXRESET_AUTO        :std_logic_vector(23 downto 0);
    signal GT_TXUSRCLK         :std_logic_vector(23 downto 0);
    signal txresetdone         :std_logic_vector(23 downto 0);
    signal rxresetdone         :std_logic_vector(23 downto 0);
    signal txfsmresetdone      :std_logic_vector(23 downto 0);
    signal rxfsmresetdone      :std_logic_vector(23 downto 0);
    signal cpllfbclklost       :std_logic_vector(23 downto 0);
    signal cplllock            :std_logic_vector(23 downto 0);
    signal rxcdrlock           :std_logic_vector(GBT_NUM-1 downto 0);

    signal TX_RESET            :std_logic_vector(23 downto 0);
    signal TX_RESET_i          :std_logic_vector(23 downto 0);
    signal GT_TX_WORD_CLK      :std_logic_vector(23 downto 0);
    signal TX_TC_METHOD        :std_logic_vector(23 downto 0);
    signal TX_DATA_20b  : data20barray := (others => ("00000000000000000000"));

    signal SOFT_TXRST_GT       :std_logic_vector(23 downto 0);
    signal SOFT_RXRST_GT       :std_logic_vector(23 downto 0);
    signal SOFT_TXRST_ALL      :std_logic_vector(5 downto 0);
    signal SOFT_RXRST_ALL      :std_logic_vector(5 downto 0);
    signal TX_OPT              :std_logic_vector(95 downto 0);
    signal DATA_TXFORMAT       :std_logic_vector(47 downto 0);
    signal DATA_TXFORMAT_i     :std_logic_vector(47 downto 0);
    --signal GBT_CHANNEL_RXRST_AUTOENABLE  :std_logic_vector(47 downto 0);
    signal TX_TC_DLY_VALUE      :std_logic_vector(95 downto 0);
    signal GTH_RefClk           :std_logic_vector(23 downto 0);

    signal GBT_ALIGNMENT_DONE   :std_logic_vector(23 downto 0);
    signal GBT_RXCDR_LOCK       :std_logic_vector(23 downto 0);

    signal CXP1_GTH_RefClk      :std_logic;
    signal CXP2_GTH_RefClk      :std_logic;


    signal GT_RXOUTCLK          :std_logic_vector(23 downto 0);
    signal GT_TXOUTCLK          :std_logic_vector(23 downto 0);
    signal RXByteisAligned      :std_logic_vector(GBT_NUM-1 downto 0);
    signal TC_EDGE              :std_logic_vector(23 downto 0);
    signal RxDisperr : std_logic_vector(95 downto 0);
    signal GT_RxDisperr : std_logic_vector(95 downto 0);
    signal GT_RXUSRCLK          :std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_120b_in_i         :txrx120b_type(0 to (GBT_NUM-1));
    signal TX_120b_in_wc        :txrx120b_type(0 to (GBT_NUM-1)); --synchronized to TX word clock using XPM CDC
    signal RX_DATA_33b_s        :txrx33b_type(0 to (GBT_NUM-1));
    signal rxcommadeten_in	  :std_logic_vector(47 downto 0);
    signal rst_hw_23            :std_logic_vector(23 downto 0);
    --attribute mark_debug : string;
    --attribute dont_touch : string;
    ----  attribute mark_debug of RX_DATA_33b_s : signal is "true";
    --attribute mark_debug of RXRESET_AUTO : signal is "true";
    --
    --attribute dont_touch of RXRESET_AUTO : signal is "true";

    COMPONENT thFMch_input_fifo -- @suppress "Component declaration 'thFMch_input_fifo' has none or multiple matching entity declarations"
        PORT (
            wr_clk : IN STD_LOGIC;
            wr_rst : IN STD_LOGIC;
            rd_clk : IN STD_LOGIC;
            rd_rst : IN STD_LOGIC;
            din : IN STD_LOGIC_VECTOR(32 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout : OUT STD_LOGIC_VECTOR(32 DOWNTO 0);
            full : OUT STD_LOGIC;
            empty : OUT STD_LOGIC;
            valid : OUT STD_LOGIC
        );
    END COMPONENT;




begin
    rst_hw_23 <= (others => rst_hw);

    RX_FLAG_O <= (others => '0');  -- unused

    --IBUFDS_GTE2

    REFCLK_CXP1 <= CXP1_GTH_RefClk;
    REFCLK_CXP2 <= CXP2_GTH_RefClk;

    ibufds_instq2_clk0: IBUFDS_GTE3
        generic map (
            REFCLK_EN_TX_PATH => '0',   -- Refer to Transceiver User Guide
            REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
            REFCLK_ICNTL_RX => "00"     -- Refer to Transceiver User Guide
        )
        port map (
            O     => CXP1_GTH_RefClk,
            ODIV2 => open,
            CEB   => '0',
            I     => Q2_CLK0_GTREFCLK_PAD_P_IN,
            IB    => Q2_CLK0_GTREFCLK_PAD_N_IN
        );

    g0_8ChaRef: if (GBT_NUM = 8) generate
        --
        -- Connections GTREF clk Q2 Quad 128 --
        -- Channel 0 to 3
        GTH_RefClk(0) <= CXP1_GTH_RefClk;
        GTH_RefClk(1) <= CXP1_GTH_RefClk;
        GTH_RefClk(2) <= CXP1_GTH_RefClk;
        GTH_RefClk(3) <= CXP1_GTH_RefClk;
        --
        -- Connections GTREF clk Q8 Quad 133 --
        -- Channel 4 to 7
        GTH_RefClk(4) <= CXP2_GTH_RefClk;
        GTH_RefClk(5) <= CXP2_GTH_RefClk;
        GTH_RefClk(6) <= CXP2_GTH_RefClk;
        GTH_RefClk(7) <= CXP2_GTH_RefClk;
    end generate g0_8ChaRef;

    g0_24ChaRef: if (GBT_NUM /= 8) generate
        --
        -- Connections GTREF clk Q2 Quad 126, 127 and 128 --
        -- Channel 0 to 11

        GTH_RefClk(0) <= CXP1_GTH_RefClk;
        GTH_RefClk(1) <= CXP1_GTH_RefClk;
        GTH_RefClk(2) <= CXP1_GTH_RefClk;
        GTH_RefClk(3) <= CXP1_GTH_RefClk;
        GTH_RefClk(4) <= CXP1_GTH_RefClk;
        GTH_RefClk(5) <= CXP1_GTH_RefClk;
        GTH_RefClk(6) <= CXP1_GTH_RefClk;
        GTH_RefClk(7) <= CXP1_GTH_RefClk;
        GTH_RefClk(8) <= CXP1_GTH_RefClk;
        GTH_RefClk(9) <= CXP1_GTH_RefClk;
        GTH_RefClk(10) <= CXP1_GTH_RefClk;
        GTH_RefClk(11) <= CXP1_GTH_RefClk;
        --IBUFDS_GTE2

        GTH_RefClk(12) <= CXP2_GTH_RefClk;
        GTH_RefClk(13) <= CXP2_GTH_RefClk;
        GTH_RefClk(14) <= CXP2_GTH_RefClk;
        GTH_RefClk(15) <= CXP2_GTH_RefClk;
        GTH_RefClk(16) <= CXP2_GTH_RefClk;
        GTH_RefClk(17) <= CXP2_GTH_RefClk;
        GTH_RefClk(18) <= CXP2_GTH_RefClk;
        GTH_RefClk(19) <= CXP2_GTH_RefClk;
        GTH_RefClk(20) <= CXP2_GTH_RefClk;
        GTH_RefClk(21) <= CXP2_GTH_RefClk;
        GTH_RefClk(22) <= CXP2_GTH_RefClk;
        GTH_RefClk(23) <= CXP2_GTH_RefClk;
    end generate g0_24ChaRef;

    ibufds_instq8_clk0: IBUFDS_GTE3
        generic map (
            REFCLK_EN_TX_PATH => '0',   -- Refer to Transceiver User Guide
            REFCLK_HROW_CK_SEL => "00", -- Refer to Transceiver User Guide
            REFCLK_ICNTL_RX => "00"     -- Refer to Transceiver User Guide
        )
        port map (
            O     => CXP2_GTH_RefClk,
            ODIV2 => open,
            CEB   => '0',
            I     => Q8_CLK0_GTREFCLK_PAD_P_IN,
            IB    => Q8_CLK0_GTREFCLK_PAD_N_IN
        );


    soft_reset(23 downto 0)         <= register_map_control.GBT_SOFT_RESET(23 downto 0) or rst_hw_23;
    cpll_reset(23 downto 0)         <= register_map_control.GBT_PLL_RESET.CPLL_RESET(23 downto 0);
    qpll_reset(5 downto 0)          <= register_map_control.GBT_PLL_RESET.QPLL_RESET(53 downto 48) or rst_hw_23(5 downto 0);

    SOFT_TXRST_GT(23 downto 0)      <= register_map_control.GBT_SOFT_TX_RESET.RESET_GT(23 downto 0);
    SOFT_RXRST_GT(23 downto 0)      <= register_map_control.GBT_SOFT_RX_RESET.RESET_GT(23 downto 0) or RXRESET_AUTO;
    SOFT_TXRST_ALL(5 downto 0)      <= register_map_control.GBT_SOFT_TX_RESET.RESET_ALL(53 downto 48) or rst_hw_23(5 downto 0);
    SOFT_RXRST_ALL(5 downto 0)      <= register_map_control.GBT_SOFT_RX_RESET.RESET_ALL(53 downto 48) or rst_hw_23(5 downto 0);-- or RXRESET_AUTO;

    TX_TC_DLY_VALUE(47 downto 0)    <= register_map_control.GBT_TX_TC_DLY_VALUE1;
    TX_TC_DLY_VALUE(95 downto 48)   <= register_map_control.GBT_TX_TC_DLY_VALUE2;

    TX_OPT(47 downto 0)             <= x"000000555555"; -- Register was removed in RM 4.0 register_map_control.GBT_TX_OPT;
    DATA_TXFORMAT(47 downto 0)      <= register_map_control.GBT_DATA_TXFORMAT1(47 downto 0);

    TX_RESET(23 downto 0)           <= register_map_control.GBT_TX_RESET(23 downto 0);
    TX_TC_METHOD(23 downto 0)       <= register_map_control.GBT_TX_TC_METHOD(23 downto 0);
    TC_EDGE(23 downto 0)            <= register_map_control.GBT_TC_EDGE(23 downto 0);

    register_map_gbt_monitor.GBT_VERSION.DATE            <=  GBT_VERSION(63 downto 48);
    register_map_gbt_monitor.GBT_VERSION.GBT_VERSION(35 downto 32)     <=  GBT_VERSION(23 downto 20);
    register_map_gbt_monitor.GBT_VERSION.GTH_IP_VERSION(19 downto 16)  <=  GBT_VERSION(19 downto 16);
    register_map_gbt_monitor.GBT_VERSION.RESERVED        <=  GBT_VERSION(15 downto 3);
    register_map_gbt_monitor.GBT_VERSION.GTHREFCLK_SEL   <=  GBT_VERSION(2 downto 2);
    register_map_gbt_monitor.GBT_VERSION.RX_CLK_SEL      <=  (others => '0');
    register_map_gbt_monitor.GBT_VERSION.PLL_SEL         <=  GBT_VERSION(0 downto 0);

    --
    --
    register_map_gbt_monitor.GBT_TXRESET_DONE(23 downto 0)        <= txresetdone(23 downto 0);
    register_map_gbt_monitor.GBT_RXRESET_DONE(23 downto 0)        <= rxresetdone(23 downto 0);
    register_map_gbt_monitor.GBT_TXFSMRESET_DONE(23 downto 0)     <= txfsmresetdone(23 downto 0);--txpmaresetdone(11 downto 0);
    register_map_gbt_monitor.GBT_RXFSMRESET_DONE(23 downto 0)     <= rxfsmresetdone(23 downto 0);--rxpmaresetdone(11 downto 0);
    --register_map_gbt_monitor.GBT_RXFSMRESET_DONE(23 downto 0)     <= rxfsmresetdone(23 downto 0);--rxpmaresetdone(23 downto 12);
    register_map_gbt_monitor.GBT_CPLL_FBCLK_LOST(23 downto 0)     <= cpllfbclklost(23 downto 0);
    register_map_gbt_monitor.GBT_PLL_LOCK.CPLL_LOCK(23 downto 0)  <= cplllock(23 downto 0);
    register_map_gbt_monitor.GBT_PLL_LOCK.QPLL_LOCK(53 downto 48) <= (others => '0');
    register_map_gbt_monitor.GBT_RXCDR_LOCK(23 downto 0)          <= GBT_RXCDR_LOCK; --RxCdrLock_int(23 downto 0);

    register_map_gbt_monitor.GBT_ALIGNMENT_DONE(23 downto 0)      <= GBT_ALIGNMENT_DONE; --from fullmode_auto_rx_reset 
    ----------------------------------
    -- Registers for FULL mode
    ----------------------------------
    register_map_gbt_monitor.GBT_FM_RX_DISP_ERROR1      <= GT_RxDisperr(47 downto 0);
    register_map_gbt_monitor.GBT_FM_RX_DISP_ERROR2      <= GT_RxDisperr(95 downto 48);
    register_map_gbt_monitor.GBT_FM_RX_NOTINTABLE1      <= (others => '0');  -- unused
    register_map_gbt_monitor.GBT_FM_RX_NOTINTABLE2      <= (others => '0');  -- unused

    -------
    datamod_gen1 : if DYNAMIC_DATA_MODE_EN='1' generate
        DATA_TXFORMAT_i <= DATA_TXFORMAT;
    end generate;

    datamod_gen2 : if DYNAMIC_DATA_MODE_EN='0' generate
        DATA_TXFORMAT_i <= GBT_DATA_TXFORMAT_PACKAGE(47 downto 0);
    end generate;

    autorxreset0: entity work.fullmode_auto_rxreset     generic map(
            GBT_NUM => GBT_NUM
        )
        port map(
            clk40_in => clk40_in,
            register_map_control => register_map_control,
            GBT_ALIGNMENT_DONE => GBT_ALIGNMENT_DONE,
            GT_RXUSRCLK => GT_RXUSRCLK,
            RX_DATA_33b_in => RX_DATA_33b_s,
            RxDisperr_in => RxDisperr,
            RxDisperr_out => GT_RxDisperr,
            RXRESET_AUTO => RXRESET_AUTO(GBT_NUM-1 downto 0),
            GBT_RXCDR_LOCK => GBT_RXCDR_LOCK,
            rxcdrlock_in => rxcdrlock,
            RXByteisAligned => RXByteisAligned
        );




    gbtTx : for i in GBT_NUM-1 downto 0 generate

        process(GT_TX_WORD_CLK)
        begin
            if rising_edge(GT_TX_WORD_CLK(i)) then
                TX_RESET_i(i) <= TX_RESET(i) or (not txresetdone(i)) or (not txfsmresetdone(i));-- for V7
            end if;
        end process;

        process(TX_FRAME_CLK_I)
        begin
            if rising_edge(TX_FRAME_CLK_I) then
                TX_120b_in_i(i) <= TX_120b_in(i);
            end if;
        end process;
        
        sync_TX_120b_in : xpm_cdc_array_single
           generic map (
              DEST_SYNC_FF => 6, --Equivalent to 1 40 MHz cycle.
              INIT_SYNC_FF => 0,
              SIM_ASSERT_CHK => 0,
              SRC_INPUT_REG => 0,
              WIDTH => 120
           )
           port map (
              dest_out => TX_120b_in_wc(i),
              dest_clk => GT_TX_WORD_CLK(i),
              src_clk => clk40_in,
              src_in => TX_120b_in(i)
           );

        gbtTx_inst: entity work.gbtTx_FELIX
            generic map
            (
                channel                     => i
            )

            Port map
            (
                TX_FLAG => TX_FLAG_O(i),
                TX_RESET_I => TX_RESET_i(i),
                TX_FRAMECLK_I => TX_FRAME_CLK_I,
                TX_WORDCLK_I => GT_TX_WORD_CLK(i),
                Tx_latopt_scr => TX_OPT(24+i),
                TX_LATOPT_TC => TX_OPT(i),
                TX_TC_METHOD => TX_TC_METHOD(i),
                TC_EDGE => TC_EDGE(i),
                DATA_MODE_CFG => DATA_TXFORMAT_i(2*i+1 downto 2*i),
                TX_TC_DLY_VALUE => TX_TC_DLY_VALUE(4*i+2 downto 4*i),
                --TX_ISDATA_SEL_I  => TX_ISDATA_SEL_I,
                TX_HEADER_I => TX_120b_in_i(i)(119 downto 116),
                TX_DATA_84b_I => TX_120b_in_wc(i)(115 downto 32),
                TX_EXTRA_DATA_WIDEBUS_I => TX_120b_in_wc(i)(31 downto 0),
                TX_DATA_20b_O => TX_DATA_20b(i)
            );

    end generate;

    TXUSRCLK_OUT <= GT_TXUSRCLK(0);

    clk_generate : for i in (GBT_NUM-1) downto 0 generate

        --  GTTXOUTCLK_BUFG: BUFG
        --  port map
        --  (
        --    I => GT_TXOUTCLK(4*i),
        --    O => GT_TXUSRCLK(i)
        --   );

        GTTXOUTCLK_BUFG: BUFG_GT -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
            port map (
                O       => GT_TXUSRCLK(i),
                CE      => '1',           -- 1-bit input: Buffer enable
                CEMASK  => '0',   -- 1-bit input: CE Mask
                CLR     => '0',         -- 1-bit input: Asynchronous clear
                CLRMASK => '0', -- 1-bit input: CLR Mask
                DIV     => "000",         -- 3-bit input: Dynamic divide Value
                I       => GT_TXOUTCLK(i)              -- 1-bit input: Buffer
            );

        GT_TX_WORD_CLK(i) <= GT_TXUSRCLK(i);

    end generate;

    clk_generate1 : for i in GBT_NUM-1 downto 0 generate
        GTRXOUTCLK_BUFG: BUFG_GT -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
            port map (
                O       => GT_RXUSRCLK(i),
                CE      => '1',
                CEMASK  => '0',
                CLR     => '0',
                CLRMASK => '0',
                DIV     => "000",
                I       => GT_RXOUTCLK(i)
            );
        RXUSRCLK_OUT(i) <= GT_RXUSRCLK(i);
    end generate;

    ---------------------------------
    -- GTH TX using CPLL
    -- GTH RX using QPLL
    ---------------------------------
    GTH_inst: for i in (GBT_NUM-1) downto 0 generate
        signal SOFT_TXRST_GT_g, SOFT_RXRST_GT_g: std_logic;
    begin

        SOFT_TXRST_GT_g <= SOFT_TXRST_GT(i) or SOFT_TXRST_ALL(i/4) or rst_hw;
        SOFT_RXRST_GT_g <= SOFT_RXRST_GT(i) or SOFT_RXRST_ALL(i/4) or rst_hw;

        rxcommadeten_in(i) <='1';--not kchar_fail_rst(4*i+0);



        GTH_FM_TOP_INST: entity work.gth_fullmode_wrapper_ku
            Port map
(
                ---------- Clocks
                GTH_RefClk => GTH_RefClk(i),
                DRP_CLK_IN => DRP_CLK_IN,
                gt0_rxusrclk_in => GT_RXUSRCLK(i), --clk240_in,
                gt0_rxoutclk_out => GT_RXOUTCLK(i),
                gt0_txusrclk_in => GT_TXUSRCLK(i),
                gt0_txoutclk_out => GT_TXOUTCLK(i),
                reset_all_in => soft_reset(i),
                cpllreset_in => cpll_reset(i downto i),
                rxcommadeten_in => rxcommadeten_in(i),
                reset_tx_pll_and_datapath_in => SOFT_TXRST_GT_g,
                reset_tx_datapath_in => SOFT_TXRST_GT_g,
                reset_rx_pll_and_datapath_in => qpll_reset(i/4),
                reset_rx_datapath_in => SOFT_RXRST_GT_g,
                gt_cplllock_out => cplllock(i),
                gt_cpllfbclklost_out => cpllfbclklost(i),
                gt_txresetdone_out => txresetdone(i),
                gt_rxresetdone_out => rxresetdone(i),
                gt_txfsmresetdone_out => txfsmresetdone(i),
                gt_rxfsmresetdone_out => rxfsmresetdone(i),
                gt_rxcdrlock_out => rxcdrlock(i),
                gt_rxbyteisaligned_out => RXByteisAligned(i),
                gt0_rxdisperr_out => RxDisperr(4*i+3 downto 4*i),
                RX_DATA_gt0_33b => RX_DATA_33b_s(i), --gt_rx_data_33b(4*i),  --RX_DATA_33b(4*i),
                TX_DATA_gt0_20b => TX_DATA_20b(i),
                RXN_IN => RX_N(i),
                RXP_IN => RX_P(i),
                TXN_OUT => TX_N(i),
                TXP_OUT => TX_P(i)

            );





    end generate;

g_ph1: if Phase2CRToHost_FM = false generate
    g_fifo: for i in GBT_NUM-1 downto 0 generate
        signal fifo_rd_en, fifo_empty, fifo_wr_en, fifo_full : std_logic;
        signal fifo_reset: std_logic;
        signal fifo_reset_rxusrclk, fifo_reset_clk250_in: std_logic;
        signal alignment_done_rxusrclk: std_logic;
    begin

        reset_proc: process(DRP_CLK_IN)
        begin
            if rising_edge(DRP_CLK_IN) then
                if rst_hw = '1' or GBT_ALIGNMENT_DONE(i) = '0' then -- GT_RxByteisAligned(i) = '0' then
                    fifo_reset <= '1';
                else
                    fifo_reset <= '0';
                end if;
            end if;
        end process;
        
        xpm_cdc_fifo_reset_rxusrclk : xpm_cdc_sync_rst
           generic map (
              DEST_SYNC_FF => 2,
              INIT => 1,
              INIT_SYNC_FF => 1,
              SIM_ASSERT_CHK => 0
           )
           port map (
              dest_rst => fifo_reset_rxusrclk,
              dest_clk => GT_RXUSRCLK(i),
              src_rst => fifo_reset
           );
        
        xpm_cdc_fifo_reset_clk250_in : xpm_cdc_sync_rst
           generic map (
              DEST_SYNC_FF => 2,
              INIT => 1,
              INIT_SYNC_FF => 1,
              SIM_ASSERT_CHK => 0
           )
           port map (
              dest_rst => fifo_reset_clk250_in,
              dest_clk => clk250_in,
              src_rst => fifo_reset
           );
           
        xpm_cdc_single_alignment_done_inst : xpm_cdc_single
           generic map (
              DEST_SYNC_FF => 2,
              INIT_SYNC_FF => 1,
              SIM_ASSERT_CHK => 0,
              SRC_INPUT_REG => 0
           )
           port map (
              dest_out => alignment_done_rxusrclk,
              dest_clk => GT_RXUSRCLK(i),
              src_clk => clk40_in,
              src_in =>  GBT_ALIGNMENT_DONE(i)
           );
        
        fifo0: thFMch_input_fifo
            port map(
                wr_clk => GT_RXUSRCLK(i),
                wr_rst => fifo_reset_rxusrclk,
                rd_clk => clk250_in,
                rd_rst => fifo_reset_clk250_in,
                din => RX_DATA_33b_s(i),
                wr_en => fifo_wr_en,
                rd_en => fifo_rd_en,
                dout => RX_DATA_33b(i),
                full => fifo_full,
                empty => fifo_empty,
                valid => RX_DATA_33b_rdy(i)
            );

        fifo_rd_en <= not fifo_empty;
        fifo_wr_en <= alignment_done_rxusrclk and not fifo_full;
    end generate;
  end generate g_ph1;
  g_ph2: if Phase2CRToHost_FM generate
    RX_DATA_33b <= RX_DATA_33b_s;
    RX_DATA_33b_rdy <= GBT_ALIGNMENT_DONE(GBT_NUM-1 downto 0);
  end generate g_ph2;

end Behavioral;
