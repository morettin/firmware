--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Kai Chen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-------------------------------------------------------------------------------
-- Kai Chen @ BNL
-- July, 2016
-- For PRBS bit error counting
-- Support PASUE

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

--***********************************Entity Declaration*******************************

entity gtx_one_PRBS_CHK is
generic
( 
    TX_DATA_WIDTH            : integer := 64
);
port
(
    -- User Interface
    --UNSCRAMBLED_DATA_IN      : in  std_logic_vector((TX_DATA_WIDTH-1) downto 0); 
    DATA_IN       : in std_logic_vector((TX_DATA_WIDTH-1) downto 0); 
    DATA_OUT_o : out std_logic_vector((TX_DATA_WIDTH-1) downto 0); 
    DATA_VALID_IN            : in  std_logic;
    --errcnt: out std_logic_vector(15 downto 0);
    -- System Interface
    USER_CLK                 : in  std_logic;      
    SYSTEM_RESET             : in  std_logic
);


end gtx_one_PRBS_CHK;

architecture RTL of gtx_one_PRBS_CHK is


--***********************************Parameter Declarations********************

    constant DLY : time := 1 ns;

--***************************Internal Register Declarations******************** 

    signal   poly               :  std_logic_vector(30 downto 0);
    signal   scrambler          :  std_logic_vector(30 downto 0);
    signal   scrambled_data_i   :  std_logic_vector((TX_DATA_WIDTH-1) downto 0);
    signal   tempdata,frame_head           :  std_logic_vector((TX_DATA_WIDTH-1) downto 0);
    signal err_cnt_i: std_logic_vector(14 downto 0):=(others=>'0');
    signal find_start,find_head:std_logic:='0';

--*********************************Main Body of Code***************************
begin


    process( scrambler )
    variable   poly_i       :  std_logic_vector(30 downto 0);
    variable   tempData_i   :  std_logic_vector((TX_DATA_WIDTH-1) downto 0);
    variable   xorBit     :  std_logic;
    variable   i          :  integer;
    begin
        poly_i := scrambler;
        for  i in 0 to (TX_DATA_WIDTH-1) loop
            xorBit := poly_i(27) xor poly_i(30);--UNSCRAMBLED_DATA_IN(i) xor poly_i(27) xor poly_i(30);
            poly_i   := (poly_i(29 downto 0) & xorBit);
            tempData_i(i) := xorBit;
        end loop;
        poly          <=   poly_i;
        tempdata      <=   tempdata_i;
    end process;

    --________________ Scrambled Data assignment to output port _______________    

    process( USER_CLK )
    begin
        if(USER_CLK'event and USER_CLK = '1') then
            if (SYSTEM_RESET = '1') then
                scrambler          <= "0000000000000000000000000000000" after DLY;
                --PRBS_DATA_OUT <= (others => '0') after DLY;
                
                find_head<='0';
            elsif (DATA_VALID_IN = '1') then 
                if DATA_IN = frame_head or find_head='1' then
                    scrambler          <=   poly after DLY;
                    find_head <='1';
              --      PRBS_DATA_OUT <=   tempdata after DLY;
                  end if;
                
            end if;
        end if;
    end process;
    
    process( USER_CLK )
      begin
          if(USER_CLK'event and USER_CLK = '1') then  
                if (SYSTEM_RESET = '1' and find_start ='0') then
                        find_start<='1';
                        frame_head<=frame_head;
                 elsif find_start = '1' then
                        find_start <='0';
                        frame_head <=tempdata;
                   else
                        frame_head<=frame_head;
                        find_start<=find_start;
                  end if;
                 end if;
      end process;
    
   
                    
      process(USER_CLK)      
      begin
         if(USER_CLK'event and USER_CLK = '1') then
            if SYSTEM_RESET = '1' then
                err_cnt_i <= (others=>'0');
            elsif find_head='1' then
                    if DATA_IN /= tempdata then
                        err_cnt_i <= err_cnt_i + '1';
                       else
                        err_cnt_i <= err_cnt_i;
                     end if;
              
              end if;
              end if;
              end process;
              
              errcnt <= err_cnt_i & find_head;
                    
         
         
end RTL;

