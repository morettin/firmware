--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/09/2016 01:34:52 PM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------



library  ieee, UNISIM;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity top is
  Port (
  
  Q2_CLK0_GTREFCLK_PAD_N_IN : in     std_logic;
  Q2_CLK0_GTREFCLK_PAD_P_IN : in     std_logic;
 
  opto_inhibit              : out    STD_LOGIC_VECTOR(1 downto 0);
 
  RX_N                      : in     std_logic_vector(3 downto 0);
  RX_P                      : in     std_logic_vector(3 downto 0);
 
  TX_N                      : out    std_logic_vector(3 downto 0);
  TX_P                      : out    std_logic_vector(3 downto 0);
  app_clk_in_n              : in     std_logic;
  app_clk_in_p              : in     std_logic
  
  
   );
end top;

architecture Behavioral of top is

signal gt3_gthrxn_in, gt3_gthrxp_in, gt3_gthtxp_out, gt3_gthtxn_out,gt3_rxcharisk_out0,gt2_rxcharisk_out0_r : std_logic;
signal gt2_gthrxn_in, gt2_gthrxp_in, gt2_gthtxp_out, gt2_gthtxn_out,gt2_rxcharisk_out0,gt0_rxcharisk_out0_r : std_logic;
signal gt1_gthrxn_in, gt1_gthrxp_in, gt1_gthtxp_out, gt1_gthtxn_out,gt1_rxcharisk_out0 ,gt1_rxcharisk_out0_r: std_logic;
signal gt0_gthrxn_in, gt0_gthrxp_in, gt0_gthtxp_out, gt0_gthtxn_out,gt0_rxcharisk_out0,gt3_rxcharisk_out0_r : std_logic;

signal grefclk,sysclk_in_i,comma_en_p,q2_clk0_refclk_i:std_logic;

signal gt3_tx_fsm_reset_done_out: std_logic;
signal gt2_tx_fsm_reset_done_out: std_logic;
signal gt1_tx_fsm_reset_done_out: std_logic;
signal gt0_tx_fsm_reset_done_out: std_logic;

signal gt3_rx_fsm_reset_done_out: std_logic;
signal gt2_rx_fsm_reset_done_out: std_logic;
signal gt1_rx_fsm_reset_done_out: std_logic;
signal gt0_rx_fsm_reset_done_out: std_logic;

signal gt0_rxresetdone_out, gt0_txresetdone_out, gt0_qpllreset_t :std_logic;
signal gt1_rxresetdone_out, gt1_txresetdone_out :std_logic;
signal gt2_rxresetdone_out, gt2_txresetdone_out :std_logic;
signal gt3_rxresetdone_out, gt3_txresetdone_out :std_logic;
signal gt_gtrxreset_in, gt_gttxreset_in,gt0_txcharisk_in_p:std_logic_vector(3 downto 0);
signal error_cnt3,error_cnt2,error_cnt1,error_cnt0:std_logic_vector(9 downto 0);

signal SOFT_RESET_TX_IN, SOFT_RESET_RX_IN,error_clr:std_logic_vector(0 downto 0);

signal comma_type : std_logic_vector(1 downto 0);

signal gt0_txusrclk2_i, gt0_txusrclk_i,gt0_rxusrclk2_i, gt0_rxusrclk_i, gt0_txoutclk_i, gt0_rxoutclk_i: std_logic;
signal gt1_txusrclk2_i, gt1_txusrclk_i,gt1_rxusrclk2_i, gt1_rxusrclk_i, gt1_txoutclk_i, gt1_rxoutclk_i: std_logic;
signal gt2_txusrclk2_i, gt2_txusrclk_i,gt2_rxusrclk2_i, gt2_rxusrclk_i, gt2_txoutclk_i, gt2_rxoutclk_i: std_logic;
signal gt3_txusrclk2_i, gt3_txusrclk_i,gt3_rxusrclk2_i, gt3_rxusrclk_i, gt3_txoutclk_i, gt3_rxoutclk_i: std_logic;
signal gt0_gtrxreset_in, gt1_gtrxreset_in, gt2_gtrxreset_in,gt3_gtrxreset_in:std_logic;
signal gt0_gttxreset_in, gt1_gttxreset_in, gt2_gttxreset_in,gt3_gttxreset_in:std_logic;

signal gt0_txdata_in, gt0_rxdata_out, gt0_rxdata_chk : std_logic_vector(31 downto 0);
signal gt1_txdata_in, gt1_rxdata_out,gt1_rxdata_out_r, gt1_rxdata_chk,gt1_rxdata_chk_r : std_logic_vector(31 downto 0);
signal gt2_txdata_in, gt2_rxdata_out,gt2_rxdata_out_r, gt2_rxdata_chk,gt2_rxdata_chk_r : std_logic_vector(31 downto 0);
signal gt3_txdata_in, gt3_rxdata_out, gt3_rxdata_chk,gt3_rxdata_chk_r : std_logic_vector(31 downto 0);

signal gt0_txcharisk_in, gt0_rxcharisk_out: std_logic_vector(3 downto 0);
signal gt1_txcharisk_in, gt1_rxcharisk_out,gt1_rxcharisk_out_r: std_logic_vector(3 downto 0);
signal gt2_txcharisk_in, gt2_rxcharisk_out,gt2_rxcharisk_out_r: std_logic_vector(3 downto 0);
signal gt3_txcharisk_in, gt3_rxcharisk_out,gt3_rxcharisk_out_r: std_logic_vector(3 downto 0);

signal prbscnt:std_logic_vector(9 downto 0);


signal    gt0_qplllock_i, gt0_qpllrefclklost_i, gt0_qpllreset_i, gt0_qplloutclk_i, gt0_qplloutrefclk_i,commonreset_i,tied_to_ground_i,tied_to_vcc_i:std_logic;

begin


opto_inhibit <="11";

gt3_gthrxn_in <= RX_N(3);
gt3_gthrxp_in <= RX_P(3);
gt2_gthrxn_in <= RX_N(2);
gt2_gthrxp_in <= RX_P(2);
gt1_gthrxn_in <= RX_N(1);
gt1_gthrxp_in <= RX_P(1);
gt0_gthrxn_in <= RX_N(0);
gt0_gthrxp_in <= RX_P(0);

TX_P(0) <= gt0_gthtxp_out;
TX_N(0) <= gt0_gthtxn_out;
TX_P(1) <= gt1_gthtxp_out;
TX_N(1) <= gt1_gthtxn_out;
TX_P(2) <= gt2_gthtxp_out;
TX_N(2) <= gt2_gthtxn_out;
TX_P(3) <= gt3_gthtxp_out;
TX_N(3) <= gt3_gthtxn_out;



clkgen : entity work.clk_wiz_0 
 port map(
  clk_in1_p => app_clk_in_p,
  clk_in1_n => app_clk_in_n,
 
  clk_out1 => grefclk,
  clk_out2 => sysclk_in_i,
  
  reset  => '0',
  locked => open
 );
 
 vio_inst: entity work.vio_0
 PORT MAP(
 clk => sysclk_in_i,
 probe_in0 => gt3_tx_fsm_reset_done_out & gt2_tx_fsm_reset_done_out & gt1_tx_fsm_reset_done_out & gt0_tx_fsm_reset_done_out,
 probe_in1 => gt3_rx_fsm_reset_done_out & gt2_rx_fsm_reset_done_out & gt1_rx_fsm_reset_done_out & gt0_rx_fsm_reset_done_out,
 probe_in2 => gt3_rxresetdone_out & gt2_rxresetdone_out & gt1_rxresetdone_out & gt0_rxresetdone_out,
 probe_in3 => gt3_txresetdone_out & gt2_txresetdone_out & gt1_txresetdone_out & gt0_txresetdone_out,
 probe_in4 => error_cnt3,
 probe_in5 => error_cnt2,
 probe_in6 => error_cnt1,
 probe_in7 => error_cnt0,
 probe_out0 => SOFT_RESET_TX_IN,
 probe_out1 => SOFT_RESET_RX_IN,
 probe_out2 => gt_gtrxreset_in,
 probe_out3 => gt_gttxreset_in,
 probe_out4 => error_clr
 );

gt0_gtrxreset_in <= gt_gtrxreset_in(0);
gt1_gtrxreset_in <= gt_gtrxreset_in(1);
gt2_gtrxreset_in <= gt_gtrxreset_in(2);
gt3_gtrxreset_in <= gt_gtrxreset_in(3);
gt0_gttxreset_in <= gt_gttxreset_in(0);
gt1_gttxreset_in <= gt_gttxreset_in(1);
gt2_gttxreset_in <= gt_gttxreset_in(2);
gt3_gttxreset_in <= gt_gttxreset_in(3);
 

  gt0tx: entity work. gtx_one_PRBS
generic map
( 
    TX_DATA_WIDTH           => 32
)
port map
(
    -- User Interface
    --UNSCRAMBLED_DATA_IN      : in  std_logic_vector((TX_DATA_WIDTH-1) downto 0); 
    PRBS_DATA_OUT       => gt0_txdata_in,
    comma_type          => comma_type,
    DATA_VALID_IN         => not comma_en_p, 
    datain   => x"55559999",
    -- System Interface
    USER_CLK                => gt0_txusrclk2_i,    
    SYSTEM_RESET            =>'0'
);
 
-- gt0tx: entity work.PRBS_ANY
--    generic map(      
--       CHK_MODE => false,
--       INV_PATTERN => false,
--       POLY_LENGHT=> 31 ,
--       POLY_TAP => 28 ,
--       NBITS => 32
--    )
--    port map (
--       RST             => '0',--
--       CLK             => gt0_txusrclk2_i,--                         -- system clock
--       DATA_IN         => x"00000000",--
--       EN              => not comma_en_p,                           -- enable/pause pattern generation
--       DATA_OUT_o       => gt0_txdata_in
--    );
 
 
process(gt0_txusrclk2_i)
begin
if gt0_txusrclk2_i'event and gt0_txusrclk2_i='1' then

  gt1_txdata_in <=gt0_txdata_in;
  gt2_txdata_in <=gt0_txdata_in;
  gt3_txdata_in <=gt0_txdata_in;
  gt3_txcharisk_in <= gt0_txcharisk_in;
  gt1_txcharisk_in <= gt0_txcharisk_in;
  gt2_txcharisk_in <= gt0_txcharisk_in;


 gt0_txcharisk_in_p <= "000" & comma_en_p;
 gt0_txcharisk_in <= gt0_txcharisk_in_p;
 prbscnt <= prbscnt + '1';
 if prbscnt="0000000000" then
     comma_en_p <='1';
     comma_type <= "01"; --SOP  28.1
 elsif prbscnt="0000000001" then
     comma_en_p <='0';  
 elsif prbscnt="0100000000" then
     comma_en_p <='1';
     comma_type <= "10"; --EOP  28.6
 elsif prbscnt="0100000001" then
     comma_en_p <='1';
     comma_type <= "00"; --IDLE  28.5
 else    
     comma_en_p <=comma_en_p;
     comma_type <= comma_type;
 end if;
 
end if;
end process; 


biterr1: entity work.bit_err_calc
  Port map( 

dataA => x"55559999",
dataB => gt0_rxdata_chk,
error_counter => error_cnt0,
error_clr => error_clr(0),
clk => gt0_rxusrclk2_i,
charflag => gt0_rxcharisk_out0_r

);
biterr2: entity work.bit_err_calc
  Port map( 

dataA => x"55559999",
dataB => gt1_rxdata_chk,
error_counter => error_cnt1,
error_clr => error_clr(0),
clk => gt1_rxusrclk2_i,
charflag => gt1_rxcharisk_out0_r

);
biterr3: entity work.bit_err_calc
  Port map( 

dataA => x"55559999",
dataB => gt2_rxdata_chk,
error_counter => error_cnt2,
error_clr => error_clr(0),
clk => gt2_rxusrclk2_i,
charflag => gt2_rxcharisk_out0_r

);
biterr4: entity work.bit_err_calc
  Port map( 

dataA => x"55559999",
dataB => gt3_rxdata_chk,
error_counter => error_cnt3,
error_clr => error_clr(0),
clk => gt3_rxusrclk2_i,
charflag => gt3_rxcharisk_out0_r

);


process(gt0_rxusrclk2_i,error_clr)
begin
if gt0_rxusrclk2_i'event and gt0_rxusrclk2_i='1' then
gt0_rxcharisk_out0 <= gt0_rxcharisk_out(0);
gt1_rxcharisk_out0 <= gt1_rxcharisk_out(0);
gt2_rxcharisk_out0 <= gt2_rxcharisk_out(0);
gt3_rxcharisk_out0 <= gt3_rxcharisk_out(0);
gt0_rxcharisk_out0_r <=gt0_rxcharisk_out0;
gt1_rxcharisk_out0_r <=gt1_rxcharisk_out0;
gt2_rxcharisk_out0_r <=gt2_rxcharisk_out0;
gt3_rxcharisk_out0_r <=gt3_rxcharisk_out0;
--if error_clr(0)='1' then
-- error_cnt0 <="000000";
--elsif gt0_rxdata_chk/=x"55559999" and gt0_rxcharisk_out0_r='0' then
--error_cnt0 <= error_cnt0 +'1';
--elsif gt0_rxdata_chk/=x"123456bc" and gt0_rxcharisk_out0_r='1' then
-- error_cnt0 <= error_cnt0 +'1';
-- end if;
-- if error_clr(0)='1' then
--  error_cnt1 <="000000";
-- elsif gt1_rxdata_chk/=x"55559999" and gt1_rxcharisk_out0_r='0' then
--  error_cnt1 <= error_cnt1 +'1';
--  elsif gt1_rxdata_chk/=x"123456bc" and gt1_rxcharisk_out0_r='1' then
--   error_cnt1 <= error_cnt1 +'1';
--  end if;
--  if error_clr(0)='1' then
--   error_cnt2 <="000000";
--  elsif gt2_rxdata_chk/=x"55559999" and gt2_rxcharisk_out0_r='0' then
--   error_cnt2 <= error_cnt2 +'1';
--   elsif gt2_rxdata_chk/=x"123456bc" and gt2_rxcharisk_out0_r='1' then
--    error_cnt2 <= error_cnt2 +'1';
--   end if;
--   if error_clr(0)='1' then
--    error_cnt3 <="000000";
--   elsif gt3_rxdata_chk/=x"55559999" and gt3_rxcharisk_out0_r='0' then
--    error_cnt3 <= error_cnt3 +'1';
--    elsif gt3_rxdata_chk/=x"123456bc" and gt3_rxcharisk_out0_r='1' then
--     error_cnt3 <= error_cnt3 +'1';
--    end if;
 
end if;
end process;


ila_inst: entity work.ila_0
PORT MAP(
clk => gt0_txusrclk2_i,


probe0 => gt2_rxcharisk_out_r & gt2_rxdata_out_r,
probe1 => gt3_rxcharisk_out_r & gt3_rxdata_chk,
probe2 => gt2_txcharisk_in & gt2_txdata_in,
probe3 => gt1_rxcharisk_out_r & gt1_rxdata_out_r,
probe4 => comma_type,
probe5 => (others=>comma_en_p)

);

process(gt2_rxusrclk2_i)
begin
if gt2_rxusrclk2_i'event and gt2_rxusrclk2_i='1' then
gt2_rxdata_out_r <= gt2_rxdata_out;
gt2_rxcharisk_out_r <= gt2_rxcharisk_out;
end if;
end process;
process(gt1_rxusrclk2_i)
begin
if gt1_rxusrclk2_i'event and gt1_rxusrclk2_i='1' then
gt1_rxdata_out_r <= gt1_rxdata_out;
gt1_rxcharisk_out_r <= gt1_rxcharisk_out;
end if;
end process;


gt0rx: entity work.gtx_one_PRBS_CHKNEW
generic map
( 
    TX_DATA_WIDTH          => 32
)
port map
(
   
    PRBS_DATA_IN      => gt0_rxdata_out,
    DATA_VALID_IN        => not gt0_rxcharisk_out(0), 
    --errcnt: out std_logic_vector(15 downto 0);
    -- System Interface
    USER_CLK               => gt0_rxusrclk2_i,--  
    DATA_OUT       => gt0_rxdata_chk,
    SYSTEM_RESET          =>'0'
);
gt1rx: entity work.gtx_one_PRBS_CHKNEW
generic map
( 
    TX_DATA_WIDTH          => 32
)
port map
(
   
    PRBS_DATA_IN      => gt1_rxdata_out,
    DATA_VALID_IN        => not gt1_rxcharisk_out(0), 
    --errcnt: out std_logic_vector(15 downto 0);
    -- System Interface
    USER_CLK               => gt1_rxusrclk2_i,--  
    DATA_OUT       => gt1_rxdata_chk,
    SYSTEM_RESET          =>'0'
);
gt2rx: entity work.gtx_one_PRBS_CHKNEW
generic map
( 
    TX_DATA_WIDTH          => 32
)
port map
(
   
    PRBS_DATA_IN      => gt2_rxdata_out,
    DATA_VALID_IN        => not gt2_rxcharisk_out(0), 
    --errcnt: out std_logic_vector(15 downto 0);
    -- System Interface
    USER_CLK               => gt2_rxusrclk2_i,--  
    DATA_OUT       => gt2_rxdata_chk,
    SYSTEM_RESET          =>'0'
);
gt3rx: entity work.gtx_one_PRBS_CHKNEW
generic map
( 
    TX_DATA_WIDTH          => 32
)
port map
(
   
    PRBS_DATA_IN      => gt3_rxdata_out,
    DATA_VALID_IN        => not gt3_rxcharisk_out(0), 
    --errcnt: out std_logic_vector(15 downto 0);
    -- System Interface
    USER_CLK               => gt3_rxusrclk2_i,--  
    DATA_OUT       => gt3_rxdata_chk,
    SYSTEM_RESET          =>'0'
);

-- gt0rx: entity work.PRBS_ANY_rx
--    generic map(      
--       CHK_MODE=>true,
--       INV_PATTERN => false,
--       POLY_LENGHT => 31,
--       POLY_TAP => 28 ,
--       NBITS => 32
--    )
--    port map (
--       RST             => '0',--
--       CLK             => gt0_rxusrclk2_i,--                         -- system clock
--       DATA_IN         => gt0_rxdata_out,--
--       EN              => not gt0_txcharisk_in(0),                           -- enable/pause pattern generation
--       DATA_OUT_o       => gt0_rxdata_chk
--    );

-- gt1rx: entity work.PRBS_ANY_rx
--    generic map(      
--       CHK_MODE=>true,
--       INV_PATTERN => false,
--       POLY_LENGHT => 31,
--       POLY_TAP => 28 ,
--       NBITS => 32
--    )
--    port map (
--       RST             => '0',--
--       CLK             => gt1_rxusrclk2_i,--                         -- system clock
--       DATA_IN         => gt1_rxdata_out,--
--       EN              => not gt1_txcharisk_in(0),                           -- enable/pause pattern generation
--       DATA_OUT_o       => gt1_rxdata_chk
--    );
-- gt2rx: entity work.PRBS_ANY_rx
--       generic map(      
--          CHK_MODE=>true,
--          INV_PATTERN => false,
--          POLY_LENGHT => 31,
--          POLY_TAP => 28 ,
--          NBITS => 32
--       )
--       port map (
--          RST             => '0',--
--          CLK             => gt2_rxusrclk2_i,--                         -- system clock
--          DATA_IN         => gt2_rxdata_out,--
--          EN              => not gt2_txcharisk_in(0),                           -- enable/pause pattern generation
--          DATA_OUT_o       => gt2_rxdata_chk
--       );
-- gt3rx: entity work.PRBS_ANY_rx
--          generic map(      
--             CHK_MODE=>true,
--             INV_PATTERN => false,
--             POLY_LENGHT => 31,
--             POLY_TAP => 28 ,
--             NBITS => 32
--          )
--          port map (
--             RST             => '0',--
--             CLK             => gt3_rxusrclk2_i,--                         -- system clock
--             DATA_IN         => gt3_rxdata_out,--
--             EN              => not gt3_txcharisk_in(0),                           -- enable/pause pattern generation
--             DATA_OUT_o       => gt3_rxdata_chk
--          );           








-------------------

    --  Static signal Assigments
tied_to_ground_i                             <= '0';
tied_to_vcc_i                                <= '1';

     gt0_qpllreset_t <= commonreset_i or gt0_qpllreset_i;
   
          gt_usrclk_source : entity work.prbs8b10b_9p6g_GT_USRCLK_SOURCE
          port map
         (
       
              GT0_TXUSRCLK_OUT                =>      gt0_txusrclk_i,
              GT0_TXUSRCLK2_OUT               =>      gt0_txusrclk2_i,
              GT0_TXOUTCLK_IN                 =>      gt0_txoutclk_i,
              GT0_RXUSRCLK_OUT                =>      gt0_rxusrclk_i,
              GT0_RXUSRCLK2_OUT               =>      gt0_rxusrclk2_i,
              GT0_RXOUTCLK_IN                 =>      gt0_rxoutclk_i,
       
              GT1_TXUSRCLK_OUT                =>      gt1_txusrclk_i,
              GT1_TXUSRCLK2_OUT               =>      gt1_txusrclk2_i,
              GT1_TXOUTCLK_IN                 =>      gt1_txoutclk_i,
              GT1_RXUSRCLK_OUT                =>      gt1_rxusrclk_i,
              GT1_RXUSRCLK2_OUT               =>      gt1_rxusrclk2_i,
              GT1_RXOUTCLK_IN                 =>      gt1_rxoutclk_i,
       
              GT2_TXUSRCLK_OUT                =>      gt2_txusrclk_i,
              GT2_TXUSRCLK2_OUT               =>      gt2_txusrclk2_i,
              GT2_TXOUTCLK_IN                 =>      gt2_txoutclk_i,
              GT2_RXUSRCLK_OUT                =>      gt2_rxusrclk_i,
              GT2_RXUSRCLK2_OUT               =>      gt2_rxusrclk2_i,
              GT2_RXOUTCLK_IN                 =>      gt2_rxoutclk_i,
       
              GT3_TXUSRCLK_OUT                =>      gt3_txusrclk_i,
              GT3_TXUSRCLK2_OUT               =>      gt3_txusrclk2_i,
              GT3_TXOUTCLK_IN                 =>      gt3_txoutclk_i,
              GT3_RXUSRCLK_OUT                =>      gt3_rxusrclk_i,
              GT3_RXUSRCLK2_OUT               =>      gt3_rxusrclk2_i,
              GT3_RXOUTCLK_IN                 =>      gt3_rxoutclk_i,
              Q1_CLK1_GTREFCLK_PAD_N_IN       =>      Q2_CLK0_GTREFCLK_PAD_N_IN,
              Q1_CLK1_GTREFCLK_PAD_P_IN       =>      Q2_CLK0_GTREFCLK_PAD_P_IN,
              Q1_CLK1_GTREFCLK_OUT            =>      q2_clk0_refclk_i
      
          );
     


  prbs8b10b_9p6g_init_i : entity work.prbs8b10b_9p6g
    port map
    (
        sysclk_in                       =>      sysclk_in_i,
        soft_reset_tx_in                =>      SOFT_RESET_TX_IN(0),
        soft_reset_rx_in                =>      SOFT_RESET_RX_IN(0),
        dont_reset_on_data_error_in     =>      '1',--DONT_RESET_ON_DATA_ERROR_IN,
        gt0_tx_fsm_reset_done_out       =>      gt0_tx_fsm_reset_done_out,
        gt0_rx_fsm_reset_done_out       =>      gt0_rx_fsm_reset_done_out,
        gt0_data_valid_in               =>      '1',--gt0_data_valid_in,
        gt1_tx_fsm_reset_done_out       =>      gt1_tx_fsm_reset_done_out,
        gt1_rx_fsm_reset_done_out       =>      gt1_rx_fsm_reset_done_out,
        gt1_data_valid_in               =>      '1',--gt1_data_valid_in,
        gt2_tx_fsm_reset_done_out       =>      gt2_tx_fsm_reset_done_out,
        gt2_rx_fsm_reset_done_out       =>      gt2_rx_fsm_reset_done_out,
        gt2_data_valid_in               =>      '1',--gt2_data_valid_in,
        gt3_tx_fsm_reset_done_out       =>      gt3_tx_fsm_reset_done_out,
        gt3_rx_fsm_reset_done_out       =>      gt3_rx_fsm_reset_done_out,
        gt3_data_valid_in               =>      '1',--gt3_data_valid_in,

        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0  (X1Y4)

        ---------------------------- Channel - DRP Ports  --------------------------
        gt0_drpaddr_in                  =>      (others=>'0'),--gt0_drpaddr_in,
        gt0_drpclk_in                   =>      sysclk_in_i,
        gt0_drpdi_in                    =>      (others=>'0'),--gt0_drpdi_in,
        gt0_drpdo_out                   =>      open,--gt0_drpdo_out,
        gt0_drpen_in                    =>      '0',--(others=>'0'),--gt0_drpen_in,
        gt0_drprdy_out                  =>      open,--gt0_drprdy_out,
        gt0_drpwe_in                    =>      '0',--(others=>'0'),--gt0_drpwe_in,
        --------------------- RX Initialization and Reset Ports --------------------
        gt0_eyescanreset_in             =>      '0',--gt0_eyescanreset_in,
        gt0_rxuserrdy_in                =>      '1',--gt0_rxuserrdy_in,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt0_eyescandataerror_out        =>     open,-- gt0_eyescandataerror_out,
        gt0_eyescantrigger_in           =>      '0',--(others=>'0'),--gt0_eyescantrigger_in,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt0_dmonitorout_out             =>      open,--gt0_dmonitorout_out,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt0_rxusrclk_in                 =>      gt0_rxusrclk_i,
        gt0_rxusrclk2_in                =>      gt0_rxusrclk2_i,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt0_rxdata_out                  =>      gt0_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt0_rxdisperr_out               =>      open,--gt0_rxdisperr_out,
        gt0_rxnotintable_out            =>      open,--gt0_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt0_gthrxn_in                   =>      gt0_gthrxn_in,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt0_rxmcommaalignen_in          =>      '0',--gt0_rxmcommaalignen_in,
        gt0_rxpcommaalignen_in          =>      '1',--gt0_rxpcommaalignen_in,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt0_rxmonitorout_out            =>      open,--gt0_rxmonitorout_out,
        gt0_rxmonitorsel_in             =>      (others=>'0'),--gt0_rxmonitorsel_in,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt0_rxoutclkfabric_out          =>      open,--gt0_rxoutclkfabric_out,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt0_gtrxreset_in                =>      gt0_gtrxreset_in,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt0_rxcharisk_out               =>      gt0_rxcharisk_out,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt0_gthrxp_in                   =>      gt0_gthrxp_in,
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt0_rxresetdone_out             =>      gt0_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt0_gttxreset_in                =>      gt0_gttxreset_in,
        gt0_txuserrdy_in                =>      '1',--gt0_txuserrdy_in,
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt0_txusrclk_in                 =>      gt0_txusrclk_i,
        gt0_txusrclk2_in                =>      gt0_txusrclk2_i,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt0_txdata_in                   =>      gt0_txdata_in,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt0_gthtxn_out                  =>      gt0_gthtxn_out,
        gt0_gthtxp_out                  =>      gt0_gthtxp_out,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt0_txoutclk_out                =>      gt0_txoutclk_i,
        gt0_rxoutclk_out                =>      gt0_rxoutclk_i,
        gt0_txoutclkfabric_out          =>      open,--gt0_txoutclkfabric_out,
        gt0_txoutclkpcs_out             =>      open,--gt0_txoutclkpcs_out,
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt0_txresetdone_out             =>      gt0_txresetdone_out,
        ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt0_txcharisk_in                =>      gt0_txcharisk_in,



        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT1  (X1Y5)

        ---------------------------- Channel - DRP Ports  --------------------------
        gt1_drpaddr_in                  =>      (others=>'0'),--gt1_drpaddr_in,
        gt1_drpclk_in                   =>      sysclk_in_i,
        gt1_drpdi_in                    =>      (others=>'0'),--gt1_drpdi_in,
        gt1_drpdo_out                   =>      open,--gt1_drpdo_out,
        gt1_drpen_in                    =>      '0',--(others=>'0'),--gt1_drpen_in,
        gt1_drprdy_out                  =>      open,--gt1_drprdy_out,
        gt1_drpwe_in                    =>     '0',--(others=>'0'),-- gt1_drpwe_in,
        --------------------- RX Initialization and Reset Ports --------------------
        gt1_eyescanreset_in             =>      '0',--gt1_eyescanreset_in,
        gt1_rxuserrdy_in                =>      '1',--gt1_rxuserrdy_in,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt1_eyescandataerror_out        =>      open,--gt1_eyescandataerror_out,
        gt1_eyescantrigger_in           =>      '0',--(others=>'0'),--gt1_eyescantrigger_in,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt1_dmonitorout_out             =>      open,--gt1_dmonitorout_out,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt1_rxusrclk_in                 =>      gt1_rxusrclk_i,
        gt1_rxusrclk2_in                =>      gt1_rxusrclk2_i,
        gt1_rxoutclk_out                =>      gt1_rxoutclk_i,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt1_rxdata_out                  =>      gt1_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt1_rxdisperr_out               =>      open,--gt1_rxdisperr_out,
        gt1_rxnotintable_out            =>      open,--gt1_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt1_gthrxn_in                   =>      gt1_gthrxn_in,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt1_rxmcommaalignen_in          =>      '0',--gt1_rxmcommaalignen_in,
        gt1_rxpcommaalignen_in          =>      '1',--gt1_rxpcommaalignen_in,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt1_rxmonitorout_out            =>      open,--gt1_rxmonitorout_out,
        gt1_rxmonitorsel_in             =>      (others=>'0'),--gt1_rxmonitorsel_in,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt1_rxoutclkfabric_out          =>      open,--gt1_rxoutclkfabric_out,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt1_gtrxreset_in                =>      gt1_gtrxreset_in,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt1_rxcharisk_out               =>      gt1_rxcharisk_out,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt1_gthrxp_in                   =>      gt1_gthrxp_in,
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt1_rxresetdone_out             =>      gt1_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt1_gttxreset_in                =>      gt1_gttxreset_in,
        gt1_txuserrdy_in                =>      '1',--gt1_txuserrdy_in,
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt1_txusrclk_in                 =>      gt1_txusrclk_i,
        gt1_txusrclk2_in                =>      gt1_txusrclk2_i,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt1_txdata_in                   =>      gt1_txdata_in,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt1_gthtxn_out                  =>      gt1_gthtxn_out,
        gt1_gthtxp_out                  =>      gt1_gthtxp_out,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt1_txoutclk_out                =>      gt1_txoutclk_i,
        gt1_txoutclkfabric_out          =>      open,--gt1_txoutclkfabric_out,
        gt1_txoutclkpcs_out             =>      open,--gt1_txoutclkpcs_out,
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt1_txresetdone_out             =>      gt1_txresetdone_out,
        ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt1_txcharisk_in                =>      gt1_txcharisk_in,



        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT2  (X1Y6)

        ---------------------------- Channel - DRP Ports  --------------------------
        gt2_drpaddr_in                  =>      (others=>'0'),--gt2_drpaddr_in,
        gt2_drpclk_in                   =>      sysclk_in_i,
        gt2_drpdi_in                    =>      (others=>'0'),--gt2_drpdi_in,
        gt2_drpdo_out                   =>      open,--gt2_drpdo_out,
        gt2_drpen_in                    =>      '0',--(others=>'0'),--gt2_drpen_in,
        gt2_drprdy_out                  =>      open,--gt2_drprdy_out,
        gt2_drpwe_in                    =>      '0',--(others=>'0'),--gt2_drpwe_in,
        --------------------- RX Initialization and Reset Ports --------------------
        gt2_eyescanreset_in             =>      '0',--gt2_eyescanreset_in,
        gt2_rxuserrdy_in                =>      '1',--gt2_rxuserrdy_in,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt2_eyescandataerror_out        =>      open,--gt2_eyescandataerror_out,
        gt2_eyescantrigger_in           =>      '0',--gt2_eyescantrigger_in,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt2_dmonitorout_out             =>      open,--gt2_dmonitorout_out,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt2_rxusrclk_in                 =>      gt2_rxusrclk_i,
        gt2_rxusrclk2_in                =>      gt2_rxusrclk2_i,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt2_rxdata_out                  =>      gt2_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt2_rxdisperr_out               =>      open,--gt2_rxdisperr_out,
        gt2_rxnotintable_out            =>      open,--gt2_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt2_gthrxn_in                   =>      gt2_gthrxn_in,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt2_rxmcommaalignen_in          =>      '0',--gt2_rxmcommaalignen_in,
        gt2_rxpcommaalignen_in          =>      '1',--gt2_rxpcommaalignen_in,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt2_rxmonitorout_out            =>      open,--gt2_rxmonitorout_out,
        gt2_rxmonitorsel_in             =>      (others=>'0'),--gt2_rxmonitorsel_in,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt2_rxoutclkfabric_out          =>      open,--gt2_rxoutclkfabric_out,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt2_gtrxreset_in                =>      gt2_gtrxreset_in,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt2_rxcharisk_out               =>      gt2_rxcharisk_out,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt2_gthrxp_in                   =>      gt2_gthrxp_in,
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt2_rxresetdone_out             =>      gt2_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt2_gttxreset_in                =>      gt2_gttxreset_in,
        gt2_txuserrdy_in                =>      '1',--gt2_txuserrdy_in,
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt2_txusrclk_in                 =>      gt2_txusrclk_i,
        gt2_txusrclk2_in                =>      gt2_txusrclk2_i,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt2_txdata_in                   =>      gt2_txdata_in,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt2_gthtxn_out                  =>      gt2_gthtxn_out,
        gt2_gthtxp_out                  =>      gt2_gthtxp_out,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt2_txoutclk_out                =>      gt2_txoutclk_i,
        gt2_txoutclkfabric_out          =>      open,--gt2_txoutclkfabric_out,
        gt2_txoutclkpcs_out             =>      open,--gt2_txoutclkpcs_out,
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt2_txresetdone_out             =>      gt2_txresetdone_out,
        gt2_rxoutclk_out                =>      gt2_rxoutclk_i,
        ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt2_txcharisk_in                =>      gt2_txcharisk_in,



        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT3  (X1Y7)

        ---------------------------- Channel - DRP Ports  --------------------------
        gt3_drpaddr_in                  =>      (others=>'0'),--gt3_drpaddr_in,
        gt3_drpclk_in                   =>      sysclk_in_i,
        gt3_drpdi_in                    =>      (others=>'0'),--gt3_drpdi_in,
        gt3_drpdo_out                   =>      open,--gt3_drpdo_out,
        gt3_drpen_in                    =>     '0',-- (others=>'0'),--gt3_drpen_in,
        gt3_drprdy_out                  =>      open,--gt3_drprdy_out,
        gt3_drpwe_in                    =>      '0',--(others=>'0'),--gt3_drpwe_in,
        --------------------- RX Initialization and Reset Ports --------------------
        gt3_eyescanreset_in             =>      '0',--gt3_eyescanreset_in,
        gt3_rxuserrdy_in                =>      '1',--gt3_rxuserrdy_in,
        -------------------------- RX Margin Analysis Ports ------------------------
        gt3_eyescandataerror_out        =>      open,--gt3_eyescandataerror_out,
        gt3_eyescantrigger_in           =>      '0',--(others=>'0'),--gt3_eyescantrigger_in,
        ------------------- Receive Ports - Digital Monitor Ports ------------------
        gt3_dmonitorout_out             =>      open,--gt3_dmonitorout_out,
        ------------------ Receive Ports - FPGA RX Interface Ports -----------------
        gt3_rxusrclk_in                 =>      gt3_rxusrclk_i,
        gt3_rxusrclk2_in                =>      gt3_rxusrclk2_i,
        ------------------ Receive Ports - FPGA RX interface Ports -----------------
        gt3_rxdata_out                  =>      gt3_rxdata_out,
        ------------------ Receive Ports - RX 8B/10B Decoder Ports -----------------
        gt3_rxdisperr_out               =>      open,--gt3_rxdisperr_out,
        gt3_rxnotintable_out            =>      open,--gt3_rxnotintable_out,
        ------------------------ Receive Ports - RX AFE Ports ----------------------
        gt3_gthrxn_in                   =>      gt3_gthrxn_in,
        -------------- Receive Ports - RX Byte and Word Alignment Ports ------------
        gt3_rxmcommaalignen_in          =>      '0',--gt3_rxmcommaalignen_in,
        gt3_rxpcommaalignen_in          =>      '1',--gt3_rxpcommaalignen_in,
        --------------------- Receive Ports - RX Equalizer Ports -------------------
        gt3_rxmonitorout_out            =>      open,--gt3_rxmonitorout_out,
        gt3_rxmonitorsel_in             =>      (others=>'0'),--gt3_rxmonitorsel_in,
        --------------- Receive Ports - RX Fabric Output Control Ports -------------
        gt3_rxoutclkfabric_out          =>      open,--gt3_rxoutclkfabric_out,
        ------------- Receive Ports - RX Initialization and Reset Ports ------------
        gt3_gtrxreset_in                =>      gt3_gtrxreset_in,
        ------------------- Receive Ports - RX8B/10B Decoder Ports -----------------
        gt3_rxcharisk_out               =>      gt3_rxcharisk_out,
        ------------------------ Receive Ports -RX AFE Ports -----------------------
        gt3_gthrxp_in                   =>      gt3_gthrxp_in,
        -------------- Receive Ports -RX Initialization and Reset Ports ------------
        gt3_rxresetdone_out             =>      gt3_rxresetdone_out,
        --------------------- TX Initialization and Reset Ports --------------------
        gt3_gttxreset_in                =>      gt3_gttxreset_in,
        gt3_txuserrdy_in                =>      '1',--gt3_txuserrdy_in,
        ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
        gt3_txusrclk_in                 =>      gt3_txusrclk_i,
        gt3_txusrclk2_in                =>      gt3_txusrclk2_i,
        ------------------ Transmit Ports - TX Data Path interface -----------------
        gt3_txdata_in                   =>      gt3_txdata_in,
        ---------------- Transmit Ports - TX Driver and OOB signaling --------------
        gt3_gthtxn_out                  =>      gt3_gthtxn_out,
        gt3_gthtxp_out                  =>      gt3_gthtxp_out,
        ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
        gt3_txoutclk_out                =>      gt3_txoutclk_i,
        gt3_txoutclkfabric_out          =>      open,--gt3_txoutclkfabric_out,
        gt3_txoutclkpcs_out             =>      open,--gt3_txoutclkpcs_out,
        ------------- Transmit Ports - TX Initialization and Reset Ports -----------
        gt3_txresetdone_out             =>      gt3_txresetdone_out,
        gt3_rxoutclk_out                =>      gt3_rxoutclk_i,
        ----------- Transmit Transmit Ports - 8b10b Encoder Control Ports ----------
        gt3_txcharisk_in                =>      gt3_txcharisk_in,



    gt0_qplllock_in => gt0_qplllock_i,
    gt0_qpllrefclklost_in => gt0_qpllrefclklost_i,
    gt0_qpllreset_out => gt0_qpllreset_i,
    gt0_qplloutclk_in => gt0_qplloutclk_i,
    gt0_qplloutrefclk_in => gt0_qplloutrefclk_i
    );




   common0_i: entity work.prbs8b10b_9p6g_common 
  generic map
  (
  -- WRAPPER_SIM_GTRESET_SPEEDUP => '0',--EXAMPLE_SIM_GTRESET_SPEEDUP,
   SIM_QPLLREFCLK_SEL => "001"
  )
 port map
   (
    QPLLREFCLKSEL_IN    => "001",
    GTREFCLK0_IN      => tied_to_ground_i,
    GTREFCLK1_IN      => tied_to_ground_i,--q1_clk1_refclk_i,
    QPLLLOCK_OUT => gt0_qplllock_i,
    GTGREFCLK_IN   => grefclk,
    QPLLLOCKDETCLK_IN => sysclk_in_i,
    QPLLOUTCLK_OUT => gt0_qplloutclk_i,
    QPLLOUTREFCLK_OUT => gt0_qplloutrefclk_i,
    QPLLREFCLKLOST_OUT => gt0_qpllrefclklost_i,    
    QPLLRESET_IN => gt0_qpllreset_t

);

    common_reset_i: entity work.prbs8b10b_9p6g_common_reset 
   generic map 
   (
      STABLE_CLOCK_PERIOD =>25        -- Period of the stable clock driving this state-machine, unit is [ns]
   )
   port map
   (    
      STABLE_CLOCK => sysclk_in_i,             --Stable Clock, either a stable clock from the PCB
     
      SOFT_RESET => soft_reset_tx_in(0),               --User Reset, can be pulled any time
      COMMON_RESET => commonreset_i              --Reset QPLL
   );


end Behavioral;
