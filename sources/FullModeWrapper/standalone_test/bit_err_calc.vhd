--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Kai Chen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: BNL
-- Engineer: Kai Chen
--
-- Create Date: 05/10/2016 12:14:07 AM
-- Design Name:
-- Module Name: bit_err_calc - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library  ieee, UNISIM;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bit_err_calc is
  Port (

dataA : in std_logic_vector(31 downto 0);
dataB : in std_logic_vector(31 downto 0);
error_counter : out std_logic_vector(9 downto 0);
error_clr : in std_logic;
clk : in std_logic;
charflag : in std_logic

);
end bit_err_calc;

architecture Behavioral of bit_err_calc is

signal dataC : std_logic_vector(31 downto 0):=x"00000000";
type error1t is array (7 downto 0) of std_logic_vector(5 downto 0);
signal error1 : error1t;
signal error21, error22, error23, error24, error31, error32, error4:std_logic_vector(5 downto 0);
signal error_f: std_logic_vector(9 downto 0);

begin
gen1: for i in 0 to 7 generate
process(clk)
begin
if clk'event and clk='1' then
    if dataC(4*i+3 downto 4*i) = "0000" then
	    error1(i)<="000000";
	elsif dataC(4*i+3 downto 4*i) = "1111" then
	    error1(i)<="000100";
	elsif ((dataC(4*i+3 downto 4*i) = "0001" or dataC(4*i+3 downto 4*i) = "0010")
	or (dataC(4*i+3 downto 4*i) = "0100" or dataC(4*i+3 downto 4*i) = "1000"))  then
	    error1(i)<="000001";
	elsif ((dataC(4*i+3 downto 4*i) = "1110" or dataC(4*i+3 downto 4*i) = "1101")
	or (dataC(4*i+3 downto 4*i) = "1011" or dataC(4*i+3 downto 4*i) = "0111"))  then
	    error1(i)<="000011";
	else
	    error1(i)<="000010";
	end if;
end if;
end process;
end generate;


process(clk)
begin
if clk'event and clk='1' then
error21 <= error1(0)+error1(1);
error22 <= error1(2)+error1(3);
error23 <= error1(4)+error1(5);
error24 <= error1(6)+error1(7);

error31 <= error21+error22;
error32 <= error23+error24;

error4 <= error31 + error32;
if error_clr = '1' then
    error_f <= "0000000000";
else
    error_f <= error_f + ("0000" & error4);
end if;
error_counter <= error_f;
end if;
end process;

process(clk)
begin
if clk'event and clk='1' then
    if charflag='0' then
        dataC(0)<= dataA(0) xor dataB(0);
        dataC(1)<= dataA(1) xor dataB(1);
		dataC(2)<= dataA(2) xor dataB(2);
		dataC(3)<= dataA(3) xor dataB(3);
        dataC(4)<= dataA(4) xor dataB(4);
        dataC(5)<= dataA(5) xor dataB(5);
		dataC(6)<= dataA(6) xor dataB(6);
		dataC(7)<= dataA(7) xor dataB(7);
		dataC(8)<= dataA(8) xor dataB(8);
		dataC(9)<= dataA(9) xor dataB(9);
        dataC(10)<= dataA(10) xor dataB(10);
        dataC(11)<= dataA(11) xor dataB(11);
		dataC(12)<= dataA(12) xor dataB(12);
		dataC(13)<= dataA(13) xor dataB(13);
        dataC(14)<= dataA(14) xor dataB(14);
        dataC(15)<= dataA(15) xor dataB(15);
		dataC(16)<= dataA(16) xor dataB(16);
		dataC(17)<= dataA(17) xor dataB(17);
		dataC(18)<= dataA(18) xor dataB(18);
		dataC(19)<= dataA(19) xor dataB(19);
        dataC(20)<= dataA(20) xor dataB(20);
        dataC(21)<= dataA(21) xor dataB(21);
		dataC(22)<= dataA(22) xor dataB(22);
		dataC(23)<= dataA(23) xor dataB(23);
        dataC(24)<= dataA(24) xor dataB(24);
        dataC(25)<= dataA(25) xor dataB(25);
		dataC(26)<= dataA(26) xor dataB(26);
		dataC(27)<= dataA(27) xor dataB(27);
		dataC(28)<= dataA(28) xor dataB(28);
		dataC(29)<= dataA(29) xor dataB(29);
        dataC(30)<= dataA(30) xor dataB(30);
        dataC(31)<= dataA(31) xor dataB(31);
	else
	    dataC <= x"00000000";
	end if;
end if;
end process;

end Behavioral;
