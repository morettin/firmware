--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Kai Chen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-- Kai Chen @ BNL
-- July, 2016
-- For 32 bit PRBS31 decoding
-- Support PASUE


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

--***********************************Entity Declaration*******************************

entity gtx_one_PRBS_chknew is
generic
( 
    TX_DATA_WIDTH            : integer := 64
);
port
(
    -- User Interface
    PRBS_DATA_IN      : in  std_logic_vector((TX_DATA_WIDTH-1) downto 0); 
    DATA_OUT       : out std_logic_vector((TX_DATA_WIDTH-1) downto 0); 
    DATA_VALID_IN            : in  std_logic;

    -- System Interface
    USER_CLK                 : in  std_logic;      
    SYSTEM_RESET             : in  std_logic
);


end gtx_one_PRBS_chknew;

architecture RTL of gtx_one_PRBS_chknew is


--***********************************Parameter Declarations********************

    constant DLY : time := 1 ns;

--***************************Internal Register Declarations******************** 

    signal   poly               :  std_logic_vector(30 downto 0);
    signal   scrambler          :  std_logic_vector(30 downto 0);
    signal   scrambled_data_i,dataout ,PRBS_DATA_IN_r  :  std_logic_vector((TX_DATA_WIDTH-1) downto 0);
    signal   tempdata           :  std_logic_vector((TX_DATA_WIDTH-1) downto 0);
signal DATA_VALID_IN_r:std_logic;
--*********************************Main Body of Code***************************
begin


 process( USER_CLK )
    begin
    if(USER_CLK'event and USER_CLK = '1') then
        dataout(0) <= poly(3) xor poly(0) xor PRBS_DATA_IN(0);
        dataout(1) <= poly(4) xor poly(1) xor PRBS_DATA_IN(1);
        dataout(2) <= poly(5) xor poly(2) xor PRBS_DATA_IN(2);
        dataout(3) <= poly(6) xor poly(3) xor PRBS_DATA_IN(3);
        dataout(4) <= poly(7) xor poly(4) xor PRBS_DATA_IN(4);
        dataout(5) <= poly(8) xor poly(5) xor PRBS_DATA_IN(5);                
        dataout(6) <= poly(9) xor poly(6) xor PRBS_DATA_IN(6);
        dataout(7) <= poly(10) xor poly(7) xor PRBS_DATA_IN(7);
        dataout(8) <= poly(11) xor poly(8) xor PRBS_DATA_IN(8);
        dataout(9) <= poly(12) xor poly(9) xor PRBS_DATA_IN(9);
        dataout(10) <= poly(13) xor poly(10) xor PRBS_DATA_IN(10);
        dataout(11) <= poly(14) xor poly(11) xor PRBS_DATA_IN(11);     
        dataout(12) <= poly(15) xor poly(12) xor PRBS_DATA_IN(12);
        dataout(13) <= poly(16) xor poly(13) xor PRBS_DATA_IN(13);
        dataout(14) <= poly(17) xor poly(14) xor PRBS_DATA_IN(14);
        dataout(15) <= poly(18) xor poly(15) xor PRBS_DATA_IN(15);                
        dataout(16) <= poly(19) xor poly(16) xor PRBS_DATA_IN(16);
        dataout(17) <= poly(20) xor poly(17) xor PRBS_DATA_IN(17);
        dataout(18) <= poly(21) xor poly(18) xor PRBS_DATA_IN(18);
        dataout(19) <= poly(22) xor poly(19) xor PRBS_DATA_IN(19);
        dataout(20) <= poly(23) xor poly(20) xor PRBS_DATA_IN(20);
        dataout(21) <= poly(24) xor poly(21) xor PRBS_DATA_IN(21);  
        dataout(22) <= poly(25) xor poly(22) xor PRBS_DATA_IN(22);
        dataout(23) <= poly(26) xor poly(23) xor PRBS_DATA_IN(23);
        dataout(24) <= poly(27) xor poly(24) xor PRBS_DATA_IN(24);
        dataout(25) <= poly(28) xor poly(25) xor PRBS_DATA_IN(25);                
        dataout(26) <= poly(29) xor poly(26) xor PRBS_DATA_IN(26);
        dataout(27) <= poly(30) xor poly(27) xor PRBS_DATA_IN(27);
        dataout(28) <= PRBS_DATA_IN(0) xor PRBS_DATA_IN(28) xor poly(28);
        dataout(29) <= PRBS_DATA_IN(1) xor PRBS_DATA_IN(29) xor poly(29);
        dataout(30) <= PRBS_DATA_IN(2) xor PRBS_DATA_IN(30) xor poly(30);
        dataout(31) <= PRBS_DATA_IN(3) xor PRBS_DATA_IN(31) xor PRBS_DATA_IN(0);              
    
    
        PRBS_DATA_IN_r <= PRBS_DATA_IN;
        if (SYSTEM_RESET = '1') then
           -- poly          <= "000" & x"0000000";
            DATA_OUT <= (others => '0');
        elsif (DATA_VALID_IN_r = '1') then 
           -- poly          <=   PRBS_DATA_IN(31 downto 1);
            DATA_OUT <=   dataout;
        else
           -- poly          <=   POLY;
            DATA_OUT <=   PRBS_DATA_IN_r;--x"123456BC";    
        end if;
         if (SYSTEM_RESET = '1') then
               poly          <= "000" & x"0000000";
              -- DATA_OUT <= (others => '0');
           elsif (DATA_VALID_IN = '1') then 
               poly          <=   PRBS_DATA_IN(31 downto 1);
               --DATA_OUT <=   dataout;
           else
               poly          <=   POLY;
              -- DATA_OUT <=   x"123456BC";    
           end if;
        DATA_VALID_IN_r <= DATA_VALID_IN;
    end if;
    end process;

         
end RTL;

