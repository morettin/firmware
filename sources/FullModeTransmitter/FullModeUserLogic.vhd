--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    09/10/2016 
--! Module Name:    FullModeUserLogic
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library work, ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--! Full Mode user logic example module
entity FullModeUserLogic is
port (
    clk         : in  std_logic;
    rst         : in  std_logic;
    --
    busy        : out std_logic;
    link_rdy_i  : in  std_logic;
    fifo_rclk   : in  std_logic;
    fifo_re     : in  std_logic;
    fifo_dout   : out std_logic_vector(31 downto 0);
    fifo_dtype  : out std_logic_vector(1 downto 0);
    fifo_empty  : out std_logic
    );
end FullModeUserLogic;


architecture Behavioral of FullModeUserLogic is

component FMuserDataRAM
port (
    clka    : in std_logic;
    rsta    : in std_logic;
    wea     : in std_logic_vector(0 downto 0);
    addra   : in std_logic_vector(9 downto 0);
    dina    : in std_logic_vector(35 downto 0);
    douta   : out std_logic_vector(35 downto 0)
  );
end component;
--------------------------
component FMuserFIFO
port (
    wr_clk  : in std_logic;
    wr_rst  : in std_logic;
    rd_clk  : in std_logic;
    rd_rst  : in std_logic;
    din     : in std_logic_vector(35 downto 0);
    wr_en   : in std_logic;
    rd_en   : in std_logic;
    dout    : out std_logic_vector(35 downto 0);
    full    : out std_logic;
    empty   : out std_logic
  );
end component;
--------------------------

signal reset_state : std_logic := '1'; 
signal fifo_we,fifo_we1 : std_logic := '0'; 
signal data_rd_addr : std_logic_vector(9 downto 0) := (others=>'1');
signal rst_fall,count_ena,fifo_full : std_logic; 
signal user_data_out,user_data_in,fifo_dout36bit : std_logic_vector(35 downto 0); 


begin

---------------------------------------------------------------------------------------
-- reset state
---------------------------------------------------------------------------------------
rst_fall_pulse: entity work.pulse_fall_pw01 port map(clk, rst, rst_fall);
--
rst_latch: process(clk,rst)
begin
    if rst = '1' then
        reset_state <= '1';
    elsif rising_edge (clk) then
        if rst_fall = '1' then
            reset_state <= '0';
        end if;
	end if;
end process;


----------
user_data_source: FMuserDataRAM
port map (
    clka    => clk,
    rsta    => reset_state,
    wea     => "0",
    addra   => data_rd_addr,
    dina    => user_data_in,
    douta   => user_data_out
    );

--
data_addr: process(clk)
begin
    if rising_edge (clk) then
        if reset_state = '1' or link_rdy_i = '0' then
            data_rd_addr <= (others=>'1');
        elsif count_ena = '1' then
            data_rd_addr <= data_rd_addr + 1; -- flips over
        end if;
	end if;
end process;



--
we: process(clk)
begin
    if rising_edge (clk) then
        if reset_state = '1'  or link_rdy_i = '0' then
            fifo_we     <= '0';
            fifo_we1    <= '0';
        else
            fifo_we1    <= count_ena;
            fifo_we     <= fifo_we1;
        end if;
	end if;
end process;


----------
user_output_FIFO: FMuserFIFO
port map (
    wr_clk  => clk,
    wr_rst  => reset_state,
    rd_clk  => fifo_rclk,
    rd_rst  => reset_state,
    din     => user_data_out,
    wr_en   => fifo_we,
    rd_en   => fifo_re,
    dout    => fifo_dout36bit,
    full    => fifo_full,
    empty   => fifo_empty
    );

count_ena   <= (not fifo_full) and (not reset_state) and link_rdy_i;
fifo_dout   <= fifo_dout36bit(31 downto 0);
fifo_dtype  <= fifo_dout36bit(33 downto 32);


end Behavioral;

