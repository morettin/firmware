--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               RHabraken
--!               Mesfin Gebyehu
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.



library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all; -- @suppress "Deprecated package"
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
    use work.centralRouter_package.all;
    use work.FMTransceiverPackage.all;
    use work.FELIX_gbt_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
library XPM;
    use XPM.vcomponents.all;

entity FullModeTransceiver is
    generic(
        NUM_LINKS                           : integer := 12
    );
    port(
        GTREFCLK0_0_N_IN                    : in   std_logic;
        GTREFCLK0_0_P_IN                    : in   std_logic;
        GTREFCLK0_1_N_IN                    : in   std_logic;
        GTREFCLK0_1_P_IN                    : in   std_logic;
        gtrxp_in                            : in   std_logic_vector(NUM_LINKS-1 downto 0);
        gtrxn_in                            : in   std_logic_vector(NUM_LINKS-1 downto 0);
        gttxn_out                           : out  std_logic_vector(NUM_LINKS-1 downto 0);
        gttxp_out                           : out  std_logic_vector(NUM_LINKS-1 downto 0);

        TXUSRCLK_OUT                        : out  std_logic_vector(NUM_LINKS-1 downto 0);
        RXUSRCLK_OUT                        : out  std_logic_vector(NUM_LINKS-1 downto 0);

        ------------------ Transmit Ports - TX Data Path interface -----------------
        txdata_in                           : in   array_32b(0 to NUM_LINKS-1);
        txcharisk_in                        : in   array_4b(0 to NUM_LINKS-1);
        TTCout                              : out TTC_data_array_type(0 to NUM_LINKS-1);
        -----
        rst_hw                              : in   std_logic;
        clk40_out : out std_logic_vector(NUM_LINKS-1 downto 0);
        drpclk_in : in std_logic;

        --For received GBT data
        RX_120b_out                 : out txrx120b_type(0 to NUM_LINKS-1);
        --For received LTI data
        LTI_RECEIVER_ACTIVE_out : out std_logic_vector(NUM_LINKS-1 downto 0);

        FRAME_LOCKED_O              : out std_logic_vector(NUM_LINKS-1 downto 0);
        register_map_control        : in register_map_control_type;
        register_map_link_monitor    : out register_map_link_monitor_type

    );

end FullModeTransceiver;

architecture RTL of FullModeTransceiver is
    COMPONENT fullmodetransceiver_core
        PORT (
            gtwiz_userclk_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_tx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userclk_rx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
            gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            drpclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxn_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxp_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtrefclk0_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rx8b10ben_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcommadeten_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxmcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxslide_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxusrclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxusrclk2_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            tx8b10ben_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            txctrl0_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            txctrl1_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            txctrl2_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            gthtxn_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthtxp_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtpowergood_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxbyteisaligned_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxbyterealign_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcdrlock_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcommadet_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxctrl0_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            rxctrl1_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            rxctrl2_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            rxctrl3_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            rxoutclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txprgdivresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;


    constant GBT_NUM : integer := NUM_LINKS;

    signal RxSlide_c      : STD_LOGIC_VECTOR(NUM_LINKS-1 downto 0);
    signal RxSlide_i      : std_logic_vector(NUM_LINKS-1 downto 0);
    signal gttx_reset     : std_logic_vector(NUM_LINKS-1 downto 0);

    signal gtrx_reset     : std_logic_vector(NUM_LINKS-1 downto 0);
    signal soft_reset     : std_logic_vector(NUM_LINKS-1 downto 0);
    signal cpll_reset     : std_logic_vector(NUM_LINKS-1 downto 0);
    signal txresetdone    : std_logic_vector(NUM_LINKS-1 downto 0);

    signal rxresetdone    : std_logic_vector(NUM_LINKS-1 downto 0);
    signal rxcdrlock,RxCdrLock_int      : std_logic_vector(NUM_LINKS-1 downto 0);



    signal RX_RESET       : std_logic_vector(NUM_LINKS-1 downto 0);
    signal RX_RESET_i     : std_logic_vector(NUM_LINKS-1 downto 0);
    signal RX_FLAG_Oi     : std_logic_vector(NUM_LINKS-1 downto 0);
    signal outsel_i       : std_logic_vector(NUM_LINKS-1 downto 0);
    signal outsel_ii      : std_logic_vector(NUM_LINKS-1 downto 0);
    signal outsel_o       : std_logic_vector(NUM_LINKS-1 downto 0);
    signal RX_120b_out_i  : txrx120b_type(0 to (GBT_NUM-1));
    signal RX_120b_out_ii : txrx120b_type(0 to (GBT_NUM-1));

    signal rx_is_header   : std_logic_vector(NUM_LINKS-1 downto 0);
    signal alignment_done : std_logic_vector(NUM_LINKS-1 downto 0);
    signal RX_HEADER_FOUND: std_logic_vector(NUM_LINKS-1 downto 0);

    signal RxSlide        : std_logic_vector(NUM_LINKS-1 downto 0);


    signal RX_DATA_20b    : array_20b(0 to GBT_NUM-1) := (others => (x"00000"));

    signal alignment_chk_rst_c    : std_logic_vector(NUM_LINKS-1 downto 0);
    signal alignment_chk_rst_c1   : std_logic_vector(NUM_LINKS-1 downto 0);
    signal alignment_chk_rst      : std_logic_vector(NUM_LINKS-1 downto 0);
    signal alignment_chk_rst_f    : std_logic_vector(NUM_LINKS-1 downto 0);

    signal alignment_chk_rst_i    : std_logic;

    signal DESMUX_USE_SW          : std_logic;

    SIGNAL DATA_RXFORMAT          : std_logic_vector(2*NUM_LINKS-1 downto 0);
    signal DATA_RXFORMAT_i        : std_logic_vector(2*NUM_LINKS-1 downto 0);

    signal General_ctrl           : std_logic_vector(63 downto 0);

    signal TXPMARESETDONE         : std_logic_vector(NUM_LINKS-1 downto 0);
    signal RXPMARESETDONE         : std_logic_vector(NUM_LINKS-1 downto 0);
    signal alignment_done_f       : std_logic_vector(NUM_LINKS-1 downto 0);
    signal soft_reset_f           : std_logic_vector(NUM_LINKS-1 downto 0);
    signal fifo_empty             : std_logic_vector(NUM_LINKS-1 downto 0);
    signal userclk_rx_reset_in    : std_logic_vector(NUM_LINKS-1 downto 0);
    SIGNAL Channel_disable        : std_logic_vector(NUM_LINKS-1 downto 0);

    signal GTH_RefClk             : std_logic_vector(NUM_LINKS-1 downto 0);


    signal CXP1_GTH_RefClk        : std_logic;
    signal CXP2_GTH_RefClk        : std_logic;



    signal fifo_rst               : std_logic_vector(NUM_LINKS-1 downto 0);
    signal fifo_rden              : std_logic_vector(NUM_LINKS-1 downto 0);
    signal error_orig             : std_logic_vector(NUM_LINKS-1 downto 0);
    signal error_f                : std_logic_vector(NUM_LINKS-1 downto 0);
    signal FSM_RST                : std_logic_vector(NUM_LINKS-1 downto 0);
    signal auto_gth_rxrst         : std_logic_vector(NUM_LINKS-1 downto 0);
    signal auto_gbt_rxrst         : std_logic_vector(NUM_LINKS-1 downto 0);
    signal gtrx_reset_i           : std_logic_vector(NUM_LINKS-1 downto 0);


    signal BITSLIP_MANUAL_r       : std_logic_vector(NUM_LINKS-1 downto 0);
    signal BITSLIP_MANUAL_2r      : std_logic_vector(NUM_LINKS-1 downto 0);


    signal userclk_rx_active_out_p: std_logic_vector(GBT_NUM-1 downto 0);
    signal userclk_rx_active_in   : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxusrclk, rxoutclk     : std_logic_vector(GBT_NUM-1 downto 0);
    signal txusrclk               : std_logic_vector(GBT_NUM-1 downto 0);
    signal rx8b10ben_in : STD_LOGIC_VECTOR(GBT_NUM-1  DOWNTO 0);
    signal clk40: std_logic_vector(GBT_NUM-1 downto 0);
    signal reset_RX_40M_FrameClk_BUFG_GBT : std_logic_vector(GBT_NUM-1 downto 0);
    signal reset_RX_40M_FrameClk_BUFG_LTI : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_DATA_32b    : array_32b(0 to NUM_LINKS-1) := (others => (x"0000_0000"));
    signal RX_Charisk_4b  : array_4b(0 to NUM_LINKS-1) := (others => ("0000"));
    signal LTI_Receiver_aligned : std_logic_vector(0 to GBT_NUM-1);
    signal LTI_CRC_Err : std_logic_vector(0 to GBT_NUM-1);

begin
    clk40_out <= clk40;

    FRAME_LOCKED_O <= alignment_done_f(GBT_NUM-1 downto 0);
    TXUSRCLK_OUT <= txusrclk;
    RXUSRCLK_OUT <= rxusrclk;

    g_sync: for i in 0 to GBT_NUM-1 generate
        rx8b10ben_in_sync: xpm_cdc_single generic map(
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0
            )
            port map(
                src_clk => '0',
                src_in => register_map_control.LINK_FULLMODE_LTI(i),
                dest_clk => rxusrclk(i),
                dest_out => rx8b10ben_in(i)
            );
        LTI_RECEIVER_ACTIVE_out(i) <= rx8b10ben_in(i);
    end generate;
    --bank 126, 127, 128 use clk from bank 127
    ibufds_instq2_clk0 : IBUFDS_GTE3 -- @suppress "Generic map uses default values. Missing optional actuals: REFCLK_EN_TX_PATH, REFCLK_HROW_CK_SEL, REFCLK_ICNTL_RX"
        port map(
            O               =>   CXP1_GTH_RefClk,
            ODIV2           =>    open,
            CEB             =>   '0',
            I             => GTREFCLK0_0_P_IN, -- 1-bit input: Refer to Transceiver User Guide
            IB             => GTREFCLK0_0_N_IN -- 1-bit input: Refer to Transceiver User Guide
        );

    --bank 131, 132, 133 use clk from bank 132
    ibufds_instq8_clk0 : IBUFDS_GTE3 -- @suppress "Generic map uses default values. Missing optional actuals: REFCLK_EN_TX_PATH, REFCLK_HROW_CK_SEL, REFCLK_ICNTL_RX"
        port map(
            O               =>     CXP2_GTH_RefClk,
            ODIV2           =>    open,
            CEB             =>     '0',
            I             => GTREFCLK0_1_P_IN, -- 1-bit input: Refer to Transceiver User Guide
            IB             => GTREFCLK0_1_N_IN -- 1-bit input: Refer to Transceiver User Guide
        );
    g_REFCLK0: for i in 0 to 11 generate --(bank 128, 127, 126)
        g_NUM_LINKS: if i < NUM_LINKS generate
            GTH_RefClk(i)         <= CXP1_GTH_RefClk;
        end generate;
    end generate;
    g_REFCLK1: for i in 12 to 23 generate --(bank 133, 132, 131)
        g_NUM_LINKS: if i < NUM_LINKS generate
            GTH_RefClk(i)         <= CXP2_GTH_RefClk;
        end generate;
    end generate;



    Channel_disable(NUM_LINKS-1 downto 0)          <= register_map_control.GBT_CHANNEL_DISABLE(NUM_LINKS-1 downto 0);
    General_ctrl                          <= register_map_control.GBT_GENERAL_CTRL;

    gttx_reset(NUM_LINKS-1 downto 0)           <= register_map_control.GBT_GTTX_RESET(NUM_LINKS-1 downto 0);
    gtrx_reset(NUM_LINKS-1 downto 0)           <= register_map_control.GBT_GTRX_RESET(NUM_LINKS-1 downto 0);
    soft_reset(NUM_LINKS-1 downto 0)           <= register_map_control.GBT_SOFT_RESET(NUM_LINKS-1 downto 0);
    cpll_reset(NUM_LINKS-1 downto 0)           <= register_map_control.GBT_PLL_RESET.CPLL_RESET(NUM_LINKS-1 downto 0);

    DATA_RXFORMAT(NUM_LINKS*2-1 downto 0)        <= register_map_control.GBT_DATA_RXFORMAT1(NUM_LINKS*2-1 downto 0);


    RX_RESET(NUM_LINKS-1 downto 0)             <= register_map_control.GBT_RX_RESET(NUM_LINKS-1 downto 0);
    outsel_i(NUM_LINKS-1 downto 0)             <= register_map_control.GBT_OUTMUX_SEL(NUM_LINKS-1 downto 0);

    register_map_link_monitor.GBT_VERSION.DATE             <=  GBT_VERSION(63 downto 48);
    register_map_link_monitor.GBT_VERSION.GBT_VERSION(35 downto 32)      <=  GBT_VERSION(23 downto 20);
    register_map_link_monitor.GBT_VERSION.GTH_IP_VERSION(19 downto 16)   <=  GBT_VERSION(19 downto 16);
    register_map_link_monitor.GBT_VERSION.RESERVED         <=  GBT_VERSION(15 downto 3);
    register_map_link_monitor.GBT_VERSION.GTHREFCLK_SEL    <=  (others => '0');
    register_map_link_monitor.GBT_VERSION.RX_CLK_SEL       <=  GBT_VERSION(1 downto 1);
    register_map_link_monitor.GBT_VERSION.PLL_SEL          <=  GBT_VERSION(0 downto 0);

    --

    register_map_link_monitor.GBT_TXRESET_DONE(NUM_LINKS-1 downto 0)        <= txresetdone(NUM_LINKS-1 downto 0);
    register_map_link_monitor.GBT_RXRESET_DONE(NUM_LINKS-1 downto 0)        <= rxresetdone(NUM_LINKS-1 downto 0);
    register_map_link_monitor.GBT_TXFSMRESET_DONE(NUM_LINKS-1 downto 0)     <= TXPMARESETDONE(NUM_LINKS-1 downto 0);
    register_map_link_monitor.GBT_RXFSMRESET_DONE(NUM_LINKS-1 downto 0)     <= RXPMARESETDONE(NUM_LINKS-1 downto 0);
    register_map_link_monitor.GBT_PLL_LOCK.CPLL_LOCK(NUM_LINKS-1 downto 0)  <= rxresetdone(NUM_LINKS-1 downto 0);--cplllock(47 downto 0);
    register_map_link_monitor.GBT_PLL_LOCK.QPLL_LOCK <= (others => '0');
    register_map_link_monitor.GBT_RXCDR_LOCK(NUM_LINKS-1 downto 0)          <= rxcdrlock(NUM_LINKS-1 downto 0);

    register_map_link_monitor.GBT_RX_IS_HEADER(NUM_LINKS-1 downto 0)        <= rx_is_header(NUM_LINKS-1 downto 0);
    register_map_link_monitor.GBT_RX_HEADER_FOUND(NUM_LINKS-1 downto 0)     <= RX_HEADER_FOUND(NUM_LINKS-1 downto 0);

    register_map_link_monitor.GBT_ALIGNMENT_DONE(NUM_LINKS-1 downto 0)      <= alignment_done_f(NUM_LINKS-1 downto 0);



    register_map_link_monitor.GBT_OUT_MUX_STATUS(NUM_LINKS-1 downto 0)    <= outsel_o(NUM_LINKS-1 downto 0);
    register_map_link_monitor.GBT_ERROR(NUM_LINKS-1 downto 0)             <= error_f(NUM_LINKS-1 downto 0);



    ----------------------------------------
    ------ REGISTERS MAPPING
    ----------------------------------------
    alignment_chk_rst_i           <= General_ctrl(0);


    DESMUX_USE_SW                 <= register_map_control.GBT_MODE_CTRL.DESMUX_USE_SW(0);


    datamod_gen1 : if DYNAMIC_DATA_MODE_EN='1' generate
        DATA_RXFORMAT_i <= DATA_RXFORMAT;
    end generate;

    datamod_gen2 : if DYNAMIC_DATA_MODE_EN='0' generate
        DATA_RXFORMAT_i <= GBT_DATA_RXFORMAT_PACKAGE(2*NUM_LINKS-1 downto 0);
    end generate;



    rxalign_auto : for i in GBT_NUM-1 downto 0 generate
        signal pulse_cnt              : std_logic_vector(29 downto 0) := (others => '0');
        signal pulse_lg               : std_logic;
        signal alignment_done_chk_cnt : std_logic_vector(12 downto 0) := (others => '0');
        signal alignment_done_a       : std_logic;
    begin

        process(clk40(i))
        begin
            if rising_edge(clk40(i)) then
                pulse_lg <= pulse_cnt(20);
                if pulse_cnt(20)='1' then
                    pulse_cnt <=(others=>'0');
                else
                    pulse_cnt <= pulse_cnt+'1';
                end if;
            end if;
        end process;

        process(clk40(i))
        begin
            if rising_edge(clk40(i)) then
                alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';
            end if;
        end process;

        process(clk40(i))
        begin
            if rising_edge(clk40(i)) then
                if register_map_control.LINK_FULLMODE_LTI(i) = '0' then
                    if alignment_done_chk_cnt="0000000000000" then
                        alignment_done_a <= rxcdrlock(i) and alignment_done(i);
                    else
                        alignment_done_a <= rxcdrlock(i) and alignment_done(i) and alignment_done_a;
                    end if;
                    if alignment_done_chk_cnt="0000000000000" then
                        alignment_done_f(i) <=  rxcdrlock(i) and alignment_done_a;
                    end if;
                    error_f(i) <= error_orig(i) and alignment_done_f(i);

                else
                    alignment_done_f(i) <= LTI_Receiver_aligned(i);
                    error_f(i) <= LTI_CRC_Err(i) and LTI_Receiver_aligned(i);
                end if;
            end if;
        end process;

        RX_120b_out(i) <= RX_120b_out_ii(i) when alignment_done_f(i)='1'
                            else (others =>'0');

        auto_rxrst : entity work.FELIX_GBT_RX_AUTO_RST
            port map(
                ext_trig_realign => open,--ext_trig_realign(i),
                FSM_CLK => clk40(i),
                GBT_LOCK => alignment_done_f(i), --alignment_done(i),
                pulse_lg => pulse_lg,
                GTHRXRESET_DONE => rxresetdone(i), -- and RxFsmResetDone(i),
                AUTO_GTH_RXRST => auto_gth_rxrst(i),
                alignment_chk_rst => alignment_chk_rst_c1(i),
                AUTO_GBT_RXRST => auto_gbt_rxrst(i)
            );

        rafsm : entity work.FELIX_GBT_RXSLIDE_FSM
            port map(
                alignment_chk_rst => alignment_chk_rst_c(i),
                --ext_trig_realign => ext_trig_realign(i),
                FSM_RST => FSM_RST(i),
                FSM_CLK => clk40(i),
                GBT_LOCK => alignment_done(i),
                RxSlide => RxSlide_c(i)
            );

        FSM_RST(i)          <= RX_RESET(i);-- or RX_ALIGN_SW;

        RX_RESET_i(i)       <= --RX_RESET(i) when RX_ALIGN_SW='1' else
                               (RX_RESET(i) or auto_gbt_rxrst(i));
        alignment_chk_rst(i)        <= --alignment_chk_rst_i when RX_ALIGN_SW='1' else
                                       (alignment_chk_rst_i or alignment_chk_rst_c(i) or alignment_chk_rst_c1(i));
        RxSlide_i(i)             <= RxSlide_c(i) and rxcdrlock(i);

    end generate;

    outsel_ii             <= outsel_o when DESMUX_USE_SW = '0' else
                             outsel_i;

    ltireceiver0: entity work.Frontend_TTC_LTI_Receiver generic map(
            GBT_NUM => GBT_NUM
        )
        port map(
            TTCout => TTCout,
            clk40_in => clk40,
            LTI_CRC_Err => LTI_CRC_Err,
            LTI_Receiver_aligned => LTI_Receiver_aligned,
            LTI_RX_Data_Transceiver => RX_DATA_32b,
            LTI_RX_RX_CharIsK => RX_Charisk_4b,
            LTI_RXUSRCLK_in => rxusrclk,
            reset_RX_40M_FrameClk_BUFG => reset_RX_40M_FrameClk_BUFG_LTI
        );

    gbtRxTx : for i in GBT_NUM-1 downto 0 generate
        signal fifo_rst_RX_WORD_CLK: std_logic;
    begin
        process(rxusrclk(i))
        begin
            if rxusrclk(i)'event and rxusrclk(i)='1' then
                BITSLIP_MANUAL_r(i)     <= RxSlide_i(i);
                BITSLIP_MANUAL_2r(i)    <= BITSLIP_MANUAL_r(i);
                --BITSLIP_MANUAL_3r(i)    <= BITSLIP_MANUAL_2r(i);
                RxSlide(i)              <= (BITSLIP_MANUAL_r(i) and (not BITSLIP_MANUAL_2r(i))) and (not rx8b10ben_in(i));
            end if;
        end process;


        alignment_chk_rst_f(i) <= alignment_chk_rst(i);

        gbtTxRx_inst: entity work.FELIX_gbt_wrapper_no_gth
            port map(
                alignment_chk_rst => alignment_chk_rst_f(i),
                alignment_done_O => alignment_done(i),
                outsel_i => outsel_ii(i),
                outsel_o => outsel_o(i),
                error_o => error_orig(i),
                --OddEven => '0', --OddEven_i(i),
                --TopBot => '0', --TopBot_i(i),
                --data_sel => (others => '0'),
                RX_FLAG => RX_FLAG_Oi(i), --RX_FLAG_O(i),
                RX_LATOPT_DES => '1', --RX_OPT(i),
                Rx_Data_Format => DATA_RXFORMAT_i(2*i+1 downto 2*i),
                RX_RESET_I => RX_RESET_i(i),
                RX_FRAME_CLK_I => clk40(i),
                RX_HEADER_FOUND => RX_HEADER_FOUND(i),
                RX_WORD_IS_HEADER_O => rx_is_header(i),
                RX_WORDCLK_I => rxusrclk(i),
                --RX_ISDATA_FLAG_O => rx_is_data(i),
                RX_DATA_20b_I => RX_DATA_20b(i),
                RX_DATA_120b_O => RX_120b_out_i(i),
                des_rxusrclk => rxusrclk(i),

                reset_RX_40M_FrameClk_BUFG => reset_RX_40M_FrameClk_BUFG_GBT(i)
            );


        fifo_rst(i) <= rst_hw or (not alignment_done_f(i)) or RX_RESET_i(i) or General_ctrl(4);
        fifo_rden(i) <= not fifo_empty(i);

        sync_fifo_rst : xpm_cdc_sync_rst
            generic map (
                DEST_SYNC_FF => 2,
                INIT => 1,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0
            )
            port map (
                src_rst => fifo_rst(i),
                dest_clk => rxusrclk(i),
                dest_rst => fifo_rst_RX_WORD_CLK -- 1-bit output: src_rst synchronized to the destination clock domain. This output

            );

        fifo_inst: xpm_fifo_async
            generic map(
                FIFO_MEMORY_TYPE => "block",
                FIFO_WRITE_DEPTH => 32,
                CASCADE_HEIGHT => 0,
                RELATED_CLOCKS => 0,
                WRITE_DATA_WIDTH => 120,
                READ_MODE => "std",
                FIFO_READ_LATENCY => 1,
                FULL_RESET_VALUE => 0,
                USE_ADV_FEATURES => "0200", --only programmable empty
                READ_DATA_WIDTH => 120,
                CDC_SYNC_STAGES => 2,
                WR_DATA_COUNT_WIDTH => 5,
                PROG_FULL_THRESH => 28,
                RD_DATA_COUNT_WIDTH => 5,
                PROG_EMPTY_THRESH => 5,
                DOUT_RESET_VALUE => "0",
                ECC_MODE => "no_ecc",
                SIM_ASSERT_CHK => 0,
                WAKEUP_TIME => 0
            )
            port map(
                sleep => '0',
                rst => fifo_rst_RX_WORD_CLK, --rst_hw,
                wr_clk => rxusrclk(i),
                wr_en => RX_FLAG_Oi(i),
                din => RX_120b_out_i(i),
                full => open,
                prog_full => open,
                wr_data_count => open,
                overflow => open,
                wr_rst_busy => open,
                almost_full => open,
                wr_ack => open,
                rd_clk => clk40(i), --FIFO_RD_CLK(i),
                rd_en => fifo_rden(i), --not fifo_empty(i),--'1',--FIFO_RD_EN(i),
                dout => RX_120b_out_ii(i),
                empty => open,
                prog_empty => fifo_empty(i), --FIFO_EMPTY(i)
                rd_data_count => open,
                underflow => open,
                rd_rst_busy => open,
                almost_empty => open,
                data_valid => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr => open,
                dbiterr => open
            );

    end generate;





    -------------------------------
    ------ GTH TOP WRAPPER
    -------------------------------

    clk_generate : for i in GBT_NUM-1 downto 0 generate
        signal bufg_clr: std_logic;
    begin

        GTRXOUTCLK_BUFG: bufg_gt -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
            port map(
                o => rxusrclk(i),
                ce => '1',
                cemask => '0',
                clr => bufg_clr, --userclk_tx_reset_in,--'0',
                clrmask => '1',
                div => "000",
                i => rxoutclk(i)
            );
        bufg_clr <= reset_RX_40M_FrameClk_BUFG_GBT(i) when rx8b10ben_in(i) = '0' else reset_RX_40M_FrameClk_BUFG_LTI(i);
        GTRXOUTCLK_BUFG_DIV6: bufg_gt -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
            port map(
                o => clk40(i),
                ce => '1',
                cemask => '0',
                clr => bufg_clr, --userclk_tx_reset_in,--'0',
                clrmask => '0',
                div => "101",
                i => rxoutclk(i)
            );

    end generate;


    --drpclk(0) <= drpclk_in;

    --process(drpclk_in)
    --begin
    --    if drpclk_in'event and drpclk_in='1' then
    --        cdr_cnt <=cdr_cnt+'1';
    --    end if;
    --end process;




    GTH_inst : for i in GBT_NUM-1 downto 0 generate
        signal rxctrl0_out : STD_LOGIC_VECTOR(15 DOWNTO 0);
        signal rxctrl1_out : STD_LOGIC_VECTOR(15 DOWNTO 0);
        --signal rxctrl2_out : STD_LOGIC_VECTOR(7 DOWNTO 0);
        --signal rxctrl3_out : STD_LOGIC_VECTOR(7 DOWNTO 0);
        signal gtwiz_userdata_rx_out : STD_LOGIC_VECTOR(31 DOWNTO 0);
        signal drpclk_i: std_logic_vector(0 downto 0);
    begin
        drpclk_i(0) <= drpclk_in;

        rxdata_sel_proc: process(rx8b10ben_in(i), gtwiz_userdata_rx_out, rxctrl0_out, rxctrl1_out ) is
            variable RX_DATA_40b: std_logic_vector(39 downto 0);
        begin
            if rx8b10ben_in(i) = '1' then
                RX_DATA_20b(i) <= (others => '0');
                RX_DATA_32b(i) <= gtwiz_userdata_rx_out(31 downto 0);
                RX_Charisk_4b(i) <= rxctrl0_out(3 downto 0);
            else
                RX_DATA_32b(i) <= (others => '0');
                RX_Charisk_4b(i) <= (others => '0');
                RX_DATA_40b := rxctrl1_out(3) & rxctrl0_out(3) & gtwiz_userdata_rx_out(31 downto 24) &
                               rxctrl1_out(2) & rxctrl0_out(2) & gtwiz_userdata_rx_out(23 downto 16) &
                               rxctrl1_out(1) & rxctrl0_out(1) & gtwiz_userdata_rx_out(15 downto  8) &
                               rxctrl1_out(0) & rxctrl0_out(0) & gtwiz_userdata_rx_out( 7 downto  0);
                for b in 0 to 19 loop
                    RX_DATA_20b(i)(b) <= RX_DATA_40b(b*2);
                end loop;
            end if;
        end process;

        process(userclk_rx_reset_in(i), rxusrclk(i))
        begin
            if userclk_rx_reset_in(i) = '1' then
                userclk_rx_active_in(i)          <= '0';
                userclk_rx_active_out_p(i)        <= '0';
            elsif rising_edge(rxusrclk(i)) then
                userclk_rx_active_out_p(i)        <= '1';
                userclk_rx_active_in(i)           <= userclk_rx_active_out_p(i);
            end if;
        end process;

        GTH_TOP_INST : fullmodetransceiver_core
            PORT MAP (
                gtwiz_userclk_tx_reset_in => (others => '0'),
                gtwiz_userclk_tx_srcclk_out => open,
                gtwiz_userclk_tx_usrclk_out => txusrclk(i downto i),
                gtwiz_userclk_tx_usrclk2_out => open,
                gtwiz_userclk_tx_active_out => open,
                gtwiz_userclk_rx_active_in =>  userclk_rx_active_in(i downto i),
                gtwiz_buffbypass_tx_reset_in => "0",
                gtwiz_buffbypass_tx_start_user_in => "0",
                gtwiz_buffbypass_tx_done_out => open,
                gtwiz_buffbypass_tx_error_out => open,
                gtwiz_reset_clk_freerun_in => drpclk_i,
                gtwiz_reset_all_in => soft_reset_f(i downto i),
                gtwiz_reset_tx_pll_and_datapath_in => cpll_reset(i downto i),
                gtwiz_reset_tx_datapath_in => gttx_reset(i downto i),
                gtwiz_reset_rx_pll_and_datapath_in => cpll_reset(i downto i),
                gtwiz_reset_rx_datapath_in => gtrx_reset_i(i downto i),
                gtwiz_reset_rx_cdr_stable_out => RxCdrLock_int(i downto i),
                gtwiz_reset_tx_done_out => txresetdone(i downto i),
                gtwiz_reset_rx_done_out => rxresetdone(i downto i),
                gtwiz_userdata_tx_in => txdata_in(i),
                rx8b10ben_in => rx8b10ben_in(i downto i),
                gtwiz_userdata_rx_out => gtwiz_userdata_rx_out,
                drpclk_in => drpclk_i,
                gthrxn_in => gtrxn_in(i downto i),
                gthrxp_in => gtrxp_in(i downto i),
                gtrefclk0_in => GTH_RefClk(i downto i),
                rxslide_in => RxSlide(i downto i),
                rxusrclk_in => rxusrclk(i downto i),
                rxusrclk2_in => rxusrclk(i downto i),
                tx8b10ben_in => "1",
                txctrl0_in => (others => '0'),
                txctrl1_in => (others => '0'),
                txctrl2_in(7 downto 4) => "0000",
                txctrl2_in(3 downto 0) => txcharisk_in(i),
                gthtxn_out => gttxn_out(i downto i),
                gthtxp_out => gttxp_out(i downto i),
                gtpowergood_out => open,
                rxcdrlock_out => open,
                rxoutclk_out => rxoutclk(i downto i),
                rxpmaresetdone_out => RXPMARESETDONE(i downto i),
                txpmaresetdone_out => TXPMARESETDONE(i downto i),
                txprgdivresetdone_out => open,
                rxctrl0_out => rxctrl0_out,
                rxctrl1_out => rxctrl1_out,
                rxctrl2_out => open,
                rxctrl3_out => open,
                rxbyteisaligned_out => open,
                rxbyterealign_out => open,
                rxcommadet_out => open,
                rxcommadeten_in => rx8b10ben_in(i downto i),
                rxmcommaalignen_in => rx8b10ben_in(i downto i),
                rxpcommaalignen_in => rx8b10ben_in(i downto i)
            );

        rxcdrlock(i) <= (not Channel_disable(i)) and RxCdrLock_int(i);
        gtrx_reset_i(i) <= --GTRX_RESET(i) when RX_ALIGN_SW='1' else
                           gtrx_reset(i) or (auto_gth_rxrst(i) and rxcdrlock(i));

        soft_reset_f(i) <= soft_reset(i/4) or cpll_reset(i);--or rst_hw; -- or GTRX_RESET(i);

        userclk_rx_reset_in(i) <=not RXPMARESETDONE(i);
    --RxResetDone_f(i) <= RxResetDone(i);

    end generate;



end RTL;
