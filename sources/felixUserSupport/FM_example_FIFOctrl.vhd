--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.





library ieee, UNISIM;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;

entity FM_example_FIFOctrl is
  port (
    EMU32b_EN     : out    std_logic;
    EMU32b_dout   : in     std_logic_vector(31 downto 0);
    EMU32b_dvalid : in     std_logic;
    FIFO34b_WE    : out    std_logic;
    clk240        : in     std_logic;
    fifo34_din    : out    std_logic_vector(31 downto 0);
    fifo34_dtype  : out    std_logic_vector(1 downto 0);
    fifo34_full   : in     std_logic;
    rst           : in     std_logic);
end entity FM_example_FIFOctrl;



architecture rtl of FM_example_FIFOctrl is

constant PACKAGE_LENGTH: std_logic_vector(15 downto 0) := x"0078"; -- decimal 120

signal DO_READ: std_logic;  -- signal is high when data is ready from CNT or FIFO32b
signal word_Cnt: std_logic_vector (15 downto 0); -- TODO: set from register

signal Dout: std_logic_vector(31 downto 0);
signal sEMU32b_EN, sFIFO256b32b_RE: std_logic;


begin

process (clk240, rst)
variable ignore_data: std_logic;
variable data_type: std_logic_vector(1 downto 0):= "11"; -- set default to "ignore data".
variable data_ready: std_logic;

begin
    if (rising_edge(clk240)) then
        if (rst = '1' ) then 
            data_type := "11";
            DO_READ <= '1';
            ignore_data := '1';
            data_ready := '0';
            word_Cnt <= PACKAGE_LENGTH; 
        else
            DO_READ <= '1';
            ignore_data := '1';
            data_ready := EMU32b_dvalid;
            if ((data_ready = '1') or (word_Cnt = 0)) then
                ignore_data := '0';
                if(word_Cnt = PACKAGE_LENGTH ) then --SOP
                    -- last idle character and "prepare SOP"
                    data_type := "01";  -- start of packet
                    word_Cnt <= (word_Cnt -1);
                elsif (word_Cnt = 4) then -- Do not enable read
                    data_type := "00";  -- intermediate word of current packet
                    word_Cnt <= (word_Cnt -1);
                    DO_READ <= '0';
                elsif(word_Cnt = 1) then --EOP
                    -- send EOP and write to fifo34b
                    data_type := "10"; -- EOP
                    word_Cnt <= (word_Cnt -1);
                elsif(word_Cnt = 0) then --IDLE
                    --start with three idle characters
                    data_ready:= '1';
                    data_type := "11";
                    word_Cnt <= PACKAGE_LENGTH;
                    
                else --payload data send data and write to fifo34b
                    data_type := "00";  -- intermediate word of current packet
                    word_Cnt <= (word_Cnt -1);
                end if;
            end if;
            FIFO34b_WE <= (data_ready and not ignore_data);  -- update on output after 1 clk cycle
            fifo34_din <= EMU32b_dout;
            fifo34_dtype <= data_type;
        end if;
    end if;  
end process;


-- FIFO combinational logic
process (fifo34_full, DO_READ)
begin
   sEMU32b_EN <= '0';         
   if (fifo34_full = '0' and DO_READ = '1') then
      sEMU32b_EN <= '1';
   end if;
   
 end process;


EMU32b_EN <= sEMU32b_EN;

end architecture rtl ; -- of FM_example_FIFOctrl

