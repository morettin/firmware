--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    17/08/2015 
--! Module Name:    elinkInterface_package
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee;
use ieee.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;

--! 
package elinkInterface_package is

constant elinkRate      : integer := 80; -- 80 / 160 / 320 / 640 Mbps   (*640 Mbps receiver only, data source is an 8b10b encoded data from emulator)
-- if elinkRate is 160 MHz, only {00-direct data} or {01-8b10b encoding} is supported
constant elinkEncoding  : std_logic_vector(1 downto 0) := "01"; -- 00-direct data / 01-8b10b encoding / 10-HDLC encoding 
--
constant packet_size    : std_logic_vector(7 downto 0) := x"0a";

end package elinkInterface_package ;