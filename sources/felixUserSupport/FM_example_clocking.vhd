--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               RHabraken
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------
--!                                                             
--!           NIKHEF - National Institute for Subatomic Physics 
--!
--!                       Electronics Department                
--!                                                             
--!-----------------------------------------------------------------------------
--! @class clock_and_reset
--! 
--!
--! @author      Andrea Borga    (andrea.borga@nikhef.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)
--!
--!
--! @date        07/01/2015    created
--!
--! @version     1.0
--!
--! @brief 
--! Clock and Reset instantiates an MMCM. It generates clocks of 40,
--! 80, 160 and 320 MHz.
--! Additionally a reset signal is issued when the MMCM is not locked.
--! Reset_out is synchronous to 40MHz
--! 
--!
--!-----------------------------------------------------------------------------
--! @TODO
--!  
--!
--! ------------------------------------------------------------------------------
-- 
--! @brief ieee



library ieee, UNISIM;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;

entity FM_example_clocking is
  generic(
    RECOVER_CLK_FROM_RX_GBT : boolean := false);
  port (
    RXUSRCLK_OUT         : in     std_logic;
    app_clk_in_n         : in     std_logic;
    app_clk_in_p         : in     std_logic;
    clk240               : out    std_logic;
    clk40                : out    std_logic;
    clk_si5324_240_out_n : out    std_logic;
    clk_si5324_240_out_p : out    std_logic;
    reset_out            : out    std_logic; --! Active high reset out (synchronous to clk40)
    sys_reset_n          : in     std_logic);
end entity FM_example_clocking;



architecture rtl of FM_example_clocking is



component clk_wiz_156_1
port
 (-- Clock in ports
  clk_in1_p      : in    std_logic;
  clk_in1_n      : in    std_logic;
  -- Clock out ports
  clk40             : out    std_logic;
  clk240            : out    std_logic;
  -- Status and control signals
  reset             : in     std_logic;
  locked            : out    std_logic
 );
end component;

   signal reset_in        : std_logic; 
   
   signal locked_s        : std_logic;
   signal clk240_s        : std_logic;
   signal clk_si5324_240_out     : std_logic;

begin
  

 -- FLX-709 Si570 @ 156.25MHz
clk0 : clk_wiz_156_1
  port map ( 
    -- Clock in ports
    clk_in1_p => app_clk_in_p,
    clk_in1_n => app_clk_in_n,
    -- Clock out ports  
    clk40  => clk40,
    clk240  => clk240_s,
    -- Status and control signals                
    reset => reset_in,
    locked => locked_s
    );
 
  clk240 <= clk240_s;
 
  reset_in  <= not sys_reset_n;
  reset_out <= not locked_s;
  
  g_recclk: if RECOVER_CLK_FROM_RX_GBT=true generate
    clk_si5324_240_out <= RXUSRCLK_OUT;
  end generate;
  
  g_xtalclk: if RECOVER_CLK_FROM_RX_GBT=false generate
    clk_si5324_240_out <= clk240_s;
  end generate;
  
  --OBUFDS to route the 160 MHz clock into the Si5324 jitter cleaner (VC709) to create the GBT Reference clock
  OBUF160: OBUFDS 
  generic map (
    IOSTANDARD => "LVDS",
    SLEW       => "FAST")
  port map(
    I => clk_si5324_240_out,
    O => clk_si5324_240_out_p,
    OB => clk_si5324_240_out_n);
 
end architecture rtl ; -- of FM_example_clocking

