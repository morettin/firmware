--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Frans Schreuder
--!               Julia Narevicius
--!               Israel Grayzman
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    07/07/2014 
--! Module Name:    GBTdataEmulator
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library work, IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.pcie_package.all;
use work.centralRouter_package.all;

--! E-link data emulator
entity GBTdataEmulator is
Generic ( 
    EMU_DIRECTION       : string := "ToHost";  -- ToHost or ToFrontEnd
    useGBTdataEmulator  : boolean := true;
    wideMode            : boolean := false
    );
Port (
    clk40                   : in  std_logic;
    wrclk                   : in  std_logic;
    rst_hw                  : in  std_logic;
    rst_soft                : in  std_logic;
    xoff                    : in  std_logic;
    register_map_control    : in  register_map_control_type;
    ---------
    GBTdata                 : out std_logic_vector(119 downto 0);
    GBTlinkValid            : out std_logic    
    );
end GBTdataEmulator;

architecture Behavioral of GBTdataEmulator is

----------------------------------
----------------------------------
component emuram_0
port (
    clka    : in  std_logic;
    wea     : in  std_logic_vector(0 downto 0);
    addra   : in  std_logic_vector(13 downto 0);
    dina    : in  std_logic_vector(15 downto 0);
    douta   : out std_logic_vector(15 downto 0);
    clkb    : in  std_logic;
    rstb    : in  std_logic;
    web     : in  std_logic_vector(0 downto 0);
    addrb   : in  std_logic_vector(13 downto 0);
    dinb    : in  std_logic_vector(15 downto 0);
    doutb   : out std_logic_vector(15 downto 0)
    );
end component emuram_0;
----------------------------------
----------------------------------
component emuram_1
port (
    clka    : in  std_logic;
    wea     : in  std_logic_vector(0 downto 0);
    addra   : in  std_logic_vector(13 downto 0);
    dina    : in  std_logic_vector(15 downto 0);
    douta   : out std_logic_vector(15 downto 0);
    clkb    : in  std_logic;
    rstb    : in  std_logic;
    web     : in  std_logic_vector(0 downto 0);
    addrb   : in  std_logic_vector(13 downto 0);
    dinb    : in  std_logic_vector(15 downto 0);
    doutb   : out std_logic_vector(15 downto 0)
    );
end component emuram_1;
----------------------------------
----------------------------------
component emuram_2
port (
    clka    : in  std_logic;
    wea     : in  std_logic_vector(0 downto 0);
    addra   : in  std_logic_vector(13 downto 0);
    dina    : in  std_logic_vector(15 downto 0);
    douta   : out std_logic_vector(15 downto 0);
    clkb    : in  std_logic;
    rstb    : in  std_logic;
    web     : in  std_logic_vector(0 downto 0);
    addrb   : in  std_logic_vector(13 downto 0);
    dinb    : in  std_logic_vector(15 downto 0);
    doutb   : out std_logic_vector(15 downto 0)
    );
end component emuram_2;
----------------------------------
----------------------------------
component emuram_3
port (
    clka    : in  std_logic;
    wea     : in  std_logic_vector(0 downto 0);
    addra   : in  std_logic_vector(13 downto 0);
    dina    : in  std_logic_vector(15 downto 0);
    douta   : out std_logic_vector(15 downto 0);
    clkb    : in  std_logic;
    rstb    : in  std_logic;
    web     : in  std_logic_vector(0 downto 0);
    addrb   : in  std_logic_vector(13 downto 0);
    dinb    : in  std_logic_vector(15 downto 0);
    doutb   : out std_logic_vector(15 downto 0)
    );
end component emuram_3;
----------------------------------
----------------------------------
component emuram_4
port (
    clka    : in  std_logic;
    wea     : in  std_logic_vector(0 downto 0);
    addra   : in  std_logic_vector(13 downto 0);
    dina    : in  std_logic_vector(15 downto 0);
    douta   : out std_logic_vector(15 downto 0);
    clkb    : in  std_logic;
    rstb    : in  std_logic;
    web     : in  std_logic_vector(0 downto 0);
    addrb   : in  std_logic_vector(13 downto 0);
    dinb    : in  std_logic_vector(15 downto 0);
    doutb   : out std_logic_vector(15 downto 0)
    );
end component emuram_4;
----------------------------------
----------------------------------
component emuram_5
port (
    clka    : in  std_logic;
    wea     : in  std_logic_vector(0 downto 0);
    addra   : in  std_logic_vector(13 downto 0);
    dina    : in  std_logic_vector(15 downto 0);
    douta   : out std_logic_vector(15 downto 0);
    clkb    : in  std_logic;
    rstb    : in  std_logic;
    web     : in  std_logic_vector(0 downto 0);
    addrb   : in  std_logic_vector(13 downto 0);
    dinb    : in  std_logic_vector(15 downto 0);
    doutb   : out std_logic_vector(15 downto 0)
    );
end component emuram_5;
----------------------------------
----------------------------------
component emuram_6
port (
    clka    : in  std_logic;
    wea     : in  std_logic_vector(0 downto 0);
    addra   : in  std_logic_vector(13 downto 0);
    dina    : in  std_logic_vector(15 downto 0);
    douta   : out std_logic_vector(15 downto 0);
    clkb    : in  std_logic;
    rstb    : in  std_logic;
    web     : in  std_logic_vector(0 downto 0);
    addrb   : in  std_logic_vector(13 downto 0);
    dinb    : in  std_logic_vector(15 downto 0);
    doutb   : out std_logic_vector(15 downto 0)
    );
end component emuram_6;
----------------------------------
----------------------------------

signal emuram_rdaddr, emuram_wraddr : std_logic_vector(13 downto 0) := (others => '0'); 
signal RESET    : std_logic := '1';
signal rst_fall : std_logic;
signal ena      : std_logic := '0';
signal emuram_wrdata : std_logic_vector(15 downto 0) := (others=>'0');

signal GBTdata_s : std_logic_vector(119 downto 0);

type we_array_type is array (0 to 6) of std_logic_vector(0 downto 0); 
signal wea : we_array_type :=((others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'));

constant web        : std_logic_vector(0 downto 0) := (others=>'0');
constant zeros16bit : std_logic_vector(15 downto 0) := (others=>'0');
constant zeros32bit : std_logic_vector(31 downto 0) := (others=>'0');
constant addr_max   : std_logic_vector(13 downto 0) := "11111111111011"; -- 16379 (5 x 3276)

-- add by Israel Grayzman to be eqvivalent to the CR reset scheme
constant fifoFLUSHcount_max : std_logic_vector (7 downto 0) := "10000000";
constant commonRSTcount_max : std_logic_vector (7 downto 0) := (others=>'1');
--IG signal cr_rst_rr : std_logic := '1';
signal rstTimerCount : std_logic_vector (7 downto 0) := (others=>'0');

begin
--
disableEmulator: if useGBTdataEmulator = false generate
GBTdata_s     <= (others=>'0');
GBTlinkValid  <= '0';
end generate disableEmulator;
--

--
enableEmulator: if useGBTdataEmulator = true generate

--****************************************************************************
-- add by Israel Grayzman to be eqvivalent to the CR reset scheme

rstTimerCounter: process(clk40,rst_soft)
begin
    if (rst_soft = '1') then
        rstTimerCount <= (others=>'0');
    elsif rising_edge(clk40) then
        if rstTimerCount = commonRSTcount_max then -- stop counting
            rstTimerCount <= rstTimerCount; -- freese counter
        else
            rstTimerCount <= rstTimerCount + 1;
        end if;
	end if;
end process;
--
cr_rst_out: process(clk40,rst_soft)
begin
    if (rst_soft = '1') then
        RESET <= '1';
    elsif rising_edge(clk40) then
        if rstTimerCount = commonRSTcount_max then
            RESET <= '0';
        else
            RESET <= RESET;
        end if;
	end if;
end process;
--************************************************************************

--IG -- global reset
--IG rst_fall_pulse: entity work.pulse_fall_pw01(behavioral) port map(clk40, rst_soft, rst_fall);
--IG --
--IG RESET_latch: process(clk40, rst_hw)
--IG begin
--IG     if rst_hw = '1' then
--IG         RESET <= '1'; 
--IG     elsif clk40'event and clk40 = '1' then
--IG         if rst_soft = '1' then
--IG             RESET <= '1';
--IG         elsif rst_fall = '1' then
--IG             RESET <= '0';
--IG         end if;
--IG     end if;
--IG end process;

--RESET <= rst_hw or rst_soft;

-- data out enable
process(clk40, RESET)
begin
    if RESET = '1' then
        ena <= '0';
    elsif clk40'event and clk40 = '1' then
      if EMU_DIRECTION = "ToHost" then
        ena <= to_sl(register_map_control.GBT_EMU_ENA.TOHOST) and (not xoff);
      else
        ena <= to_sl(register_map_control.GBT_EMU_ENA.TOFRONTEND) and (not xoff);
      end if;
    end if;
end process;
--
--
process(wrclk,RESET)
begin
    if RESET = '1' then
        wea             <= ((others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'),(others=>'0'));
        emuram_wraddr   <= (others=>'0');
        emuram_wrdata   <= (others=>'0');
    elsif wrclk'event and wrclk = '1' then
        wea(0)(0) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(0); -- 
        wea(1)(0) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(1); -- 
        wea(2)(0) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(2); -- 
        wea(3)(0) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(3); -- 
        wea(4)(0) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(4); -- 
        wea(5)(0) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(5); -- 
        wea(6)(0) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(6); -- 
        emuram_wraddr   <= register_map_control.GBT_EMU_CONFIG.WRADDR;   -- 14 bit
        emuram_wrdata   <= register_map_control.GBT_EMU_CONFIG.WRDATA;   -- 16 bit data
	end if;
end process;

-- address counter
--IG address_counter: process(clk40)
address_counter: process(RESET, clk40) -- IG: add RESET dependency
begin
    if (RESET = '1') then
        emuram_rdaddr <= (others => '0');
    elsif clk40'event and clk40 = '1' then
		if ena = '1' then
			if emuram_rdaddr = addr_max then
                emuram_rdaddr <= (others => '0'); 
            else
                emuram_rdaddr <= emuram_rdaddr + 1; 
            end if;
		else
			if emuram_rdaddr >= "00000000000100" then
                emuram_rdaddr <= (others => '0'); 
            else
                emuram_rdaddr <= emuram_rdaddr + 1; 
            end if;
		end if;
	end if;
end process;
--

--
emuram_00 : emuram_0
port map (
    clka    => wrclk,
    wea     => wea(0),
    addra   => emuram_wraddr,
    dina    => emuram_wrdata,
    douta   => open,
    --
    clkb    => clk40,
    rstb    => RESET,
    web     => web,
    addrb   => emuram_rdaddr,
    dinb    => zeros16bit,
    doutb   => GBTdata_s(15 downto 0)
    );
--
emuram_01 : emuram_1
port map (
    clka    => wrclk,
    wea     => wea(1),
    addra   => emuram_wraddr,
    dina    => emuram_wrdata,
    douta   => open,
    --
    clkb    => clk40,
    rstb    => RESET,
    web     => web,
    addrb   => emuram_rdaddr,
    dinb    => zeros16bit,
    doutb   => GBTdata_s(31 downto 16)
    );
--
emuram_02 : emuram_2
port map (
    clka    => wrclk,
    wea     => wea(2),
    addra   => emuram_wraddr,
    dina    => emuram_wrdata,
    douta   => open,
    --
    clkb    => clk40,
    rstb    => RESET,
    web     => web,
    addrb   => emuram_rdaddr,
    dinb    => zeros16bit,
    doutb   => GBTdata_s(47 downto 32)
    );
--
emuram_03 : emuram_3
port map (
    clka    => wrclk,
    wea     => wea(3),
    addra   => emuram_wraddr,
    dina    => emuram_wrdata,
    douta   => open,
    --
    clkb    => clk40,
    rstb    => RESET,
    web     => web,
    addrb   => emuram_rdaddr,
    dinb    => zeros16bit,
    doutb   => GBTdata_s(63 downto 48)
    );
--
emuram_04 : emuram_4
port map (
    clka    => wrclk,
    wea     => wea(4),
    addra   => emuram_wraddr,
    dina    => emuram_wrdata,
    douta   => open,
    --
    clkb    => clk40,
    rstb    => RESET,
    web     => web,
    addrb   => emuram_rdaddr,
    dinb    => zeros16bit,
    doutb   => GBTdata_s(79 downto 64)
    );
--
emuram_05 : emuram_5
port map (
    clka    => wrclk,
    wea     => wea(5),
    addra   => emuram_wraddr,
    dina    => emuram_wrdata,
    douta   => open,
    --
    clkb    => clk40,
    rstb    => RESET,
    web     => web,
    addrb   => emuram_rdaddr,
    dinb    => zeros16bit,
    doutb   => GBTdata_s(95 downto 80)
    );
--
emuram_06 : emuram_6
port map (
    clka    => wrclk,
    wea     => wea(6),
    addra   => emuram_wraddr,
    dina    => emuram_wrdata,
    douta   => open,
    --
    clkb    => clk40,
    rstb    => RESET,
    web     => web,
    addrb   => emuram_rdaddr,
    dinb    => zeros16bit,
    doutb   => GBTdata_s(111 downto 96)
    );
--

GBTdata_s(119 downto 112) <= "0101" & "11" & "11"; --GBTdata_s(49 downto 48); --"00";
GBTlinkValid <= '1';

end generate enableEmulator;
--
--
isWideMode: if wideMode = true generate
GBTdata <= GBTdata_s;
end generate isWideMode;
--
isNormalMode: if wideMode = false generate
GBTdata <= GBTdata_s(119 downto 112) & GBTdata_s(79 downto 0) & zeros32bit;
end generate isNormalMode;
--

end Behavioral;

