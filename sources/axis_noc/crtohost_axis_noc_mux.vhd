library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

library xpm;
use xpm.vcomponents.all; --@suppress

use work.axi_stream_package.all;
entity crtohost_axis_noc_mux is
  generic(
    LINK_NUM: integer := 12;
    DATA_WIDTH: integer := 512;
    BLOCK_SIZE: integer := 1024
  );
  Port (
    aresetn : in std_logic;
    m_aclk: in std_logic;
    s_aclk: in std_logic_vector(0 to 12);
    s_axis : in axis_512_array_type(0 to 12);
    s_axis_tready : out axis_tready_array_type(0 to 12);
    m_axis : out axis_512_array_type(0 to 3);
    m_axis_tready : in axis_tready_array_type(0 to 3)
  );
end crtohost_axis_noc_mux;

architecture Behavioral of crtohost_axis_noc_mux is
    signal m_axis_i : axis_512_array_type(0 to 3);
    signal m_axis_i_tready : axis_tready_array_type(0 to 3);
    constant BLOCKSIZE_DW : integer := BLOCK_SIZE/(DATA_WIDTH/8); --16 for 1024 block size and 512b data width.
    constant PAGES : integer := 1024/BLOCKSIZE_DW;
    constant PAGE_BITS : integer := 6; --! @TODO: use f_log2 function to calculate 
    constant BLOCKSIZE_BITS: integer := 4; --! @TODO: use f_log2 function to calculate
    type IntArray_type is array (0 to 3) of integer range 0 to 3;
    signal InLinkID: IntArray_type;
    
    
    
    signal M00_AXIS_0_tkeep: std_logic_vector(DATA_WIDTH/8-1 downto 0);
    signal M01_AXIS_0_tkeep: std_logic_vector(DATA_WIDTH/8-1 downto 0);
    signal M02_AXIS_0_tkeep: std_logic_vector(DATA_WIDTH/8-1 downto 0);
    signal M03_AXIS_0_tkeep: std_logic_vector(DATA_WIDTH/8-1 downto 0);
    
    

   component axis_noc is
     port(
       M00_AXIS_0_tdata    : out std_logic_vector(DATA_WIDTH-1 downto 0);
       M00_AXIS_0_tkeep    : out std_logic_vector(DATA_WIDTH/8-1 downto 0);
       M00_AXIS_0_tlast    : out std_logic;
       M00_AXIS_0_tready   : in std_logic;
       M00_AXIS_0_tvalid   : out std_logic;
       M01_AXIS_0_tdata    : out std_logic_vector(DATA_WIDTH-1 downto 0);
       M01_AXIS_0_tkeep    : out std_logic_vector(DATA_WIDTH/8-1 downto 0);
       M01_AXIS_0_tlast    : out std_logic;
       M01_AXIS_0_tready   : in std_logic;
       M01_AXIS_0_tvalid   : out std_logic;
       M02_AXIS_0_tdata    : out std_logic_vector(DATA_WIDTH-1 downto 0);
       M02_AXIS_0_tkeep    : out std_logic_vector(DATA_WIDTH/8-1 downto 0);
       M02_AXIS_0_tlast    : out std_logic;
       M02_AXIS_0_tready   : in std_logic;
       M02_AXIS_0_tvalid   : out std_logic;
       M03_AXIS_0_tdata    : out std_logic_vector(DATA_WIDTH-1 downto 0);
       M03_AXIS_0_tkeep    : out std_logic_vector(DATA_WIDTH/8-1 downto 0);
       M03_AXIS_0_tlast    : out std_logic;
       M03_AXIS_0_tready   : in std_logic;
       M03_AXIS_0_tvalid   : out std_logic;
       S00_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S00_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S00_AXIS_0_tlast    : in std_logic;
       S00_AXIS_0_tready   : out std_logic;
       S00_AXIS_0_tvalid   : in std_logic;
       S01_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S01_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S01_AXIS_0_tlast    : in std_logic;
       S01_AXIS_0_tready   : out std_logic;
       S01_AXIS_0_tvalid   : in std_logic;
       S02_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S02_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S02_AXIS_0_tlast    : in std_logic;
       S02_AXIS_0_tready   : out std_logic;
       S02_AXIS_0_tvalid   : in std_logic;
       S03_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S03_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S03_AXIS_0_tlast    : in std_logic;
       S03_AXIS_0_tready   : out std_logic;
       S03_AXIS_0_tvalid   : in std_logic;
       S04_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S04_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S04_AXIS_0_tlast    : in std_logic;
       S04_AXIS_0_tready   : out std_logic;
       S04_AXIS_0_tvalid   : in std_logic;
       S05_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S05_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S05_AXIS_0_tlast    : in std_logic;
       S05_AXIS_0_tready   : out std_logic;
       S05_AXIS_0_tvalid   : in std_logic;
       S06_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S06_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S06_AXIS_0_tlast    : in std_logic;
       S06_AXIS_0_tready   : out std_logic;
       S06_AXIS_0_tvalid   : in std_logic;
       S07_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S07_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S07_AXIS_0_tlast    : in std_logic;
       S07_AXIS_0_tready   : out std_logic;
       S07_AXIS_0_tvalid   : in std_logic;
       S08_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S08_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S08_AXIS_0_tlast    : in std_logic;
       S08_AXIS_0_tready   : out std_logic;
       S08_AXIS_0_tvalid   : in std_logic;
       S09_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S09_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S09_AXIS_0_tlast    : in std_logic;
       S09_AXIS_0_tready   : out std_logic;
       S09_AXIS_0_tvalid   : in std_logic;
       S10_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S10_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S10_AXIS_0_tlast    : in std_logic;
       S10_AXIS_0_tready   : out std_logic;
       S10_AXIS_0_tvalid   : in std_logic;
       S11_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S11_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S11_AXIS_0_tlast    : in std_logic;
       S11_AXIS_0_tready   : out std_logic;
       S11_AXIS_0_tvalid   : in std_logic;
       S12_AXIS_0_tdata    : in std_logic_vector(DATA_WIDTH-1 downto 0);
       S12_AXIS_0_tkeep    : in std_logic_vector(DATA_WIDTH/8-1 downto 0);
       S12_AXIS_0_tlast    : in std_logic;
       S12_AXIS_0_tready   : out std_logic;
       S12_AXIS_0_tvalid   : in std_logic;
       aclk0_0             : in std_logic;
       aclk1_0             : in std_logic;
       aclk2_0             : in std_logic;
       aclk3_0             : in std_logic;
       aclk4_0             : in std_logic;
       aclk5_0             : in std_logic;
       aclk6_0             : in std_logic;
       aclk7_0             : in std_logic;
       aclk8_0             : in std_logic;
       aclk9_0             : in std_logic;
       aclk10_0            : in std_logic;
       aclk11_0            : in std_logic;
       aclk12_0            : in std_logic;
       aclk13_0            : in std_logic
       
    
     );
    end component;
   
  
begin


    
axis_noc_mux_i: axis_noc port map(
        M00_AXIS_0_tdata  => m_axis_i(0).tdata,
        M00_AXIS_0_tkeep  => M00_AXIS_0_tkeep,
        M00_AXIS_0_tlast  => m_axis_i(0).tlast,
        M00_AXIS_0_tready => m_axis_i_tready(0),
        M00_AXIS_0_tvalid => m_axis_i(0).tvalid,
        M01_AXIS_0_tdata  => m_axis_i(1).tdata,
        M01_AXIS_0_tkeep  => M01_AXIS_0_tkeep,
        M01_AXIS_0_tlast  => m_axis_i(1).tlast,
        M01_AXIS_0_tready => m_axis_i_tready(1),
        M01_AXIS_0_tvalid => m_axis_i(1).tvalid,
        M02_AXIS_0_tdata  => m_axis_i(2).tdata,
        M02_AXIS_0_tkeep  => M02_AXIS_0_tkeep,
        M02_AXIS_0_tlast  => m_axis_i(2).tlast,
        M02_AXIS_0_tready => m_axis_i_tready(2),
        M02_AXIS_0_tvalid => m_axis_i(2).tvalid,
        M03_AXIS_0_tdata  => m_axis_i(3).tdata,
        M03_AXIS_0_tkeep  => M03_AXIS_0_tkeep,
        M03_AXIS_0_tlast  => m_axis_i(3).tlast,
        M03_AXIS_0_tready => m_axis_i_tready(3),
        M03_AXIS_0_tvalid => m_axis_i(3).tvalid,
        S00_AXIS_0_tdata  => s_axis(0).tdata,
        S00_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S00_AXIS_0_tkeep(3 downto 0)  => x"0",
        S00_AXIS_0_tlast  => s_axis(0).tlast,
        S00_AXIS_0_tready => s_axis_tready(0),
        S00_AXIS_0_tvalid => s_axis(0).tvalid,
        S01_AXIS_0_tdata  => s_axis(1).tdata,
        S01_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S01_AXIS_0_tkeep(3 downto 0)  => x"1",
        S01_AXIS_0_tlast  => s_axis(1).tlast,
        S01_AXIS_0_tready => s_axis_tready(1),
        S01_AXIS_0_tvalid => s_axis(1).tvalid,
        S02_AXIS_0_tdata  => s_axis(2).tdata,
        S02_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S02_AXIS_0_tkeep(3 downto 0)  => x"2",
        S02_AXIS_0_tlast  => s_axis(2).tlast,
        S02_AXIS_0_tready => s_axis_tready(2),
        S02_AXIS_0_tvalid => s_axis(2).tvalid,
        S03_AXIS_0_tdata  => s_axis(3).tdata,
        S03_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S03_AXIS_0_tkeep(3 downto 0)  => x"3",
        S03_AXIS_0_tlast  => s_axis(3).tlast,
        S03_AXIS_0_tready => s_axis_tready(3),
        S03_AXIS_0_tvalid => s_axis(3).tvalid,
        S04_AXIS_0_tdata  => s_axis(4).tdata,
        S04_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S04_AXIS_0_tkeep(3 downto 0)  => x"0",
        S04_AXIS_0_tlast  => s_axis(4).tlast,
        S04_AXIS_0_tready => s_axis_tready(4),
        S04_AXIS_0_tvalid => s_axis(4).tvalid,
        S05_AXIS_0_tdata  => s_axis(5).tdata,
        S05_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S05_AXIS_0_tkeep(3 downto 0)  => x"1",
        S05_AXIS_0_tlast  => s_axis(5).tlast,
        S05_AXIS_0_tready => s_axis_tready(5),
        S05_AXIS_0_tvalid => s_axis(5).tvalid,
        S06_AXIS_0_tdata  => s_axis(6).tdata,
        S06_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S06_AXIS_0_tkeep(3 downto 0)  => x"2",
        S06_AXIS_0_tlast  => s_axis(6).tlast,
        S06_AXIS_0_tready => s_axis_tready(6),
        S06_AXIS_0_tvalid => s_axis(6).tvalid,
        S07_AXIS_0_tdata  => s_axis(7).tdata,
        S07_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S07_AXIS_0_tkeep(3 downto 0)  => x"0",
        S07_AXIS_0_tlast  => s_axis(7).tlast,
        S07_AXIS_0_tready => s_axis_tready(7),
        S07_AXIS_0_tvalid => s_axis(7).tvalid,
        S08_AXIS_0_tdata  => s_axis(8).tdata,
        S08_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S08_AXIS_0_tkeep(3 downto 0)  => x"1",
        S08_AXIS_0_tlast  => s_axis(8).tlast,
        S08_AXIS_0_tready => s_axis_tready(8),
        S08_AXIS_0_tvalid => s_axis(8).tvalid,
        S09_AXIS_0_tdata  => s_axis(9).tdata,
        S09_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S09_AXIS_0_tkeep(3 downto 0)  => x"2",
        S09_AXIS_0_tlast  => s_axis(9).tlast,
        S09_AXIS_0_tready => s_axis_tready(9),
        S09_AXIS_0_tvalid => s_axis(9).tvalid,
        S10_AXIS_0_tdata  => s_axis(10).tdata,
        S10_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S10_AXIS_0_tkeep(3 downto 0)  => x"0",
        S10_AXIS_0_tlast  => s_axis(10).tlast,
        S10_AXIS_0_tready => s_axis_tready(10),
        S10_AXIS_0_tvalid => s_axis(10).tvalid,
        S11_AXIS_0_tdata  => s_axis(11).tdata,
        S11_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S11_AXIS_0_tkeep(3 downto 0)  => x"1",
        S11_AXIS_0_tlast  => s_axis(11).tlast,
        S11_AXIS_0_tready => s_axis_tready(11),
        S11_AXIS_0_tvalid => s_axis(11).tvalid,
        S12_AXIS_0_tdata  => s_axis(12).tdata,
        S12_AXIS_0_tkeep(DATA_WIDTH/8-1 downto 4)  => (others => '1'),
        S12_AXIS_0_tkeep(3 downto 0)  => x"2",
        S12_AXIS_0_tlast  => s_axis(12).tlast,
        S12_AXIS_0_tready => s_axis_tready(12),
        S12_AXIS_0_tvalid => s_axis(12).tvalid,
        aclk0_0           => s_aclk(0),
        aclk1_0           => s_aclk(1),
        aclk2_0           => s_aclk(2),
        aclk3_0           => s_aclk(3),
        aclk4_0           => s_aclk(4),
        aclk5_0           => s_aclk(5),
        aclk6_0           => s_aclk(6),
        aclk7_0           => s_aclk(7),
        aclk8_0           => s_aclk(8),
        aclk9_0           => s_aclk(9),
        aclk10_0           => s_aclk(10),
        aclk11_0           => s_aclk(11),
        aclk12_0           => s_aclk(12),
        aclk13_0           => m_aclk
        );
-- Connections:
    InLinkID(0) <= to_integer(unsigned('0'&M00_AXIS_0_tkeep(2 downto 0)));
    InLinkID(1) <= to_integer(unsigned('0'&M01_AXIS_0_tkeep(2 downto 0)));
    InLinkID(2) <= to_integer(unsigned('0'&M02_AXIS_0_tkeep(2 downto 0)));
    InLinkID(3) <= to_integer(unsigned('0'&M03_AXIS_0_tkeep(2 downto 0)));
    
    g_mems: for i in 0 to 3 generate
        signal wea: std_logic_vector(0 downto 0);
        signal dina, doutb: std_logic_vector(DATA_WIDTH-1 downto 0);
        signal addra, addrb: std_logic_vector(11 downto 0); --Determined by the size of one URAM depth. Depending on DATA_WIDTH, 4 or 8 URAM primitives will be instantiated
        type slvLINK_NUM_array is array (0 to PAGES-1) of std_logic_vector(LINK_NUM-1 downto 0);
        signal tready_link_id: slvLINK_NUM_array:= (others => (others => '1'));
        type IntBLOCKSIZEArray is array(0 to LINK_NUM-1) of integer range 0 to BLOCKSIZE_DW-1;
        type IntPAGESArray is array(0 to LINK_NUM-1) of integer range 0 to PAGES-1;
        signal addraCounters, addrbCounters: IntBLOCKSIZEArray;
        signal addraPages, addrbPages: IntPAGESArray;
        
        signal OutLinkId: integer range 0 to 3;
        
        signal m_axis_out_tvalid_p1 : std_logic;
        signal m_axis_out_tvalid_p2 : std_logic;
        signal m_axis_out_tvalid_p3 : std_logic;
        signal m_axis_out_tlast_p1  : std_logic;
        signal m_axis_out_tlast_p2  : std_logic;
        signal m_axis_out_tlast_p3  : std_logic;
    begin
    
        xpm_memory_sdpram_inst : xpm_memory_sdpram --Using 8 URAM primitives due to the data width of 512 bits. Depth is defined by the depth of 1 URAM.
        generic map (
          ADDR_WIDTH_A => 12,
          ADDR_WIDTH_B => 12,
          AUTO_SLEEP_TIME => 0,
          BYTE_WRITE_WIDTH_A => 512,
          CASCADE_HEIGHT => 0,
          CLOCKING_MODE => "common_clock",
          ECC_MODE => "no_ecc",
          MEMORY_INIT_FILE => "none",
          MEMORY_INIT_PARAM => "0",
          MEMORY_OPTIMIZATION => "true",
          MEMORY_PRIMITIVE => "ultra",
          MEMORY_SIZE => 4096*512,
          MESSAGE_CONTROL => 0,
          READ_DATA_WIDTH_B => 512,
          READ_LATENCY_B => 2,
          READ_RESET_VALUE_B => "0",
          RST_MODE_A => "SYNC",
          RST_MODE_B => "SYNC",
          SIM_ASSERT_CHK => 0,
          USE_EMBEDDED_CONSTRAINT => 0,
          USE_MEM_INIT => 1,
          WAKEUP_TIME => "disable_sleep",
          WRITE_DATA_WIDTH_A => 512,
          WRITE_MODE_B => "read_first"
        )
        port map (
          dbiterrb => open,
          doutb => doutb,
          sbiterrb => open,
          addra => addra,
          addrb => addrb,
          clka => m_aclk,
          clkb => m_aclk,
          dina => dina,
          ena => '1',
          enb => m_axis_out_tvalid_p1,
          injectdbiterra => '0',
          injectsbiterra => '0',
          regceb => '1',
          rstb => '0',
          sleep => '0',
          wea => wea
        );
        memory_handling_proc: process(m_aclk)
        begin
            if rising_edge(m_aclk) then
                dina <= m_axis_i(i).tdata;
                wea(0) <= m_axis_i(i).tvalid and m_axis_i_tready(i);
                addra <= std_logic_vector(to_unsigned(addraPages(InLinkID(i)),PAGE_BITS))&
                         std_logic_vector(to_unsigned(InLinkID(i),2)) & 
                         std_logic_vector(to_unsigned(addraCounters(InLinkID(i)),BLOCKSIZE_BITS));
                addrb <= std_logic_vector(to_unsigned(addrbPages(OutLinkID),PAGE_BITS))&
                         std_logic_vector(to_unsigned(OutLinkId,2)) & 
                         std_logic_vector(to_unsigned(addrbCounters(OutLinkId),BLOCKSIZE_BITS));
            end if;
        end process;
        
        m_axis_i_tready(i) <= tready_link_id(addraPages(InLinkID(i)))(InLinkID(i));
        
        m_axis(i).tdata <= doutb;
        m_axis(i).tvalid <= m_axis_out_tvalid_p3;
        m_axis(i).tlast <= m_axis_out_tlast_p3;
       
        fifo_sort_proc: process(m_aclk)
            variable WritingOut : boolean := false;
            variable NextOutLinkId: integer range 0 to LINK_NUM-1:= 0;
        begin
            if aresetn = '0' then
                addraCounters <= (others => 0);
                addrbCounters <= (others => 0);
                addraPages <= (others => 0);
                addrbPages <= (others => 0);
                m_axis_out_tvalid_p1 <= '0';
                m_axis_out_tlast_p1 <= '0';
                OutLinkID <= 0;
                WritingOut := false;
                NextOutLinkId := 0;
            elsif rising_edge(m_aclk) then
                if m_axis_i(i).tvalid = '1' and m_axis_i_tready(i) = '1' then
                    if m_axis_i(i).tlast = '0' then
                        addraCounters(InLinkID(i)) <= addraCounters(InLinkID(i)) + 1;
                    else
                        addraCounters(InLinkID(i)) <= 0; --Move to address 0 of the next page
                        tready_link_id(addraPages(InLinkID(i)))(InLinkID(i)) <= '0';
                        if addraPages(InLinkID(i)) < PAGES-1 then
                            addraPages(InLinkID(i)) <= addraPages(InLinkID(i)) + 1;
                        else
                            addraPages(InLinkID(i)) <= 0;
                        end if; 
                    end if; 
                 end if;
                 
                 --Read the FIFO:
                 if m_axis_tready(i) = '1' then
                     if WritingOut = false then
                        m_axis_out_tvalid_p1 <= '0';
                        m_axis_out_tlast_p1 <= '0';
                        if tready_link_id(addrbPages(OutLinkId))(OutLinkId) = '1' then
                            if OutLinkID /= LINK_NUM-1 then
                                OutLinkID <= OutLinkID + 1;
                            else
                                OutLinkID <= 0;
                            end if;
                        else
                            WritingOut := true;
                            if OutLinkID /= LINK_NUM-1 then
                                NextOutLinkId := OutLinkID + 1;
                            else
                                NextOutLinkId := 0;
                            end if;
                        end if;
                     end if;
                     if WritingOut then
                        
                        m_axis_out_tvalid_p1 <= '1';
                        m_axis_out_tlast_p1 <= '0';
                        if addrbCounters(OutLinkId) /= BLOCKSIZE_DW-1 then
                            addrbCounters(OutLinkId) <= addrbCounters(OutLinkId) + 1;
                        else
                            addrbCounters(OutLinkId) <= 0;
                            m_axis_out_tlast_p1 <= '1';
                            WritingOut := false;
                            tready_link_id(addrbPages(OutLinkId))(OutLinkId) <= '1';
                            if addrbPages(OutLinkId) < PAGES-1 then
                                addrbPages(OutLinkId) <= addrbPages(OutLinkId) + 1;
                            else
                                addrbPages(OutLinkId) <= 0;
                            end if;
                            
                            if tready_link_id(addrbPages(NextOutLinkId))(NextOutLinkId) = '0' and NextOutLinkId /= OutLinkId then
                                OutLinkId <= NextOutLinkId;                            
                                WritingOut := true;
                            end if;
                               
                        end if;
                        if tready_link_id(addrbPages(NextOutLinkId))(NextOutLinkId) = '1' then
                            if NextOutLinkID /= LINK_NUM-1 then
                                NextOutLinkId := NextOutLinkId + 1;
                            else
                                NextOutLinkId := 0;
                            end if;
                        end if;
                     end if;
                     m_axis_out_tvalid_p2 <= m_axis_out_tvalid_p1;
                     m_axis_out_tlast_p2 <= m_axis_out_tlast_p1;
                     m_axis_out_tvalid_p3 <= m_axis_out_tvalid_p2;
                     m_axis_out_tlast_p3 <= m_axis_out_tlast_p2;
                 end if; --tready
            end if;
        end process;
    end generate g_mems;

end Behavioral;
