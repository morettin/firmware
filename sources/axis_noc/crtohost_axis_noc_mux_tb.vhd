
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD.ALL;

use work.axi_stream_package.all;


entity crtohost_axis_noc_mux_tb is
end crtohost_axis_noc_mux_tb;

architecture Behavioral of crtohost_axis_noc_mux_tb is
    signal aresetn: std_logic;
    signal m_axis : axis_512_type;
    signal m_axis_tready : std_logic;
    signal s_axis : axis_512_array_type(0 to 11);
    signal s_axis_tready : axis_tready_array_type(0 to 11);
    signal m_aclk : std_logic;
    signal s_aclk : std_logic;
    constant m_aclk_period: time := 4 ns;
    constant s_aclk_period: time := 4.167 ns;
begin

    m_aclk_proc: process
    begin
        m_aclk <= '1';
        wait for m_aclk_period / 2;
        m_aclk <= '0';
        wait for m_aclk_period / 2;
    end process;
    
    s_aclk_proc: process
    begin
        s_aclk <= '1';
        wait for s_aclk_period / 2;
        s_aclk <= '0';
        wait for s_aclk_period / 2;
    end process;
    
    g_slaves: for i in 0 to 11 generate
        gen: process(s_aclk)
            variable cnt: integer range 0 to 15:= 0;
            variable pausecnt: integer range 0 to 239;
            variable state: boolean := false;
        begin
            if rising_edge(s_aclk) then
                if s_axis_tready(i) = '1' then
                    
                    s_axis(i).tdata <= (others => '0');
                    s_axis(i).tvalid <= '0';
                    s_axis(i).tlast <= '0';
                    if not state then --Generate data
                        s_axis(i).tvalid <= '1';
                        s_axis(i).tdata <= std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(cnt,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16))&
                                           std_logic_vector(to_unsigned(i,16));
                        if cnt < 15 then
                            cnt := cnt + 1;
                        else
                            cnt := 0;
                            s_axis(i).tlast <= '1';
                            state := true;
                        end if;
                        
                    else --Wait for 240 clock cycles to match FULL mode data rate.
                        if pausecnt < 239 then
                            pausecnt := pausecnt + 1;
                        else
                            pausecnt := 0;
                            state := false;
                        end if;
                    end if;
                end if;
            end if;
        end process;
    end generate;

uut: entity work.crtohost_axis_noc_mux 
  generic map(
    LINK_NUM => 12,
    DATA_WIDTH => 512,
    BLOCK_SIZE => 1024
  )
  Port map(
    aresetn => aresetn,
    m_aclk => m_aclk,
    s_aclk => s_aclk,
    s_axis => s_axis,
    s_axis_tready => s_axis_tready,
    m_axis => m_axis,
    m_axis_tready => m_axis_tready
  );
  
  m_axis_tready <= '1';
  
  aresetn_proc: process
  begin
    aresetn <= '0';
    wait for m_aclk_period * 10;
    aresetn <= '1';
    wait;
  end process;
  
end Behavioral;
