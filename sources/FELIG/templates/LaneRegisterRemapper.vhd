--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Michael Oberling
--
-- Design Name:  Lane Register Remapper
-- Version:    1.0
-- Date:    5/18/2018
--
-- Description:   Remaps registers into per lane controls and monitors
--
-- Change Log:  V1.0 -
--
--==============================================================================
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;
--use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;
    use work.function_lib.ALL;

    use work.pcie_package.all;
    use work.centralRouter_package.all;
    use work.FELIX_package.all;

entity LaneRegisterRemapper is
    generic
(
        GBT_NUM                     : integer := 24;
        FIRMWARE_MODE               : integer := FIRMWARE_MODE_FELIG_GBT
    --    ILA                         : integer := 1
    );
    port(
        register_map_monitor  : out  register_map_monitor_type;
        register_map_control  : in  register_map_control_type;

        lane_control      : out  array_of_lane_control_type(GBT_NUM-1 downto 0);
        lane_monitor      : in  array_of_lane_monitor_type(GBT_NUM-1 downto 0);
        LINKSconfig             : out   std_logic_vector(2 downto 0)
    );
end entity LaneRegisterRemapper;

architecture RTL of LaneRegisterRemapper is
    signal felig_counter_rx_slide  : array_of_slv_31_0(lane_control'range);
    signal felig_counter_fc_error  : array_of_slv_31_0(lane_control'range);
    signal felig_counter_freq_tx  : array_of_slv_31_0(lane_control'range);
    signal felig_counter_freq_rx  : array_of_slv_31_0(lane_control'range);
    signal felig_mon_picxo_error  : array_of_slv_20_0(lane_control'range);
    signal felig_mon_picxo_volt    : array_of_slv_21_0(lane_control'range);
    signal felig_mon_l1_id      : array_of_slv_31_0(lane_control'range);
    signal felig_mon_ttc_bch_mon  : array_of_array_0_3_slv_31_0(lane_control'range);


    signal felig_data_gen_config          : bitfield_felig_data_gen_config_w_type_array(lane_control'range);
    signal felig_elink_config            : bitfield_felig_elink_config_w_type_array(lane_control'range);
    signal felig_data_gen_config_userdata   : array_of_slv_15_0(lane_control'range);
    signal felig_elink_enable_orig          : array_of_slv_39_0(lane_control'range);
    signal felig_elink_enable               : array_of_slv_111_0(lane_control'range);
    signal felig_elink_endian_mode          : array_of_slv_111_0(lane_control'range);
    signal felig_elink_input_width          : array_of_slv_111_0(lane_control'range);
    --MT  signal felig_elink_output_width  : array_of_array_0_39_slv_1_0(lane_control'range);
    signal felig_elink_output_width  : array_of_array_0_111_slv_2_0(lane_control'range);
    signal felig_lane_config    : bitfield_felig_lane_config_w_type_array(lane_control'range);
    signal LINKSconfig_int          : std_logic_vector(2 downto 0) := "000";
    signal DATARATE                 : std_logic := '1';
    signal FECMODE                  : std_logic := '0';
begin

    DATARATE          <= '1';--register_map_control.LPGBT_DATARATE(0)
    FECMODE           <= '0';--register_map_control.LPGBT_FEC(0)

    --for reference upLinkTxDataPath FECMODE and DATARATE parameters
    --localparam FEC5           = 1'b0;
    --localparam FEC12          = 1'b1;
    --localparam TxDataRate5G12 = 1'b0;
    --localparam TxDataRate10G24= 1'b1;
    LINKSconfig_int  <= '0' & DATARATE & FECMODE when FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT else
                        '1' & DATARATE & FECMODE when FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT;
    LINKSconfig <= LINKSconfig_int;
    --RL modified to convert the old rm with 5 egroups into up to 7. egroups 5 and 6 always equal to 4
    GEN_WIDTHS : for i in lane_control'range generate
        GEN_GEN_WIDTHS : for j in lane_control(0).emulator'range generate
            signal felig_output_width : std_logic_vector(2 downto 0);
            signal felig_output_width_array :   array_of_slv_2_0(15 downto 0);
            signal felig_input_width : std_logic;
            signal felig_endian_mode : std_logic;
        begin
            --            less_that_five : if j<5 generate
            --                felig_output_width <= "0" & felig_elink_config(i).OUTPUT_WIDTH(j*2+1 downto j*2);
            --                felig_input_width <= felig_elink_config(i).INPUT_WIDTH(j+felig_elink_config(i).INPUT_WIDTH'low);
            --                felig_endian_mode <= felig_elink_config(i).ENDIAN_MOD(j+felig_elink_config(i).ENDIAN_MOD'low);
            --                felig_elink_enable(i)(j*16+7 downto j*16) <= felig_elink_enable_orig(i)(j*8+7 downto j*8);
            --                felig_elink_enable(i)(j*16+15 downto j*16+8) <= felig_elink_enable_orig(i)(j*8+7 downto j*8)  when (LINKSconfig_int(2 downto 1) = "11") else X"00";
            --            end generate less_that_five;
            --            more_that_five : if j>4 generate
            --                felig_output_width <= "0" & felig_elink_config(i).OUTPUT_WIDTH(9 downto 8);
            --                felig_input_width <= felig_elink_config(i).INPUT_WIDTH(4+felig_elink_config(i).INPUT_WIDTH'low);
            --                felig_endian_mode <= felig_elink_config(i).ENDIAN_MOD(4+felig_elink_config(i).ENDIAN_MOD'low);
            --                felig_elink_enable(i)(j*16+7 downto j*16) <= felig_elink_enable_orig(i)(4*8+7 downto 4*8);
            --                felig_elink_enable(i)(j*16+15 downto j*16+8) <= felig_elink_enable_orig(i)(4*8+7 downto 4*8) when (LINKSconfig_int(2 downto 1) = "11") else X"00";
            --            end generate more_that_five;
            felig_output_width <= felig_elink_config(i).OUTPUT_WIDTH(j*3+2 downto j*3);
            felig_input_width  <= felig_elink_config(i).INPUT_WIDTH(j+felig_elink_config(i).INPUT_WIDTH'low);
            felig_endian_mode  <= felig_elink_config(i).ENDIAN_MOD(j+felig_elink_config(i).ENDIAN_MOD'low);

            GEN_ENABLES_GBT : if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT generate
                --RL 40bits used. maximum of 8 E-Links (2b) per E-group. 5 EGroups x 8 E-Links = 40.
                less_that_five : if j<5 generate
                    felig_elink_enable(i)(j*16+7 downto j*16)    <= felig_elink_enable_orig(i)(j*8+7 downto j*8);
                    felig_elink_enable(i)(j*16+15 downto j*16+8) <= X"00";
                end generate less_that_five;
                more_that_five : if j>4 generate
                    felig_elink_enable(i)(j*16+15 downto j*16) <= (others=>'0');
                end generate more_that_five;
            end generate GEN_ENABLES_GBT;

            GEN_ENABLES_LPGBT : if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT generate
                --RL 28bits used. Maximum of 4 E-Links (8b) per E-group. 7 EGroups x 4 E-Links = 28.
                felig_elink_enable(i)(j*16+15 downto j*16)  <= "000" & felig_elink_enable_orig(i)(4*j+3) &
                                                               "000" & felig_elink_enable_orig(i)(4*j+2) &
                                                               "000" & felig_elink_enable_orig(i)(4*j+1) &
                                                               "000" & felig_elink_enable_orig(i)(4*j);
            end generate GEN_ENABLES_LPGBT;

            felig_output_width_array(0)  <= felig_output_width;
            felig_output_width_array(1)  <= "000";
            felig_output_width_array(2)  <= "00" & felig_output_width(0);
            felig_output_width_array(3)  <= "000";
            felig_output_width_array(4)  <= '0' & felig_output_width(1 downto 0);
            felig_output_width_array(5)  <= "000";
            felig_output_width_array(6)  <= "00" & felig_output_width(0);
            felig_output_width_array(7)  <= "000";
            felig_output_width_array(8)  <= '0' & felig_output_width(1 downto 0);
            felig_output_width_array(9)  <= "000";
            felig_output_width_array(10) <= "00" & felig_output_width(0);
            felig_output_width_array(11) <= "000";
            felig_output_width_array(12) <= '0' & felig_output_width(1 downto 0);
            felig_output_width_array(13) <= "000";
            felig_output_width_array(14) <= "00" & felig_output_width(0);
            felig_output_width_array(15) <= "000";

            lane_control(i).emulator(j).output_width <= felig_output_width;

            GEN_GEN_GEN_WIDTHS : for h in 0 to 15 generate
                felig_elink_output_width(i)(j*16+h)   <= felig_output_width_array(h);
                --felig_elink_output_width(i)(j*16+h+8) <= felig_output_width_array(h) when (LINKSconfig_int(2 downto 1) = "11") else "111";
                felig_elink_input_width(i)(j*16+h)    <= felig_input_width;
                --felig_elink_input_width(i)(j*16+h+8)  <= felig_input_width;
                felig_elink_endian_mode(i)(j*16+h)    <= felig_endian_mode;
            --felig_elink_endian_mode(i)(j*16+h+8)  <= felig_endian_mode;
            end generate GEN_GEN_GEN_WIDTHS;
        end generate GEN_GEN_WIDTHS;
    end generate GEN_WIDTHS;

    GEN_LANE_CONTROL_MAP : for i in lane_control'range generate
        felig_data_gen_config(i)                    <= register_map_control.FELIG_DATA_GEN_CONFIG(i)                ;
        felig_data_gen_config_userdata(i)           <= register_map_control.FELIG_DATA_GEN_CONFIG_USERDATA(i)       ;
        felig_elink_config(i)                       <= register_map_control.FELIG_ELINK_CONFIG(i)                   ;
        felig_elink_enable_orig(i)                  <= register_map_control.FELIG_ELINK_ENABLE(i)                   ;
        felig_lane_config(i)                        <= register_map_control.FELIG_LANE_CONFIG(i)                    ;

        lane_control(i).global.lane_reset      <= register_map_control.FELIG_RESET.LANE            (i+register_map_control.FELIG_RESET.LANE'low);
        lane_control(i).global.framegen_reset    <= register_map_control.FELIG_RESET.FRAMEGEN          (i+register_map_control.FELIG_RESET.FRAMEGEN'low);
        lane_control(i).global.elink_sync      <= felig_lane_config(i).ELINK_SYNC                (felig_lane_config(i).ELINK_SYNC'low);
        lane_control(i).global.framegen_data_select  <= felig_lane_config(i).FG_SOURCE                (felig_lane_config(i).FG_SOURCE'low);
        lane_control(i).global.emu_data_select    <= felig_lane_config(i).GBT_EMU_SOURCE              (felig_lane_config(i).GBT_EMU_SOURCE'low);
        lane_control(i).global.l1a_source      <= felig_lane_config(i).L1A_SOURCE                (felig_lane_config(i).L1A_SOURCE'low);
        lane_control(i).global.loopback_fifo_delay  <= felig_lane_config(i).LB_FIFO_DELAY              ;
        lane_control(i).global.loopback_fifo_reset  <= register_map_control.FELIG_RESET.LB_FIFO            (register_map_control.FELIG_RESET.LB_FIFO'low);
        lane_control(i).global.a_ch_bit_sel      <= felig_lane_config(i).A_CH_BIT_SEL              ;
        --MT tmp
        --    lane_control(i).global.b_ch_bit_sel      <= felig_lane_config(i).B_CH_BIT_SEL              ;
        lane_control(i).global.b_ch_bit_sel      <= felig_lane_config(i).B_CH_BIT_SEL(48 downto 42)              ;
        lane_control(i).global.l1a_counter_reset    <= '0';--register_map_control.FELIG_L1ID_RESET                        (register_map_control.FELIG_L1ID_RESET'low);
        lane_control(i).global.MSB                  <= register_map_control.ENCODING_REVERSE_10B                    (register_map_control.ENCODING_REVERSE_10B'low);
        lane_control(i).clock.gth_tx_pi_hold    <= felig_lane_config(i).PI_HOLD                  (felig_lane_config(i).PI_HOLD'low);
        lane_control(i).clock.picxo_offset_en    <= felig_lane_config(i).PICXO_OFFEST_EN              (felig_lane_config(i).PICXO_OFFEST_EN'low);
        --lane_control(i).clock.picxo_offset_ppm    <= register_map_control.FELIG_GLOBAL_CONTROL.PICXO_OFFSET_PPM  ;
        lane_control(i).clock.picxo_offset_ppm    <= (others=> '0');
        lane_control(i).gbt.rxslide_select      <= register_map_control.GBT_RXSLIDE_SELECT            (i+register_map_control.GBT_RXSLIDE_SELECT'low);
        lane_control(i).gbt.rx_reset        <= register_map_control.GBT_RX_RESET              (i+register_map_control.GBT_RX_RESET'low);
        lane_control(i).gbt.tc_edge          <= register_map_control.GBT_TC_EDGE                (i+register_map_control.GBT_TC_EDGE'low);
        lane_control(i).gbt.tx_reset        <= register_map_control.GBT_TX_RESET              (i+register_map_control.GBT_TX_RESET'low);
        lane_control(i).gbt.tx_tc_method      <= register_map_control.GBT_TX_TC_METHOD            (i+register_map_control.GBT_TX_TC_METHOD'low);
        lane_control(i).gbt.rx_data_format(0)    <= register_map_control.GBT_DATA_RXFORMAT1            (i+register_map_control.GBT_DATA_RXFORMAT1'low);
        lane_control(i).gbt.rx_data_format(1)    <= register_map_control.GBT_DATA_RXFORMAT2            (i+register_map_control.GBT_DATA_RXFORMAT2'low);
        lane_control(i).gbt.tx_data_format(0)    <= register_map_control.GBT_DATA_TXFORMAT1            (i+register_map_control.GBT_DATA_TXFORMAT1'low);
        lane_control(i).gbt.tx_data_format(1)    <= register_map_control.GBT_DATA_TXFORMAT2            (i+register_map_control.GBT_DATA_TXFORMAT2'low);
        lane_control(i).gbt.tx_tc_dly_value(0)    <= register_map_control.GBT_TX_TC_DLY_VALUE1          (i+register_map_control.GBT_TX_TC_DLY_VALUE1'low);
        lane_control(i).gbt.tx_tc_dly_value(1)    <= register_map_control.GBT_TX_TC_DLY_VALUE2          (i+register_map_control.GBT_TX_TC_DLY_VALUE2'low);
        lane_control(i).gbt.tx_tc_dly_value(2)    <= register_map_control.GBT_TX_TC_DLY_VALUE3          (i+register_map_control.GBT_TX_TC_DLY_VALUE3'low);
        lane_control(i).gbt.tx_tc_dly_value(3)    <= register_map_control.GBT_TX_TC_DLY_VALUE4          (i+register_map_control.GBT_TX_TC_DLY_VALUE4'low);
        lane_control(i).gbt.loopback_enable      <= felig_lane_config(i).GBH_LB_ENABLE              (felig_lane_config(i).GBH_LB_ENABLE'low);
        lane_control(i).gbt.rxslide_count_reset    <= register_map_control.FELIG_RX_SLIDE_RESET          (i+register_map_control.FELIG_RX_SLIDE_RESET'low);
        lane_control(i).gth.manual_gth_rxreset    <= register_map_control.GBT_SOFT_RX_RESET.RESET_GT        (i+register_map_control.GBT_SOFT_RX_RESET.RESET_GT'low);
        lane_control(i).gth.txpolarity        <= register_map_control.GBT_TXPOLARITY              (i+register_map_control.GBT_TXPOLARITY'low);
        lane_control(i).gth.rxpolarity        <= register_map_control.GBT_RXPOLARITY              (i+register_map_control.GBT_RXPOLARITY'low);
        lane_control(i).gth.loopback_enable      <= felig_lane_config(i).GBT_LB_ENABLE              (felig_lane_config(i).GBT_LB_ENABLE'low);
        GEN_LANE_EGROUP_CONTROL_MAP : for j in lane_control(0).emulator'range generate --RL:changed so it compiles. sizes need to match 0 to 4 generate
            --            less_that_five : if j<5 generate
            lane_control(i).emulator(j).pattern_select(0)  <= felig_data_gen_config(i).PATTERN_SEL    (j+felig_data_gen_config(i).PATTERN_SEL'low);
            lane_control(i).emulator(j).pattern_select(1)  <= '0'                                      ;
            lane_control(i).emulator(j).data_format(0)    <= felig_data_gen_config(i).DATA_FORMAT    (2*j+felig_data_gen_config(i).DATA_FORMAT'low);
            lane_control(i).emulator(j).data_format(1)      <= felig_data_gen_config(i).DATA_FORMAT    (2*j+1+felig_data_gen_config(i).DATA_FORMAT'low);
            lane_control(i).emulator(j).sw_busy        <= felig_data_gen_config(i).SW_BUSY      (j+felig_data_gen_config(i).SW_BUSY'low);
            lane_control(i).emulator(j).reset        <= felig_data_gen_config(i).RESET      (j+felig_data_gen_config(i).RESET'low);
            lane_control(i).emulator(j).chunk_length    <= felig_data_gen_config(i).CHUNK_LENGTH  ;
            lane_control(i).emulator(j).userdata            <= felig_data_gen_config_userdata(i)        ;
        --lane_control(i).emulator(j).output_width    <= felig_elink_output_width          (i)(j*8);
        --lane_control(i).emulator(j).userdata      <= felig_data_gen_config(i).USERDATA    ;
        --          end generate less_that_five;
        --          more_that_five : if j>4 generate
        --          lane_control(i).emulator(j).pattern_select(0)  <= felig_data_gen_config(i).PATTERN_SEL    (4+felig_data_gen_config(i).PATTERN_SEL'low);
        --          lane_control(i).emulator(j).pattern_select(1)  <= '0'                                      ;
        --          lane_control(i).emulator(j).data_format      <= felig_data_gen_config(i).DATA_FORMAT    (4+felig_data_gen_config(i).DATA_FORMAT'low);
        --          lane_control(i).emulator(j).sw_busy        <= felig_data_gen_config(i).SW_BUSY      (4+felig_data_gen_config(i).SW_BUSY'low);
        --          lane_control(i).emulator(j).reset        <= felig_data_gen_config(i).RESET      (4+felig_data_gen_config(i).RESET'low);
        --          --lane_control(i).emulator(j).output_width    <= felig_elink_output_width          (i)(4*8);
        --          lane_control(i).emulator(j).chunk_length    <= felig_data_gen_config(i).CHUNK_LENGTH  ;
        --          --lane_control(i).emulator(j).userdata      <= felig_data_gen_config(i).USERDATA    ;
        --          lane_control(i).emulator(j).userdata            <= felig_data_gen_config_userdata(i)        ;
        --        end generate more_that_five;
        end generate GEN_LANE_EGROUP_CONTROL_MAP;
        GEN_LANE_ELINK_CONTROL_MAP : for j in lane_control(0).elink'range generate --RL:changed so it compiles. sizes need to match 0 to 39 generate --
            lane_control(i).elink(j).output_width        <= felig_elink_output_width  (i)(j);
            lane_control(i).elink(j).input_width        <= felig_elink_input_width  (i)(j);
            lane_control(i).elink(j).endian_mode        <= felig_elink_endian_mode  (i)(j);
            lane_control(i).elink(j).enable            <= felig_elink_enable    (i)(j);
        end generate GEN_LANE_ELINK_CONTROL_MAP;
    end generate GEN_LANE_CONTROL_MAP;

    GEN_LANE_MONITOR_MAP : for i in lane_monitor'range generate
        register_map_monitor.register_map_link_monitor.GBT_ALIGNMENT_DONE(i)            <= lane_monitor(i).gbt.frame_locked        ;
        register_map_monitor.register_map_link_monitor.GBT_RX_IS_HEADER(i)                <= lane_monitor(i).gbt.rx_is_header        ;
        register_map_monitor.register_map_link_monitor.GBT_RX_HEADER_FOUND(i)              <= lane_monitor(i).gbt.rx_header_found      ;
        register_map_monitor.register_map_link_monitor.GBT_ERROR(i)                    <= lane_monitor(i).gbt.error          ;
        register_map_monitor.register_map_link_monitor.GBT_TXRESET_DONE(i)                <= lane_monitor(i).gth.txreset_done        ;
        register_map_monitor.register_map_link_monitor.GBT_RXRESET_DONE(i)                <= lane_monitor(i).gth.rxreset_done        ;
        register_map_monitor.register_map_link_monitor.GBT_TXFSMRESET_DONE(i)              <= lane_monitor(i).gth.txfsmreset_done      ;
        register_map_monitor.register_map_link_monitor.GBT_RXFSMRESET_DONE(i)              <= lane_monitor(i).gth.rxfsmreset_done      ;
        felig_counter_rx_slide(i)                                    <= lane_monitor(i).gbt.rxslide_count      ;
        felig_mon_l1_id(i)                                        <= lane_monitor(i).global.l1a_id        ;
        felig_counter_fc_error(i)                                    <= lane_monitor(i).global.fc_error_count    ;
        felig_counter_freq_rx(i)                                    <= lane_monitor(i).clock.freq_rx_clk      ;
        felig_counter_freq_tx(i)                                    <= lane_monitor(i).clock.freq_tx_clk      ;
        felig_mon_picxo_error(i)                                    <= lane_monitor(i).clock.picxo_error      ;
        felig_mon_picxo_volt(i)                                      <= lane_monitor(i).clock.picxo_volt        ;

        register_map_monitor.register_map_generators.FELIG_MON_COUNTERS(i).SLIDE_COUNT      <= felig_counter_rx_slide(i)                    ;
        register_map_monitor.register_map_generators.FELIG_MON_COUNTERS(i).FC_ERROR_COUNT   <= felig_counter_fc_error(i)                    ;
        register_map_monitor.register_map_generators.FELIG_MON_FREQ(i).RX                   <= felig_counter_freq_rx(i)                     ;
        register_map_monitor.register_map_generators.FELIG_MON_FREQ(i).TX                   <= felig_counter_freq_tx(i)                     ;
        register_map_monitor.register_map_generators.FELIG_MON_PICXO(i).VLOT                <= felig_mon_picxo_volt(i)                      ;
        register_map_monitor.register_map_generators.FELIG_MON_PICXO(i).ERROR               <= felig_mon_picxo_error(i)                     ;
        register_map_monitor.register_map_generators.FELIG_MON_L1A_ID(i)                    <= felig_mon_l1_id(i)                           ;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).FMT                 <= lane_monitor(i).global.ttc_mon.FMT           ;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).LEN                 <= lane_monitor(i).global.ttc_mon.LEN           ;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).reserved0           <= lane_monitor(i).global.ttc_mon.reserved0     ;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).BCID                <= lane_monitor(i).global.ttc_mon.BCID          ;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).XL1ID               <= lane_monitor(i).global.ttc_mon.XL1ID         ;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_0(i).L1ID                <= lane_monitor(i).global.ttc_mon.L1ID          ;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_1(i).orbit               <= lane_monitor(i).global.ttc_mon.orbit         ;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_1(i).trigger_type        <= lane_monitor(i).global.ttc_mon.trigger_type  ;
        register_map_monitor.register_map_generators.FELIG_MON_TTC_1(i).reserved1           <= lane_monitor(i).global.ttc_mon.reserved1     ;
    end generate GEN_LANE_MONITOR_MAP;

--  GEN_ILA: if ILA = 1 generate
--     signal ila_enable          : std_logic_vector(111 downto 0);
--     signal ila_REG_ENABLE      : std_logic_vector( 39 downto 0);
--     signal ila_REG_RESET       : std_logic_vector(  6 downto 0);
--     signal ila_REG_SW_BUSY     : std_logic_vector(  6 downto 0);
--     signal ila_REG_DATA_FORMAT : std_logic_vector( 13 downto 0);
--     signal ila_REG_PATTERN_SEL : std_logic_vector(  6 downto 0);
--     signal ila_REG_ENDIAN      : std_logic_vector(  6 downto 0);
--     signal ila_REG_INWIDTH     : std_logic_vector(  6 downto 0);
--     signal ila_REG_OUTWIDTH    : std_logic_vector( 20 downto 0);
--     signal ila_clock           : std_logic;

--     COMPONENT ila_REG IS
--        PORT (
--            clk : IN STD_LOGIC;
--            probe0 : IN STD_LOGIC_VECTOR(111 DOWNTO 0);
--            probe1 : IN STD_LOGIC_VECTOR(39 DOWNTO 0);
--            probe2 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
--            probe3 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
--            probe4 : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
--            probe5 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
--            probe6 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
--            probe7 : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
--            probe8 : IN STD_LOGIC_VECTOR(20 DOWNTO 0)
--            );
--        END COMPONENT;

--  begin
--     ila_enable          <= felig_elink_enable(0);
--     ila_REG_ENABLE      <= register_map_control.FELIG_ELINK_ENABLE(0);
--     ila_REG_RESET       <= register_map_control.FELIG_DATA_GEN_CONFIG(0).RESET;
--     ila_REG_SW_BUSY     <= register_map_control.FELIG_DATA_GEN_CONFIG(0).SW_BUSY;
--     ila_REG_DATA_FORMAT <= register_map_control.FELIG_DATA_GEN_CONFIG(0).DATA_FORMAT;
--     ila_REG_PATTERN_SEL <= register_map_control.FELIG_DATA_GEN_CONFIG(0).PATTERN_SEL;
--     ila_REG_ENDIAN      <= register_map_control.FELIG_ELINK_CONFIG(0).ENDIAN_MOD;
--     ila_REG_INWIDTH     <= register_map_control.FELIG_ELINK_CONFIG(0).INPUT_WIDTH;
--     ila_REG_OUTWIDTH    <= register_map_control.FELIG_ELINK_CONFIG(0).OUTPUT_WIDTH;
--     ila_clock           <= clk;

--     ila_REGISTER: ila_REG
--         port map(
--             clk    => ila_clock,
--             probe0 => ila_enable,
--             probe1 => ila_REG_ENABLE,
--             probe2 => ila_REG_RESET,
--             probe3 => ila_REG_SW_BUSY,
--             probe4 => ila_REG_DATA_FORMAT,
--             probe5 => ila_REG_PATTERN_SEL,
--             probe6 => ila_REG_ENDIAN,
--             probe7 => ila_REG_INWIDTH,
--             probe8 => ila_REG_OUTWIDTH
--               );
--  end generate GEN_ILA;

end RTL;
