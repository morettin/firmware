

library ieee, UNISIM;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_unsigned.all;-- @suppress "Deprecated package"
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.I2C.all;
    use work.FELIX_package.all;

entity housekeeping_module_FELIG is
    generic(
        CARD_TYPE                       : integer := 710;
        OPTO_TRX                        : integer := 2;
        GBT_NUM                         : integer := 24;
        ENDPOINTS                       : integer := 1; --Number of PCIe Endpoints in the design, GBT_NUM has to be multiplied by this number in some cases.
        generateTTCemu                  : boolean := false;
        AUTOMATIC_CLOCK_SWITCH          : boolean := false;
        FIRMWARE_MODE                   : integer := 0;
        USE_Si5324_RefCLK               : boolean := false;
        --MT added: =1 to use RXUSRCLK scaled down to 40 MHZ for TX GTREFCLK, =0 to use xtal_40
        --USE_LMK_RXUSRCLK                : boolean := false; --RL: commented because its never used with the addition of protocol
        GENERATE_XOFF                   : boolean := true; -- FromHost Xoff transmission enabled on Full mode busy
        IncludeDecodingEpath2_HDLC      : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath2_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath4_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath8_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath16_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only
        IncludeEncodingEpath2_HDLC      : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath2_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath4_8b10b     : std_logic_vector(4 downto 0) := "00000";
        IncludeEncodingEpath8_8b10b     : std_logic_vector(4 downto 0) := "00000";
        BLOCKSIZE                       : integer := 1024;
        CHUNK_TRAILER_32B               : boolean := false;
        FULL_HALFRATE                   : boolean := false);
    port (
        MMCM_Locked_in              : in     std_logic;
        MMCM_OscSelect_in           : in     std_logic;
        SCL                         : inout  std_logic;
        SDA                         : inout  std_logic;
        SI5345_A                    : out    std_logic_vector(1 downto 0);
        SI5345_INSEL                : out    std_logic_vector(1 downto 0);
        SI5345_OE                   : out    std_logic;
        SI5345_RSTN                 : out    std_logic;
        SI5345_SEL                  : out    std_logic;
        SI5345_nLOL                 : in     std_logic;
        appreg_clk                  : in     std_logic;
        emcclk                      : in     std_logic;
        flash_SEL                   : out    std_logic;
        flash_a                     : out    std_logic_vector(24 downto 0);
        flash_a_msb                 : inout  std_logic_vector(1 downto 0);
        flash_adv                   : out    std_logic;
        flash_cclk                  : out    std_logic;
        flash_ce                    : out    std_logic;
        flash_d                     : inout  std_logic_vector(15 downto 0);
        flash_re                    : out    std_logic;
        flash_we                    : out    std_logic;
        i2cmux_rst                  : out    std_logic;
        TACH                        : in     std_logic;
        clk10_xtal                  : in     std_logic;
        clk40_xtal                  : in     std_logic;
        clk40                       : in     std_logic; --MT added
        clk40_rxusrclk              : in     std_logic; --MT added RXUSRCLK scaled down to 40 MHZ
        RESET_TO_LMK                : in     std_logic; --MT added
        leds                        : out    std_logic_vector(7 downto 0);
        opto_inhibit                : out    std_logic_vector(OPTO_TRX-1 downto 0);
        --opto_los                    : in     std_logic_vector(OPTO_TRX-1 downto 0);--not commented in felig
        register_map_control        : in     register_map_control_type;
        register_map_gen_board_info : out    register_map_gen_board_info_type;
        register_map_hk_monitor     : out    register_map_hk_monitor_type;
        rst_soft                    : in     std_logic;
        sys_reset_n                 : in     std_logic;
        rst_hw                      : in std_logic;
        CLK40_FPGA2LMK_P            : out std_logic;
        CLK40_FPGA2LMK_N            : out std_logic;
        LMK_DATA                    : out std_logic;
        LMK_CLK                     : out std_logic;
        LMK_LE                      : out std_logic;
        LMK_GOE                     : out std_logic;
        LMK_LD                      : in std_logic;
        LMK_SYNCn                   : out std_logic;
        I2C_SMB                   : out    std_logic;
        I2C_SMBUS_CFG_nEN         : out    std_logic;
        MGMT_PORT_EN              : out    std_logic;
        PCIE_PERSTn1              : out    std_logic;
        PCIE_PERSTn2              : out    std_logic;
        PEX_PERSTn                : out    std_logic;
        PEX_SCL                   : out    std_logic;
        PEX_SDA                   : inout  std_logic;
        PORT_GOOD                 : in     std_logic_vector(7 downto 0);
        SHPC_INT                  : out    std_logic;
        lnk_up                    : in     std_logic_vector(1 downto 0);
        RXUSRCLK_IN               : in     std_logic_vector((GBT_NUM*ENDPOINTS)-1 downto 0);
        versal_sys_reset_n_out    : out    std_logic
    );
end entity housekeeping_module_FELIG;


architecture structure of housekeeping_module_FELIG is

    signal RST                            : std_logic;
    signal nReset                         : std_logic;
    signal clk                            : std_logic;
    signal cmd_ack                        : std_logic;
    --signal ack_out                        : std_logic; --not commented in felig
    signal Dout                           : std_logic_vector(7 downto 0);
    signal Din                            : std_logic_vector(7 downto 0);
    signal ack_in                         : std_logic;
    signal write                          : std_logic;
    signal read                           : std_logic;
    signal stop                           : std_logic;
    signal start                          : std_logic;
    signal ena                            : std_logic;
    signal reset                          : std_logic;
    signal AUTOMATIC_CLOCK_SWITCH_ENABLED : std_logic_vector(0 downto 0);
    signal TACH_CNT                       : std_logic_vector(19 downto 0);
    signal TACH_CNT_LATCHED               : std_logic_vector(19 downto 0);
    signal TACH_FLAG                      : std_logic:='0';
    signal TACH_R                         : std_logic:='0';
    signal TACH_2R                        : std_logic:='0';
    signal TACH_3R                        : std_logic:='0';

    signal dna_out_data                   : std_logic_vector(95 downto 0); --IG: get the entire output vector data and assign the relevant bits only
    signal LMK_locked                     : std_logic;

    --signal scl_cips_o                     : std_logic := '1';
    --signal sda_cips_o                     : std_logic := '1';
    signal scl_opencores_o                : std_logic := '1';
    signal sda_opencores_o                : std_logic := '1';
    --signal sda_opencores_cips_clk_o       : std_logic := '1';
    --signal scl_opencores_cips_clk_o       : std_logic := '1';
    signal SDA_i                          : std_logic := '1';
    signal SCL_i                          : std_logic := '1';
    signal SDA_o                          : std_logic := '1';
    signal SCL_o                          : std_logic := '1';


begin
    leds <= register_map_control.STATUS_LEDS;
    register_map_hk_monitor.LMK_LOCKED(0) <= LMK_locked;

    i2c0: entity work.simple_i2c
        port map(
            clk     => clk,
            ena     => ena,
            nReset  => nReset,
            clk_cnt => "01100100",
            start   => start,
            stop    => stop,
            read    => read,
            write   => write,
            ack_in  => ack_in,
            Din     => Din,
            cmd_ack => cmd_ack,
            ack_out => open, --ack_out, --not commented in felig
            Dout    => Dout,
            SCL_i    => SCL_i,
            SDA_i    => SDA_i,
            SCL_o    => scl_opencores_o,
            SDA_o    => sda_opencores_o
        );

    SCL_iobuf: IOBUF  -- @suppress "Generic map uses default values. Missing optional actuals: DRIVE, IBUF_LOW_PWR, IOSTANDARD, SLEW"
        port map(
            O => SCL_i,
            IO => SCL,
            I => '0',
            T => SCL_o
        );

    SDA_iobuf: IOBUF  -- @suppress "Generic map uses default values. Missing optional actuals: DRIVE, IBUF_LOW_PWR, IOSTANDARD, SLEW"
        port map(
            O => SDA_i,
            IO => SDA,
            I => '0',
            T => SDA_o
        );

    --RL copied from standart housekeeping module. FELIG is only implemented in FLX712
    --    g_182_i2c: if (CARD_TYPE = 182) generate
    --    begin
    --        xpm_cdc_sda : xpm_cdc_single
    --            generic map(
    --                DEST_SYNC_FF => 2,
    --                INIT_SYNC_FF => 0,
    --                SIM_ASSERT_CHK => 0,
    --                SRC_INPUT_REG => 1
    --            )

    --            port map(
    --                src_clk => clk,
    --                src_in => sda_opencores_o,
    --                dest_clk => pl0_ref_clk,
    --                dest_out => sda_opencores_cips_clk_o
    --            );

    --        xpm_cdc_scl : xpm_cdc_single
    --            generic map(
    --                DEST_SYNC_FF => 2,
    --                INIT_SYNC_FF => 0,
    --                SIM_ASSERT_CHK => 0,
    --                SRC_INPUT_REG => 1
    --            )

    --            port map(
    --                src_clk => clk,
    --                src_in => scl_opencores_o,
    --                dest_clk => pl0_ref_clk,
    --                dest_out => scl_opencores_cips_clk_o
    --            );

    --        process (pl0_ref_clk)
    --        begin
    --            if rising_edge(pl0_ref_clk) then
    --                SDA_o  <=  sda_opencores_cips_clk_o and sda_cips_o;
    --                SCL_o  <=  scl_opencores_cips_clk_o and scl_cips_o;
    --            end if;
    --        end process;

    --    else generate -- g_182_i2c
    process (clk)
    begin
        if rising_edge(clk) then
            SDA_o <= sda_opencores_o;
            SCL_o <= scl_opencores_o;
        end if;
    end process;

    --        scl_cips_o <= '1';
    --        sda_cips_o <= '1';
    --    end generate; -- g_182_i2c

    i2cint0: entity work.i2c_interface
        port map(
            Din                  => Din,
            Dout                 => Dout,
            I2C_RD               => register_map_hk_monitor.I2C_RD,
            I2C_WR               => register_map_hk_monitor.I2C_WR,
            RST                  => RST,
            ack_in               => ack_in,
            --ack_out              => ack_out, --not commented in felig
            appreg_clk           => appreg_clk,
            clk                  => clk,
            cmd_ack              => cmd_ack,
            ena                  => ena,
            nReset               => nReset,
            read                 => read,
            register_map_control => register_map_control,
            rst_soft             => rst_soft,
            start                => start,
            stop                 => stop,
            write                => write);

    const0: entity work.GenericConstantsToRegs
        generic map(
            GBT_NUM => GBT_NUM,
            ENDPOINTS => ENDPOINTS,
            OPTO_TRX => OPTO_TRX,
            generateTTCemu => generateTTCemu,
            AUTOMATIC_CLOCK_SWITCH => AUTOMATIC_CLOCK_SWITCH,
            FIRMWARE_MODE => FIRMWARE_MODE,
            USE_Si5324_RefCLK => USE_Si5324_RefCLK,
            --CREnableFromHost                => CREnableFromHost,
            --includeDirectMode               => includeDirectMode,
            GENERATE_XOFF => GENERATE_XOFF,
            IncludeDecodingEpath2_HDLC => IncludeDecodingEpath2_HDLC,
            IncludeDecodingEpath2_8b10b => IncludeDecodingEpath2_8b10b,
            IncludeDecodingEpath4_8b10b => IncludeDecodingEpath4_8b10b,
            IncludeDecodingEpath8_8b10b => IncludeDecodingEpath8_8b10b,
            IncludeDecodingEpath16_8b10b => IncludeDecodingEpath16_8b10b,
            IncludeEncodingEpath2_HDLC => IncludeEncodingEpath2_HDLC,
            IncludeEncodingEpath2_8b10b => IncludeEncodingEpath2_8b10b,
            IncludeEncodingEpath4_8b10b => IncludeEncodingEpath4_8b10b,
            IncludeEncodingEpath8_8b10b => IncludeEncodingEpath8_8b10b,
            BLOCKSIZE => BLOCKSIZE,
            CHUNK_TRAILER_32B => CHUNK_TRAILER_32B)
        port map(
            AUTOMATIC_CLOCK_SWITCH_ENABLED => AUTOMATIC_CLOCK_SWITCH_ENABLED,
            register_map_gen_board_info    => register_map_gen_board_info);

    g_HTG710: if CARD_TYPE = 710 generate
        i2cmux_rst      <= reset;
    end generate;

    g_notHTG710: if CARD_TYPE /= 710 generate
        i2cmux_rst      <= not reset;
    end generate;

    g_VC709: if CARD_TYPE = 709 generate
        opto_inhibit     <= (others => '0');  -- SFP_TX_DISABLE_H: 1 = DISABLE
    end generate;

    g_notVC709: if CARD_TYPE /= 709 generate
        opto_inhibit    <= (others => '1');  -- MiniPOD_nRST: 0 = RESET
    end generate;

    xadc0: entity work.xadc_drp
        generic map(
            CARD_TYPE => CARD_TYPE)
        port map(
            clk40   => appreg_clk,
            reset   => reset,
            temp    => register_map_hk_monitor.FPGA_CORE_TEMP,
            vccint  => register_map_hk_monitor.FPGA_CORE_VCCINT,
            vccaux  => register_map_hk_monitor.FPGA_CORE_VCCAUX,
            vccbram => register_map_hk_monitor.FPGA_CORE_VCCBRAM);

    dna0: entity work.dna
        generic map(
            CARD_TYPE => CARD_TYPE)
        port map(
            clk40   => appreg_clk,
            reset   => reset,
            --IG      dna_out(63 downto 0) => register_map_hk_monitor.FPGA_DNA,
            --IG      dna_out(95 downto 64) => open);
            dna_out => dna_out_data);
    register_map_hk_monitor.FPGA_DNA  <= dna_out_data(63 downto 0);

    u1: entity work.flash_wrapper
        port map(
            rst                  => RST,
            clk                  => emcclk,
            flash_a_msb          => flash_a_msb,
            flash_a              => flash_a,
            flash_d              => flash_d,
            flash_adv            => flash_adv,
            flash_cclk           => flash_cclk,
            flash_ce             => flash_ce,
            flash_we             => flash_we,
            flash_re             => flash_re,
            register_map_control => register_map_control,
            config_flash_rd      => register_map_hk_monitor.CONFIG_FLASH_RD,
            flash_SEL            => flash_SEL);

    g_180: if (CARD_TYPE = 180) generate
        component cips_bd_wrapper
            port (
                pl0_resetn : out STD_LOGIC
            );
        end component;
    begin
        versal_cips_block0: cips_bd_wrapper
            port map(
                pl0_resetn => versal_sys_reset_n_out
            );
    end generate g_180;

    g_711_712: if (CARD_TYPE = 711) or (CARD_TYPE = 712) generate

        LMK_gbt: if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT generate --RL
            lmk_init0: entity work.LMK03200_wrapper
                generic map(
                    freq => 240)
                port map(
                    rst_lmk          => RESET_TO_LMK, --added MT/SS from LMK_RESET_process
                    hw_rst           => rst_hw,
                    LMK_locked       => LMK_locked,--u1_LMK_locked(0),
                    CLK40_FPGA2LMK_P => open, --moved to link wrapper
                    CLK40_FPGA2LMK_N => open, --moved to link wrapper
                    LMK_DATA         => LMK_DATA,
                    LMK_CLK          => LMK_CLK,
                    LMK_LE           => LMK_LE,
                    LMK_GOE          => LMK_GOE,
                    LMK_LD           => LMK_LD,
                    LMK_SYNCn        => LMK_SYNCn,
                    clk40m_in        => clk40_xtal,
                    clk10m_in        => clk10_xtal);
        end generate LMK_gbt;

        LMK_lpgbt: if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT generate --RL
            --MT added
            --REC: if USE_LMK_RXUSRCLK=true generate --RL: commented because its never used with the addition of protocol
            lmk_init0_RXUSRCLK: entity work.LMK03200_wrapper
                generic map(
                    freq => 240)
                port map(
                    rst_lmk => RESET_TO_LMK,--'0',
                    hw_rst => rst_hw,
                    LMK_locked => LMK_locked,
                    clk40m_in => clk40_xtal,--clk40_rxusrclk,
                    clk10m_in => clk10_xtal,
                    CLK40_FPGA2LMK_P => open, --moved to link wrapper
                    CLK40_FPGA2LMK_N => open, --moved to link wrapper
                    LMK_DATA => LMK_DATA,
                    LMK_CLK => LMK_CLK,
                    LMK_LE => LMK_LE,
                    LMK_GOE => LMK_GOE,
                    LMK_LD => LMK_LD,
                    LMK_SYNCn => LMK_SYNCn);
        end generate LMK_lpgbt;

        pex_init0: entity work.pex_init
            generic map(
                CARD_TYPE => CARD_TYPE)
            port map(
                I2C_SMB           => I2C_SMB,
                I2C_SMBUS_CFG_nEN => I2C_SMBUS_CFG_nEN,
                MGMT_PORT_EN      => MGMT_PORT_EN,
                PCIE_PERSTn1      => PCIE_PERSTn1,
                PCIE_PERSTn2      => PCIE_PERSTn2,
                PEX_PERSTn        => PEX_PERSTn,
                PEX_SCL           => PEX_SCL,
                PEX_SDA           => PEX_SDA,
                PORT_GOOD         => PORT_GOOD,
                SHPC_INT          => SHPC_INT,
                clk40             => clk40_xtal,
                lnk_up0           => lnk_up(0),
                lnk_up1           => lnk_up(1),
                --reset_pcie        => open,
                sys_reset_n       => sys_reset_n);

    end generate;

    freqmeter0: entity work.gc_multichannel_frequency_meter
        generic map(
            g_WITH_INTERNAL_TIMEBASE => true, --: boolean := true;
            g_CLK_SYS_FREQ           => 25000000, --: integer;
            g_COUNTER_BITS           => 32, --: integer := 32;
            g_CHANNELS               => GBT_NUM*ENDPOINTS)--: integer := 1)

        port map(
            clk_sys_i     => appreg_clk,--: in  std_logic;
            clk_in_i      => RXUSRCLK_IN,--: in  std_logic_vector(g_CHANNELS -1 downto 0);
            rst_n_i       => '1',--: in  std_logic;
            pps_p1_i      => '0',--: in  std_logic;
            channel_sel_i => register_map_control.RXUSRCLK_FREQ.CHANNEL,--: in  std_logic_vector(5 downto 0);
            freq_o        => register_map_hk_monitor.RXUSRCLK_FREQ.VAL,
            freq_valid_o  => register_map_hk_monitor.RXUSRCLK_FREQ.VALID(38)
        );

    reset <= rst_soft or RST;

    RST <= not sys_reset_n;

    --  type bitfield_hk_ctrl_fmc_type is record
    --    SI5345_INSEL                   : std_logic_vector(6 downto 5);    -- Selects the input clock source
    --    SI5345_A                       : std_logic_vector(4 downto 3);    -- Si5345 address select
    --    SI5345_OE                      : std_logic_vector(2 downto 2);    -- Si5345 output enable
    --    SI5345_RSTN                    : std_logic_vector(1 downto 1);    -- Si5345 reset,
    --    SI5345_SEL                     : std_logic_vector(0 downto 0);    -- Si5345 i2c select bit
    --  end record;

    SI5345_INSEL(1 downto 0) <= register_map_control.HK_CTRL_FMC.SI5345_INSEL(6 downto 5);
    SI5345_A(1 downto 0)     <= register_map_control.HK_CTRL_FMC.SI5345_A(4 downto 3);
    SI5345_OE                <= register_map_control.HK_CTRL_FMC.SI5345_OE(2);
    SI5345_RSTN              <= register_map_control.HK_CTRL_FMC.SI5345_RSTN(1);
    SI5345_SEL               <= register_map_control.HK_CTRL_FMC.SI5345_SEL(0);

    --Removed in RM 4.0
    --register_map_hk_monitor.MMCM_MAIN.AUTOMATIC_CLOCK_SWITCH_ENABLED <= AUTOMATIC_CLOCK_SWITCH_ENABLED;
    --register_map_hk_monitor.MMCM_MAIN.OSC_SEL <= (others => MMCM_OscSelect_in);

    --Main MMCM Oscillator Input
    --2: LCLK fixed
    --1: TTC fixed
    --0: selectable
    process(MMCM_OscSelect_in, AUTOMATIC_CLOCK_SWITCH_ENABLED)
    begin
        if(AUTOMATIC_CLOCK_SWITCH_ENABLED = "0") then
            register_map_hk_monitor.MMCM_MAIN.MAIN_INPUT<= "00";
        elsif(MMCM_OscSelect_in = '1') then --TTC Clock
            register_map_hk_monitor.MMCM_MAIN.MAIN_INPUT<= "01";
        else
            register_map_hk_monitor.MMCM_MAIN.MAIN_INPUT<= "10";
        end if;
    end process;

    register_map_hk_monitor.MMCM_MAIN.PLL_LOCK <= (others => MMCM_Locked_in);


    register_map_hk_monitor.HK_CTRL_FMC.SI5345_LOL <= (others=> (not SI5345_nLOL));


    -- for fan speed monitoring, latest FLX712
    -- counter how many cycles for one fan revolution

    process(clk10_xtal)
    begin
        if clk10_xtal'event and clk10_xtal='1' then
            TACH_R <= TACH;
            TACH_2R <= TACH_R;
            TACH_3R <= TACH_2R;
            if TACH_2R='1' and TACH_3R='0' and TACH_FLAG='0' then
                TACH_CNT <= x"00000";
                TACH_CNT_LATCHED <= TACH_CNT;
                TACH_FLAG <= not TACH_FLAG;
            elsif TACH_2R='1' and TACH_3R='0' and TACH_FLAG='1' then
                TACH_CNT <= TACH_CNT + '1';
                TACH_FLAG <= not TACH_FLAG;
                TACH_CNT_LATCHED <= TACH_CNT_LATCHED;
            else
                TACH_CNT <= TACH_CNT + '1';
                TACH_FLAG <= TACH_FLAG;
                TACH_CNT_LATCHED <= TACH_CNT_LATCHED;
            end if;
        end if;
    end process;

    register_map_hk_monitor.TACH_CNT <= TACH_CNT_LATCHED; -- 20 bits counter



end architecture structure ; -- of housekeeping_module
