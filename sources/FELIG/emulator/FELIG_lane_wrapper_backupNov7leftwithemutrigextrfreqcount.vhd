--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	John Anderson, Michael Oberling, Soo Ryu
--
-- Design Name:	FELIG_lane_wrapper
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Coming soon.
--
-- Change Log:	V1.0 - 
--
--==============================================================================

--=============================================================================================================
--	The FELIG Lane Wrapper ties together all FIFO, GBT and data generation associated with one lane of a quad.
--
--	Overall system architecture of this block is
--
--											+---------------+						
-- 			+-------+		+-----------+	|	+-------+	|	+-----------+		
--			|		|		|			|	|	|		|	|	|			|		
--  GTH RX =+		+------>+ GBT RX	+-->+--->+		+	+-->+	L1A		|		
--			|		|		|			|		|		|		|	decode	|		
-- 			|		|		+-----------+		|		|		+-----------+		
--			|LB FIFO|							|LB FIFO|				|			
-- 			|		|		+-----------+		|		|		+-------V-------+	
--			|		|		|			|		|		|		|	data gen	|	
--  GTH TX =+		+<------+ GBT TX	+<------+		+<------+				|	
--			|		|		|			|		|		|		|	(emulator)	|	
-- 			+-------+		+-----------+		+-------+		+---------------+	
--=============================================================================================================
LIBRARY IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;
--use ieee.numeric_std.all;               

library UNISIM;
use UNISIM.VComponents.all;

use work.type_lib.ALL;
use work.ip_lib.ALL;
use work.function_lib.ALL;

use work.pcie_package.all;
use work.centralRouter_package.all;

entity FELIG_lane_wrapper is
generic ( 
	GEN_ILA				: boolean	:= false;
	useGBTdataEmulator	: boolean	:= false;
	sim_emulator		: boolean	:= false;
	LANE_ID				: integer	:= 0
);
port (
	--================================
	-- Clocks & Reset
	--================================
	clk_xtal_40			: in	std_logic;						-- BUGBUG: check clock usage
	lane_rxclk_240		: in	std_logic;						-- BUFR/BUFG copy of the RXOUTCLK
	lane_txclk_240		: in	std_logic;						-- From TX clk MMCM. Used to generate data back to the GTH (GBT only)
	
	rx_clk_div_2_mon	: out	std_logic;
	tx_clk_div_2_mon	: out	std_logic;
	
	--================================
	-- PICXO Interface
	--================================
	picxo_reset			: in	std_logic;

	--================================
	-- GTX Interface
	--================================
        --MT remove the optional choices of driving the tx_data with frame_gen
        --and loopback. If you want to put it back do it outse
        --felig_lane_wrapper, in the top file
--	gth_rx_data_20b		: in	std_logic_vector(19 downto 0);	-- data received from the GTH
--MT gbtTxRx is now in _KCU
--	gth_tx_data_20b		: out	std_logic_vector(19 downto 0);	-- data driven to the GTH
--does this collide with something else?
--MT commented
--	gth_rxslide			: out	std_logic;						--
--	gth_rxreset			: out	std_logic;						--
--MT commented out
--	gth_rxresetdone		: in	std_logic;						-- 
--	gth_rxfsmresetdone	: in	std_logic;						-- 
	gth_rxusrclk2		: in	std_logic;
--MT commented out
--	gth_txresetdone		: in	std_logic;						-- 
--MT commented out
--	gth_txfsmresetdone	: in	std_logic;						-- 
--	gth_txpippmstepsize	: out	std_logic_vector(4 downto 0);
	gth_txusrclk2		: in	std_logic;						--

---MT commented out
--	gth_txpolarity		: out	std_logic;
--	gth_rxpolarity		: out	std_logic;
--MT commented out
--	gth_txpippmen		: out	std_logic;

--	--================================
--	-- L1A Interface
--	--================================
        --internally generated trigger l1a and id. Generation in gt_core_exdes
	l1a_int_trigger		: in	std_logic;
	l1a_int_int_id			: in	std_logic_vector(15 downto 0);
	l1a_trigger_out		: out	std_logic;
	
	--================================
	-- Control and Status Interface
	--================================
	lane_control		: in	lane_control_type;
	lane_monitor		: out	lane_monitor_type;
        --Frans 1
	SELECT_RANDOM : in std_logic_vector(0 downto 0);
        --MT 2 (Fran 2)
        FMEMU_RANDOM_RAM_ADDR          : in std_logic_vector(9 downto 0);    -- Controls the address of the ramblock for the random number generator
        FMEMU_RANDOM_RAM               : in bitfield_fmemu_random_ram_t_type;
        FMEMU_RANDOM_CONTROL           : in bitfield_fmemu_random_control_w_type;

        --MT added
        gbt_frame_locked_in : in std_logic
--        fc_rx_slide_out : out std_logic
        
        
);
end entity FELIG_lane_wrapper;

architecture Behavioral of FELIG_lane_wrapper is
signal emu_data_120b				: std_logic_vector(119 downto 0)	:= (others => '0');
signal ext_l1a_id					: std_logic_vector( 15 downto 0)	:= (others => '0');
signal ext_l1a_pipe					: std_logic_vector(  3 downto 0)	:= (others => '0');
signal ext_l1a_select				: std_logic							:= '0';
signal ext_l1a_trigger				: std_logic							:= '0';
signal ext_l1id						: std_logic							:= '0';
--MT
--signal fc_error_bit					: std_logic							:= '0';
--signal fc_error_count				: std_logic_vector(  7 downto 0)	:= (others => '0');
--signal fc_error_frame				: std_logic							:= '0';
--signal fc_error_seed				: std_logic							:= '0';
--signal fc_error_word				: std_logic							:= '0';
--signal fc_rx_auto_reset				: std_logic							:= '0';
--signal fc_rx_data					: std_logic_vector( 15 downto 0)	:= (others => '0');
--signal fc_rx_slide					: std_logic							:= '0';
--signal fg_data_select				: std_logic							:= '0';
--signal fg_reset						: std_logic							:= '0';
--signal fg_tx_data					: std_logic_vector( 19 downto 0)	:= (others => '0');
signal freq_rx_clk					: std_logic_vector( 31 downto 0)	:= (others => '0');
signal freq_tx_clk					: std_logic_vector( 31 downto 0)	:= (others => '0');
--MT
--signal gbt_alignment_chk_rst		: std_logic							:= '0';
--signal gbt_error					: std_logic							:= '0';
signal gbt_extracted_bchan			: std_logic							:= '0';
signal gbt_extractedl1a				: std_logic							:= '0';
signal l1a_int_trigger_pipe		: std_logic_vector(  3 downto 0)	:= (others => '0');
signal l1a_int_trigger_piped		: std_logic							:= '0';
--MT
signal gbt_frame_locked				: std_logic							:= '0';
signal gbt_frame_locked_a			: std_logic_vector( 47 downto 0)	:= (others => '0');
signal gbt_frame_locked_c			: std_logic							:= '0';
--MT
--signal gbt_gth_rxrst				: std_logic							:= '0';
--signal gbt_lb_enable				: std_logic							:= '0';
--signal gbt_lb_fifo_almost_empty		: std_logic							:= '0';
--signal gbt_lb_fifo_dout				: std_logic_vector(119 downto 0)	:= (others => '0');
--signal gbt_lb_fifo_empty			: std_logic							:= '0';
--signal gbt_lb_fifo_full				: std_logic							:= '0';
--signal gbt_lb_fifo_prog_empty		: std_logic							:= '0';
--signal gbt_lb_fifo_rd_en			: std_logic							:= '0';
signal gbt_rx_data_120b				: std_logic_vector(119 downto 0)	:= (others => '0');
--MT commented out
--signal gbt_rx_data_format			: std_logic_vector(  1 downto 0)		:= (others => '0');
signal gbt_rx_flag					: std_logic							:= '0';
--MT commented out
--signal gbt_rx_header_found			: std_logic							:= '0';
--signal gbt_rx_is_header				: std_logic							:= '0';
--signal gbt_rx_reset					: std_logic							:= '0';
--signal gbt_rx_slide					: std_logic							:= '0';
--MT commented out
--signal gbt_tc_edge					: std_logic							:= '0';
signal gbt_tx_data_120b				: std_logic_vector(119 downto 0)	:= (others => '0');
--MT commented out
--signal gbt_tx_data_20b				: std_logic_vector( 19 downto 0)	:= (others => '0');
--MT commented out
--signal gbt_tx_data_format			: std_logic_vector(  1 downto 0)	:= (others => '0');
signal gbt_tx_flag					: std_logic							:= '0';
--MT commented out
--signal gbt_tx_reset					: std_logic							:= '0';
--signal gbt_tx_tc_dly_value			: std_logic_vector(  3 downto 0)	:= (others => '0');
--signal gbt_tx_tc_method				: std_logic							:= '0';
--signal gbtbit						: integer							:= 0;
signal gbtbitsel					: std_logic_vector(  6 downto 0)	:= (others => '0');
signal b_ch_bit_sel					: std_logic_vector(  6 downto 0)	:= (others => '0');
--signal gbt_emu_data_select			: std_logic							:= '0';
--signal gth_lb_enable				: std_logic							:= '0';
--signal gth_lb_fifo_almost_empty		: std_logic							:= '0';
--signal gth_lb_fifo_dout				: std_logic_vector( 19 downto 0)	:= (others => '0');
--signal gth_lb_fifo_empty			: std_logic							:= '0';
--signal gth_lb_fifo_full				: std_logic							:= '0';
--signal gth_lb_fifo_prog_empty		: std_logic							:= '0';
--signal gth_lb_fifo_rd_en			: std_logic							:= '0';
--signal gth_lb_fifo_wr_en			: std_logic							:= '1';
--MT commented out
--signal gth_rxcdrlock				: std_logic							:= '1';
--signal gth_rxreset_count			: std_logic_vector(  1 downto 0)		:= (others => '0');
--MT commented out
--signal gth_rxresetdone_i			: std_logic							:= '0';
--signal gth_tx_pi_hold				: std_logic							:= '0';
--MT commented out
--signal gth_txresetdone_i			: std_logic							:= '0';
signal l1a_id						: std_logic_vector( 15 downto 0)	:= (others => '0');
signal l1a_trigger					: std_logic							:= '0';
signal l1id							: std_logic_vector( 15 downto 0)	:= (others => '0');
signal l1a_ext_int_id							: std_logic_vector( 15 downto 0)	:= (others => '0'); --MT
signal l1a_ext_int_id_extra							: std_logic_vector( 31 downto 0)	:= (others => '0'); --MT
signal lane_reset					: std_logic							:= '0';
--MT
--signal lb_fifo_prog_empty_thresh	: std_logic_vector(  4 downto 0)	:= (others => '0');
--signal lb_fifo_rst					: std_logic							:= '0';
--signal manual_gth_rxreset			: std_logic							:= '0';
--signal picxo_error					: std_logic_vector( 20 downto 0)	:= (others => '0');
--signal picxo_offset_en				: std_logic							:= '0';
--signal picxo_offset_ppm				: std_logic_vector( 21 downto 0)	:= (others => '0');
--signal picxo_volt					: std_logic_vector( 21 downto 0)	:= (others => '0');
--signal rst_hw						: std_logic							:= '0';
--signal rst_hw_pipe					: std_logic_vector(  3 downto 0)	:= (others => '0');
--signal rx_data						: std_logic_vector( 19 downto 0)	:= (others => '0');
--signal rx_latopt_des				: std_logic							:= '0';
--signal rx_slide_sel					: std_logic							:= '0';
--signal rxslide_count				: std_logic_vector( 31 downto 0)	:= (others => '0');
--signal rxslide_count_reset			: std_logic							:= '0';
--signal rxslide_count_unused			: std_logic_vector( 47 downto 32)	:= (others => '0');
signal tx_120b_from_mach			: std_logic_vector(119 downto  0)	:= (others => '0');
--signal tx_120b_from_ram				: std_logic_vector(119 downto  0)	:= (others => '0');
--signal tx_data						: std_logic_vector( 19 downto  0)	:= (others => '0');
signal chB_l1a_id					: std_logic_vector( 23 downto  0)	:= (others => '0');
signal chB_busy						: std_logic							:= '0';
--signal xoff							: std_logic							:= '0';
--signal l1a_trigger_lat				: std_logic							:= '0';
signal elink_sync					: std_logic							:= '0';

signal emu_data_re					: std_logic_vector(0 to  4);
signal emu_data_out					: array_of_slv_9_0(0 to  4);
signal elink_data_in				: array_of_slv_9_0(0 to 39);
signal elink_data_re				: std_logic_vector(0 to 39) ;

signal elink_control				: lane_elink_control_array(0 to 39);
signal emu_control					: lane_emulator_control_array(4 downto 0);
constant epath						: std_logic_vector(4 downto 0) := (others => '0');
signal TTC_out						: std_logic_vector(9 downto 0) := (others => '0');
signal TTC_ToHost_Data				: TTC_ToHost_data_type;


signal ila_data_gen_out			: array_of_slv_17_0	(0 to 4);
signal ila_fifo_out				: array_of_slv_9_0	(0 to 4);
signal ila_data_gen_we			: std_logic_vector	(0 to 4);
signal ila_gbt_word_gen_state	: std_logic_vector	(2 downto 0);
--MT
signal ila_count_chk_out        : array_of_slv_10_0_MT(0 to 4);
signal ila_count_chk_out2        : array_of_slv_10_0_MT(0 to 4);
signal ila_count_upstfifochk     : array_of_slv_10_0_MT(0 to 4);
signal ila_fifo_flush            : std_logic_vector(0 to 4);
signal ila_isEOP_chk2 : std_logic_vector(0 to 4);
signal ila_efifoDout_8b10b      : array_of_slv_9_0(0 to 4);
signal ila_enc10bitRdy          : std_logic_vector(0 to 4);
signal ila_efifoFull : std_logic_vector      (0 to 4);
signal ila_efifoEmpty : std_logic_vector      (0 to 4);
signal ila_efifoPfull : std_logic_vector      (0 to 4);
signal ila_elink_data_re : std_logic_vector      (0 to 4);
signal ila_efifoDoutRdy : std_logic_vector      (0 to 4);
signal chunk_length_ila : array_of_slv_11_0_MT(0 to 4);
--

--MT  checker gbt word
signal ila_count_pyld : std_logic_vector(8 downto 0) := (others => '0');

signal ila_testpass_gwchk : std_logic_vector(3 downto 0) := "0000";
signal ila_dist_gwchk : std_logic_vector(3 downto 0) := "0000";
signal ila_word10b_gwchk : std_logic_vector(9 downto 0) := (others => '0');

signal ila_phlck_gwchk : std_logic := '0';  --decide the 2b phase in the 10b word
signal ila_en_gwchk : std_logic := '0';
signal ila_count_gwchk : std_logic_vector(2 downto 0) := "000";
signal ila_count_start_gwchk : std_logic_vector(1 downto 0) := "00";
--
begin
--MT commented out for now
        --sim_gen_check_ila : if sim_emulator = false generate
	--ch_gen_check_ila : if GEN_ILA = true generate
	--comp_lane_ila : component lane_ila
	--port map (
	--	clk			=> lane_txclk_240,
	--	probe0		=> gbt_tx_data_120b,
	--	probe1		=> emu_data_out(3),
	--	probe2 		=> ila_fifo_out(3),
	--	probe3	 	=> ila_data_gen_out(3),
	--	probe4		=> gbt_lb_fifo_dout,
	--	probe5(0)	=> ila_data_gen_we(0),
	--	probe5(1)	=> l1a_int_trigger_piped,
	--	probe5(2)	=> ext_l1a_trigger,
	--	probe5(3)	=> gbt_rx_flag,
	--	probe5(4)	=> gbt_tx_flag,
	--	probe5(5)	=> gbt_extractedl1a,
	--	probe5(6)	=> gbt_extracted_bchan,
	--	probe5(7)	=> gbt_frame_locked_c,
	--	probe6		=> emu_data_re,
	--	probe7		=> l1a_id,
	--	probe8		=> ila_gbt_word_gen_state,
	--	probe9      => ila_count_chk_out(0),  --trigger with
        --                                              --ila_data_gen_out = X10000
	--	Probe10     => ila_count_upstfifochk(0),
	--	probe11     => ila_count_upstfifochk(1),
	--	probe12     => ila_count_upstfifochk(2),
	--	probe13     => ila_count_upstfifochk(3),
	--	probe14     => ila_count_upstfifochk(4),
        --        probe15(0)     => ila_fifo_flush(0),
        --        probe16(0)     => ila_fifo_flush(1),
        --        probe17(0)     => ila_fifo_flush(2),
        --        probe18(0)     => ila_fifo_flush(3),
        --        probe19(0)     => ila_fifo_flush(4),
        --        probe20        => ila_efifoDout_8b10b(0),
        --        probe21(0)        => ila_enc10bitRdy(0),
        --        probe22(0) => '0',
        --        probe22(1) => ila_efifoFull(3),
        --        probe22(2) => ila_efifoEmpty(3),
        --        probe22(3) => ila_efifoPfull(3),
        --        probe22(4) => ila_elink_data_re(3),
        --        probe22(5) => ila_efifoDoutRdy(3),
        --        probe22(6) => ila_data_gen_we(3),
	--	probe23    => ila_fifo_out(0),
	--	probe24    => ila_fifo_out(1),
	--	probe25    => ila_fifo_out(2),
	--	probe26    => ila_fifo_out(4),
	--	probe27      => ila_count_chk_out(1),
	--	probe28      => ila_count_chk_out(2),
	--	probe29      => ila_count_chk_out(3),
	--	probe30      => ila_count_chk_out(4),
	--	probe31      => ila_count_chk_out2(0),                
	--	probe32      => ila_count_chk_out2(1),
	--	probe33      => ila_count_chk_out2(2),
	--	probe34      => ila_count_chk_out2(3),
	--	probe35      => ila_count_chk_out2(4),
        --        probe36(3 downto 0) => ila_testpass_gwchk,
        --        probe36(7 downto 4) => ila_dist_gwchk,
        --        probe36(8) => ila_phlck_gwchk,
        --        probe36(9) => ila_en_gwchk,
        --        probe36(12 downto 10) => ila_count_gwchk,
        --        probe36(14 downto 13) => ila_count_start_gwchk,                
        --        probe37 => ila_word10b_gwchk,
        --        probe38 => ila_count_pyld,
        --        probe39 => chunk_length_ila(0),
        --        probe40 => chunk_length_ila(1),
        --        probe41 => chunk_length_ila(2),
        --           probe42 => chunk_length_ila(3),
        --           probe43 => chunk_length_ila(4),                
        --        probe44 => SELECT_RANDOM
                
                
                
	--);
	--end generate ch_gen_check_ila;
	--end generate sim_gen_check_ila;

	l1a_trigger_out <= l1a_trigger;
	
	u0_mon: process(lane_rxclk_240)
	begin
		if rising_edge(lane_rxclk_240) then
			if (gbt_rx_flag = '1') then
				 gbt_frame_locked_a(47 downto 1) <= gbt_frame_locked_a(46 downto 0);
				 gbt_frame_locked_a(0) <= gbt_frame_locked;
				 if gbt_frame_locked_a = x"FFFFFFFFFFFF" then
					gbt_frame_locked_c <= '1';
				 else 
					gbt_frame_locked_c <= '0';
				 end if;
			end if;
		end if;
	end process u0_mon;
	
	lane_monitor.gbt.frame_locked						<= gbt_frame_locked_c	;
--MT commented out (already in _KCU)
--	lane_monitor.gbt.rx_is_header						<= gbt_rx_is_header		;

--	lane_monitor.gbt.rx_header_found					<= gbt_rx_header_found	;
--	lane_monitor.gbt.error								<= gbt_error			;
--	lane_monitor.gbt.rxslide_count						<= rxslide_count		;
--	lane_monitor.gth.txreset_done						<= gth_txresetdone		;
--MT commented out
--	lane_monitor.gth.rxreset_done						<= gth_rxresetdone		;
--	gbt_emu_data_select			<= lane_control.global.emu_data_select		;
	ext_l1a_select				<= lane_control.global.l1a_source			;
--	lb_fifo_prog_empty_thresh	<= lane_control.global.loopback_fifo_delay	;
--	lb_fifo_rst					<= lane_control.global.loopback_fifo_reset	;
	gbtbitsel					<= lane_control.global.a_ch_bit_sel			;
	b_ch_bit_sel				<= lane_control.global.b_ch_bit_sel			;

--MT commented out
--	gth_tx_pi_hold				<= lane_control.clock.gth_tx_pi_hold		;
--	picxo_offset_en				<= lane_control.clock.picxo_offset_en		;
--	picxo_offset_ppm			<= lane_control.clock.picxo_offset_ppm		;
	
--	rx_slide_sel				<= lane_control.gbt.rxslide_select			;
--MT commented out
--	gbt_rx_reset				<= lane_control.gbt.rx_reset				;
--	gbt_tc_edge					<= lane_control.gbt.tc_edge					;
--	gbt_tx_reset				<= lane_control.gbt.tx_reset				;
--	gbt_tx_tc_method			<= lane_control.gbt.tx_tc_method			;
--MT commented out
--	gbt_rx_data_format			<= lane_control.gbt.rx_data_format			;
--	gbt_tx_data_format			<= lane_control.gbt.tx_data_format			;
--	gbt_tx_tc_dly_value			<= lane_control.gbt.tx_tc_dly_value			;
--	gbt_lb_enable				<= lane_control.gbt.loopback_enable			;
--	rxslide_count_reset			<= lane_control.gbt.rxslide_count_reset		;
--MT commented out
--	manual_gth_rxreset			<= lane_control.gth.manual_gth_rxreset		;
--	gth_txpolarity				<= lane_control.gth.txpolarity				;
--	gth_rxpolarity				<= lane_control.gth.rxpolarity				;
--	gth_lb_enable				<= lane_control.gth.loopback_enable			;
								
	emu_control					<= lane_control.emulator					;
	
	elink_control				<= lane_control.elink						;
	
--MT commented out
--	gth_txpippmen <= not gth_tx_pi_hold;
	
	--================================================================
	-- Frequency Monitors
	--================================================================
	gen_freq_counter : if sim_emulator = false generate
	freq_counter_lane_rxclk_240: entity work.clock_frequncy_counter
	generic map (
		clk_timebase_frequency		=> 40000000.0,	-- Units: Hz
		accumulation_period			=> 32.0,			-- Units: Seconds  / 32-bit counter allows max 4,294,967,295 counts; @ 1 second can only count to 50,000,000
		clk_meas_prescale_factor	=> 5 -- 4 should be enough, but just to be sure...
		-- clk_meas_prescale divides down the clk_meas frequency
		-- (clk_meas freq) / (2^clk_meas_prescale_factor)  must be less than (clk_timebase freq) / 2
		-- values other than 1 will limit resolution.
		-- Minimum value = 1
	)
	-- Port list
	port map (
		-- stable local clock input
		-- must be at least twice as fast at the clk_meas / clk_meas_prescale
		clk_timebase			=> clk_xtal_40,
		-- clk to measure
		clk_meas				=> lane_rxclk_240,
		-- output count
		frequency				=> freq_rx_clk, -- because prescale and accumulation are the same value, unit here will be in Hz.
		div_2_clock_signal		=> rx_clk_div_2_mon,
		prescaled_clock_signal	=> open
	);
	
	freq_counter_lane_txclk_240: entity work.clock_frequncy_counter
	generic map (
		clk_timebase_frequency		=> 40000000.0,	-- Units: Hz
		accumulation_period			=> 32.0,			-- Units: Seconds  / 32-bit counter allows max 4,294,967,295 counts; @ 1 second can only count to 50,000,000
		clk_meas_prescale_factor	=> 5 -- 4 should be enough, but just to be sure...
		-- clk_meas_prescale divides down the clk_meas frequency
		-- (clk_meas freq) / (2^clk_meas_prescale_factor)  must be less than (clk_timebase freq) / 2
		-- values other than 1 will limit resolution.
		-- Minimum value = 1
	)
	-- Port list
	port map (
		-- stable local clock input
		-- must be at least twice as fast at the clk_meas / clk_meas_prescale
		clk_timebase			=> clk_xtal_40,
		-- clk to measure
		clk_meas				=> lane_txclk_240,
		-- output count
		frequency				=> freq_tx_clk, -- because prescale and accumulation are the same value, unit here will be in Hz.
		div_2_clock_signal		=> tx_clk_div_2_mon,
		prescaled_clock_signal	=> open
	);
	end generate gen_freq_counter;
	
	--================================================================
	-- Resets
	--================================================================
--MT commented resets happening in _KCU
--	gth_rxreset	<= (gbt_gth_rxrst and (not rx_slide_sel)) or manual_gth_rxreset or (fc_rx_auto_reset and rx_slide_sel);
	
	--reset_gbt_proc: process (lane_reset, lane_txclk_240)
	--begin
	--	if (lane_reset = '1') then
	--		rst_hw <= '1';
	--	elsif (lane_txclk_240'event and lane_txclk_240 = '1') then
	--		rst_hw_pipe <= rst_hw_pipe(rst_hw_pipe'high-1 downto 0) & rst_hw;
 	--		if (rst_hw_pipe(rst_hw_pipe'high) = '1') then
 	--			rst_hw <= '0';
	--		end if;
	--	end if;
	--end process reset_gbt_proc;

--MT commented: decided not to use it anymore: instead use recovered and cleaned clock
--as GTREFCLK for TX
	----================================================================
	---- DPLL
	----================================================================
	--gen_picxo : if sim_emulator = false generate
	--picxo: entity work.picxo_top_wrapper_gth_test 
	--port map (
	--	RESET_I		=> picxo_reset,
	--	REF_CLK_I	=> gth_rxusrclk2,
	--	TXOUTCLK_I	=> gth_txusrclk2,
	--	ACC_DATA	=> gth_txpippmstepsize,
	--	error		=> picxo_error,
	--	volt		=> picxo_volt,
	--	OFFSET_PPM	=> picxo_offset_ppm,
	--	OFFSET_EN	=> picxo_offset_en
	--);
	--end generate gen_picxo;
	 
	--================================================================
	-- Data registers to ease routing
	--================================================================
        --MT remove the optional choices of driving the tx_data with frame_gen
        --and loopback. If you want to put it back do it outse
        --felig_lane_wrapper, in the top file

	--gen_rx_data_regsiter : if sim_emulator = false generate
	--rx_data_regsiter: process (gth_rxusrclk2)
	--begin
	--	if (gth_rxusrclk2'event and gth_rxusrclk2 = '1') then
	--		rx_data <= gth_rx_data_20b;
	--	end if;
	--end process rx_data_regsiter;
	--end generate gen_rx_data_regsiter;
	
	--gen_rx_sim_data_regsiter : if sim_emulator = true generate
	--	signal gbt_rx_slide_align : integer := 0;
	--	signal rx_data_pipe : std_logic_vector(19 downto 0) := (others => '0');
	--begin
	--rx_data_regsiter: process (gth_rxusrclk2)
	--begin
	--	if (gth_rxusrclk2'event and gth_rxusrclk2 = '1') then
	--		if (gbt_rx_slide = '1') then
	--			if (gbt_rx_slide_align = 19) then
	--				gbt_rx_slide_align <= 0;
	--			else
	--				gbt_rx_slide_align <= gbt_rx_slide_align + 1;
	--			end if;
	--			rx_data_pipe <= gth_rx_data_20b;
	--			case gbt_rx_slide_align is
	--			when  0 => rx_data <=                             gth_rx_data_20b(19 downto  0);
	--			when  1 => rx_data <= rx_data_pipe( 0 downto 0) & gth_rx_data_20b(19 downto  1);
	--			when  2 => rx_data <= rx_data_pipe( 1 downto 0) & gth_rx_data_20b(19 downto  2);
	--			when  3 => rx_data <= rx_data_pipe( 2 downto 0) & gth_rx_data_20b(19 downto  3);
	--			when  4 => rx_data <= rx_data_pipe( 3 downto 0) & gth_rx_data_20b(19 downto  4);
	--			when  5 => rx_data <= rx_data_pipe( 4 downto 0) & gth_rx_data_20b(19 downto  5);
	--			when  6 => rx_data <= rx_data_pipe( 5 downto 0) & gth_rx_data_20b(19 downto  6);
	--			when  7 => rx_data <= rx_data_pipe( 6 downto 0) & gth_rx_data_20b(19 downto  7);
	--			when  8 => rx_data <= rx_data_pipe( 7 downto 0) & gth_rx_data_20b(19 downto  8);
	--			when  9 => rx_data <= rx_data_pipe( 8 downto 0) & gth_rx_data_20b(19 downto  9);
	--			when 10 => rx_data <= rx_data_pipe( 9 downto 0) & gth_rx_data_20b(19 downto 10);
	--			when 11 => rx_data <= rx_data_pipe(10 downto 0) & gth_rx_data_20b(19 downto 11);
	--			when 12 => rx_data <= rx_data_pipe(11 downto 0) & gth_rx_data_20b(19 downto 12);
	--			when 13 => rx_data <= rx_data_pipe(12 downto 0) & gth_rx_data_20b(19 downto 13);
	--			when 14 => rx_data <= rx_data_pipe(13 downto 0) & gth_rx_data_20b(19 downto 14);
	--			when 15 => rx_data <= rx_data_pipe(14 downto 0) & gth_rx_data_20b(19 downto 15);
	--			when 16 => rx_data <= rx_data_pipe(15 downto 0) & gth_rx_data_20b(19 downto 16);
	--			when 17 => rx_data <= rx_data_pipe(16 downto 0) & gth_rx_data_20b(19 downto 17);
	--			when 18 => rx_data <= rx_data_pipe(17 downto 0) & gth_rx_data_20b(19 downto 18);
	--			when 19 => rx_data <= rx_data_pipe(18 downto 0) & gth_rx_data_20b(19 downto 19);
	--			when others => rx_data <= (others => '0');
	--			end case;
	--		end if;
	--	end if;
	--end process rx_data_regsiter;
	--end generate gen_rx_sim_data_regsiter;

--MT gbtTxRx is in _KCU
--	tx_data_regsiter: process (gth_txusrclk2)
--	begin
--		if (gth_txusrclk2'event and gth_txusrclk2 = '1') then
--			gth_tx_data_20b <= tx_data;
----MT commented out
----			gth_txresetdone_i <= gth_txresetdone;
--		end if;
--	end process tx_data_regsiter;
	
	--================================================================
	-- Physical Link Check / BERT Logic
	--================================================================
        --MT remove the optional choices of driving the tx_data with frame_gen
        --and loopback. If you want to put it back do it outse
        --felig_lane_wrapper, in the top file

	--gen_gt_frame_gen : if sim_emulator = false generate
	--gt_frame_gen: entity work.gt_core_GT_FRAME_GEN
	--port map
	--(
	--	-- User Interface
	--	TX_DATA_OUT		=>	fg_tx_data,
	--	-- System Interface
	--	USER_CLK		=>	lane_txclk_240,
	--	SYSTEM_RESET	=>	fg_reset
	--);
	
	--gt_frame_check: entity work.gt_core_GT_FRAME_CHECK
	--port map
	--(
	--	-- GT Interface
	--	RX_DATA_IN			=> rx_data,
	--	SLIDE_OUT			=> fc_rx_slide,
	--	AUTO_RXRST_OUT		=> fc_rx_auto_reset,
	--	-- System Interface
	--	RX_DATA_OUT			=> fc_rx_data,
	--	USER_CLK			=> lane_rxclk_240,
	--	SYSTEM_RESET		=> fg_reset,
	--	ERROR_COUNT_OUT		=> fc_error_count,
	--	monitor_error_bit	=> fc_error_bit,
	--	monitor_error_frame	=> fc_error_frame,
	--	monitor_error_seed	=> fc_error_seed,
	--	monitor_error_word	=> fc_error_word
	--);
	--end generate gen_gt_frame_gen;

        --MT added
        --fc_rx_slide_out <= fc_rx_slide;
        --
	
	--================================================================
	--  GTH loopback FIFO.
	--================================================================
        --MT remove the optional choices of driving the tx_data with frame_gen
        --and loopback. If you want to put it back do it outse
        --felig_lane_wrapper, in the top file

	--gth_lb_fifo_wr_en <= '1'; -- tie off write enable to 1.
	
	--gen_gth_loopback_fifo : if sim_emulator = false generate
	--gth_loopback_fifo: component gth_word_domain_cross_fifo
	--port map (
	--	rst					=> lb_fifo_rst,
	--	wr_clk				=> lane_rxclk_240,
	--	rd_clk				=> lane_txclk_240,
	--	din					=> rx_data,
	--	wr_en				=> gth_lb_fifo_wr_en,
	--	rd_en				=> gth_lb_fifo_rd_en,
	--	prog_empty_thresh	=> lb_fifo_prog_empty_thresh,
	--	dout				=> gth_lb_fifo_dout,
	--	full				=> gth_lb_fifo_full,
	--	empty				=> gth_lb_fifo_empty,
	--	almost_empty		=> gth_lb_fifo_almost_empty,
	--	prog_empty			=> gth_lb_fifo_prog_empty
	--);
	--end generate gen_gth_loopback_fifo;
	
	--  fifo read control and mux (enable) logic.
	--gth_loopback_fifo_control: process (lane_txclk_240)
	--begin
	--	if (lane_txclk_240'event and lane_txclk_240 = '1') then
	--		-- simple loopback of not prog empty to start.
	--		gth_lb_fifo_rd_en <= not gth_lb_fifo_prog_empty;
	
	--		if (gth_lb_enable = '1') then
	--			tx_data <= gth_lb_fifo_dout;
	--		elsif (fg_data_select = '1') then
	--			tx_data <= fg_tx_data;
	--		else
	--			tx_data <= gbt_tx_data_20b;
	--		end if;
	--	end if;
	--end process gth_loopback_fifo_control;

--        tx_data <= gbt_tx_data_20b;
	
	--================================================================
	--  GBT loopback FIFO.
	--================================================================
        --MT remove the optional choices of driving the tx_data with frame_gen
        --and loopback. If you want to put it back do it outse
        --felig_lane_wrapper, in the top file

	--gen_gbt_loopback_fifo : if sim_emulator = false generate
	--gbt_loopback_fifo: component gbt_word_domain_cross_fifo
	--port map (
	--	rst					=> lb_fifo_rst,
	--	wr_clk				=> lane_rxclk_240,
	--	rd_clk				=> lane_txclk_240,
	--	din					=> gbt_rx_data_120b,
	--	wr_en				=> gbt_rx_flag,
	--	rd_en				=> gbt_lb_fifo_rd_en,
	--	prog_empty_thresh	=> lb_fifo_prog_empty_thresh,
	--	dout				=> gbt_lb_fifo_dout,
	--	full				=> gbt_lb_fifo_full,
	--	empty				=> gbt_lb_fifo_empty,
	--	almost_empty		=> gbt_lb_fifo_almost_empty,
	--	prog_empty			=> gbt_lb_fifo_prog_empty
	--);
	--end generate gen_gbt_loopback_fifo;
	
	----  fifo read control and mux (enable) logic.
	--gbt_loopback_fifo_control: process (lane_txclk_240)
	--begin
	--	if (lane_txclk_240'event and lane_txclk_240 = '1') then
	--		-- simple loopback of not prog empty to start.
	--		if (gbt_tx_flag = '1') then 
	--		 gbt_lb_fifo_rd_en <= not gbt_lb_fifo_prog_empty;
	--		else
	--		 gbt_lb_fifo_rd_en <= '0';
	--		end if;
	--		if (gbt_lb_enable = '0') then
	--			gbt_tx_data_120b <= emu_data_120b;
	--		else
	--			gbt_tx_data_120b <= gbt_lb_fifo_dout;
	--		end if;
	--	end if;
	--end process gbt_loopback_fifo_control;
        
        --MT hard coding the only option (emu data)
        gbt_tx_data_120b <= emu_data_120b;        
	
	--================================================================
	--  GBT Wrapper
	--================================================================
--MT commented out
--	gth_rxslide		<= gbt_rx_slide when (rx_slide_sel = '0') else fc_rx_slide;
--	gth_rxcdrlock	<= '1';


--MT added
        gbt_frame_locked <= gbt_frame_locked_in;  --comes from _KCU
--from outside : to do
        gbt_rx_data_120b <= gbt_rx_data_120b_in;
        gbt_rx_flag <=  gbt_rx_flag_in;  --from gbtTxRx_FELIX
--        gbt_rx_slide <= gbt_rx_slide_in;  -- from FELIX_GBT_RXSLIDE_FSM,
        gbt_tx_data_120b_out <= gbt_tx_data_120b;  --to gbtTxRx_FELIX
--        gbt_tx_data_20b <= gbt_tx_data_20b_in; --from gbtTxRx_FELIX it gets
                                              --assigned to gth_tx_data_20b
                                              --sent to MGT (depending on the
                                              --flag gth data can looped back,
                                              --from frame gen for ibert tests
                                              --or normal operation from gbtTxRx_FELIX)
        gbt_tx_flag <= gbt_tx_flag_in;  --from gbtTxRx_FELIX

                        
--MT commented out: gbt_wrapper features alignment logic, FELIX_GBT_RX_AUTO_RST,
--FELIX_GBT_RXSLIDE_FSM, and gbtTxRx_FELIX which are already in gbt_wrapper_KCU
--(kai)
----	gen_gbt_wrapper : if sim_emulator = false generate
--	gbt_wrapper: entity work.FELIX_gbt_wrapper
--	port map (
--                --hard coded to '0' anyway
--		alignment_chk_rst_i	=> gbt_alignment_chk_rst,           --: in  std_logic;                         
--		clk40_in			=> clk_xtal_40,             --	: in  std_logic;

--                --gbt_rx_data_format fro register: kai has similar register.
         
--		data_rxformat		=> gbt_rx_data_format,              --: in  std_logic_vector(1 downto 0);
--                --same as above
--		data_txformat		=> gbt_tx_data_format,              --: in  std_logic_vector(1 downto 0);
--                --goes to monitor register, logic in Kai wrapper is the same                                                            -
--		error_f				=> gbt_error,               --	: out std_logic;
--                ---alignment_done_f as in KAI, but goes to monitor reg frame_locked
--                ---(in Kai doesn't go anywhere). to be routed to a register
--		frame_locked_o		=> gbt_frame_locked,                --: out std_logic;
--                --used for gbttxrx_felix and for the bitsplip                                                             --
--		gt_rxusrclk			=> lane_rxclk_240,          --	: in  std_logic;                 --used for gbttxrx_felix
--		gt_txusrclk			=> lane_txclk_240,          --	: in  std_logic;                 
--		gtrx_reset			=> manual_gth_rxreset,      --	: in  std_logic;
--                --	--=gtrx_reset or auto_gth_rxrst from gbt_rx_auto_rst
--                --	goes to MGT (gt*_rxreset), KAI has similar one going to
--                --	MGT (reset_rx_datapath_in)
--		gtrx_reset_i		=> gbt_gth_rxrst,                   --: out std_logic;
--                --output of gbtTxRx_FELIX
--		rx_120b_out			=> gbt_rx_data_120b,        --	: out std_logic_vector(119 downto
--                --input of gbtTxRx_FELIX
--		rx_data_20b			=> rx_data,                 --	: in  std_logic_vector(19 downto
--                --output of gbtTxRx_FELIX                                                                            --	
--		rx_flag_o			=> gbt_rx_flag,             --	: out std_logic;
--                --ouput of gbtTxRx_FELIX                                                            --	
--   		rx_header_found		=> gbt_rx_header_found,             --: out std_logic;
----output of gbtTxRx_FELIX
--		rx_is_header		=> gbt_rx_is_header,                --: out std_logic;
----goes to gbtTxRx_FELIX and gbt_rxslide_fsm
--		rx_reset			=> gbt_rx_reset,            --	: in  std_logic;
----assignment to alignment (as for Kai)
--		rxcdrlock			=> gth_rxcdrlock,           --	: in  std_logic;
----input to FELIX_GBT_RX_AUTO_RST
--		rxfsmresetdone		=> gth_rxfsmresetdone,              --: in  std_logic;   --input to FELIX_GBT_RX_AUTO_RST                      
--		rxresetdone			=> gth_rxresetdone,         --	: in  std_logic;
----from FELIX_GBT_RXSLIDE_FSM, same in kai
--		rxslide				=> gbt_rx_slide,            --	: out std_logic;
----input in gbtTxRx_FELIX
--		tc_edge				=> gbt_tc_edge,             --	: in  std_logic; --input in gbtTxRx_FELIX                
--		tx_120b_in			=> gbt_tx_data_120b,        --	: in  std_logic_vector(119 downto
----output of  gbtTxRx_FELIX
--		tx_data_20b			=> gbt_tx_data_20b,         --	: out std_logic_vector(19 downto
----output of  gbtTxRx_FELIX
--		tx_flag_o			=> gbt_tx_flag,             --	: out std_logic;
----output gbtTxRx_FELIX
--		tx_frame_clk_i		=> gbt_rx_flag,  -- not used as cloc--: in  std_logic;                         k, change to gbt_rx_flag
----used by gbtTxRx_FELIX
--		tx_reset			=> gbt_tx_reset,            --	: in  std_logic;
----used by gbtTxRx_FELIX
--		tx_tc_dly_value		=> gbt_tx_tc_dly_value,             --: in  std_logic_vector(3 downto 0);
----used by gbtTxRx_FELIX                                                                           --
--		tx_tc_method		=> gbt_tx_tc_method,                --: in  std_logic;
----used by gbtTxRx_FELIX and from MGt
--		txfsmresetdone		=> gth_txfsmresetdone,              --: in  std_logic;
----same as above                                                                            --
--		txresetdone			=> gth_txresetdone_i        --	: in  std_logic                  
--	);
----	end generate gen_gbt_wrapper;

--MT commented out: move it to _KCU where the slides are happening?
	--rx_slide_counter: dsp_counter
	--PORT MAP (
	--	CLK				=> lane_rxclk_240,
	--	CE				=> gbt_rx_slide,
	--	SCLR			=> rst_hw,
	--	UP				=> '1',
	--	LOAD			=> rxslide_count_reset,
	--	L				=> X"000000000000",
	--	Q(47 downto 32)	=> rxslide_count_unused,
	--	Q(31 downto  0)	=> rxslide_count
	--);
	
	--================================================================
	-- L1A Extraction
	--================================================================
	l1a_mux: entity work.mux_128_sync
	port map (
--		clk							=> rx_frame_clk_bufr,
		clk							=> lane_rxclk_240,
		bit_input(127 downto 121)	=> "0000000" ,
		bit_input(120)				=> '1',
		bit_input(119 downto 0)		=> gbt_rx_data_120b,
		bit_select					=> gbtbitsel,
		bit_output_sync				=> gbt_extractedl1a,
		bit_output_aync				=> open
	);
	
	bchan_mux: entity work.mux_128_sync
	port map (
--		clk							=> rx_frame_clk_bufr,
		clk							=> lane_rxclk_240,
		bit_input(127 downto 121)	=> "0000000" ,
		bit_input(120)				=> '1',
		bit_input(119 downto 0)		=> gbt_rx_data_120b,
		bit_select					=> b_ch_bit_sel,
		bit_output_sync				=> gbt_extracted_bchan,
		bit_output_aync				=> open
	);
	
	l1a_generator_domain_cross_a: process(l1a_int_trigger, lane_txclk_240)
	begin
--		if (gbt_extractedl1a = '1') then
		if (l1a_int_trigger = '1') then
			l1a_int_trigger_pipe(0) <= '1';
		elsif (lane_txclk_240'event and lane_txclk_240='1') then
			if (l1a_int_trigger_pipe(2) = '1') then
				l1a_int_trigger_pipe(0) <= '0';
			end if;
		end if;
	end process;
	
	l1a_generator_domain_cross_b: process(lane_txclk_240)
	begin
		if (lane_txclk_240'event and lane_txclk_240='1') then
			l1a_int_trigger_pipe(1)	<= l1a_int_trigger_pipe(0);
			l1a_int_trigger_pipe(2)	<= l1a_int_trigger_pipe(1);
			l1a_int_trigger_pipe(3)	<= l1a_int_trigger_pipe(2);
		end if;
	end process;
	
	l1a_generator: process(lane_txclk_240)
	begin
		if (lane_txclk_240'event and lane_txclk_240='1') then
			if (l1a_int_trigger_pipe (3) = '0' and l1a_int_trigger_pipe(2) = '1') then
				l1a_int_trigger_piped <= '1';
				l1id <= l1a_int_int_id; --MT for debugging set to const X"2DE4"; 
			else
				l1a_int_trigger_piped <= '0';
			end if;
		end if;
	end process;
	
	
	--================================================================
	-- External L1A Domain Cross
	--================================================================
--MT	ext_l1a_generator_domain_cross_a: process(l1a_int_trigger, lane_txclk_240)
	ext_l1a_generator_domain_cross_a: process(TTC_out(0), lane_txclk_240)        
	begin
		if (TTC_out(0) = '1') then
			ext_l1a_pipe(0) <= '1';
		elsif lane_txclk_240'event and lane_txclk_240='1' then
			if (ext_l1a_pipe(2) = '1') then
				ext_l1a_pipe(0) <= '0';
			end if;
		end if;
	end process;
	
	ext_l1a_generator_domain_cross_b: process(lane_txclk_240)
	begin
		if lane_txclk_240'event and lane_txclk_240='1' then
			ext_l1a_pipe(1)	<= ext_l1a_pipe(0);
			ext_l1a_pipe(2)	<= ext_l1a_pipe(1);
			ext_l1a_pipe(3)	<= ext_l1a_pipe(2);
		end if;
	end process;
	
	ext_l1a_generator: process(lane_txclk_240)
	begin
		if lane_txclk_240'event and lane_txclk_240='1' then
			if (ext_l1a_pipe (3) = '0' and ext_l1a_pipe(2) = '1') then
				ext_l1a_trigger <= '1';
 --internally generated, but relying on external l1a
                                ext_l1a_id <= l1a_ext_int_id; 
--MT commented			ext_l1a_id <= chB_l1a_id(23 downto 8);
			else
				ext_l1a_trigger <= '0';
			end if;
		end if;
	end process;

        --MT added
	l1a_ext_int_id_counter : dsp_counter
	PORT MAP (
		CLK	        => lane_txclk_240,
		CE              => ext_l1a_trigger,
		SCLR	        => '0',
		UP		=> '1',
		LOAD	        => '0',
		L		=> X"000000000000",
		Q(47 downto 16)	=> l1a_ext_int_id_extra, --open,
		Q(15 downto  0)	=> l1a_ext_int_id
	);
--

        
	
	--================================================================
	-- L1a Selection Mux
	--================================================================
	l1a_ext_mux: process(lane_txclk_240)
		begin
		if lane_txclk_240'event and lane_txclk_240='1' then
			if (ext_l1a_select = '0') then
				l1a_trigger	<= l1a_int_trigger_piped;
				l1a_id		<= l1id;
			else
				l1a_trigger	<= ext_l1a_trigger;
				l1a_id		<= ext_l1a_id;
			end if;
		end if;
	end process;
	
	--================================================================
	-- Data Emulator
	--================================================================
	-- BUGBUG this section appears to not be complete.
	--
	--
	--	Generation of fake GBT data can occur from state machines or from
	--	RAMs.
	--	Top-level mux
	EMU_DATA_MUX: process(lane_txclk_240)
	begin
		if lane_txclk_240'event and lane_txclk_240='1' then
--			if (gbt_emu_data_select ='0') then
				emu_data_120b <= tx_120b_from_mach;
--			else
--				emu_data_120b <= tx_120b_from_ram;
--			end if;
		end if;
	end process;
	
--	BUGBUG	RAM option is too large at the moment to fit within a single clock region. About 100 BRAM used per instantiation.
--			Only 300 aviable per region.
--			Need to allocate two lane per region.
--			Could share between pairs of lanes... or reduce size.  May also be able to reduce overheads such as ILA (8 BRAMs).
	-- MBO 20170921: Not a good long term solution to have this as currently implemented.  Limited by avaiable software at the moment.
	--	RAM block option
--	RAMDataGen: entity work.GBTdataEmulator
--	generic map ( 
--		useGBTdataEmulator		=> useGBTdataEmulator
--	)
--	port map (
--		rdclk					=> lane_txclk_240,
--		wrclk					=> clk_xtal_40,
--		gbt_tx_flag				=> gbt_tx_flag,
--		l1a_trigger_in			=> l1a_trigger,
--		rst_hw					=> rst_hw,
--		rst_soft				=> rst_soft(0),
--		xoff					=> '0',
--		register_map_control	=> register_map_control_40xtal,
--		l1a_trigger_lat_o       => l1a_trigger_lat,
--		GBTdata					=> tx_120b_from_ram
--	);
	
	-- testing optimized method of building GBT word
	
--	gen_elink_muxer : if sim_emulator = false generate
	elink_muxer:   entity work.gbt_word_printer
	generic map (
		enable_endian_control	=> '1'	-- if '0', then operation is always in little endian.
	)
	port map (
		clk240					=> lane_txclk_240,
		elink_sync				=> elink_sync,
		elink_control			=> elink_control,
		elink_data				=> elink_data_in,
		elink_read_enable		=> elink_data_re,
		gbt_payload				=> tx_120b_from_mach( 79+32 downto 32),
		tx_flag					=> gbt_tx_flag,
		ila_gbt_word_gen_state	=> ila_gbt_word_gen_state
	);
--	end generate gen_elink_muxer;

        --MT checker gbt word
        --goal: for 2b elink (e.g: group1 with current config) accumulate 10b
        --over 5xgbt_tx_flag and look for following pattern: 0xaa,
        --chunk_length(2bytes), L1ID 2bytes), 0xbb, 0xaa
        --count distance between 0xaa & 0x00 & 0x3C  and 0xbb + 0xaa (assuming
        --chunk_lenght is = 0x03C)

	gbtword_checker:   entity work.gbtword_checker
	port map (
          lane_txclk_240	  => lane_txclk_240	  ,	
          gbt_tx_flag             => gbt_tx_flag          ,   
          tx_120b_from_mach       => tx_120b_from_mach    ,   

          ila_phlck_gwchk         => ila_phlck_gwchk      ,   
          ila_en_gwchk            => ila_en_gwchk         ,   
          ila_count_gwchk         => ila_count_gwchk      ,   
          ila_count_start_gwchk   => ila_count_start_gwchk,   
          ila_testpass_gwchk      => ila_testpass_gwchk   ,   
          ila_dist_gwchk          => ila_dist_gwchk       ,   
          ila_word10b_gwchk       => ila_word10b_gwchk    ,   
          ila_count_pyld          => ila_count_pyld          
          
);


        --
	
	tx_120b_from_mach(119 downto 112) <= "0101" & "11" & "11";
	
--	gen_dgen_group : if sim_emulator = false generate
	dgen_group: for i in 0 to 4 generate
		emu_data_re(i) <= elink_data_re(i*8);
		
		emu_0: entity work.elink_data_emulator
		generic map (
		   epath		=> epath,
		   egroup		=> to_std_logic_vector(i,3),
		   LANE_ID      => LANE_ID
                   
		)
		port map (
			clk40				=> clk_xtal_40,  --Frans1
			clk240				=> lane_txclk_240,
			emu_control			=> emu_control(i),
			elink_data_out		=> emu_data_out(i),
			elink_data_re		=> emu_data_re(i),
			l1a_trig			=> l1a_trigger,
			l1a_id				=> l1a_id,
			ila_data_gen_out	=> ila_data_gen_out(i),
			ila_fifo_out		=> ila_fifo_out(i),
			ila_data_gen_we		=> ila_data_gen_we(i),
                        --MT checker upstream fifo
                        ila_count_upstfifochk   => ila_count_upstfifochk(i),
                        ila_fifo_flush   => ila_fifo_flush(i),                        
			ila_count_chk_out       => ila_count_chk_out(i),
			ila_count_chk_out2      => ila_count_chk_out2(i),
                        ila_isEOP_chk2          => ila_isEOP_chk2(i),
                        ila_efifoDout_8b10b     => ila_efifoDout_8b10b(i),
                        ila_enc10bitRdy         => ila_enc10bitRdy(i),

                        ila_efifoFull           => ila_efifoFull(i)     ,
                        ila_efifoEmpty          => ila_efifoEmpty(i)    ,
                        ila_efifoPfull          => ila_efifoPfull(i),
                        ila_elink_data_re       => ila_elink_data_re(i) ,
                        ila_efifoDoutRdy        => ila_efifoDoutRdy(i)  ,
                        --Frans 1
			SELECT_RANDOM => SELECT_RANDOM,
                        chunk_length_out => chunk_length_ila(i),
                        --MT 2 (Fran 2)
                        FMEMU_RANDOM_RAM_ADDR          => FMEMU_RANDOM_RAM_ADDR ,
                        FMEMU_RANDOM_RAM               => FMEMU_RANDOM_RAM      ,
                        FMEMU_RANDOM_CONTROL           => FMEMU_RANDOM_CONTROL  
                        
                        
                        
			--
		);
		
		dmap : for j in 0 to 7 generate
			elink_data_in(i*8+j) <= emu_data_out(i);
		end generate dmap;
	end generate dgen_group;
--	end generate gen_dgen_group;

--	gen_ttc_wrapper_comp : if sim_emulator = false generate
	ttc_wrapper_comp : entity work.ttc_wrapper
	port map (
		a_data_in			=> gbt_extractedl1a,
		b_data_in			=> gbt_extracted_bchan,
		ttc_strobe			=> gbt_rx_flag,
		TTC_out				=> TTC_out,
		clk240				=> lane_rxclk_240,
		BUSY				=> chB_busy,
		L1ID_Bch			=> chB_l1a_id,
		TTC_ToHost_Data_out	=> TTC_ToHost_Data
	);
--	end generate gen_ttc_wrapper_comp;
end architecture Behavioral;
