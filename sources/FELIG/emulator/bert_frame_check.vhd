--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	Michael Oberling
--
-- Design Name:	bert_frame_check
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Coming soon.
--
-- Change Log:	V1.0 - 
--
--==============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.type_lib.ALL;
use work.ip_lib.ALL;
use work.function_lib.ALL;

entity bert_frame_check is
port (
	clk_word					: in  std_logic;
	
	frame_data					: in std_logic_vector(15 downto 0);
	frame_data_valid			: in std_logic;
	
	diag_state					: out std_logic_vector(7 downto 0);
	monitor_counter_reset		: in  std_logic;
	monitor_data				: out std_logic_vector(15 downto 0);
	monitor_error_bit			: out std_logic;
	monitor_error_frame			: out std_logic;
	monitor_error_seed			: out std_logic;
	monitor_error_word			: out std_logic;
	monitor_error_count_bit		: out std_logic_vector(31 downto 0);
	monitor_error_count_frame	: out std_logic_vector(31 downto 0);
	monitor_error_count_seed	: out std_logic_vector(31 downto 0);
	monitor_error_count_word	: out std_logic_vector(31 downto 0);
	monitor_frame_count			: out std_logic_vector(63 downto 0);
	monitor_checked_frames		: out std_logic_vector(63 downto 0);
	monitor_check_word			: out std_logic_vector(15 downto 0)
);
end entity bert_frame_check;

architecture arch of bert_frame_check is
	type check_state is (
		ST_INIT				,
		ST_FIND_START_0		,
		ST_FIND_START_1		,
		ST_FIND_START_2		,
		ST_HEADER_0			,
		ST_HEADER_1			,
		ST_HEADER_2			,
		ST_LENGTH			,
		ST_FRAME_COUNT_0	,
		ST_FRAME_COUNT_1	,
		ST_FRAME_COUNT_2	,
		ST_FRAME_COUNT_3	,
		ST_FRAME_SEED_0		,
		ST_FRAME_SEED_1		,
		ST_FRAME_SEED_2		,
		ST_FRAME_SEED_3		,
		ST_FRAME_SEED_4		,
		ST_FRAME_SEED_5		,
		ST_FRAME_SEED_6		,
		ST_FRAME_SEED_7		,
		ST_FRAME_RAND_START	,
		ST_FRAME_DATA		
	);
	
	signal bert_word				: std_logic_vector(15 downto  0)	:= (others => '0');
	signal bert_word_enable			: std_logic							:= '0';
	signal bert_word_valid			: std_logic							:= '0';
	signal checked_frame			: std_logic							:= '0';
	signal checked_frame_count		: std_logic_vector(63 downto  0)	:= (others => '0');
	signal error_bit				: std_logic							:= '0';
	signal error_count_bit			: std_logic_vector(31 downto  0)	:= (others => '0');
	signal error_count_bit_unused	: std_logic_vector(47 downto 32)	:= (others => '0');
	signal error_count_frame		: std_logic_vector(31 downto  0)	:= (others => '0');
	signal error_count_frame_unused	: std_logic_vector(47 downto 32)	:= (others => '0');
	signal error_count_reset		: std_logic							:= '0';
	signal error_count_seed			: std_logic_vector(31 downto  0)	:= (others => '0');
	signal error_count_seed_unused	: std_logic_vector(47 downto 32)	:= (others => '0');
	signal error_count_word			: std_logic_vector(31 downto  0)	:= (others => '0');
	signal error_count_word_unused	: std_logic_vector(47 downto 32)	:= (others => '0');
	signal error_frame				: std_logic							:= '0';
	signal error_pending_seed		: std_logic							:= '0';
	signal error_pending_word		: std_logic							:= '0';
	signal error_seed				: std_logic							:= '0';
	signal error_word				: std_logic							:= '0';
	signal frame_count				: std_logic_vector(63 downto  0)	:= (others => '0');
	signal frame_length				: std_logic_vector(15 downto  0)	:= (others => '0');
	signal frame_word_count			: std_logic_vector(15 downto  0)	:= (others => '0');
	signal frame_word_count_unused	: std_logic_vector(47 downto 16)	:= (others => '0');
	signal frame_word_counter_reset	: std_logic							:= '0';
	signal last_bert_word			: std_logic_vector(15 downto  0)	:= (others => '0');
	signal next_frame_count			: std_logic_vector( 15 downto  0)	:= (others => '0');
	signal seed_enable				: std_logic							:= '0';
	signal seed_in					: std_logic_vector(127 downto  1)	:= (others => '0');
	signal seed_out					: std_logic_vector(127 downto  1)	:= (others => '0');
	signal state					: check_state						:= ST_INIT;
begin
	monitor_error_bit			<= error_bit			;
	monitor_error_frame			<= error_frame			;
	monitor_error_seed			<= error_seed			;
	monitor_error_word			<= error_word			;
	monitor_error_count_bit		<= error_count_bit		;
	monitor_error_count_frame	<= error_count_frame	;
	monitor_error_count_seed	<= error_count_seed		;
	monitor_error_count_word	<= error_count_word		;
	monitor_frame_count			<= frame_count			;
	monitor_checked_frames		<= checked_frame_count	;
	monitor_check_word			<= bert_word			;
	
	random_word_gen_comp : entity work.random_word_gen
	port map (
		clk_word			=> clk_word,
			
		rand_word			=> bert_word,
		rand_word_enable	=> bert_word_enable,
		rand_word_valid		=> bert_word_valid,
		
		seed_enable			=> seed_enable,
		seed_in				=> seed_in,
		seed_out			=> seed_out
	);

	checked_frame_counter : dsp_counter
	PORT MAP (
		CLK				=> clk_word,
		CE				=> checked_frame,
		SCLR			=> error_count_reset,
		UP				=> '1',
		LOAD			=> '0',
		L				=> X"000000000000",
		Q(47 downto  0)	=> checked_frame_count(47 downto  0)	
	);
		
	error_counter_bit : dsp_counter
	PORT MAP (
		CLK				=> clk_word,
		CE				=> error_bit,
		SCLR			=> error_count_reset,
		UP				=> '1',
		LOAD			=> '0',
		L				=> X"000000000000",
		Q(47 downto 32)	=> error_count_bit_unused,
		Q(31 downto  0)	=> error_count_bit
	);
				
	error_counter_frame : dsp_counter
	PORT MAP (
		CLK				=> clk_word,
		CE				=> error_frame,
		SCLR			=> error_count_reset,
		UP				=> '1',
		LOAD			=> '0',
		L				=> X"000000000000",
		Q(47 downto 32)	=> error_count_frame_unused,
		Q(31 downto  0)	=> error_count_frame
	);		
		
	error_counter_seed : dsp_counter
	PORT MAP (
		CLK				=> clk_word,
		CE				=> error_seed,
		SCLR			=> error_count_reset,
		UP				=> '1',
		LOAD			=> '0',
		L				=> X"000000000000",
		Q(47 downto 32)	=> error_count_seed_unused,
		Q(31 downto  0)	=> error_count_seed
	);		
	
	error_counter_word : dsp_counter
	PORT MAP (
		CLK				=> clk_word,
		CE				=> error_word,
		SCLR			=> error_count_reset,
		UP				=> '1',
		LOAD			=> '0',
		L				=> X"000000000000",
		Q(47 downto 32)	=> error_count_word_unused,
		Q(31 downto  0)	=> error_count_word
	);
	
	frame_word_counter : dsp_counter
	PORT MAP (
		CLK				=> clk_word,
		CE				=> frame_data_valid,
		SCLR			=> frame_word_counter_reset,
		UP				=> '1',
		LOAD			=> '0',
		L				=> X"000000000000",
		Q(47 downto 16)	=> frame_word_count_unused,
		Q(15 downto  0)	=> frame_word_count
	
	);
	
	-- only works for async loop back as currently written.
	frame_check : process(clk_word)
	begin
		if (clk_word'event and clk_word = '1') then
			if((state = ST_INIT) or monitor_counter_reset = '1') then
				error_count_reset <= '1';
			else
				error_count_reset <= '0';
			end if;
			case state is
			when ST_INIT			=>
				diag_state			<= X"00";
				bert_word_enable	<= '0';
				checked_frame		<= '0';
				error_bit			<= '0';
				error_frame			<= '0';
				error_pending_seed	<= '0';
				error_pending_word	<= '0';
				error_seed			<= '0';
				error_word			<= '0';
				seed_enable			<= '0';
				state				<= ST_FIND_START_0;
				
			when ST_FIND_START_0	=>
				diag_state			<= X"01";
				bert_word_enable	<= '0';
				checked_frame		<= '0';
				error_bit			<= '0';
				error_frame			<= '0';
				error_pending_seed	<= '0';
				error_pending_word	<= '0';
				error_seed			<= '0';
				error_word			<= '0';
				seed_enable			<= '0';
				frame_word_counter_reset	<= '1';
				if (frame_data_valid = '1') then
					if (frame_data = X"7FFF") then
						state <= ST_FIND_START_1;
					else
						state <= ST_FIND_START_0;
					end if;
				end if;
				
			when ST_FIND_START_1	=>
				diag_state			<= X"01";
				if (frame_data_valid = '1') then
					if (frame_data = X"8000") then
						state <= ST_FIND_START_2;
					else
						state <= ST_FIND_START_0;
					end if;
				end if;
				
			when ST_FIND_START_2	=>
				diag_state			<= X"02";
				if (frame_data_valid = '1') then
					if (frame_data = X"AAAA") then
						frame_word_counter_reset	<= '0';
						state <= ST_LENGTH;
					else
						state <= ST_FIND_START_1;
					end if;
				end if;
				
			when ST_HEADER_0		=>
				frame_word_counter_reset	<= '1';
				diag_state					<= X"03";
				bert_word_enable			<= '0';
				checked_frame				<= '0';
				error_bit					<= '0';
				error_frame					<= '0';
				error_pending_seed			<= '0';
				error_pending_word			<= '0';
				error_seed					<= '0';
				error_word					<= '0';
				seed_enable					<= '0';
				if (frame_data_valid = '1') then
					if (frame_data = X"7FFF") then
						state <= ST_HEADER_1;
					end if;
				end if;
				
			when ST_HEADER_1		=>
				diag_state			<= X"04";
				if (frame_data_valid = '1') then
					if (frame_data = X"8000") then
						state		<= ST_HEADER_2;
					else
						state		<= ST_HEADER_0;
					end if;
--					else
--						state		<= ST_HEADER_0;
				end if;
				
			when ST_HEADER_2		=>
				diag_state			<= X"05";
				if (frame_data_valid = '1') then
					if (frame_data = X"AAAA") then
						frame_word_counter_reset	<= '0';
						state		<= ST_LENGTH;
					else
						state		<= ST_HEADER_1;
					end if;
--					else
--						state		<= ST_HEADER_1;
				end if;
				
			when ST_LENGTH			=>
				diag_state			<= X"06";
				bert_word_enable	<= '0';
				checked_frame		<= '0';
				error_bit			<= '0';
				error_pending_seed	<= '0';
				error_pending_word	<= '0';
				error_seed			<= '0';
				error_word			<= '0';
				next_frame_count	<= frame_count(15 downto 0) + 1;
				if (frame_data_valid = '1') then
					frame_length		<= frame_data;
					state			<= ST_FRAME_COUNT_0;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_COUNT_0	=>
				diag_state			<= X"07";
				if (frame_data_valid = '1') then
					frame_count(15 downto 0)	<= frame_data;
--					if (next_frame_count /= frame_data) then
--							error_frame		<= '1';
--						error_pending_seed		<= '1';
--						state			<= ST_FIND_START_0;
--					else
						state			<= ST_FRAME_COUNT_1;
--					end if;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_COUNT_1	=>
				diag_state			<= X"08";
				frame_count(31 downto 16)	<= frame_data;
				if (frame_data_valid = '1') then
					state			<= ST_FRAME_COUNT_2;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_COUNT_2	=>
				diag_state			<= X"09";
				if (frame_data_valid = '1') then
					frame_count(47 downto 32)	<= frame_data;
					state			<= ST_FRAME_COUNT_3;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_COUNT_3	=>
				diag_state			<= X"0A";
				if (frame_data_valid = '1') then
					frame_count(63 downto 48)	<= frame_data;
					state			<= ST_FRAME_SEED_0;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_SEED_0	=>
				diag_state			<= X"0B";
				if (frame_data_valid = '1') then
					seed_in(16 downto 1)	<= frame_data;
					
--					if(frame_data /= seed_out(16 downto 1)) then
--						error_pending_seed <= '1';
--					end if;
					
					state			<= ST_FRAME_SEED_1;
					
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_SEED_1	=>
				diag_state			<= X"0C";
				if (frame_data_valid = '1') then
					seed_in(32 downto 17)	<= frame_data;
					
--					if(frame_data /= seed_out(32 downto 17)) then
--						error_pending_seed <= '1';
--					end if;
				
					state			<= ST_FRAME_SEED_2;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_SEED_2	=>
				diag_state			<= X"0D";
				if (frame_data_valid = '1') then
					seed_in(48 downto 33)	<= frame_data;
					
--					if(frame_data /= seed_out(48 downto 33)) then
--						error_pending_seed <= '1';
--					end if;
					
					state			<= ST_FRAME_SEED_3;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_SEED_3	=>
				diag_state			<= X"0E";
				if (frame_data_valid = '1') then
					seed_in(64 downto 49)	<= frame_data;
					
--					if(frame_data /= seed_out(64 downto 49)) then
--						error_pending_seed <= '1';
--					end if;
					
					state			<= ST_FRAME_SEED_4;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_SEED_4	=>
				diag_state			<= X"0F";
				if (frame_data_valid = '1') then
					seed_in(80 downto 65)	<= frame_data;
					
--					if(frame_data /= seed_out(80 downto 65)) then
--						error_pending_seed <= '1';
--					end if;
					
					state			<= ST_FRAME_SEED_5;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_SEED_5	=>
				diag_state			<= X"10";
				if (frame_data_valid = '1') then
					seed_in(96 downto 81)	<= frame_data;
					
--					if(frame_data /= seed_out(96 downto 81)) then
--						error_pending_seed <= '1';
--					end if;
					
					state			<= ST_FRAME_SEED_6;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_SEED_6	=>
				diag_state			<= X"11";
				if (frame_data_valid = '1') then
					seed_in(112 downto 97)	<= frame_data;
					
--					if(frame_data /= seed_out(112 downto 97)) then
--						error_pending_seed <= '1';
--					end if;
					
					state			<= ST_FRAME_SEED_7;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_SEED_7	=>
				diag_state			<= X"12";
				if (frame_data_valid = '1') then
					bert_word_enable	<= '0';
					seed_enable			<= '1';
					seed_in(127 downto 113)	<= frame_data(14 downto 0);
					
--					if(frame_data /= seed_out(127 downto 113)) then
--						error_pending_seed <= '1';
--					end if;
					
					state			<= ST_FRAME_RAND_START;
--					else
--						error_frame		<= '1';
--						state			<= ST_FIND_START_0;
				end if;
				
			when ST_FRAME_RAND_START	=>
				diag_state			<= X"13";
				seed_enable			<= '0';
				if (frame_data_valid = '1') then
					bert_word_enable	<= '1';
					last_bert_word		<= bert_word;
					
					if(frame_data = X"AAAA") then
						state <= ST_FRAME_DATA;
					else
						error_frame		<= '1';
						state			<= ST_FIND_START_0;
					end if;
					
					checked_frame	<= '1';
--					else
				end if;
				
			when ST_FRAME_DATA		=>
				diag_state			<= X"14";
				checked_frame		<= '0';
				seed_enable			<= '0';
				error_seed 			<= error_pending_seed;
				error_pending_seed	<= '0';
				if (frame_data_valid = '1') then
					if ((bert_word_enable = '0') and (frame_data = last_bert_word)) then
						error_bit <= '0';
					elsif (frame_data = bert_word) then
						error_bit <= '0';
					else
						error_bit <= '1';
					end if;
					
					if (frame_word_count = frame_length) then
						bert_word_enable	<= '0';
						state <= ST_HEADER_0;
					else
						bert_word_enable	<= '1';
					end if;
--					else
--						error_word		<= '1';
				else
					error_bit <= '0';
					bert_word_enable	<= '0';
				end if;
			end case;
--			else
--				if(frame_data_valid = '0') then
--					bert_word_enable	<= '0';
--				end if;
		end if;
	end process frame_check;
end architecture arch;
