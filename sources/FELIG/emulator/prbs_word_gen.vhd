--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	Michael Oberling
--
-- Design Name:	random_word_gen
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Coming soon.
--
-- Change Log:	V1.0 - 
--
--==============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity random_word_gen is
port (
	clk_word			: in  std_logic;
		
	rand_word			: out std_logic_vector(15 downto 0);
	rand_word_enable	: in  std_logic;
	rand_word_valid		: out std_logic;
	
	seed_enable			: in  std_logic;
	seed_in				: in  std_logic_vector(127 downto 1);
	seed_out			: out std_logic_vector(127 downto 1)
);
end entity random_word_gen;


architecture arch of random_word_gen is
	signal lfsr						: std_logic_vector(127 downto 1)	:= (others => '0');
	signal shift_out				: std_logic_vector(rand_word'range)	:= (others => '0');
	signal feedback_bit				: std_logic							:= '0';
begin
	-- Maximal run length 127 bit lfsr.
	rand_word <= lfsr(121) & lfsr(102) & lfsr(92) & lfsr(79) & lfsr(67) & lfsr(56) & lfsr(46) & lfsr(37) & lfsr(29) & lfsr(22) & lfsr(16) & lfsr(11) & lfsr(7) & lfsr(4) & lfsr(2) & lfsr(1);
	feedback_bit <= lfsr(126) xnor lfsr(127);
	seed_out <= lfsr;
	
	rand : process (clk_word)
	begin
		if (clk_word'event and clk_word = '1') then
			if ((seed_enable = '1') and (rand_word_enable = '1')) then
				lfsr			<= seed_in (126 downto 1) & (seed_in(126) xnor seed_in(127));
			elsif (seed_enable = '1') then
				lfsr			<= seed_in;
			elsif (rand_word_enable = '1') then
				lfsr			<= lfsr (126 downto 1) & feedback_bit;
			end if;
			
			rand_word_valid <= rand_word_enable;									
		end if;
	end process rand;	
end architecture arch;
