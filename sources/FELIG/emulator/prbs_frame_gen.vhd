--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	Michael Oberling
--
-- Design Name:	random_frame_gen
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Coming soon.
--
-- Change Log:	V1.0 - 
--
--==============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use work.type_lib.ALL;
use work.ip_lib.ALL;
use work.function_lib.ALL;

entity random_frame_gen is
generic (
	run_length				: integer
);
port (
	reset					: in  std_logic;
	clk_word				: in  std_logic;
	
	rand_frame_data			: out std_logic_vector(15 downto 0);
	rand_frame_enable		: in  std_logic;
	rand_frame_data_valid	: out std_logic;
	
	seed_enable				: in  std_logic;
	seed_in					: in  std_logic_vector(127 downto 1)
);
end entity random_frame_gen;


architecture arch of random_frame_gen is
	constant frame_length				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  1, 16);
	constant word_init					: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  1, 16);
	constant word_header_0				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  2, 16);
	constant word_header_1				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  3, 16);
	constant word_header_2				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  4, 16);
	constant word_length				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  5, 16);
	constant word_frame_count_0			: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  6, 16);
	constant word_frame_count_1			: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  7, 16);
	constant word_frame_count_2			: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  8, 16);
	constant word_frame_count_3			: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 -  9, 16);
	constant word_seed_0				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 - 10, 16);
	constant word_seed_1				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 - 11, 16);
	constant word_seed_2				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 - 12, 16);
	constant word_seed_3				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 - 13, 16);
	constant word_seed_4				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 - 14, 16);
	constant word_seed_5				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 - 15, 16);
	constant word_seed_6				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 - 16, 16);
	constant word_seed_7				: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 - 17, 16);
	constant word_rand_start			: std_logic_vector(15 downto 0)	:= to_std_logic_vector(run_length + 1 + 3 + 1 + 4 + 8 + 1 + 1 - 18, 16);
	constant word_0						: std_logic_vector(15 downto 0)	:= to_std_logic_vector(0, 16);
	constant word_1						: std_logic_vector(15 downto 0)	:= to_std_logic_vector(1, 16);
	constant reported_frame_length		: std_logic_vector(15 downto 0)	:= frame_length - 5;

	signal frame_count					: std_logic_vector(63 downto 0)		:= (others => '0');
	signal rand_word					: std_logic_vector(15 downto 0)		:= (others => '0');
	signal rand_word_enable				: std_logic							:= '0';
	signal rand_word_valid				: std_logic							:= '0';
	signal seed_out						: std_logic_vector(127 downto 1)	:= (others => '0');
	signal word_count					: std_logic_vector( 15 downto 0)	:= (others => '0');
	signal word_count_enable			: std_logic							:= '0';
	signal word_count_load_enable		: std_logic							:= '0';
	signal word_count_unused			: std_logic_vector(47 downto 16)	:= (others => '0');
begin
	random_word_gen_comp : entity work.random_word_gen
	port map (
		clk_word			=> clk_word,
		
		rand_word			=> rand_word,
		rand_word_enable	=> rand_word_enable,
		rand_word_valid		=> rand_word_valid,
		
		seed_enable			=> seed_enable,
		seed_in				=> seed_in,
		seed_out			=> seed_out
	);
	
	frame_counter : component dsp_counter
	port map (
		CLK				=> clk_word,
		CE				=> rand_frame_enable,
		SCLR			=> reset,
		UP				=> '1',
		LOAD			=> '0',
		L				=> X"000000000000",
		Q(47 downto 0)	=> frame_count(47 downto 0)
	);
	
	word_counter : component dsp_counter
	port map (
		CLK				=> clk_word,
		CE				=> word_count_enable,
		SCLR			=> '0',
		UP				=> '0',
		LOAD			=> word_count_load_enable,
		L(47 downto 16)	=> X"00000000",
		L(15 downto  0)	=> frame_length,
		Q(47 downto 16)	=> word_count_unused,
		Q(15 downto  0)	=> word_count
	);
	
	frame_gen : process(clk_word)
	begin
		if (clk_word'event and clk_word = '1') then
			if (rand_frame_enable = '1') then
				word_count_load_enable <= '1';
			else
				word_count_load_enable <= '0';
			end if;
			
			if (rand_frame_enable = '1') then
				word_count_enable <= '1';
			elsif (word_count = 1) then
				word_count_enable <= '0';
			end if;
			
			case word_count is
			when word_init			=>
				rand_frame_data			<= (others => '0');
				rand_frame_data_valid	<= '0';
				rand_word_enable		<= '0';
			when word_header_0		=>
				rand_frame_data			<= X"7FFF";
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_header_1		=>
				rand_frame_data			<= X"8000";
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_header_2		=>
				rand_frame_data			<= X"AAAA";
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_length			=>
--				rand_frame_data			<= frame_length;
				rand_frame_data			<= reported_frame_length;
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_frame_count_0	=>
				rand_frame_data			<= frame_count(15 downto 0);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_frame_count_1	=>
				rand_frame_data			<= frame_count(31 downto 16);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_frame_count_2	=>
				rand_frame_data			<= frame_count(47 downto 32);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_frame_count_3	=>
				rand_frame_data			<= frame_count(63 downto 48);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_seed_0		=>
				rand_frame_data			<= seed_out(16 downto 1);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_seed_1		=>
				rand_frame_data			<= seed_out(32 downto 17);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_seed_2		=>
				rand_frame_data			<= seed_out(48 downto 33);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_seed_3		=>
				rand_frame_data			<= seed_out(64 downto 49);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_seed_4		=>
				rand_frame_data			<= seed_out(80 downto 65);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_seed_5		=>
				rand_frame_data			<= seed_out(96 downto 81);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_seed_6		=>
				rand_frame_data			<= seed_out(112 downto 97);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_seed_7		=>
				rand_frame_data			<= '0' & seed_out(127 downto 113);
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_rand_start		=>
				rand_frame_data			<= X"AAAA";
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '1';
			when word_1				=>
				rand_frame_data			<= rand_word;
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '0';
			when word_0				=>
				rand_frame_data			<= (others => '0');
				rand_frame_data_valid	<= '0';
				rand_word_enable		<= '0';
			when others				=>
				rand_frame_data			<= rand_word;
				rand_frame_data_valid	<= '1';
				rand_word_enable		<= '1';
			end case;
		end if;
	end process frame_gen;
	
end architecture arch;
