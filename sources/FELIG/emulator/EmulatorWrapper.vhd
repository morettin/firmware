--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Shelfali Saxena
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer: Initially created for HG710 as gt_core_exdes by Michael Oberling
-- modified for FLX-712 by Marco Trovato

-- Design Name:  EmulatorWrapper
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;
--use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;
--use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.centralRouter_package.all;

--***********************************Entity Declaration************************

entity EmulatorWrapper is
    generic
(
        STABLE_CLOCK_PERIOD         : integer := 10;
        --  DATARATE                    : integer := 0;  --0=4.8 Gbps (GBT, Phase1),
        --                                                 --1=5.12 Gbps (LPGBT, Phase2)
        --                                                 --2=10.24 Gbps(LPGBT, Phase2)
        --  FEC                         : integer := 0; --0=FEC5, 1=FEC12
        --  NUMELINK                    : integer := 40;
        --  NUMEGROUP                   : integer := 5;
        --  ELINKdivEGROUP              : integer := 8;
        GBT_NUM                     : integer := 24;
        NUMELINKmax                 : integer := 112;
        NUMEGROUPmax                : integer := 7
    );
    port
(

        --================================
        -- Clocks & Reset
        --================================

        clk_xtal_40        : in  std_logic;
        --either 240 MHZ (DATARATE=0) or 320 MHZ (DATARATE=1)
        gt_txusrclk_in                        : in std_logic_vector(GBT_NUM-1 downto 0);
        gt_rxusrclk_in                        : in std_logic_vector(GBT_NUM-1 downto 0);
        gt_rx_clk_div_2_mon      : out std_logic_vector      (23 downto 0);
        gt_tx_clk_div_2_mon      : out std_logic_vector      (23 downto 0);

        --================================
        -- L1A Interface
        --================================

        l1a_trigger_out      : out std_logic_vector      (23 downto 0);

        --================================
        -- Data
        --================================

        link_tx_data_228b_array_out            : out txrx228b_type(0 to GBT_NUM-1);
        --cernphase1 only the ls120b will be used
        link_rx_data_120b_array_in             : in  txrx120b_type(0 to GBT_NUM-1);
        --cernphase2 only the ls36b will be used

        link_tx_flag_in                        : in std_logic_vector(GBT_NUM-1 downto 0);
        link_rx_flag_in                        : in std_logic_vector(GBT_NUM-1 downto 0);

        --================================
        -- Control and Status Interface
        --================================

        lane_control        : in  array_of_lane_control_type(GBT_NUM-1 downto 0);
        lane_monitor        : out array_of_lane_monitor_type(GBT_NUM-1 downto 0);
        --  lane_control        : in  array_of_lane_control_type(23 downto 0);
        --  lane_monitor        : out array_of_lane_monitor_type(23 downto 0);
        register_map_control_40xtal          : in  register_map_control_type;

        link_frame_locked_array_in             : in std_logic_vector(GBT_NUM-1 downto 0);
        --MT preserve the same order as the lpgbt wrapper (0 to GBT_NUM-1);
        --SS (SWAP LSB MSB)
        --fhCR_REVERSE_10B                      : in  std_logic
        LINKSconfig          : in std_logic_vector(2 downto 0)
    );


end EmulatorWrapper;

architecture RTL of EmulatorWrapper is
    attribute DowngradeIPIdentifiedWarnings: string;
    attribute DowngradeIPIdentifiedWarnings of RTL : architecture is "yes";

    attribute CORE_GENERATION_INFO : string;
    attribute CORE_GENERATION_INFO of RTL : architecture is "gt_core,gtwizard_v3_6_3,{protocol_file=Start_from_scratch}";

    ------------------------------- Global Signals -----------------------------

    signal  tied_to_ground_i                : std_logic;
    signal  tied_to_ground_vec_i            : std_logic_vector(63 downto 0);
    signal  tied_to_vcc_i                   : std_logic;
    signal  tied_to_vcc_vec_i               : std_logic_vector(7 downto 0);

    attribute keep: string;
    ------------------------------- User Clocks ---------------------------------
    signal l1a_int_trigger        : std_logic;
    signal l1a_int_int_id          : std_logic_vector(15 downto 0);
    signal l1a_int_int_id_extra             : std_logic_vector(31 downto 0);
    signal l1a_int_count ,L1A_INT_MAX_COUNT  : std_logic_vector(31 downto 0);
    signal l1a_int_max_count_extra          : std_logic_vector(15 downto 0) := (others=>'0');

    signal l1a_int_count_load : std_logic;
    signal l1a_int_int_id_sclr : std_logic;

    --RL: adding l1id internal counter reset.
    signal l1a_int_counter_reset : std_logic := '0';
    signal flag_int_counter_reset : std_logic := '0';

--**************************** Main Body of Code *******************************
begin

    --  Static signal Assigments
    tied_to_ground_i                             <= '0';
    tied_to_ground_vec_i                         <= x"0000000000000000";
    tied_to_vcc_i                                <= '1';
    tied_to_vcc_vec_i                            <= "11111111";



    L1A_INT_MAX_COUNT    <= "0000" & register_map_control_40xtal.FELIG_GLOBAL_CONTROL.FAKE_L1A_RATE;



    l1a_int_counter : dsp_counter
        PORT MAP (
            CLK    => clk_xtal_40,
            CE    => '1',
            SCLR  => '0',
            UP    => '0',
            LOAD  => l1a_int_count_load,
            L(47 downto 32)    => X"0000",
            L(31 downto  0)    => l1a_int_max_count,
            Q(47 downto 32)    => l1a_int_max_count_extra, --MT SIMU+
            Q(31 downto  0)    => l1a_int_count
        );

    l1a_int_int_id_counter : dsp_counter
        PORT MAP (
            CLK    => clk_xtal_40,
            CE    => l1a_int_trigger,
            SCLR  => l1a_int_int_id_sclr,
            UP    => '1',
            LOAD  => l1a_int_counter_reset, --RL
            L    => X"000000000000",
            Q(47 downto 16)    => l1a_int_int_id_extra, --MT SIMU+ open,
            Q(15 downto  0)    => l1a_int_int_id
        );

    --RL: adding l1id internal counter reset.
    process(clk_xtal_40)
    begin
        if clk_xtal_40'event and clk_xtal_40='1' then
            if lane_control(0).global.l1a_counter_reset = '1' and flag_int_counter_reset = '0' then
                l1a_int_counter_reset <= '1';
                flag_int_counter_reset <= '1';
            elsif l1a_int_int_id  = X"0000" then
                l1a_int_counter_reset <= '0';
                if lane_control(0).global.l1a_counter_reset = '0' then
                    flag_int_counter_reset <= '0';
                end if;
            end if;
        end if;
    end process;

    process(clk_xtal_40)
    begin
        if clk_xtal_40'event and clk_xtal_40='1' then
            if l1a_int_max_count = 0 then
                l1a_int_trigger <= '0';
                l1a_int_int_id_sclr <= '1';
                l1a_int_count_load <= '1';
            elsif l1a_int_count = 0 then

                l1a_int_trigger <= '1';
                l1a_int_int_id_sclr <= '0';
                l1a_int_count_load <= '1';
            else
                l1a_int_trigger <= '0';
                l1a_int_int_id_sclr <= '0';
                l1a_int_count_load <= '0';
            end if;
        end if;
    end process;

    -- CXP1 links 1/2   ( gt2/ gt0)
    -- CXP1 links 3/4   ( gt3/ gt1)
    -- CXP1 links 5/6   ( gt5/ gt4)
    -- CXP1 links 7/8   ( gt7/ gt6)
    -- CXP1 links 9/10  ( gt8/gt10)
    -- CXP1 links 11/12 ( gt9/gt11)
    -- CXP2 links 1/2   (gt14/gt12)
    -- CXP2 links 3/4   (gt15/gt13)
    -- CXP2 links 5/6   (gt17/gt16)
    -- CXP2 links 7/8   (gt19/gt18)
    -- CXP2 links 9/10  (gt20/gt22)
    -- CXP2 links 11/12 (gt21/gt23)


    emulator_inst : for igbt in GBT_NUM-1 downto 0 generate

        gbt : entity work.emulator
            generic map (
                GEN_ILA      => false,
                useGBTdataEmulator          => false,
                sim_emulator                  => false,
                LANE_ID                  => igbt
            )
            port map (
                clk_xtal_40            => clk_xtal_40,
                --MT
                --          lane_rxclk_240    => gt_rxusrclk_in(igbt),
                --          lane_txclk_240    => gt_txusrclk_in(igbt),
                lane_rxclk            => gt_rxusrclk_in(igbt),
                lane_txclk          => gt_txusrclk_in(igbt),

                rx_clk_div_2_mon          => gt_rx_clk_div_2_mon(igbt),
                tx_clk_div_2_mon          => gt_tx_clk_div_2_mon(igbt),

                gth_rxusrclk2            => gt_rxusrclk_in(igbt),
                gth_txusrclk2            => gt_txusrclk_in(igbt),


                l1a_int_trigger    => l1a_int_trigger,
                l1a_int_int_id    => l1a_int_int_id,
                l1a_trigger_out    => l1a_trigger_out(igbt),

                --MT
                --          gbt_tx_data_120b_out    => gbt_tx_data_120b_array_out(igbt),
                --          gbt_rx_data_120b_in     => gbt_rx_data_120b_array_in(igbt),
                gbt_tx_data_228b_out    => link_tx_data_228b_array_out(igbt),
                gbt_rx_data_120b_in     => link_rx_data_120b_array_in(igbt),
                gbt_tx_flag_in          => link_tx_flag_in(igbt),
                gbt_rx_flag_in          => link_tx_flag_in(igbt),

                lane_control    => lane_control(igbt),
                lane_monitor    => lane_monitor(igbt),

                --Other registers

                SELECT_RANDOM           => register_map_control_40xtal.FMEMU_RANDOM_CONTROL.SELECT_RANDOM,
                FMEMU_RANDOM_RAM_ADDR   => register_map_control_40xtal.FMEMU_RANDOM_RAM_ADDR ,
                FMEMU_RANDOM_RAM        => register_map_control_40xtal.FMEMU_RANDOM_RAM      ,
                FMEMU_RANDOM_CONTROL    => register_map_control_40xtal.FMEMU_RANDOM_CONTROL,
                gbt_frame_locked_in     => link_frame_locked_array_in(igbt),
                --SS (SWAP LSB MSB). RL changed so it works
                --fhCR_REVERSE_10B        => register_map_control_40xtal.ENCODING_REVERSE_10B(register_map_control_40xtal.ENCODING_REVERSE_10B'low),--fhCR_REVERSE_10B ,
                LINKSconfig       => LINKSconfig
            );

    end generate;

end RTL;
