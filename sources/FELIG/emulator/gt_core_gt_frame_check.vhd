--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	Michael Oberling
--
-- Design Name:	gt_core_GT_FRAME_CHECK
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Coming soon.
--
-- Change Log:	V1.0 - 
--
--==============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use ieee.std_logic_unsigned.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

use work.ip_lib.ALL;

entity gt_core_GT_FRAME_CHECK is
port
(
	RX_DATA_IN			: in	std_logic_vector(19 downto 0);
	ERROR_COUNT_OUT		: out	std_logic_vector(7 downto 0);
	RX_DATA_OUT			: out	std_logic_vector(15 downto 0);
	SLIDE_OUT			: out	std_logic;
	AUTO_RXRST_OUT		: out	std_logic;
	USER_CLK			: in	std_logic;
	SYSTEM_RESET		: in	std_logic;
	monitor_error_bit	: out	std_logic;
	monitor_error_frame	: out	std_logic;
	monitor_error_seed	: out	std_logic;
	monitor_error_word	: out	std_logic
);
end entity gt_core_GT_FRAME_CHECK;

architecture arch of gt_core_GT_FRAME_CHECK is
	signal code_err						: std_logic_vector(1 downto 0);
	signal diag_state					: std_logic_vector(7 downto 0);
	signal disp_err						: std_logic_vector(1 downto 0);
	signal frame_data					: std_logic_vector(15 downto 0);
	signal frame_data_valid				: std_logic;
	signal kout							: std_logic_vector(1 downto 0);
	signal monitor_check_word			: std_logic_vector(15 downto 0);
	signal monitor_checked_frames		: std_logic_vector(63 downto 0);
	signal monitor_data					: std_logic_vector(15 downto 0);
	signal monitor_error_count_bit		: std_logic_vector(31 downto 0);
	signal monitor_error_count_frame	: std_logic_vector(31 downto 0);
	signal monitor_error_count_seed		: std_logic_vector(31 downto 0);
	signal monitor_error_count_word		: std_logic_vector(31 downto 0);
	signal monitor_frame_count			: std_logic_vector(63 downto 0);
	signal run_disp						: std_logic_vector(1 downto 0);
	signal rx_data						: std_logic_vector(15 downto 0);
	signal slide_holdoff_counter		: std_logic_vector(11 downto 0);
	signal slide_out_i					: std_logic;
	signal system_reset_r				: std_logic;
	signal system_reset_r2				: std_logic;
	
	attribute keep						: string;
	attribute keep of system_reset_r	: signal is "true";
	attribute keep of system_reset_r2	: signal is "true";
	
	attribute ASYNC_REG						: string;
	attribute ASYNC_REG of system_reset_r	: signal is "TRUE";
	attribute ASYNC_REG of system_reset_r2	: signal is "TRUE";
begin
	ERROR_COUNT_OUT <= monitor_error_count_bit (ERROR_COUNT_OUT'range);
	
	process( USER_CLK )
	begin
		if(USER_CLK'event and USER_CLK = '1') then
			system_reset_r <= SYSTEM_RESET; 
			system_reset_r2 <= system_reset_r; 
			RX_DATA_OUT <= rx_data;
			
			frame_data <= rx_data;
			if (kout = "00") then
				frame_data_valid <= '1';
			else
				frame_data_valid <= '0';
			end if;
		end if;
	end process; 

	process( USER_CLK )
	begin
		if(USER_CLK'event and USER_CLK = '1') then
			if (slide_holdoff_counter = 0) then
				slide_holdoff_counter <= (others => '1');
				if (code_err /= "00") then
					SLIDE_OUT <= '1';
					AUTO_RXRST_OUT <= '0';
				elsif (kout(0) /= kout(1)) then
					SLIDE_OUT <= '0';
					AUTO_RXRST_OUT <= '1';
				else
					SLIDE_OUT <= '0';
					AUTO_RXRST_OUT <= '0';
				end if;				
			else
				slide_holdoff_counter <= slide_holdoff_counter - 1;
				SLIDE_OUT <= '0';
				AUTO_RXRST_OUT <= '0';
			end if;
		end if;
		
	end process;
	
	decoder_9_0 : entity work.decode_8b10b_wrapper
	port map (
		clk			=> USER_CLK,
		din			=> RX_DATA_IN(9 downto 0),
		dout		=> rx_data(7 downto 0),
		kout		=> kout(0),
		
		sinit		=> system_reset_r2,
		code_err	=> code_err(0),
		disp_err	=> disp_err(0),
		run_disp	=> run_disp(0)
	);
	
	decoder_19_10 : entity work.decode_8b10b_wrapper
	port map (
		clk			=> USER_CLK,
		din			=> RX_DATA_IN(19 downto 10),
		dout		=> rx_data(15 downto 8),
		kout		=> kout(1),
		
		sinit		=> system_reset_r2,
		code_err	=> code_err(1),
		disp_err	=> disp_err(1),
		run_disp	=> run_disp(1)
	);
	
	comp_bert_frame_check : entity work.bert_frame_check
	port map (
		clk_word => USER_CLK,
		
		frame_data					=> frame_data,
		frame_data_valid			=> frame_data_valid,
		
		diag_state					=> diag_state,
		monitor_counter_reset		=> system_reset_r2,
		monitor_data				=> monitor_data,
		monitor_error_bit			=> monitor_error_bit,
		monitor_error_frame			=> monitor_error_frame,
		monitor_error_seed			=> monitor_error_seed,
		monitor_error_word			=> monitor_error_word,
		monitor_error_count_bit		=> monitor_error_count_bit,
		monitor_error_count_frame	=> monitor_error_count_frame,
		monitor_error_count_seed	=> monitor_error_count_seed,
		monitor_error_count_word	=> monitor_error_count_word,
		monitor_frame_count			=> monitor_frame_count,
		monitor_checked_frames		=> monitor_checked_frames,
		monitor_check_word			=> monitor_check_word
	);
end architecture arch;

