--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Shelfali Saxena
--!               mtrovato
--!               Ricardo Luz
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineeer: Initially created as FELIG_lane_wrapper for HG710 by John Anderson, Michael Oberling, Soo Ryu
-- modified for FLX-712 by Marco Trovato
--
-- Design Name:  Emulator
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

--==============
LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;
--use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;
    use work.function_lib.ALL;

    use work.pcie_package.all;
    use work.centralRouter_package.all;

entity Emulator is
    generic (
        GEN_ILA          : boolean  := false;
        useGBTdataEmulator    : boolean  := false;
        sim_emulator        : boolean  := false;
        LANE_ID          : integer   := 0;
        NUMELINKmax           : integer := 112;
        NUMEGROUPmax          : integer := 7
    );
    port (
        --================================
        -- Clocks & Reset
        --================================
        clk_xtal_40      : in  std_logic;
        --MT
        --  lane_rxclk_240    : in  std_logic;
        --  lane_txclk_240    : in  std_logic;
        lane_rxclk            : in  std_logic;
        lane_txclk            : in  std_logic;

        rx_clk_div_2_mon          : out  std_logic;
        tx_clk_div_2_mon          : out  std_logic;

        gth_rxusrclk2            : in  std_logic;
        gth_txusrclk2            : in  std_logic;

        --================================
        -- L1A Interface
        --================================
        --internally generated trigger l1a and id. Generation in gt_core_exdes
        l1a_int_trigger    : in  std_logic;
        l1a_int_int_id    : in  std_logic_vector(15 downto 0);
        l1a_trigger_out    : out  std_logic;

        --================================
        -- DATA
        --================================
        --MT
        --  gbt_tx_data_120b_out          : out std_logic_vector(119 downto 0);
        --  gbt_rx_data_120b_in           : in  std_logic_vector(119 downto 0);
        --gbt_tx_data_256b_out          : out std_logic_vector(255 downto 0);
        gbt_tx_data_228b_out          : out std_logic_vector(227 downto 0);
        gbt_rx_data_120b_in           : in  std_logic_vector(119 downto 0);
        gbt_tx_flag_in                : in std_logic;
        gbt_rx_flag_in                : in std_logic;
        --================================
        -- Control and Status Interface
        --================================
        lane_control            : in  lane_control_type;
        lane_monitor            : out  lane_monitor_type;
        SELECT_RANDOM : in std_logic_vector(0 downto 0);
        FMEMU_RANDOM_RAM_ADDR         : in std_logic_vector(9 downto 0);    -- Controls the address of the ramblock for the random number generator
        FMEMU_RANDOM_RAM              : in bitfield_fmemu_random_ram_t_type;
        FMEMU_RANDOM_CONTROL          : in bitfield_fmemu_random_control_w_type;
        gbt_frame_locked_in           : in std_logic;
        --SS (SWAP LSB MSB)
        --fhCR_REVERSE_10B              : in  std_logic;
        LINKSconfig                   : in std_logic_vector(2 downto 0)
    );
end entity Emulator;

architecture Behavioral of Emulator is
    signal emu_data_120b        : std_logic_vector(119 downto 0)  := (others => '0');
    signal ext_l1a_id          : std_logic_vector( 15 downto 0)  := (others => '0');
    signal ext_l1a_pipe          : std_logic_vector(  3 downto 0)  := (others => '0');
    signal ext_l1a_select        : std_logic              := '0';
    signal ext_l1a_trigger        : std_logic              := '0';
    signal ext_l1id            : std_logic              := '0';
    signal freq_rx_clk          : std_logic_vector( 31 downto 0)  := (others => '0');
    signal freq_tx_clk          : std_logic_vector( 31 downto 0)  := (others => '0');
    signal gbt_extracted_bchan      : std_logic              := '0';
    signal gbt_extractedl1a        : std_logic              := '0';
    signal l1a_int_trigger_pipe        : std_logic_vector(  3 downto 0)  := (others => '0');
    signal l1a_int_trigger_piped    : std_logic              := '0';
    signal gbt_frame_locked        : std_logic              := '0';
    signal gbt_frame_locked_a      : std_logic_vector( 47 downto 0)  := (others => '0');
    signal gbt_frame_locked_c      : std_logic              := '0';
    signal gbt_rx_data_120b        : std_logic_vector(119 downto 0)  := (others => '0');
    signal gbt_rx_flag          : std_logic              := '0';
    signal gbt_tx_data_120b        : std_logic_vector(119 downto 0)  := (others => '0');
    signal gbt_tx_flag          : std_logic              := '0';
    signal gbtbitsel          : std_logic_vector(  6 downto 0)  := (others => '0');
    signal b_ch_bit_sel          : std_logic_vector(  6 downto 0)  := (others => '0');
    signal l1a_id            : std_logic_vector( 15 downto 0)  := (others => '0');
    signal l1a_trigger          : std_logic              := '0';
    signal l1id              : std_logic_vector( 15 downto 0)  := (others => '0');
    signal l1a_ext_int_id              : std_logic_vector( 15 downto 0)  := (others => '0'); --MT
    signal l1a_ext_int_id_extra              : std_logic_vector( 31 downto 0)  := (others => '0'); --MT
    signal lane_reset          : std_logic              := '0';
    --signal tx_120b_from_mach      : std_logic_vector(119 downto  0)  := (others => '0');

    --Phase1: 80b, Phase2: max payload 224
    signal chB_l1a_id          : std_logic_vector( 23 downto  0)  := (others => '0');
    signal chB_busy            : std_logic              := '0';
    signal elink_sync          : std_logic              := '0';

    signal emu_data_re          : std_logic_vector(0 to NUMEGROUPmax-1); --(0 to  4);
    signal emu_data_out          : array_of_slv_9_0(0 to NUMEGROUPmax-1); --(0 to  4);
    --ph1
    --signal elink_data_in        : array_of_slv_9_0(0 to 39);
    --signal elink_data_re        : std_logic_vector(0 to 39) ;
    --ph2
    signal elink_data_in        : array_of_slv_9_0(0 to NUMELINKmax-1);
    signal elink_data_in_pipe       : array_of_slv_9_0(0 to NUMELINKmax-1);
    signal elink_data_in_zero       : array_of_slv_9_0(0 to NUMELINKmax-41);
    signal elink_data_re        : std_logic_vector(0 to NUMELINKmax-1) ;
    --signal elink_data_re_ph1      : std_logic_vector(0 to NUMELINKmax-1) ;
    signal elink_data_re_ph2      : std_logic_vector(0 to NUMELINKmax-1) ;

    signal elink_control        : lane_elink_control_array(0 to NUMELINKmax-1);
    signal emu_control          : lane_emulator_control_array(0 to NUMEGROUPmax-1); --(4 downto 0);
    constant epath            : std_logic_vector(4 downto 0) := (others => '0');
    signal TTC_out            : std_logic_vector(9 downto 0) := (others => '0');
    signal TTC_ToHost_Data        : TTC_ToHost_data_type;


    signal ila_data_gen_out      : array_of_slv_17_0  (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_fifo_out        : array_of_slv_9_0  (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_data_gen_we      : std_logic_vector  (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_gbt_word_gen_state  : std_logic_vector  (3 downto 0);

    signal ila_count_chk_out        : array_of_slv_10_0_MT (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_count_chk_out2        : array_of_slv_10_0_MT(0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_count_upstfifochk     : array_of_slv_10_0_MT(0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_fifo_flush            : std_logic_vector    (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_isEOP_chk2 : std_logic_vector               (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_efifoDout_8b10b      : array_of_slv_9_0     (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_enc10bitRdy          : std_logic_vector     (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_efifoFull : std_logic_vector                (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_efifoEmpty : std_logic_vector               (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_efifoPfull : std_logic_vector               (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_elink_data_re : std_logic_vector            (0 to NUMEGROUPmax-1); --(0 to 4);
    signal ila_efifoDoutRdy : std_logic_vector             (0 to NUMEGROUPmax-1); --(0 to 4);
    signal chunk_length_out : array_of_slv_11_0_MT         (0 to NUMEGROUPmax-1); --(0 to 4);
    --

    signal ila_count_pyld : std_logic_vector(8 downto 0) := (others => '0');

    signal ila_testpass_gwchk : std_logic_vector(3 downto 0) := "0000";
    signal ila_dist_gwchk : std_logic_vector(3 downto 0) := "0000";
    signal ila_word10b_gwchk : std_logic_vector(9 downto 0) := (others => '0');

    signal ila_phlck_gwchk : std_logic := '0';  --decide the 2b phase in the 10b word
    signal ila_en_gwchk : std_logic := '0';
    signal ila_count_gwchk : std_logic_vector(2 downto 0) := "000";
    signal ila_count_start_gwchk : std_logic_vector(1 downto 0) := "00";
    --

    signal payload_224b        : std_logic_vector(223 downto  0)  := (others => '0');
    signal payload_224b_pipe  : array_of_slv_223_0(0 to 1);
    --signal payload_224b_ph1        : std_logic_vector(223 downto  0)  := (others => '0');
    signal payload_224b_ph2        : std_logic_vector(223 downto  0)  := (others => '0');
    --signal payload_224b_pipe_ph1  : array_of_slv_223_0(0 to 1);
    signal payload_224b_pipe_ph2  : array_of_slv_223_0(0 to 1);
    signal HDR_4b             : std_logic_vector(3 downto 0);
    signal HDR_4b_pipe        : array_of_slv_3_0(0 to 1);
    signal IC_2b              : std_logic_vector(1 downto 0);
    signal IC_2b_pipe         : array_of_slv_1_0(0 to 1);
    signal EC_2b              : std_logic_vector(1 downto 0);
    signal EC_2b_pipe         : array_of_slv_1_0(0 to 1);
    signal FEC_48b            : std_logic_vector(47 downto 0);
    signal FEC_48b_pipe       : array_of_slv_47_0(0 to 1);
    signal LM_2b              : std_logic_vector(1 downto 0);
    signal LM_2b_pipe         : array_of_slv_1_0(0 to 1);

    --RL payload

    --signal payload_80b_gbt                        : std_logic_vector( 79 downto  0)  := (others => '0');
    --signal payload_112b_lpgbt_DR512_FEC5        : std_logic_vector(111 downto  0)  := (others => '0');
    --signal payload_96b_lpgbt_DR512_FEC12        : std_logic_vector( 95 downto  0)  := (others => '0');
    --signal payload_224b_lpgbt_DR1024_FEC5        : std_logic_vector(223 downto  0)  := (others => '0');
    --signal payload_192b_lpgbt_DR1024_FEC12      : std_logic_vector(191 downto  0)  := (others => '0');

    -- RL sim only. delete later
    --signal payload_egroup0    : std_logic_vector (31 downto 0)  := (others => '0');
    --signal payload_egroup1    : std_logic_vector (31 downto 0)  := (others => '0');
    --signal payload_egroup2    : std_logic_vector (31 downto 0)  := (others => '0');
    --signal payload_egroup3    : std_logic_vector (31 downto 0)  := (others => '0');
    --signal payload_egroup4    : std_logic_vector (31 downto 0)  := (others => '0');
    --signal payload_egroup5    : std_logic_vector (31 downto 0)  := (others => '0');
    --signal payload_egroup6    : std_logic_vector (31 downto 0)  := (others => '0');

    signal payload_egroup0_ph2    : std_logic_vector (31 downto 0)  := (others => '0');
    signal payload_egroup1_ph2    : std_logic_vector (31 downto 0)  := (others => '0');
    signal payload_egroup2_ph2    : std_logic_vector (31 downto 0)  := (others => '0');
    signal payload_egroup3_ph2    : std_logic_vector (31 downto 0)  := (others => '0');
    signal payload_egroup4_ph2    : std_logic_vector (31 downto 0)  := (others => '0');
    signal payload_egroup5_ph2    : std_logic_vector (31 downto 0)  := (others => '0');
    signal payload_egroup6_ph2    : std_logic_vector (31 downto 0)  := (others => '0');

    --RL others

    type STATEM  is (st_idle, st_sync, st_generate) ;
    signal state : STATEM := st_idle;

    signal start_gen        : std_logic;
    signal gen_done         : std_logic_vector(0 to NUMEGROUPmax-1) := (others => '0');
    constant gen_done_1     : std_logic_vector(0 to NUMEGROUPmax-1) := (others => '1');
    constant gen_done_0     : std_logic_vector(0 to NUMEGROUPmax-1) := (others => '0');
    signal egroup_enable    : std_logic_vector(0 to NUMEGROUPmax-1);
    signal flag_data_gen    : std_logic_vector(0 to NUMEGROUPmax-1);
    signal flag_sync        : std_logic_vector(0 to NUMEGROUPmax-1);
    signal count_sync       : std_logic_vector(0 to 5) := "000000";
    signal sync_read_enable : std_logic :='0';

    signal or_enable        : std_logic;
    signal fifo_empty       : std_logic_vector(0 to 6);

    signal out_of_sync      : std_logic_vector(NUMEGROUPmax-1 downto 0) := (others => '0');
    signal or_out_of_sync   : std_logic;
    signal will_sync        : std_logic := '0';
    signal flag_will_sync   : std_logic := '0';

    --signal elink_control_enable_array   : array_of_slv_7_0  (NUMEGROUP-1 downto 0);
    --signal elink_data_re_array          : array_of_slv_7_0  (NUMEGROUP-1 downto 0);
    --signal elink_data_re_array_0        : array_of_slv_7_0  (NUMEGROUP-1 downto 0);

    --adding l1id external counter reset.
    signal l1a_ext_counter_reset    : std_logic := '0';
    signal flag_ext_counter_reset   : std_logic := '0';
    signal MSBfirst         : std_logic := '0';

    --signal  gbt_tx_data_256b_out_temp  : std_logic_vector(255 downto 0);
    signal  gbt_tx_data_228b_out_temp  : std_logic_vector(227 downto 0);
    --signal  gbt_tx_data_228b_out_temp_ph1  : std_logic_vector(227 downto 0);
    signal  gbt_tx_data_228b_out_temp_ph2  : std_logic_vector(227 downto 0);

    --RL ILA
    --signal ila_l1a_trigger              : std_logic;
    --signal ila_l1a_id                   : std_logic_vector(15 downto 0);
    --signal ila_payload_80b_gbt          : std_logic_vector(79 downto 0);
    --signal ila_clk_xtal_40              : std_logic;
    --signal ila_gbt_tx_data_120b         : std_logic_vector(119 downto 0);
    --signal ila_emu_data_out             : array_of_slv_9_0(0 to NUMEGROUPmax-1);
    --signal ila_emu_control_output_width : array_of_slv_2_0(0 to NUMEGROUPmax-1);

    --signal payload_80b_gbt_test         : std_logic_vector(79 downto 0);
    --signal payload_80b_gbt_test_ph1     : std_logic_vector(79 downto 0);
    signal payload_80b_gbt_test_ph2     : std_logic_vector(79 downto 0);
    signal aurora_en                    : std_logic_vector(0 to NUMELINKmax-1);
    signal aurora_en_EGROUP             : std_logic_vector(0 to NUMEGROUPmax-1);
--
--    COMPONENT  ila_0 IS
--    PORT (
--    clk : IN STD_LOGIC;
--    probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
--    probe2 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe3 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe4 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe5 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe6 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe7 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe8 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe9 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe10 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe11 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe12 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe13 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe14 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe15 : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
--    probe16 : IN STD_LOGIC_VECTOR(79 DOWNTO 0);
--    probe17 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
--    probe18 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
--    probe19 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
--    probe20 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
--    probe21 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
--    probe22 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
--    probe23 : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
--    probe24 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe25 : IN STD_LOGIC_VECTOR(119 DOWNTO 0));
--  END COMPONENT;
begin

    l1a_trigger_out <= l1a_trigger;

    --MT  u0_mon: process(lane_rxclk_240)
    u0_mon: process(lane_rxclk)
    begin
        --MT    if rising_edge(lane_rxclk_240) then
        if rising_edge(lane_rxclk) then
            if (gbt_rx_flag = '1') then
                gbt_frame_locked_a(47 downto 1) <= gbt_frame_locked_a(46 downto 0);
                gbt_frame_locked_a(0) <= gbt_frame_locked;
                if gbt_frame_locked_a = x"FFFFFFFFFFFF" then
                    gbt_frame_locked_c <= '1';
                else
                    gbt_frame_locked_c <= '0';
                end if;
            end if;
        end if;
    end process u0_mon;

    lane_monitor.gbt.frame_locked    <= gbt_frame_locked_c  ;
    ext_l1a_select            <= lane_control.global.l1a_source      ;
    gbtbitsel              <= lane_control.global.a_ch_bit_sel      ;
    b_ch_bit_sel        <= lane_control.global.b_ch_bit_sel      ;
    emu_control        <= lane_control.emulator          ;
    elink_control        <= lane_control.elink            ;
    lane_monitor.global.l1a_id      <= X"0000" & l1a_id                         ; --RL
    MSBfirst <= lane_control.global.MSB;
    --================================================================
    -- Frequency Monitors
    --================================================================
    gen_freq_counter : if sim_emulator = false generate
        --MT    freq_counter_lane_rxclk_240: entity work.clock_frequncy_counter
        freq_counter_lane_rxclk: entity work.clock_frequncy_counter
            generic map (
                clk_timebase_frequency    => 40000000.0,  -- Units: Hz
                accumulation_period    => 32.0,      -- Units: Seconds  / 32-bit counter allows max 4,294,967,295 counts; @ 1 second can only count to 50,000,000
                clk_meas_prescale_factor  => 5 -- 4 should be enough, but just to be sure...
            -- clk_meas_prescale divides down the clk_meas frequency
            -- (clk_meas freq) / (2^clk_meas_prescale_factor)  must be less than (clk_timebase freq) / 2
            -- values other than 1 will limit resolution.
            -- Minimum value = 1
            )
            -- Port list
            port map (
                -- stable local clock input
                -- must be at least twice as fast at the clk_meas / clk_meas_prescale
                clk_timebase      => clk_xtal_40,
                -- clk to measure
                --MT        clk_meas      => lane_rxclk_240,
                clk_meas      => lane_rxclk,
                -- output count
                frequency      => freq_rx_clk, -- because prescale and accumulation are the same value, unit here will be in Hz.
                div_2_clock_signal    => rx_clk_div_2_mon,
                prescaled_clock_signal          => open
            );

        --    freq_counter_lane_txclk_240: entity work.clock_frequncy_counter
        freq_counter_lane_txclk: entity work.clock_frequncy_counter
            generic map (
                clk_timebase_frequency    => 40000000.0,  -- Units: Hz
                accumulation_period    => 32.0,      -- Units: Seconds  / 32-bit counter allows max 4,294,967,295 counts; @ 1 second can only count to 50,000,000
                clk_meas_prescale_factor  => 5 -- 4 should be enough, but just to be sure...
            -- clk_meas_prescale divides down the clk_meas frequency
            -- (clk_meas freq) / (2^clk_meas_prescale_factor)  must be less than (clk_timebase freq) / 2
            -- values other than 1 will limit resolution.
            -- Minimum value = 1
            )
            -- Port list
            port map (
                -- stable local clock input
                -- must be at least twice as fast at the clk_meas / clk_meas_prescale
                clk_timebase      => clk_xtal_40,
                -- clk to measure
                --MT        clk_meas      => lane_txclk_240,
                clk_meas      => lane_txclk,
                -- output count
                frequency      => freq_tx_clk, -- because prescale and accumulation are the same value, unit here will be in Hz.
                div_2_clock_signal    => tx_clk_div_2_mon,
                prescaled_clock_signal          => open
            );
    end generate gen_freq_counter;

    gbt_frame_locked <= gbt_frame_locked_in;
    gbt_rx_data_120b <= gbt_rx_data_120b_in;
    gbt_rx_flag <=  gbt_rx_flag_in;  --from gbtTxRx_FELIX
    gbt_tx_flag <= gbt_tx_flag_in;  --from gbtTxRx_FELIX

    --================================================================
    -- L1A Extraction
    --================================================================
    --****************************
    --*******EXTERNAL TRIGGER*****
    --*******EXT L1A, INT L1AID***
    --****************************
    --1a) Extraction from RX_DATA (External)
    l1a_mux: entity work.mux_128_sync
        port map (
            --    clk              => rx_frame_clk_bufr,
            --MT      clk      => lane_rxclk_240,
            clk      => lane_rxclk,
            bit_input(127 downto 121)  => "0000000" ,
            bit_input(120)    => '1',
            bit_input(119 downto 0)  => gbt_rx_data_120b,
            bit_select    => gbtbitsel,
            bit_output_sync    => gbt_extractedl1a,
            bit_output_aync    => open
        );

    bchan_mux: entity work.mux_128_sync
        port map (
            --    clk              => rx_frame_clk_bufr,
            --MT      clk      => lane_rxclk_240,
            clk      => lane_rxclk,
            bit_input(127 downto 121)  => "0000000" ,
            bit_input(120)    => '1',
            bit_input(119 downto 0)  => gbt_rx_data_120b,
            bit_select    => b_ch_bit_sel,
            bit_output_sync    => gbt_extracted_bchan,
            bit_output_aync    => open
        );

    --1b) L1A Domain Cross (External)
    --MT  ext_l1a_generator_domain_cross_a: process(TTC_out(0), lane_txclk_240)
    ext_l1a_generator_domain_cross_a: process(TTC_out(0), lane_txclk)
    begin
        if (TTC_out(0) = '1') then
            ext_l1a_pipe(0) <= '1';
        --MT    elsif lane_txclk_240'event and lane_txclk_240='1' then
        elsif lane_txclk'event and lane_txclk='1' then
            if (ext_l1a_pipe(2) = '1') then
                ext_l1a_pipe(0) <= '0';
            end if;
        end if;
    end process;

    --MT  ext_l1a_generator_domain_cross_b: process(lane_txclk_240)
    ext_l1a_generator_domain_cross_b: process(lane_txclk)
    begin
        --MT    if lane_txclk_240'event and lane_txclk_240='1' then
        if lane_txclk'event and lane_txclk='1' then
            ext_l1a_pipe(1)  <= ext_l1a_pipe(0);
            ext_l1a_pipe(2)  <= ext_l1a_pipe(1);
            ext_l1a_pipe(3)  <= ext_l1a_pipe(2);
        end if;
    end process;

    --MT  ext_l1a_generator: process(lane_txclk_240)
    ext_l1a_generator: process(lane_txclk)
    begin
        --MT    if lane_txclk_240'event and lane_txclk_240='1' then
        if lane_txclk'event and lane_txclk='1' then
            if (ext_l1a_pipe (3) = '0' and ext_l1a_pipe(2) = '1') then
                ext_l1a_trigger <= '1';
                --internally generated, but relying on external l1a
                ext_l1a_id <= l1a_ext_int_id;
            else
                ext_l1a_trigger <= '0';
            end if;
        end if;
    end process;



    --1c) L1A ID internal generation based on External trigger
    l1a_ext_int_id_counter : dsp_counter
        PORT MAP (
            --MT      CLK          => lane_txclk_240,
            CLK          => lane_txclk,
            CE              => ext_l1a_trigger,
            SCLR          => '0',
            UP    => '1',
            LOAD          => l1a_ext_counter_reset,--'0',
            L    => X"000000000000",
            Q(47 downto 16)  => l1a_ext_int_id_extra, --open,
            Q(15 downto  0)  => l1a_ext_int_id
        );
    --
    --RL: adding l1id external counter reset.
    process(lane_txclk)
    begin
        if lane_txclk'event and lane_txclk='1' then
            if lane_control.global.l1a_counter_reset = '1' and flag_ext_counter_reset = '0' then
                l1a_ext_counter_reset <= '1';
                flag_ext_counter_reset <= '1';
            elsif l1a_ext_int_id  = X"0000" then
                l1a_ext_counter_reset <= '0';
                if lane_control.global.l1a_counter_reset = '0' then
                    flag_ext_counter_reset <= '0';
                end if;
            end if;
        end if;
    end process;


    --****************************
    --*******INTERNAL TRIGGER*****
    --*******INT L1A, INT L1AID***
    --****************************
    --2) From generated trigger in EmulatorWrapper (Internal)
    --MT  l1a_generator_domain_cross_a: process(l1a_int_trigger, lane_txclk_240)
    l1a_generator_domain_cross_a: process(l1a_int_trigger, lane_txclk)
    begin
        --    if (gbt_extractedl1a = '1') then
        if (l1a_int_trigger = '1') then
            l1a_int_trigger_pipe(0) <= '1';
        --MT    elsif (lane_txclk_240'event and lane_txclk_240='1') then
        elsif (lane_txclk'event and lane_txclk='1') then
            if (l1a_int_trigger_pipe(2) = '1') then
                l1a_int_trigger_pipe(0) <= '0';
            end if;
        end if;
    end process;

    --MT  l1a_generator_domain_cross_b: process(lane_txclk_240)
    l1a_generator_domain_cross_b: process(lane_txclk)
    begin
        --MT    if (lane_txclk_240'event and lane_txclk_240='1') then
        if (lane_txclk'event and lane_txclk='1') then
            l1a_int_trigger_pipe(1)  <= l1a_int_trigger_pipe(0);
            l1a_int_trigger_pipe(2)  <= l1a_int_trigger_pipe(1);
            l1a_int_trigger_pipe(3)  <= l1a_int_trigger_pipe(2);
        end if;
    end process;

    --MT  l1a_generator: process(lane_txclk_240)
    l1a_generator: process(lane_txclk)
    begin
        --MT    if (lane_txclk_240'event and lane_txclk_240='1') then
        if (lane_txclk'event and lane_txclk='1') then
            if (l1a_int_trigger_pipe (3) = '0' and l1a_int_trigger_pipe(2) = '1') then
                l1a_int_trigger_piped <= '1';
                l1id <= l1a_int_int_id; --MT for debugging set to const X"2DE4";
            else
                l1a_int_trigger_piped <= '0';
            end if;
        end if;
    end process;

    --****************************
    --*****L1a Selection Mux******
    --****************************
    --****************************
    --3) Selection between External and Internal trigger
    --MT  l1a_ext_mux: process(lane_txclk_240)
    l1a_ext_mux: process(lane_txclk)
    begin
        --MT    if lane_txclk_240'event and lane_txclk_240='1' then
        if lane_txclk'event and lane_txclk='1' then
            if (ext_l1a_select = '0') then
                l1a_trigger  <= l1a_int_trigger_piped;
                l1a_id    <= l1id;
            else
                l1a_trigger  <= ext_l1a_trigger;
                l1a_id    <= ext_l1a_id;
            end if;
        end if;
    end process;



    --================================================================
    -- Data Emulator
    --================================================================
    --****************************
    --****************************
    --1a) data generation: payload

    --  gbt_tx_data_120b_out <= gbt_tx_data_120b;

    --gbt_loopback_fifo_control: process (lane_txclk_240)
    --begin
    --  if (lane_txclk_240'event and lane_txclk_240 = '1') then
    --    gbt_tx_data_120b <= emu_data_120b;
    --  end if;
    --end process gbt_loopback_fifo_control;
    --EMU_DATA_MUX: process(lane_txclk_240)
    --begin
    --  if lane_txclk_240'event and lane_txclk_240='1' then
    --    emu_data_120b <= tx_120b_from_mach;
    --  end if;
    --end process;

    --PROTOCOL, DATARATE, FEC. 0.X,X = 4.8 Gbps    (GBT   Phase1),
    --                         1,0.0 = 5.12 FEC5   (lpGBT Phase2),
    --                         1,0.1 = 5.12 FEC12  (lpGBT Phase2),
    --                         1.1,0 = 10.24 FEC5  (lpGBT Phase2),
    --                         1.1,1 = 10.24 FEC12 (lpGBT Phase2)

    gbt_tx_data_228b_out <= gbt_tx_data_228b_out_temp_ph2;
    --RL restructured. only 224 data bits, IC an EC are used in the lpgbt wrapper
    --  gbt_tx_data_228b_out_temp <= x"000000000000000000000000000"       &
    --                               HDR_4b_pipe(1)                       &
    --                               IC_2b_pipe(1)                        &
    --                               EC_2b_pipe(1)                        &
    --                               payload_224b_pipe(1)(159 downto 144) &
    --                               payload_224b_pipe(1)(127 downto 112) &
    --                               payload_224b_pipe(1)( 95 downto  80) &
    --                               payload_224b_pipe(1)( 63 downto  48) &
    --                               payload_224b_pipe(1)( 31 downto  16) &
    --                               FEC_48b_pipe(1)(31 downto 0)         when (LINKSconfig(2) = '0') else
    --                               IC_2b_pipe(1)                        &
    --                               EC_2b_pipe(1)                        &
    --                               payload_224b_pipe(1)                 ; --when lpgbt
    gbt_tx_data_228b_out_temp <= gbt_tx_data_228b_out_temp_ph2; --only used for ila


    --  gbt_tx_data_228b_out_temp_ph1 <= x"000000000000000000000000000"       &
    --                                  HDR_4b_pipe(1)                       &
    --                                  IC_2b_pipe(1)                        &
    --                                  EC_2b_pipe(1)                        &
    --                                  payload_224b_pipe_ph1(1)(159 downto 144) &
    --                                  payload_224b_pipe_ph1(1)(127 downto 112) &
    --                                  payload_224b_pipe_ph1(1)( 95 downto  80) &
    --                                  payload_224b_pipe_ph1(1)( 63 downto  48) &
    --                                  payload_224b_pipe_ph1(1)( 31 downto  16) &
    --                                  FEC_48b_pipe(1)(31 downto 0)         when (LINKSconfig(2) = '0') else
    --                                  IC_2b_pipe(1)                        &
    --                                  EC_2b_pipe(1)                        &
    --                                  payload_224b_pipe_ph1(1)             ; --when lpgbt

    gbt_tx_data_228b_out_temp_ph2 <= x"000000000000000000000000000"       &
                                     HDR_4b_pipe(1)                       &
                                     IC_2b_pipe(1)                        &
                                     EC_2b_pipe(1)                        &
                                     payload_224b_pipe_ph2(1)(143 downto 128) &
                                     payload_224b_pipe_ph2(1)(111 downto  96) &
                                     payload_224b_pipe_ph2(1)( 79 downto  64) &
                                     payload_224b_pipe_ph2(1)( 47 downto  32) &
                                     payload_224b_pipe_ph2(1)( 15 downto  0) &
                                     FEC_48b_pipe(1)(31 downto 0)         when (LINKSconfig(2) = '0') else
                                     IC_2b_pipe(1)                        &
                                     EC_2b_pipe(1)                        &
                                     payload_224b_pipe_ph2(1)             ; --when lpgbt



    --  payload_80b_gbt_test_ph1 <= gbt_tx_data_228b_out_temp_ph1(111 downto 32);
    payload_80b_gbt_test_ph2 <= gbt_tx_data_228b_out_temp_ph2(111 downto 32);

    --sim monitoring only, delete later
    --  payload_egroup0 <= payload_224b_pipe(1)( 31 downto   0);
    --  payload_egroup1 <= payload_224b_pipe(1)( 63 downto  32);
    --  payload_egroup2 <= payload_224b_pipe(1)( 95 downto  64);
    --  payload_egroup3 <= payload_224b_pipe(1)(127 downto  96);
    --  payload_egroup4 <= payload_224b_pipe(1)(159 downto 128);
    --  payload_egroup5 <= payload_224b_pipe(1)(191 downto 160);
    --  payload_egroup6 <= payload_224b_pipe(1)(223 downto 192);

    payload_egroup0_ph2 <= payload_224b_pipe_ph2(1)( 31 downto   0);
    payload_egroup1_ph2 <= payload_224b_pipe_ph2(1)( 63 downto  32);
    payload_egroup2_ph2 <= payload_224b_pipe_ph2(1)( 95 downto  64);
    payload_egroup3_ph2 <= payload_224b_pipe_ph2(1)(127 downto  96);
    payload_egroup4_ph2 <= payload_224b_pipe_ph2(1)(159 downto 128);
    payload_egroup5_ph2 <= payload_224b_pipe_ph2(1)(191 downto 160);
    payload_egroup6_ph2 <= payload_224b_pipe_ph2(1)(223 downto 192);

    --  EMU_DATA_MUX: process(lane_txclk_240)
    EMU_DATA_MUX: process(lane_txclk)
    begin
        --MT    if lane_txclk_240'event and lane_txclk_240='1' then
        if lane_txclk'event and lane_txclk='1' then
            payload_224b_pipe(1) <= payload_224b_pipe(0);
            payload_224b_pipe(0) <= payload_224b;
            --      payload_224b_pipe_ph1(1) <= payload_224b_pipe_ph1(0);
            --      payload_224b_pipe_ph1(0) <= payload_224b_ph1;
            payload_224b_pipe_ph2(1) <= payload_224b_pipe_ph2(0);
            payload_224b_pipe_ph2(0) <= payload_224b_ph2;
            HDR_4b_pipe(1)       <= HDR_4b_pipe(0);
            HDR_4b_pipe(0)       <= HDR_4b;
            IC_2b_pipe(1)        <= IC_2b_pipe(0);
            IC_2b_pipe(0)        <= IC_2b;
            EC_2b_pipe(1)        <= EC_2b_pipe(0);
            EC_2b_pipe(0)        <= EC_2b;
            FEC_48b_pipe(1)      <= FEC_48b_pipe(0);
            FEC_48b_pipe(0)      <= FEC_48b;
            LM_2b_pipe(1)        <= LM_2b_pipe(0);
            LM_2b_pipe(0)        <= LM_2b;
        end if;
    end process;


    --  gen_elink_muxer : if sim_emulator = false generate

    --RL: state machine that controls elink_sync.

    start_gen <= lane_control.global.elink_sync;
    sync_readenable : process(lane_txclk)
    begin
        if lane_txclk'event and lane_txclk ='1' then
            sm_sync: case state is
                when st_idle =>
                    sync_read_enable <= '0';
                    count_sync<="000000";
                    if l1a_trigger = '1' then
                        state <= st_generate;
                    elsif start_gen = '1' then
                        state <= st_sync;
                    else
                        state <= st_idle;
                    end if;
                when st_sync =>
                    gen_done <= gen_done_0;
                    if(count_sync = "111111") then
                        if(will_sync = '1') then
                            sync_read_enable <= '1';--'1';
                            will_sync <= '0';
                        end if;
                        count_sync<="000000";
                    else
                        sync_read_enable <= '0';
                        count_sync <= count_sync + '1';
                    end if;
                    if l1a_trigger = '1' then
                        state <= st_generate;
                        count_sync<="000000";
                    else
                        state <= st_sync;
                    end if;
                when st_generate =>
                    sync_read_enable <= '0';
                    for i in 0 to NUMEGROUPmax-1 loop
                        if(flag_sync(i) = '1') then
                            gen_done(i) <= '1';
                        end if;
                    end loop;
                    if gen_done = gen_done_1 or start_gen = '1' then
                        state <= st_sync;
                    else
                        state <= st_generate;
                    end if;
            end case sm_sync;
            if ( lane_control.global.elink_sync = '1' or or_out_of_sync = '1' ) and flag_will_sync = '0' then
                will_sync <= '1';
                flag_will_sync <= '1';
            elsif lane_control.global.elink_sync = '0' and or_out_of_sync = '0' and will_sync = '0' then
                flag_will_sync <= '0';
            end if;
        end if;
    end process sync_readenable;

    --elink_sync <= lane_control.global.elink_sync;--
    elink_sync <= sync_read_enable;

    --RL replaced with the new word_printer below.
    --  elink_muxer:   entity work.gbt_word_printer
    --    generic map (
    --      enable_endian_control  => '1',  -- if '0', then operation is always in little endian.
    ----      DATARATE            => DATARATE,
    ----      NUMELINK            => NUMELINK,
    ----      FEC                 => FEC
    --        NUMELINKmax         => NUMELINKmax
    --      )
    --    port map (
    ----MT      clk240      => lane_txclk_240,
    --      clk      => lane_txclk,
    --      elink_sync    => elink_sync,
    --      elink_control    => elink_control,
    --      elink_data    => elink_data_in,
    --      elink_read_enable    => elink_data_re_ph1,
    ----      gbt_payload    => tx_120b_from_mach( 79+32 downto 32),
    --      gbt_payload    => payload_224b_ph1,
    --      tx_flag      => gbt_tx_flag,
    --      ila_gbt_word_gen_state  => ila_gbt_word_gen_state,
    --      LINKSconfig       => LINKSconfig
    --      );
    ----  end generate gen_elink_muxer;

    elink_muxer_v2:   entity work.gbt_word_printer_v2
        generic map (
            NUMELINKmax         => NUMELINKmax
        )
        port map (
            clk      => lane_txclk,
            elink_sync    => elink_sync,
            elink_control    => elink_control,
            elink_data    => elink_data_in,
            tx_flag      => gbt_tx_flag,
            LINKSconfig       => LINKSconfig,
            MSBfirst          => MSBfirst,
            gbt_payload    => payload_224b_ph2,
            elink_read_enable  => elink_data_re_ph2,
            aurora_en         => aurora_en
        );
    --  end generate ge
    elink_data_re <= elink_data_re_ph2;
    payload_224b  <= payload_224b_ph2;
    --****************************
    --****************************
    --1b) Set Header, IC, EC (Hardcoded)
    HDR_4b <= "0101"      when (LINKSconfig(2) = '0'  ) else "00" & "01";
    --  HDR_4b <= "0101"      when (LINKSconfig(2) = '0'  ) else
    --            "00" & "01" when (LINKSconfig    = "100") else
    --            "00" & "01" when (LINKSconfig    = "101") else
    --            "00" & "01" when (LINKSconfig    = "110") else
    --            "00" & "01" when (LINKSconfig    = "111") ;
    IC_2b  <= "11";
    --  IC_2b  <= "11"        when (LINKSconfig(2) = '0'  ) else
    --            "11"        when (LINKSconfig    = "100") else
    --            "11"        when (LINKSconfig    = "101") else
    --            "11"        when (LINKSconfig    = "110") else
    --            "11"        when (LINKSconfig    = "111") ;
    EC_2b  <= "11";
    --  EC_2b  <= "11"        when (LINKSconfig(2) = '0'  ) else
    --            "11"        when (LINKSconfig    = "100") else
    --            "11"        when (LINKSconfig    = "101") else
    --            "11"        when (LINKSconfig    = "110") else
    --            "11"        when (LINKSconfig    = "111") ;

    --****************************
    --****************************
    --1c) Set FEC (Hardcoded until I import the right module)
    FEC_48b <= (others=>'0');
    --  FEC_48b <= (others=>'0')      when (LINKSconfig(2) = '0'  ) else
    --             (others=>'0')      when (LINKSconfig    = "100") else
    --             (others=>'0')      when (LINKSconfig    = "101") else
    --             (others=>'0')      when (LINKSconfig    = "110") else
    --             (others=>'0')      when (LINKSconfig    = "111") ;

    --****************************
    --****************************
    --1d) Set LM (Hardcoded)
    LM_2b  <= "00";
    --  LM_2b  <= (others=>'0')      when (LINKSconfig(2) = '0'  ) else
    --             (others=>'0')      when (LINKSconfig    = "100") else
    --             (others=>'0')      when (LINKSconfig    = "101") else
    --             (others=>'0')      when (LINKSconfig    = "110") else
    --             (others=>'0')      when (LINKSconfig    = "111") ;


    --****************************
    --****************************
    --1e) data checker: just for debug purposes (implemented only for 2bit elinks
    --in Phase1)

    ----MT checker gbt word
    ----goal: for 2b elink (e.g: group1 with current config) accumulate 10b
    ----over 5xgbt_tx_flag and look for following pattern: 0xaa,
    ----chunk_length(2bytes), L1ID 2bytes), 0xbb, 0xaa
    ----count distance between 0xaa & 0x00 & 0x3C  and 0xbb + 0xaa (assuming
    ----chunk_lenght is = 0x03C)

    --gbtword_checker:   entity work.gbtword_checker
    --  port map (
    --    lane_txclk_240    => lane_txclk_240    ,
    --    gbt_tx_flag             => gbt_tx_flag          ,
    --    tx_120b_from_mach       => tx_120b_from_mach    ,

    --    ila_phlck_gwchk         => ila_phlck_gwchk      ,
    --    ila_en_gwchk            => ila_en_gwchk         ,
    --    ila_count_gwchk         => ila_count_gwchk      ,
    --    ila_count_start_gwchk   => ila_count_start_gwchk,
    --    ila_testpass_gwchk      => ila_testpass_gwchk   ,
    --    ila_dist_gwchk          => ila_dist_gwchk       ,
    --    ila_word10b_gwchk       => ila_word10b_gwchk    ,
    --    ila_count_pyld          => ila_count_pyld

    --    );

    --PROTOCOL, DATARATE, FEC. 0.X,X = 4.8 Gbps    (GBT   Phase1) 5 Egroups  40 links,
    --                         1,0.0 = 5.12 FEC5   (lpGBT Phase2) 7 Egroups  56 links,
    --                         1,0.1 = 5.12 FEC12  (lpGBT Phase2) 6 Egroups  48 links,
    --                         1.1,0 = 10.24 FEC5  (lpGBT Phase2) 7 Egroups 112 limks,
    --                         1.1,1 = 10.24 FEC12 (lpGBT Phase2) 6 Egroups  96 links

    or_out_of_sync <= or_reduce(out_of_sync);

    --  gen_elink_data_in_zero : for i in 0 to NUMELINKmax-41 generate
    --    elink_data_in_zero(i) <= "0000000000";
    --  end generate gen_elink_data_in_zero;

    --  gen_dgen_group : if sim_emulator = false generate
    --ph1
    --  dgen_group: for i in 0 to 4 generate
    dgen_group: for i in 0 to NUMEGROUPmax-1 generate
        signal elink_control_enable_array : std_logic_vector(15 downto 0);
        signal elink_data_re_array        : std_logic_vector(15 downto 0);
        signal elink_data_re_array_0      : std_logic_vector(15 downto 0) := (others => '0');
    begin
        --  MT if any of the elink in the egroup is active generate data for the
        --  all group (processed data by bit_feeder will have _we=1 only if
        --  enable for that elink is 1)
        --  emu_data_re(i) <= elink_data_re(i*8);
        --  emu_data_re(i) <=  elink_data_re(i*8) or elink_data_re(i*8+1) or elink_data_re(i*8+2) or elink_data_re(i*8+3) or elink_data_re(i*8+4) or elink_data_re(i*8+5) or elink_data_re(i*8+6) or elink_data_re(i*8+7);
        emu_data_re(i) <= or_reduce(elink_data_re_array);
        egroup_enable(i) <= or_reduce(elink_control_enable_array);

        --elink_data_in_tmp <= elink_data_in when

        generate_arrays : for j in 0 to 15 generate
            elink_control_enable_array(j)     <=  elink_control(i*16+j).enable;
            elink_data_re_array(j)            <=  elink_data_re(i*16+j);
            --elink_data_in_pipe(i*16+j)        <= emu_data_out(i) when egroup_enable = '1' else 0;
            aurora_en(i*16+j) <= aurora_en_EGROUP(i);  -- RL Check LATER
        end generate generate_arrays;

        dmap : for j in 0 to 7 generate
            elink_data_in(i*16+j)   <= emu_data_out(i) when egroup_enable(i) = '1' else "0000000000";
            elink_data_in(i*16+j+8) <= emu_data_out(i) when (LINKSconfig(2) = '1' and LINKSconfig(1) = '1' and egroup_enable(i) = '1') else "0000000000";
        end generate dmap;

        flag_sync(i) <= (not egroup_enable(i)) or flag_data_gen(i);

        --RL read enable should always be either equal to 0 or the enable vector. If not they are desynced.
        check_read_enables : process(lane_txclk)
        begin
            if lane_txclk'event and lane_txclk ='1' then
                if elink_data_re_array = elink_data_re_array_0 then
                    out_of_sync(i) <= '0';
                elsif elink_data_re_array = elink_control_enable_array then
                    out_of_sync(i) <= '0';
                else
                    out_of_sync(i) <= '1';
                end if;
            end if;
        end process check_read_enables;


        emu_0: entity work.elink_data_emulator
            generic map (
                epath    => epath,
                egroup    => to_std_logic_vector(i,3),
                LANE_ID     => LANE_ID
            )
            port map (
                clk40        => clk_xtal_40,  --Frans1
                --        clk240        => lane_txclk_240,
                clk        => lane_txclk,
                emu_control      => emu_control(i),
                elink_data_out    => emu_data_out(i),
                elink_data_re    => emu_data_re(i),
                l1a_trig      => l1a_trigger,
                l1a_id        => l1a_id,
                flag_data_gen       => flag_data_gen(i),
                ila_data_gen_out  => ila_data_gen_out(i),
                ila_fifo_out    => ila_fifo_out(i),
                ila_data_gen_we    => ila_data_gen_we(i),
                --MT checker upstream fifo
                ila_count_upstfifochk   => ila_count_upstfifochk(i),
                ila_fifo_flush          => ila_fifo_flush(i),
                ila_count_chk_out       => ila_count_chk_out(i),
                ila_count_chk_out2      => ila_count_chk_out2(i),
                ila_isEOP_chk2          => ila_isEOP_chk2(i),
                ila_efifoDout_8b10b     => ila_efifoDout_8b10b(i),
                ila_enc10bitRdy         => ila_enc10bitRdy(i),

                ila_efifoFull           => ila_efifoFull(i)     ,
                ila_efifoEmpty          => ila_efifoEmpty(i)    ,
                ila_efifoPfull          => ila_efifoPfull(i),
                ila_elink_data_re       => ila_elink_data_re(i) ,
                ila_efifoDoutRdy        => ila_efifoDoutRdy(i)  ,
                --Frans 1
                SELECT_RANDOM => SELECT_RANDOM,
                chunk_length_out => chunk_length_out(i),
                --MT 2 (Fran 2)
                FMEMU_RANDOM_RAM_ADDR          => FMEMU_RANDOM_RAM_ADDR ,
                FMEMU_RANDOM_RAM               => FMEMU_RANDOM_RAM      ,
                FMEMU_RANDOM_CONTROL           => FMEMU_RANDOM_CONTROL  ,
                --SS (SWAP LSB MSB)
                --fhCR_REVERSE_10B               => MSBfirst ,
                aurora_en               => aurora_en_EGROUP(i)
            );



    --ph1
    --    dmap : for j in 0 to 7 generate
    --MT ph2 (16 2b virtual elinks
    --    dmap : for j in 0 to 15 generate
    --      elink_data_in(i*8+j) <= emu_data_out(i);
    --    end generate dmap;
    --  end generate dgen_group;
    --  end generate gen_dgen_group;

    end generate dgen_group;


    --  gen_ttc_wrapper_comp : if sim_emulator = false generate
    ttc_wrapper_comp : entity work.ttc_wrapper
        port map (
            a_data_in      => gbt_extractedl1a,
            b_data_in      => gbt_extracted_bchan,
            ttc_strobe    => gbt_rx_flag,
            TTC_out      => TTC_out,
            --MT      clk240      => lane_rxclk_240,
            clk240      => lane_rxclk, --Q.: what if is 320 MHZ?
            BUSY      => chB_busy,
            L1ID_Bch      => chB_l1a_id,
            TTC_ToHost_Data_out  => TTC_ToHost_Data
        );
--  end generate gen_ttc_wrapper_comp;

--ila
--  ila_l1a_trigger <= l1a_trigger;
--  ila_l1a_id <= l1a_id;
--  ila_payload_80b_gbt <= gbt_tx_data_228b_out_temp(111 downto 32);
--  ila_clk_xtal_40 <= clk_xtal_40;
--  ila_gbt_tx_data_120b <= gbt_tx_data_228b_out_temp(119 downto 0);
--  ila_gen: for i in 0 to NUMEGROUPmax-1 generate
--    ila_emu_data_out(i) <= emu_data_out(i);
--    ila_emu_control_output_width(i) <= emu_control(i).output_width;
--  end generate ila_gen;


--  ila_lane: if LANE_ID < 2 generate
--    ila_one_link : ila_0
--      port map(
--        clk => lane_txclk,
--        probe0(0)   => ila_l1a_trigger, --0:0
--        probe1      => ila_l1a_id, --15:0
--        probe2      => ila_fifo_out(0), -- 9:0
--        probe3      => ila_fifo_out(1), -- 9:0
--        probe4      => ila_fifo_out(2), -- 9:0
--        probe5      => ila_fifo_out(3), -- 9:0
--        probe6      => ila_fifo_out(4), -- 9:0
--        probe7      => ila_fifo_out(5), -- 9:0
--        probe8      => ila_fifo_out(6), -- 9:0
--        probe9      => ila_emu_data_out(0), -- 9:0
--        probe10     => ila_emu_data_out(1), -- 9:0
--        probe11     => ila_emu_data_out(2), -- 9:0
--        probe12     => ila_emu_data_out(3), -- 9:0
--        probe13     => ila_emu_data_out(4), -- 9:0
--        probe14     => ila_emu_data_out(5), -- 9:0
--        probe15     => ila_emu_data_out(6), -- 9:0
--        probe16     => ila_payload_80b_gbt, --79:0
--        probe17     => ila_emu_control_output_width(0), --2:0
--        probe18     => ila_emu_control_output_width(1), --2:0
--        probe19     => ila_emu_control_output_width(2), --2:0
--        probe20     => ila_emu_control_output_width(3), --2:0
--        probe21     => ila_emu_control_output_width(4), --2:0
--        probe22     => ila_emu_control_output_width(5), --2:0
--        probe23     => ila_emu_control_output_width(6), --2:0
--        probe24(0)  => ila_clk_xtal_40,--0:0
--        probe25     => ila_gbt_tx_data_120b); --79:0
--    end generate ila_lane;





end architecture Behavioral;
