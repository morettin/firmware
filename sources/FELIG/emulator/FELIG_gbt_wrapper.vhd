--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	(modified by) Michael Oberling
--
-- Design Name:	FELIX_gbt_wrapper
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Modification of FELIX_gbt_wrapper to remove embedded GTH core.
-- 				TAB SPACING = 4
-- 					
--
-- Change Log:	V1.0	Cut out GTH implementation and output FIFO, for use in 
--						FELIG. All signal names changed to lower case.
-- 						Removed generic hooks, as the logic remaining within
--						this wrapper is not card or quad specific. Otherwise, 
--						source is left functionally identical. (we hope)
--
--==============================================================================

-- Original File Header --
--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: FELIX V7 (GTH) GBT Wrapper
-- Module Name: gbt_top - Behavioral
-- Project Name:
-- Target Devices: V7
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX GBT & GTH
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity FELIX_gbt_wrapper is
port (
	alignment_chk_rst_i	: in  std_logic;
	clk40_in			: in  std_logic;
	data_rxformat		: in  std_logic_vector(1 downto 0);
	data_txformat		: in  std_logic_vector(1 downto 0);
	error_f				: out std_logic;
	frame_locked_o		: out std_logic;
	gt_rxusrclk			: in  std_logic;
	gt_txusrclk			: in  std_logic;
	gtrx_reset			: in  std_logic;
	gtrx_reset_i		: out std_logic;
	rx_120b_out			: out std_logic_vector(119 downto 0);
	rx_data_20b			: in  std_logic_vector(19 downto 0);
	rx_flag_o			: out std_logic;
	rx_header_found		: out std_logic;
	rx_is_header		: out std_logic;
	rx_reset			: in  std_logic;
	rxcdrlock			: in  std_logic;
	rxfsmresetdone		: in  std_logic;
	rxresetdone			: in  std_logic;
	rxslide				: out std_logic;
	tc_edge				: in  std_logic;
	tx_120b_in			: in  std_logic_vector(119 downto 0);
	tx_data_20b			: out std_logic_vector(19 downto 0);
	tx_flag_o			: out std_logic;
	tx_frame_clk_i		: in  std_logic;
	tx_reset			: in  std_logic;
	tx_tc_dly_value		: in  std_logic_vector(3 downto 0);
	tx_tc_method		: in  std_logic;
	txfsmresetdone		: in  std_logic;
	txresetdone			: in  std_logic
);
end FELIX_gbt_wrapper;

architecture Behavioral of FELIX_gbt_wrapper is
	-- unused "IO"
	constant data_sel				: std_logic_vector(3 downto 0)	:= (others => '0');
	constant desmux_use_sw			: std_logic						:= '0';
	constant outsel_i				: std_logic						:= '0';
	constant rst_hw					: std_logic						:= '0';
	constant rx_opt					: std_logic						:= '0';
	constant tx_opt					: std_logic_vector(1 downto 0)	:= (others => '0');
	signal rx_is_data				: std_logic;

	signal alignment_chk_rst		: std_logic;
	signal alignment_chk_rst_c		: std_logic;
	signal alignment_chk_rst_c1		: std_logic;
	signal alignment_done			: std_logic;
	signal alignment_done_a			: std_logic;
	signal alignment_done_chk_cnt	: std_logic_vector(12 downto 0)	:= (others => '0');
	signal alignment_done_f			: std_logic;
	signal auto_gbt_rxrst			: std_logic;
	signal auto_gth_rxrst			: std_logic;
	signal bitslip_manual_2r		: std_logic;
	signal bitslip_manual_r			: std_logic;
	signal data_rxformat_i			: std_logic_vector(1 downto 0);
	signal data_txformat_i			: std_logic_vector(1 downto 0);
	signal error_orig				: std_logic;
	signal ext_trig_realign			: std_logic;
	signal fsm_rst					: std_logic;
	signal gt_rx_word_clk			: std_logic;
	signal gt_tx_word_clk			: std_logic;
	signal outsel_ii				: std_logic;
	signal outsel_o					: std_logic;
	signal pulse_cnt				: std_logic_vector(29 downto 0)	:= (others => '0');
	signal pulse_lg					: std_logic;
	signal rx_120b_out_i			: std_logic_vector(119 downto 0);
	signal rx_120b_out_ii			: std_logic_vector(119 downto 0);
	signal rx_flag_oi				: std_logic;
	signal rxcdrlock_f				: std_logic;
	signal rxslide_c				: std_logic;
	signal rxslide_i				: std_logic;
	signal rx_reset_i				: std_logic;
	signal tx_reset_i				: std_logic;
begin
	data_rxformat_i		<= data_rxformat;
	data_txformat_i		<= data_txformat;
	frame_locked_o		<= alignment_done_f;
	fsm_rst				<= rx_reset;
	gtrx_reset_i		<= (gtrx_reset or auto_gth_rxrst);
	gt_rx_word_clk		<= gt_rxusrclk;
	gt_tx_word_clk		<= gt_txusrclk;
	rx_120b_out			<= rx_120b_out_ii when alignment_done_f='1' else (others =>'0');
	rx_120b_out_ii		<= rx_120b_out_i;
	rx_flag_o			<= rx_flag_oi;
	rx_reset_i			<= (rx_reset or auto_gbt_rxrst);
	rxcdrlock_f			<= rxcdrlock;
	rxslide_i			<= rxcdrlock_f and rxslide_c;
	tx_reset_i			<= tx_reset or (not txresetdone) or (not txfsmresetdone);-- for V7
	alignment_chk_rst	<= (alignment_chk_rst_i or alignment_chk_rst_c or alignment_chk_rst_c1);
	error_f				<= error_orig and alignment_done_f;
	outsel_ii			<= outsel_o when desmux_use_sw = '0' else outsel_i;
	
	process(clk40_in)
	begin
		if clk40_in'event and clk40_in='1' then
			pulse_lg <= pulse_cnt(20);
			if pulse_cnt(20)='1' then
				pulse_cnt <=(others=>'0');
			else
				pulse_cnt <= pulse_cnt+'1';
			end if;
		end if;
	end process;
	
	process(clk40_in)
	begin
		if clk40_in'event and clk40_in='1' then
			alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';
		end if;
	end process;
	
	process(clk40_in)
	begin
		if clk40_in'event and clk40_in='1' then
			if alignment_done_chk_cnt="0000000000000" then
				alignment_done_a <= alignment_done;
			else
				alignment_done_a <= alignment_done and alignment_done_a;
			end if;
			
			if alignment_done_chk_cnt="1000000000000" then
				alignment_done_f <=  rxcdrlock_f and alignment_done_a;
			end if;
		end if;
	end process;
	
	auto_rxrst : entity work.FELIX_GBT_RX_AUTO_RST
	port map
	(
		FSM_CLK				=> clk40_in,
		pulse_lg			=> pulse_lg,
		GTHRXRESET_DONE		=> rxresetdone and rxfsmresetdone,
		alignment_chk_rst	=> alignment_chk_rst_c1,
		GBT_LOCK			=> alignment_done_f,--alignment_done,
		AUTO_GTH_RXRST		=> auto_gth_rxrst,
		ext_trig_realign	=> ext_trig_realign,
		AUTO_GBT_RXRST		=> auto_gbt_rxrst
	);
	
	rafsm : entity work.FELIX_GBT_RXSLIDE_FSM
	port map
	(
		ext_trig_realign	=> ext_trig_realign,
		FSM_RST				=> fsm_rst,
		FSM_CLK				=> clk40_in,
		GBT_LOCK			=> alignment_done,
		RxSlide				=> rxslide_c,
		alignment_chk_rst	=> alignment_chk_rst_c
	);
	
	process(gt_rx_word_clk)
	begin
		if gt_rx_word_clk'event and gt_rx_word_clk='1' then
			bitslip_manual_r	<= rxslide_i;
			bitslip_manual_2r	<= bitslip_manual_r;
			rxslide				<= bitslip_manual_r and (not bitslip_manual_2r);
		end if;
	end process;
	
	gbtTxRx_inst: entity work.gbtTxRx_FELIX
--	generic map
--	(
--		channel => i
--	)
	port map
	(
		error_o				=> error_orig,
		RX_FLAG				=> rx_flag_oi,
		TX_FLAG				=> tx_flag_o,
		
		Tx_DATA_FORMAT		=> data_txformat_i(1 downto 0),
		Rx_DATA_FORMAT		=> data_rxformat_i(1 downto 0),
		
		Tx_latopt_tc		=> tx_opt(0),
		Tx_latopt_scr		=> tx_opt(1),
		RX_LATOPT_DES		=> rx_opt,
		
		TX_TC_METHOD		=> tx_tc_method,
		TC_EDGE				=> tc_edge,
		TX_TC_DLY_VALUE		=> tx_tc_dly_value(2 downto 0),
		
		alignment_chk_rst	=> alignment_chk_rst,
		alignment_done_O	=> alignment_done,
		L40M				=> clk40_in,
		outsel_i			=> outsel_ii,
		outsel_o			=> outsel_o,
		OddEven				=> '0',--oddeven_i,
		TopBot				=> '0',--topbot_i,
		data_sel			=> data_sel(3 downto 0),
		
		TX_RESET_I			=> tx_reset_i,
		TX_FRAMECLK_I		=> tx_frame_clk_i,
		TX_WORDCLK_I		=> gt_tx_word_clk,
		TX_DATA_120b_I		=> tx_120b_in,
		TX_DATA_20b_O		=> tx_data_20b,
		
		RX_RESET_I			=> rx_reset_i,
		RX_FRAME_CLK_O		=> open,--rx_frame_clk_o,
		RX_WORD_IS_HEADER_O	=> rx_is_header,
		RX_HEADER_FOUND		=> rx_header_found,
		RX_ISDATA_FLAG_O	=> rx_is_data,
		RX_DATA_20b_I		=> rx_data_20b,
		RX_DATA_120b_O		=> rx_120b_out_i,
		des_rxusrclk		=> gt_rx_word_clk,
		RX_WORDCLK_I		=> gt_rx_word_clk
	);
end Behavioral;
