--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	Michael Oberling
--
-- Design Name:	gt_core_GT_FRAME_GEN
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Coming soon.
--
-- Change Log:	V1.0 - 
--
--==============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;

use work.ip_lib.ALL;

entity gt_core_GT_FRAME_GEN is
port
(
	-- User Interface
	TX_DATA_OUT		: out	std_logic_vector(19 downto 0);
	-- System Interface
	USER_CLK		: in	std_logic;	  
	SYSTEM_RESET	: in	std_logic
); 


end entity gt_core_GT_FRAME_GEN;

architecture arch of gt_core_GT_FRAME_GEN is
	signal frame_gen_counter		: std_logic_vector( 9 downto  0);	
	signal frame_gen_counter_unused	: std_logic_vector(47 downto 10);	
	signal kin						: std_logic;
	signal rand_frame_data_valid	: std_logic;
	signal rand_frame_enable		: std_logic;
	signal seed_in					: std_logic_vector(127 downto 1);
	signal system_reset_r			: std_logic;
	signal system_reset_r2			: std_logic;
	signal tx_data					: std_logic_vector(15 downto 0);
	signal tx_data_out_i			: std_logic_vector(15 downto 0);
	
	attribute keep						: string;
	attribute keep of system_reset_r	: signal is "true";
	attribute keep of system_reset_r2	: signal is "true";
	
	attribute ASYNC_REG						: string;
	attribute ASYNC_REG of system_reset_r	: signal is "TRUE";
	attribute ASYNC_REG of system_reset_r2	: signal is "TRUE";
begin
	seed_in <=  X"FEDCBA9876543210123456789ABCDEF" & "101";
	tx_data_out_i <= tx_data when (kin = '0') else "0001110000011100";
	
	process( USER_CLK )
	begin
		if(USER_CLK'event and USER_CLK = '1') then
			system_reset_r <= SYSTEM_RESET; 
			system_reset_r2 <= system_reset_r; 
		end if;
	end process;
	
	kin <= not rand_frame_data_valid;
	
	encoder_9_0 : entity work.encode_8b10b_wrapper
	port map (
		clk			=> USER_CLK,
		din			=> tx_data_out_i(7 downto 0),
		kin			=> kin,
		dout		=> TX_DATA_OUT(9 downto 0),
		disp_out	=> open
	);
	
	encoder_19_10 : entity work.encode_8b10b_wrapper
	port map (
		clk			=> USER_CLK,
		din			=> tx_data_out_i(15 downto 8),
		kin			=> kin,
		dout		=> TX_DATA_OUT(19 downto 10),
		disp_out	=> open
	);
	
	comp_random_frame_gen : entity work.random_frame_gen
	generic map (
		run_length		=> 1000
	)
	port map (
		reset					=> system_reset_r2,
		clk_word				=> USER_CLK,
		
		rand_frame_data			=> tx_data,
		rand_frame_enable		=> rand_frame_enable,
		rand_frame_data_valid	=> rand_frame_data_valid,
		
		seed_enable				=> system_reset_r2,
		seed_in					=> seed_in
	);
	
	frame_gen_delay_counter : dsp_counter
	PORT MAP (
		CLK				=> USER_CLK,
		CE				=> '1',
		SCLR			=> '0',
		UP				=> '1',
		LOAD			=> '0',
		L				=> X"000000000000",
		Q(47 downto 10)	=> frame_gen_counter_unused,
		Q(9 downto  0)	=> frame_gen_counter
	);
	
	
	process( USER_CLK )
	begin
		if(USER_CLK'event and USER_CLK = '1') then
			if ((frame_gen_counter = 0) and (SYSTEM_RESET = '0')) then
				rand_frame_enable <= '1';
			else
				rand_frame_enable <= '0';
			end if;
		end if;
	end process;
end architecture arch;

