--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--************** Psedo Random data generator**********************
-- date 21-2-2019
-- 10bit PRDG with LUT to set the disterubution
-- LFSR is used to generate PRD with generic polynom and seed value
-- seed value must not be zero
-- polynom value bit (10) must be one to have a ten bit RDG
--****************************************************************

library ieee, UNISIM, work;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_unsigned.all;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;

entity Random_gen is
    generic (
        LANE_ID              : integer := 0
    );
    port (
        rst                  : in     std_logic;
        clk40                : in     std_logic;
        --    clk240               : in     std_logic;
        clk               : in     std_logic;
        --    register_map_control : in     register_map_control_type;
        rg_rst               : in     std_logic;
        rg_enb               : in     std_logic;
        rg_doutb             : out    std_logic_vector(15 downto 0);
        --MT 2 (Fran 2)
        FMEMU_RANDOM_RAM_ADDR          : in std_logic_vector(9 downto 0);    -- Controls the address of the ramblock for the random number generator
        FMEMU_RANDOM_RAM               : in bitfield_fmemu_random_ram_t_type;
        FMEMU_RANDOM_CONTROL           : in bitfield_fmemu_random_control_w_type


    );
end entity Random_gen;

architecture rtl of Random_gen is

    constant wd : integer := 10;
    signal rg_temp : std_logic_vector(wd - 1 downto 0):= (wd - 1 => '1', others => '0');
    signal rtemp_init : std_logic_vector(31 downto 0):= (others => '0');
    --signal rg_rst :std_logic := '0';
    signal rg_en :std_logic := '0';

    constant  G_M           : integer           := 10;
    signal r_lfsr           : std_logic_vector (G_M - 1 downto 0) := (others => '0');
    signal w_mask           : std_logic_vector (G_M - 1 downto 0) := (others => '0');
    signal w_poly           : std_logic_vector (G_M - 1 downto 0) := (others => '0');
    signal rG_POLY       : std_logic_vector(9 downto 0) := "1001000000";
    signal rSeed       : std_logic_vector(9 downto 0) := "1001000000";

    signal rg_dina : std_logic_vector(15 downto 0) := (others => '0');
    signal rg_addra : std_logic_vector(9 downto 0) := (others => '0');
    signal rg_addrb : std_logic_vector(9 downto 0) := (others => '0');
    signal rg_wea :std_logic_vector(0 downto 0) := (others => '0');

    COMPONENT Distr_LUT_felig
        PORT (
            clka : IN STD_LOGIC;
            wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            addra : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            dina : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            clkb : IN STD_LOGIC;
            enb : IN STD_LOGIC;
            addrb : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
            doutb : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
        );
    END COMPONENT;

begin
    ---- %%%%%%% for test only
    rG_POLY <= FMEMU_RANDOM_CONTROL.POLYNOMIAL; --MT register_map_control.FMEMU_RANDOM_CONTROL.POLYNOMIAL;
    rSeed <= FMEMU_RANDOM_CONTROL.SEED; --MT register_map_control.FMEMU_RANDOM_CONTROL.SEED;
    --  random_rst <= register_map_control.GEN_FM_CONTROL1.PACKAGE_LENGTH(15);
    ----%%%%%%%% write LUT
    rg_addra   <= FMEMU_RANDOM_RAM_ADDR; --MT register_map_control.FMEMU_RANDOM_RAM_ADDR;
    rg_dina      <= FMEMU_RANDOM_RAM.DATA; --MT register_map_control.FMEMU_RANDOM_RAM.DATA;
    rg_wea   <= FMEMU_RANDOM_RAM.WE when FMEMU_RANDOM_RAM.CHANNEL_SELECT(16+LANE_ID) = '1' else "0";  --MT register_map_control.FMEMU_RANDOM_RAM.WE when register_map_control.FMEMU_RANDOM_RAM.CHANNEL_SELECT(16+LANE_ID) = '1' else "0";

    ----%%%%%%%% end write BRAM

    --***** n bits LFSR with polynom settings
    w_poly  <= rG_POLY;
    g_mask : for k in G_M - 1 downto 0 generate
        w_mask(k)  <= w_poly(k) and rg_temp(0);
    end generate g_mask;

    --p_lfsr : process (clk240,rg_rst, rst, rg_en) begin
    p_lfsr : process (clk,rg_rst, rst, rg_en) begin --
        --  if rising_edge(clk240) then
        if rising_edge(clk) then --
            rg_addrb <= rg_temp;
            if(rg_rst = '1' or rst = '1' or rg_temp = "0000000000") then
                rg_temp(9 downto 0)   <= rSeed;
            elsif (rg_enb = '1') then
                rg_temp(9 downto 0)   <= ('0' & rg_temp(G_M - 1 downto 1) ) xor w_mask(G_M - 1 downto 0);
            end if;
        end if;
    end process p_lfsr;


    --******** LUT to output the needed disteribbtion
    Distr_LUT_COMP: Distr_LUT_felig
        port map(
            clka => clk40,
            wea   => rg_wea,
            addra => rg_addra,
            dina  => rg_dina,
            --  clkb  => clk240,
            clkb  => clk,
            enb   => rg_enb,
            addrb => rg_addrb,
            doutb => rg_doutb
        );
end architecture rtl ; -- of OUTPUTctrl
