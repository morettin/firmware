--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen      (kchen@bnl.gov)
--! modified:    Marco Trovato (mtrovato@anl.gov)
--!              Ricardo Luz   (rluz@anl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:43:14 PM
-- Design Name: FLX_LpGBT_FE_Wrapper
-- Module Name: FLX_LpGBT_FE_Wrapper - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              FLX_LpGBT_FrontEnd_Wrapper
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;-- @suppress "Deprecated package"
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;-- @suppress "Deprecated package"

library UNISIM;
    use UNISIM.VComponents.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;

entity FLX_LpGBT_FE_Wrapper_FELIG is
    Generic (
        GBT_NUM                     : integer   := 24;
        --MT SIMU+
        sim_emulator            : boolean       := false
    --
    );
    Port (

        FE_DOWNLINK_USER_DATA       : out txrx32b_type(0 to GBT_NUM-1);
        FE_DOWNLINK_EC_DATA         : out txrx2b_type(0 to GBT_NUM-1);
        FE_DOWNLINK_IC_DATA         : out txrx2b_type(0 to GBT_NUM-1);

        FE_UPLINK_USER_DATA         : in txrx224b_type(0 to GBT_NUM-1);
        FE_UPLINK_EC_DATA           : in txrx2b_type(0 to GBT_NUM-1);
        FE_UPLINK_IC_DATA           : in txrx2b_type(0 to GBT_NUM-1);

        clk40_in                    : in std_logic;
        clk320_in                   : in std_logic; --MT SIMU+
        rst_hw                      : in std_logic;
        FE_SIDE_RX40MCLK         : out std_logic;

        RX_P                        : in std_logic_vector(GBT_NUM-1 downto 0);
        RX_N                        : in std_logic_vector(GBT_NUM-1 downto 0);
        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);
        --MT commented
        --GTHREFCLK                   : in std_logic_vector(GBT_NUM-1 downto 0);
        --MT added
        GTHREFCLK0                  : in std_logic_vector(GBT_NUM-1 downto 0); --RX
        GTHREFCLK1                  : in std_logic_vector(GBT_NUM-1 downto 0); --TX
        --MT externalizing gtrx/txusrclk for FELIG logic
        GT_TXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);
        GT_RXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);

        CTRL_SOFT_RESET             : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TXPLL_DATAPATH_RESET   : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_RXPLL_DATAPATH_RESET   : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TX_DATAPATH_RESET      : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_RX_DATAPATH_RESET      : in std_logic_vector(GBT_NUM/4-1 downto 0);
        CTRL_TXPOLARITY             : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_RXPOLARITY             : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBTTXRST               : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBTRXRST               : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_DATARATE               : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_FECMODE                : in std_logic_vector(GBT_NUM-1 downto 0);

        CTRL_CHANNEL_DISABLE        : in std_logic_vector(GBT_NUM-1 downto 0);
        CTRL_GBT_General_ctrl       : in std_logic_vector(63 downto 0);



        MON_RXRSTDONE               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXRSTDONE               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXRSTDONE_QUAD          : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_TXRSTDONE_QUAD          : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_RXPMARSTDONE            : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_TXPMARSTDONE            : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXCDR_LCK               : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_RXCDR_LCK_QUAD          : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_QPLL_LCK                : out std_logic_vector(GBT_NUM/4-1 downto 0);
        MON_CPLL_LCK                : out std_logic_vector(GBT_NUM-1 downto 0);

        MON_ALIGNMENT_DONE          : out std_logic_vector(GBT_NUM-1 downto 0);
        MON_LPGBT_ERRFLG            : out std_logic_vector(GBT_NUM-1 downto 0);
        --MT for FELIG from mgt_framealigner . 1 pulse every c_wordRatio (=8)
        sta_headerFlag_out          : out std_logic_vector(GBT_NUM-1 downto 0);
        --MT added: needed by FELIG to latch tx payload to generated data. Need to
        --be synchronized with emulator logic
        tx_flag_out                 : out std_logic_vector(GBT_NUM-1 downto 0);
        --MT LMK lock signal
        LMK_LD                      : in  std_logic;
        RxCdrLock_o                 : out std_logic_vector(GBT_NUM-1 downto 0);
        alignment_done_o            : out std_logic_vector(GBT_NUM-1 downto 0)


    );
end FLX_LpGBT_FE_Wrapper_FELIG;


architecture Behavioral of FLX_LpGBT_FE_Wrapper_FELIG is

    component fifo_generator_fe IS
        PORT (
            rst : IN STD_LOGIC;
            wr_clk : IN STD_LOGIC;
            rd_clk : IN STD_LOGIC;
            din : IN STD_LOGIC_VECTOR(35 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout : OUT STD_LOGIC_VECTOR(35 DOWNTO 0);
            full : OUT STD_LOGIC;
            empty : OUT STD_LOGIC;
            prog_full : OUT STD_LOGIC;
            prog_empty : OUT STD_LOGIC;
            wr_rst_busy : OUT STD_LOGIC;
            rd_rst_busy : OUT STD_LOGIC
        );
    END component;

    component rxclkgen is
        port
 (

            clk_out1 : out std_logic;

            reset : in std_logic;
            locked: out std_logic;

            clk_in1: in std_logic
        );
    end component;

    signal rxrecclk40m          : std_logic;
    signal pulse_lg             : std_logic;
    signal rxclk_locked         : std_logic;
    signal alignment_done_f     : std_logic_vector(GBT_NUM-1 downto 0);
    signal alignment_done_f_latched : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_RESET_i           : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_RESET_i           : std_logic_vector(GBT_NUM-1 downto 0);
    signal TxResetDone          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TX_WORD_CLK       : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_RX_WORD_CLK       : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxresetdone          : std_logic_vector(GBT_NUM-1 downto 0);
    --signal data_rdy             : std_logic_vector(GBT_NUM-1 downto 0); --RL not commented check
    signal RxSlide              : std_logic_vector(GBT_NUM-1 downto 0);
    --signal UplinkRdy            : std_logic_vector(GBT_NUM-1 downto 0); --RL not commented check
    signal fifo_rst             : std_logic_vector(GBT_NUM-1 downto 0);
    signal fifo_empty           : std_logic_vector(GBT_NUM-1 downto 0);
    signal fifo_rden            : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TXOUTCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_RXOUTCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_TXUSRCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal GT_RXUSRCLK          : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_N_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal RX_P_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_N_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal TX_P_i               : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxpmaresetdone       : std_logic_vector(GBT_NUM-1 downto 0);
    signal txpmaresetdone       : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxCdrLock            : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxCdrLock_int        : std_logic_vector(GBT_NUM-1 downto 0);
    signal RxCdrLock_a          : std_logic_vector(GBT_NUM-1 downto 0);
    signal rxcdrlock_out        : std_logic_vector(GBT_NUM-1 downto 0);
    signal auto_gth_rxrst       : std_logic_vector(GBT_NUM-1 downto 0);
    signal drpclk_vec           : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal rxresetdone_quad     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal txresetdone_quad     : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal RxCdrLock_quad       : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal userclk_rx_reset_in  : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal userclk_tx_reset_in  : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal RX_DATAPATH_RESET_FINL   : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal GBT_General_ctrl     : std_logic_vector(63 downto 0);


    --type txrx80b_12ch_type        is array (GBT_NUM/4-1 downto 0) of std_logic_vector(79 downto 0);  --RL not commented check
    --signal RX_DATA_80b            : txrx80b_12ch_type; --RL not commented check
    type data16barray       is array (0 to GBT_NUM-1) of std_logic_vector(15 downto 0);
    type data32barray       is array (0 to GBT_NUM-1) of std_logic_vector(31 downto 0);
    type data128barray       is array (0 to GBT_NUM/4-1) of std_logic_vector(127 downto 0);
    signal RX_DATA_16b      : data16barray := (others => ("0000000000000000"));
    signal TX_DATA_32b      : data32barray := (others => ("00000000000000000000000000000000"));
    signal TX_DATA_128b      : data128barray := (others => (x"00000000000000000000000000000000"));
    type data64barray       is array (0 to GBT_NUM-1) of std_logic_vector(63 downto 0);
    signal RX_DATA_64b      : data64barray;
    type data36barray       is array (0 to GBT_NUM-1) of std_logic_vector(35 downto 0);
    signal downLinkData_i   : data36barray;
    signal downLinkData_o   : data36barray;

    signal cdr_cnt        : std_logic_vector(19 downto 0);
    signal pulse_cnt      : std_logic_vector(29 downto 0);

    --MT added
    signal QpllLock_inv           : std_logic_vector(GBT_NUM/4-1 downto 0); --(11 downto 0);
    signal QPLL_RESET_LMK         : std_logic_vector(GBT_NUM/4-1 downto 0); --(11 downto 0);
    signal QPLL_PIPE              : std_logic_vector(3 downto 0);
    signal qpll1_lck_i            : std_logic_vector(GBT_NUM/4-1 downto 0);

    --RL
    signal reset_rxclkgen   : std_logic;
    signal reset_tx_pll_and_datapath_in : std_logic_vector(GBT_NUM/4-1 downto 0);
    signal txclk40m         : std_logic_vector(GBT_NUM-1 downto 0);

begin

    GBT_General_ctrl <= CTRL_GBT_General_ctrl;


    MON_ALIGNMENT_DONE <= alignment_done_f when GBT_General_ctrl(43)='0' else alignment_done_f_latched;
    MON_LPGBT_ERRFLG <= (others=>'0'); --unused

    FE_SIDE_RX40MCLK <= rxrecclk40m;
    RxCdrLock_o <= RxCdrLock;
    alignment_done_o <= alignment_done_f;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            pulse_lg <= pulse_cnt(20);
            if pulse_cnt(20)='1' then
                pulse_cnt <=(others=>'0');
            else
                pulse_cnt <= pulse_cnt+'1';
            end if;
        end if;
    end process;

    gbtRxTx : for i in 0 to GBT_NUM-1 generate
        signal rst_dnlink : std_logic;--RL
    begin
        RX_RESET_i(i)       <= CTRL_GBTRXRST(i);
        TX_RESET_i(i)       <= CTRL_GBTTXRST(i) or (not TxResetDone(i));

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if GBT_General_ctrl(45)='1' then
                    alignment_done_f_latched(i) <='1';
                else
                    alignment_done_f_latched(i) <= alignment_done_f_latched(i) and alignment_done_f(i);
                end if;

                if pulse_lg = '1' then
                    if alignment_done_f(i)='0' and rxresetdone_quad(i/4)='1' and rxresetdone(i)='1' then
                        auto_gth_rxrst(i) <='1';
                    else
                        auto_gth_rxrst(i) <='0';
                    end if;
                else
                    auto_gth_rxrst(i) <='0';
                end if;
            end if;
        end process;
        rst_dnlink <= (not rxclk_locked) or RX_RESET_i(i); --RL
        lpgbtemu: entity work.FLX_LpGBT_FE
            Port map
    (
                TXCLK40 => txclk40m(i),--clk40_in,
                RXCLK40 => rxrecclk40m,
                TXCLK320 => GT_TX_WORD_CLK(i),
                RXCLK320 => GT_RX_WORD_CLK(i),
                rst_uplink_i => TX_RESET_i(i), --RL: not?
                --uplinkClkEn_i               => '1',
                ctr_clkSlip_s => RxSlide(i),
                aligned => alignment_done_f(i),
                --uplinkRdy_o                 => open,  --RL uncommented in FELIG
                --downlinkRdy_o               => open,--downlinkrdy(i), --RL uncommented in FELIG
                sta_headerFlag_o => sta_headerFlag_out(i),
                sta_headerFlag_shift => open, --sta_headerFlag_shift(i),
                --sta_rxgbxRdy_o              => open,--sta_rxgbxRdy_o(i), --RL uncommented in FELIG
                clk_dataFlag_rxGb_s_o => open,--clk_dataFlag_rxGb_s_o(i),
                dat_upLinkWord_fromGb_s => TX_DATA_32b(i),
                dat_downLinkWord_fromMgt_s16 => RX_DATA_16b(i),
                --sta_mgtTxRdy_s              => txresetdone(i),  --RL uncommented in FELIG
                rst_dnlink_i => rst_dnlink,--RL
                sta_mgtRxRdy_s => rxresetdone(i),

                downLinkBypassDeinterleaver => '0',
                downLinkBypassFECDecoder=> '0',
                downLinkBypassDescsrambler => '0',

                enableFECErrCounter => '0',

                upLinkScramblerBypass => GBT_General_ctrl(32),

                upLinkFecBypass => GBT_General_ctrl(33),
                upLinkInterleaverBypass => GBT_General_ctrl(34),
                fecMode => CTRL_FECMODE(i),
                txDataRate => CTRL_DATARATE(i),
                phase_sel => "100",
                upLinkData => FE_UPLINK_USER_DATA(i),
                upLinkDataIC => FE_UPLINK_IC_DATA(i),
                upLinkDataEC => FE_UPLINK_EC_DATA(i),
                downLinkData => downLinkData_i(i)(31 downto 0),
                downLinkDataIC => downLinkData_i(i)(33 downto 32),
                downLinkDataEC => downLinkData_i(i)(35 downto 34),
                --MT added: needed by FELIG to latch tx payload to generated data. Need to
                --be synchronized with emulator logic
                tx_flag_out                 => tx_flag_out(i)
            );

        FE_DOWNLINK_USER_DATA(i) <= downLinkData_o(i)(31 downto 0);
        FE_DOWNLINK_IC_DATA(i) <= downLinkData_o(i)(33 downto 32);
        FE_DOWNLINK_EC_DATA(i) <= downLinkData_o(i)(35 downto 34);
        fifo_rst(i) <= (not alignment_done_f(i)) or rst_hw or (not rxclk_locked) or GBT_General_ctrl(4);
        fifo_rden(i) <= not fifo_empty(i);

        fifo_inst: fifo_generator_fe
            PORT MAP(
                rst => fifo_rst(i),--rst_hw,
                wr_clk => rxrecclk40m,
                rd_clk => rxrecclk40m,
                din => downLinkData_i(i),
                wr_en => '1',--fifo_wr(i),
                rd_en => fifo_rden(i),
                dout => downLinkData_o(i),
                full  => open,
                empty  => fifo_empty(i),
                prog_full  => open,
                prog_empty  => open,
                wr_rst_busy  => open,
                rd_rst_busy  => open
            );

    end generate;

    reset_rxclkgen <=(not alignment_done_f(0)) or GBT_General_ctrl(41);--RL

    rxclkgen_inst: rxclkgen
        port map
    (

            clk_out1 => rxrecclk40m,

            reset => reset_rxclkgen,--RL
            locked => rxclk_locked,

            clk_in1=> GT_RX_WORD_CLK(0)
        );

    --MT SIMU+
    outclk_sim : if sim_emulator = true generate
        GT_TXOUTCLK <= (others => clk320_in);
        GT_RXOUTCLK <= (others => clk320_in);
    end generate;

    clk_generate : for i in 0 to GBT_NUM-1 generate

        GTTXOUTCLK_BUFG: bufg_gt -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
            port map(
                o => GT_TXUSRCLK(i),
                ce => '1',
                cemask => '0',
                clr => '0', --userclk_tx_reset_in,--'0',
                clrmask => '0',
                div => "000",
                i => GT_TXOUTCLK(i)
            );

        GT_TX_WORD_CLK(i) <= GT_TXUSRCLK(i);

        GTRXOUTCLK_BUFG: bufg_gt -- @suppress "Generic map uses default values. Missing optional actuals: SIM_DEVICE, STARTUP_SYNC"
            port map(
                o => GT_RXUSRCLK(i),
                ce => '1',
                cemask => '0',
                clr => '0', --userclk_tx_reset_in,--'0',
                clrmask => '0',
                div => "000",
                i => GT_RXOUTCLK(i)
            );

        GT_RX_WORD_CLK(i) <= GT_RXUSRCLK(i);

        --RL
        BUFGCE_DIV_inst : BUFGCE_DIV
            generic map (
                BUFGCE_DIVIDE => 8, -- 1-8 -divide by 8 to get 40 MHz
                -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
                IS_CE_INVERTED => '0', -- Optional inversion for CE
                IS_CLR_INVERTED => '0', -- Optional inversion for CLR
                IS_I_INVERTED => '0' -- Optional inversion for I
            )
            port map (
                O => txclk40m(i), -- 1-bit output: Buffer
                CE => '1', -- 1-bit input: Buffer enable
                CLR => '0', -- 1-bit input: Asynchronous clear
                I => GT_TX_WORD_CLK(i) -- 1-bit input: Buffer 240 MHz RXUSER_CLK after BUFG_GT
            );
    -- End of BUFGCE_DIV_inst instantiation
    end generate;


    mgt_sim : if sim_emulator = true generate
        loopMGT : for i in GBT_NUM-1 downto 0 generate
            RX_DATA_16b(i) <= TX_DATA_32b(i)(15 downto 0); --data looped back
        end generate;
        loopMGTQUAD : for i in (GBT_NUM-1)/4 downto 0 generate
            MON_RXRSTDONE(4*i+3 downto 4*i)             <= "1111";
            MON_TXRSTDONE(4*i+3 downto 4*i)             <= "1111";
            MON_RXRSTDONE_QUAD(i downto i)              <= "1";
            MON_TXRSTDONE_QUAD(i downto i)              <= "1";
            MON_RXPMARSTDONE(4*i+3 downto 4*i)          <= "1111";
            MON_TXPMARSTDONE(4*i+3 downto 4*i)          <= "1111";
            MON_RXCDR_LCK(4*i+3 downto 4*i)             <= "1111";
            MON_RXCDR_LCK_QUAD(i downto i)              <= "1";
        end generate;
    end generate; --mgt_sim

    mgt_notsim : if sim_emulator = false generate
        port_trans : for i in 0 to GBT_NUM-1 generate
            RX_N_i(i)   <= RX_N(i);
            RX_P_i(i)   <= RX_P(i);
            TX_N(i)     <= TX_N_i(i);
            TX_P(i)     <= TX_P_i(i);
        end generate;

        GTH_inst : for i in 0 to (GBT_NUM/4)-1 generate

            RX_DATA_16b(4*i+0) <= RX_DATA_64b(i)(15 downto 0);
            RX_DATA_16b(4*i+1) <= RX_DATA_64b(i)(31 downto 16);
            RX_DATA_16b(4*i+2) <= RX_DATA_64b(i)(47 downto 32);
            RX_DATA_16b(4*i+3) <= RX_DATA_64b(i)(63 downto 48);
            drpclk_vec(i)       <= clk40_in;
            TX_DATA_128b(i) <= TX_DATA_32b(4*i+3) & TX_DATA_32b(4*i+2) & TX_DATA_32b(4*i+1) & TX_DATA_32b(4*i+0);

            --- QPLL Reset Process----- MT
            QpllLock_inv <= not qpll1_lck_i;

            process(clk40_in)
            begin
                if clk40_in'event and clk40_in='1' then
                    QPLL_PIPE(3 downto 1) <= QPLL_PIPE(2 downto 0);
                    QPLL_PIPE(0) <= LMK_LD;
                    if ((QPLL_PIPE(3) = '0') AND (QPLL_PIPE(0) = '1')) then --rising edge
                        QPLL_RESET_LMK  <= QpllLock_inv;
                    else
                        QPLL_RESET_LMK  <= (others =>'0');
                    end if;
                end if;
            end process;

            --MT added
            MON_QPLL_LCK <= qpll1_lck_i;
            --
            --RL
            reset_tx_pll_and_datapath_in(i) <= CTRL_TXPLL_DATAPATH_RESET(i) or QPLL_RESET_LMK(i);
            GTH_TOP_INST: entity work.FLX_LpGBT_GTH_FE_FELIG
                Port map
        (
                    gthrxn_in => RX_N_i(4*i+3 downto 4*i),
                    gthrxp_in => RX_P_i(4*i+3 downto 4*i),
                    gthtxn_out => TX_N_i(4*i+3 downto 4*i),
                    gthtxp_out => TX_P_i(4*i+3 downto 4*i),
                    drpclk_in => drpclk_vec(i downto i),
                    --MT commented
                    --gtrefclk0_in => GTHREFCLK(4*i downto 4*i), --in master
                    --MT added
                    gtrefclk0_in => GTHREFCLK0(4*i downto 4*i), --RX 320
                    gtrefclk1_in => GTHREFCLK1(4*i downto 4*i), --TX 240
                    gt_rxusrclk_in => GT_RX_WORD_CLK(4*i+3 downto 4*i),
                    gt_rxoutclk_out => GT_RXOUTCLK(4*i+3 downto 4*i),
                    gt_txusrclk_in => GT_TX_WORD_CLK(4*i+3 downto 4*i),
                    gt_txoutclk_out => GT_TXOUTCLK(4*i+3 downto 4*i),
                    userdata_tx_in => TX_DATA_128b(i),
                    userdata_rx_out => RX_DATA_64b(i),
                    rxpolarity_in => CTRL_RXPOLARITY(4*i+3 downto 4*i),
                    txpolarity_in => CTRL_TXPOLARITY(4*i+3 downto 4*i),
                    loopback_in => "000",
                    rxcdrhold_in => '0',
                    userclk_rx_reset_in => userclk_rx_reset_in(i downto i), --(others=>(not rxpmaresetdone_out(i))),--locked,
                    userclk_tx_reset_in => userclk_tx_reset_in(i downto i), --(others=>(not txpmaresetdone_out(i))),--,--locked,
                    reset_all_in => CTRL_SOFT_RESET(i downto i),
                    --MT commented
                    --reset_tx_pll_and_datapath_in => CTRL_TXPLL_DATAPATH_RESET(i downto i), --RL replaced with line below
                    reset_tx_pll_and_datapath_in => reset_tx_pll_and_datapath_in(i downto i),
                    reset_tx_datapath_in => CTRL_TX_DATAPATH_RESET(i downto i),
                    reset_rx_pll_and_datapath_in => CTRL_RXPLL_DATAPATH_RESET(i downto i),
                    reset_rx_datapath_in => RX_DATAPATH_RESET_FINL(i downto i),
                    qpll0lock_out => open,
                    qpll1lock_out => qpll1_lck_i(i downto i), --MT MON_QPLL_LCK(i downto i),--RL changed in FELIG
                    qpll1fbclklost_out => open,
                    qpll0fbclklost_out => open,
                    rxslide_in => RxSlide(4*i+3 downto 4*i),
                    cplllock_out => MON_CPLL_LCK(4*i+3 downto 4*i),
                    rxresetdone_out => rxresetdone(4*i+3 downto 4*i),
                    txresetdone_out => TxResetDone(4*i+3 downto 4*i),
                    rxpmaresetdone_out => rxpmaresetdone(4*i+3 downto 4*i),
                    txpmaresetdone_out => txpmaresetdone(4*i+3 downto 4*i),

                    reset_tx_done_out => txresetdone_quad(i downto i),
                    reset_rx_done_out => rxresetdone_quad(i downto i),
                    reset_rx_cdr_stable_out => RxCdrLock_quad(i downto i),
                    rxcdrlock_out => rxcdrlock_out(4*i+3 downto 4*i)
                );


            MON_RXRSTDONE(4*i+3 downto 4*i)             <= rxresetdone(4*i+3 downto 4*i);
            MON_TXRSTDONE(4*i+3 downto 4*i)             <= TxResetDone(4*i+3 downto 4*i);
            MON_RXRSTDONE_QUAD(i downto i)              <= rxresetdone_quad(i downto i);
            MON_TXRSTDONE_QUAD(i downto i)              <= txresetdone_quad(i downto i);
            MON_RXPMARSTDONE(4*i+3 downto 4*i)          <= rxpmaresetdone(4*i+3 downto 4*i);
            MON_TXPMARSTDONE(4*i+3 downto 4*i)          <= txpmaresetdone(4*i+3 downto 4*i);
            MON_RXCDR_LCK(4*i+3 downto 4*i)             <= RxCdrLock(4*i+3 downto 4*i);
            MON_RXCDR_LCK_QUAD(i downto i)              <= RxCdrLock_quad(i downto i);

            process(clk40_in)
            begin
                if clk40_in'event and clk40_in='1' then
                    if cdr_cnt ="00000000000000000000" then
                        RxCdrLock_a(4*i)     <= rxcdrlock_out(4*i);
                        RxCdrLock_a(4*i+1)   <= rxcdrlock_out(4*i+1);
                        RxCdrLock_a(4*i+2)   <= rxcdrlock_out(4*i+2);
                        RxCdrLock_a(4*i+3)   <= rxcdrlock_out(4*i+3);
                    else
                        RxCdrLock_a(4*i) <= RxCdrLock_a(4*i) and rxcdrlock_out(4*i);
                        RxCdrLock_a(4*i+1) <= RxCdrLock_a(4*i+1) and rxcdrlock_out(4*i+1);
                        RxCdrLock_a(4*i+2) <= RxCdrLock_a(4*i+2) and rxcdrlock_out(4*i+2);
                        RxCdrLock_a(4*i+3) <= RxCdrLock_a(4*i+3) and rxcdrlock_out(4*i+3);
                    end if;
                    if cdr_cnt="00000000000000000000" then
                        RxCdrLock_int(4*i) <=RxCdrLock_a(4*i);
                        RxCdrLock_int(4*i+1) <=RxCdrLock_a(4*i+1);
                        RxCdrLock_int(4*i+2) <=RxCdrLock_a(4*i+2);
                        RxCdrLock_int(4*i+3) <=RxCdrLock_a(4*i+3);
                    end if;
                end if;
            end process;

            RxCdrLock(4*i) <= (not CTRL_CHANNEL_DISABLE(4*i)) and RxCdrLock_int(4*i);
            RxCdrLock(4*i+1) <= (not CTRL_CHANNEL_DISABLE(4*i+1)) and RxCdrLock_int(4*i+1);
            RxCdrLock(4*i+2) <= (not CTRL_CHANNEL_DISABLE(4*i+2)) and RxCdrLock_int(4*i+2);
            RxCdrLock(4*i+3) <= (not CTRL_CHANNEL_DISABLE(4*i+3)) and RxCdrLock_int(4*i+3);


            userclk_rx_reset_in(i) <=not (rxpmaresetdone(4*i+0) or rxpmaresetdone(4*i+1) or rxpmaresetdone(4*i+2) or rxpmaresetdone(4*i+3));
            userclk_tx_reset_in(i) <=not (txpmaresetdone(4*i+0) or txpmaresetdone(4*i+1) or txpmaresetdone(4*i+2) or txpmaresetdone(4*i+3));

            RX_DATAPATH_RESET_FINL(i) <= CTRL_RX_DATAPATH_RESET(i) or (auto_gth_rxrst(4*i) and RxCdrLock(4*i))
                                         or (auto_gth_rxrst(4*i+1) and RxCdrLock(4*i+1))
                                         or (auto_gth_rxrst(4*i+2) and RxCdrLock(4*i+2))
                                         or (auto_gth_rxrst(4*i+3) and RxCdrLock(4*i+3)) ;



        end generate;


        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                cdr_cnt <=cdr_cnt+'1';
            end if;
        end process;
    end generate; --mgt_notsim

    --MT externalizing gtrx/txusrclk for FELIG logic
    GT_TXUSRCLK_OUT <= GT_TXUSRCLK(GBT_NUM-1 downto 0);
    GT_RXUSRCLK_OUT <= GT_RXUSRCLK(GBT_NUM-1 downto 0);



end Behavioral;
