
library ieee;
    use ieee.std_logic_1164.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
    use work.centralRouter_package.all;

entity link_wrapper_FELIG is --Modified from link_wrapper by Ricardo Luz rluz@anl.gov
    generic(
        GBT_NUM : integer := 4; -- number of GBT channels
        CARD_TYPE : integer := 712;
        GTHREFCLK_SEL : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
        FIRMWARE_MODE : integer := 0;
        PLL_SEL : std_logic := '1'; -- 0: CPLL, 1: QPLL
        GTREFCLKS : integer := 5;
        OPTO_TRX : integer := 4;  -- number of optical transceivers
        sim_emulator : boolean := false);
    port (
        register_map_control : in register_map_control_type;
        register_map_link_monitor : out register_map_link_monitor_type;
        clk40 : in std_logic;
        clk240 : in std_logic;
        clk40_xtal : in std_logic;
        GTREFCLK_N_in : in std_logic_vector(GTREFCLKS-1 downto 0);
        GTREFCLK_P_in : in std_logic_vector(GTREFCLKS-1 downto 0);
        rst_hw : in  std_logic;
        OPTO_LOS : in std_logic_vector(OPTO_TRX-1 downto 0);
        RXUSRCLK_OUT : out std_logic_vector(GBT_NUM-1 downto 0);
        GBT_DOWNLINK_USER_DATA : in txrx120b_type(0 to (GBT_NUM-1));
        GBT_UPLINK_USER_DATA : out txrx120b_type(0 to (GBT_NUM-1));
        lpGBT_DOWNLINK_USER_DATA : in  txrx224b_type(0 to GBT_NUM-1); --RL; FELIG is different from FELIX. It recieves (UPLINK) the txrx32b and it sends (downlink) the txrx224b
        lpGBT_DOWNLINK_IC_DATA   : in  txrx2b_type(0 to GBT_NUM-1);
        lpGBT_DOWNLINK_EC_DATA   : in  txrx2b_type(0 to GBT_NUM-1);
        lpGBT_UPLINK_USER_DATA   : out txrx32b_type(0 to GBT_NUM-1);
        lpGBT_UPLINK_EC_DATA     : out txrx2b_type(0 to GBT_NUM-1);
        lpGBT_UPLINK_IC_DATA     : out txrx2b_type(0 to GBT_NUM-1);
        LinkAligned : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_P : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_P : in std_logic_vector(GBT_NUM-1 downto 0);
        RX_N : in std_logic_vector(GBT_NUM-1 downto 0);
        GTH_FM_RX_33b_out : out txrx33b_type;
        LMK_P : in std_logic_vector(7 downto 0);
        LMK_N : in std_logic_vector(7 downto 0);
        --FELIG specific signals
        link_rx_flag_i : out std_logic_vector(GBT_NUM-1 downto 0);
        link_tx_flag_i : out std_logic_vector(GBT_NUM-1 downto 0);
        TXUSRCLK_OUT : out std_logic_vector(GBT_NUM-1 downto 0);
        appreg_clk : in std_logic; --global_appreg_clk
        CLK40_FPGA2LMK_N : out std_logic;
        CLK40_FPGA2LMK_P : out std_logic;
        LMK_LD : in std_logic;
        RESET_TO_LMK : out std_logic;
        clk40_rxusrclk : out std_logic;
        clk320_in: in std_logic; --MT/RL simulation only
        LINKSconfig : in std_logic_vector(2 downto 0)
    );
end entity link_wrapper_FELIG;

architecture rtl of link_wrapper_FELIG is
begin

    --GBT, copied from phase1
    g_GBTMODE: if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT generate
        signal TX_FRAME_CLK_I      : std_logic_vector(GBT_NUM-1 downto 0);
        signal gbt_tx_data_120b_array_i         : txrx120b_type(0 to GBT_NUM-1);
        signal gbt_rx_data_120b_array_i         : txrx120b_type(0 to GBT_NUM-1);
    begin

        u0: for i in 0 to GBT_NUM-1 generate
        begin
            TX_FRAME_CLK_I(i) <= clk40;
        end generate u0;

        gbt_tx_data_120b_array_i <= GBT_DOWNLINK_USER_DATA;
        GBT_UPLINK_USER_DATA <= gbt_rx_data_120b_array_i;

        u1: entity work.FELIX_gbt_wrapper_FELIGKCU --_sim
            generic map(
                STABLE_CLOCK_PERIOD       => 24,
                GBT_NUM                   => GBT_NUM,
                CARD_TYPE                 => CARD_TYPE,
                PLL_SEL                   => PLL_SEL,
                GTHREFCLK_SEL             => GTHREFCLK_SEL,
                sim_emulator              => sim_emulator)
            port map(
                RX_FLAG_O                 => link_rx_flag_i, --MT open,
                TX_FLAG_O                 => link_tx_flag_i, --MT open,
                REFCLK_CXP1               => open, --REFCLK_CXP1,RL: not used
                REFCLK_CXP2               => open,
                REFCLK_CXP1_LMK            => open, -- added MT/SS for LMK03200
                REFCLK_CXP2_LMK            => open,
                rst_hw                    => rst_hw,
                register_map_control      => register_map_control,
                register_map_gbt_monitor  => register_map_link_monitor,
                DRP_CLK_IN                => appreg_clk,
                Q2_CLK0_GTREFCLK_PAD_N_IN => GTREFCLK_N_in(0), -- copied from phase2 link wrapper Q2_CLK0_GTREFCLK_PAD_N_IN
                Q2_CLK0_GTREFCLK_PAD_P_IN => GTREFCLK_P_in(0), --Q2_CLK0_GTREFCLK_PAD_P_IN
                Q8_CLK0_GTREFCLK_PAD_N_IN => GTREFCLK_N_in(1), --Q8_CLK0_GTREFCLK_PAD_N_IN
                Q8_CLK0_GTREFCLK_PAD_P_IN => GTREFCLK_P_in(1), --Q8_CLK0_GTREFCLK_PAD_P_IN
                Q4_CLK0_GTREFCLK_PAD_N_IN => GTREFCLK_N_in(2), --Q4_CLK0_GTREFCLK_PAD_N_IN
                Q4_CLK0_GTREFCLK_PAD_P_IN => GTREFCLK_P_in(2), --Q4_CLK0_GTREFCLK_PAD_P_IN
                Q5_CLK0_GTREFCLK_PAD_N_IN => GTREFCLK_N_in(3), --Q5_CLK0_GTREFCLK_PAD_N_IN
                Q5_CLK0_GTREFCLK_PAD_P_IN => GTREFCLK_P_in(3), --Q5_CLK0_GTREFCLK_PAD_P_IN
                Q6_CLK0_GTREFCLK_PAD_N_IN => GTREFCLK_N_in(4), --Q6_CLK0_GTREFCLK_PAD_N_IN
                Q6_CLK0_GTREFCLK_PAD_P_IN => GTREFCLK_P_in(4), --Q6_CLK0_GTREFCLK_PAD_P_IN
                LMK_GTH_REFCLK1_P         => LMK_P(1), --LMK_GTH_REFCLK1_P, -- LMK03200 GTH REF clocks
                LMK_GTH_REFCLK1_N         => LMK_N(1), --LMK_GTH_REFCLK1_N,
                LMK_GTH_REFCLK3_P         => LMK_P(3), --LMK_GTH_REFCLK3_P,
                LMK_GTH_REFCLK3_N         => LMK_N(3), --LMK_GTH_REFCLK3_N,
                GREFCLK_IN                => clk240,
                clk40_in                  => clk40,
                clk240_in                 => clk240,
                TX_120b_in                => gbt_tx_data_120b_array_i, --MT TX_120b_in_combined,
                RX_120b_out               => gbt_rx_data_120b_array_i, --MT RX_120b_out,
                FRAME_LOCKED_O            => LinkAligned,  --MT route this lane_monitor.gbt.frame_locked as John does?
                TX_FRAME_CLK_I            => TX_FRAME_CLK_I,
                GT_TXUSRCLK_OUT           => TXUSRCLK_OUT,
                GT_RXUSRCLK_OUT           => RXUSRCLK_OUT,
                TX_P                      => TX_P,
                TX_N                      => TX_N,
                RX_P                      => RX_P,
                RX_N                      => RX_N,
                CLK40_FPGA2LMK_out_P      => CLK40_FPGA2LMK_P, -- LMK03200 clock input pins added --MT/SS
                CLK40_FPGA2LMK_out_N      => CLK40_FPGA2LMK_N, -- LMK03200 clock input pins added --MT/SS
                LMK_LD                    => LMK_LD,
                RESET_TO_LMK              => RESET_TO_LMK
            --
            );


    end generate g_GBTMODE;

    --  lpGBT, moved from top
    g_lpGBTMODE: if FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT generate
        signal TX_2b_IC_in                 : txrx2b_type(0 to GBT_NUM-1);
        signal TX_2b_EC_in                 : txrx2b_type(0 to GBT_NUM-1);
        signal TX_224b_DATA_in             : txrx224b_type(0 to GBT_NUM-1);
        signal RX_2b_IC_out                : txrx2b_type(0 to GBT_NUM-1);
        signal RX_2b_EC_out                : txrx2b_type(0 to GBT_NUM-1);
        signal RX_32b_DATA_out             : txrx32b_type(0 to GBT_NUM-1);
        signal clk40_rxusrclk_lp           : std_logic;
    begin

        TX_2b_IC_in              <= lpGBT_DOWNLINK_IC_DATA;
        TX_2b_EC_in              <= lpGBT_DOWNLINK_EC_DATA;
        TX_224b_DATA_in          <= lpGBT_DOWNLINK_USER_DATA;
        lpGBT_UPLINK_IC_DATA     <= RX_2b_IC_out;
        lpGBT_UPLINK_EC_DATA     <= RX_2b_EC_out;
        lpGBT_UPLINK_USER_DATA   <= RX_32b_DATA_out;
        clk40_rxusrclk           <= clk40_rxusrclk_lp;

        u2: entity work.FELIX_LpGBT_Wrapper_FELIG
            generic map(       STABLE_CLOCK_PERIOD => 24,
                GBT_NUM                   => GBT_NUM,
                PRBS_TEST_EN              => 0,
                CARD_TYPE                 => CARD_TYPE,
                PLL_SEL                   => PLL_SEL,
                GTHREFCLK_SEL             => GTHREFCLK_SEL,
                CLK_CHIP_SEL              => 1,
                sim_emulator              => sim_emulator)
            port map(
                rst_hw                    => rst_hw,
                rxrecclk40m_out           => open,
                register_map_control      => register_map_control, --RL changed to match module
                register_map_link_monitor => register_map_link_monitor,
                CLK40_IN                  => clk40,
                clk320_in                 => clk320_in, --MT SIMU+
                GREFCLK_IN                => clk240, --MT comment: it is not connected anywhere other than simu (added by me)
                RX320_CH0                 => open,
                GTREFCLK_P_s              => GTREFCLK_P_in,--GTREFCLK_P_s,
                GTREFCLK_N_s              => GTREFCLK_N_in,--GTREFCLK_N_s,
                LMK_P                     => LMK_P,
                LMK_N                     => LMK_N,
                RX_LINK_LCK               => open,
                TX_2b_IC_in               => TX_2b_IC_in, --RL: TX_256b_in divided into components
                TX_2b_EC_in               => TX_2b_EC_in,
                TX_224b_DATA_in           => TX_224b_DATA_in,
                RX_2b_IC_out              => RX_2b_IC_out,  --RL: RX_36b_out divided into components
                RX_2b_EC_out              => RX_2b_EC_out,
                RX_32b_DATA_out           => RX_32b_DATA_out,
                GT_TXUSRCLK_OUT           => TXUSRCLK_OUT, --gt_txusrclk_i,
                GT_RXUSRCLK_OUT           => RXUSRCLK_OUT, --gt_rxusrclk_i,
                clk40_rxusrclk_out        => clk40_rxusrclk_lp, --MT 40 MHZ rxusr clk to drive LMK
                TX_P                      => TX_P,
                TX_N                      => TX_N,
                RX_P                      => RX_P,
                RX_N                      => RX_N,
                sta_headerFlag_out        => link_rx_flag_i,--lpgbt_rx_flag_i --MT for FELIG from mgt_framealigner . 1 pulse every c_wordRatio (=8)
                tx_flag_out               => link_tx_flag_i,--lpgbt_tx_flag_i --MT added: needed by FELIG to latch tx payload to generated data. Need to be synchronized with emulator logic
                alignment_done_out        => LinkAligned, --MT added: alignment flag needed by the emulator,
                RESET_TO_LMK              => RESET_TO_LMK, --MT added
                LMK_LD                    => LMK_LD, --MT LMK lock signal
                LINKSconfig               => LINKSconfig, --RL added
                CLK40_FPGA2LMK_out_P      => CLK40_FPGA2LMK_P, -- LMK03200 clock input pins added --MT/SS/RL
                CLK40_FPGA2LMK_out_N      => CLK40_FPGA2LMK_N  -- LMK03200 clock input pins added --MT/SS/RL
            );
    end generate g_lpGBTMODE;
end architecture rtl ; -- of link_wrapper_FELIG

