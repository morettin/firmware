--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:                Kai Chen    (kchen@bnl.gov)
--! adapted for FELIG by:  Marco Trovato (mtrovato@anl.gov)
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: FELIX BNL-711 GBT Wrapper
-- Module Name: gbt_top - Behavioral
-- Project Name:
-- Target Devices: KCU
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX GBT & GTH
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
    use UNISIM.VComponents.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use ieee.numeric_std.all;
    use work.sim_lib.all; --MT SIMU+

entity FELIX_gbt_wrapper_FELIGKCU is
    Generic (
        STABLE_CLOCK_PERIOD         : integer   := 24;  --period of the drp_clock
        GBT_NUM                     : integer := 24;
        GTHREFCLK_SEL               : std_logic := '1'; --GREFCLK        : std_logic := '1';
        --MGTREFCLK      : std_logic := '0';
        CARD_TYPE                   : integer := 712;
        PLL_SEL                     : std_logic := '0';  -- CPLL : '0'
        -- QPLL : '1'
        --QUAD_NUM                  : integer := 6
        --MT added for simu
        sim_emulator            : boolean       := false
    --
    );
    Port (
        -------------------
        ---- For debug
        -------------------
        -- For Debugging
        RX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);
        REFCLK_CXP1                 : out std_logic;
        REFCLK_CXP2                 : out std_logic;
        -- added MT/SS for LMK03200
        REFCLK_CXP1_LMK                 : out std_logic;
        REFCLK_CXP2_LMK                 : out std_logic;
        --
        rst_hw                      : in std_logic;

        register_map_control        : in register_map_control_type;
        register_map_gbt_monitor    : out register_map_link_monitor_type;

        -- GTH REFCLK, DRPCLK, GREFCLK
        DRP_CLK_IN                  : in std_logic;
        Q2_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q2_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        Q8_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q8_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        Q4_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q4_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        Q5_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q5_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        Q6_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
        Q6_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
        GREFCLK_IN                  : in std_logic;
        -- LMK03200 GTH REF clocks -- MT/SS
        LMK_GTH_REFCLK1_P         : in     std_logic;
        LMK_GTH_REFCLK1_N         : in     std_logic;
        LMK_GTH_REFCLK3_P         : in     std_logic;
        LMK_GTH_REFCLK3_N         : in     std_logic;
        --
        clk40_in                    : in std_logic;
        clk240_in                   : in std_logic;
        -- for CentralRouter
        TX_120b_in                  : in  txrx120b_type(0 to GBT_NUM-1);
        RX_120b_out                 : out txrx120b_type(0 to GBT_NUM-1);
        FRAME_LOCKED_O              : out std_logic_vector(GBT_NUM-1 downto 0);
        -- TX_ISDATA_I              : in std_logic_vector(GBT_NUM-1 downto 0);
        -- RX_ISDATA_O              : out std_logic_vector(GBT_NUM-1 downto 0);
        -- RX_FRAME_CLK_O           : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_FRAME_CLK_I              : in std_logic_vector(GBT_NUM-1 downto 0);

        -- FIFO_RD_CLK              : in std_logic_vector(GBT_NUM-1 downto 0);
        -- FIFO_RD_EN               : in std_logic_vector(GBT_NUM-1 downto 0);
        -- FIFO_FULL                : out std_logic_vector(GBT_NUM-1 downto 0);
        -- FIFO_EMPTY               : out std_logic_vector(GBT_NUM-1 downto 0);

        --MT externalizing gtrx/txusrclk for FELIG logic
        GT_TXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);
        GT_RXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);
        --
        -- GTH Data pins
        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);
        RX_P                        : in  std_logic_vector(GBT_NUM-1 downto 0);
        RX_N                        : in  std_logic_vector(GBT_NUM-1 downto 0);
        -- LMK03200 clock input pins added --MT/SS
        CLK40_FPGA2LMK_out_P        : out std_logic;
        CLK40_FPGA2LMK_out_N        : out std_logic;
        --
        --    clk_adn_160                 : in  std_logic; --added and then commented
        --    since is unused MT/SS
        LMK_LD                      : in  std_logic; -- added MT/SS LMK lock signal
        RESET_TO_LMK                : out  std_logic --added MT/SS

    );
end FELIX_gbt_wrapper_FELIGKCU;

architecture Behavioral of FELIX_gbt_wrapper_FELIGKCU is



    component fifo_GBT2CR IS
        PORT (
            wr_clk    : IN STD_LOGIC;
            wr_rst    : IN STD_LOGIC;
            rd_clk    : IN STD_LOGIC;
            rd_rst    : IN STD_LOGIC;
            din       : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
            wr_en     : IN STD_LOGIC;
            rd_en     : IN STD_LOGIC;
            dout      : OUT STD_LOGIC_VECTOR(119 DOWNTO 0);
            full      : OUT STD_LOGIC;
            empty     : OUT STD_LOGIC;
            prog_empty : OUT STD_LOGIC
        );
    END component;

    -- constant QUAD_NUM : integer := GBT_NUM / 4;

    signal rxslide_manual : STD_LOGIC_VECTOR(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RxSlide_c      : STD_LOGIC_VECTOR(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RxSlide_i      : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal rxslide_sel    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal txusrrdy       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal rxusrrdy       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal gttx_reset     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal gtrx_reset     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal soft_reset     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal cpll_reset     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal qpll_reset     : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal txresetdone    : std_logic_vector(47 downto 0) := (others=>'1'); --MT SIMU+ defaults

    signal clk_sampled    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal rxresetdone    : std_logic_vector(47 downto 0) := (others=>'1'); --MT SIMU+ defaults
    signal txfsmresetdone : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal rxfsmresetdone : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal cpllfbclklost  : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal cplllock       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal rxcdrlock      : std_logic_vector(47 downto 0) := (others=>'1'); --MT SIMU+ defaults
    signal RxCdrLock_int  : std_logic_vector(47 downto 0) := (others=>'1'); --MT SIMU+ defaults
    signal qplllock       : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal cdr_cnt        : std_logic_vector(19 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal tx_is_data     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TX_RESET       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TX_RESET_i     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal RX_RESET       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RX_RESET_i     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GT_TXUSRCLK    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GT_RXUSRCLK    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RX_FLAG_Oi     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal gbt_data_format: std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal RX_ALIGN_SW    : std_logic                     := '0'; --MT SIMU+ added defaults;
    signal RX_ALIGN_TB_SW : std_logic                     := '0'; --MT SIMU+ added defaults;

    signal rx_pll_locked  : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal outsel_i       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal outsel_ii      : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal outsel_o       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RX_120b_out_i  : txrx120b_type(0 to (GBT_NUM-1));
    signal RX_120b_out_ii : txrx120b_type(0 to (GBT_NUM-1));

    signal rx_is_header   : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal alignment_done : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal rx_is_data     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RX_HEADER_FOUND: std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    --  signal RxSlide_tmp    : std_logic_vector(47 downto 0);
    signal RxSlide        : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal GT_TX_WORD_CLK : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TX_TC_METHOD   : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TC_EDGE        : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    type data20barray     is array (0 to 47) of std_logic_vector(19 downto 0);
    signal TX_DATA_20b    : data20barray := (others => ("00000000000000000000"));
    signal RX_DATA_20b    : data20barray := (others => ("00000000000000000000"));

    signal GT_RX_WORD_CLK         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal alignment_chk_rst_c    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal alignment_chk_rst_c1   : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal alignment_chk_rst      : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal alignment_chk_rst_f    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal rstframeclk            : std_logic := '0'; --MT SIMU+ added defaults;
    signal alignment_chk_rst_i    : std_logic := '0'; --MT SIMU+ added defaults;
    signal rstframeclk1           : std_logic := '0'; --MT SIMU+ added defaults;

    signal DESMUX_USE_SW          : std_logic := '0'; --MT SIMU+ added defaults;

    signal rstframeclk_3r         : std_logic := '0'; --MT SIMU+ added defaults;
    signal rstframeclk_r          : std_logic := '0'; --MT SIMU+ added defaults;
    signal rstframeclk_2r         : std_logic := '0'; --MT SIMU+ added defaults;
    signal rstframeclk1_3r        : std_logic := '0'; --MT SIMU+ added defaults;
    signal rstframeclk1_r         : std_logic := '0'; --MT SIMU+ added defaults;
    signal rstframeclk1_2r        : std_logic := '0'; --MT SIMU+ added defaults;
    signal cxp1_tx_pll_rst        : std_logic := '0'; --MT SIMU+ added defaults;
    signal cxp2_tx_pll_rst        : std_logic := '0'; --MT SIMU+ added defaults;
    signal SOFT_TXRST_GT          : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TopBot                 : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TopBot_C               : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TopBot_i               : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal SOFT_RXRST_GT          : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal SOFT_TXRST_ALL         : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal SOFT_RXRST_ALL         : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TX_OPT                 : std_logic_vector(95 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RX_OPT                 : std_logic_vector(95 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL DATA_TXFORMAT          : std_logic_vector(95 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal DATA_TXFORMAT_i        : std_logic_vector(95 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL DATA_RXFORMAT          : std_logic_vector(95 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal DATA_RXFORMAT_i        : std_logic_vector(95 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    SIGNAL OddEven                : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal OddEven_i              : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal OddEven_c              : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal ext_trig_realign       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal General_ctrl           : std_logic_vector(63 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal GBT_RXSLIDE            : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TXUSRRDY           : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_RXUSRRDY           : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_GTTX_RESET         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_GTRX_RESET         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_PLL_RESET          : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_SOFT_TX_RESET      : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_SOFT_RX_RESET      : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_ODDEVEN            : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TOPBOT             : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TX_TC_DLY_VALUE1   : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TX_TC_DLY_VALUE2   : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TX_TC_DLY_VALUE3   : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TX_TC_DLY_VALUE4   : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TX_OPT             : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_RX_OPT             : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_DATA_TXFORMAT      : std_logic_vector(95 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_DATA_RXFORMAT      : std_logic_vector(95 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TX_RESET           : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_RX_RESET           : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TX_TC_METHOD       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_TC_EDGE            : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GBT_OUTMUX_SEL         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    SIGNAL GBT_TXRESET_DONE       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_RXRESET_DONE       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TXPMARESETDONE         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RXPMARESETDONE         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal alignment_done_f       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal soft_reset_f           : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal fifo_empty             : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal userclk_rx_reset_in    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal userclk_tx_reset_in    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal TXPMARESETDONE_out     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RXPMARESETDONE_out     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    SIGNAL GBT_TXFSMRESET_DONE    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_RXFSMRESET_DONE    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_CPLL_FBCLK_LOST    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_PLL_LOCK           : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_RXCDR_LOCK         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_CLK_SAMPLED        : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_RX_IS_HEADER       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_RX_IS_DATA         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_RX_HEADER_FOUND    : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_ALIGNMENT_DONE     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_OUT_MUX_STATUS     : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_ERROR              : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL GBT_GBT_TOPBOT_C       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal rxcdrlock_a            : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    SIGNAL LOGIC_RST    : std_logic_vector(63 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL Channel_disable        : std_logic_vector(63 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL Mode_ctrl              : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    SIGNAL TX_TC_DLY_VALUE        : std_logic_vector(191 downto 0) := (others=>'0'); --MT SIMU+ added defaults);
    signal data_sel               : std_logic_vector(191 downto 0) := (others=>'0'); --MT SIMU+ added defaults);

    signal GTH_RefClk             : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GTH_RefClk_LMK         : std_logic_vector(23 downto 0) := (others=>'0'); --MT SIMU+ added defaults; --added MT/SS
    signal gbt_sel                : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal lock_lg                : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal pulse_cnt              : std_logic_vector(29 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal pulse_lg               : std_logic := '0'; --MT SIMU+;

    signal CXP1_GTH_RefClk        : std_logic := '0'; --MT SIMU+ added defaults;
    signal CXP2_GTH_RefClk        : std_logic := '0'; --MT SIMU+ added defaults;
    signal CXP4_GTH_RefClk        : std_logic := '0'; --MT SIMU+ added defaults;
    signal CXP3_GTH_RefClk        : std_logic := '0'; --MT SIMU+ added defaults;
    signal CXP5_GTH_RefClk        : std_logic := '0'; --MT SIMU+ added defaults;
    signal CXP1_GTH_RefClk_LMK    : std_logic := '0'; --MT SIMU+ added defaults; -- added MT/SS
    signal CXP2_GTH_RefClk_LMK    : std_logic := '0'; --MT SIMU+ added defaults; -- added MT/SS
    signal des_rxusrclk_cxp1      : std_logic := '0'; --MT SIMU+ added defaults;
    signal des_rxusrclk_cxp2      : std_logic := '0'; --MT SIMU+ added defaults;

    signal alignment_done_chk_cnt : std_logic_vector(12 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal alignment_done_a       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal fifo_rst               : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal fifo_rden              : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal clksampled             : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal des_rxusrclk           : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal error_orig             : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal error_f                : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal FSM_RST                : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal auto_gth_rxrst         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal auto_gbt_rxrst         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal gbt_rx_reset_i         : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal gtrx_reset_i           : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults

    signal TX_LINERATE            : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal RX_LINERATE            : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal GT_RXOUTCLK            : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal GT_TXOUTCLK            : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults

    signal BITSLIP_MANUAL_r       : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal BITSLIP_MANUAL_2r      : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    signal BITSLIP_MANUAL_3r      : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults
    type txrx80b_12ch_type        is array (11 downto 0) of std_logic_vector(79 downto 0);
    signal RX_DATA_80b            : txrx80b_12ch_type;
    signal TX_DATA_80b            : txrx80b_12ch_type;

    signal gttx_reset_merge       : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal gtrx_reset_merge       : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal rxcdrlock_quad         : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal rxresetdone_quad       : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal txresetdone_quad       : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal rxcdrlock_out          : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal RxResetDone_f          : std_logic_vector(47 downto 0) := (others=>'0'); --MT SIMU+ added defaults;

    signal RX_N_i                 : std_logic_vector(47 downto 0):=x"000000000000";
    signal RX_P_i                 : std_logic_vector(47 downto 0):=x"000000000000";
    signal TX_N_i                 : std_logic_vector(47 downto 0):=x"000000000000";
    signal TX_P_i                 : std_logic_vector(47 downto 0):=x"000000000000";

    signal drpclk_in              : std_logic_vector(0 downto 0) := (others=>'0'); --MT SIMU+ added defaults;
    signal GT_RXUSRCLK_40MHz      : std_logic; -- added MT/SS
    signal QPLL_RESET_LMK         : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults; -- added MT/SS (others=>'0');
    signal QpllLock_inv           : std_logic_vector(11 downto 0) := (others=>'0'); --MT SIMU+ added defaults; -- added MT/SS
    signal alignment_done_inv     : std_logic := '0'; --MT SIMU+
    signal QPLL_PIPE              : std_logic_vector(3 downto 0) := (others=>'0'); --MT SIMU+ added defaults; -- added MT/SS
    signal LMK_PIPE               : std_logic_vector(3 downto 0) := (others=>'0'); --MT SIMU+ added defaults; -- added MT/SS
    signal LMK_RESET              : std_logic := '0'; --MT SIMU+

--ila RL
--  signal ila_TX_120b_in         : txrx120b_type(0 to GBT_NUM-1);
--  signal ila_GT_TX_WORD_CLK     : std_logic_vector(47 downto 0);
--  signal ila_cplllock           : std_logic_vector(47 downto 0); --MT SIMU+ added defaults;
--  signal ila_qplllock           : std_logic_vector(11 downto 0);

--  COMPONENT  ila_link_frame IS
--    PORT (
--        clk : IN STD_LOGIC;
--        probe0 : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
--        probe1 : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
--        probe2 : IN STD_LOGIC_VECTOR(11 DOWNTO 0));
--  END COMPONENT  ;
begin

    FRAME_LOCKED_O <= alignment_done_f(GBT_NUM-1 downto 0);
    --MT SIMU+
    outclk_sim : if sim_emulator = true generate
        GT_RXOUTCLK <= (others => clk240_in);
        GT_TXOUTCLK <= (others => clk240_in);
    end generate;

    refclk_notsim : if sim_emulator = false generate
        -- GTHREFCLK_1 : if GTHREFCLK_SEL = '0' generate
        -- IBUFDS_GTE2
        REFCLK_CXP1 <= CXP1_GTH_RefClk;
        REFCLK_CXP2 <= CXP2_GTH_RefClk;

        --bank 126, 127, 128 use clk from bank 127
        ibufds_instq2_clk0 : IBUFDS_GTE3
            port map
    (
                O               =>   CXP1_GTH_RefClk,
                ODIV2           =>    open,
                CEB             =>   '0',
                I               =>   Q2_CLK0_GTREFCLK_PAD_P_IN,
                IB              =>   Q2_CLK0_GTREFCLK_PAD_N_IN
            );

        --bank 131, 132, 133 use clk from bank 132
        ibufds_instq8_clk0 : IBUFDS_GTE3
            port map
    (
                O               =>     CXP2_GTH_RefClk,
                ODIV2           =>    open,
                CEB             =>     '0',
                I               =>     Q8_CLK0_GTREFCLK_PAD_P_IN,
                IB              =>     Q8_CLK0_GTREFCLK_PAD_N_IN
            );

        --bank 231, 232, 233,use clk from bank 232
        ibufds_instq4_clk0 : IBUFDS_GTE3
            port map
    (
                O               =>     CXP3_GTH_RefClk,
                ODIV2           =>    open,
                CEB             =>     '0',
                I               =>     Q4_CLK0_GTREFCLK_PAD_P_IN,
                IB              =>     Q4_CLK0_GTREFCLK_PAD_N_IN
            );

        --bank 228 use clk from bank 228
        ibufds_instq5_clk0 : IBUFDS_GTE3
            port map
    (
                O               =>     CXP4_GTH_RefClk,
                ODIV2           =>    open,
                CEB             =>     '0',
                I               =>     Q5_CLK0_GTREFCLK_PAD_P_IN,
                IB              =>     Q5_CLK0_GTREFCLK_PAD_N_IN
            );

        --bank 224, 225 use clk from bank 225
        CXP5: if (CARD_TYPE = 712) generate
            ibufds_instq6_clk0 : IBUFDS_GTE3
                port map
      (
                    O               =>     CXP5_GTH_RefClk,
                    ODIV2           =>    open,
                    CEB             =>     '0',
                    I               =>     Q6_CLK0_GTREFCLK_PAD_P_IN,
                    IB              =>     Q6_CLK0_GTREFCLK_PAD_N_IN
                );
        end generate CXP5;
    end generate; --refclk_notsim


    --================================================================-----
    ----- Jitter cleaner LMK03200 GTH REF clks------------------------------
    --================================================================------
    refclk_notsim2 : if sim_emulator = false generate
        -- GTHREFCLK_1 : if GTHREFCLK_SEL = '0' generate -- Jitter cleaner LMK03200
        -- IBUFDS_GTE2
        REFCLK_CXP1_LMK <= CXP1_GTH_RefClk_LMK;  -- Jitter cleaner LMK03200
        REFCLK_CXP2_LMK <= CXP2_GTH_RefClk_LMK;  -- Jitter cleaner LMK03200

        --bank 126, 127, 128 use clk from bank 127 -- Jitter cleaner LMK03200
        ibufds_LMK1 : IBUFDS_GTE3
            port map
    (
                O               =>   CXP1_GTH_RefClk_LMK,
                ODIV2           =>    open,
                CEB             =>   '0',
                I               =>   LMK_GTH_REFCLK1_P,
                IB              =>   LMK_GTH_REFCLK1_N
            );

        --bank 131, 132, 133 use clk from bank 132 -- Jitter cleaner LMK03200
        ibufds_LMK2 : IBUFDS_GTE3
            port map
    (
                O               =>     CXP2_GTH_RefClk_LMK,
                ODIV2           =>    open,
                CEB             =>     '0',
                I               =>     LMK_GTH_REFCLK3_P,
                IB              =>     LMK_GTH_REFCLK3_N
            );
        --================================================================-----
        ----- End Jitter cleaner LMK03200 GTH REF clks-------------------------
        --================================================================------

        --SLR0 banks: 126, 127, 128, 224, 225, 228
        --SLR1 banks: 131, 132, 133, 231, 232, 233
        g_refclk_8ch: if (GBT_NUM <= 8) generate
            GTH_RefClk( 0)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 1)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 2)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 3)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)

            GTH_RefClk( 4)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk( 5)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk( 6)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk( 7)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
        end generate g_refclk_8ch;

        g_refclk_16ch: if ((8 < GBT_NUM) and (GBT_NUM <= 16)) generate
            GTH_RefClk( 0)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 1)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 2)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 3)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 4)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 5)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 6)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 7)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)

            GTH_RefClk( 8)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk( 9)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(10)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(11)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(12)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(13)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(14)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(15)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
        end generate g_refclk_16ch;

        g_refclk_24ch: if ((16 < GBT_NUM) and (GBT_NUM <= 24)) generate
            GTH_RefClk( 0)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 1)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 2)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 3)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 4)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 5)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 6)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 7)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 8)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk( 9)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(10)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(11)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)

            GTH_RefClk(12)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(13)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(14)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(15)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(16)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(17)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(18)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(19)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(20)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(21)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(22)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(23)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
        end generate g_refclk_24ch;

        g_refclk_48ch: if ((24 < GBT_NUM) and (GBT_NUM <= 48)) generate
            GTH_RefClk( 0)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 1)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 2)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 3)         <= CXP1_GTH_RefClk;--bank 128 (up to  8 channels)
            GTH_RefClk( 4)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 5)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 6)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 7)         <= CXP1_GTH_RefClk;--bank 127 (up to 16 channels)
            GTH_RefClk( 8)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk( 9)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(10)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(11)         <= CXP1_GTH_RefClk;--bank 126 (up to 24 channels)
            GTH_RefClk(12)         <= CXP4_GTH_RefClk;--bank 228 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(13)         <= CXP4_GTH_RefClk;--bank 228 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(14)         <= CXP4_GTH_RefClk;--bank 228 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(15)         <= CXP4_GTH_RefClk;--bank 228 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(16)         <= CXP5_GTH_RefClk;--bank 224 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(17)         <= CXP5_GTH_RefClk;--bank 224 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(18)         <= CXP5_GTH_RefClk;--bank 224 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(19)         <= CXP5_GTH_RefClk;--bank 224 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(20)         <= CXP5_GTH_RefClk;--bank 225 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(21)         <= CXP5_GTH_RefClk;--bank 225 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(22)         <= CXP5_GTH_RefClk;--bank 225 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(23)         <= CXP5_GTH_RefClk;--bank 225 (up to 48 channels) - BNL712 ONLY

            GTH_RefClk(24)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(25)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(26)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(27)         <= CXP2_GTH_RefClk;--bank 133 (up to  8 channels)
            GTH_RefClk(28)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(29)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(30)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(31)         <= CXP2_GTH_RefClk;--bank 132 (up to 16 channels)
            GTH_RefClk(32)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(33)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(34)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(35)         <= CXP2_GTH_RefClk;--bank 131 (up to 24 channels)
            GTH_RefClk(36)         <= CXP3_GTH_RefClk;--bank 231 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(37)         <= CXP3_GTH_RefClk;--bank 231 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(38)         <= CXP3_GTH_RefClk;--bank 231 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(39)         <= CXP3_GTH_RefClk;--bank 231 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(40)         <= CXP3_GTH_RefClk;--bank 232 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(41)         <= CXP3_GTH_RefClk;--bank 232 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(42)         <= CXP3_GTH_RefClk;--bank 232 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(43)         <= CXP3_GTH_RefClk;--bank 232 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(44)         <= CXP3_GTH_RefClk;--bank 233 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(45)         <= CXP3_GTH_RefClk;--bank 233 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(46)         <= CXP3_GTH_RefClk;--bank 233 (up to 48 channels) - BNL712 ONLY
            GTH_RefClk(47)         <= CXP3_GTH_RefClk;--bank 233 (up to 48 channels) - BNL712 ONLY
        end generate g_refclk_48ch;

        --  GTH Ref clock from LMK03200 ---  added by RL
        g_refclk_8ch_LMK: if (GBT_NUM <= 8) generate
            GTH_RefClk_LMK( 0)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 1)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 2)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 3)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)

            GTH_RefClk_LMK( 4)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK( 5)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK( 6)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK( 7)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
        end generate g_refclk_8ch_LMK;

        --  GTH Ref clock from LMK03200 ---  added by RL
        g_refclk_16ch_LMK: if ((8 < GBT_NUM) and (GBT_NUM <= 16)) generate
            GTH_RefClk_LMK( 0)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 1)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 2)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 3)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 4)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 5)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 6)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 7)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)

            GTH_RefClk_LMK( 8)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK( 9)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(10)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(11)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(12)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(13)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(14)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(15)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
        end generate g_refclk_16ch_LMK;

        --  GTH Ref clock from LMK03200 ---  added by MT/SS
        g_refclk_24ch_LMK: if ((16 < GBT_NUM) and (GBT_NUM <= 24)) generate
            GTH_RefClk_LMK( 0)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 1)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 2)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 3)         <= CXP1_GTH_RefClk_LMK;--bank 128 (up to  8 channels)
            GTH_RefClk_LMK( 4)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 5)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 6)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 7)         <= CXP1_GTH_RefClk_LMK;--bank 127 (up to 16 channels)
            GTH_RefClk_LMK( 8)         <= CXP1_GTH_RefClk_LMK;--bank 126 (up to 24 channels)
            GTH_RefClk_LMK( 9)         <= CXP1_GTH_RefClk_LMK;--bank 126 (up to 24 channels)
            GTH_RefClk_LMK(10)         <= CXP1_GTH_RefClk_LMK;--bank 126 (up to 24 channels)
            GTH_RefClk_LMK(11)         <= CXP1_GTH_RefClk_LMK;--bank 126 (up to 24 channels)

            GTH_RefClk_LMK(12)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(13)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(14)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(15)         <= CXP2_GTH_RefClk_LMK;--bank 133 (up to  8 channels)
            GTH_RefClk_LMK(16)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(17)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(18)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(19)         <= CXP2_GTH_RefClk_LMK;--bank 132 (up to 16 channels)
            GTH_RefClk_LMK(20)         <= CXP2_GTH_RefClk_LMK;--bank 131 (up to 24 channels)
            GTH_RefClk_LMK(21)         <= CXP2_GTH_RefClk_LMK;--bank 131 (up to 24 channels)
            GTH_RefClk_LMK(22)         <= CXP2_GTH_RefClk_LMK;--bank 131 (up to 24 channels)
            GTH_RefClk_LMK(23)         <= CXP2_GTH_RefClk_LMK;--bank 131 (up to 24 channels)
        end generate g_refclk_24ch_LMK;
    end generate; -- refclk_notsim2;

    --IG  GTH_RefClk(0)         <= CXP1_GTH_RefClk;
    --IG  GTH_RefClk(1)         <= CXP1_GTH_RefClk;
    --IG  GTH_RefClk(2)         <= CXP1_GTH_RefClk;
    --IG  GTH_RefClk(3)         <= CXP1_GTH_RefClk;
    --IG  GTH_RefClk(4)         <= CXP1_GTH_RefClk;
    --IG  GTH_RefClk(5)         <= CXP1_GTH_RefClk;
    --IG  GTH_RefClk(6)         <= CXP1_GTH_RefClk;
    --IG  GTH_RefClk(7)         <= CXP1_GTH_RefClk;
    --IG
    --IG    -- For 16 channels (and below) put 8 channels in SRL0, channel 8..15 in SRL1.
    --IG  g_refclk0: if GBT_NUM <= 16 generate
    --IG    GTH_RefClk(8)       <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(9)       <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(10)      <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(11)      <= CXP2_GTH_RefClk;
    --IG  end generate;
    --IG
    --IG  -- For 24 channels put 12 channels in SRL0, channel 12..23 in SRL1.
    --IG  g_refclk1: if GBT_NUM > 16 generate
    --IG    GTH_RefClk(8)       <= CXP1_GTH_RefClk;
    --IG    GTH_RefClk(9)       <= CXP1_GTH_RefClk;
    --IG    GTH_RefClk(10)      <= CXP1_GTH_RefClk;
    --IG    GTH_RefClk(11)      <= CXP1_GTH_RefClk;
    --IG  end generate;
    --IG
    --IG refclkgen_v2p0 : if CARD_TYPE=712 generate
    --IG    g_refclk11: if GBT_NUM <25 generate
    --IG   --IBUFDS_GTE2
    --IG
    --IG    GTH_RefClk(12)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(13)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(14)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(15)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(16)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(17)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(18)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(19)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(20)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(21)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(22)        <= CXP2_GTH_RefClk;
    --IG    GTH_RefClk(23)        <= CXP2_GTH_RefClk;
    --IG  end generate;
    --IG  g_refclk12: if GBT_NUM >24 generate
    --IG     GTH_RefClk(24)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(25)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(26)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(27)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(28)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(29)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(30)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(31)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(32)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(33)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(34)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(35)        <= CXP2_GTH_RefClk;
    --IG
    --IG      GTH_RefClk(36)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(37)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(38)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(39)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(40)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(41)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(42)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(43)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(44)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(45)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(46)        <= CXP3_GTH_RefClk;
    --IG GTH_RefClk(47)        <= CXP3_GTH_RefClk;
    --IG
    --IG GTH_RefClk(12)        <= CXP4_GTH_RefClk;
    --IG GTH_RefClk(13)        <= CXP4_GTH_RefClk;
    --IG GTH_RefClk(14)        <= CXP4_GTH_RefClk;
    --IG GTH_RefClk(15)        <= CXP4_GTH_RefClk;
    --IG
    --IG GTH_RefClk(16)        <= CXP5_GTH_RefClk;
    --IG GTH_RefClk(17)        <= CXP5_GTH_RefClk;
    --IG GTH_RefClk(18)        <= CXP5_GTH_RefClk;
    --IG GTH_RefClk(19)        <= CXP5_GTH_RefClk;
    --IG GTH_RefClk(20)        <= CXP5_GTH_RefClk;
    --IG GTH_RefClk(21)        <= CXP5_GTH_RefClk;
    --IG GTH_RefClk(22)        <= CXP5_GTH_RefClk;
    --IG GTH_RefClk(23)        <= CXP5_GTH_RefClk;
    --IG
    --IG--bank 224, 225 use clk from bank 225
    --IG    ibufds_instq6_clk0 : IBUFDS_GTE3
    --IG  port map
    --IG  (
    --IG    O               =>     CXP5_GTH_RefClk,
    --IG    ODIV2           =>    open,
    --IG    CEB             =>     '0',
    --IG    I               =>     Q6_CLK0_GTREFCLK_PAD_P_IN,
    --IG    IB              =>     Q6_CLK0_GTREFCLK_PAD_N_IN
    --IG    );
    --IG
    --IG end generate;
    --IG
    --IG end generate;
    --IG
    --IGrefclkgen_v1p5 : if CARD_TYPE=711 generate
    --IG
    --IG
    --IG  GTH_RefClk(12)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(13)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(14)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(15)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(16)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(17)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(18)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(19)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(20)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(21)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(22)        <= CXP2_GTH_RefClk;
    --IG  GTH_RefClk(23)        <= CXP2_GTH_RefClk;
    --IG
    --IG
    --IG  GTH_RefClk(24)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(25)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(26)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(27)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(28)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(29)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(30)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(31)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(32)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(33)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(34)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(35)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(40)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(41)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(42)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(43)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(44)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(45)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(46)        <= CXP3_GTH_RefClk;
    --IG  GTH_RefClk(47)        <= CXP3_GTH_RefClk;
    --IG
    --IG
    --IG  GTH_RefClk(36)        <= CXP4_GTH_RefClk;
    --IG  GTH_RefClk(37)        <= CXP4_GTH_RefClk;
    --IG  GTH_RefClk(38)        <= CXP4_GTH_RefClk;
    --IG  GTH_RefClk(39)        <= CXP4_GTH_RefClk;
    --IG
    --IG   end generate;

    Channel_disable(47 downto 0)          <= register_map_control.GBT_CHANNEL_DISABLE;
    General_ctrl                          <= register_map_control.GBT_GENERAL_CTRL;

    RxSlide_Manual(47 downto 0)           <= register_map_control.GBT_RXSLIDE_MANUAL(47 downto 0);
    RxSlide_Sel(47 downto 0)              <= register_map_control.GBT_RXSLIDE_SELECT(47 downto 0);
    TXUSRRDY(47 downto 0)             <= register_map_control.GBT_TXUSRRDY(47 downto 0);
    RXUSRRDY(47 downto 0)             <= register_map_control.GBT_RXUSRRDY(47 downto 0);
    GTTX_RESET(47 downto 0)           <= register_map_control.GBT_GTTX_RESET(47 downto 0);
    GTRX_RESET(47 downto 0)           <= register_map_control.GBT_GTRX_RESET(47 downto 0);
    SOFT_RESET(47 downto 0)           <= register_map_control.GBT_SOFT_RESET(47 downto 0);
    CPLL_RESET(47 downto 0)           <= register_map_control.GBT_PLL_RESET.CPLL_RESET(47 downto 0);
    QPLL_RESET(11 downto 0)           <= register_map_control.GBT_PLL_RESET.QPLL_RESET(59 downto 48) or QPLL_RESET_LMK;

    SOFT_TXRST_GT(47 downto 0)     <= register_map_control.GBT_SOFT_TX_RESET.RESET_GT;  -- Default: 0b000
    SOFT_RXRST_GT(47 downto 0)     <= register_map_control.GBT_SOFT_RX_RESET.RESET_GT; -- Default: 0b000
    SOFT_TXRST_ALL(11 downto 0)    <= register_map_control.GBT_SOFT_TX_RESET.RESET_ALL(59 downto 48);
    SOFT_RXRST_ALL(11 downto 0)    <= register_map_control.GBT_SOFT_RX_RESET.RESET_ALL(59 downto 48);

    OddEven(47 downto 0)              <= register_map_control.GBT_ODD_EVEN(47 downto 0);
    TopBot(47 downto 0)               <= register_map_control.GBT_TOPBOT(47 downto 0);

    TX_TC_DLY_VALUE(47 downto 0)  <= register_map_control.GBT_TX_TC_DLY_VALUE1;
    TX_TC_DLY_VALUE(95 downto 48) <=register_map_control.GBT_TX_TC_DLY_VALUE2;
    TX_TC_DLY_VALUE(143 downto 96)  <= register_map_control.GBT_TX_TC_DLY_VALUE3;
    TX_TC_DLY_VALUE(191 downto 144) <= register_map_control.GBT_TX_TC_DLY_VALUE4;


    -- TX_OPT(47 downto 0)           <= GBT_TX_OPT(47 DOWNTO 0);  --
    -- RX_OPT(47 downto 0)           <= GBT_RX_OPT(47 DOWNTO 0);  --


    -- GBT_TX_OPT(47 downto 0)               <= x"000000555555"; -- ! TODO:Register was removed in RM4.0 register_map_control.GBT_TX_OPT;
    -- GBT_RX_OPT(47 downto 0)               <= x"000000555555"; -- ! TODO:Register was removed in RM4.0 register_map_control.GBT_RX_OPT;
    DATA_TXFORMAT(47 downto 0)        <= register_map_control.GBT_DATA_TXFORMAT1(47 downto 0);
    DATA_RXFORMAT(47 downto 0)        <= register_map_control.GBT_DATA_RXFORMAT1(47 downto 0);
    DATA_TXFORMAT(95 downto 48)       <= register_map_control.GBT_DATA_TXFORMAT2(47 downto 0);
    DATA_RXFORMAT(95 downto 48)       <= register_map_control.GBT_DATA_RXFORMAT2(47 downto 0);

    TX_RESET(47 downto 0)             <= register_map_control.GBT_TX_RESET(47 downto 0);
    RX_RESET(47 downto 0)             <= register_map_control.GBT_RX_RESET(47 downto 0);
    TX_TC_METHOD(47 downto 0)         <= register_map_control.GBT_TX_TC_METHOD(47 downto 0);
    TC_EDGE(47 downto 0)              <= register_map_control.GBT_TC_EDGE(47 downto 0);
    outsel_i(47 downto 0)             <= register_map_control.GBT_OUTMUX_SEL(47 downto 0);

    register_map_gbt_monitor.GBT_VERSION.DATE             <=  GBT_VERSION(63 downto 48);
    register_map_gbt_monitor.GBT_VERSION.GBT_VERSION(35 downto 32)      <=  GBT_VERSION(23 downto 20);
    register_map_gbt_monitor.GBT_VERSION.GTH_IP_VERSION(19 downto 16)   <=  GBT_VERSION(19 downto 16);
    register_map_gbt_monitor.GBT_VERSION.RESERVED         <=  GBT_VERSION(15 downto 3);
    register_map_gbt_monitor.GBT_VERSION.GTHREFCLK_SEL    <=  (others => GTHREFCLK_SEL);
    register_map_gbt_monitor.GBT_VERSION.RX_CLK_SEL       <=  GBT_VERSION(1 downto 1);
    register_map_gbt_monitor.GBT_VERSION.PLL_SEL          <=  GBT_VERSION(0 downto 0);
    --

    register_map_gbt_monitor.GBT_TXRESET_DONE(47 downto 0)        <= TxResetDone(47 downto 0);
    register_map_gbt_monitor.GBT_RXRESET_DONE(47 downto 0)        <= RxResetDone(47 downto 0);
    register_map_gbt_monitor.GBT_TXFSMRESET_DONE(47 downto 0)     <= txpmaresetdone(47 downto 0);
    register_map_gbt_monitor.GBT_RXFSMRESET_DONE(47 downto 0)     <= rxpmaresetdone(47 downto 0);
    register_map_gbt_monitor.GBT_CPLL_FBCLK_LOST(47 downto 0)     <= CpllFbClkLost (47 downto 0);
    register_map_gbt_monitor.GBT_PLL_LOCK.CPLL_LOCK(47 downto 0)  <= CpllLock(47 downto 0);
    register_map_gbt_monitor.GBT_PLL_LOCK.QPLL_LOCK(59 downto 48) <= QpllLock(11 downto 0);
    register_map_gbt_monitor.GBT_RXCDR_LOCK(47 downto 0)          <= RxCdrLock(47 downto 0);
    register_map_gbt_monitor.GBT_CLK_SAMPLED(47 downto 0)         <= clk_sampled(47 downto 0);

    register_map_gbt_monitor.GBT_RX_IS_HEADER(47 downto 0)        <= RX_IS_HEADER(47 downto 0);
    register_map_gbt_monitor.GBT_RX_IS_DATA(47 downto 0)          <= RX_IS_DATA(47 downto 0);
    register_map_gbt_monitor.GBT_RX_HEADER_FOUND(47 downto 0)     <= RX_HEADER_FOUND(47 downto 0);

    register_map_gbt_monitor.GBT_ALIGNMENT_DONE(47 downto 0)      <= alignment_done_f(47 downto 0);

    --    register_map_monitor.register_map_link_monitor.GBT_ALIGNMENT_DONE(i)            <= lane_monitor(i).gbt.frame_locked        ;
    --    register_map_monitor.register_map_link_monitor.GBT_RX_IS_HEADER(i)                <= lane_monitor(i).gbt.rx_is_header        ;
    --    register_map_monitor.register_map_link_monitor.GBT_RX_HEADER_FOUND(i)              <= lane_monitor(i).gbt.rx_header_found      ;
    --    register_map_monitor.register_map_link_monitor.GBT_ERROR(i)                    <= lane_monitor(i).gbt.error          ;
    --    register_map_monitor.register_map_link_monitor.GBT_TXRESET_DONE(i)                <= lane_monitor(i).gth.txreset_done        ;
    --    register_map_monitor.register_map_link_monitor.GBT_RXRESET_DONE(i)                <= lane_monitor(i).gth.rxreset_done        ;
    --    register_map_monitor.register_map_link_monitor.GBT_TXFSMRESET_DONE(i)              <= lane_monitor(i).gth.txfsmreset_done      ;
    --    register_map_monitor.register_map_link_monitor.GBT_RXFSMRESET_DONE(i)              <= lane_monitor(i).gth.rxfsmreset_done      ;

    -- aligndone_gen : for i in 23 downto 0 generate
    --   alignment_done_f(i) <=  RxCdrLock(i) and alignment_done(i);
    -- end generate;

    register_map_gbt_monitor.GBT_OUT_MUX_STATUS(47 downto 0)    <= outsel_o(47 downto 0);
    register_map_gbt_monitor.GBT_ERROR(47 downto 0)             <= error_f(47 downto 0);

    error_gen : for i in 47 downto 0 generate
        error_f(i) <= error_orig(i) and alignment_done_f(i);
    end generate;

    register_map_gbt_monitor.GBT_GBT_TOPBOT_C(47 downto 0)      <= TopBot_c(47 downto 0);


    ----------------------------------------
    ------ REGISTERS MAPPING
    ----------------------------------------
    alignment_chk_rst_i           <= General_ctrl(0);


    DESMUX_USE_SW                 <= register_map_control.GBT_MODE_CTRL.DESMUX_USE_SW(0);
    RX_ALIGN_SW                   <= register_map_control.GBT_MODE_CTRL.RX_ALIGN_SW(1);
    RX_ALIGN_TB_SW                <= register_map_control.GBT_MODE_CTRL.RX_ALIGN_TB_SW(2);






    -------

    datamod_gen1 : if DYNAMIC_DATA_MODE_EN='1' generate
        DATA_TXFORMAT_i <= DATA_TXFORMAT;
        DATA_RXFORMAT_i <= DATA_RXFORMAT;
    end generate;

    datamod_gen2 : if DYNAMIC_DATA_MODE_EN='0' generate
        DATA_TXFORMAT_i <= GBT_DATA_TXFORMAT_PACKAGE;
        DATA_RXFORMAT_i <= GBT_DATA_RXFORMAT_PACKAGE;
    end generate;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            pulse_lg <= pulse_cnt(20);
            if pulse_cnt(20)='1' then
                pulse_cnt <=(others=>'0');
            else
                pulse_cnt <= pulse_cnt+'1';
            end if;
        end if;
    end process;

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';
        end if;
    end process;

    rxalign_auto : for i in GBT_NUM-1 downto 0 generate

        --  process(clk40_in)
        --  begin
        --    if clk40_in'event and clk40_in='1' then
        --      if pulse_lg = '1' then
        --        gbt_sel(i) <= lock_lg(i);
        --      end if;
        --      if  pulse_lg = '1' then
        --        lock_lg(i) <='1';
        --      elsif alignment_done_f(i)='0' then
        --        lock_lg(i) <='0';
        --      end if;
        --    end if;
        --  end process;

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                if alignment_done_chk_cnt="0000000000000" then
                    alignment_done_a(i) <= RxCdrLock(i) and alignment_done(i);
                else
                    alignment_done_a(i) <= RxCdrLock(i) and alignment_done(i) and alignment_done_a(i);
                end if;
                if alignment_done_chk_cnt="0000000000000" then
                    alignment_done_f(i) <=  RxCdrLock(i) and alignment_done_a(i);
                end if;
            end if;
        end process;


        RX_120b_out(i) <= RX_120b_out_ii(i) when alignment_done_f(i)='1'
                            else (others =>'0');

        auto_rxrst : entity work.FELIX_GBT_RX_AUTO_RST
            port map
      (
                FSM_CLK                 => clk40_in,
                pulse_lg                => pulse_lg,
                GTHRXRESET_DONE         => RxResetDone(i),-- and RxFsmResetDone(i),
                alignment_chk_rst       => alignment_chk_rst_c1(i),
                GBT_LOCK                => alignment_done_f(i),--alignment_done(i),
                AUTO_GTH_RXRST          => auto_gth_rxrst(i),
                ext_trig_realign        => open,--ext_trig_realign(i),
                AUTO_GBT_RXRST          => auto_gbt_rxrst(i)
            );

        rafsm : entity work.FELIX_GBT_RXSLIDE_FSM
            port map
      (
                --ext_trig_realign        => ext_trig_realign(i),
                FSM_RST                 => FSM_RST(i),
                FSM_CLK                 => clk40_in,
                GBT_LOCK                => alignment_done(i),
                RxSlide                 => RxSlide_c(i),
                alignment_chk_rst       => alignment_chk_rst_c(i)
            );

        FSM_RST(i)          <= RX_RESET(i);-- or RX_ALIGN_SW;
        -- GTRX_RESET_i(i)     <= --GTRX_RESET(i) when RX_ALIGN_SW='1' else
        --                      (GTRX_RESET(i) or auto_gth_rxrst(i));
        RX_RESET_i(i)       <= --RX_RESET(i) when RX_ALIGN_SW='1' else
                               (RX_RESET(i) or auto_gbt_rxrst(i));
        alignment_chk_rst(i)        <= --alignment_chk_rst_i when RX_ALIGN_SW='1' else
                                       (alignment_chk_rst_i or alignment_chk_rst_c(i) or alignment_chk_rst_c1(i));
        RxSlide_i(i)             <= RxSlide_c(i) and RxCdrLock(i);
        TX_RESET_i(i)       <= TX_RESET(i) or (not TxResetDone(i));-- or (not TxFsmResetDone(i));
    end generate;

    outsel_ii             <= outsel_o when DESMUX_USE_SW = '0' else
                             outsel_i;

    --  OddEven_i           <= OddEven_c when RX_ALIGN_SW ='0' else
    --                      OddEven;

    --  TopBot_i            <= TopBot_c when RX_ALIGN_SW='0' else --and RX_ALIGN_TB_SW='0'  else
    --                      TopBot;

    --RxSlide_i             <= RxSlide_c;-- when RX_ALIGN_SW='0' else
    --                     RxSlide_Manual;

    RX_FLAG_O             <= RX_FLAG_Oi(GBT_NUM-1 downto 0);

    gbtRxTx : for i in GBT_NUM-1 downto 0 generate
        process(GT_RX_WORD_CLK(i))
        begin
            if GT_RX_WORD_CLK(i)'event and GT_RX_WORD_CLK(i)='1' then
                BITSLIP_MANUAL_r(i)     <= RxSlide_i(i);
                BITSLIP_MANUAL_2r(i)    <= BITSLIP_MANUAL_r(i);
                BITSLIP_MANUAL_3r(i)    <= BITSLIP_MANUAL_2r(i);

                RxSlide(i)              <= BITSLIP_MANUAL_r(i) and (not BITSLIP_MANUAL_2r(i));
            --MT
            --        RxSlide_tmp(i)              <= BITSLIP_MANUAL_r(i) and (not BITSLIP_MANUAL_2r(i));
            end if;
        end process;

        --MT added ******to check wheter RxSlide_tmp is exactly what John had (ie: from
        --gbtrxslide_fsm and then through bitspli manual)*****
        --    RxSlide(i) <= RxSlide_tmp(i) when (RxSlide_Sel(i) = '0') else fc_rx_slide_in(i);
        --


        alignment_chk_rst_f(i)      <= alignment_chk_rst(i);-- or (not RxCdrLock(i));
        gbtTxRx_inst: entity work.gbtTxRx_FELIX
            generic map
      (
                channel => i
            )
            port map
      (
                error_o                 => error_orig(i),
                RX_FLAG                 => RX_FLAG_Oi(i),--RX_FLAG_O(i),
                TX_FLAG                 => TX_FLAG_O(i),

                Tx_DATA_FORMAT          => DATA_TXFORMAT_i(2*i+1 downto 2*i),
                Rx_DATA_FORMAT          => DATA_RXFORMAT_i(2*i+1 downto 2*i),

                --Tx_latopt_tc            => '1',--TX_OPT(i),
                --Tx_latopt_scr           => '1',--TX_OPT(24+i),
                RX_LATOPT_DES           => '1',--RX_OPT(i),

                TX_TC_METHOD            => TX_TC_METHOD(i),
                TC_EDGE                 => TC_EDGE(i),
                TX_TC_DLY_VALUE    => TX_TC_DLY_VALUE(4*i+2 downto 4*i),

                alignment_chk_rst       => alignment_chk_rst_f(i),
                alignment_done_O        => alignment_done(i),
                L40M                    => clk40_in,
                outsel_i                => outsel_ii(i),
                outsel_o                => outsel_o(i),

                --BITSLIP_MANUAL        => RxSlide_i(i),
                --BITSLIP_SEL           => RxSlide_Sel(i),
                --GT_RXSLIDE    => RxSlide(i),
                --OddEven      => '0',--OddEven_i(i),
                --TopBot                  => '0',--TopBot_i(i),
                --data_sel                => data_sel(4*i+3 downto 4*i),

                TX_RESET_I     => TX_RESET_i(i),
                TX_FRAMECLK_I          => TX_FRAME_CLK_I(i),
                TX_WORDCLK_I           => GT_TX_WORD_CLK(i),
                --TX_ISDATA_SEL_I  => TX_IS_DATA(i),
                TX_DATA_120b_I          => TX_120b_in(i),
                TX_DATA_20b_O          => TX_DATA_20b(i),

                RX_RESET_I      => RX_RESET_i(i),
                RX_FRAME_CLK_O     => open,--RX_FRAME_CLK_O(i),
                RX_WORD_IS_HEADER_O     => RX_IS_HEADER(i),
                RX_HEADER_FOUND          => RX_HEADER_FOUND(i),
                --RX_ISDATA_FLAG_O        => RX_IS_DATA(i),
                RX_DATA_20b_I      => RX_DATA_20b(i),
                RX_DATA_120b_O      => RX_120b_out_i(i),
                des_rxusrclk            => GT_RX_WORD_CLK(i),
                RX_WORDCLK_I        => GT_RX_WORD_CLK(i)

            );

        fifo_rst(i) <= rst_hw or (not alignment_done_f(i)) or RX_RESET_i(i) or General_ctrl(4);
        fifo_rden(i) <= not fifo_empty(i);

        fifo_inst: fifo_GBT2CR
            PORT MAP(
                rd_rst          => fifo_rst(i),--rst_hw,
                wr_rst          => fifo_rst(i),--rst_hw,
                wr_clk          => GT_RX_WORD_CLK(i),
                rd_clk          => clk40_in,--FIFO_RD_CLK(i),
                din             => RX_120b_out_i(i),
                wr_en           => RX_FLAG_Oi(i),
                rd_en           => fifo_rden(i),--not fifo_empty(i),--'1',--FIFO_RD_EN(i),
                dout            => RX_120b_out_ii(i),
                full            => open,
                empty           => open,
                prog_empty      => fifo_empty(i)--FIFO_EMPTY(i)
            );

    end generate;

    --  ila_TX_120b_in <= TX_120b_in;
    --  ila_GT_TX_WORD_CLK <= GT_TX_WORD_CLK;
    --  ila_cplllock <= cplllock; --48
    --  ila_qplllock <= qplllock; --12

    --  ila_gen : for i in 0 to 1 generate
    --    ila_one_link : ila_link_frame
    --      port map(
    --        clk => ila_GT_TX_WORD_CLK(i),
    --        probe0 => ila_TX_120b_in(i),
    --        probe1 => ila_cplllock,
    --        probe2 => ila_qplllock
    --      );
    --  end generate ila_gen;

    -------------------------------
    ------ GTH TOP WRAPPER
    -------------------------------

    clk_generate : for i in GBT_NUM-1 downto 0 generate

        GTTXOUTCLK_BUFG: bufg_gt
            port map(
                i       => GT_TXOUTCLK(i),
                div     => "000",
                clr     => '0',--userclk_tx_reset_in,--'0',
                cemask  => '0',
                clrmask => '0',
                ce      => '1',
                o       => GT_TX_WORD_CLK(i)
            );

        GT_TXUSRCLK(i) <= GT_TX_WORD_CLK(i);

        GTRXOUTCLK_BUFG: bufg_gt
            port map(
                i       => GT_RXOUTCLK(i),
                div     => "000",
                clr     => '0',--userclk_tx_reset_in,--'0',
                cemask  => '0',
                clrmask => '0',
                ce      => '1',
                o       => GT_RX_WORD_CLK(i) --changed MT/SS
            );

        -- GT_RXUSRCLK(i) <=  clk240_in;


        GT_RXUSRCLK(i) <= GT_RX_WORD_CLK(i); -- changed MT/SS
    --RXUSRCLK_OUT(i)   <= GT_RXUSRCLK(i);

    end generate;

    --MT SIMU+
    rxcdrlock_sim : if sim_emulator = true generate
        rxcdrlock_gen : for i in GBT_NUM-1 downto 0 generate
            RxCdrLock(i) <= '1'; --hard coding to 1 since MGT has been commented out
        end generate;
    end generate; --rxcdrlock_sim
    --

    -- QPLL Reset Process----- added MT/SS
    qpllreset_notsim : if sim_emulator = false generate
        QpllLock_inv <= not QpllLock;

        process(clk40_in)
        begin
            if clk40_in'event and clk40_in='1' then
                QPLL_PIPE(3 downto 1) <= QPLL_PIPE(2 downto 0);
                QPLL_PIPE(0) <= LMK_LD;
                if ((QPLL_PIPE(3) = '0') AND (QPLL_PIPE(0) = '1')) then --rising edge
                    QPLL_RESET_LMK  <= QpllLock_inv;
                else
                    QPLL_RESET_LMK  <=x"000";
                end if;
            end if;
        end process;

        --- LMK Reset Process----- added MT/SS
        alignment_done_inv <= not alignment_done_f(0);

        process(clk40_in)
        begin
            if (clk40_in'event and clk40_in='1') then  --rising_edge(clk40_in)
                LMK_PIPE(3 downto 1) <= LMK_PIPE(2 downto 0);
                LMK_PIPE(0) <= alignment_done_f(0);
                if ((LMK_PIPE(3) = '0') AND (LMK_PIPE(0) = '1')) then --rising edge
                    LMK_RESET  <= '1';
                else
                    LMK_RESET  <= '0';
                end if;
            end if;
        end process;
        RESET_TO_LMK <= LMK_RESET; -- added MT/SS
    end generate; --qpllreset_notsim

    qpllgen_sim : if sim_emulator = true generate
        RX_DATA_20b <= TX_DATA_20b; --data looped back
    end generate; --qpllgen_sim

    qpllgen_notsim : if sim_emulator = false generate
        drpclk_in(0) <= clk40_in;

        QPLL_GEN: if PLL_SEL = QPLL generate

            port_trans : for i in GBT_NUM-1 downto 0 generate
                RX_N_i(i)   <= RX_N(i);
                RX_P_i(i)   <= RX_P(i);
                TX_N(i)     <= TX_N_i(i);
                TX_P(i)     <= TX_P_i(i);

            end generate;

            GTH_inst : for i in (GBT_NUM-1)/4 downto 0 generate

                RX_DATA_20b(4*i+0) <= RX_DATA_80b(i)(19 downto 0);
                RX_DATA_20b(4*i+1) <= RX_DATA_80b(i)(39 downto 20);
                RX_DATA_20b(4*i+2) <= RX_DATA_80b(i)(59 downto 40);
                RX_DATA_20b(4*i+3) <= RX_DATA_80b(i)(79 downto 60);


                TX_DATA_80b(i) <= TX_DATA_20b(4*i+3) & TX_DATA_20b(4*i+2) & TX_DATA_20b(4*i+1) & TX_DATA_20b(4*i+0);

                --REFCLK_SEL : if (i > 0) generate

                GTH_TOP_INST: entity work.GTH_QPLL_Wrapper_FELIG
                    Port map(
                        gthrxn_in                       => RX_N_i(4*i+3 downto 4*i),
                        gthrxp_in                       => RX_P_i(4*i+3 downto 4*i),
                        gthtxn_out                      => TX_N_i(4*i+3 downto 4*i),
                        gthtxp_out                      => TX_P_i(4*i+3 downto 4*i),

                        drpclk_in                       => drpclk_in,--(others=>clk40_in),
                        gtrefclk0_in                    => GTH_RefClk(4*i downto 4*i), -- for RX REFCLK from Si
                        gtrefclk1_in                    => GTH_RefClk_LMK(4*i downto 4*i), --added for TX MGTREFCLK from LMK--MT/SS
                        gt_rxusrclk_in                  => GT_RX_WORD_CLK(4*i+3 downto 4*i),
                        gt_rxoutclk_out                 => GT_RXOUTCLK(4*i+3 downto 4*i),
                        gt_txusrclk_in                  => GT_TX_WORD_CLK(4*i+3 downto 4*i),
                        gt_txoutclk_out                 => GT_TXOUTCLK(4*i+3 downto 4*i),

                        userdata_tx_in                  =>  TX_DATA_80b(i),
                        userdata_rx_out                 =>  RX_DATA_80b(i),
                        rxpolarity_in                   => register_map_control.GBT_RXPOLARITY(4*i+3 downto 4*i),
                        txpolarity_in                   => register_map_control.GBT_TXPOLARITY(4*i+3 downto 4*i),


                        -- for loopback: default, both signal need to be all '0'
                        -- read kcu gth manual for the details. NOTE: the TXBUFFER is disabled, so some type of loopbhack may be
                        -- not supported.
                        -- for loopback rxcdrhold needs to be set, a register needs to be added, check KCU GTH manual for details
                        -- not tested yet
                        loopback_in                     => register_map_control.GTH_LOOPBACK_CONTROL,
                        rxcdrhold_in                    => '0',

                        userclk_rx_reset_in             => userclk_rx_reset_in(i downto i),--(others=>(not rxpmaresetdone_out(i))),--locked,
                        userclk_tx_reset_in             => userclk_tx_reset_in(i downto i),--(others=>(not txpmaresetdone_out(i))),--,--locked,

                        -- reset_clk_freerun_in                    : in std_logic_vector(0 downto 0);
                        reset_all_in                           => SOFT_RESET_f(i downto i),
                        reset_tx_pll_and_datapath_in           => QPLL_RESET(i downto i),
                        reset_tx_datapath_in                   => GTTX_RESET_MERGE(i downto i),
                        reset_rx_pll_and_datapath_in           => CPLL_RESET(i downto i), -- changed MT/SS
                        reset_rx_datapath_in                   => GTRX_RESET_MERGE(i downto i),

                        qpll0lock_out                          => open,
                        qpll1lock_out                          => QpllLock(i downto i),
                        qpll1fbclklost_out                     => open,--
                        qpll0fbclklost_out                     => open,
                        rxslide_in                             => RxSlide(4*i+3 downto 4*i),

                        cplllock_out                           => cplllock(4*i+3 downto 4*i), --RL

                        rxresetdone_out                         => rxresetdone(4*i+3 downto 4*i),
                        txresetdone_out                         => txresetdone(4*i+3 downto 4*i),
                        rxpmaresetdone_out                      => rxpmaresetdone(4*i+3 downto 4*i),
                        txpmaresetdone_out                      => txpmaresetdone(4*i+3 downto 4*i),
                        reset_tx_done_out                       => txresetdone_quad(i downto i),
                        reset_rx_done_out                       => rxresetdone_quad(i downto i),
                        reset_rx_cdr_stable_out                 => RxCdrLock_quad(i downto i),
                        rxcdrlock_out                           => rxcdrlock_out(4*i+3 downto 4*i)
                    );
                --end generate;

                --this was done to test the firmware with fibers connected in loopback. In this case the quad 0 TX  gets recovered clock from Si and all other
                --  quads TX get revovered clock from LMK.

                --  REFCLK_SEL_1 : if (i = 0) generate
                --    GTH_TOP_INST_1: entity work.GTH_QPLL_Wrapper_FELIG
                --      Port map(
                --        gthrxn_in                       => RX_N_i(4*i+3 downto 4*i),
                --        gthrxp_in                       => RX_P_i(4*i+3 downto 4*i),
                --        gthtxn_out                      => TX_N_i(4*i+3 downto 4*i),
                --        gthtxp_out                      => TX_P_i(4*i+3 downto 4*i),

                --        drpclk_in                       => drpclk_in,--(others=>clk40_in),
                --        gtrefclk0_in                    => GTH_RefClk(4*i downto 4*i), -- for RX REFCLK from Si --MT/SS
                --        gtrefclk1_in                    => GTH_RefClk(4*i downto 4*i), --added for TX MGTREFCLK from Si-MT/SS
                --        gt_rxusrclk_in                  => GT_RX_WORD_CLK(4*i+3 downto 4*i),
                --        gt_rxoutclk_out                 => GT_RXOUTCLK(4*i+3 downto 4*i),
                --        gt_txusrclk_in                  => GT_TX_WORD_CLK(4*i+3 downto 4*i),
                --        gt_txoutclk_out                 => GT_TXOUTCLK(4*i+3 downto 4*i),

                --        userdata_tx_in                  =>  TX_DATA_80b(i),
                --        userdata_rx_out                 =>  RX_DATA_80b(i),
                --        rxpolarity_in                   => register_map_control.GBT_RXPOLARITY(4*i+3 downto 4*i),
                --        txpolarity_in                   => register_map_control.GBT_TXPOLARITY(4*i+3 downto 4*i),


                --        -- for loopback: default, both signal need to be all '0'
                --        -- read kcu gth manual for the details. NOTE: the TXBUFFER is disabled, so some type of loopbhack may be
                --        -- not supported.
                --        -- for loopback rxcdrhold needs to be set, a register needs to be added, check KCU GTH manual for details
                --        -- not tested yet
                --        loopback_in                     => register_map_control.GTH_LOOPBACK_CONTROL,
                --        rxcdrhold_in                    => '0',

                --        userclk_rx_reset_in             => userclk_rx_reset_in(i downto i),--(others=>(not rxpmaresetdone_out(i))),--locked,
                --        userclk_tx_reset_in             => userclk_tx_reset_in(i downto i),--(others=>(not txpmaresetdone_out(i))),--,--locked,

                --        -- reset_clk_freerun_in                    : in std_logic_vector(0 downto 0);
                --        reset_all_in                           => SOFT_RESET_f(i downto i),
                --        reset_tx_pll_and_datapath_in           => QPLL_RESET(i downto i),
                --        reset_tx_datapath_in                   => GTTX_RESET_MERGE(i downto i),
                --        reset_rx_pll_and_datapath_in           => CPLL_RESET(i downto i),
                --        reset_rx_datapath_in                   => GTRX_RESET_MERGE(i downto i),

                --        qpll0lock_out                          => open,
                --        qpll1lock_out                          => QpllLock(i downto i),
                --        qpll1fbclklost_out                     => open,--
                --        qpll0fbclklost_out                     => open,
                --        rxslide_in                             => RxSlide(4*i+3 downto 4*i),

                --        rxresetdone_out                         => rxresetdone(4*i+3 downto 4*i),
                --        txresetdone_out                         => txresetdone(4*i+3 downto 4*i),
                --        rxpmaresetdone_out                      => rxpmaresetdone(4*i+3 downto 4*i),
                --        txpmaresetdone_out                      => txpmaresetdone(4*i+3 downto 4*i),
                --        reset_tx_done_out                       => txresetdone_quad(i downto i),
                --        reset_rx_done_out                       => rxresetdone_quad(i downto i),
                --        reset_rx_cdr_stable_out                 => RxCdrLock_quad(i downto i),
                --        rxcdrlock_out                           => rxcdrlock_out(4*i+3 downto 4*i)
                --        );
                --  end generate;


                process(clk40_in)
                begin
                    if clk40_in'event and clk40_in='1' then
                        if cdr_cnt ="00000000000000000000" then
                            RxCdrLock_a(4*i)     <= rxcdrlock_out(4*i);
                            RxCdrLock_a(4*i+1)   <= rxcdrlock_out(4*i+1);
                            RxCdrLock_a(4*i+2)   <= rxcdrlock_out(4*i+2);
                            RxCdrLock_a(4*i+3)   <= rxcdrlock_out(4*i+3);
                        else
                            RxCdrLock_a(4*i) <= RxCdrLock_a(4*i) and rxcdrlock_out(4*i);
                            RxCdrLock_a(4*i+1) <= RxCdrLock_a(4*i+1) and rxcdrlock_out(4*i+1);
                            RxCdrLock_a(4*i+2) <= RxCdrLock_a(4*i+2) and rxcdrlock_out(4*i+2);
                            RxCdrLock_a(4*i+3) <= RxCdrLock_a(4*i+3) and rxcdrlock_out(4*i+3);
                        end if;
                        if cdr_cnt="00000000000000000000" then
                            RxCdrLock_int(4*i) <=RxCdrLock_a(4*i);
                            RxCdrLock_int(4*i+1) <=RxCdrLock_a(4*i+1);
                            RxCdrLock_int(4*i+2) <=RxCdrLock_a(4*i+2);
                            RxCdrLock_int(4*i+3) <=RxCdrLock_a(4*i+3);
                        end if;
                    end if;
                end process;
                RxCdrLock(4*i) <= (not Channel_disable(4*i)) and RxCdrLock_int(4*i);
                RxCdrLock(4*i+1) <= (not Channel_disable(4*i+1)) and RxCdrLock_int(4*i+1);
                RxCdrLock(4*i+2) <= (not Channel_disable(4*i+2)) and RxCdrLock_int(4*i+2);
                RxCdrLock(4*i+3) <= (not Channel_disable(4*i+3)) and RxCdrLock_int(4*i+3);

                SOFT_RESET_f(i) <= SOFT_RESET(i) or QPLL_RESET(i);--or rst_hw;-- or GTRX_RESET(i);

                userclk_rx_reset_in(i) <=not (rxpmaresetdone(4*i+0) or rxpmaresetdone(4*i+1) or rxpmaresetdone(4*i+2) or rxpmaresetdone(4*i+3));
                userclk_tx_reset_in(i) <=not (txpmaresetdone(4*i+0) or txpmaresetdone(4*i+1) or txpmaresetdone(4*i+2) or txpmaresetdone(4*i+3));

                GTTX_RESET_MERGE(i) <= GTTX_RESET(4*i) or GTTX_RESET(4*i+1) or GTTX_RESET(4*i+2) or GTTX_RESET(4*i+3);
                GTRX_RESET_MERGE(i) <= (GTRX_RESET(4*i) or (auto_gth_rxrst(4*i) and RxCdrLock(4*i)))
                                       or (GTRX_RESET(4*i+1) or (auto_gth_rxrst(4*i+1) and RxCdrLock(4*i+1)))
                                       or (GTRX_RESET(4*i+2) or (auto_gth_rxrst(4*i+2) and RxCdrLock(4*i+2)))
                                       or (GTRX_RESET(4*i+3) or (auto_gth_rxrst(4*i+3) and RxCdrLock(4*i+3))) ;
            --GTRX_RESET_MERGE(i) <= GTRX_RESET(4*i) or GTRX_RESET(4*i+1) or GTRX_RESET(4*i+2) or GTRX_RESET(4*i+3);

            --CpllLock(i) <= '1';

            end generate;--GTH_TOP_INST

        end generate; --QPLL_GEN
    end generate; --qpllgen_notsim

    process(clk40_in)
    begin
        if clk40_in'event and clk40_in='1' then
            cdr_cnt <=cdr_cnt+'1';
        end if;
    end process;


    cpllgen_notsim : if sim_emulator = false generate
        CPLL_GEN: if  PLL_SEL = CPLL generate
            GTH_inst : for i in GBT_NUM-1 downto 0 generate

                GTH_TOP_INST: entity work.GTH_CPLL_Wrapper
                    Port map(
                        gthrxn_in                              => RX_N(i downto i),
                        gthrxp_in                              => RX_P(i downto i),
                        gthtxn_out                             => TX_N(i downto i),
                        gthtxp_out                             => TX_P(i downto i),
                        drpclk_in                              => drpclk_in,--(others=>clk40_in),
                        gtrefclk0_in                           => GTH_RefClk(i downto i),

                        gt0_rxusrclk_in                        => GT_RX_WORD_CLK(i downto i),
                        gt0_rxoutclk_out                       => GT_RXOUTCLK(i downto i),
                        gt0_txusrclk_in                        => GT_TX_WORD_CLK(i downto i),
                        gt0_txoutclk_out                       => GT_TXOUTCLK(i downto i),

                        userdata_tx_in                         =>  TX_DATA_20b(i),
                        userdata_rx_out                        =>  RX_DATA_20b(i),
                        rxpolarity_in                          => register_map_control.GBT_RXPOLARITY(i downto i),
                        txpolarity_in                          => register_map_control.GBT_TXPOLARITY(i downto i),

                        -- for loopback: default, both signal need to be all '0'
                        -- read kcu gth manual for the details. NOTE: the TXBUFFER is disabled, so some type of loopbhack may be
                        -- not supported.
                        -- for loopback rxcdrhold needs to be set, a register needs to be added, check KCU GTH manual for details
                        -- not tested yet
                        loopback_in                            => register_map_control.GTH_LOOPBACK_CONTROL,
                        rxcdrhold_in                           => '0',


                        userclk_rx_reset_in                    => userclk_rx_reset_in(i downto i),--(others=>(not rxpmaresetdone_out(i))),--locked,
                        userclk_tx_reset_in                    => userclk_tx_reset_in(i downto i),--(others=>(not txpmaresetdone_out(i))),--,--locked,

                        -- reset_clk_freerun_in                    : in std_logic_vector(0 downto 0);
                        reset_all_in                           => SOFT_RESET_f(i downto i),
                        reset_tx_pll_and_datapath_in           => CPLL_RESET(i downto i),
                        reset_tx_datapath_in                   => GTTX_RESET(i downto i),
                        reset_rx_pll_and_datapath_in           => CPLL_RESET(i downto i),
                        reset_rx_datapath_in                   => GTRX_RESET_i(i downto i),-- and RxCdrLock(i downto i),


                        cpllfbclklost_out                      => cpllfbclklost(i downto i),
                        cplllock_out                           => cplllock(i downto i),


                        rxslide_in                             => RxSlide(i downto i),



                        rxpmaresetdone_out                     => rxpmaresetdone(i downto i),
                        txpmaresetdone_out                     => txpmaresetdone(i downto i),

                        reset_tx_done_out                      => txresetdone(i downto i),
                        reset_rx_done_out                      => rxresetdone(i downto i),
                        reset_rx_cdr_stable_out                => RxCdrLock_int(i downto i)

                    );


                RxCdrLock(i) <= (not Channel_disable(i)) and RxCdrLock_int(i);
                GTRX_RESET_i(i) <= --GTRX_RESET(i) when RX_ALIGN_SW='1' else
                                   GTRX_RESET(i) or (auto_gth_rxrst(i) and RxCdrLock(i));

                SOFT_RESET_f(i) <= SOFT_RESET(i/4) or CPLL_RESET(i);--or rst_hw; -- or GTRX_RESET(i);

                userclk_rx_reset_in(i) <=not rxpmaresetdone(i);
                userclk_tx_reset_in(i) <=not txpmaresetdone(i);
            --RxResetDone_f(i) <= RxResetDone(i);

            end generate; --GTH_TOP_INST
        end generate; --CPLL_GEN
    end generate; --cpllgen_notsim

    --MT externalizing gtrx/txusrclk for FELIG logic
    GT_TXUSRCLK_OUT <= GT_TXUSRCLK(GBT_NUM-1 downto 0);
    GT_RXUSRCLK_OUT <= GT_RXUSRCLK(GBT_NUM-1 downto 0);
    --


    bufgceobuf_notsim : if sim_emulator = false generate
        -- BUFGCE_DIV instantiation: added MT/SS
        BUFGCE_DIV_inst : BUFGCE_DIV
            generic map (
                BUFGCE_DIVIDE => 6, -- 1-8 -divide by 6 to get 40 MHz
                -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
                IS_CE_INVERTED => '0', -- Optional inversion for CE
                IS_CLR_INVERTED => '0', -- Optional inversion for CLR
                IS_I_INVERTED => '0' -- Optional inversion for I
            )
            port map (
                O => GT_RXUSRCLK_40MHz, -- 1-bit output: Buffer
                CE => '1', -- 1-bit input: Buffer enable
                CLR => '0', -- 1-bit input: Asynchronous clear
                I => GT_RX_WORD_CLK(0) -- 1-bit input: Buffer 240 MHz RXUSER_CLK after BUFG_GT
            );
        -- End of BUFGCE_DIV_inst instantiation

        --OBUFDS to route the 240 MHz RXUSER CLK into the LMK03200 jitter cleaner  to create the TXREFCLK --MT/SS
        OBUF240_LMK03200: OBUFDS
            generic map (
                IOSTANDARD => "LVDS",
                SLEW       => "FAST")
            port map(
                I => GT_RXUSRCLK_40MHz,
                O => CLK40_FPGA2LMK_out_P,
                OB => CLK40_FPGA2LMK_out_N);
    end generate; --bufgceobuf_notsim
end Behavioral;
