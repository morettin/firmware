--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen      (kchen@bnl.gov)
--! modified:    Marco Trovato (mtrovato@anl.gov)
--!              Ricardo Luz   (rluz@anl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2019/05/21 04:43:14 PM
-- Design Name: FELIX LpGBT Wrapper
-- Module Name: FELIX LpGBT Wrapper - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX LpGBT adapted for FELIG . Left only FE
--              Wrapper
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
    use UNISIM.VComponents.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;

entity FELIX_LpGBT_Wrapper_FELIG is
    Generic (
        STABLE_CLOCK_PERIOD         : integer   := 24;
        -- period of the drp_clock
        GBT_NUM                     : integer   := 24;
        PRBS_TEST_EN                : integer := 1;
        GTHREFCLK_SEL               : std_logic := '0';
        -- GREFCLK              : std_logic := '1';
        -- MGTREFCLK            : std_logic := '0';
        CARD_TYPE                   : integer   := 712;
        --        FE_EMU_EN                   : integer   := 1;    --MT commented
        CLK_CHIP_SEL                : integer   := 1;
        -- SI5345               : integer   := 0;
        -- LMK03200             : integer   := 1;
        PLL_SEL                     : std_logic := '0';
        -- CPLL : '0'
        -- QPLL : '1'
        --MT SIMU+
        sim_emulator            : boolean       := false
    --


    );
    Port (
        rst_hw                      : in std_logic;
        rxrecclk40m_out             : out std_logic;
        register_map_control        : in register_map_control_type;
        register_map_link_monitor    : out register_map_link_monitor_type;

        CLK40_IN                    : in std_logic;
        clk320_in                   : in std_logic; --MT SIMU+
        GREFCLK_IN                  : in std_logic;
        RX320_CH0                   : out std_logic;

        GTREFCLK_P_s                : in std_logic_vector(4 downto 0);
        GTREFCLK_N_s                : in std_logic_vector(4 downto 0);
        LMK_P                       : in std_logic_vector(7 downto 0);
        LMK_N                       : in std_logic_vector(7 downto 0);

        RX_LINK_LCK                 : out std_logic_vector(GBT_NUM-1 downto 0);
        --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);

        --IC (2b) & EC (2b) & D (224b)
        --TX_256b_in                  : in  txrx256b_type(0 to GBT_NUM-1); --MT txrx120b_type(0 to GBT_NUM-1);
        TX_2b_IC_in                : in txrx2b_type(0 to GBT_NUM-1); --RL
        TX_2b_EC_in                : in txrx2b_type(0 to GBT_NUM-1); --RL
        TX_224b_DATA_in            : in txrx224b_type(0 to GBT_NUM-1); --RL
        --IC (2b) & EC (2b) & D (32b)
        --RX_36b_out                 : out txrx36b_type(0 to GBT_NUM-1); --MT txrx120b_type(0 to GBT_NUM-1);
        RX_2b_IC_out                : out txrx2b_type(0 to GBT_NUM-1); --RL
        RX_2b_EC_out                : out txrx2b_type(0 to GBT_NUM-1); --RL
        RX_32b_DATA_out             : out txrx32b_type(0 to GBT_NUM-1); --RL
        --MT externalizing gtrx/txusrclk for FELIG logic
        --
        GT_TXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);
        GT_RXUSRCLK_OUT             : out std_logic_vector(GBT_NUM-1 downto 0);
        --MT 40 MHZ rxusr clk to drive LMK
        clk40_rxusrclk_out          : out std_logic;

        TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
        TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
        RX_P                        : in  std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
        RX_N                        : in  std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
        --MT for FELIG from mgt_framealigner . 1 pulse every c_wordRatio (=8)
        sta_headerFlag_out          : out std_logic_vector(GBT_NUM-1 downto 0);
        --MT added: needed by FELIG to latch tx payload to generated data. Need to
        --be synchronized with emulator logic
        tx_flag_out                 : out std_logic_vector(GBT_NUM-1 downto 0);
        --MT added: alignment flag needed by the emulator
        alignment_done_out          : out std_logic_vector(GBT_NUM-1 downto 0);
        --MT added
        RESET_TO_LMK                : out  std_logic;
        --MT LMK lock signal
        LMK_LD                      : in  std_logic;
        --
        LINKSconfig                 : in std_logic_vector(2 downto 0);
        -- LMK03200 clock input pins added --MT/SS
        CLK40_FPGA2LMK_out_P         : out std_logic;
        CLK40_FPGA2LMK_out_N         : out std_logic
    );
end FELIX_LpGBT_Wrapper_FELIG;


architecture Behavioral of FELIX_LpGBT_Wrapper_FELIG is

    --signal FE_SIDE_RX40MCLK         : std_logic; --MT commented
    signal FELIX_SIDE_RX40MCLK      : std_logic;
    signal PRBS_ERR_CLR             : std_logic;

    signal GTH_REFCLK_LMK_OUT           : std_logic_vector(GBT_NUM-1 downto 0); --added
    signal GTH_REFCLK_OUT           : std_logic_vector(GBT_NUM-1 downto 0);
    --signal GTH_EMU_REFCLK_OUT       : std_logic_vector(GBT_NUM-1 downto 0);
    ----MT commented

    signal CTRL_TXPOLARITY           : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal CTRL_RXPOLARITY           : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal CTRL_GBTTXRST             : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal CTRL_GBTRXRST             : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal CTRL_DATARATE             : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal CTRL_FECMODE              : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal CTRL_CHANNEL_DISABLE      : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal MON_RXRSTDONE             : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal MON_TXRSTDONE             : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal MON_RXPMARSTDONE          : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal MON_TXPMARSTDONE          : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal MON_RXCDR_LCK             : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal MON_CPLL_LCK              : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal MON_ALIGNMENT_DONE        : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal MON_LPGBT_ERRFLG          : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);

    signal MON_RXRSTDONE_QUAD        : std_logic_vector(GBT_NUM/4-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)/4-1 downto 0);
    signal MON_TXRSTDONE_QUAD        : std_logic_vector(GBT_NUM/4-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)/4-1 downto 0);
    signal MON_RXCDR_LCK_QUAD        : std_logic_vector(GBT_NUM/4-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)/4-1 downto 0);
    signal MON_QPLL_LCK              : std_logic_vector(GBT_NUM/4-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)/4-1 downto 0);


    signal CTRL_SOFT_RESET           : std_logic_vector(GBT_NUM/4-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)/4-1 downto 0);
    signal CTRL_TXPLL_DATAPATH_RESET : std_logic_vector(GBT_NUM/4-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)/4-1 downto 0);
    signal CTRL_RXPLL_DATAPATH_RESET : std_logic_vector(GBT_NUM/4-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)/4-1 downto 0);
    signal CTRL_TX_DATAPATH_RESET    : std_logic_vector(GBT_NUM/4-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)/4-1 downto 0);
    signal CTRL_RX_DATAPATH_RESET    : std_logic_vector(GBT_NUM/4-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)/4-1 downto 0);

    signal CTRL_GBT_General_ctrl    : std_logic_vector(63 downto 0);
    signal MON_FEC_ERROR            : std_logic_vector(GBT_NUM-1 downto 0);

    --MT commented
    --signal FELIX_DOWNLINK_USER_DATA    : txrx32b_type(GBT_NUM-1 downto 0);
    --signal FELIX_DOWNLINK_IC_DATA    : txrx2b_type(GBT_NUM-1 downto 0);
    --signal FELIX_DOWNLINK_EC_DATA    : txrx2b_type(GBT_NUM-1 downto 0);
    --signal FELIX_UPLINK_USER_DATA      : txrx230b_type(GBT_NUM-1 downto 0);
    --signal FELIX_UPLINK_EC_DATA        : txrx2b_type(GBT_NUM-1 downto 0);
    --signal FELIX_UPLINK_IC_DATA        : txrx2b_type(GBT_NUM-1 downto 0);
    --


    signal FE_DOWNLINK_USER_DATA    : txrx32b_type(GBT_NUM-1 downto 0);
    signal FE_DOWNLINK_IC_DATA    : txrx2b_type(GBT_NUM-1 downto 0);
    signal FE_DOWNLINK_EC_DATA    : txrx2b_type(GBT_NUM-1 downto 0);
    signal FE_UPLINK_USER_DATA      : txrx224b_type(GBT_NUM-1 downto 0);
    signal FE_UPLINK_EC_DATA        : txrx2b_type(GBT_NUM-1 downto 0);
    signal FE_UPLINK_IC_DATA        : txrx2b_type(GBT_NUM-1 downto 0);


    signal PRBS_CH_SEL       : std_logic_vector(7 downto 0);

    --MT added
    signal sta_headerFlag_i : std_logic_vector(GBT_NUM-1 downto 0);
    signal LMK_PIPE               : std_logic_vector(3 downto 0);
    signal LMK_RESET              : std_logic;
    signal alignment_done_inv     : std_logic; --added SS

    --
    --RL
    signal DATARATE             : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);
    signal FECMODE              : std_logic_vector(GBT_NUM-1 downto 0); --MT std_logic_vector(GBT_NUM*(FE_EMU_EN+1)-1 downto 0);


    --RL

    signal GT_RXUSRCLK_40MHz    : std_logic;
    signal RxCdrLock_o          : std_logic_vector(GBT_NUM-1 downto 0);
    signal alignment_done_o     : std_logic_vector(GBT_NUM-1 downto 0);

    signal alignment_done_o_40rec   : std_logic;
    signal sta_headerFlag_i_40rec   : std_logic;
    signal RxCdrLock_o_40rec        : std_logic;

    type STATEM  is (st_idle, st_wait_for_aligment, st_LMK_reseted);
    signal state            : STATEM := st_idle;
    signal LMK_RESET_b      : std_logic;

    --RL this are lpgbt BE counters, not implemented in FE. signals are initiated to comply with Regs_RW inputs
    signal MON_FEC_ERR_CNT          : array_32b(0 to GBT_NUM-1) := (others => (others => '0'));
    signal MON_AUTO_RX_RESET_CNT    : array_32b(0 to GBT_NUM-1) := (others => (others => '0'));

--COMPONENT ila_SM_LMK IS
--PORT (
--    clk : IN STD_LOGIC;
--    probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe4 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--    probe6 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
--);
--END COMPONENT;

--signal ila_st_idle                    : std_logic;
--signal ila_st_wait_for_aligment       : std_logic;
--signal ila_st_LMK_reseted             : std_logic;
--signal ila_ila_st_idle                : std_logic;
--signal ila_ila_st_wait_for_aligment   : std_logic;
--signal ila_ila_st_LMK_reseted         : std_logic;
--signal ila_RxCdrLock_o                : std_logic;
--signal ila_sta_headerFlag_i           : std_logic;
--signal ila_LMK_RESET_b                : std_logic;
--signal ila_alignment_done_o           : std_logic;

begin

    --MT commented: data to/from _BE_ or PRBS. Not needed. RL: removed commented lines

    refclk_notsim : if sim_emulator = false generate
        Reference_Clk_Gen_SI5345 : entity work.RefClk_Gen
            Generic Map(
                GBT_NUM => GBT_NUM,
                CARD_TYPE                   => CARD_TYPE,
                CLK_CHIP_SEL                => 0 -- SI5345
            )
            Port Map(
                --GREFCLK_IN                  => GREFCLK_IN, --not used

                SI53XX_0_P                  => GTREFCLK_P_s(0),
                SI53XX_0_N                  => GTREFCLK_N_s(0),
                SI53XX_2_P                  => GTREFCLK_P_s(1),
                SI53XX_2_N                  => GTREFCLK_N_s(1),
                SI53XX_4_P                  => GTREFCLK_P_s(3),
                SI53XX_4_N                  => GTREFCLK_N_s(3),
                SI53XX_5_P                  => GTREFCLK_P_s(2),
                SI53XX_5_N                  => GTREFCLK_N_s(2),
                SI53XX_8_P                  => GTREFCLK_P_s(4),
                SI53XX_8_N                  => GTREFCLK_N_s(4),

                LMK0_P                      => LMK_P(0),
                LMK0_N                      => LMK_N(0),
                LMK1_P                      => LMK_P(1),
                LMK1_N                      => LMK_N(1),
                LMK2_P                      => LMK_P(2),
                LMK2_N                      => LMK_N(2),
                LMK3_P                      => LMK_P(3),
                LMK3_N                      => LMK_N(3),
                LMK4_P                      => LMK_P(4),
                LMK4_N                      => LMK_N(4),
                LMK5_P                      => LMK_P(5),
                LMK5_N                      => LMK_N(5),
                LMK6_P                      => LMK_P(6),
                LMK6_N                      => LMK_N(6),
                LMK7_P                      => LMK_P(7),
                LMK7_N                      => LMK_N(7),

                GTREFCLK_P_IN               => (others => '0'),
                GTREFCLK_N_IN               => (others => '0'),

                GTH_REFCLK_OUT              => GTH_REFCLK_OUT
            );

        Reference_Clk_Gen_LMK03200 : entity work.RefClk_Gen
            Generic Map(
                GBT_NUM => GBT_NUM,
                CARD_TYPE                   => CARD_TYPE,
                CLK_CHIP_SEL                => 1 -- LMK03200
            )
            Port Map(
                --GREFCLK_IN                  => GREFCLK_IN, --not used

                SI53XX_0_P                  => GTREFCLK_P_s(0),
                SI53XX_0_N                  => GTREFCLK_N_s(0),
                SI53XX_2_P                  => GTREFCLK_P_s(1),
                SI53XX_2_N                  => GTREFCLK_N_s(1),
                SI53XX_4_P                  => GTREFCLK_P_s(3),
                SI53XX_4_N                  => GTREFCLK_N_s(3),
                SI53XX_5_P                  => GTREFCLK_P_s(2),
                SI53XX_5_N                  => GTREFCLK_N_s(2),
                SI53XX_8_P                  => GTREFCLK_P_s(4),
                SI53XX_8_N                  => GTREFCLK_N_s(4),

                LMK0_P                      => LMK_P(0),
                LMK0_N                      => LMK_N(0),
                LMK1_P                      => LMK_P(1),
                LMK1_N                      => LMK_N(1),
                LMK2_P                      => LMK_P(2),
                LMK2_N                      => LMK_N(2),
                LMK3_P                      => LMK_P(3),
                LMK3_N                      => LMK_N(3),
                LMK4_P                      => LMK_P(4),
                LMK4_N                      => LMK_N(4),
                LMK5_P                      => LMK_P(5),
                LMK5_N                      => LMK_N(5),
                LMK6_P                      => LMK_P(6),
                LMK6_N                      => LMK_N(6),
                LMK7_P                      => LMK_P(7),
                LMK7_N                      => LMK_N(7),

                GTREFCLK_P_IN               => (others => '0'),
                GTREFCLK_N_IN               => (others => '0'),

                GTH_REFCLK_OUT              => GTH_REFCLK_LMK_OUT
            );
    end generate; --refclk_notsim
    --
    REGS_INTERFACE : entity work.Regs_RW
        Generic map(
            GBT_NUM                     => GBT_NUM,
            --FE_EMU_EN                   => 0 --MT FE_EMU_EN
            CARD_TYPE                   => CARD_TYPE
        )
        Port map(

            CTRL_SOFT_RESET             => CTRL_SOFT_RESET,
            CTRL_TXPLL_DATAPATH_RESET   => CTRL_TXPLL_DATAPATH_RESET,
            CTRL_RXPLL_DATAPATH_RESET   => CTRL_RXPLL_DATAPATH_RESET,
            CTRL_TX_DATAPATH_RESET      => CTRL_TX_DATAPATH_RESET,
            CTRL_RX_DATAPATH_RESET      => CTRL_RX_DATAPATH_RESET,
            CTRL_TXPOLARITY             => CTRL_TXPOLARITY,
            CTRL_RXPOLARITY             => CTRL_RXPOLARITY,
            CTRL_GBTTXRST               => CTRL_GBTTXRST,
            CTRL_GBTRXRST               => CTRL_GBTRXRST,
            CTRL_CHANNEL_DISABLE        => CTRL_CHANNEL_DISABLE,
            CTRL_FECMODE                => CTRL_FECMODE,
            --CTRL_DATARATE               => CTRL_DATARATE,--RL
            CTRL_GBT_General_ctrl       => CTRL_GBT_General_ctrl,



            MON_RXRSTDONE               => MON_RXRSTDONE,
            MON_TXRSTDONE               => MON_TXRSTDONE,

            -- RL: used for 709, not relevant here
            MON_TXFSMRESETDONE          => (others => '0'),
            MON_RXFSMRESETDONE          => (others => '0'),
            --MON_RXRSTDONE_QUAD          => MON_RXRSTDONE_QUAD,--RL
            --MON_TXRSTDONE_QUAD          => MON_TXRSTDONE_QUAD,--RL
            MON_RXPMARSTDONE            => MON_RXPMARSTDONE,
            MON_TXPMARSTDONE            => MON_TXPMARSTDONE,
            MON_RXCDR_LCK               => MON_RXCDR_LCK,
            --MON_RXCDR_LCK_QUAD          => MON_RXCDR_LCK_QUAD,--RL
            MON_QPLL_LCK                => MON_QPLL_LCK,
            MON_CPLL_LCK                => MON_CPLL_LCK,

            MON_ALIGNMENT_DONE          => MON_ALIGNMENT_DONE,
            --MON_LPGBT_ERRFLG            => MON_LPGBT_ERRFLG, --RL

            MON_FEC_ERROR               =>  MON_FEC_ERROR,
            MON_FEC_ERR_CNT             => MON_FEC_ERR_CNT, --RL see comment at signal definition july 2023
            MON_AUTO_RX_RESET_CNT       => MON_AUTO_RX_RESET_CNT, --RL see comment at signal definition july 2023
            --CTRL_PRBS_ERR_CLR           => PRBS_ERR_CLR,
            --CTRL_ERROR_CNT_SEL          => PRBS_CH_SEL,

            clk40_in                    => CLK40_IN, --RL add july 2023
            register_map_control        => register_map_control,
            register_map_link_monitor    => register_map_link_monitor

        );

    --RL removed commented lines with
    --PRBS_TEST: if PRBS_TEST_EN = 1 generate
    --and
    --FLX_LpGBT_BE_INST: entity work.FLX_LpGBT_BE_Wrapper

    --MT commented
    --    FLX_LpGBT_FE_EMU_INST: if FE_EMU_EN = 1 generate
    --
    --

    --MT added. RL: changed to match link_wrapper inputs
    RX_DOWNLINK_inst: for I in 0 to GBT_NUM - 1 generate
        RX_2b_IC_out(I) <= FE_DOWNLINK_IC_DATA(I);
        RX_2b_EC_out(I) <= FE_DOWNLINK_EC_DATA(I);
        RX_32b_DATA_out(I) <= FE_DOWNLINK_USER_DATA(I);
    end generate RX_DOWNLINK_inst;

    TX_UPLINK_inst: for I in 0 to GBT_NUM - 1 generate
        FE_UPLINK_IC_DATA(I)   <= TX_2b_IC_in(I);
        FE_UPLINK_EC_DATA(I)   <= TX_2b_EC_in(I);
        FE_UPLINK_USER_DATA(I) <= TX_224b_DATA_in(I);
    end generate TX_UPLINK_inst;

    --RL: Added DATARATE and FECMODE for FELIG
    DATARATE <= (others => LINKSconfig(1));
    FECMODE  <= (others => LINKSconfig(0));

    FLX_LpGBT_FE_INST: entity work.FLX_LpGBT_FE_Wrapper_FELIG
        Generic map(
            GBT_NUM                     => GBT_NUM,
            sim_emulator                => sim_emulator
        )
        Port map(

            clk40_in                    => clk40_in,
            clk320_in                   => clk320_in, --MT SIMU+
            rst_hw                      => rst_hw,
            --MT commented
            --        FE_SIDE_RX40MCLK            => FE_SIDE_RX40MCLK,
            --MT added
            FE_SIDE_RX40MCLK            => clk40_rxusrclk_out, --40 MHZ CLK from
            --CLKWIZ (input
            --RXUSRCLK 320)
            --MT commented
            --GTHREFCLK                   => GTH_REFCLK_OUT, --MT GTH_EMU_REFCLK_OUT,
            --MT added
            GTHREFCLK0                   => GTH_REFCLK_OUT,     --SI  RX 320
            GTHREFCLK1                   => GTH_REFCLK_LMK_OUT, --LMK TX 240
            --
            --MT externalizing gtrx/txusrclk for FELIG logic
            GT_TXUSRCLK_OUT           => GT_TXUSRCLK_OUT,
            GT_RXUSRCLK_OUT           => GT_RXUSRCLK_OUT,
            --

            --MT commented
            --RX_P                        => RX_P(GBT_NUM*2-1 downto GBT_NUM),
            --RX_N                        => RX_N(GBT_NUM*2-1 downto GBT_NUM),
            --TX_P                        => TX_P(GBT_NUM*2-1 downto GBT_NUM),
            --TX_N                        => TX_N(GBT_NUM*2-1 downto GBT_NUM),
            --MT added
            RX_P                        => RX_P(GBT_NUM-1 downto 0),
            RX_N                        => RX_N(GBT_NUM-1 downto 0),
            TX_P                        => TX_P(GBT_NUM-1 downto 0),
            TX_N                        => TX_N(GBT_NUM-1 downto 0),


            FE_DOWNLINK_USER_DATA       => FE_DOWNLINK_USER_DATA,
            FE_DOWNLINK_EC_DATA         => FE_DOWNLINK_EC_DATA,
            FE_DOWNLINK_IC_DATA         => FE_DOWNLINK_IC_DATA,

            FE_UPLINK_USER_DATA         => FE_UPLINK_USER_DATA,
            FE_UPLINK_EC_DATA           => FE_UPLINK_EC_DATA,
            FE_UPLINK_IC_DATA           => FE_UPLINK_IC_DATA,

            --MT using _BE_ reg ranges instead
            --CTRL_SOFT_RESET             => CTRL_SOFT_RESET(GBT_NUM/2-1 downto GBT_NUM/4),
            --CTRL_TXPLL_DATAPATH_RESET   => CTRL_TXPLL_DATAPATH_RESET(GBT_NUM/2-1 downto GBT_NUM/4),
            --CTRL_RXPLL_DATAPATH_RESET   => CTRL_RXPLL_DATAPATH_RESET(GBT_NUM/2-1 downto GBT_NUM/4),
            --CTRL_TX_DATAPATH_RESET      => CTRL_TX_DATAPATH_RESET(GBT_NUM/2-1 downto GBT_NUM/4),
            --CTRL_RX_DATAPATH_RESET      => CTRL_RX_DATAPATH_RESET(GBT_NUM/2-1 downto GBT_NUM/4),
            --CTRL_TXPOLARITY             => CTRL_TXPOLARITY(2*GBT_NUM-1 downto GBT_NUM),
            --CTRL_RXPOLARITY             => CTRL_TXPOLARITY(2*GBT_NUM-1 downto GBT_NUM),
            --CTRL_GBTTXRST               => CTRL_GBTTXRST(2*GBT_NUM-1 downto GBT_NUM),
            --CTRL_GBTRXRST               => CTRL_GBTRXRST(2*GBT_NUM-1 downto GBT_NUM),
            --CTRL_DATARATE               => CTRL_DATARATE(2*GBT_NUM-1 downto GBT_NUM),
            --CTRL_FECMODE                => CTRL_FECMODE(2*GBT_NUM-1 downto GBT_NUM),
            --CTRL_CHANNEL_DISABLE        => CTRL_CHANNEL_DISABLE(2*GBT_NUM-1 downto GBT_NUM),
            --CTRL_GBT_General_ctrl       => CTRL_GBT_General_ctrl,



            --MON_RXRSTDONE               => MON_RXRSTDONE(2*GBT_NUM-1 downto GBT_NUM),
            --MON_TXRSTDONE               => MON_TXRSTDONE(2*GBT_NUM-1 downto GBT_NUM),
            --MON_RXRSTDONE_QUAD          => MON_RXRSTDONE_QUAD(GBT_NUM/2-1 downto GBT_NUM/4),
            --MON_TXRSTDONE_QUAD          => MON_TXRSTDONE_QUAD(GBT_NUM/2-1 downto GBT_NUM/4),
            --MON_RXPMARSTDONE            => MON_RXPMARSTDONE(2*GBT_NUM-1 downto GBT_NUM),
            --MON_TXPMARSTDONE            => MON_TXPMARSTDONE(2*GBT_NUM-1 downto GBT_NUM),
            --MON_RXCDR_LCK               => MON_RXCDR_LCK(2*GBT_NUM-1 downto GBT_NUM),
            --MON_RXCDR_LCK_QUAD          => MON_RXCDR_LCK_QUAD(GBT_NUM/2-1 downto GBT_NUM/4),
            --MON_QPLL_LCK                => MON_QPLL_LCK(GBT_NUM/2-1 downto GBT_NUM/4),
            --MON_CPLL_LCK                => MON_CPLL_LCK(2*GBT_NUM-1 downto GBT_NUM),

            --MON_ALIGNMENT_DONE          => MON_ALIGNMENT_DONE(2*GBT_NUM-1 downto GBT_NUM),
            --MON_LPGBT_ERRFLG            => MON_LPGBT_ERRFLG(2*GBT_NUM-1 downto GBT_NUM),

            CTRL_SOFT_RESET             => CTRL_SOFT_RESET(GBT_NUM/4-1 downto 0),
            CTRL_TXPLL_DATAPATH_RESET   => CTRL_TXPLL_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_RXPLL_DATAPATH_RESET   => CTRL_RXPLL_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_TX_DATAPATH_RESET      => CTRL_TX_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_RX_DATAPATH_RESET      => CTRL_RX_DATAPATH_RESET(GBT_NUM/4-1 downto 0),
            CTRL_TXPOLARITY             => CTRL_TXPOLARITY(GBT_NUM-1 downto 0),
            CTRL_RXPOLARITY             => CTRL_TXPOLARITY(GBT_NUM-1 downto 0),
            CTRL_GBTTXRST               => CTRL_GBTTXRST(GBT_NUM-1 downto 0),
            CTRL_GBTRXRST               => CTRL_GBTRXRST(GBT_NUM-1 downto 0),
            CTRL_DATARATE               => DATARATE,--CTRL_DATARATE(GBT_NUM-1 downto 0), --RL removed from Reg_RW
            CTRL_FECMODE                => FECMODE,--CTRL_FECMODE(GBT_NUM-1 downto 0),
            CTRL_CHANNEL_DISABLE        => CTRL_CHANNEL_DISABLE(GBT_NUM-1 downto 0),
            CTRL_GBT_General_ctrl       => CTRL_GBT_General_ctrl,



            MON_RXRSTDONE               => MON_RXRSTDONE(GBT_NUM-1 downto 0),
            MON_TXRSTDONE               => MON_TXRSTDONE(GBT_NUM-1 downto 0),
            MON_RXRSTDONE_QUAD          => MON_RXRSTDONE_QUAD(GBT_NUM/4-1 downto 0), --RL removed from Reg_RW
            MON_TXRSTDONE_QUAD          => MON_TXRSTDONE_QUAD(GBT_NUM/4-1 downto 0), --RL removed from Reg_RW
            MON_RXPMARSTDONE            => MON_RXPMARSTDONE(GBT_NUM-1 downto 0),
            MON_TXPMARSTDONE            => MON_TXPMARSTDONE(GBT_NUM-1 downto 0),
            MON_RXCDR_LCK               => MON_RXCDR_LCK(GBT_NUM-1 downto 0),
            MON_RXCDR_LCK_QUAD          => MON_RXCDR_LCK_QUAD(GBT_NUM/4-1 downto 0), --RL removed from Reg_RW
            MON_QPLL_LCK                => MON_QPLL_LCK(GBT_NUM/4-1 downto 0),
            MON_CPLL_LCK                => MON_CPLL_LCK(GBT_NUM-1 downto 0),

            MON_ALIGNMENT_DONE          => MON_ALIGNMENT_DONE(GBT_NUM-1 downto 0),
            MON_LPGBT_ERRFLG            => MON_LPGBT_ERRFLG(GBT_NUM-1 downto 0),  --RL removed from Reg_RW

            --MT for FELIG from mgt_framealigner . 1 pulse every c_wordRatio (=8)
            sta_headerFlag_out         => sta_headerFlag_i,
            --
            --MT added: needed by FELIG to latch tx payload to generated data. Need to
            --be synchronized with emulator logic
            tx_flag_out                => tx_flag_out,
            --MT LMK lock signal
            LMK_LD                     => LMK_LD,
            RxCdrLock_o                => RxCdrLock_o,
            alignment_done_o           => alignment_done_o
        );
    sta_headerFlag_out <= sta_headerFlag_i; --MT added
    alignment_done_out <= MON_ALIGNMENT_DONE; --MT added

    --MT commented
    --    end generate;
    --

    --RL: coppied from GBT wrapper. sends the RX clock to the LMK
    bufgceobuf_notsim : if sim_emulator = false generate
        -- BUFGCE_DIV instantiation: added MT/SS
        BUFGCE_DIV_inst : BUFGCE_DIV
            generic map (
                BUFGCE_DIVIDE => 8, -- 1-8 -divide by 8 to get 40 MHz
                -- Programmable Inversion Attributes: Specifies built-in programmable inversion on specific pins
                IS_CE_INVERTED => '0', -- Optional inversion for CE
                IS_CLR_INVERTED => '0', -- Optional inversion for CLR
                IS_I_INVERTED => '0' -- Optional inversion for I
            )
            port map (
                O => GT_RXUSRCLK_40MHz, -- 1-bit output: Buffer
                CE => '1', -- 1-bit input: Buffer enable
                CLR => '0', -- 1-bit input: Asynchronous clear
                I => GT_RXUSRCLK_OUT(0) -- 1-bit input: Buffer 240 MHz RXUSER_CLK after BUFG_GT
            );
        -- End of BUFGCE_DIV_inst instantiation

        --OBUFDS to route the 240 MHz RXUSER CLK into the LMK03200 jitter cleaner  to create the TXREFCLK --MT/SS
        OBUF240_LMK03200: OBUFDS
            generic map (
                IOSTANDARD => "LVDS",
                SLEW       => "FAST")
            port map(
                I => GT_RXUSRCLK_40MHz,
                O => CLK40_FPGA2LMK_out_P,
                OB => CLK40_FPGA2LMK_out_N);
    end generate; --bufgceobuf_notsim

    --- LMK Reset Process----- MT
    --   alignment_done_inv <= not sta_headerFlag_i(0);
    --MT SIMU+
    resetlmk_sim : if sim_emulator = true generate
        RESET_TO_LMK <= '0';
    end generate;
    --

    resetlmk_notsim : if sim_emulator = false generate
        process(clk40_in)
        begin
            if (clk40_in'event and clk40_in='1') then  --rising_edge(clk40_in)
                LMK_PIPE(3 downto 1) <= LMK_PIPE(2 downto 0);
                LMK_PIPE(0) <= LMK_RESET_b; --sta_headerFlag_i(0)
                if ((LMK_PIPE(3) = '0') AND (LMK_PIPE(0) = '1')) then --rising edge
                    LMK_RESET  <= '1';
                else
                    LMK_RESET  <= '0';
                end if;
            end if;
        end process;
        RESET_TO_LMK <= LMK_RESET; -- MT
    end generate;  --resetlmk_notsim

    --RL state machine that generates LMK_RESET

    process(clk40_rxusrclk_out)
    begin
        if (clk40_rxusrclk_out'event and clk40_rxusrclk_out='1') then
            sta_headerFlag_i_40rec <= sta_headerFlag_i(0);
            RxCdrLock_o_40rec <= RxCdrLock_o(0);
            alignment_done_o_40rec <= alignment_done_o(0);
        end if;
    end process;

    process(clk40_rxusrclk_out)
    begin
        if (clk40_rxusrclk_out'event and clk40_rxusrclk_out='1') then
       sm_lmk_reset: case state is
                when st_idle =>
                    --           ila_st_idle              <= '1';
                    --           ila_st_wait_for_aligment <= '0';
                    --           ila_st_LMK_reseted       <= '0';
                    LMK_RESET_b <= '0';
                    if RxCdrLock_o_40rec = '1' then
                        state <= st_wait_for_aligment;
                    else
                        state <= st_idle;
                    end if;
                when st_wait_for_aligment =>
                    --           ila_st_idle              <= '0';
                    --           ila_st_wait_for_aligment <= '1';
                    --           ila_st_LMK_reseted       <= '0';
                    if alignment_done_o_40rec = '1'then
                        LMK_RESET_b <= '1';
                        state <= st_LMK_reseted;
                    elsif RxCdrLock_o_40rec = '0' then
                        LMK_RESET_b <= '0';
                        state <= st_idle;
                    else
                        LMK_RESET_b <= '0';
                        state <= st_wait_for_aligment;
                    end if;
                when st_LMK_reseted =>
                    --           ila_st_idle              <= '0';
                    --           ila_st_wait_for_aligment <= '0';
                    --           ila_st_LMK_reseted       <= '1';
                    LMK_RESET_b <= '0';
                    if RxCdrLock_o_40rec = '0' and LMK_RESET_b <= '0'then
                        state <= st_idle;
                    else
                        state <= st_LMK_reseted;
                    end if;
                when others =>
                    state <= st_idle;
            end case sm_lmk_reset;
        end if;
    end process;

--  process(clk40_in)
--  begin
--    if (clk40_in'event and clk40_in='1') then
--      ila_RxCdrLock_o <= RxCdrLock_o_40rec;
--      ila_sta_headerFlag_i <= sta_headerFlag_i_40rec;
--      ila_LMK_RESET_b <= LMK_RESET_b;
--      ila_alignment_done_o <= alignment_done_o_40rec;
--      ila_ila_st_idle <= ila_st_idle;
--      ila_ila_st_wait_for_aligment <= ila_st_wait_for_aligment;
--      ila_ila_st_LMK_reseted <= ila_st_LMK_reseted;
--    end if;
--  end process;

--  ila_STATE_LMK : ila_SM_LMK
--    port map(
--      clk       => clk40_in,
--      probe0(0) => ila_ila_st_idle,
--      probe1(0) => ila_ila_st_wait_for_aligment,
--      probe2(0) => ila_ila_st_LMK_reseted,
--      probe3(0) => ila_RxCdrLock_o,
--      probe4(0) => ila_sta_headerFlag_i,
--      probe5(0) => ila_LMK_RESET_b,
--      probe6(0) => ila_alignment_done_o
--    );

end Behavioral;
