--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-- v2 by Ricardo Luz.
-- Based on elink_printer.vhd initially written by Michael Oberlingand and later modified by Marco Trovato.
-- Complies with 32-b width and MSB first for all widths.


LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;

library UNISIM;
    use UNISIM.VComponents.all;

entity gbt_word_printer_v2 is
    generic (
        NUMELINKmax                   : integer := 112
    );
    port (
        clk            : in  std_logic;
        elink_control    : in  lane_elink_control_array(0 to NUMELINKmax-1);
        elink_sync      : in  std_logic;
        elink_data      : in  array_of_slv_9_0(0 to NUMELINKmax-1);
        tx_flag        : in  std_logic;
        LINKSconfig         : in  std_logic_vector(2 downto 0);
        MSBfirst            : in  std_logic;
        elink_read_enable  : out std_logic_vector(0 to NUMELINKmax-1);
        gbt_payload      : out std_logic_vector(223 downto 0);
        aurora_en           : in  std_logic_vector(0 to NUMELINKmax-1)
    );
end gbt_word_printer_v2;

architecture Behavioral of gbt_word_printer_v2 is
    signal gbt_shift_count    : std_logic_vector(  3 downto 0) := (others => '0');
    signal bit_stream_en    : std_logic_vector(  3 downto 0) := (others => '0');
    signal bit_stream_sync    : std_logic := '0';
    signal elink_sync_lat    : std_logic := '0';
    signal gbt_word_latch    : std_logic := '0';
    signal bit_stream           : array_of_slv_31_0(0 to NUMELINKmax-1);
    signal gbt_bit_stream    : std_logic_vector(223 downto 0) := (others => '0');
    signal gbt_payload_i      : std_logic_vector(223 downto 0) := (others => '0');
    signal elink_read_enable_i  : std_logic_vector(0 to NUMELINKmax-1);
    signal lpgbt                : std_logic := '0';
begin

    gbt_payload <= gbt_payload_i;
    elink_read_enable <= elink_read_enable_i;
    lpgbt <= LINKSconfig(2); --0 is gbt, 1 is lpgbt

    reg_input : process(clk)
    begin
        if clk'event and clk ='1' then
            if (tx_flag = '1' ) then
                gbt_shift_count <= (others => '0');
                if gbt_shift_count /= "0101" and lpgbt = '0' then
                    elink_sync_lat <= '1';
                elsif gbt_shift_count /= "0111" and lpgbt = '1' then
                    elink_sync_lat <= '1';
                end if;
            elsif (gbt_shift_count /= "1111") then
                gbt_shift_count <= gbt_shift_count + 1;
            end if;
            --------
            if (gbt_word_latch = '1') then
                gbt_payload_i <= gbt_bit_stream;
            end if;
            --------
            case gbt_shift_count is
                when "0000" =>
                    bit_stream_en <= "1000";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0001" =>
                    bit_stream_en <= "1000";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0010" =>
                    bit_stream_en <= "1100";
                    bit_stream_sync <= elink_sync_lat;
                    gbt_word_latch <= '0';
                    elink_sync_lat <= elink_sync;
                when "0011" =>
                    bit_stream_en <= "1111";
                    bit_stream_sync <= '0';
                    if lpgbt = '0' then
                        gbt_word_latch <= '1';
                    else
                        gbt_word_latch <= '0';
                    end if;
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0100" =>
                    bit_stream_en <= "1110";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0101" =>
                    bit_stream_en <= "1100";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0110" =>
                    bit_stream_en <= "1000";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when "0111" =>
                    bit_stream_en <= "1000";
                    bit_stream_sync <= '0';
                    if lpgbt = '1' then
                        gbt_word_latch <= '1';
                    else
                        gbt_word_latch <= '0';
                    end if;
                    if (elink_sync = '1') then
                        elink_sync_lat <= '1';
                    end if;
                when others =>
                    bit_stream_en <= "0000";
                    bit_stream_sync <= '0';
                    gbt_word_latch <= '0';
                    elink_sync_lat <= '1';
            end case;
        end if;
    end process reg_input;

    gen_elink_printers : for i in 0 to NUMELINKmax-1 generate
        elink_bit_feeder : entity work.elink_printer_bit_feeder_v2
            port map (
                clk              => clk,
                enable          => elink_control(i).enable,
                elink_endian_mode      => elink_control(i).endian_mode,
                elink_output_width  => elink_control(i).output_width,
                elink_input_width      => elink_control(i).input_width,
                word_in          => elink_data(i),
                bit_stream_en        => bit_stream_en,
                bit_stream_sync    => bit_stream_sync,
                LINKSconfig           => LINKSconfig,
                gbt_word_latch        => gbt_word_latch,
                MSBfirst              => MSBfirst,
                aurora                => aurora_en(i),
                read_enable        => elink_read_enable_i(i),
                bit_stream_out        => bit_stream(i)
            );
    end generate gen_elink_printers;

    gen_gbt_stream : for i in 0 to 6 generate
        signal output_width : std_logic_vector(2 downto 0);
    begin
        output_width <= elink_control(i*16).output_width;
        gbt_bit_stream(32*(i+1)-1 downto 32*i)
                   <= bit_stream(i*16+15)( 1 downto 0) &
                      bit_stream(i*16+14)( 1 downto 0) &
                      bit_stream(i*16+13)( 1 downto 0) &
                      bit_stream(i*16+12)( 1 downto 0) &
                      bit_stream(i*16+11)( 1 downto 0) &
                      bit_stream(i*16+10)( 1 downto 0) &
                      bit_stream(i*16+ 9)( 1 downto 0) &
                      bit_stream(i*16+ 8)( 1 downto 0) &
                      bit_stream(i*16+ 7)( 1 downto 0) &
                      bit_stream(i*16+ 6)( 1 downto 0) &
                      bit_stream(i*16+ 5)( 1 downto 0) &
                      bit_stream(i*16+ 4)( 1 downto 0) &
                      bit_stream(i*16+ 3)( 1 downto 0) &
                      bit_stream(i*16+ 2)( 1 downto 0) &
                      bit_stream(i*16+ 1)( 1 downto 0) &
                      bit_stream(i*16+ 0)( 1 downto 0) when output_width="000" else
                      bit_stream(i*16+14)( 3 downto 0) &
                      bit_stream(i*16+12)( 3 downto 0) &
                      bit_stream(i*16+10)( 3 downto 0) &
                      bit_stream(i*16+ 8)( 3 downto 0) &
                      bit_stream(i*16+ 6)( 3 downto 0) &
                      bit_stream(i*16+ 4)( 3 downto 0) &
                      bit_stream(i*16+ 2)( 3 downto 0) &
                      bit_stream(i*16+ 0)( 3 downto 0) when output_width="001" else
                      bit_stream(i*16+12)( 7 downto 0) &
                      bit_stream(i*16+ 8)( 7 downto 0) &
                      bit_stream(i*16+ 4)( 7 downto 0) &
                      bit_stream(i*16+ 0)( 7 downto 0) when output_width="010" else
                      bit_stream(i*16+ 8)(15 downto 0) &
                      bit_stream(i*16+ 0)(15 downto 0) when output_width="011" else
                      bit_stream(i*16+ 0)(31 downto 0) when output_width="100" else
                      (others => '0');
    end generate gen_gbt_stream;

end Behavioral;
