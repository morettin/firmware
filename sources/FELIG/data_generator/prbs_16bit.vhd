--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	Unknown (Soo Ryu / John Anderson)
--
-- Design Name:	prbs_16bit
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Coming soon.
--
-- Change Log:	V1.0 - 
--
--==============================================================================

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity prbs_16bit is
    Port ( clk_i : in STD_LOGIC;
           hold_i : in std_logic;
           rst_i : in STD_LOGIC;
           seed_i : in std_logic_vector(15 downto 0);
           data_rdy : out STD_LOGIC;
           data_o : out STD_LOGIC_VECTOR (15 downto 0));
end prbs_16bit;

architecture RTL of prbs_16bit is
    signal data : std_logic_vector(15 downto 0) := x"0000";
begin
data_o <= data;

u0 : process (rst_i, clk_i,seed_i)
begin
    if ( rst_i = '1') then
      data <= seed_i ;
      data_rdy <= '0';
	elsif (clk_i'event and clk_i = '1') then
	  data(0) <= data(15) XOR data(7) XOR data(4) XOR data(1);
	  if ( hold_i = '0') then
        data(15 downto 1) <= data(14 downto 0);
        data_rdy <= '1';
      else
        data_rdy <= '0';
      end if;    
	end if;
end process;

end RTL;
