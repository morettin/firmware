--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  Soo Ryu
-- Modified By:  Michael Oberling
--
-- Design Name:  elink_packet_generator
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;
--use ieee.numeric_std.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.centralRouter_package.all;
    use work.ip_lib.ALL;

entity elink_packet_generator is
    generic (
        epath  : std_logic_vector (4 downto 0);
        egroup  : std_logic_vector (2 downto 0)
    );
    Port (
        clk_i : in STD_LOGIC;
        rst_i : in STD_LOGIC;
        --MT  ewidth : in STD_LOGIC_VECTOR (1 downto 0);
        ewidth : in STD_LOGIC_VECTOR (2 downto 0);
        pattern_sel_i : in STD_LOGIC_VECTOR (1 downto 0);
        userdata_i : in STD_LOGIC_VECTOR (15 downto 0);
        chunk_length_i : in STD_LOGIC_VECTOR (11 downto 0);
        l1trigger_i : in STD_LOGIC;
        l1a_id : in STD_LOGIC_VECTOR (15 downto 0);
        sw_busy_i : in std_logic;
        elinkdata_o : out STD_LOGIC_VECTOR (17 downto 0);
        elinkdata_rdy_o : out STD_LOGIC;
        --Frans 2
        chunk_length_trig_o : out STD_LOGIC --ask for a new chunk length if 1 (could be random).
    );
end elink_packet_generator;

architecture Behavioral of elink_packet_generator is
    --
    constant sof_code : std_logic_vector (1 downto 0) := "10";    --[comma][sof]
    constant eof_code : std_logic_vector (1 downto 0) := "01";    --[eof][comma]
    constant data_code : std_logic_vector (1 downto 0) := "00";    --[data][data]
    constant comma_code : std_logic_vector (1 downto 0) := "11";    --[comma][comma]

    type ChunkFSM   is (idl, soc, h1, h2, h3, payload, eoc) ;
    type BusyFSM    is (idl, sob, eob) ;

    signal busy_data : std_logic_vector(17 downto 0);
    signal data16b, prbs16b, din, userdata  : std_logic_vector(15 downto 0);
    signal pattern_sel   : std_logic_vector(1 downto 0);
    signal clk           : std_logic;
    signal hold, rst     : std_logic;
    -- signal l1trig        : std_logic; -- RL: commented because it is never used
    signal prbs16b_rdy   : std_logic;
    signal chunk_length  : std_logic_vector(11 downto 0) := x"0A0";    -- chunk length 12 bits in byte
    signal state : ChunkFSM;
    signal busy_state : BusyFSM;
    signal dataCode : std_logic_vector(1 downto 0);
    signal count_length : std_logic_vector(11 downto 0) := x"000";
    signal count_chunk  : std_logic_vector(11 downto 0) := x"000";
    signal l1a_pipe : std_logic := '0';
    signal is_header : std_logic := '0';
    signal elinkdata_rdy : std_logic := '0';

    signal q_unused : std_logic_vector(47 downto count_length'high);

    --- Signals for L1A trigger FIFO ----
    signal l1a_id_Fifo   : std_logic_vector(15 downto 0);

    signal data_in  : std_logic_vector(15 downto 0);
    --signal write_en : std_logic ; RL: commented because it is never used
    signal read_en  : std_logic ;
    signal valid_out  : std_logic;
    signal data_out : std_logic_vector(15 downto 0);
    signal Fifo_full    : std_logic;
    signal Fifo_almost_full : std_logic;
    signal Fifo_empty  : std_logic;
    signal Fifo_almost_empty    : std_logic;
    signal State_counter : std_logic_vector(4 downto 0);
    signal Fifo_Data_Count : std_logic_vector(10 downto 0);


    --FIFO for the triggers
    COMPONENT L1A_Fifo
        PORT (
            clk : IN STD_LOGIC;
            srst : IN STD_LOGIC;
            din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            wr_en : IN STD_LOGIC;
            rd_en : IN STD_LOGIC;
            dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            full : OUT STD_LOGIC;
            almost_full : OUT STD_LOGIC;
            empty : OUT STD_LOGIC;
            almost_empty : OUT STD_LOGIC;
            valid : OUT STD_LOGIC;
            data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
            wr_rst_busy : OUT STD_LOGIC;
            rd_rst_busy : OUT STD_LOGIC
        );
    END COMPONENT;


begin

    clk          <= clk_i;
    rst          <= rst_i;
    pattern_sel  <= pattern_sel_i;
    chunk_length <= chunk_length_i;
    userdata     <= userdata_i;

    elinkdata_o <= dataCode & data16b when sw_busy_i = '0' else
                   busy_data; -- 18bit

    -- MBO: commented to save on slices.  This uses 55slices x 24 channels = 1,320 (about 1%) x [# emulators / channel].
    --prbs_gen: entity work.prbs_16bit
    --port map (
    --  clk_i    => clk,
    --  rst_i    => rst,
    --  hold_i   => hold,
    --  seed_i   => userdata,
    --  data_rdy => prbs16b_rdy,
    --  data_o   => prbs16b
    --);

    din <= x"0000" when pattern_sel = "11" else
           prbs16b  when pattern_sel = "10" else
           userdata when pattern_sel = "01" else
           x"0" & count_length when pattern_sel = "00";

    -- l1trig <= l1trigger_i; -- RL: commented because it is never used
    elinkdata_rdy_o <= elinkdata_rdy;

    -- MBO: slice off a few more slices.
    -- mbo: Changed to count down, forced even.
    chunk_counter : dsp_counter
        PORT MAP (
            CLK                => clk,
            CE                => elinkdata_rdy,
            SCLR              => rst,
            UP                => '0',
            LOAD              => is_header,
            L(47 downto count_length'high)  => (others => '0'),
            L(count_length'high-1 downto 0)  => chunk_length(count_length'high downto 1),
            Q(47 downto count_length'high)  => q_unused,
            Q(count_length'high-1 downto 0)  => count_length(count_length'high downto 1)
        );
    count_length(0) <= '0';

    -- L1A trigger Instantiation ----
    -- SS: added FIFO for randon L1A trigger--
    data_in <=  l1a_id;--(15 downto 0);
    L1A_Trigger_FIFO_Inst : L1A_Fifo
        PORT MAP (
            clk => clk,
            srst => rst,
            din => data_in,
            wr_en => l1trigger_i,
            rd_en => read_en,
            valid => valid_out,
            dout => data_out,
            full => Fifo_full,
            almost_full => Fifo_almost_full,
            empty => Fifo_empty,
            almost_empty => Fifo_almost_empty,
            data_count => Fifo_Data_Count,
            wr_rst_busy => open,
            rd_rst_busy => open
        );

    -- End L1A trigger Instantiation ----

    chunk_generator: process (clk, rst)
    begin
        if rst = '1' then
            state      <= idl;
            dataCode    <= "11";  -- comma
            count_chunk  <= (others => '0');
            busy_state    <= sob;
        elsif clk'event and clk='1' then
            --Frans 1
            --chunk_length_trig_o <= '0'; --ask for a new chunk length if 1 (could be random).
            -- RL commented to clean up code
            if sw_busy_i = '1' then
                chunk_length_trig_o <= '0'; --ask for a new chunk length if 1 (could be random).
        busy_mode: case busy_state is
                    when sob =>
                        busy_data <= "00" & "00000000" & Kchar_sob;
                        elinkdata_rdy <= '1';
                        busy_state <= idl;
                    when idl =>
                        elinkdata_rdy <= '0';
                        if (chunk_length < count_length) then
                            busy_state <= eob;
                        end if;
                    when eob =>
                        busy_data <= "00" & "00000000" & Kchar_eob;
                        elinkdata_rdy <= '1';
                end case busy_mode;
            else
                busy_state <= sob;
        normal_mode: case state is
                    when idl =>
                        data16b  <= (others => '0');
                        is_header <= '1';
                        chunk_length_trig_o <= '0'; --ask for a new chunk length if 1 (could be random).
                        if  valid_out = '1' then
                            state    <= soc;
                            dataCode <= sof_code;
                            elinkdata_rdy <= '1';     -- MBO: added line to assert elinkdata_rdy_o
                            read_en <= '0'; -- SS making read_en 0 after getting the trigger
                            l1a_id_Fifo   <= data_out;
                        else
                            read_en <= '1'; -- RL read_en value was unknown until the first state change
                            dataCode <= comma_code;  -- MBO: don't care as long as elinkdata_rdy_o is '0'
                            elinkdata_rdy <= '0';     -- MBO: added line to release elinkdata_rdy_o
                        end if;
                    when soc =>
                        state <= h1;
                        dataCode <= data_code;
                        --mbo: 0xAA,CL_MSB (or 0x00)?
                        data16b(15 downto 8) <= X"AA";
                        data16b( 7 downto 0) <= X"0" & chunk_length(11 downto 8);
                    when h1 =>
                        state <= h2;
                        dataCode <= data_code;
                        --mbo: CL_LSB,I1A_ID_LSB
                        data16b(15 downto 8) <= chunk_length(7 downto 1) & '0';-- MBO: Forced even.
                        data16b( 7 downto 0) <= l1a_id_Fifo(15 downto 8);
                    when h2 =>
                        state <= h3;
                        dataCode <= data_code;
                        is_header <= '0';    -- MBO: enable counter at this point to allow for DSP pipeline delay (1 clock)
                        --mbo: I1A_ID_MSB, 0xBB
                        data16b(15 downto 8) <= l1a_id_Fifo(7 downto 0);
                        data16b( 7 downto 0) <= X"BB";
                    when h3 =>
                        dataCode <= data_code;
                        --mbo: 0xAA,Fixed Value?
                        data16b(15 downto 8) <= X"AA";
                        if (ewidth = "000") then   data16b( 7 downto 0) <= X"02";
                        elsif (ewidth = "001") then    data16b( 7 downto 0) <= X"04";
                        elsif (ewidth = "010") then    data16b( 7 downto 0) <= X"08";
                        elsif (ewidth = "011") then    data16b( 7 downto 0) <= X"10";
                        --MT
                        elsif (ewidth = "100") then    data16b( 7 downto 0) <= X"20";
                        else           data16b( 7 downto 0) <= X"00";
                        end if;
                        if count_length = 0 then
                            state <= eoc;
                        else
                            state <= payload;
                        end if;
                    when payload =>
                        dataCode <= data_code;
                        data16b <= din;
                        if count_length = 0 then
                            state <= eoc;
                        end if;
                    when eoc =>
                        state    <= idl;
                        dataCode <= eof_code;
                        data16b  <= (others => '0');  -- MBO: don't care.
                        count_chunk <= count_chunk + 1;
                        read_en <= '1';
                        --Frans 1
                        chunk_length_trig_o <= '1'; --ask for a new chunk length if 1 (could be random).
                end case normal_mode;
            end if;
        end if;
    end process;

end Behavioral;
