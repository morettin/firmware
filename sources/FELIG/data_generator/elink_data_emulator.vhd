--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Shelfali Saxena
--!               mtrovato
--!               Ricardo Luz
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.
--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  John Anderson, Soo Ryu
-- Modified By:  Michael Oberling
--
-- Design Name:  elink_data_emulator_lightweight
-- Version:    1.0
-- Date:    9/13/2017
--
-- Description:  Coming soon.
--
-- Change Log:  V1.0 -
--
--==============================================================================

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    --MT
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;
    --

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

    use work.type_lib.ALL;

    use work.centralRouter_package.all;

--Frans 1
    use work.pcie_package.all;
entity elink_data_emulator is
    generic (
        epath        : std_logic_vector (4 downto 0);
        egroup        : std_logic_vector (2 downto 0);
        --Frans 2
        LANE_ID             : integer := 0
    );
    port (
        clk40        : in STD_LOGIC;
        --        clk240        : in STD_LOGIC;
        clk            : in STD_LOGIC;
        emu_control      : in lane_emulator_control;
        elink_data_out    : out STD_LOGIC_VECTOR (9 downto 0);
        elink_data_re    : in  std_logic;
        l1a_trig      : in STD_LOGIC;
        l1a_id        : in STD_LOGIC_VECTOR (15 downto 0);
        flag_data_gen       : out std_logic; --RL added to be used to sync the read enables
        ila_data_gen_out  : out std_logic_vector (17 downto 0);
        ila_fifo_out    : out std_logic_vector ( 9 downto 0);
        ila_data_gen_we    : out std_logic;
        --MT checker upstream fifo
        ila_count_upstfifochk   : out std_logic_vector(10 downto 0);
        ila_fifo_flush          : out std_logic;
        --MT
        ila_count_chk_out       : out std_logic_vector(10 downto 0);
        ila_count_chk_out2      : out std_logic_vector(10 downto 0);
        ila_isEOP_chk2          : out std_logic;
        ila_efifoDout_8b10b     : out std_logic_vector(9 downto 0);
        ila_enc10bitRdy         : out std_logic;

        ila_efifoFull : out std_logic;
        ila_efifoEmpty : out std_logic;
        ila_efifoPfull : out std_logic;
        ila_elink_data_re : out std_logic;
        ila_efifoDoutRdy : out std_logic;
        --Frans 1
        SELECT_RANDOM : in std_logic_vector(0 downto 0);
        chunk_length_out : out std_logic_vector(11 downto 0)        ;
        --
        --MT 2 (Fran 2)
        FMEMU_RANDOM_RAM_ADDR          : in std_logic_vector(9 downto 0);    -- Controls the address of the ramblock for the random number generator
        FMEMU_RANDOM_RAM               : in bitfield_fmemu_random_ram_t_type;
        FMEMU_RANDOM_CONTROL           : in bitfield_fmemu_random_control_w_type;
        aurora_en                      : out std_logic
    --SS (SWAP LSB MSB)
    --fhCR_REVERSE_10B               : in  std_logic
    );
end entity elink_data_emulator;

architecture Behavioral of elink_data_emulator is
    signal elinkdata_o    : std_logic_vector(17 downto 0); -- 18 bit input to the elinkinterface
    signal elinkdata_rdy_o  : std_logic;

    --RC outputs of the NSW packet gen:
    signal elinkdata_o_nsw    : std_logic_vector(17 downto 0); -- 18 bit input to the elinkinterface
    signal elinkdata_rdy_o_nsw  : std_logic;


    --RC outputs of the default elink packet gen:
    signal elinkdata_o_def    : std_logic_vector(17 downto 0); -- 18 bit input to the elinkinterface
    signal elinkdata_rdy_o_def  : std_logic;


    signal elink_tx_rst    : std_logic;
    signal fifo_flush    : std_logic;
    signal efifoPfull    : std_logic;

    signal efifoDoutRdy, efifoFull, efifoEmpty : std_logic := '0';
    signal EDATA_OUT : std_logic_vector(9 downto 0) := (others => '0');
    signal efifoDout_8b10b: std_logic_vector(9 downto 0) := (others => '0');
    signal enc10bit : std_logic_vector (9 downto 0);
    signal enc10bitRdy : std_logic;

    --Frans 1
    signal chunk_length : std_logic_vector(11 downto 0);
    --Frans 2
    signal chunk_length_trig: std_logic;
    signal random_chunk_length : std_logic_vector(15 downto 0);
    --RC outputs of each generator:
    signal chunk_length_trig_nsw: std_logic;
    signal chunk_length_trig_def: std_logic;


    --MT checker
    signal rst_chk : std_logic:= '0';
    signal datacode_chk : std_logic_vector(1 downto 0) := (others => '0');
    signal data_chk : std_logic_vector(15 downto 0) := (others => '0');
    signal valid_chk : std_logic := '0';
    signal count_chk : std_logic_vector(10 downto 0) := (others => '0');
    type STCHK  is (st_idl, st_start, st_count) ;
    signal state_chk : STCHK := st_idl;
    signal err_chk : std_logic := '0';
    --upstreamEpathFifoWrap
    signal count_fifochk : std_logic_vector(15 downto 0) := (others => '0');
    --MT checker upstream fifo
    signal data_upstfifochk : std_logic_vector(9 downto 0) := (others => '0');
    signal valid_upstfifochk : std_logic := '0';
    signal count_upstfifochk : std_logic_vector(10 downto 0) := (others => '0');
    signal state_upstfifochk : STCHK := st_idl;
    signal err_upstfifochk : std_logic := '0';
    --MT checker2
    signal rst_chk2 : std_logic:= '0';
    signal data_chk2 : std_logic_vector(9 downto 0) := (others => '0');
    signal count_chk2 : std_logic_vector(10 downto 0) := (others => '0');
    signal state_chk2 : STCHK := st_idl;
    signal err_chk2 : std_logic := '0';
    signal isEOP_chk2 : std_logic := '0';
    signal valid_chk2 : std_logic := '0';

    --signal aurora_std           : std_logic := '0';
    signal elink_data_re_final      : std_logic := '0';
    signal elink_data_re_aurora     : std_logic := '0';
    signal elink_data_out_pre       : std_logic_vector(9 downto 0) := (others => '0');
    signal elink_data_out_aurora    : std_logic_vector(9 downto 0) := (others => '0');
    signal elink_data_out_aurora_b  : std_logic_vector(9 downto 0) := (others => '0');

begin
    ila_data_gen_out  <= elinkdata_o;
    ila_fifo_out    <= EDATA_OUT;
    ila_data_gen_we    <= elinkdata_rdy_o;

    --MT
    ila_efifoFull           <= efifoFull;
    ila_efifoEmpty          <= efifoEmpty;
    ila_efifoPfull          <= efifoPfull;
    ila_elink_data_re       <= elink_data_re;
    ila_efifoDoutRdy        <= efifoDoutRdy;

    --MT checker upstream fifo
    ila_count_upstfifochk <= count_upstfifochk;
    ila_fifo_flush <= fifo_flush;

    --MT
    ila_count_chk_out       <= count_chk;
    ila_count_chk_out2      <= count_chk2;
    ila_isEOP_chk2          <= isEOP_chk2;
    ila_efifoDout_8b10b     <= efifoDout_8b10b;
    ila_enc10bitRdy         <= enc10bitRdy;
    --

    --RC: added NSW packet generator & muxes
    nsw_data_gen : entity work.nsw_packet_generator
        generic map (
            epath      => epath,
            egroup      => egroup
        )
        port map (
            clk_i      => clk, --clk240,
            rst_i      => emu_control.reset,
            ewidth      => emu_control.output_width,
            pattern_sel_i  => emu_control.pattern_select,
            userdata_i    => emu_control.userdata,
            chunk_length_i  => chunk_length, --Frans 1 emu_control.chunk_length(11 downto 0),  -- length of packet in bytes, 12bits = 4kBytes
            sw_busy_i    => emu_control.sw_busy,
            elinkdata_o    => elinkdata_o_nsw,          -- 18bit = 2bit datacode & 16bit data
            l1trigger_i    => l1a_trig,          -- L1 trigger
            l1a_id      => l1a_id,
            elinkdata_rdy_o  => elinkdata_rdy_o_nsw,
            --Frans 2
            chunk_length_trig_o => chunk_length_trig_nsw,
            even_parity_i   => emu_control.nsw_even_parity
        );


    def_data_gen : entity work.elink_packet_generator
        generic map (
            epath      => epath,
            egroup      => egroup
        )
        port map (
            clk_i      => clk, --clk240,
            rst_i      => emu_control.reset,
            ewidth      => emu_control.output_width,
            pattern_sel_i  => emu_control.pattern_select,
            userdata_i    => emu_control.userdata,
            chunk_length_i  => chunk_length, --Frans 1 emu_control.chunk_length(11 downto 0),  -- length of packet in bytes, 12bits = 4kBytes
            sw_busy_i    => emu_control.sw_busy,
            elinkdata_o    => elinkdata_o_def,          -- 18bit = 2bit datacode & 16bit data
            l1trigger_i    => l1a_trig,          -- L1 trigger
            l1a_id      => l1a_id,
            elinkdata_rdy_o  => elinkdata_rdy_o_def,
            --Frans 2
            chunk_length_trig_o => chunk_length_trig_def
        );

    elinkdata_o <= elinkdata_o_nsw when (emu_control.pack_gen_select = '1')  else
                   elinkdata_o_def;

    elinkdata_rdy_o <= elinkdata_rdy_o_nsw when (emu_control.pack_gen_select = '1') else
                       elinkdata_rdy_o_def;

    chunk_length_trig <= chunk_length_trig_nsw when (emu_control.pack_gen_select = '1') else
                         chunk_length_trig_def;

    --Frans 1

    --chunkLengthSel: process(register_map_control_40xtal, emu_control) --, random_chunk_length)
    --begin
    --   if register_map_control_40xtal.FMEMU_RANDOM_CONTROL.SELECT_RANDOM = "0" then
    --      chunk_length <= emu_control.chunk_length(11 downto 0);
    --   else
    --      chunk_length <= X"01A"; --random_chunk_length(11 downto 0);
    --   end if;
    --end process;
    --
    --    chunk_length <= emu_control.chunk_length(11 downto 0) when (SELECT_RANDOM = "0") else
    --                    X"01A";

    chunk_length_out <= chunk_length;

    --Frans 2
    chunk_length <= emu_control.chunk_length(11 downto 0) when (SELECT_RANDOM = "0") else
                    random_chunk_length(11 downto 0);


    random_gen0: entity work.Random_gen
        generic map(
            LANE_ID  => LANE_ID
        )
        port map(
            rst                  => elink_tx_rst,--: in     std_logic;
            clk40                => clk40,--: in     std_logic;
            --        clk240               => clk240,--: in     std_logic;
            clk                  => clk,--: in     std_logic;
            --        register_map_control => register_map_control_40xtal,--: in     register_map_control_type;
            rg_rst               => elink_tx_rst,--: in     std_logic;
            rg_enb               => chunk_length_trig,--: in     std_logic;
            rg_doutb             => random_chunk_length, --: out    std_logic_vector(15 downto 0)
            --MT 2 (Fran 2)
            FMEMU_RANDOM_RAM_ADDR          => FMEMU_RANDOM_RAM_ADDR ,
            FMEMU_RANDOM_RAM               => FMEMU_RANDOM_RAM      ,
            FMEMU_RANDOM_CONTROL           => FMEMU_RANDOM_CONTROL
        );
    --


    --MT checker
    --datacode: elinkdata_o(17 downto 16)
    --data:  elinkdata_o(15 downto 0)
    --valid  elinkdata_rdy_o
    rst_chk <= emu_control.reset;
    datacode_chk <= elinkdata_o(17 downto 16);
    data_chk <= elinkdata_o(15 downto 0);
    valid_chk <= elinkdata_rdy_o;
    --        checker: process (clk240, rst_chk)
    checker: process (clk, rst_chk)
    begin
        if rst_chk = '1' then
            state_chk <= st_idl;
            count_chk <= (others => '0');
            err_chk <= '0';
        --          elsif clk240'event and clk240='1' then
        elsif clk'event and clk='1' then
            if valid_chk = '1' then
              sm_checker: case state_chk is
                    when st_idl =>
                        count_chk <= (others => '0');
                        err_chk <= '0';
                        --                  if data_chk = X"AA00" and datacode_chk = "10" then
                        if datacode_chk = "10" then
                            state_chk <= st_start;
                        end if;
                    when st_start =>
                        --                  if data_chk = X"0000" and datacode_chk = "01" then
                        if datacode_chk = "01" then
                            state_chk <= st_idl;
                            count_chk <= (others => '0');
                        else
                            state_chk <= st_count;
                            count_chk <= count_chk + 1;
                        end if;
                    when st_count =>
                        if datacode_chk = "01" then
                            state_chk <= st_idl;
                            count_chk <= (others => '0');
                        elsif datacode_chk = "00" then
                            state_chk <= st_count;
                            count_chk <= count_chk + 1;
                        else
                            state_chk <= st_idl;
                            err_chk <= '1';
                            count_chk <= (others => '0');
                        end if;
                end case sm_checker;
            end if;
        end if;
    end process;

    ------------------------------------------------------------
    -- EPATH_FIFO
    ------------------------------------------------------------
    UEF_IN : entity work.upstreamEpathFifoWrap
        port map(
            bitCLK    => '0',
            rst      => elink_tx_rst,
            fifoFLUSH  => fifo_flush,
            --    clk      => clk240,
            clk      => clk,
            ---
            wr_en    => elinkdata_rdy_o,
            din      => elinkdata_o,
            ---
            rd_en    => elink_data_re_final,--elink_data_re,
            dout    => EDATA_OUT,
            doutRdy    => efifoDoutRdy,
            ---
            full    => efifoFull,
            empty    => efifoEmpty,
            prog_full  => efifoPfull
        );
    elink_data_re_final <= elink_data_re_aurora  when (emu_control.data_format = "10") else
                           elink_data_re;


    --MT count data in the fifo
    --        fifochk: process (clk240, elink_tx_rst, fifo_flush)
    fifochk: process (clk, elink_tx_rst, fifo_flush)
    begin
        if elink_tx_rst = '1' or fifo_flush = '1' then
            count_fifochk <= (others => '0');
        --          elsif clk240'event and clk240='1' then
        elsif clk'event and clk='1' then
            if elinkdata_rdy_o = '1' and elink_data_re_final = '1' then
                count_fifochk <= count_fifochk; --+1 -1
            elsif elinkdata_rdy_o = '1' and elink_data_re_final = '0' then
                count_fifochk <= count_fifochk + 1; --+1
            elsif elinkdata_rdy_o = '0' and elink_data_re_final = '1' and count_fifochk /= X"0000" then
                count_fifochk <= count_fifochk - 1; ---1
            else
                count_fifochk <= count_fifochk; --0
            end if;
        end if;
    end process;
    --

    --MT
    --MT checker upstream fifo
    data_upstfifochk <= EDATA_OUT;
    valid_upstfifochk <= efifoDoutRdy;
    --        checker_upstfifo: process (clk240, elink_tx_rst, fifo_flush)
    checker_upstfifo: process (clk, elink_tx_rst, fifo_flush)
    begin
        if elink_tx_rst = '1' or fifo_flush = '1' then
            count_upstfifochk <= (others => '0');
            err_upstfifochk <= '0';
        --          elsif clk240'event and clk240='1' then
        elsif clk'event and clk='1' then
            if valid_upstfifochk = '1' then
              fifo_checker: case state_upstfifochk is
                    when st_idl =>
                        count_upstfifochk <= (others => '0');
                        err_upstfifochk <= '0';
                        if data_upstfifochk(9 downto 8) = "10" then
                            state_upstfifochk <= st_start;
                        end if;
                    when st_start =>
                        if data_upstfifochk(9 downto 8) = "01" then
                            state_upstfifochk <= st_idl;
                            count_upstfifochk <= (others => '0');
                        else
                            state_upstfifochk <= st_count;
                            count_upstfifochk <= count_upstfifochk + 1;
                        end if;
                    when st_count =>
                        if data_upstfifochk(9 downto 8) = "01" then
                            state_upstfifochk <= st_idl;
                            count_upstfifochk <= (others => '0');
                        elsif data_upstfifochk(9 downto 8) = "00" then
                            state_upstfifochk <= st_count;
                            count_upstfifochk <= count_upstfifochk + 1;
                        else
                            state_upstfifochk <= st_idl;
                            err_upstfifochk <= '1';
                            count_upstfifochk <= (others => '0');
                        end if;
                end case fifo_checker;
            end if;
        end if;
    end process;

    --  elink_data_out_pre <= efifoDout_8b10b when (emu_control.data_format = "01") else EDATA_OUT;
    elink_data_out <= EDATA_OUT               when (emu_control.data_format = "00") else
                      efifoDout_8b10b         when (emu_control.data_format = "01") else
                      elink_data_out_aurora   when (emu_control.data_format = "10");

    aurora_en <= '1' when (emu_control.data_format = "10") else '0';

    enc8b10bx : entity work.enc8b10_wrap
        port map (
            --    clk        => clk240,
            clk        => clk,
            rst        => elink_tx_rst,
            dataCode    => EDATA_OUT(9 downto 8), -- 00"data, 01"eop, 10"sop, 11"comma
            dataIN      => EDATA_OUT(7 downto 0),
            dataINrdy    => efifoDoutRdy,
            encDataOut    => enc10bit,
            encDataOutrdy  => enc10bitRdy
        );

    --  efifoDout_8b10b <= enc10bit(9) & enc10bit(8) & enc10bit(7) & enc10bit(6) & enc10bit(5) & enc10bit(4) & enc10bit(3) & enc10bit(2) & enc10bit(1) & enc10bit(0);
    --  efifoDout_8b10b <= enc10bit(0) & enc10bit(1) & enc10bit(2) & enc10bit(3) & enc10bit(4) & enc10bit(5) & enc10bit(6) & enc10bit(7) & enc10bit(8) & enc10bit(9);

    -- Swap MSB LSB added SS
    efifoDout_8b10b <= enc10bit(0) & enc10bit(1) & enc10bit(2) & enc10bit(3) & enc10bit(4) & enc10bit(5) & enc10bit(6) & enc10bit(7) & enc10bit(8) & enc10bit(9);
    --when (fhCR_REVERSE_10B = '0') -- LSB First when '0' controlled by register CR_REVERSE_10B.FROMHOST --SS
    --else                          -- MSB First when '1'
    --enc10bit(9) & enc10bit(8) & enc10bit(7) & enc10bit(6) & enc10bit(5) & enc10bit(4) & enc10bit(3) & enc10bit(2) & enc10bit(1) & enc10bit(0);

    --MT checker 2
    --SOP/EOP=K28.1/K28.6=3c/dc (centralrouter_package) are encoded as in https://en.wikipedia.org/wiki/8b/10b_encoding
    --SOP/EOP after the encoding can assume two numbers, depending on
    --current parity (estimated from previous packet)
    rst_chk2 <= emu_control.reset;
    data_chk2 <= efifoDout_8b10b;
    valid_chk2 <= enc10bitRdy;
    --isEOP_chk2 <= '0';
    --        checker2: process (clk240, rst_chk2)
    --! FS: From centralRouter_package.vhd: let's use thes instead of X"0F9" etc. X"0F9" represents a 12-bit number,
    --! FS: officcially the comparisons should always be false. Vivado seems to swallow it though
    --! FS: constant EOCp       : std_logic_vector (9 downto 0) := "0011110110"; -- -K.28.6
    --! FS: constant EOCn       : std_logic_vector (9 downto 0) := "1100001001"; -- +K.28.6
    --! FS: constant SOCp       : std_logic_vector (9 downto 0) := "0011111001"; -- -K.28.1
    --! FS: constant SOCn       : std_logic_vector (9 downto 0) := "1100000110"; -- +K.28.1
    checker2: process (clk, rst_chk2)
    begin
        if rst_chk2 = '1' then
            state_chk2 <= st_idl;
            count_chk2 <= (others => '0');
            err_chk2 <= '0';
        --          elsif clk240'event and clk240='1' then
        elsif clk'event and clk='1' then
            if valid_chk2 = '1' then
              sm_checker2: case state_chk2 is
                    when st_idl =>
                        count_chk2 <= (others => '0');
                        err_chk2 <= '0';
                        isEOP_chk2 <= '0';
                        if data_chk2 = SOCn or data_chk2 = SOCp then  --SOP
                            state_chk2 <= st_start;
                        end if;
                    when st_start =>
                        if data_chk2 = EOCp or data_chk2 = EOCn then  --EOP
                            state_chk2 <= st_idl;
                            count_chk2 <= (others => '0');
                            isEOP_chk2 <= '1';
                        else
                            state_chk2 <= st_count;
                            count_chk2 <= count_chk2 + 1;
                            isEOP_chk2 <= '0';
                        end if;
                    when st_count =>
                        if data_chk2 = EOCp or data_chk2 = EOCn then  --EOP
                            state_chk2 <= st_idl;
                            count_chk2 <= (others => '0');
                            isEOP_chk2 <= '1';
                        else
                            state_chk2 <= st_count;
                            count_chk2 <= count_chk2 + 1;
                            isEOP_chk2 <= '0';
                        --else
                        --  state_chk2 <= st_idl;
                        --  err_chk2 <= '1';
                        --  count_chk2 <= (others => '0');
                        --  isEOP_chk2 <= '0';
                        end if;
                end case sm_checker2;
            end if;
        end if;
    end process;
    flag_data_gen <= isEOP_chk2; --RL

    rst0: entity work.CRresetManager
        port map (
            --    clk40           => clk240,
            clk40           => clk,
            rst             => emu_control.reset,
            cr_rst          => elink_tx_rst,
            cr_fifo_flush   => fifo_flush
        );

    --RL 64b66b
    --    aurora_wrapper_FELIG: entity work.aurora_wrapper_FELIG
    --  port map (
    --    clk           => clk,
    --    rst           => elink_tx_rst,
    --    read_en_in    => elink_data_re,
    --    count_fifochk => count_fifochk,
    --    data_in       => EDATA_OUT,
    --    --output_width  => emu_control.output_width,
    --    read_en_out   => elink_data_re_aurora,
    --    data_out      => elink_data_out_aurora_b
    --    );

    elink_data_out_aurora <= elink_data_out_aurora_b(0) & elink_data_out_aurora_b(1)
                             & elink_data_out_aurora_b(2) & elink_data_out_aurora_b(3)
                             & elink_data_out_aurora_b(4) & elink_data_out_aurora_b(5)
                             & elink_data_out_aurora_b(6) & elink_data_out_aurora_b(7)
                             & elink_data_out_aurora_b(8) & elink_data_out_aurora_b(9);-- when (fhCR_REVERSE_10B = '1')
--                      else elink_data_out_aurora_b(9) & elink_data_out_aurora_b(8)
--                         & elink_data_out_aurora_b(7) & elink_data_out_aurora_b(6)
--                         & elink_data_out_aurora_b(5) & elink_data_out_aurora_b(4)
--                         & elink_data_out_aurora_b(3) & elink_data_out_aurora_b(2)
--                         & elink_data_out_aurora_b(1) & elink_data_out_aurora_b(0);

end Behavioral;


