--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Ricardo Luz
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-- v2 by Ricardo Luz.
-- Based on elink_printer.vhd initially written by Michael Oberlingand and later modified by Marco Trovato.
-- Complies with 32-b width and MSB first for all widths.

--changes log:
--2021/07/15 - added aurora support

LIBRARY IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.STD_LOGIC_ARITH.ALL;
    USE IEEE.STD_LOGIC_UNSIGNED.ALL;
    USE ieee.numeric_std.ALL;

    use work.type_lib.ALL;
    use work.ip_lib.ALL;

library UNISIM;
    use UNISIM.VComponents.all;

entity elink_printer_bit_feeder_v2 is
    port (
        clk                : in  std_logic;
        enable            : in  std_logic;                    -- enable output, read_enable
        elink_endian_mode    : in  std_logic;                    -- bitwise endianness: '0' little endian (8b10b), '1' big endian.
        elink_output_width    : in  std_logic_vector(2 downto 0); -- "000","001","010","011", "100" = 2,4,8,16,32-bit elink width
        elink_input_width    : in  std_logic;                -- '0' = 8-bit input, '1' = 10 bit-input
        word_in            : in  std_logic_vector(9 downto 0); -- from upstreamEpathFifoWrap
        bit_stream_en      : in  std_logic_vector(3 downto 0); -- enable for (0): 2b/4b (1): 8b (2): 16b (3): 32b                                                                          --
        bit_stream_sync      : in  std_logic;                    -- sync by zeroing the counter
        LINKSconfig           : in  std_logic_vector(2 downto 0);
        gbt_word_latch        : in  std_logic;
        MSBfirst              : in  std_logic;
        aurora                : in  std_logic;
        read_enable          : out std_logic;
        bit_stream_out        : out std_logic_vector(31 downto 0)
    );
end elink_printer_bit_feeder_v2;

architecture Behavioral of elink_printer_bit_feeder_v2 is
    signal shift_en_q      : std_logic            := '0';
    signal shift_en_d      : std_logic            := '0';
    signal read_enable_buf    : std_logic            := '0';
    signal read_enable_pr      : std_logic            := '0';
    signal read_enable_d    : std_logic            := '0';
    signal read_enable_dd    : std_logic            := '0';
    signal read_enable_ddd    : std_logic            := '0';
    signal read_enable_dddd    : std_logic            := '0';
    signal shift_mode      : std_logic            := '0';
    signal input_width      : std_logic            := '0';
    signal shift_op        : std_logic_vector(2 downto 0)  := (others => '0');
    signal output_width      : std_logic_vector(2 downto 0)  := (others => '0');
    signal cycle_count_q    : std_logic_vector(2 downto 0)  := (others => '0');
    signal cycle_count_d    : std_logic_vector(2 downto 0)  := (others => '0');

    signal reg_160              : std_logic_vector(159 downto 0) := (others => '0');
    signal reg_160_8b10b        : std_logic_vector(159 downto 0) := (others => '0');
    signal reg_160_direc        : std_logic_vector(159 downto 0) := (others => '0');

    signal word_in_d          : std_logic_vector(9 downto 0)  := (others => '0');
    signal word_test          : std_logic_vector(9 downto 0)  := (others => '0');
    signal word_test_h          : std_logic_vector(9 downto 0)  := (others => '0');
    signal word_test_l          : std_logic_vector(9 downto 0)  := (others => '0');
    signal flag                 : std_logic := '0';
    signal flag_d               : std_logic := '0';
    signal wr_to_reg            : std_logic := '0';
    signal wr_to_reg_2b         : std_logic := '0';
    signal wr_to_reg_final      : std_logic := '0';
    signal wr_to_reg_final_d    : std_logic := '0';
    signal count                : integer range 0 to 15;
    signal count_max            : integer range 0 to 15;
    signal count_to_five        : std_logic_vector(2 downto 0) := (others => '0');
    signal bit_stream_zero      : std_logic_vector(31 downto 0)  := (others => '0');
    signal bit_stream           : std_logic_vector(31 downto 0);
    signal bit_stream_t         : std_logic_vector(31 downto 0);
    signal flag_count           : std_logic := '0';
    signal lpgbt                 : std_logic := '0';

begin
    shift_en_d    <= bit_stream_en(0) when (output_width = "000") else  -- Mux input 0:  2 bit output, use 1 clock wide enable
                     bit_stream_en(0) when (output_width = "001") else  -- Mux input 1:  4 bit output, use 1 clock wide enable
                     bit_stream_en(1) when (output_width = "010") else  -- Mux input 2:  8 bit output, use 2 clock wide enable
                     bit_stream_en(2) when (output_width = "011") else  -- Mux input 3: 16 bit output, use 4 clock wide enable
                     bit_stream_en(3) when (output_width = "100") else  -- Mux input 4: 32 bit output, use 6/8 clock wide enable
                     '0';

    cycle_count_d  <=  "001" when (shift_mode = '0' and input_width = '0' and cycle_count_q = "000") else  -- 2b. direct
                      "010" when (shift_mode = '0' and input_width = '0' and cycle_count_q = "001") else  -- 2b. direct
                      "011" when (shift_mode = '0' and input_width = '0' and cycle_count_q = "010") else  -- 2b. direct
                      "001" when (shift_mode = '1' and input_width = '0' and cycle_count_q = "000") else  -- all others. direct
                      "001" when (                     input_width = '1' and cycle_count_q = "000") else  -- all sizes. 8b10b
                      "010" when (                     input_width = '1' and cycle_count_q = "001") else  -- all sizes. 8b10b
                      "011" when (                     input_width = '1' and cycle_count_q = "010") else  -- all sizes. 8b10b
                      "111" when (                     input_width = '1' and cycle_count_q = "011") else  -- all sizes. 8b10b
                      "000";

    shift_op      <=  "010" when (shift_mode = '0' and cycle_count_q(1 downto 0) = "01") else
                     "010" when (shift_mode = '0' and cycle_count_q(1 downto 0) = "10") else
                     "010" when (shift_mode = '0' and cycle_count_q(1 downto 0) = "11") else
                     
                     "011" when (shift_mode = '1' and cycle_count_q(1 downto 0) = "01") else
                     "001" when (shift_mode = '1' and cycle_count_q(1 downto 0) = "10") else
                     "011" when (shift_mode = '1' and cycle_count_q(1 downto 0) = "11") else
                     "000";

    lpgbt <= LINKSconfig(2); --0 is gbt, 1 is lpgbt

    read_enable_dddd <= '1' when ((enable = '1' ) and (shift_en_q = '1') and (shift_op(1) = '0')) else '0';
    read_enable_buf <= read_enable_dd when lpgbt = '0' else
                       read_enable_pr when lpgbt = '1';
    read_enable <= read_enable_buf;

    reg_input : process(clk)
    begin
        if clk'event and clk ='1' then
            read_enable_pr    <= read_enable_d;
            read_enable_d      <= read_enable_dd;
            read_enable_dd    <= read_enable_ddd;
            read_enable_ddd    <= read_enable_dddd;

            shift_en_q <= shift_en_d;

            if (bit_stream_sync = '1') then
                cycle_count_q <= (others => '0');
            elsif (shift_en_q = '1') then
                cycle_count_q <= cycle_count_d;
            end if;
        end if;
    end process reg_input;

    mode_reg : process(clk)
    begin
        if clk'event and clk ='1' then
            output_width <= elink_output_width;
            input_width <= elink_input_width;
            if (elink_output_width =  "000") then
                shift_mode <= '0';
            else
                shift_mode <= '1';
            end if;
        end if;
    end process mode_reg;

    flag <= '1' when shift_op = "000" and input_width = '1' and aurora = '0' else
            '1' when shift_op = "001" and input_width = '1' and aurora = '0' else
            read_enable_buf when input_width = '0' and aurora = '0' else
            read_enable_buf when aurora = '1' else
            '0';

    --  2*5 =  10  1 word
    --  4*5 =  20  2 words
    --  8*5 =  40  4 words
    -- 16*5 =  80  8 words
    -- 32*5 = 160 16 words

    count_max <=  0 when output_width = "000" else -- 2b
                 1 when output_width = "001" else -- 4b
                 3 when output_width = "010" else -- 8b
                 7 when output_width = "011" else --16b
                 15 when output_width = "100" else --32b
                 0;

    wr_to_reg <= '1' when flag = '1' and flag_d = '0' and output_width /= "010" else
                 '1' when flag = '0' and flag_d = '1' and output_width = "010" else
                 '0';

    wr_to_reg_final <= wr_to_reg_2b when output_width = "000" else
                       wr_to_reg;

    output_reg : process (clk)
    begin
        if clk'event and clk ='1' then
            flag_d <= flag;
            if MSBfirst = '0' then
                word_in_d <= word_in;
            else
                for i in 0 to 9 loop
                    word_in_d(9-i) <= word_in(i);
                end loop;
            end if;
            wr_to_reg_final_d <= wr_to_reg_final;
            if flag = '0' and flag_d = '1' and (output_width = "010") and input_width = '1' then
                word_test <= word_in_d;
            elsif flag = '1' and flag_d = '0' and (output_width = "000" or output_width = "001" or output_width = "011" or output_width = "100") and input_width = '1' then
                word_test <= word_in_d;
            end if;
            if wr_to_reg_final_d = '1' then
                reg_160_8b10b((count+1)*10 - 1 downto count*10) <= word_test;
                reg_160_direc((count+1)*8  - 1 downto count*8 ) <= word_test(7 downto 0);
                if flag_count <= '0' then
                    if count = count_max then
                        count <= 0;
                    else
                        count <= count + 1;
                    end if;
                else --reseting after a bit_stream_sync
                    if output_width = "000" then --2b
                        flag_count <= '0';
                    elsif output_width = "001" and ((count_to_five = "001" and lpgbt = '0') or (count_to_five = "000" and lpgbt = '1')) then -- 4b
                        count <= 0;
                        flag_count <= '0';
                    elsif output_width = "010" and ((count_to_five = "001" and lpgbt = '0') or (count_to_five = "000" and lpgbt = '1')) then --8b
                        count <= 2;
                        flag_count <= '0';
                    elsif output_width = "011" and ((count_to_five = "011" and lpgbt = '0') or (count_to_five = "010" and lpgbt = '1')) then --16b
                        count <= 7;
                        flag_count <= '0';
                    elsif output_width = "100" and count_to_five = "001" and lpgbt = '1' then --32b only lpgbt
                        count <= 7;
                        flag_count <= '0';
                    end if;
                end if;
            end if;

            if gbt_word_latch = '1' then
                bit_stream <= bit_stream_t;
                if (count_to_five = "100" and input_width = '1') or (count_to_five = "011" and input_width = '0') then
                    count_to_five <= "000";
                    wr_to_reg_2b <= '1';
                else
                    count_to_five <= count_to_five + "001"; -- can be moved to the module above.
                    wr_to_reg_2b <= '0';
                end if;
            else
                wr_to_reg_2b <= '0';
            end if;

            if (bit_stream_sync = '1') then
                flag_count <= '1';
                reg_160_8b10b <= (others => '0');
                reg_160_direc <= (others => '0');
                if input_width = '1' then
                    count_to_five <= "100";
                else
                    count_to_five <= "011";
                end if;
            end if;
        end if;
    end process output_reg;

    reg_160 <= reg_160_8b10b when input_width = '1' else reg_160_direc;

    bit_stream_t<=bit_stream_zero(29 downto 0) & reg_160(  1 downto   0) when output_width = "000" and count_to_five= "000" else -- 2b elink
                   bit_stream_zero(29 downto 0) & reg_160(  3 downto   2) when output_width = "000" and count_to_five= "001" else
                   bit_stream_zero(29 downto 0) & reg_160(  5 downto   4) when output_width = "000" and count_to_five= "010" else
                   bit_stream_zero(29 downto 0) & reg_160(  7 downto   6) when output_width = "000" and count_to_five= "011" else
                   bit_stream_zero(29 downto 0) & reg_160(  9 downto   8) when output_width = "000" and count_to_five= "100" else
                   bit_stream_zero(27 downto 0) & reg_160(  3 downto   0) when output_width = "001" and count_to_five= "000" else -- 4b elink
                   bit_stream_zero(27 downto 0) & reg_160(  7 downto   4) when output_width = "001" and count_to_five= "001" else
                   bit_stream_zero(27 downto 0) & reg_160( 11 downto   8) when output_width = "001" and count_to_five= "010" else
                   bit_stream_zero(27 downto 0) & reg_160( 15 downto  12) when output_width = "001" and count_to_five= "011" else
                   bit_stream_zero(27 downto 0) & reg_160( 19 downto  16) when output_width = "001" and count_to_five= "100" else
                   bit_stream_zero(23 downto 0) & reg_160(  7 downto   0) when output_width = "010" and count_to_five= "000" else -- 8b elink
                   bit_stream_zero(23 downto 0) & reg_160( 15 downto   8) when output_width = "010" and count_to_five= "001" else
                   bit_stream_zero(23 downto 0) & reg_160( 23 downto  16) when output_width = "010" and count_to_five= "010" else
                   bit_stream_zero(23 downto 0) & reg_160( 31 downto  24) when output_width = "010" and count_to_five= "011" else
                   bit_stream_zero(23 downto 0) & reg_160( 39 downto  32) when output_width = "010" and count_to_five= "100" else
                   bit_stream_zero(15 downto 0) & reg_160( 15 downto   0) when output_width = "011" and count_to_five= "000" else --16b elink
                   bit_stream_zero(15 downto 0) & reg_160( 31 downto  16) when output_width = "011" and count_to_five= "001" else
                   bit_stream_zero(15 downto 0) & reg_160( 47 downto  32) when output_width = "011" and count_to_five= "010" else
                   bit_stream_zero(15 downto 0) & reg_160( 63 downto  48) when output_width = "011" and count_to_five= "011" else
                   bit_stream_zero(15 downto 0) & reg_160( 79 downto  64) when output_width = "011" and count_to_five= "100" else
                   reg_160( 31 downto   0) when output_width = "100" and count_to_five= "000" else --32b elink
                   reg_160( 63 downto  32) when output_width = "100" and count_to_five= "001" else
                   reg_160( 95 downto  64) when output_width = "100" and count_to_five= "010" else
                   reg_160(127 downto  96) when output_width = "100" and count_to_five= "011" else
                   reg_160(159 downto 128) when output_width = "100" and count_to_five= "100" else
                   bit_stream_zero;

    --add endianess
    bit_stream_out <= bit_stream
                      when MSBfirst = '0' and enable = '1' else
                      bit_stream(29 downto 0)
                      & bit_stream(0) & bit_stream(1)
                      when MSBfirst = '1' and output_width = "000" and enable = '1' else
                      bit_stream_zero(27 downto 0)
                      & bit_stream(0) & bit_stream(1) & bit_stream(2) & bit_stream(3)
                      when MSBfirst = '1' and output_width = "001" and enable = '1' else
                      bit_stream_zero(23 downto 0)
                      & bit_stream(0) & bit_stream(1) & bit_stream(2)  & bit_stream(3)  & bit_stream(4)  & bit_stream(5)  & bit_stream(6)  & bit_stream(7)
                      when MSBfirst = '1' and output_width = "010" and enable = '1' else
                      bit_stream_zero(15 downto 0)
                      & bit_stream(0) & bit_stream(1) & bit_stream(2)  & bit_stream(3)  & bit_stream(4)  & bit_stream(5)  & bit_stream(6)  & bit_stream(7)
                      & bit_stream(8) & bit_stream(9) & bit_stream(10) & bit_stream(11) & bit_stream(12) & bit_stream(13) & bit_stream(14) & bit_stream(15)
                      when MSBfirst = '1' and output_width = "011" and enable = '1' else
                      bit_stream(0)  & bit_stream(1)  & bit_stream(2)  & bit_stream(3)  & bit_stream(4)  & bit_stream(5)  & bit_stream(6)  & bit_stream(7)
                      & bit_stream(8)  & bit_stream(9)  & bit_stream(10) & bit_stream(11) & bit_stream(12) & bit_stream(13) & bit_stream(14) & bit_stream(15)
                      & bit_stream(16) & bit_stream(17) & bit_stream(18) & bit_stream(19) & bit_stream(20) & bit_stream(21) & bit_stream(22) & bit_stream(23)
                      & bit_stream(24) & bit_stream(25) & bit_stream(26) & bit_stream(27) & bit_stream(28) & bit_stream(29) & bit_stream(30) & bit_stream(31)
                      when MSBfirst = '1' and output_width = "100" and enable = '1' else
                      bit_stream_zero;
end Behavioral;
