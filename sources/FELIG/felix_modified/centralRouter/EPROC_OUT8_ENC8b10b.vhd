--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	(modified by) Michael Oberling
--
-- Design Name:	EPROC_OUT8_ENC8b10b_lightweight
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Modification of EPROC_OUT8_ENC8b10b for FELIG
--
-- Change Log:	V1.0 - 
--				V1.1 - 20180705 - Updated with latest FELIX base.
--
--==============================================================================

-- Original File Header --
----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Modify Data:    02/20/2018 by Israel Grayzman
--! Create Date:    05/19/2014 
--! Module Name:    EPROC_OUT8_ENC8b10b
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use work.centralRouter_package.all;

--! 8b10b encoder for EPROC_OUT8 module
entity EPROC_OUT8_ENC8b10b_lightweight is
port(
    clk240    : in  std_logic;
    rst         : in  std_logic; 
    edataIN     : in  std_logic_vector (9 downto 0);
    edataINrdy  : in  std_logic;
    EdataOUT    : out std_logic_vector(9 downto 0) -- ready on every bitCLK
    );
end EPROC_OUT8_ENC8b10b_lightweight;

architecture Behavioral of EPROC_OUT8_ENC8b10b_lightweight is
----------------------------------
component enc8b10_wrap 
port ( 	
	clk            : in  std_logic;
	rst            : in  std_logic;
	dataCode       : in  std_logic_vector (1 downto 0); -- 00"data, 01"eop, 10"sop, 11"comma
	dataIN         : in  std_logic_vector (7 downto 0);
	dataINrdy      : in  std_logic;
	encDataOut     : out  std_logic_vector (9 downto 0);
	encDataOutrdy  : out  std_logic
	);
end component enc8b10_wrap;
----------------------------------


signal enc10bit : std_logic_vector (9 downto 0);
signal enc10bitRdy : std_logic;

begin

-------------------------------------------------------------------------------------------
-- 8b10b encoding
-------------------------------------------------------------------------------------------
enc8b10bx: enc8b10_wrap 
port map ( 	
	clk            => clk240,
	rst            => rst,
	dataCode       => edataIN(9 downto 8), -- 00"data, 01"eop, 10"sop, 11"comma
	dataIN         => edataIN(7 downto 0),
	dataINrdy      => edataINrdy, -- one? CLKx4 after inp_request_trig_out
	encDataOut     => enc10bit,
	encDataOutrdy  => enc10bitRdy
	);

	EdataOUT <= enc10bit(0) & enc10bit(1) & enc10bit(2) & enc10bit(3) & enc10bit(4) & enc10bit(5) & enc10bit(6) & enc10bit(7) & enc10bit(8) & enc10bit(9);

end Behavioral;

