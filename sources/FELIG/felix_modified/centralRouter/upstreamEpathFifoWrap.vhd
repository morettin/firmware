--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Shelfali Saxena
--!               Ricardo Luz
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:  (modified by) Michael Oberling
--
-- Design Name:  upstreamEpathFifoWrap_lightweight
-- Version:    1.2
-- Date:    9/18/2017
--
-- Description:  Modification of upstreamEpathFifoWrap for FELIG
--
-- Change Log:  V1.0 -  Replaced FIFO with BuiltIN CommonClock type to save on
--            resources.
--        V1.1 - 20180705 - Updated with latest FELIX base.
--        V1.2 - 20181001 - Connected emptyEfifo
--
--==============================================================================

-- Original File Header --
----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    23/06/2015
--! Module Name:    upstreamEpathFifoWrap
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee,work;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    --use work.all;

    use work.ip_lib.ALL;

--! EPATH FIFO 18 bit wide, 2K deep
entity upstreamEpathFifoWrap is
    port (
        bitCLK          : in std_logic; --IG: add clock 40 to extract ready signal in 40 MHz
        rst             : in std_logic;
        fifoFLUSH       : in std_logic;
        clk       : in std_logic;
        ---
        wr_en           : in std_logic;
        din             : in std_logic_vector(17 downto 0);
        ---
        rd_en           : in std_logic;
        dout            : out std_logic_vector(9 downto 0);
        doutRdy         : out std_logic;
        ---
        full            : out std_logic;
        empty           : out std_logic;
        prog_full       : out std_logic;
        --added SS
        ila_rd_en_s     : out std_logic;
        ila_din_r       : out std_logic_vector(17 downto 0);
        ila_dout18bit   : out std_logic_vector(17 downto 0);
        ila_dout18bit_valid : out std_logic;
        ila_byte0       : out std_logic_vector(9 downto 0);
        ila_byte1       : out std_logic_vector(9 downto 0);
        ila_rd_en_pipe_0 : out std_logic;
        ila_rd_en_pipe_1 : out std_logic
    );
end upstreamEpathFifoWrap;

architecture Behavioral of upstreamEpathFifoWrap is


    signal rd_en_s, empty_efifo, prog_full_s : std_logic := '0';
    signal OE, rst_state, byte_cnt,byte_mux_sel,byte_rdy,rd_en1,rd_en2 : std_logic := '0';
    signal dout18bit : std_logic_vector(17 downto 0) := (others => '0');
    signal dout18bit_valid :std_logic := '0'; --IG: set with data out
    signal byte0, byte1 : std_logic_vector(9 downto 0) := "1100000000";
    constant comma_byte : std_logic_vector(9 downto 0) := "1100000000";

    signal byte0_code,byte1_code,word16_code : std_logic_vector(1 downto 0) := (others => '0');
    signal empty_efifo1 : std_logic := '0';

    signal rd_en_pipe : std_logic_vector(1 downto 0) := "00";
    signal wr_en_r  : std_logic := '0';
    signal din_r    : std_logic_vector(17 downto 0) := "110000000000000000";

    signal empty_efifo_array : std_logic_vector(2 downto 0) := (others => '0'); --IG: in order to initiate the "end of message" Kchar and the Comma afterwards, a valid signal need to set 2 more times after an empty flag raised

    signal DoutReady    : std_logic := '0'; --IG: add sample according to the additional output register in the FIFO
    signal DoutReady2   : std_logic := '0'; --IG: second sample of DoutReady

begin

    ila_rd_en_s     <= rd_en_s;
    ila_din_r       <= din_r;
    ila_dout18bit   <= dout18bit;
    ila_dout18bit_valid <= dout18bit_valid;
    ila_byte0       <= byte0;
    ila_byte1       <= byte1;
    ila_rd_en_pipe_0 <= rd_en_pipe(0);
    ila_rd_en_pipe_1 <= rd_en_pipe(0);

    -------------------------------------------------------------------------------------------
    -- write pipeline
    -------------------------------------------------------------------------------------------
    process(clk)
    begin
        if clk'event and clk = '1' then
            wr_en_r   <= wr_en;
            din_r     <= din;
        end if;
    end process;
    --
    -------------------------------------------------------------------------------------------
    -- FIFO - ip
    -------------------------------------------------------------------------------------------
    epathFIFO: epath_fifo
        port map(
            clk         => clk,
            --MT only synch reset supported
            --   rst         => fifoFLUSH,
            srst         => fifoFLUSH,
            din         => din_r,
            wr_en       => wr_en_r,
            rd_en       => rd_en_s,
            dout        => dout18bit, --18 bit
            valid       => dout18bit_valid, --IG: set with the data out
            full        => full,
            empty       => empty_efifo, --IG: transfer the post processing to the Epath units       --IG empty_efifo,
            prog_full   => prog_full_s -- 1008/960 from 1024
        );

    process(clk,rst)
    begin
        if rst = '1' then
            byte_cnt <= '0';
        elsif clk'event and clk = '1' then
            if ((rd_en = '1') and (empty_efifo = '0')) then -- 1 clk trigger  --IG: toggle the byte_cnt only when the FIFO is not empty
                byte_cnt <= not byte_cnt;
            end if;
        end if;
    end process;
    --
    rd_en_s <= rd_en and (not byte_cnt) and (not empty_efifo); -- only when byte_cnt = 0

    --
    word16_code <= dout18bit(17 downto 16);
    --
    process(word16_code,empty_efifo1)
    begin
        if empty_efifo1 = '1' then
            byte0_code <= "11";
            byte1_code <= "11";
        else
            if word16_code = "10" then -- start of message
                byte0_code <= "11";
                byte1_code <= "10";
            elsif word16_code = "01" then -- end of message
                byte0_code <= "01";
                byte1_code <= "11";
            else -- "00" data
                byte0_code <= "00";
                byte1_code <= "00";
            end if;
        end if;
    end process;

    byte0 <= byte0_code & dout18bit(15 downto 8);
    byte1 <= byte1_code & dout18bit(7 downto 0);

    process(rst, clk)
    begin
        if (rst = '1') then
            dout    <= (others => '0');
        elsif rising_edge(clk) then
            rd_en_pipe (0) <= rd_en; --SS: as the epathfifo has latency of 1, rd_en needs to be delayed by one.
            --rd_en_pipe (1) <= rd_en_pipe(0);    --SS
            doutRdy <= rd_en_pipe (0);--SS: doutRdy <= rd_en_pipe (1);
            if (dout18bit_valid = '1') then --IG: the data delay in 1 clock due to FIFO's output register
                empty_efifo1 <= empty_efifo;
                dout  <= byte0;
            elsif (rd_en_pipe (0) = '1') then -- SS: rd_en_pipe (1) = '1'
                empty_efifo1 <= empty_efifo;
                dout  <= byte1;
            end if;
        end if;
    end process;
    --
    empty <= empty_efifo;-- clk domain
    --
    process(clk)
    begin
        if clk'event and clk = '1' then
            rst_state   <= rst or fifoFLUSH;
            OE          <= not rst_state;
        end if;
    end process;
    --
    prog_full   <= prog_full_s and OE;

end Behavioral;

