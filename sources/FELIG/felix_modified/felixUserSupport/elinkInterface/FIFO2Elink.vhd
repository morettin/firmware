--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	(modified by) Michael Oberling
--
-- Design Name:	FIFO2Elink_lightweight
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Modification of FIFO2Elink for FELIG
--
-- Change Log:	V1.0 - 
--
--==============================================================================

-- Original File Header --
----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    17/08/2015 
--! Module Name:    FIFO2Elink
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library ieee, work;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use work.all;

--! consists of 1 E-path
entity FIFO2Elink_lightweight is
port ( 
    clk240      : in  std_logic;
    rst         : in  std_logic;
    fifo_flush  : in  std_logic;
    ------   
    efifoDin    : in  std_logic_vector (17 downto 0);   -- [data_code,2bit][data,16bit]
    efifoWe     : in  std_logic;
    efifoPfull  : out std_logic;
    ------   
    efifoDout   : out  std_logic_vector (9 downto 0);   -- [data_code,2bit][data,16bit]
    efifoRe     : in  std_logic;
	elinkEncoding : in std_logic
    );
end FIFO2Elink_lightweight;

architecture Behavioral of FIFO2Elink_lightweight is

----
signal getDataTrig, doutRdy : std_logic;
signal EDATA_OUT : std_logic_vector(9 downto 0); 
signal dout2bit  : std_logic_vector(1 downto 0); 
signal bitCount1,dout2bit_r : std_logic := '0';
signal dout4bit, dout4bit_r : std_logic_vector(3 downto 0); 
signal dout8bit, dout8bit_r : std_logic_vector(7 downto 0); 
signal bitCount2 : std_logic_vector(1 downto 0) := "00";
signal bitCount3 : std_logic_vector(2 downto 0) := "000";
signal efifoDout_8b10b: std_logic_vector(9 downto 0); 
----

begin


------------------------------------------------------------
-- EPATH_FIFO
------------------------------------------------------------
UEF_IN : entity work.upstreamEpathFifoWrap
port map(
    bitCLK			=> '0',
    rst 	        => rst,
    fifoFLUSH       => fifo_flush,
    clk 	=> clk240,
    ---
    wr_en 	=> efifoWe,
    din     => efifoDin,
    ---
    rd_en 	=> efifoRe, 
    dout 	=> EDATA_OUT,
    doutRdy => open,
    ---
    full        => open,
    empty       => open,
    prog_full   => efifoPfull
    );
--

	efifoDout <= efifoDout_8b10b when (elinkEncoding = '1') else EDATA_OUT;

ENC8b10b_case: entity work.EPROC_OUT8_ENC8b10b_lightweight
port map(
	clk240    => clk240,
    rst         => rst, 
    edataIN     => EDATA_OUT,
    edataINrdy  => efifoRe,
    EdataOUT    => efifoDout_8b10b
    );

end Behavioral;


