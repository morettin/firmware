--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	(modified by) Michael Oberling
--
-- Design Name:	housekeeping_module
-- Version:		1.0
-- Date:		7/5/2017
--
-- Description:	Updated port map of dna to match latest FELIX base.
--
-- Change Log:	V1.0 - Changed dna_out from 64 to 96 bits to match latest FELIX base.
--
--==============================================================================

-- Original File Header --
-- (none)




library ieee, UNISIM, work;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_1164.all;
use work.pcie_package.all;
use work.I2C.all;

entity housekeeping_module is
  generic(
    CARD_TYPE               : integer := 710;
    OPTO_TRX                : integer := 2;
    GBT_MAPPING             : integer := 0; -- GBT mapping: 0 NORMAL CXP1 -> GBT1-12 | 1 ALTERNATE CXP1 -> GBT 1-4,9-12,17-20
    GBT_NUM                 : integer := 24;
    TTC_test_mode           : boolean := false;
    generateTTCemu          : boolean := false;
    GENERATE_GBT            : boolean := true;
    crInternalLoopbackMode  : boolean := false;
    wideMode                : boolean := false;
    DEBUG_MODE              : boolean := false;
    AUTOMATIC_CLOCK_SWITCH  : boolean := false;
    includeToHoEproc8s      : boolean := true;
    includeToHoEproc16      : boolean := false;
    includeToHoEproc4s      : boolean := true;
    includeToHoEproc2s      : boolean := true;
    includeFrHoEproc8s      : boolean := true;
    includeFrHoEproc4s      : boolean := true;
    includeFrHoEproc2s      : boolean := true;
    FIRMWARE_MODE           : integer := 0;
    generate_IC_EC_TTC_only : boolean := false;
    GTHREFCLK_SEL           : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
    USE_Si5324_RefCLK       : boolean := false);
  port (
    LMK_locked                  : in     std_logic_vector(0 downto 0);
    MMCM_Locked_in              : in     std_logic;
    MMCM_OscSelect_in           : in     std_logic;
    SCL                         : inout  std_logic;
    SDA                         : inout  std_logic;
    SI5345_A                    : out    std_logic_vector(1 downto 0);
    SI5345_INSEL                : out    std_logic_vector(1 downto 0);
    SI5345_OE                   : out    std_logic;
    SI5345_RSTN                 : out    std_logic;
    SI5345_SEL                  : out    std_logic;
    SI5345_nLOL                 : in     std_logic;
    appreg_clk                  : in     std_logic;
    emcclk                      : in     std_logic;
    flash_SEL                   : out    std_logic;
    flash_a                     : out    std_logic_vector(24 downto 0);
    flash_a_msb                 : inout  std_logic_vector(1 downto 0);
    flash_adv                   : out    std_logic;
    flash_cclk                  : out    std_logic;
    flash_ce                    : out    std_logic;
    flash_d                     : inout  std_logic_vector(15 downto 0);
    flash_re                    : out    std_logic;
    flash_we                    : out    std_logic;
    i2cmux_rst                  : out    std_logic;
    leds                        : out    std_logic_vector(7 downto 0);
    opto_inhibit                : out    std_logic_vector(OPTO_TRX-1 downto 0);
    opto_los                    : in     std_logic_vector(OPTO_TRX-1 downto 0);
    opto_los_net                : out    std_logic_vector(OPTO_TRX-1 downto 0);
    register_map_control        : in     register_map_control_type;
    register_map_gen_board_info : out    register_map_gen_board_info_type;
    register_map_hk_monitor     : out    register_map_hk_monitor_type;
    rst_soft                    : in     std_logic;
    sys_reset_n                 : in     std_logic);
end entity housekeeping_module;


architecture structure of housekeeping_module is

  signal RST                            : std_logic;
  signal nReset                         : std_logic;
  signal clk                            : std_logic;
  signal cmd_ack                        : std_logic;
  signal ack_out                        : std_logic;
  signal Dout                           : std_logic_vector(7 downto 0);
  signal Din                            : std_logic_vector(7 downto 0);
  signal ack_in                         : std_logic;
  signal write                          : std_logic;
  signal read                           : std_logic;
  signal stop                           : std_logic;
  signal start                          : std_logic;
  signal ena                            : std_logic;
  signal reset                          : std_logic;
  signal AUTOMATIC_CLOCK_SWITCH_ENABLED : std_logic_vector(0 downto 0);

  component i2c_interface
    port (
      Din                  : out    std_logic_vector(7 downto 0);
      Dout                 : in     std_logic_vector(7 downto 0);
      I2C_RD               : out    bitfield_i2c_rd_r_type;
      I2C_WR               : out    bitfield_i2c_wr_r_type;
      RST                  : in     std_logic;
      ack_in               : out    std_logic;
      ack_out              : in     std_logic;
      appreg_clk           : in     std_logic;
      clk                  : out    std_logic;
      cmd_ack              : in     std_logic;
      ena                  : out    std_logic;
      nReset               : out    std_logic;
      read                 : out    std_logic;
      register_map_control : in     register_map_control_type;
      rst_soft             : in     std_logic;
      start                : out    std_logic;
      stop                 : out    std_logic;
      write                : out    std_logic);
  end component i2c_interface;

  component GenericConstantsToRegs
    generic(
      GBT_NUM                 : integer := 24;
      GBT_MAPPING             : integer := 0; -- GBT mapping: 0 NORMAL CXP1 -> GBT1-12 | 1 ALTERNATE CXP1 -> GBT 1-4,9-12,17-20
      OPTO_TRX                : integer := 2;
      crInternalLoopbackMode  : boolean := false;
      wideMode                : boolean := false;
      DEBUG_MODE              : boolean := false;
      GENERATE_GBT            : boolean := true;
      TTC_test_mode           : boolean := false;
      generateTTCemu          : boolean := false;
      AUTOMATIC_CLOCK_SWITCH  : boolean := false;
      includeToHoEproc8s      : boolean := true;
      includeToHoEproc16      : boolean := false;
      includeToHoEproc4s      : boolean := true;
      includeToHoEproc2s      : boolean := true;
      includeFrHoEproc8s      : boolean := true;
      includeFrHoEproc4s      : boolean := true;
      includeFrHoEproc2s      : boolean := true;
      FIRMWARE_MODE           : integer := 0;
      generate_IC_EC_TTC_only : boolean := false;
      GTHREFCLK_SEL           : std_logic := '0'; -- GREFCLK: '1', MGTREFCLK: '0'
      USE_Si5324_RefCLK       : boolean := false);
    port (
      AUTOMATIC_CLOCK_SWITCH_ENABLED : out    std_logic_vector(0 downto 0);
      register_map_gen_board_info    : out    register_map_gen_board_info_type);
  end component GenericConstantsToRegs;

  component card_type_specific_ios
    generic(
      CARD_TYPE : integer := 710;
      OPTO_TRX  : integer := 2);
    port (
      rst_hw       : in     std_logic;
      rst_soft     : in     std_logic;
      i2cmux_rst   : out    std_logic;
      opto_inhibit : out    std_logic_vector(OPTO_TRX-1 downto 0);
      opto_los_s   : out    std_logic_vector(OPTO_TRX-1 downto 0);
      opto_los     : in     std_logic_vector(OPTO_TRX-1 downto 0));
  end component card_type_specific_ios;

  component xadc_drp
    generic(
      CARD_TYPE : integer := 711);
    port (
      clk40   : in     std_logic;
      reset   : in     std_logic;
      temp    : out    std_logic_vector(11 downto 0);
      vccint  : out    std_logic_vector(11 downto 0);
      vccaux  : out    std_logic_vector(11 downto 0);
      vccbram : out    std_logic_vector(11 downto 0));
  end component xadc_drp;

  component dna
    generic(
      CARD_TYPE : integer := 710);
    port (
      clk40   : in     std_logic;
      reset   : in     std_logic;
      dna_out : out    std_logic_vector(95 downto 0));
  end component dna;

  component flash_wrapper
    port (
      rst                  : in     std_logic;
      clk                  : in     std_logic;
      flash_a              : out    std_logic_vector(24 downto 0);
      flash_d              : inout  std_logic_vector(15 downto 0);
      flash_adv            : out    std_logic;
      flash_cclk           : out    std_logic;
      flash_ce             : out    std_logic;
      flash_we             : out    std_logic;
      flash_re             : out    std_logic;
      flash_SEL            : out    std_logic;
      register_map_control : in     register_map_control_type;
      config_flash_rd      : out    bitfield_config_flash_rd_r_type;
      flash_a_msb          : inout  std_logic_vector(1 downto 0));
  end component flash_wrapper;

begin
  leds <= register_map_control.STATUS_LEDS;
  register_map_hk_monitor.LMK_LOCKED <= LMK_locked;

  i2c0: simple_i2c
    port map(
      clk     => clk,
      ena     => ena,
      nReset  => nReset,
      clk_cnt => "01100100",
      start   => start,
      stop    => stop,
      read    => read,
      write   => write,
      ack_in  => ack_in,
      Din     => Din,
      cmd_ack => cmd_ack,
      ack_out => ack_out,
      Dout    => Dout,
      SCL     => SCL,
      SDA     => SDA);

  i2cint0: i2c_interface
    port map(
      Din                  => Din,
      Dout                 => Dout,
      I2C_RD               => register_map_hk_monitor.I2C_RD,
      I2C_WR               => register_map_hk_monitor.I2C_WR,
      RST                  => RST,
      ack_in               => ack_in,
      ack_out              => ack_out,
      appreg_clk           => appreg_clk,
      clk                  => clk,
      cmd_ack              => cmd_ack,
      ena                  => ena,
      nReset               => nReset,
      read                 => read,
      register_map_control => register_map_control,
      rst_soft             => rst_soft,
      start                => start,
      stop                 => stop,
      write                => write);

  const0: GenericConstantsToRegs
    generic map(
      GBT_NUM                 => GBT_NUM,
      GBT_MAPPING             => GBT_MAPPING,
      OPTO_TRX                => OPTO_TRX,
      crInternalLoopbackMode  => crInternalLoopbackMode,
      wideMode                => wideMode,
      DEBUG_MODE              => DEBUG_MODE,
      GENERATE_GBT            => GENERATE_GBT,
      TTC_test_mode           => TTC_test_mode,
      generateTTCemu          => generateTTCemu,
      AUTOMATIC_CLOCK_SWITCH  => AUTOMATIC_CLOCK_SWITCH,
      includeToHoEproc8s      => includeToHoEproc8s,
      includeToHoEproc16      => includeToHoEproc16,
      includeToHoEproc4s      => includeToHoEproc4s,
      includeToHoEproc2s      => includeToHoEproc2s,
      includeFrHoEproc8s      => includeFrHoEproc8s,
      includeFrHoEproc4s      => includeFrHoEproc4s,
      includeFrHoEproc2s      => includeFrHoEproc2s,
      FIRMWARE_MODE           => FIRMWARE_MODE,
      generate_IC_EC_TTC_only => generate_IC_EC_TTC_only,
      GTHREFCLK_SEL           => GTHREFCLK_SEL,
      USE_Si5324_RefCLK       => false)
    port map(
      AUTOMATIC_CLOCK_SWITCH_ENABLED => AUTOMATIC_CLOCK_SWITCH_ENABLED,
      register_map_gen_board_info    => register_map_gen_board_info);

  u0: card_type_specific_ios
    generic map(
      CARD_TYPE => CARD_TYPE,
      OPTO_TRX  => OPTO_TRX)
    port map(
      rst_hw       => RST,
      rst_soft     => rst_soft,
      i2cmux_rst   => i2cmux_rst,
      opto_inhibit => opto_inhibit,
      opto_los_s   => opto_los_net,
      opto_los     => opto_los);

  xadc0: xadc_drp
    generic map(
      CARD_TYPE => CARD_TYPE)
    port map(
      clk40   => appreg_clk,
      reset   => reset,
      temp    => register_map_hk_monitor.FPGA_CORE_TEMP,
      vccint  => register_map_hk_monitor.FPGA_CORE_VCCINT,
      vccaux  => register_map_hk_monitor.FPGA_CORE_VCCAUX,
      vccbram => register_map_hk_monitor.FPGA_CORE_VCCBRAM);

  dna0: dna
    generic map(
      CARD_TYPE => CARD_TYPE)
    port map(
      clk40   => appreg_clk,
      reset   => RST,
      dna_out(register_map_hk_monitor.FPGA_DNA'range) => register_map_hk_monitor.FPGA_DNA,
      dna_out(95 downto register_map_hk_monitor.FPGA_DNA'length) => open);

  u1: flash_wrapper
    port map(
      rst                  => RST,
      clk                  => emcclk,
      flash_a              => flash_a,
      flash_d              => flash_d,
      flash_adv            => flash_adv,
      flash_cclk           => flash_cclk,
      flash_ce             => flash_ce,
      flash_we             => flash_we,
      flash_re             => flash_re,
      flash_SEL            => flash_SEL,
      register_map_control => register_map_control,
      config_flash_rd      => register_map_hk_monitor.CONFIG_FLASH_RD,
      flash_a_msb          => flash_a_msb);
  reset <= rst_soft or RST;

  RST <= not sys_reset_n;

  --  type bitfield_hk_ctrl_fmc_type is record
  --    SI5345_INSEL                   : std_logic_vector(6 downto 5);    -- Selects the input clock source
  --    SI5345_A                       : std_logic_vector(4 downto 3);    -- Si5345 address select
  --    SI5345_OE                      : std_logic_vector(2 downto 2);    -- Si5345 output enable
  --    SI5345_RSTN                    : std_logic_vector(1 downto 1);    -- Si5345 reset,
  --    SI5345_SEL                     : std_logic_vector(0 downto 0);    -- Si5345 i2c select bit
  --  end record;

  SI5345_INSEL(1 downto 0) <= register_map_control.HK_CTRL_FMC.SI5345_INSEL(6 downto 5);
  SI5345_A(1 downto 0)     <= register_map_control.HK_CTRL_FMC.SI5345_A(4 downto 3);
  SI5345_OE                <= register_map_control.HK_CTRL_FMC.SI5345_OE(2);
  SI5345_RSTN              <= register_map_control.HK_CTRL_FMC.SI5345_RSTN(1);
  SI5345_SEL               <= register_map_control.HK_CTRL_FMC.SI5345_SEL(0);

  register_map_hk_monitor.MMCM_MAIN.PLL_LOCK <= (others => MMCM_Locked_in);


  register_map_hk_monitor.HK_CTRL_FMC.SI5345_LOL <= (others=> (not SI5345_nLOL));
end architecture structure ; -- of housekeeping_module

