--==============================================================================
--
-- Argonne National Laboratory
-- High Energy Physics
-- Electronics Group
--
-- Engineer:	(modified by) Michael Oberling
--
-- Design Name:	FELIX_gbt_wrapper
-- Version:		1.0
-- Date:		9/13/2017
--
-- Description:	Modified to use 240 Mhz clock with read enable (gbt_tx_flag), and
--				l1a_trigger_in as an enable soruce.
-- 					
--
-- Change Log:	V1.0	
--
--==============================================================================

-- Original File Header --
----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.  
--! Engineer: juna
--! 
--! Create Date:    07/07/2014 
--! Module Name:    GBTdataEmulator
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library work, IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.pcie_package.all;
use work.centralRouter_package.all;

use work.ip_lib.ALL;

--! E-link data emulator
entity GBTdataEmulator is
Generic ( 
    EMU_DIRECTION       : string := "ToHost";  -- ToHost or ToFrontEnd
    useGBTdataEmulator  : boolean := true;
    wideMode            : boolean := false
    );
Port (
    rdclk                   : in  std_logic;
    wrclk                   : in  std_logic;
	gbt_tx_flag				: in  std_logic;
	l1a_trigger_in			: in  std_logic;
    rst_hw                  : in  std_logic;
    rst_soft                : in  std_logic;
    xoff                    : in  std_logic;
    register_map_control    : in  register_map_control_type;
    ---------
    GBTdata                 : out std_logic_vector(119 downto 0);
    l1a_trigger_lat_o       : out std_logic;
    GBTlinkValid            : out std_logic    
    );
end GBTdataEmulator;

architecture Behavioral of GBTdataEmulator is

COMPONENT emuram
  PORT (
    clka : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(111 DOWNTO 0);
    clkb : IN STD_LOGIC;
    addrb : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(111 DOWNTO 0)
  );
END COMPONENT emuram;

------------------------------------
------------------------------------
--component emuram_0
--port (
--    clka    : in  std_logic;
--    wea     : in  std_logic_vector(0 downto 0);
--    addra   : in  std_logic_vector(9 downto 0);
--    dina    : in  std_logic_vector(15 downto 0);
--    douta   : out std_logic_vector(15 downto 0);
--    clkb    : in  std_logic;
--    web     : in  std_logic_vector(0 downto 0);
--    addrb   : in  std_logic_vector(9 downto 0);
--    dinb    : in  std_logic_vector(15 downto 0);
--    doutb   : out std_logic_vector(15 downto 0)
--    );
--end component emuram_0;
------------------------------------
------------------------------------
--component emuram_1
--port (
--    clka    : in  std_logic;
--    wea     : in  std_logic_vector(0 downto 0);
--    addra   : in  std_logic_vector(9 downto 0);
--    dina    : in  std_logic_vector(15 downto 0);
--    douta   : out std_logic_vector(15 downto 0);
--    clkb    : in  std_logic;
--    web     : in  std_logic_vector(0 downto 0);
--    addrb   : in  std_logic_vector(9 downto 0);
--    dinb    : in  std_logic_vector(15 downto 0);
--    doutb   : out std_logic_vector(15 downto 0)
--    );
--end component emuram_1;
------------------------------------
------------------------------------
--component emuram_2
--port (
--    clka    : in  std_logic;
--    wea     : in  std_logic_vector(0 downto 0);
--    addra   : in  std_logic_vector(9 downto 0);
--    dina    : in  std_logic_vector(15 downto 0);
--    douta   : out std_logic_vector(15 downto 0);
--    clkb    : in  std_logic;
--    web     : in  std_logic_vector(0 downto 0);
--    addrb   : in  std_logic_vector(9 downto 0);
--    dinb    : in  std_logic_vector(15 downto 0);
--    doutb   : out std_logic_vector(15 downto 0)
--    );
--end component emuram_2;
------------------------------------
------------------------------------
--component emuram_3
--port (
--    clka    : in  std_logic;
--    wea     : in  std_logic_vector(0 downto 0);
--    addra   : in  std_logic_vector(9 downto 0);
--    dina    : in  std_logic_vector(15 downto 0);
--    douta   : out std_logic_vector(15 downto 0);
--    clkb    : in  std_logic;
--    web     : in  std_logic_vector(0 downto 0);
--    addrb   : in  std_logic_vector(9 downto 0);
--    dinb    : in  std_logic_vector(15 downto 0);
--    doutb   : out std_logic_vector(15 downto 0)
--    );
--end component emuram_3;
------------------------------------
------------------------------------
--component emuram_4
--port (
--    clka    : in  std_logic;
--    wea     : in  std_logic_vector(0 downto 0);
--    addra   : in  std_logic_vector(9 downto 0);
--    dina    : in  std_logic_vector(15 downto 0);
--    douta   : out std_logic_vector(15 downto 0);
--    clkb    : in  std_logic;
--    web     : in  std_logic_vector(0 downto 0);
--    addrb   : in  std_logic_vector(9 downto 0);
--    dinb    : in  std_logic_vector(15 downto 0);
--    doutb   : out std_logic_vector(15 downto 0)
--    );
--end component emuram_4;
------------------------------------
------------------------------------
--component emuram_5
--port (
--    clka    : in  std_logic;
--    wea     : in  std_logic_vector(0 downto 0);
--    addra   : in  std_logic_vector(9 downto 0);
--    dina    : in  std_logic_vector(15 downto 0);
--    douta   : out std_logic_vector(15 downto 0);
--    clkb    : in  std_logic;
--    web     : in  std_logic_vector(0 downto 0);
--    addrb   : in  std_logic_vector(9 downto 0);
--    dinb    : in  std_logic_vector(15 downto 0);
--    doutb   : out std_logic_vector(15 downto 0)
--    );
--end component emuram_5;
------------------------------------
------------------------------------
--component emuram_6
--port (
--    clka    : in  std_logic;
--    wea     : in  std_logic_vector(0 downto 0);
--    addra   : in  std_logic_vector(9 downto 0);
--    dina    : in  std_logic_vector(15 downto 0);
--    douta   : out std_logic_vector(15 downto 0);
--    clkb    : in  std_logic;
--    web     : in  std_logic_vector(0 downto 0);
--    addrb   : in  std_logic_vector(9 downto 0);
--    dinb    : in  std_logic_vector(15 downto 0);
--    doutb   : out std_logic_vector(15 downto 0)
--    );
--end component emuram_6;
------------------------------------
----------------------------------

signal emuram_wraddr : std_logic_vector(13 downto 0) := (others => '0'); 
signal ram_rdaddr, ram_wraddr : std_logic_vector(9 downto 0) := (others => '0');
signal RESET    : std_logic := '1';
--signal rst_fall : std_logic;
signal ena      : std_logic := '0';
signal l1a_trigger_lat      : std_logic := '0';
signal emuram_wrdata : std_logic_vector(15 downto 0) := (others=>'0');

signal GBTdata_s : std_logic_vector(119 downto 0);

signal wea : std_logic_vector(13 downto 0) :=(others=>'0');

constant zeros16bit : std_logic_vector(15 downto 0) := (others=>'0');
constant zeros32bit : std_logic_vector(31 downto 0) := (others=>'0');
constant addr_max   : std_logic_vector(9 downto 0) := "1111111110"; -- 1024 (5 x 1638)

-- add by Israel Grayzman to be eqvivalent to the CR reset scheme
--constant fifoFLUSHcount_max : std_logic_vector (7 downto 0) := "10000000";
constant commonRSTcount_max : std_logic_vector (7 downto 0) := (others=>'1');
--IG signal cr_rst_rr : std_logic := '1';
signal rstTimerCount : std_logic_vector (7 downto 0) := (others=>'0');

begin
--
disableEmulator: if useGBTdataEmulator = false generate
GBTdata_s     <= (others=>'0');
GBTlinkValid  <= '0';
end generate disableEmulator;
--

--
enableEmulator: if useGBTdataEmulator = true generate

--****************************************************************************
-- add by Israel Grayzman to be eqvivalent to the CR reset scheme

rstTimerCounter: process(rdclk, rst_soft)
begin
    if (rst_soft = '1') then
        rstTimerCount <= (others=>'0');
    elsif rising_edge(rdclk) then
        -- MBO counter modified to be free running.
--      if rstTimerCount = commonRSTcount_max then -- stop counting
--          rstTimerCount <= rstTimerCount; -- freese counter
--      else
            rstTimerCount <= rstTimerCount + 1;
--      end if;
	end if;
end process;
--
cr_rst_out: process(rdclk,rst_soft)
begin
    if (rst_soft = '1') then
        RESET <= '1';
    elsif rising_edge(rdclk) then
        if rstTimerCount = commonRSTcount_max then
            RESET <= '0';
--      else
--         RESET <= RESET;
        end if;
	end if;
end process;
--************************************************************************

--IG -- global reset
--IG rst_fall_pulse: entity work.pulse_fall_pw01(behavioral) port map(rdclk, rst_soft, rst_fall);
--IG --
--IG RESET_latch: process(rdclk, rst_hw)
--IG begin
--IG     if rst_hw = '1' then
--IG         RESET <= '1'; 
--IG     elsif rdclk'event and rdclk = '1' then
--IG         if rst_soft = '1' then
--IG             RESET <= '1';
--IG         elsif rst_fall = '1' then
--IG             RESET <= '0';
--IG         end if;
--IG     end if;
--IG end process;

--RESET <= rst_hw or rst_soft;
l1a_trigger_lat_o <= l1a_trigger_lat;
l1a_trigger_latch : process(rdclk) -- IG: add RESET dependency
begin
	--MBO: this is a very crude first implementaion, for intial testing.
	-- The responce will walk in time depending on when the l1A trigger occurs,
	-- as there is no means by whic hto breakout of the 5 word "idle" cycle,
	-- other than reaching the end.
	--
	-- Also, there is no support for handling multiple l1a triggers.
	if rdclk'event and rdclk = '1' then
    	if (l1a_trigger_in = '1') then
    		l1a_trigger_lat <= '1';
    	elsif (ram_rdaddr = addr_max) then	
			l1a_trigger_lat <= '0';
		end if;
	end if;
end process;

-- data out enable
process(rdclk, RESET)
begin
    if RESET = '1' then
        ena <= '0';
    elsif rdclk'event and rdclk = '1' then
      if EMU_DIRECTION = "ToHost" then
        ena <= (to_sl(register_map_control.GBT_EMU_ENA.TOHOST) and (not xoff)) or l1a_trigger_lat;
      else
        ena <= (to_sl(register_map_control.GBT_EMU_ENA.TOFRONTEND) and (not xoff)) or l1a_trigger_lat;
      end if;
    end if;
end process;
--
--
process(wrclk,RESET)
begin
    if RESET = '1' then
        wea             <= (others=>'0');
        emuram_wraddr   <= (others=>'0');
        emuram_wrdata   <= (others=>'0');
    elsif wrclk'event and wrclk = '1' then
        wea(0) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(0); -- 
        wea(1) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(0); -- 
        wea(2) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(1); -- 
        wea(3) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(1); -- 
        wea(4) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(2); -- 
        wea(5) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(2); -- 
        wea(6) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(3); -- 
        wea(7) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(3); -- 
        wea(8) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(4); -- 
        wea(9) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(4); -- 
        wea(10) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(5); -- 
        wea(11) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(5); -- 
        wea(12) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(6); -- 
        wea(13) <= register_map_control.GBT_EMU_CONFIG_WE_ARRAY(6); -- 
        emuram_wraddr   <= register_map_control.GBT_EMU_CONFIG.WRADDR;   -- 14 bit (1 bit extra +  13 bits for address on BRAM)
        emuram_wrdata   <= register_map_control.GBT_EMU_CONFIG.WRDATA;   -- 16 bit data
	end if;
end process;
ram_wraddr <= emuram_wraddr(9 downto 0);

-- address counter
--IG address_counter: process(rdclk)
address_counter: process(RESET, rdclk) -- IG: add RESET dependency
begin
    if (RESET = '1') then
        ram_rdaddr <= (others => '0');
    elsif rdclk'event and rdclk = '1' then
    	-- MBO modified read address logic to use tx_flag as the "read enable".
    	if (gbt_tx_flag = '1') then
    		-- MBO modified counting logic to run the full ram when triggered.
    		-- This is done to support triggering with the l1a. 
			if ram_rdaddr = addr_max then
			    ram_rdaddr <= (others => '0');
			elsif (ena = '0') and (ram_rdaddr = "0000000010") then
				ram_rdaddr <= (others => '0'); 
			else
				ram_rdaddr <= ram_rdaddr + 1;
			end if;
		end if;
	end if;
end process;
--

--
emuram_0 : emuram
port map (
    clka    => wrclk,
    wea  => wea,
    addra   => ram_wraddr,
    dina    => emuram_wrdata,
    --
    clkb    => rdclk,
    addrb   => ram_rdaddr,
    doutb   => GBTdata_s(111 DOWNTO 0)
    );
--
--emuram_01 : emuram_1
--port map (
--    clka    => wrclk,
--    wea(0)  => wea(1),
--    addra   => ram_wraddr,
--    dina    => emuram_wrdata,
--    douta   => open,
--    --
--    clkb    => rdclk,
--    web     => web,
--    addrb   => ram_rdaddr,
--    dinb    => zeros16bit,
--    doutb   => GBTdata_s(31 downto 16)
--    );
----
--emuram_02 : emuram_2
--port map (
--    clka    => wrclk,
--    wea(0)  => wea(2),
--    addra   => ram_wraddr,
--    dina    => emuram_wrdata,
--    douta   => open,
--    --
--    clkb    => rdclk,
--    web     => web,
--    addrb   => ram_rdaddr,
--    dinb    => zeros16bit,
--    doutb   => GBTdata_s(47 downto 32)
--    );
----
--emuram_03 : emuram_3
--port map (
--    clka    => wrclk,
--    wea(0)  => wea(3),
--    addra   => ram_wraddr,
--    dina    => emuram_wrdata,
--    douta   => open,
--    --
--    clkb    => rdclk,
--    web     => web,
--    addrb   => ram_rdaddr,
--    dinb    => zeros16bit,
--    doutb   => GBTdata_s(63 downto 48)
--    );
----
--emuram_04 : emuram_4
--port map (
--    clka    => wrclk,
--    wea(0)  => wea(4),
--    addra   => ram_wraddr,
--    dina    => emuram_wrdata,
--    douta   => open,
--    --
--    clkb    => rdclk,
--    web     => web,
--    addrb   => ram_rdaddr,
--    dinb    => zeros16bit,
--    doutb   => GBTdata_s(79 downto 64)
--    );
----
--emuram_05 : emuram_5
--port map (
--    clka    => wrclk,
--    wea(0)  => wea(5),
--    addra   => ram_wraddr,
--    dina    => emuram_wrdata,
--    douta   => open,
--    --
--    clkb    => rdclk,
--    web     => web,
--    addrb   => ram_rdaddr,
--    dinb    => zeros16bit,
--    doutb   => GBTdata_s(95 downto 80)
--    );
----
--emuram_06 : emuram_6
--port map (
--    clka    => wrclk,
--    wea(0)  => wea(6),
--    addra   => ram_wraddr,
--    dina    => emuram_wrdata,
--    douta   => open,
--    --
--    clkb    => rdclk,
--    web     => web,
--    addrb   => ram_rdaddr,
--    dinb    => zeros16bit,
--    doutb   => GBTdata_s(111 downto 96)
--    );
----

GBTdata_s(119 downto 112) <= "0101" & "11" & "11"; --GBTdata_s(49 downto 48); --"00";
GBTlinkValid <= '1';

end generate enableEmulator;
--
--
isWideMode: if wideMode = true generate
GBTdata <= GBTdata_s;
end generate isWideMode;
--
isNormalMode: if wideMode = false generate
GBTdata <= GBTdata_s(119 downto 112) & GBTdata_s(79 downto 0) & zeros32bit;
end generate isNormalMode;
--

end Behavioral;

