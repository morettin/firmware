--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 10/30/2019 03:36:26 PM
-- Design Name:
-- Module Name: EndeavourDecoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AlignmentPulseGen is
    generic (
        MAX_VAL_DEALIGN: integer := 20480; --2048 bytes at 8b10b / 80 Mb/s elink.
        MAX_VAL_ALIGN: integer := 20 --2 bytes at 8b10b / 80 Mb/s elink.
    );
    port (
        clk40 : in std_logic; --BC clock
        AlignmentPulseAlign : out std_logic; --2 pulses to realign if FLAG found
        AlignmentPulseDeAlign : out std_logic --2 pulses to realign if FLAG found


    );
end AlignmentPulseGen;

architecture Behavioral of AlignmentPulseGen is
    signal cnt_dealign: integer range 0 to MAX_VAL_DEALIGN-1:= 0;
    signal cnt_align: integer range 0 to MAX_VAL_DEALIGN-1:= 0;

begin

    cnt_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            if cnt_align /= MAX_VAL_ALIGN-1 then
                cnt_align <= cnt_align + 1;
                AlignmentPulseAlign <= '0';
            else
                cnt_align <= 0;
                AlignmentPulseAlign <= '1';
            end if;
            if cnt_dealign /= MAX_VAL_DEALIGN-1 then
                cnt_dealign <= cnt_dealign + 1;
                AlignmentPulseDeAlign <= '0';
            else
                cnt_dealign <= 0;
                AlignmentPulseDeAlign <= '1';
            end if;
        end if;
    end process;


end Behavioral;
