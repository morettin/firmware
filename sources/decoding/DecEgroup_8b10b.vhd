--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Carsten Dülsen
--!               Frans Schreuder
--!               Ton Fleuren
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.ALL;
    use work.axi_stream_package.all;


entity DecEgroup_8b10b is
    generic (
        BLOCKSIZE     : integer := 1024;
        Support32bWidth     : std_logic := '1';
        Support16bWidth     : std_logic := '1';
        Support8bWidth      : std_logic := '1';
        IncludeElinks       : std_logic_vector(3 downto 0) := "1111";
        VERSAL : boolean    := false
    );
    port (
        clk40 : in std_logic; --BC clock for DataIn
        reset : in std_logic; --Acitve high reset

        DataIn : in std_logic_vector(31 downto 0);

        EnableIn : in std_logic_vector(3 downto 0); --Enable epath (register map)
        LinkAligned : in std_logic; --lpGBT link aligned
        ElinkWidth : in std_logic_vector(2 downto 0);  -- the 4 elink widths, 3 bit each: 32, 16, 8
        AlignmentPulseAlign : in std_logic; --2 pulses to realign if FLAG found
        AlignmentPulseDeAlign : in std_logic; --2 pulses to realign if FLAG found
        AutoRealign : in std_logic; --Realign 8b10b decoder in case of not-in-table k-character
        RealignmentEvent : out std_logic_vector(3 downto 0);
        PathEncoding : in std_logic_vector(15 downto 0);
        ElinkAligned : out std_logic_vector(3 downto 0);
        DecodingErrors : out std_logic_vector(31 downto 0); --counter for decoding errors for all e-links in the E-group
        MsbFirst : in std_logic;
        ReverseInputBits : in std_logic_vector(3 downto 0);
        HGTD_ALTIROC_DECODING: in std_logic;
        FE_BUSY_out : out std_logic_vector(3 downto 0); --BUSY request from front-end
        m_axis : out axis_32_array_type(0 to 3);  --FIFO read port (axi stream)
        m_axis_tready : in axis_tready_array_type(0 to 3); --FIFO read tready (axi stream)
        m_axis_aclk : in std_logic; --FIFO read clock (axi stream)
        m_axis_prog_empty : out axis_tready_array_type(0 to 3) --Indication that the FIFO contains a block.
    );
end DecEgroup_8b10b;

architecture struct of DecEgroup_8b10b is
    signal reset_d2    : std_logic;
    signal Enable       : std_logic_vector(3 downto 0);
    signal DataGB       : std_logic_vector(39 downto 0);
    signal DataGBValid  : std_logic_vector( 3 downto 0);

    signal DataGB_32b       : std_logic_vector(39 downto 0);
    signal DataGB_32bValid  : std_logic;
    signal ElinkWidth_32b   : std_logic_vector( 2 downto 0);
    --signal OutputWidth_32b  : std_logic_vector( 2 downto 0);

    signal DataGB_16b       : std_logic_vector(19 downto 0);
    signal DataGB_16bValid  : std_logic;
    signal ElinkWidth_16b   : std_logic_vector( 2 downto 0);
    --signal OutputWidth_16b  : std_logic_vector( 2 downto 0);

    signal DataGB_8b1       : std_logic_vector( 9 downto 0);
    signal DataGB_8b1Valid  : std_logic;
    signal ElinkWidth_8b    : std_logic_vector( 2 downto 0);
    --signal OutputWidth_8b1  : std_logic_vector( 2 downto 0);

    signal DataGB_8b2       : std_logic_vector( 9 downto 0);
    signal DataGB_8b2Valid  : std_logic;
    --signal OutputWidth_8b2  : std_logic_vector( 2 downto 0);

    signal DataGB_32bValid_p1 : std_logic;
    signal DataGB_8b1Valid_p1 : std_logic;
    signal DataGB_16bValid_p1 : std_logic;
    signal DataGB_8b2Valid_p1 : std_logic;

    signal Data         : std_logic_vector(31 downto 0);
    signal DataValid    : std_logic_vector( 3 downto 0);


    signal EOP          : std_logic_vector( 3 downto 0);
    signal ElinkSOB     : std_logic_vector( 3 downto 0);
    signal ElinkEOB     : std_logic_vector( 3 downto 0);
    signal Truncate     : std_logic_vector( 3 downto 0);
    signal Data_0 : std_logic_vector(31 downto 0);
    signal Data_1 : std_logic_vector(7 downto 0);
    signal Data_2 : std_logic_vector(15 downto 0);
    signal Data_3 : std_logic_vector(7 downto 0);
    signal EOP_0 : std_logic_vector(3 downto 0);
    signal EOP_1 : std_logic_vector(0 downto 0);
    signal EOP_2 : std_logic_vector(1 downto 0);
    signal EOP_3 : std_logic_vector(0 downto 0);
    signal SOB_0 : std_logic_vector(3 downto 0);
    signal SOB_1 : std_logic_vector(0 downto 0);
    signal SOB_2 : std_logic_vector(1 downto 0);
    signal SOB_3 : std_logic_vector(0 downto 0);
    signal EOB_0 : std_logic_vector(3 downto 0);
    signal EOB_1 : std_logic_vector(0 downto 0);
    signal EOB_2 : std_logic_vector(1 downto 0);
    signal EOB_3 : std_logic_vector(0 downto 0);
    signal DataValid_0 : std_logic_vector(3 downto 0);
    signal DataValid_1 : std_logic_vector(0 downto 0);
    signal DataValid_2 : std_logic_vector(1 downto 0);
    signal DataValid_3 : std_logic_vector(0 downto 0);
    signal DecoderBitSlip : std_logic_vector(3 downto 0);
    signal GearBoxBitSlip : std_logic_vector(3 downto 0);
    signal DecoderAligned : std_logic_vector(3 downto 0);
    signal ElinkAligned_s   : std_logic_vector(3 downto 0);
    signal CodingError : std_logic_vector(3 downto 0);
    signal DisparityError : std_logic_vector(3 downto 0); -- @suppress "signal DisparityError is never read"
    signal CodingError_0, CodingError_1, CodingError_2, CodingError_3: std_logic;
    signal ByteToAxiStreamGearboxValid : std_logic_vector(3 downto 0);



    signal DataOut8b10b         : std_logic_vector(31 downto 0);
    signal DataOutValid8b10b    : std_logic_vector(3 downto 0);
    signal EOP8b10b             : std_logic_vector(3 downto 0);
    signal ElinkSOB8b10b        : std_logic_vector(3 downto 0);
    signal ElinkEOB8b10b        : std_logic_vector(3 downto 0);
    signal DecoderAligned8b10b_in  : std_logic_vector(3 downto 0);
    signal DecoderAligned8b10b_out  : std_logic_vector(3 downto 0);
    signal CodingError8b10b     : std_logic_vector(3 downto 0);
    signal DisparityError8b10b  : std_logic_vector(3 downto 0);
    signal ISK_SOC_in           : std_logic_vector(3 downto 0);
    signal ISK_SOC_out          : std_logic_vector(3 downto 0);
    signal ISK_SOC_out_p1       : std_logic_vector(3 downto 0);
    signal ISK_EOC_in           : std_logic_vector(3 downto 0);
    signal ISK_EOC_out          : std_logic_vector(3 downto 0);
    signal ISK_EOC_out_p1       : std_logic_vector(3 downto 0);
    signal DecoderBitSlipIn     : std_logic_vector(3 downto 0);

    signal ISK_DelimiterA_in     : std_logic_vector(3 downto 0);
    signal ISK_DelimiterB_in     : std_logic_vector(3 downto 0);
    signal ISK_DelimiterA_out_p1 : std_logic_vector(3 downto 0);
    signal ISK_DelimiterA_out_p2 : std_logic_vector(3 downto 0);
    signal ISK_DelimiterB_out_p1 : std_logic_vector(3 downto 0);
    signal ISK_DelimiterA_out    : std_logic_vector(3 downto 0);
    signal ISK_DelimiterB_out    : std_logic_vector(3 downto 0);
    signal CharIsK_in            : std_logic_vector(3 downto 0);
    signal CharIsK_out           : std_logic_vector(3 downto 0);
    signal CharIsK_out_p1        : std_logic_vector(3 downto 0);
    signal dispout               : std_logic_vector(3 downto 0);
    signal dispout_p1            : std_logic_vector(3 downto 0);
    signal dispin                : std_logic_vector(3 downto 0);
    signal DecodingErrors_s      : std_logic_vector(31 downto 0);
    --attribute MARK_DEBUG : string;
    --attribute MARK_DEBUG of DataIn, DataGB, DataGBValid, Data_0, Data_1, Data_2, Data_3, CodingError8b10b, DataValid_0, DataValid_1, DataValid_2, DataValid_3, CodingError_0, CodingError_1, CodingError_2, CodingError_3: signal is "true";

    -- Ternary functions
    function ternary_i(cond : boolean; res_true, res_false : integer) return integer is
    begin
        if cond then return res_true;
        else      return res_false;
        end if;
    end function;
--(4 downto 0)
--function ternary_slv(cond : boolean; res_true, res_false : std_logic_vector) return std_logic_vector is
--begin
--   if cond then return res_true;
--   else      return res_false;
--   end if;
--end function;
begin
    ElinkWidth_32b <= ElinkWidth;

    ElinkWidth_8b  <= "010";

    ElinkWidth_16b <= "011" when ElinkWidth = "011" else
                      "010";

    --OutputWidth_32b <= "011" when ElinkWidth = "100" else
    --                   "010" when ElinkWidth = "011" else
    --                   "001";

    --OutputWidth_8b1 <= "001";

    --OutputWidth_16b <= "010" when ElinkWidth = "011" else
    --                   "001";

    --OutputWidth_8b2 <= "001";

    -- only use bitslip signal from first of each set of decoders, to prevent double-slipping when multiple decoders trigger a bitslip in short succession
    GearBoxBitSlip <= "000" & DecoderBitSlip(0) when ElinkWidth = "100" else
                      "0" & DecoderBitSlip(2) & "0" & DecoderBitSlip(0) when ElinkWidth = "011" else
                      DecoderBitSlip;

    DecoderBitSlipIn <= GearBoxBitSlip(0) & GearBoxBitSlip(0) & GearBoxBitSlip(0) & GearBoxBitSlip(0) when ElinkWidth = "100" else --Feed back gearbox slip to other decoders to reset their alignment
                        GearBoxBitSlip(2) & GearBoxBitSlip(2) & GearBoxBitSlip(0) & GearBoxBitSlip(0) when ElinkWidth = "011" else
                        GearBoxBitSlip;

    ISK_SOC_in(0) <= ISK_SOC_out_p1(1) when ElinkWidth = "011" else ISK_SOC_out_p1(1) or ISK_SOC_out_p1(2) or ISK_SOC_out_p1(3) when ElinkWidth = "100" else '0';
    ISK_SOC_in(1) <= ISK_SOC_out(0) when ElinkWidth = "011" else ISK_SOC_out(0) or ISK_SOC_out_p1(2) or ISK_SOC_out_p1(3) when ElinkWidth = "100" else '0';
    ISK_SOC_in(2) <= ISK_SOC_out_p1(3) when ElinkWidth = "011" else ISK_SOC_out(0) or ISK_SOC_out(1) or ISK_SOC_out_p1(3) when ElinkWidth = "100" else '0';
    ISK_SOC_in(3) <= ISK_SOC_out(2) when ElinkWidth = "011" else ISK_SOC_out(0) or ISK_SOC_out(1) or ISK_SOC_out(2) when ElinkWidth = "100" else '0';

    ISK_EOC_in(0) <= ISK_EOC_out_p1(1) when ElinkWidth = "011" else ISK_EOC_out_p1(1) or ISK_EOC_out_p1(2) or ISK_EOC_out_p1(3) when ElinkWidth = "100" else '0';
    ISK_EOC_in(1) <= ISK_EOC_out(0) when ElinkWidth = "011" else ISK_EOC_out(0) or ISK_EOC_out_p1(2) or ISK_EOC_out_p1(3) when ElinkWidth = "100" else '0';
    ISK_EOC_in(2) <= ISK_EOC_out_p1(3) when ElinkWidth = "011" else ISK_EOC_out(0) or ISK_EOC_out(1) or ISK_EOC_out_p1(3) when ElinkWidth = "100" else '0';
    ISK_EOC_in(3) <= ISK_EOC_out(2) when ElinkWidth = "011" else ISK_EOC_out(0) or ISK_EOC_out(1) or ISK_EOC_out(2) when ElinkWidth = "100" else '0';

    dispin(0) <= dispout_p1(1) when ElinkWidth = "011" else dispout_p1(3) when ElinkWidth = "100" else dispout_p1(0);
    dispin(1) <= dispout(0) when ElinkWidth = "011" else dispout(0) when ElinkWidth = "100" else dispout_p1(1);
    dispin(2) <= dispout_p1(3) when ElinkWidth = "011" else dispout(1) when ElinkWidth = "100" else dispout_p1(2);
    dispin(3) <= dispout(2) when ElinkWidth = "011" else dispout(2) when ElinkWidth = "100" else dispout_p1(3);

    DecoderLinkSharing_proc: process( clk40 )
    begin
        if rising_edge( clk40 ) then
            for i in 0 to 3 loop
                if DataGBValid(i) = '1' then
                    ISK_SOC_out_p1(i) <= ISK_SOC_out(i);
                    ISK_EOC_out_p1(i) <= ISK_EOC_out(i);
                    ISK_DelimiterB_out_p1(i) <= ISK_DelimiterB_out(i); -- the 16 and 8 bit decoders need 1 cycle delay on the second character on the (max. length 3) of the delimiter
                    ISK_DelimiterA_out_p2(i) <= ISK_DelimiterA_out_p1(i); -- the 8 bit decoder needs 2 cycles delay on the first character of the delimiter
                    ISK_DelimiterA_out_p1(i) <= ISK_DelimiterA_out(i); -- the 16 bit decoder needs 1 cycle delay on the first character of the delimiter
                    CharIsK_out_p1(i) <= CharIsK_out(i);
                    dispout_p1(i) <= dispout(i);
                end if;
            end loop;

            if ElinkWidth = "011" then    --16-bit E-Link, share SOC/EOC/comma among 2 decoders
                --               ISK_SOC_in(0) <= ISK_SOC_out(1);
                --               ISK_SOC_in(1) <= ISK_SOC_out(0);
                --               ISK_SOC_in(2) <= ISK_SOC_out(3);
                --               ISK_SOC_in(3) <= ISK_SOC_out(2);
                ISK_DelimiterA_in(0) <= ISK_DelimiterA_out_p2(0); -- first delimiter character (out of 3), decoders own output from 2 clockcycles ago
                ISK_DelimiterA_in(1) <= ISK_DelimiterA_out_p2(1);
                ISK_DelimiterA_in(2) <= ISK_DelimiterA_out_p2(2);
                ISK_DelimiterA_in(3) <= ISK_DelimiterA_out_p2(3);
                ISK_DelimiterB_in(0) <= ISK_DelimiterB_out_p1(1); -- second delimiter character shifted by 1 decoder, one cycle ago when wrapping around
                ISK_DelimiterB_in(1) <= ISK_DelimiterB_out(0);
                ISK_DelimiterB_in(2) <= ISK_DelimiterB_out_p1(3);
                ISK_DelimiterB_in(3) <= ISK_DelimiterB_out(2);
                -- last delimiter character not shared, signal is only used inside the decoders

                DecoderAligned8b10b_in(0) <= DecoderAligned8b10b_out(1);
                DecoderAligned8b10b_in(1) <= DecoderAligned8b10b_out(0);
                DecoderAligned8b10b_in(2) <= DecoderAligned8b10b_out(3);
                DecoderAligned8b10b_in(3) <= DecoderAligned8b10b_out(2);

                CharIsK_in(0) <= CharIsK_out_p1(1); -- needed for SOC detection
                CharIsK_in(1) <= CharIsK_out(0);
                CharIsK_in(2) <= CharIsK_out_p1(3);
                CharIsK_in(3) <= CharIsK_out(2);

            elsif ElinkWidth = "100" and Support32bWidth = '1' then --32-bit E-Link, share SOC/EOC/comma among 4 decoders
                --               ISK_SOC_in(0) <= ISK_SOC_out(1) or ISK_SOC_out(2) or ISK_SOC_out(3); -- SOC signal used for ReceivingState, not for alignment
                --               ISK_SOC_in(1) <= ISK_SOC_out(0) or ISK_SOC_out(2) or ISK_SOC_out(3);
                --               ISK_SOC_in(2) <= ISK_SOC_out(0) or ISK_SOC_out(1) or ISK_SOC_out(3);
                --               ISK_SOC_in(3) <= ISK_SOC_out(0) or ISK_SOC_out(1) or ISK_SOC_out(2);
                ISK_DelimiterA_in(0) <= ISK_DelimiterA_out_p1(2); -- first delimiter character (out of 3) shifted by 2 decoders
                ISK_DelimiterA_in(1) <= ISK_DelimiterA_out_p1(3); -- when wrapping around, signals are from previous clockcycle
                ISK_DelimiterA_in(2) <= ISK_DelimiterA_out(0);
                ISK_DelimiterA_in(3) <= ISK_DelimiterA_out(1);
                ISK_DelimiterB_in(0) <= ISK_DelimiterB_out_p1(3); -- second delimiter character shifted by 1 decoder
                ISK_DelimiterB_in(1) <= ISK_DelimiterB_out(0);
                ISK_DelimiterB_in(2) <= ISK_DelimiterB_out(1);
                ISK_DelimiterB_in(3) <= ISK_DelimiterB_out(2);

                DecoderAligned8b10b_in(0) <= DecoderAligned8b10b_out(1) or DecoderAligned8b10b_out(2) or DecoderAligned8b10b_out(3);
                DecoderAligned8b10b_in(1) <= DecoderAligned8b10b_out(0) or DecoderAligned8b10b_out(2) or DecoderAligned8b10b_out(3);
                DecoderAligned8b10b_in(2) <= DecoderAligned8b10b_out(0) or DecoderAligned8b10b_out(1) or DecoderAligned8b10b_out(3);
                DecoderAligned8b10b_in(3) <= DecoderAligned8b10b_out(0) or DecoderAligned8b10b_out(1) or DecoderAligned8b10b_out(2);

                CharIsK_in(0) <= CharIsK_out_p1(3);
                CharIsK_in(1) <= CharIsK_out(0);
                CharIsK_in(2) <= CharIsK_out(1);
                CharIsK_in(3) <= CharIsK_out(2);

            else --Single decoders for 8-bit E-Links, use no alignment sharing.
                --               ISK_SOC_in(0)   <= '0';
                --               ISK_SOC_in(1)   <= '0';
                --               ISK_SOC_in(2)   <= '0';
                --               ISK_SOC_in(3)   <= '0';
                ISK_DelimiterA_in(0) <= ISK_DelimiterA_out_p2(0); -- first delimiter character, output of same decoder 2 clockcycles ago
                ISK_DelimiterA_in(1) <= ISK_DelimiterA_out_p2(1);
                ISK_DelimiterA_in(2) <= ISK_DelimiterA_out_p2(2);
                ISK_DelimiterA_in(3) <= ISK_DelimiterA_out_p2(3);
                ISK_DelimiterB_in(0) <= ISK_DelimiterB_out_p1(0); -- second delimiter character, output of same decoder 1 cyclecyle ago
                ISK_DelimiterB_in(1) <= ISK_DelimiterB_out_p1(1);
                ISK_DelimiterB_in(2) <= ISK_DelimiterB_out_p1(2);
                ISK_DelimiterB_in(3) <= ISK_DelimiterB_out_p1(3);

                DecoderAligned8b10b_in(0) <= '0';
                DecoderAligned8b10b_in(1) <= '0';
                DecoderAligned8b10b_in(2) <= '0';
                DecoderAligned8b10b_in(3) <= '0';

                CharIsK_in(0) <= CharIsK_out_p1(0);
                CharIsK_in(1) <= CharIsK_out_p1(1);
                CharIsK_in(2) <= CharIsK_out_p1(2);
                CharIsK_in(3) <= CharIsK_out_p1(3);
            end if;
        end if;
    end process;

    g_32b_gearbox0: if IncludeElinks(0) = '1' generate
        i_DecodingGearBox_32b : entity work.DecodingGearBox
            generic map (
                MAX_INPUT        => ternary_i(Support32bWidth='1',32,ternary_i(Support16bWidth='1',16,8)),
                MAX_OUTPUT       => ternary_i(Support32bWidth='1',40,ternary_i(Support16bWidth='1', 20, 10)),
                -- 32, 16, 8, 4, 2
                SUPPORT_INPUT    => Support32bWidth&Support16bWidth&Support8bWidth& "00"
            )
            port map (
                Reset            => reset,
                clk40            => clk40,

                ELinkData        => DataIn(ternary_i(Support32bWidth='1',31,ternary_i(Support16bWidth='1', 15, 7)) downto 0),
                ElinkAligned     => LinkAligned,
                ElinkWidth       => ElinkWidth_32b,
                MsbFirst         => MsbFirst,
                ReverseInputBits => ReverseInputBits(0),

                DataOut          => DataGB_32b(ternary_i(Support32bWidth='1',39,ternary_i(Support16bWidth='1', 19, 9)) downto 0),
                DataOutValid     => DataGB_32bValid,

                BitSlip          => GearBoxBitSlip(0)
            );
    end generate;
    g_8b_gearbox1: if Support8bWidth = '1' and IncludeElinks(1) = '1' generate
        i_DecodingGearBox_8b1 : entity work.DecodingGearBox
            generic map (
                MAX_INPUT        => 8,
                MAX_OUTPUT       => 10,
                -- 32, 16, 8, 4, 2
                SUPPORT_INPUT    => "00100"
            )
            port map (
                Reset            => reset,
                clk40            => clk40,

                ELinkData        => DataIn(15 downto 8),
                ElinkAligned     => LinkAligned,
                ElinkWidth       => ElinkWidth_8b,
                MsbFirst         => MsbFirst,
                ReverseInputBits => ReverseInputBits(1),

                DataOut          => DataGB_8b1,
                DataOutValid     => DataGB_8b1Valid,

                BitSlip          => GearBoxBitSlip(1)
            );
    end generate g_8b_gearbox1;

    g_16b_gearbox: if (Support8bWidth = '1' or Support16bWidth = '1') and IncludeElinks(2) = '1' generate
        i_DecodingGearBox_16b : entity work.DecodingGearBox
            generic map (
                MAX_INPUT        => 16,
                MAX_OUTPUT       => 20,
                -- 32, 16, 8, 4, 2
                SUPPORT_INPUT    => "01100"
            )
            port map (
                Reset            => reset,
                clk40            => clk40,

                ELinkData        => DataIn(31 downto 16),
                ElinkAligned     => LinkAligned,
                ElinkWidth       => ElinkWidth_16b,
                MsbFirst         => MsbFirst,
                ReverseInputBits => ReverseInputBits(2),

                DataOut          => DataGB_16b,
                DataOutValid     => DataGB_16bValid,

                BitSlip          => GearBoxBitSlip(2)
            );
    end generate g_16b_gearbox;

    g_8b_gearbox2: if Support8bWidth = '1' and IncludeElinks(3) = '1' generate
        i_DecodingGearBox_8b2 : entity work.DecodingGearBox
            generic map (
                MAX_INPUT        => 8,
                MAX_OUTPUT       => 10,
                -- 32, 16, 8, 4, 2
                SUPPORT_INPUT    => "00100"
            )
            port map (
                Reset            => reset,
                clk40            => clk40,

                ELinkData        => DataIn(31 downto 24),
                ElinkAligned     => LinkAligned,
                ElinkWidth       => ElinkWidth_8b,
                MsbFirst         => MsbFirst,
                ReverseInputBits => ReverseInputBits(3),

                DataOut          => DataGB_8b2,
                DataOutValid     => DataGB_8b2Valid,

                BitSlip          => GearBoxBitSlip(3)
            );
    end generate g_8b_gearbox2;

    DataGB( 9 downto  0) <= DataGB_32b(39 downto 30) when ElinkWidth = "100" and MsbFirst = '1' else
                            DataGB_32b(19 downto 10) when ElinkWidth = "011" and MsbFirst = '1' else
                            DataGB_32b(9 downto 0)   when ElinkWidth = "010" and MsbFirst = '1' else
                            DataGB_32b(9 downto 0)   when ElinkWidth = "100" and MsbFirst = '0' else
                            DataGB_16b(9 downto 0)   when ElinkWidth = "011" and MsbFirst = '0' else
                            DataGB_8b2               when ElinkWidth = "010" and MsbFirst = '0' else
                            "0000000000";

    DataGB(19 downto 10) <= DataGB_32b(29 downto 20) when ElinkWidth = "100" and MsbFirst = '1' else
                            DataGB_32b(9 downto 0)   when ElinkWidth = "011" and MsbFirst = '1' else
                            DataGB_8b1               when ElinkWidth = "010" and MsbFirst = '1' else
                            DataGB_32b(19 downto 10) when ElinkWidth = "100" and MsbFirst = '0' else
                            DataGB_16b(19 downto 10) when ElinkWidth = "011" and MsbFirst = '0' else
                            DataGB_16b(9 downto 0)   when ElinkWidth = "010" and MsbFirst = '0' else
                            "0000000000";

    DataGB(29 downto 20) <= DataGB_32b(19 downto 10) when ElinkWidth = "100" and MsbFirst = '1' else
                            DataGB_16b(19 downto 10) when ElinkWidth = "011" and MsbFirst = '1' else
                            DataGB_16b(9 downto 0)   when ElinkWidth = "010" and MsbFirst = '1' else
                            DataGB_32b(29 downto 20) when ElinkWidth = "100" and MsbFirst = '0' else
                            DataGB_32b(9 downto 0)   when ElinkWidth = "011" and MsbFirst = '0' else
                            DataGB_8b1               when ElinkWidth = "010" and MsbFirst = '0' else
                            "0000000000";

    DataGB(39 downto 30) <= DataGB_32b(9 downto 0)   when ElinkWidth = "100" and MsbFirst = '1' else
                            DataGB_16b(9 downto 0)   when ElinkWidth = "011" and MsbFirst = '1' else
                            DataGB_8b2               when ElinkWidth = "010" and MsbFirst = '1' else
                            DataGB_32b(39 downto 30) when ElinkWidth = "100" and MsbFirst = '0' else
                            DataGB_32b(19 downto 10) when ElinkWidth = "011" and MsbFirst = '0' else
                            DataGB_32b(9 downto 0)   when ElinkWidth = "010" and MsbFirst = '0' else
                            "0000000000";

    DataGBValid(0) <= DataGB_32bValid when                        MsbFirst = '1' else
                      DataGB_32bValid when ElinkWidth = "100" and MsbFirst = '0' else
                      DataGB_16bValid when ElinkWidth = "011" and MsbFirst = '0' else
                      DataGB_8b2Valid when ElinkWidth = "010" and MsbFirst = '0' else '0'
                      ;
    DataGBValid(1) <= DataGB_8b1Valid when ElinkWidth = "010" and MsbFirst = '1' else
                      DataGB_32bValid when ElinkWidth = "100" and MsbFirst = '1' else
                      DataGB_32bValid when ElinkWidth = "011" and MsbFirst = '1' else
                      DataGB_32bValid when ElinkWidth = "100" and MsbFirst = '0' else
                      DataGB_16bValid when ElinkWidth = "011" and MsbFirst = '0' else
                      DataGB_16bValid when ElinkWidth = "010" and MsbFirst = '0' else '0'
                      ;
    DataGBValid(2) <= DataGB_32bValid when ElinkWidth = "100" and MsbFirst = '1' else
                      DataGB_16bValid when ElinkWidth = "011" and MsbFirst = '1' else
                      DataGB_16bValid when ElinkWidth = "010" and MsbFirst = '1' else
                      DataGB_8b1Valid when ElinkWidth = "010" and MsbFirst = '0' else
                      DataGB_32bValid when ElinkWidth = "100" and MsbFirst = '0' else
                      DataGB_32bValid when ElinkWidth = "011" and MsbFirst = '0' else '0'
                      ;
    DataGBValid(3) <= DataGB_32bValid when ElinkWidth = "100" and MsbFirst = '1' else
                      DataGB_16bValid when ElinkWidth = "011" and MsbFirst = '1' else
                      DataGB_8b2Valid when ElinkWidth = "010" and MsbFirst = '1' else
                      DataGB_32bValid when                        MsbFirst = '0' else '0'
                      ;

    gen_dec : for i in 0 to 3 generate
        decoder8b10b0: entity work.Decoder8b10b
            generic map(
                GENERATE_FEI4B => false, --: boolean := false;
                GENERATE_LCB_ENC => false, --: boolean := false -- DG @ UBC
                AWAIT_SOP => true)
            port map(
                DataIn => DataGB((10*i)+9 downto (10*i)),
                DataInValid => DataGBValid(i),
                BitSlip => DecoderBitSlip(i),
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                AutoRealign => AutoRealign,
                RealignmentEvent => RealignmentEvent(i),
                reset => reset,
                clk40 => clk40,
                HGTD_ALTIROC_DECODING => HGTD_ALTIROC_DECODING,
                DataOut => DataOut8b10b((8*i)+7 downto (8*i)),
                DataOutValid => DataOutValid8b10b(i),
                EOP => EOP8b10b(i),
                ElinkSOB => ElinkSOB8b10b(i), --FrameErr(i),
                ElinkEOB => ElinkEOB8b10b(i),
                DecoderAligned_out => DecoderAligned8b10b_out(i),
                DecoderAligned_in => DecoderAligned8b10b_in(i),
                CodingError => CodingError8b10b(i),
                DisparityError => DisparityError8b10b(i),
                dispout => dispout(i),
                dispin => dispin(i),
                ISK_SOC_in       => ISK_SOC_in(i),
                ISK_SOC_out      => ISK_SOC_out(i),
                ISK_EOC_in       => ISK_EOC_in(i),
                ISK_EOC_out      => ISK_EOC_out(i),
                ISK_DelimiterA_in => ISK_DelimiterA_in(i),
                ISK_DelimiterB_in => ISK_DelimiterB_in(i),
                ISK_DelimiterA_out => ISK_DelimiterA_out(i),
                ISK_DelimiterB_out => ISK_DelimiterB_out(i),
                CharIsK_in => CharIsK_in(i),
                CharIsK_out => CharIsK_out(i),
                BitSlipIn        => DecoderBitSlipIn(i)
            );
    end generate;

    --8b/10b error counter for diagnostics. Errors are counted for all e-links in the e-group
    err_cnt_8b10b: process(clk40, reset)
        variable errs: std_logic_vector(31 downto 0);
    begin
        if reset = '1' then
            DecodingErrors_s <= x"0000_0000";
        elsif rising_edge(clk40) then
            --rigorous error counting
            errs := DecodingErrors_s;
            if(errs /= x"FFFF_FFFF" AND (CodingError8b10b(0) = '1' OR DisparityError8b10b(0) = '1')) then
                errs := errs + x"1";
            end if;
            if(errs /= x"FFFF_FFFF" AND (CodingError8b10b(1) = '1' OR DisparityError8b10b(1) = '1')) then
                errs := errs + x"1";
            end if;
            if(errs /= x"FFFF_FFFF" AND (CodingError8b10b(2) = '1' OR DisparityError8b10b(2) = '1')) then
                errs := errs + x"1";
            end if;
            if(errs /= x"FFFF_FFFF" AND (CodingError8b10b(3) = '1' OR DisparityError8b10b(3) = '1')) then
                errs := errs + x"1";
            end if;
            DecodingErrors_s <= errs;

        --quick for low-rate errors
        --            if(DecodingErrors /= "1111" AND (CodingError8b10b /= "0000" OR DisparityError8b10b /= "0000")) then
        --                DecodingErrors <= DecodingErrors + x"1";
        --            end if;
        end if;
    end process;

    DecodingErrors <= DecodingErrors_s;

    encodingmux: process(DataOut8b10b, DataOutValid8b10b, EOP8b10b,
                       ElinkSOB8b10b, ElinkEOB8b10b, DecoderAligned8b10b_out,
                       CodingError8b10b, DisparityError8b10b,
                       DataIn, LinkAligned,
                       PathEncoding, ElinkWidth, EnableIn,
                       DataGB_32bValid_p1, DataGB_16bValid_p1, DataGB_8b1Valid_p1, DataGB_8b2Valid_p1, DataValid, DataValid(0))
        function MuxEncoding (
            enc : std_logic_vector(15 downto 0);
            W: std_logic_vector(2 downto 0)
        ) return std_logic_vector is
        begin
            case W is
                when "010" => --8 bit E-Links
                    return enc;
                when "011" => --16 bit E-Links
                    return enc(11 downto 8) & enc(11 downto 8) & enc(3 downto 0) & enc(3 downto 0);
                when "100" => --32 bit E-Links
                    return enc(3 downto 0) & enc(3 downto 0) & enc(3 downto 0) & enc(3 downto 0);
                when others =>
                    return x"0000";
            end case;
        end function;
        variable ME: std_logic_vector(15 downto 0);
    begin
        ME := MuxEncoding(PathEncoding, ElinkWidth);
        for i in 0 to 3 loop
            --Defaults, uncovered encoding case
            Data(8*i+7 downto 8*i) <= x"00";
            DataValid(i)           <= '0';
            EOP(i)                 <= '0';
            ElinkSOB(i)            <= '0';
            ElinkEOB(i)            <= '0';
            DecoderAligned(i)      <= '0';
            CodingError(i)         <= '0';
            DisparityError(i)      <= '0';
            ByteToAxiStreamGearboxValid(i) <= '0';
            if ME(i*4+3 downto i*4) = "0000" then --No Encoding / Direct mode, take data directly from Egroup input bits
                if ElinkWidth = "100" then  --32-bit E-Links, use Enable bit 0
                    DataValid(i)           <= LinkAligned and EnableIn(0);
                    ByteToAxiStreamGearboxValid(0) <= DataValid(0);
                elsif ElinkWidth = "011" then --16-bit E-Links, use Enable bits 0 and 2
                    DataValid(i)           <= LinkAligned and EnableIn((i/2)*2);
                    ByteToAxiStreamGearboxValid((i/2)*2) <= DataValid(i);
                else                        -- 8-bit E-Links, use all Enable bits.
                    DataValid(i)           <= LinkAligned and EnableIn(i);
                    ByteToAxiStreamGearboxValid(i) <= DataValid(i);
                end if;
                Data(8*i+7 downto 8*i) <= DataIn(8*i+7 downto 8*i); --Connect directly to E-Link data, bypassing decoders
                EOP(i)                 <= '0';
                ElinkSOB(i)            <= '0';
                ElinkEOB(i)            <= '0';
                DecoderAligned(i)      <= LinkAligned;
                CodingError(i)         <= '0';
                DisparityError(i)      <= '0';
            end if;
            if ME(i*4+3 downto i*4) = "0001" then --8b10b encoding, take data from decoders
                Data(8*i+7 downto 8*i) <= DataOut8b10b(8*i+7 downto 8*i);
                DataValid(i)           <= DataOutValid8b10b(i);
                EOP(i)                 <= EOP8b10b(i);
                ElinkSOB(i)            <= ElinkSOB8b10b(i);
                ElinkEOB(i)            <= ElinkEOB8b10b(i);
                DecoderAligned(i)      <= DecoderAligned8b10b_out(i);
                CodingError(i)         <= CodingError8b10b(i);
                DisparityError(i)      <= DisparityError8b10b(i);
                case ElinkWidth is
                    when "010" => --8 bit E-Link
                        ByteToAxiStreamGearboxValid(0) <= DataGB_32bValid_p1;
                        ByteToAxiStreamGearboxValid(1) <= DataGB_8b1Valid_p1;
                        ByteToAxiStreamGearboxValid(2) <= DataGB_16bValid_p1;
                        ByteToAxiStreamGearboxValid(3) <= DataGB_8b2Valid_p1;
                    when "011" =>
                        ByteToAxiStreamGearboxValid(0) <= DataGB_32bValid_p1;
                        ByteToAxiStreamGearboxValid(1) <= '0';
                        ByteToAxiStreamGearboxValid(2) <= DataGB_16bValid_p1;
                        ByteToAxiStreamGearboxValid(3) <= '0';
                    when "100" =>
                        ByteToAxiStreamGearboxValid(0) <= DataGB_32bValid_p1;
                        ByteToAxiStreamGearboxValid(1) <= '0';
                        ByteToAxiStreamGearboxValid(2) <= '0';
                        ByteToAxiStreamGearboxValid(3) <= '0';
                    when others =>
                        ByteToAxiStreamGearboxValid(0) <= '0';
                        ByteToAxiStreamGearboxValid(1) <= '0';
                        ByteToAxiStreamGearboxValid(2) <= '0';
                        ByteToAxiStreamGearboxValid(3) <= '0';
                end case;
            end if;
        end loop;
    end process;

    datamux: process(ElinkWidth, Data, DataValid, EOP, ElinkEOB, ElinkSOB, DecoderAligned, CodingError)
    begin
        case ElinkWidth is
            when "010" => --8 bit E-Link
                Data_0 <= x"000000" & Data(7 downto 0);
                Data_1 <= Data(15 downto 8);
                Data_2 <= x"00" & Data(23 downto 16);
                Data_3 <= Data(31 downto 24);
                EOP_0 <= "000" & EOP(0);
                EOP_1 <= EOP(1 downto 1);
                EOP_2 <= "0" & EOP(2);
                EOP_3 <= EOP(3 downto 3);
                SOB_0 <= "000" & ElinkSOB(0);
                SOB_1 <= ElinkSOB(1 downto 1);
                SOB_2 <= "0" & ElinkSOB(2);
                SOB_3 <= ElinkEOB(3 downto 3);
                EOB_0 <= "000" & ElinkEOB(0);
                EOB_1 <= ElinkEOB(1 downto 1);
                EOB_2 <= "0" & ElinkEOB(2);
                EOB_3 <= ElinkEOB(3 downto 3);
                DataValid_0 <= "000"& DataValid(0);
                DataValid_1 <= DataValid(1 downto 1);
                DataValid_2 <= "0" & DataValid(2);
                DataValid_3 <= DataValid(3 downto 3);
                ElinkAligned_s <= DecoderAligned;
                CodingError_0 <= CodingError(0) or CodingError(1);
                CodingError_1 <= '0';
                CodingError_2 <= CodingError(2) or CodingError(3);
                CodingError_3 <= '0';
            when "011" =>
                Data_0 <= x"0000" & Data(15 downto 0);
                Data_1 <= x"00";
                Data_2 <= Data(31 downto 16);
                Data_3 <= x"00";
                EOP_0 <= "00" & EOP(1) & EOP(0);
                EOP_1 <= "0";
                EOP_2 <= EOP(3) & EOP(2);
                EOP_3 <= "0";
                SOB_0 <= "00" & ElinkSOB(1) & ElinkSOB(0);
                SOB_1 <= "0";
                SOB_2 <= ElinkSOB(3) & ElinkSOB(2);
                SOB_3 <= "0";
                EOB_0 <= "00" & ElinkEOB(1) & ElinkEOB(0);
                EOB_1 <= "0";
                EOB_2 <= ElinkEOB(3) & ElinkEOB(2);
                EOB_3 <= "0";
                DataValid_0 <= "00" & DataValid(1) & DataValid(0);
                DataValid_1 <= "0";
                DataValid_2 <= DataValid(3) & DataValid(2);
                DataValid_3 <= "0";
                ElinkAligned_s <= "0" & (DecoderAligned(3) and DecoderAligned(2)) & "0" & (DecoderAligned(1) and DecoderAligned(0));
                CodingError_0 <= CodingError(0);
                CodingError_1 <= CodingError(1);
                CodingError_2 <= CodingError(2);
                CodingError_3 <= CodingError(3);
            when "100" =>
                Data_0 <= Data(31 downto 0);
                Data_1 <= x"00";
                Data_2 <= x"0000";
                Data_3 <= x"00";
                EOP_0 <= EOP(3 downto 0);
                EOP_1 <= "0";
                EOP_2 <= "00";
                EOP_3 <= "0";
                SOB_0 <= ElinkSOB(3 downto 0);
                SOB_1 <= "0";
                SOB_2 <= "00";
                SOB_3 <= "0";
                EOB_0 <= ElinkEOB(3 downto 0);
                EOB_1 <= "0";
                EOB_2 <= "00";
                EOB_3 <= "0";
                DataValid_0 <= DataValid(3 downto 0);
                DataValid_1 <= "0";
                DataValid_2 <= "00";
                DataValid_3 <= "0";
                ElinkAligned_s <= "000" & (DecoderAligned(3) and DecoderAligned(2) and DecoderAligned(1) and DecoderAligned(0));
                CodingError_0 <= CodingError(0) or
                                 CodingError(1) or
                                 CodingError(2) or
                                 CodingError(3);
                CodingError_1 <= '0';
                CodingError_2 <= '0';
                CodingError_3 <= '0';
            when others =>
                Data_0 <= (others => '0');
                Data_1 <= (others => '0');
                Data_2 <= (others => '0');
                Data_3 <= (others => '0');
                EOP_0 <= "0000";
                EOP_1 <= "0";
                EOP_2 <= "00";
                EOP_3 <= "0";
                SOB_0 <= "0000";
                SOB_1 <= "0";
                SOB_2 <= "00";
                SOB_3 <= "0";
                EOB_0 <= "0000";
                EOB_1 <= "0";
                EOB_2 <= "00";
                EOB_3 <= "0";
                DataValid_0 <= "0000";
                DataValid_1 <= "0";
                DataValid_2 <= "00";
                DataValid_3 <= "0";
                ElinkAligned_s <= "0000";
                CodingError_0 <= '0';
                CodingError_1 <= '0';
                CodingError_2 <= '0';
                CodingError_3 <= '0';
        end case;

    end process;

    --These signals bypass the 8b10b decoder, and need to be pipelined 1 clock.
    pipe_gearbox_valid: process(clk40)
    begin
        if rising_edge(clk40) then
            DataGB_32bValid_p1 <= DataGB_32bValid;
            DataGB_8b1Valid_p1 <= DataGB_8b1Valid;
            DataGB_16bValid_p1 <= DataGB_16bValid;
            DataGB_8b2Valid_p1 <= DataGB_8b2Valid;
        end if;
    end process;


    Truncate <= (others => '0');



    Enable(0) <= EnableIn(0);
    Enable(1) <= EnableIn(1) when ElinkWidth = "010" else '0';
    Enable(2) <= EnableIn(2) when ElinkWidth /= "100" else '0';
    Enable(3) <= EnableIn(3) when ElinkWidth = "010" else '0';

    ElinkAligned <= ElinkAligned_s;

    reset_d2_proc: process(clk40, reset)
        variable reset_v: std_logic;
    begin
        if reset = '1' then
            reset_d2 <= '1';
            reset_v := '1';
        elsif rising_edge(clk40) then
            reset_d2 <= reset_v;
            reset_v := '0';
        end if;
    end process;
    g_byteToAxiStream32b0: if IncludeElinks(0) = '1' generate
        i_ByteToAxiStream0 : entity work.ByteToAxiStream
            generic map(
                BYTES => 4,
                BLOCKSIZE => BLOCKSIZE,
                USE_BUILT_IN_FIFO => '1',
                VERSAL => VERSAL
            )
            port map(
                clk40             => clk40,
                reset             => reset_d2,
                EnableIn          => Enable(0),
                DataIn            => Data_0,
                DataInValid       => DataValid_0,
                GearboxValid      => ByteToAxiStreamGearboxValid(0),
                ElinkWidth        => ElinkWidth,
                EOP               => EOP_0,
                SOB               => SOB_0,
                EOB               => EOB_0,
                TruncateIn        => Truncate,
                CodingErrorIn     => CodingError_0,
                m_axis            => m_axis(0),
                m_axis_tready     => m_axis_tready(0),
                m_axis_aclk       => m_axis_aclk,
                m_axis_prog_empty => m_axis_prog_empty(0)
            );
    else generate
        m_axis(0).tvalid <= '0';
        m_axis_prog_empty(0) <= '1';
    end generate;
    g_byteToAxiStream8b1: if Support8bWidth = '1' and IncludeElinks(1) = '1' generate
        i_ByteToAxiStream1 : entity work.ByteToAxiStream
            generic map(
                BYTES => 1,
                BLOCKSIZE => BLOCKSIZE,
                USE_BUILT_IN_FIFO => '1',
                VERSAL => VERSAL
            )
            port map(
                clk40             => clk40,
                reset             => reset_d2,
                EnableIn          => Enable(1),
                DataIn            => Data_1,
                DataInValid       => DataValid_1,
                GearboxValid      => ByteToAxiStreamGearboxValid(1),
                ElinkWidth        => ElinkWidth,
                EOP               => EOP_1,
                SOB               => SOB_1,
                EOB               => EOB_1,
                TruncateIn        => Truncate(1 downto 1),
                CodingErrorIn     => CodingError_1,
                m_axis            => m_axis(1),
                m_axis_tready     => m_axis_tready(1),
                m_axis_aclk       => m_axis_aclk,
                m_axis_prog_empty => m_axis_prog_empty(1)
            );
    else generate
        m_axis(1).tvalid <= '0';
        m_axis_prog_empty(1) <= '1';
    end generate g_byteToAxiStream8b1;

    g_byteToAxiStream16b: if (Support8bWidth = '1' or Support16bWidth = '1') and IncludeElinks(2) = '1' generate
        i_ByteToAxiStream2 : entity work.ByteToAxiStream
            generic map(
                BYTES => 2,
                BLOCKSIZE => BLOCKSIZE,
                USE_BUILT_IN_FIFO =>  '1',
                VERSAL => VERSAL
            )
            port map(
                clk40             => clk40,
                reset             => reset_d2,
                EnableIn          => Enable(2),
                DataIn            => Data_2,
                DataInValid       => DataValid_2,
                GearboxValid      => ByteToAxiStreamGearboxValid(2),
                ElinkWidth        => ElinkWidth,
                EOP               => EOP_2,
                SOB               => SOB_2,
                EOB               => EOB_2,
                TruncateIn        => Truncate(3 downto 2),
                CodingErrorIn     => CodingError_2,
                m_axis            => m_axis(2),
                m_axis_tready     => m_axis_tready(2),
                m_axis_aclk       => m_axis_aclk,
                m_axis_prog_empty => m_axis_prog_empty(2)
            );
    else generate
        m_axis(2).tvalid <= '0';
        m_axis_prog_empty(2) <= '1';
    end generate g_byteToAxiStream16b;

    g_byteToAxiStream8b2: if Support8bWidth = '1' and IncludeElinks(3) = '1' generate
        i_ByteToAxiStream3 : entity work.ByteToAxiStream
            generic map(
                BYTES => 1,
                BLOCKSIZE => BLOCKSIZE,
                USE_BUILT_IN_FIFO =>  '1',
                VERSAL => VERSAL
            )
            port map(
                clk40             => clk40,
                reset             => reset_d2,
                EnableIn          => Enable(3),
                DataIn            => Data_3,
                DataInValid       => DataValid_3,
                GearboxValid      => ByteToAxiStreamGearboxValid(3),
                ElinkWidth        => ElinkWidth,
                EOP               => EOP_3,
                SOB               => SOB_3,
                EOB               => EOB_3,
                TruncateIn        => Truncate(3 downto 3),
                CodingErrorIn     => CodingError_3,
                m_axis            => m_axis(3),
                m_axis_tready     => m_axis_tready(3),
                m_axis_aclk       => m_axis_aclk,
                m_axis_prog_empty => m_axis_prog_empty(3)
            );
    else generate
        m_axis(3).tvalid <= '0';
        m_axis_prog_empty(3) <= '1';
    end generate g_byteToAxiStream8b2;

    FE_BUSY_out_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            for i in 0 to 3 loop
                if reset = '1' or Enable(i) = '0' or ElinkAligned_s(i) = '0' then
                    FE_BUSY_out(i) <= '0';
                else
                    case i is
                        when 0 =>
                            if SOB_0 /= "0000" then
                                FE_BUSY_out(i) <= '1';
                            end if;
                            if EOB_0 /= "0000" then
                                FE_BUSY_out(i) <= '0';
                            end if;
                        when 1 =>
                            if SOB_1 /= "0" then
                                FE_BUSY_out(i) <= '1';
                            end if;
                            if EOB_1 /= "0" then
                                FE_BUSY_out(i) <= '0';
                            end if;
                        when 2 =>
                            if SOB_2 /= "00" then
                                FE_BUSY_out(i) <= '1';
                            end if;
                            if EOB_2 /= "00" then
                                FE_BUSY_out(i) <= '0';
                            end if;
                        when 3 =>
                            if SOB_3 /= "0" then
                                FE_BUSY_out(i) <= '1';
                            end if;
                            if EOB_3 /= "0" then
                                FE_BUSY_out(i) <= '0';
                            end if;
                    end case;
                end if;
            end loop;
        end if;
    end process;

end  architecture;
