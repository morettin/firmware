--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               mtrovato
--!               Ismet Siral
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.centralRouter_package.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
    use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
Library UNISIM;
    use UNISIM.vcomponents.all;



entity DecodingBCMLinkLPGBT is
    generic (
        --CARD_TYPE      : integer := 712;
        BLOCKSIZE      : integer := 1024;
        LINK           : integer := 0  ;
        --SIMU           : integer := 0;
        STREAMS_TOHOST : integer := 1;
        --PCIE_ENDPOINT  : integer := 0;
        AVALIABLE_CLK_CYCLES : integer :=TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE_BCM_PRIME)/40;

        --PIPELINE_DEPTH: integer := 64;

        VERSAL: boolean
    );
    port (

        clk40                         : in std_logic;                        --BC clock for DataIn
        reset                          : in std_logic;                        --Acitve high reset
        LinkData                       : in std_logic_vector(223 downto 0);   --LPGBT Payload
        LinkAligned                    : in std_logic;                        --LPGBT Link Aligned
        m_axis                         : out axis_32_array_type(0 to STREAMS_TOHOST-3);      --FIFO read port (axi stream)
        m_axis_tready                  : in axis_tready_array_type(0 to STREAMS_TOHOST-3);   --FIFO read tready (axi stream)
        m_axis_aclk                    : in std_logic;                        --FIFO read clock (axi stream)
        m_axis_prog_empty              : out axis_tready_array_type(0 to STREAMS_TOHOST-3);

        register_map_control           : in  register_map_control_type;       --Settings (From Wupper)

        TTC_ToHostData                 : in TTC_ToHost_data_type;


        L1AWindow                      : in std_logic_vector(4 downto 0);
        L1ADelay                       : in std_logic_vector(4 downto 0)

    --DecoderAligned :  out std_logic_vector(STREAMS_TOHOST-3 downto 0)            ;
    --DecoderDeskewed : out std_logic_vector(STREAMS_TOHOST-3 downto 0)





    --  register_map_decoding_monitor  : out register_map_decoding_monitor_type
    );
end DecodingBCMLinkLPGBT;

architecture Behavioral of DecodingBCMLinkLPGBT is
    signal DecoderOut: array_32b(6 downto 0);
    signal DecoderDataValid: STD_LOGIC_VECTOR (6 downto 0);
    signal DecoderLastWord: STD_LOGIC_VECTOR (6 downto 0);
    signal SyncBCID  : unsigned (11 downto 0);
    signal SyncL1ID  : unsigned (23 downto 0);
    signal SyncL1SubID : unsigned (4 downto 0);
    signal SyncL1A   : STD_LOGIC;
    signal SyncLinkData : std_logic_vector(223 downto 0);


begin



    process(clk40)
    begin
        if rising_edge(clk40) then


            if reset or (not LinkAligned) then
                SyncBCID <= (others => '0');
                SyncL1ID <= (others => '0');
                SyncL1A <=  '0';
                SyncL1SubID <=  (others => '1');

            --Take L1A infromation from the TTC, and process is accordingly
            elsif to_sl(register_map_control.DECODING_BCM_PRIME_EMU_BCID)='0' then
                if TTC_ToHostData.data_rdy then
                    SyncBCID <= unsigned(TTC_ToHostData.BCID);
                    SyncL1ID <= unsigned(TTC_ToHostData.L1ID);
                    SyncL1A <= '1';
                    SyncL1SubID<= "00000";
                elsif SyncL1SubID  < to_integer(unsigned(L1AWindow)) then
                    --if L1AWindow is active keep the
                    --previous L1A infromation
                    SyncL1SubID <= SyncL1SubID+1;
                --If DECODING_BCM_PRIME_ONLY_L1A is enabled, store every data event
                else
                    SyncBCID <= unsigned(TTC_ToHostData.BCID);
                    SyncL1ID <= unsigned(TTC_ToHostData.L1ID);
                    SyncL1A <= (not to_sl(register_map_control.DECODING_BCM_PRIME_ONLY_L1A));
                    SyncL1SubID <= (others => '1');
                end if;
            --Generate your own internal BCID
            else
                if SyncBCID = 3562 then
                    SyncBCID <= (others => '0');
                else
                    SyncBCID <= SyncBCID+1;
                end if;
                SyncL1ID <= (others => '0');
                SyncL1A  <= '1';
                SyncL1SubID <=  (others => '1');
            end if;





        end if;
    end process;



    DataDelay : for J in 0 to 223 generate --IG: add another bit to support the extended Test Pulse
        DataDelay_SRL : SRLC32E
            generic map(
                INIT => x"0000",
                IS_CLK_INVERTED => '0'
            )
            port map (
                Q => SyncLinkData(J), --delayed copy of TTCin
                Q31 => open,
                A => L1ADelay, --input from control register
                ce => '1', --delay_en,     --input from control register
                clk => clk40,
                D => LinkData(J) --latched copy of TTCin
            );
    end generate DataDelay;


    --Decodes the data
    g_LPGBT_Decoder: for egroup in 0 to 6 generate
    begin

        LpGBT_Decoder: entity work.BCMDec_GearBox --single lane, datainwidth 8,16,32 (only 32 tested), dataoutwidth 32b
            generic map(
                AVALIABLE_CLK_CYCLES=>AVALIABLE_CLK_CYCLES,
                DATA_SIZE => 32,
                OTHER_DATA => 22
            )
            Port map(
                clk => clk40,
                reset => reset or (not LinkAligned),
                Input => SyncLinkData( 32 * (egroup+1)-1 downto 32*(egroup) ),
                Output => DecoderOut ( (egroup) ),
                a_clk => m_axis_aclk,
                BCID => STD_LOGIC_VECTOR(SyncBCID),
                L1ID => STD_LOGIC_VECTOR(SyncL1ID),
                L1SubID => STD_LOGIC_VECTOR(SyncL1SubID),
                L1A => SyncL1A,
                DataValid => DecoderDataValid(egroup),
                LastWord => DecoderLastWord(egroup),
                --ZeroFlag => DecoderDeskewed(egroup),
                --NoErrFlag => DecoderAligned(egroup),
                PublishZeros => to_sl(register_map_control.DECODING_BCM_PRIME_PUBLISH_ZEROS)
            );

    end generate;


    g_LPGBT_ByteToAxiStream: for egroup in 0 to 6 generate
    begin
        g_axisindex_1to3: for i in 1 to 3 generate
            m_axis(egroup*4+i).tvalid <= '0';
            m_axis(egroup*4+i).tdata  <= (others => '0');
            m_axis(egroup*4+i).tlast  <= '0';
            m_axis(egroup*4+i).tkeep  <= (others => '0');
            m_axis(egroup*4+i).tuser  <= (others => '0');
            --m_axis_tready_s(i) <= '0';
            m_axis_prog_empty(egroup*4+i) <= '1';
        end generate;
        g_axisindex_0: for i in 0 to 0 generate
            signal m_axis_s                    : axis_32_type;
            signal m_axis_tready_s             : std_logic;
            signal m_axis_prog_empty_s         : std_logic;
            signal ByteToAxiStreamData         : std_logic_vector(32 downto 0);
            signal ByteToAxiStreamEOP          :  std_logic;
            signal ByteToAxiStreamFramingError : std_logic; -- @suppress "signal ByteToAxiStreamFramingError is never read"
            signal ByteToAxiStreamElinkBusy    : std_logic;
            signal ByteToAxiStreamTruncate     : std_logic;

            --signal EpathEncoding               : std_logic_vector(3 downto 0);
            signal EpathEnable                 : std_logic;
        begin
            -- EpathEncoding <= register_map_control.DECODING_EGROUP_CTRL(LINK)(egroup).PATH_ENCODING(14+4*i downto 11+4*i);
            EpathEnable <= register_map_control.DECODING_EGROUP_CTRL(LINK)(egroup).EPATH_ENA(i);



            --i=0 => hit data, i=1 => dcs
            m_axis(egroup*4+i)              <= m_axis_s;
            m_axis_tready_s               <= m_axis_tready(egroup*4+i);
            m_axis_prog_empty(egroup*4+i)   <= m_axis_prog_empty_s;

            --debug
            --      m_axis_dbg(egroup*4+i)              <= m_axis_s;
            --      m_axis_tready_dbg(egroup*4+i)               <= m_axis_tready(egroup*4+i);
            --      m_axis_prog_empty_dbg(egroup*4+i)   <= m_axis_prog_empty_s;
            --      --


            decoding_mux: process(m_axis_aclk)
            begin
                if rising_edge(m_axis_aclk) then
                    --To Do, Understand the objectives of each of these files
                    if EpathEnable = '1' then
                        ByteToAxiStreamData         <= DecoderDataValid(egroup) & DecoderOut(egroup)(31 downto 0);
                        ByteToAxiStreamEOP          <= DecoderLastWord(egroup);
                        ByteToAxiStreamFramingError <= '0';
                        ByteToAxiStreamElinkBusy    <= '0'; --to do: from the RD53 service frame with statusbit=5.
                        ByteToAxiStreamTruncate     <= '0';
                    else
                        ByteToAxiStreamData      <= (others=>'0');
                        ByteToAxiStreamEOP          <= '0';
                        ByteToAxiStreamFramingError <= '0';
                        ByteToAxiStreamElinkBusy    <= '0';
                        ByteToAxiStreamTruncate     <= '0';
                    end if; --if EpathEncoding(egroup*4+i) = "0011" and EpathEnable(egroup*4+i) = '1'
                end if;
            end process;




            toAxis0: entity work.ByteToAxiStream32b
                generic map(
                    BLOCKSIZE => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '0',
                    VERSAL => VERSAL
                )
                port map(
                    clk => m_axis_aclk,
                    reset => reset,
                    --        EnableIn => EpathEnable(egroup*4+i),
                    EnableIn => EpathEnable,
                    DataIn => ByteToAxiStreamData(31 downto 0),
                    DataInValid => ByteToAxiStreamData(32),
                    EOP => ByteToAxiStreamEOP,
                    BytesKeep => "1111",
                    ElinkBusy => ByteToAxiStreamElinkBusy,
                    TruncateIn => ByteToAxiStreamTruncate,
                    m_axis => m_axis_s,
                    m_axis_tready => m_axis_tready_s,
                    m_axis_aclk => m_axis_aclk,
                    m_axis_prog_empty => m_axis_prog_empty_s
                );
        end generate; -- g_axisindex_0to1: for i in 0 to 1 generate
    end generate; --g_LPGBT_ByteToAxiStream: for egroup in 0 to 6 generate




end Behavioral;
