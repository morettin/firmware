--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.




library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;



entity DecodingEgroupGBT is
    generic (
        INCLUDE_16b       : std_logic := '1';
        INCLUDE_8b        : std_logic := '1';
        INCLUDE_4b        : std_logic := '1';
        INCLUDE_2b        : std_logic := '1';
        INCLUDE_8b10b     : std_logic := '1';
        INCLUDE_HDLC      : std_logic := '1';
        INCLUDE_DIRECT    : std_logic := '1';
        BLOCKSIZE         : integer := 1024;
        USE_BUILT_IN_FIFO : std_logic_vector(7 downto 0) := x"55";
        GENERATE_FEI4B    : boolean := false;
        VERSAL            : boolean := false
    );
    port (
        clk40 : in std_logic; --BC clock for DataIn
        reset : in std_logic; --Acitve high reset

        EpathEnable : in std_logic_vector(7 downto 0); --From register map
        EpathEncoding : in std_logic_vector(31 downto 0); --4 bits per EPath: 0: direct, 1: 8b10b, 2: HDLC
        ElinkWidth : in std_logic_vector(2 downto 0); --runtime configuration: 0:2, 1:4, 2:8, 3:16, 4:32
        MsbFirst         : in std_logic; --Default 1, make 0 to reverse the bit order
        ReverseInputBits : in std_logic_vector(7 downto 0); --Default 0, reverse the bits of the input Elink
        EnableTruncation : in std_logic; --Default 0: 1 to enable truncation mechanism in HDLC decoder for chunks > 12 bytes
        DecoderAligned : out std_logic_vector(7 downto 0); --Status for registermap

        EGroupData : in std_logic_vector(15 downto 0);
        GBTAligned : in std_logic;
        FE_BUSY_out : out std_logic_vector(7 downto 0);

        m_axis : out axis_32_array_type(0 to 7);  --FIFO read port (axi stream)
        m_axis_tready : in axis_tready_array_type(0 to 7); --FIFO read tready (axi stream)
        m_axis_aclk : in std_logic; --FIFO read clock (axi stream)
        m_axis_prog_empty : out axis_tready_array_type(0 to 7);
        AlignmentPulseAlign : in std_logic;
        AlignmentPulseDeAlign : in std_logic;
        AutoRealign : in std_logic; --Realign 8b10b decoder in case of not-in-table k-character
        RealignmentEvent : out std_logic_vector(7 downto 0) --8b10b decoder has gone out of alignment (single pulse)
    );
end DecodingEgroupGBT;

architecture rtl of DecodingEgroupGBT is

    type IntArray is array (0 to 7) of integer;
    constant ElinkWidths  : IntArray := (16, 2, 4, 2, 8, 2, 4, 2);


    --Determine with the maximum Elink width and the include generics whether we want to generate the Epath at all.
    function g(w: integer) return boolean is
    begin
        if w = 2 and INCLUDE_2b = '0' then
            return false;
        end if;
        if w = 4 and INCLUDE_2b = '0' and INCLUDE_4b = '0' then
            return false;
        end if;
        if w = 8 and INCLUDE_2b = '0' and INCLUDE_4b = '0' and INCLUDE_8b = '0' then
            return false;
        end if;
        if w = 16 and INCLUDE_2b = '0' and INCLUDE_4b = '0' and INCLUDE_8b = '0' and INCLUDE_16b = '0' then
            return false;
        end if;
        return true;
    end function;
begin

    g_Epaths: for i in 0 to 7 generate
        do_generate: if g(ElinkWidths(i)) generate
            Epath0: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT         => ElinkWidths(i),
                    INCLUDE_16b       => INCLUDE_16b,
                    INCLUDE_8b        => INCLUDE_8b,
                    INCLUDE_4b        => INCLUDE_4b,
                    INCLUDE_2b        => INCLUDE_2b,
                    INCLUDE_8b10b     => INCLUDE_8b10b,
                    INCLUDE_HDLC      => INCLUDE_HDLC,
                    INCLUDE_DIRECT    => INCLUDE_DIRECT,
                    BLOCKSIZE         => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO(i),
                    GENERATE_FEI4B    => GENERATE_FEI4B,
                    VERSAL            => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnable(i),
                    EpathEncoding => EpathEncoding(i*4+3 downto i*4), --0: direct, 1: 8b10b, 2: HDLC
                    ElinkWidth => ElinkWidth, --runtime configuration: 0:2, 1:4, 2:8, 3:16, 4:32
                    MsbFirst   => MsbFirst, --Default 1, make 0 to reverse the bit order
                    ReverseInputBits => ReverseInputBits(i),
                    EnableTruncation => EnableTruncation,
                    ElinkData  => EGroupData(i*2+ElinkWidths(i)-1 downto i*2),
                    ElinkAligned => GBTAligned,
                    DecoderAligned => DecoderAligned(i),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => AutoRealign,
                    RealignmentEvent => RealignmentEvent(i),
                    FE_BUSY_out => FE_BUSY_out(i),
                    m_axis => m_axis(i),
                    m_axis_tready => m_axis_tready(i),
                    m_axis_aclk => m_axis_aclk,
                    m_axis_prog_empty => m_axis_prog_empty(i)
                );
        end generate;
        dont_generate: if not g(ElinkWidths(i)) generate --exclude this EPath, disable m_axis and prog_empty
            m_axis(i).tvalid <= '0';
            m_axis_prog_empty(i) <= '1';
        end generate;
    end generate;



end rtl;
