--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas---tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               mtrovato
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use work.FELIX_package.all;
    use work.pcie_package.all;
    use work.package_64b66b.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DecodingPixelLinkLPGBT is
    generic (
        CARD_TYPE      : integer := 712; -- @suppress "Unused generic: CARD_TYPE is not used in work.DecodingPixelLinkLPGBT(Behavioral)"
        RD53Version     : String := "A"; --A or B -- @suppress "Unused generic: SIMU is not used in work.DecodingPixelLinkLPGBT(Behavioral)"
        BLOCKSIZE      : integer := 1024;
        LINK           : integer := 0  ;
        SIMU           : integer := 0; -- @suppress "Unused generic: SIMU is not used in work.DecodingPixelLinkLPGBT(Behavioral)"
        STREAMS_TOHOST : integer := 1;
        PCIE_ENDPOINT  : integer := 0; -- @suppress "Unused generic: PCIE_ENDPOINT is not used in work.DecodingPixelLinkLPGBT(Behavioral)"
        VERSAL         : boolean
    );
    port (

        clk40                         : in std_logic;                        --BC clock for DataIn
        reset                          : in std_logic;                        --Acitve high reset
        MsbFirst                       : in std_logic;                        --Default 1, maske 0 to reverse the bit order
        LinkData                       : in std_logic_vector(223 downto 0);   --LPGBT Payload
        LinkAligned                    : in std_logic;                        --LPGBT Link Aligned
        m_axis                         : out axis_32_array_type(0 to STREAMS_TOHOST-3);      --FIFO read port (axi stream)
        m_axis_tready                  : in axis_tready_array_type(0 to STREAMS_TOHOST-3);   --FIFO read tready (axi stream)
        m_axis_aclk                    : in std_logic;                        --FIFO read clock (axi stream)
        m_axis_prog_empty              : out axis_tready_array_type(0 to STREAMS_TOHOST-3);
        register_map_control           : in  register_map_control_type;       --Settings (From Wupper)
        --register_map_decoding_monitor  : out register_map_decoding_monitor_type

        DecoderAligned_out             : out std_logic_vector(STREAMS_TOHOST-3 downto 0);
        DecoderDeskewed_out            : out std_logic_vector(STREAMS_TOHOST-3 downto 0);
        cnt_rx_64b66bhdr_out           : out std_logic_vector(31 downto 0);
        rx_soft_err_cnt_out            : out array_32b(0 to 6);
        rx_soft_err_rst                : in std_logic

    );
end DecodingPixelLinkLPGBT;

architecture Behavioral of DecodingPixelLinkLPGBT is

    --signal DecoderAligned_reg             : std_logic_vector(STREAMS_TOHOST-3 downto 0);
    --signal DecoderDeskewed_reg            : std_logic_vector(STREAMS_TOHOST-3 downto 0);
    --signal cnt_rx_64b66bhdr_reg           :  std_logic_vector(31 downto 0);
    signal rx_soft_err_cnt_reg               : array_32b(0 to 6);

    signal CBOPT               : std_logic_vector(3 downto 0);
    signal mask_k_char         : std_logic_vector(3 downto 0);
    signal ref_packet_in               : std_logic_vector(31 downto 0);

    --signal rx_soft_err_i       : std_logic_vector(0 to 6)            ;
    --33b = 33b=tlast, 32b=valid ; 31 downto 0= data_32b
    signal DecoderAligned     : std_logic_vector(0 to 6)     := (others => '0')            ;
    signal DecoderDeskewed     : std_logic_vector(0 to 6)     := (others => '0')            ;
    signal EgroupData_aclk    : array_data32VL_64b66b_type(0 to 6) := (others => data32VL_64b66b_type_zero); --array_34b(0 to 6)                  := (others => (others => '0'));
    signal ByteToAxiStreamKeep_aclk : array_4b(0 to 6)             := (others => (others => '0'));
    signal EgroupDCSData_aclk : array_data32VL_64b66b_type(0 to 6) := (others => data32VL_64b66b_type_zero); --array_34b(0 to 6)                  := (others => (others => '0'));


    --
    --debug
    --clkd40
    signal EgroupGB1Data_dbg           :  array_data32VHHV_64b66b_type(0 to 6); -- @suppress "signal EgroupGB1Data_dbg is never read"
    signal EgroupUnscrData_dbg         :  array_data32VHHV_64b66b_type(0 to 6); -- @suppress "signal EgroupUnscrData_dbg is never read"
    signal EgroupGB2Data_dbg           :  array_data64VHHVE_64b66b_type(0 to 6); -- @suppress "signal EgroupGB2Data_dbg is never read"
    signal EgroupRmpData_dbg           :  array_data64VHHVE_64b66b_type(0 to 6); -- @suppress "signal EgroupRmpData_dbg is never read"
    signal EgroupDecData_dbg           :  array_data64V_64b66b_type(0 to 6); -- @suppress "signal EgroupDecData_dbg is never read"
    signal EgroupDecDataSplit_dbg      :  array_data32VL_64b66b_type(0 to 6) := (others => data32VL_64b66b_type_zero); --array_34b(0 to 6); -- @suppress "signal EgroupDecDataSplit_dbg is never read"
    signal EgroupMUX1Data_dbg          :  array_data64VHHVE_64b66b_type(0 to 6); -- @suppress "signal EgroupMUX1Data_dbg is never read"
    signal EgroupBondData_3_dbg        :  array_data64VHHVE_64b66b_type(0 to 5); -- @suppress "signal EgroupBondData_3_dbg is never read"
    signal DecoderAlignedRmp_dbg       :  std_logic_vector(0 to 6); -- @suppress "signal DecoderAlignedRmp_dbg is never read"
    signal DecoderAlignedMUX1_dbg      :  std_logic_vector(0 to 6); -- @suppress "signal DecoderAlignedMUX1_dbg is never read"
    signal DecoderAlignedBond_3_dbg    :  std_logic_vector(0 to 1); -- @suppress "signal DecoderAlignedBond_3_dbg is never read"
    signal DCSDecData_dbg              :  array_data64V_64b66b_type(0 to 6); -- @suppress "signal DCSDecData_dbg is never read"


    --aclk
    signal EgroupDecDataSplit_aclk_dbg      :  array_data32VL_64b66b_type(0 to 6) := (others => data32VL_64b66b_type_zero); --array_34b(0 to 6); -- @suppress "signal EgroupDecDataSplit_aclk_dbg is never read"
    signal EgroupMUX2Data_aclk_dbg     :  array_data32VL_64b66b_type(0 to 6) := (others => data32VL_64b66b_type_zero); --array_34b(0 to 6); -- @suppress "signal EgroupMUX2Data_aclk_dbg is never read"
    signal EgroupAggrData_30_dbg       :  data32VL_64b66b_type := data32VL_64b66b_type_zero; --std_logic_vector(33 downto 0); -- @suppress "signal EgroupAggrData_30_dbg is never read"
    signal EgroupAggrData_31_dbg       :  data32VL_64b66b_type := data32VL_64b66b_type_zero; --std_logic_vector(33 downto 0); -- @suppress "signal EgroupAggrData_31_dbg is never read"
    --signal EgroupDCSData_aclk_dbg      :  array_34b(0 to 6);
    signal DecoderAlignedDec_aclk_dbg :  std_logic_vector(0 to 6);  -- @suppress "signal DecoderAlignedDec_aclk_dbg is never read"
    signal m_axis_dbg                  :  axis_32_array_type(0 to STREAMS_TOHOST-3);
    signal m_axis_tready_dbg           :  axis_tready_array_type(0 to STREAMS_TOHOST-3);
    signal m_axis_prog_empty_dbg       :  axis_tready_array_type(0 to STREAMS_TOHOST-3); -- @suppress "signal m_axis_prog_empty_dbg is never read"
    signal cnt_rx_64b66bhdr_160_i : std_logic_vector(31 downto 0); -- @suppress "signal cnt_rx_64b66bhdr_160_i is never read"
    signal cnt_rx_maxishdr_i : std_logic_vector(31 downto 0); -- @suppress "signal cnt_rx_maxishdr_i is never read"


begin
    rx_soft_err_cnt_out <= rx_soft_err_cnt_reg;
    ref_packet_in <= register_map_control.YARR_DEBUG_ALLEGROUP_TOHOST(LINK mod 12).REF_PACKET(63 downto 32);
    CBOPT <= register_map_control.DECODING_LINK_CB(LINK mod 12).CBOPT;
    mask_k_char <= register_map_control.DECODING_MASK64B66BKBLOCK;

    decoding_64b66b_0: entity work.decoding_64b66b --single lane, datainwidth 8,16,32 (only 32 tested), dataoutwidth 32b
        generic map(
            RD53Version            => RD53Version
        )
        Port map(
            useEmulator            => register_map_control.GBT_TOHOST_FANOUT.SEL(LINK),
            cbopt                  => CBOPT,
            mask_k_char            => mask_k_char,
            MsbFirst               => MsbFirst,
            reset                  => reset,
            CLK40_IN               => clk40,
            aclk_in                => m_axis_aclk,
            LinkAligned_in         => LinkAligned,
            DataIn                 => LinkData,
            DataOut                => EgroupData_aclk,
            BytesKeepOut           => ByteToAxiStreamKeep_aclk,
            DataDCSOut             => EgroupDCSData_aclk,
            DecoderAligned_out     => DecoderAligned,
            DecoderDeskewed_out    => DecoderDeskewed,
            soft_err               => open, --rx_soft_err_i,
            soft_err_cnt           => rx_soft_err_cnt_reg,
            soft_err_rst           => rx_soft_err_rst,
            --debug (regmap)
            cnt_rx_64b66bhdr_out   => cnt_rx_64b66bhdr_out,
            ref_packet_in          => ref_packet_in,
            --ila
            EgroupGB1Data_dbg             => EgroupGB1Data_dbg          ,
            EgroupUnscrData_dbg           => EgroupUnscrData_dbg        ,
            EgroupGB2Data_dbg             => EgroupGB2Data_dbg          ,
            EgroupRmpData_dbg             => EgroupRmpData_dbg          ,
            EgroupDecData_dbg             => EgroupDecData_dbg          ,
            EgroupDecDataSplit_dbg        => EgroupDecDataSplit_dbg     ,
            DCSDecData_dbg                => DCSDecData_dbg             ,
            EgroupMUX1Data_dbg            => EgroupMUX1Data_dbg         ,
            EgroupBondData_3_dbg          => EgroupBondData_3_dbg       ,
            DecoderAlignedRmp_dbg         => DecoderAlignedRmp_dbg      ,
            DecoderAlignedMUX1_dbg        => DecoderAlignedMUX1_dbg     ,
            DecoderAlignedBond_3_dbg      => DecoderAlignedBond_3_dbg   ,
            EgroupDecDataSplit_aclk_dbg       => EgroupDecDataSplit_aclk_dbg    ,
            EgroupMUX2Data_aclk_dbg       => EgroupMUX2Data_aclk_dbg    ,
            EgroupAggrData_30_dbg         => EgroupAggrData_30_dbg      ,
            EgroupAggrData_31_dbg         => EgroupAggrData_31_dbg      ,
            DCSDecDataSplit_aclk_dbg     => open,
            DecoderAlignedDec_aclk_dbg   => DecoderAlignedDec_aclk_dbg
        );


    CntRcvdPckts_64b66b_i: entity work.cntRcvdPckts_64b66b port map (
            rst                         => reset,
            clk40                       => m_axis_aclk,
            data_in                     => EgroupData_aclk(0).data(31 downto 25),
            datav_in                    => EgroupData_aclk(0).valid,
            ref_packet_in               => ref_packet_in(31 downto 25),
            cnt_packets_out             => cnt_rx_64b66bhdr_160_i
        );
    --cnt_rx_64b66bhdr_160_out <= cnt_rx_64b66bhdr_160_i; to regmap

    CntRcvdPckts_64b66b_2_i: entity work.cntRcvdPckts_64b66b port map (
            rst                         => reset,
            clk40                       => m_axis_aclk,
            data_in                     => m_axis_dbg(0).tdata(31 downto 25),
            datav_in                    => m_axis_dbg(0).tvalid and m_axis_tready_dbg(0),
            ref_packet_in               => ref_packet_in(31 downto 25),
            cnt_packets_out             => cnt_rx_maxishdr_i
        );

    g_LPGBT_ByteToAxiStream: for egroup in 0 to 6 generate
    begin
        g_axisindex_2to3: for i in 2 to 3 generate
            m_axis(egroup*4+i).tvalid <= '0';
            m_axis(egroup*4+i).tdata  <= (others => '0');
            m_axis(egroup*4+i).tlast  <= '0';
            m_axis(egroup*4+i).tkeep  <= (others => '0');
            m_axis(egroup*4+i).tuser  <= (others => '0');
            --m_axis_tready_s(i) <= '0';
            m_axis_prog_empty(egroup*4+i) <= '1';
        end generate;
        g_axisindex_0to1: for i in 0 to 1 generate
            signal m_axis_s                    : axis_32_type;
            signal m_axis_tready_s             : std_logic;
            signal m_axis_prog_empty_s         : std_logic;
            signal ByteToAxiStreamData         : std_logic_vector(32 downto 0);
            signal ByteToAxiStreamEOP          :  std_logic;
            signal ByteToAxiStreamKeep         : std_logic_vector(3 downto 0);
            --signal ByteToAxiStreamFramingError : std_logic;
            signal ByteToAxiStreamElinkBusy    : std_logic;
            signal ByteToAxiStreamTruncate     : std_logic;

            signal EpathEncoding               : std_logic_vector(3 downto 0);
            signal EpathEnable                 : std_logic;

        --signal prog_full_axis          : std_logic;
        --signal wr_data_count_axis      : std_logic_vector(9 downto 0);
        --signal almost_full_axis        : std_logic;
        begin
            EpathEncoding <= register_map_control.DECODING_EGROUP_CTRL(LINK mod 12)(egroup).PATH_ENCODING(14+4*i downto 11+4*i);
            EpathEnable <= register_map_control.DECODING_EGROUP_CTRL(LINK mod 12)(egroup).EPATH_ENA(i);

            decaligned_proc: if ( i=0 ) generate
                DecoderAligned_out(egroup*4+3 downto egroup*4) <= ("000" & DecoderAligned(egroup)) when (EpathEncoding = "0011" and EpathEnable = '1') else "0000";
                DecoderDeskewed_out(egroup*4+3 downto egroup*4) <= ("000" & DecoderDeskewed(egroup)) when (EpathEncoding = "0011" and EpathEnable = '1') else "0000";
            end generate;


            --i=0 => hit data, i=1 => dcs
            m_axis(egroup*4+i)              <= m_axis_s;
            m_axis_tready_s               <= m_axis_tready(egroup*4+i);
            m_axis_prog_empty(egroup*4+i)   <= m_axis_prog_empty_s;

            --debug
            m_axis_dbg(egroup*4+i)              <= m_axis_s;
            m_axis_tready_dbg(egroup*4+i)               <= m_axis_tready(egroup*4+i);
            m_axis_prog_empty_dbg(egroup*4+i)   <= m_axis_prog_empty_s;
            --

            decoding_mux: process(m_axis_aclk)
            begin
                if rising_edge(m_axis_aclk) then
                    if EpathEncoding = "0011" and EpathEnable = '1' then
                        if (i = 0) then
                            ByteToAxiStreamData         <= EgroupData_aclk(egroup).valid & EgroupData_aclk(egroup).data;
                            ByteToAxiStreamEOP          <= EgroupData_aclk(egroup).tlast;
                            ByteToAxiStreamKeep         <= ByteToAxiStreamKeep_aclk(egroup);
                            ByteToAxiStreamElinkBusy    <= '0'; --to do: from the RD53 service frame with statusbit=5.
                            ByteToAxiStreamTruncate     <= '0';
                        else
                            ByteToAxiStreamData         <= EgroupDCSData_aclk(egroup).valid & EgroupDCSData_aclk(egroup).data;
                            ByteToAxiStreamEOP          <= EgroupDCSData_aclk(egroup).tlast;
                            ByteToAxiStreamKeep         <= "1111";
                            ByteToAxiStreamElinkBusy    <= '0';
                            ByteToAxiStreamTruncate     <= '0';
                        end if;
                    else
                        ByteToAxiStreamData      <= (others=>'0');
                        ByteToAxiStreamEOP          <= '0';
                        ByteToAxiStreamKeep         <= (others => '0');
                        ByteToAxiStreamElinkBusy    <= '0';
                        ByteToAxiStreamTruncate     <= '0';
                    end if; --if EpathEncoding(egroup*4+i) = "0011" and EpathEnable(egroup*4+i) = '1'
                end if;
            end process;

            toAxis0: entity work.ByteToAxiStream32b
                generic map(
                    BLOCKSIZE => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '0',
                    VERSAL => VERSAL
                )
                port map(
                    clk => m_axis_aclk,
                    reset => reset,
                    EnableIn => EpathEnable,
                    DataIn => ByteToAxiStreamData(31 downto 0),
                    DataInValid => ByteToAxiStreamData(32),
                    EOP => ByteToAxiStreamEOP,
                    BytesKeep => ByteToAxiStreamKeep,
                    ElinkBusy => ByteToAxiStreamElinkBusy,
                    TruncateIn => ByteToAxiStreamTruncate,
                    m_axis => m_axis_s,
                    m_axis_tready => m_axis_tready_s,
                    m_axis_aclk => m_axis_aclk,
                    m_axis_prog_empty => m_axis_prog_empty_s
                );
        end generate; -- g_axisindex_0to1: for i in 0 to 1 generate
    end generate; --g_LPGBT_ByteToAxiStream: for egroup in 0 to 6 generate



--debug
--g_ILA_DECODINGPIXELLINKLPGBT: if SIMU = 0 and PCIE_ENDPOINT = 0 and LINK = 0 generate


--  --64b66b decoding
--ila_pixellinklpgbt_1_i: entity work.ila_pixellinklpgbt_1
--  port map
--  (
--    clk      => clk40,
--    probe0   => LinkData,
--    probe1   => EgroupGB1Data_dbg(0).hdr_valid & EgroupGB1Data_dbg(0).hdr & EgroupGB1Data_dbg(0).valid & EgroupGB1Data_dbg(0).data, --array_data32VHHV_64b66b_type 1+2+1+32
--    probe2   => EgroupGB1Data_dbg(1).hdr_valid & EgroupGB1Data_dbg(1).hdr & EgroupGB1Data_dbg(1).valid & EgroupGB1Data_dbg(1).data,
--    probe3   => EgroupGB1Data_dbg(2).hdr_valid & EgroupGB1Data_dbg(2).hdr & EgroupGB1Data_dbg(2).valid & EgroupGB1Data_dbg(2).data,
--    probe4   => EgroupGB1Data_dbg(3).hdr_valid & EgroupGB1Data_dbg(3).hdr & EgroupGB1Data_dbg(3).valid & EgroupGB1Data_dbg(3).data,
--    probe5   => EgroupGB1Data_dbg(4).hdr_valid & EgroupGB1Data_dbg(4).hdr & EgroupGB1Data_dbg(4).valid & EgroupGB1Data_dbg(4).data,
--    probe6   => EgroupGB1Data_dbg(5).hdr_valid & EgroupGB1Data_dbg(5).hdr & EgroupGB1Data_dbg(5).valid & EgroupGB1Data_dbg(5).data,
--    probe7   => EgroupGB1Data_dbg(6).hdr_valid & EgroupGB1Data_dbg(6).hdr & EgroupGB1Data_dbg(6).valid & EgroupGB1Data_dbg(6).data,
--    probe8   => EgroupUnscrData_dbg(0).hdr_valid & EgroupUnscrData_dbg(0).hdr & EgroupUnscrData_dbg(0).valid & EgroupUnscrData_dbg(0).data, --array_data32VHHV_64b66b_type 1+2+1+32
--    probe9   => EgroupUnscrData_dbg(1).hdr_valid & EgroupUnscrData_dbg(1).hdr & EgroupUnscrData_dbg(1).valid & EgroupUnscrData_dbg(1).data,
--    probe10  => EgroupUnscrData_dbg(2).hdr_valid & EgroupUnscrData_dbg(2).hdr & EgroupUnscrData_dbg(2).valid & EgroupUnscrData_dbg(2).data,
--    probe11  => EgroupUnscrData_dbg(3).hdr_valid & EgroupUnscrData_dbg(3).hdr & EgroupUnscrData_dbg(3).valid & EgroupUnscrData_dbg(3).data,
--    probe12  => EgroupUnscrData_dbg(4).hdr_valid & EgroupUnscrData_dbg(4).hdr & EgroupUnscrData_dbg(4).valid & EgroupUnscrData_dbg(4).data,
--    probe13  => EgroupUnscrData_dbg(5).hdr_valid & EgroupUnscrData_dbg(5).hdr & EgroupUnscrData_dbg(5).valid & EgroupUnscrData_dbg(5).data,
--    probe14  => EgroupUnscrData_dbg(6).hdr_valid & EgroupUnscrData_dbg(6).hdr & EgroupUnscrData_dbg(6).valid & EgroupUnscrData_dbg(6).data,
--    probe15  => EgroupGB2Data_dbg(0).en & EgroupGB2Data_dbg(0).hdr_valid & EgroupGB2Data_dbg(0).hdr & EgroupGB2Data_dbg(0).valid & EgroupGB2Data_dbg(0).data, --array_data64VHHVE_64b66b_type 1+1+2+1+64
--    probe16  => EgroupGB2Data_dbg(1).en & EgroupGB2Data_dbg(1).hdr_valid & EgroupGB2Data_dbg(1).hdr & EgroupGB2Data_dbg(1).valid & EgroupGB2Data_dbg(1).data,
--    probe17  => EgroupGB2Data_dbg(2).en & EgroupGB2Data_dbg(2).hdr_valid & EgroupGB2Data_dbg(2).hdr & EgroupGB2Data_dbg(2).valid & EgroupGB2Data_dbg(2).data,
--    probe18  => EgroupGB2Data_dbg(3).en & EgroupGB2Data_dbg(3).hdr_valid & EgroupGB2Data_dbg(3).hdr & EgroupGB2Data_dbg(3).valid & EgroupGB2Data_dbg(3).data,
--    probe19  => EgroupGB2Data_dbg(4).en & EgroupGB2Data_dbg(4).hdr_valid & EgroupGB2Data_dbg(4).hdr & EgroupGB2Data_dbg(4).valid & EgroupGB2Data_dbg(4).data,
--    probe20  => EgroupGB2Data_dbg(5).en & EgroupGB2Data_dbg(5).hdr_valid & EgroupGB2Data_dbg(5).hdr & EgroupGB2Data_dbg(5).valid & EgroupGB2Data_dbg(5).data,
--    probe21  => EgroupGB2Data_dbg(6).en & EgroupGB2Data_dbg(6).hdr_valid & EgroupGB2Data_dbg(6).hdr & EgroupGB2Data_dbg(6).valid & EgroupGB2Data_dbg(6).data,
--    probe22  => EgroupRmpData_dbg(0).en & EgroupRmpData_dbg(0).hdr_valid & EgroupRmpData_dbg(0).hdr & EgroupRmpData_dbg(0).valid & EgroupRmpData_dbg(0).data, --array_data64VHHVE_64b66b_type 1+1+2+1+64
--    probe23  => EgroupRmpData_dbg(1).en & EgroupRmpData_dbg(1).hdr_valid & EgroupRmpData_dbg(1).hdr & EgroupRmpData_dbg(1).valid & EgroupRmpData_dbg(1).data,
--    probe24  => EgroupRmpData_dbg(2).en & EgroupRmpData_dbg(2).hdr_valid & EgroupRmpData_dbg(2).hdr & EgroupRmpData_dbg(2).valid & EgroupRmpData_dbg(2).data,
--    probe25  => EgroupRmpData_dbg(3).en & EgroupRmpData_dbg(3).hdr_valid & EgroupRmpData_dbg(3).hdr & EgroupRmpData_dbg(3).valid & EgroupRmpData_dbg(3).data,
--    probe26  => EgroupRmpData_dbg(4).en & EgroupRmpData_dbg(4).hdr_valid & EgroupRmpData_dbg(4).hdr & EgroupRmpData_dbg(4).valid & EgroupRmpData_dbg(4).data,
--    probe27  => EgroupRmpData_dbg(5).en & EgroupRmpData_dbg(5).hdr_valid & EgroupRmpData_dbg(5).hdr & EgroupRmpData_dbg(5).valid & EgroupRmpData_dbg(5).data,
--    probe28  => EgroupRmpData_dbg(6).en & EgroupRmpData_dbg(6).hdr_valid & EgroupRmpData_dbg(6).hdr & EgroupRmpData_dbg(6).valid & EgroupRmpData_dbg(6).data,
--    probe29  => EgroupDecData_dbg(0).valid & EgroupDecData_dbg(0).data, --array_data64V_64b66b_type 1+64
--    probe30  => EgroupDecData_dbg(1).valid & EgroupDecData_dbg(1).data,
--    probe31  => EgroupDecData_dbg(2).valid & EgroupDecData_dbg(2).data,
--    probe32  => EgroupDecData_dbg(3).valid & EgroupDecData_dbg(3).data,
--    probe33  => EgroupDecData_dbg(4).valid & EgroupDecData_dbg(4).data,
--    probe34  => EgroupDecData_dbg(5).valid & EgroupDecData_dbg(5).data,
--    probe35  => EgroupDecData_dbg(6).valid & EgroupDecData_dbg(6).data,
--    probe36  => EgroupMUX1Data_dbg(0).en & EgroupMUX1Data_dbg(0).hdr_valid & EgroupMUX1Data_dbg(0).hdr & EgroupMUX1Data_dbg(0).valid & EgroupMUX1Data_dbg(0).data, --array_data64VHHVE_64b66b_type 1+1+2+1+64
--    probe37  => EgroupMUX1Data_dbg(1).en & EgroupMUX1Data_dbg(1).hdr_valid & EgroupMUX1Data_dbg(1).hdr & EgroupMUX1Data_dbg(1).valid & EgroupMUX1Data_dbg(1).data,
--    probe38  => EgroupMUX1Data_dbg(2).en & EgroupMUX1Data_dbg(2).hdr_valid & EgroupMUX1Data_dbg(2).hdr & EgroupMUX1Data_dbg(2).valid & EgroupMUX1Data_dbg(2).data,
--    probe39  => EgroupMUX1Data_dbg(3).en & EgroupMUX1Data_dbg(3).hdr_valid & EgroupMUX1Data_dbg(3).hdr & EgroupMUX1Data_dbg(3).valid & EgroupMUX1Data_dbg(3).data,
--    probe40  => EgroupMUX1Data_dbg(4).en & EgroupMUX1Data_dbg(4).hdr_valid & EgroupMUX1Data_dbg(4).hdr & EgroupMUX1Data_dbg(4).valid & EgroupMUX1Data_dbg(4).data,
--    probe41  => EgroupMUX1Data_dbg(5).en & EgroupMUX1Data_dbg(5).hdr_valid & EgroupMUX1Data_dbg(5).hdr & EgroupMUX1Data_dbg(5).valid & EgroupMUX1Data_dbg(5).data,
--    probe42  => EgroupMUX1Data_dbg(6).en & EgroupMUX1Data_dbg(6).hdr_valid & EgroupMUX1Data_dbg(6).hdr & EgroupMUX1Data_dbg(6).valid & EgroupMUX1Data_dbg(6).data,
--    probe43  => EgroupBondData_3_dbg(0).en & EgroupBondData_3_dbg(0).hdr_valid & EgroupBondData_3_dbg(0).hdr & EgroupBondData_3_dbg(0).valid & EgroupBondData_3_dbg(0).data, --array_data64VHHVE_64b66b_type 1+1+2+1+64
--    probe44  => EgroupBondData_3_dbg(1).en & EgroupBondData_3_dbg(1).hdr_valid & EgroupBondData_3_dbg(1).hdr & EgroupBondData_3_dbg(1).valid & EgroupBondData_3_dbg(1).data,
--    probe45  => EgroupBondData_3_dbg(2).en & EgroupBondData_3_dbg(2).hdr_valid & EgroupBondData_3_dbg(2).hdr & EgroupBondData_3_dbg(2).valid & EgroupBondData_3_dbg(2).data,
--    probe46  => EgroupBondData_3_dbg(3).en & EgroupBondData_3_dbg(3).hdr_valid & EgroupBondData_3_dbg(3).hdr & EgroupBondData_3_dbg(3).valid & EgroupBondData_3_dbg(3).data,
--    probe47  => EgroupBondData_3_dbg(4).en & EgroupBondData_3_dbg(4).hdr_valid & EgroupBondData_3_dbg(4).hdr & EgroupBondData_3_dbg(4).valid & EgroupBondData_3_dbg(4).data,
--    probe48  => EgroupBondData_3_dbg(5).en & EgroupBondData_3_dbg(5).hdr_valid & EgroupBondData_3_dbg(5).hdr & EgroupBondData_3_dbg(5).valid & EgroupBondData_3_dbg(5).data,
--    probe49  => DecoderAligned(6) & DecoderAligned(5) & DecoderAligned(4) & DecoderAligned(3) & DecoderAligned(2) & DecoderAligned(1) & DecoderAligned(0),
--    probe50  => DecoderAlignedRmp_dbg(6) & DecoderAlignedRmp_dbg(5) & DecoderAlignedRmp_dbg(4) & DecoderAlignedRmp_dbg(3) & DecoderAlignedRmp_dbg(2) & DecoderAlignedRmp_dbg(1) & DecoderAlignedRmp_dbg(0),
--    probe51  => DecoderAlignedMUX1_dbg(6) & DecoderAlignedMUX1_dbg(5) & DecoderAlignedMUX1_dbg(4) & DecoderAlignedMUX1_dbg(3) & DecoderAlignedMUX1_dbg(2) & DecoderAlignedMUX1_dbg(1) & DecoderAlignedMUX1_dbg(0),
--    probe52  => DecoderAlignedBond_3_dbg(1) & DecoderAlignedBond_3_dbg(0),
--    probe53(0)  => LinkAligned,
--    probe54  => EgroupDecDataSplit_dbg(0).tlast & EgroupDecDataSplit_dbg(0).valid & EgroupDecDataSplit_dbg(0).data, --array_data32VL_64b66b_type 1+1+32
--    probe55  => EgroupDecDataSplit_dbg(1).tlast & EgroupDecDataSplit_dbg(1).valid & EgroupDecDataSplit_dbg(1).data,
--    probe56  => EgroupDecDataSplit_dbg(2).tlast & EgroupDecDataSplit_dbg(2).valid & EgroupDecDataSplit_dbg(2).data,
--    probe57  => EgroupDecDataSplit_dbg(3).tlast & EgroupDecDataSplit_dbg(3).valid & EgroupDecDataSplit_dbg(3).data, --DCSDecData_dbg(3),
--    probe58  => EgroupDecDataSplit_dbg(4).tlast & EgroupDecDataSplit_dbg(4).valid & EgroupDecDataSplit_dbg(4).data,
--    probe59  => EgroupDecDataSplit_dbg(5).tlast & EgroupDecDataSplit_dbg(5).valid & EgroupDecDataSplit_dbg(5).data,
--    probe60  => EgroupDecDataSplit_dbg(6).tlast & EgroupDecDataSplit_dbg(6).valid & EgroupDecDataSplit_dbg(6).data,
--    probe61  => rx_soft_err_i(6) & rx_soft_err_i(5) & rx_soft_err_i(4) & rx_soft_err_i(3) & rx_soft_err_i(2) & rx_soft_err_i(1) & rx_soft_err_i(0),
--    probe62  => DecoderDeskewed(6) & DecoderDeskewed(5) & DecoderDeskewed(4) & DecoderDeskewed(3) & DecoderDeskewed(2) & DecoderDeskewed(1) & DecoderDeskewed(0),
--    probe63  => rx_soft_err_cnt_reg(6) & rx_soft_err_cnt_reg(5) & rx_soft_err_cnt_reg(4) & rx_soft_err_cnt_reg(3) & rx_soft_err_cnt_reg(2) & rx_soft_err_cnt_reg(1) & rx_soft_err_cnt_reg(0)
--    );

--  ila_pixellinklpgbt_2_i: entity work.ila_pixellinklpgbt_2
--    port map
--    (
--      clk           => m_axis_aclk,
--      probe0        => EgroupDecDataSplit_aclk_dbg(0).tlast & EgroupDecDataSplit_aclk_dbg(0).valid & EgroupDecDataSplit_aclk_dbg(0).data, --array_data32VL_64b66b_type 1+1+32
--      probe1        => EgroupDecDataSplit_aclk_dbg(1).tlast & EgroupDecDataSplit_aclk_dbg(1).valid & EgroupDecDataSplit_aclk_dbg(1).data,
--      probe2        => EgroupDecDataSplit_aclk_dbg(2).tlast & EgroupDecDataSplit_aclk_dbg(2).valid & EgroupDecDataSplit_aclk_dbg(2).data,
--      probe3        => EgroupDecDataSplit_aclk_dbg(3).tlast & EgroupDecDataSplit_aclk_dbg(3).valid & EgroupDecDataSplit_aclk_dbg(3).data,
--      probe4        => EgroupDecDataSplit_aclk_dbg(4).tlast & EgroupDecDataSplit_aclk_dbg(4).valid & EgroupDecDataSplit_aclk_dbg(4).data,
--      probe5        => EgroupDecDataSplit_aclk_dbg(5).tlast & EgroupDecDataSplit_aclk_dbg(5).valid & EgroupDecDataSplit_aclk_dbg(5).data,
--      probe6        => EgroupDecDataSplit_aclk_dbg(6).tlast & EgroupDecDataSplit_aclk_dbg(6).valid & EgroupDecDataSplit_aclk_dbg(6).data,
--      probe7        => EgroupMUX2Data_aclk_dbg(0).tlast & EgroupMUX2Data_aclk_dbg(0).valid & EgroupMUX2Data_aclk_dbg(0).data, --array_data32VL_64b66b_type 1+1+32
--      probe8        => EgroupMUX2Data_aclk_dbg(1).tlast & EgroupMUX2Data_aclk_dbg(1).valid & EgroupMUX2Data_aclk_dbg(1).data,
--      probe9        => EgroupMUX2Data_aclk_dbg(2).tlast & EgroupMUX2Data_aclk_dbg(2).valid & EgroupMUX2Data_aclk_dbg(2).data,
--      probe10       => EgroupMUX2Data_aclk_dbg(3).tlast & EgroupMUX2Data_aclk_dbg(3).valid & EgroupMUX2Data_aclk_dbg(3).data,
--      probe11       => EgroupMUX2Data_aclk_dbg(4).tlast & EgroupMUX2Data_aclk_dbg(4).valid & EgroupMUX2Data_aclk_dbg(4).data,
--      probe12       => EgroupMUX2Data_aclk_dbg(5).tlast & EgroupMUX2Data_aclk_dbg(5).valid & EgroupMUX2Data_aclk_dbg(5).data,
--      probe13       => EgroupMUX2Data_aclk_dbg(6).tlast & EgroupMUX2Data_aclk_dbg(6).valid & EgroupMUX2Data_aclk_dbg(6).data,
--      probe14       => EgroupAggrData_30_dbg.tlast & EgroupAggrData_30_dbg.valid & EgroupAggrData_30_dbg.data, --data32VL_64b66b_type 1+1+32
--      probe15       => EgroupAggrData_31_dbg.tlast & EgroupAggrData_31_dbg.valid & EgroupAggrData_31_dbg.data,
--      probe16       => EgroupData_aclk(0).tlast & EgroupData_aclk(0).valid & EgroupData_aclk(0).data, --array_data32VL_64b66b_type 1+1+32
--      probe17       => EgroupData_aclk(1).tlast & EgroupData_aclk(1).valid & EgroupData_aclk(1).data,
--      probe18       => EgroupData_aclk(2).tlast & EgroupData_aclk(2).valid & EgroupData_aclk(2).data,
--      probe19       => EgroupData_aclk(3).tlast & EgroupData_aclk(3).valid & EgroupData_aclk(3).data,
--      probe20       => EgroupData_aclk(4).tlast & EgroupData_aclk(4).valid & EgroupData_aclk(4).data,
--      probe21       => EgroupData_aclk(5).tlast & EgroupData_aclk(5).valid & EgroupData_aclk(5).data,
--      probe22       => EgroupData_aclk(6).tlast & EgroupData_aclk(6).valid & EgroupData_aclk(6).data,
--      probe23       => (others=>'0'), --EgroupDCSData_aclk_dbg(0),
--      probe24       => (others=>'0'), --EgroupDCSData_aclk_dbg(1),
--      probe25       => (others=>'0'), --EgroupDCSData_aclk_dbg(2),
--      probe26       => (others=>'0'), --EgroupDCSData_aclk_dbg(3),
--      probe27       => (others=>'0'), --EgroupDCSData_aclk_dbg(4),
--      probe28       => (others=>'0'), --EgroupDCSData_aclk_dbg(5),
--      probe29       => (others=>'0'), --EgroupDCSData_aclk_dbg(6),

--      probe30        => DecoderAlignedDec_aclk_dbg(6) & DecoderAlignedDec_aclk_dbg(5) & DecoderAlignedDec_aclk_dbg(4) & DecoderAlignedDec_aclk_dbg(3) & DecoderAlignedDec_aclk_dbg(2) & DecoderAlignedDec_aclk_dbg(1) & DecoderAlignedDec_aclk_dbg(0),
--      probe31       => m_axis_dbg(0).tlast & m_axis_dbg(0).tvalid & m_axis_dbg(0).tdata,
--      probe32       => m_axis_dbg(4).tlast & m_axis_dbg(4).tvalid & m_axis_dbg(4).tdata,
--      probe33       => m_axis_dbg(8).tlast & m_axis_dbg(8).tvalid & m_axis_dbg(8).tdata,
--      probe34       => m_axis_dbg(12).tlast & m_axis_dbg(12).tvalid & m_axis_dbg(12).tdata,
--      probe35       => m_axis_dbg(16).tlast & m_axis_dbg(16).tvalid & m_axis_dbg(16).tdata,
--      probe36       => m_axis_dbg(20).tlast & m_axis_dbg(20).tvalid & m_axis_dbg(20).tdata,
--      probe37       => m_axis_dbg(24).tlast & m_axis_dbg(24).tvalid & m_axis_dbg(24).tdata,
--      probe38       => m_axis_dbg(1).tlast & m_axis_dbg(1).tvalid & m_axis_dbg(1).tdata,
--      probe39       => m_axis_dbg(5).tlast & m_axis_dbg(5).tvalid & m_axis_dbg(5).tdata,
--      probe40       => m_axis_dbg(9).tlast & m_axis_dbg(9).tvalid & m_axis_dbg(9).tdata,
--      probe41       => m_axis_dbg(13).tlast & m_axis_dbg(13).tvalid & m_axis_dbg(13).tdata,
--      probe42       => m_axis_dbg(17).tlast & m_axis_dbg(17).tvalid & m_axis_dbg(17).tdata,
--      probe43       => m_axis_dbg(21).tlast & m_axis_dbg(21).tvalid & m_axis_dbg(21).tdata,
--      probe44       => m_axis_dbg(25).tlast & m_axis_dbg(25).tvalid & m_axis_dbg(25).tdata,
--      probe45       => m_axis_tready_dbg(0) &  m_axis_tready_dbg(4) &  m_axis_tready_dbg(8) &  m_axis_tready_dbg(12) &  m_axis_tready_dbg(16) &  m_axis_tready_dbg(20) & m_axis_tready_dbg(24),
--      probe46       => m_axis_tready_dbg(1) &  m_axis_tready_dbg(5) &  m_axis_tready_dbg(9) &  m_axis_tready_dbg(13) &  m_axis_tready_dbg(17) &  m_axis_tready_dbg(21) & m_axis_tready_dbg(25),
--      probe47       => m_axis_prog_empty_dbg(0) &  m_axis_prog_empty_dbg(4) &  m_axis_prog_empty_dbg(8) &  m_axis_prog_empty_dbg(12) &  m_axis_prog_empty_dbg(16) &  m_axis_prog_empty_dbg(20) & m_axis_prog_empty_dbg(24),
--      probe48       => m_axis_prog_empty_dbg(1) &  m_axis_prog_empty_dbg(5) &  m_axis_prog_empty_dbg(9) &  m_axis_prog_empty_dbg(13) &  m_axis_prog_empty_dbg(17) &  m_axis_prog_empty_dbg(21) & m_axis_prog_empty_dbg(25),
--      probe49(0)    => '0',
--      probe50       => cnt_rx_64b66bhdr_160_i,
--      probe51       => cnt_rx_maxishdr_i

--  );
--end generate;

end Behavioral;
