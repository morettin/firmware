--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Filiberto Bonini
--!               Marius Wensing
--!               mtrovato
--!               Elena Zhivun
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 06/06/2019 08:38:50 AM
-- Design Name:
-- Module Name: decoding - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.axi_stream_package.all;
    use work.centralRouter_package.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.pcie_package.all;
    use work.lpgbtfpga_package.all;
    use work.FELIX_gbt_package.all;
    use work.FELIX_package.all;
    use work.interlaken_package.all;
library xpm;
    use xpm.vcomponents.all;

entity decoding is
    generic (
        CARD_TYPE               : integer := 712;
        GBT_NUM                 : integer := 4; --Number of transceiver links
        FIRMWARE_MODE           : integer := 0;
        STREAMS_TOHOST          : integer := 1;           --Number of E-links (1 for FULL mode)
        BLOCKSIZE               : integer := 1024;
        LOCK_PERIOD             : integer := 20480; --Maximum chunk size of 2048 bytes on slowest E-link supported, used for locking 8b10b decoders
        IncludeDecodingEpath2_HDLC      : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath2_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath4_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath8_8b10b     : std_logic_vector(6 downto 0) := "1111111";
        IncludeDecodingEpath16_8b10b    : std_logic_vector(6 downto 0) := "0000000"; --lpGBT only -- @suppress "Unused generic: IncludeDecodingEpath16_8b10b is not used in work.decoding(Behavioral)"
        IncludeDecodingEpath32_8b10b    : std_logic_vector(6 downto 0) := "0000000";  --lpGBT only -- @suppress "Unused generic: IncludeDecodingEpath32_8b10b is not used in work.decoding(Behavioral)"
        IncludeDirectDecoding           : std_logic_vector(6 downto 0) := "0000000";
        RD53Version             : String := "A"; --A or B
        PCIE_ENDPOINT  : integer := 0;
        VERSAL : boolean := false;
        AddFULLMODEForDUNE : boolean := false

    );
    Port (
        RXUSRCLK                       : in  std_logic_vector(GBT_NUM-1 downto 0); --Data clock for FULL mode
        FULL_UPLINK_USER_DATA          : in  txrx33b_type(0 to GBT_NUM-1); --Full mode data input
        GBT_UPLINK_USER_DATA           : in  txrx120b_type(0 to GBT_NUM-1);   --GBT data input
        lpGBT_UPLINK_USER_DATA         : in  txrx224b_type(0 to GBT_NUM-1); --lpGBT data input -- @suppress "Unused port: lpGBT_UPLINK_USER_DATA is not used in work.decoding(Behavioral)"
        lpGBT_UPLINK_EC_DATA           : in  txrx2b_type(0 to GBT_NUM-1);   --lpGBT EC data input
        lpGBT_UPLINK_IC_DATA           : in  txrx2b_type(0 to GBT_NUM-1);   --lpGBT IC data input
        LinkAligned                    : in  std_logic_vector(GBT_NUM-1 downto 0); --Transceiver aligned
        clk160                         : in  std_logic; -- Used for driving aclk and internal processing
        clk240                         : in  std_logic; -- Used for driving aclk and internal processing
        clk250                         : in  std_logic; -- Used for driving aclk and internal processing
        clk40                          : in  std_logic; -- LHC BC Clock
        clk365                         : in  std_logic; -- To drive AXIs64 clock, for logic on 25Gb/s interfaces
        aclk_out                       : out std_logic; -- Driven by decoding
        aresetn                        : in  std_logic; -- Active low reset
        m_axis                         : out axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --Towards CRToHost
        m_axis_tready                  : in  axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --From CRToHost
        m_axis_prog_empty              : out axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --From CRToHost
        m_axis_noSC                    : out axis_32_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1);     --Replication of axi stream for DUNE (no superchunk)
        m_axis_noSC_tready             : in  axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --Replication of axi stream for DUNE (no superchunk)
        m_axis_noSC_prog_empty         : out axis_tready_2d_array_type(0 to GBT_NUM-1, 0 to STREAMS_TOHOST-1); --Replication of axi stream for DUNE (no superchunk)
        TTC_ToHost_Data_in             : in TTC_ToHost_data_type;
        FE_BUSY_out                    : out busyOut_array_type(0 to GBT_NUM-1) := (others => (others => '0'));
        ElinkBusyIn                    : in busyOut_array_type(0 to GBT_NUM-1);
        DmaBusyIn                      : in std_logic;
        FifoBusyIn                     : in std_logic;
        BusySumIn                      : in std_logic;
        m_axis_aux                     : out axis_32_array_type(0 to 1);
        m_axis_aux_prog_empty          : out axis_tready_array_type(0 to 1);
        m_axis_aux_tready              : in axis_tready_array_type(0 to 1);
        register_map_control           : in  register_map_control_type; --Settings (From Wupper)
        register_map_decoding_monitor  : out register_map_decoding_monitor_type;
        Interlaken_RX_Data_In          : in  slv_67_array(0 to GBT_NUM - 1);
        Interlaken_RX_Datavalid        : in  std_logic_vector(GBT_NUM - 1 downto 0);
        Interlaken_RX_Gearboxslip      : out std_logic_vector(GBT_NUM - 1 downto 0);
        Interlaken_Decoder_Aligned_out : out  std_logic_vector(GBT_NUM - 1 downto 0);
        m_axis64                       : out axis_64_array_type(0 to GBT_NUM - 1);
        m_axis64_tready                : in  axis_tready_array_type(0 to GBT_NUM - 1);
        m_axis64_prog_empty            : out axis_tready_array_type(0 to GBT_NUM - 1);
        toHost_axis64_aclk_out         : out std_logic
    --temporary. cbopt will be via reg
    --        CBOPT : in std_logic_vector(3 downto 0);
    --        DIS_LANE_IN                          : in std_logic_vector(3 downto 0);
    --        mask_k_char         : in std_logic_vector(0 to 3)
    );
end decoding;



architecture Behavioral of decoding is

    signal aclk_s : std_logic;
    type RealignmentEvent_type is array(0 to GBT_NUM-1) of std_logic_vector(STREAMS_TOHOST-1 downto 0);
    signal RealignmentEvent: RealignmentEvent_type;
begin
    aclk_out <= aclk_s;

    g_aclk_160: if TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE) = 160 generate
        aclk_s <= clk160;
    elsif  TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE) = 240 generate
        aclk_s <= clk240;
    elsif  TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE) = 250 generate
        aclk_s <= clk250;
    else generate
        error_proc: process
        begin
            report "Unable to calculate the correct ToHost AXI Stream clock frequency based on FIRMWARE_MODE" severity error;
            wait;
        end process;
    end generate;

    --For all firmware modes, there will be a BUSY virtual E-link and a TTCToHost virtual E-link:
    TTCToHostVirtualElink0: entity work.TTCToHostVirtualElink
        generic map(
            BLOCKSIZE => BLOCKSIZE,
            VERSAL => VERSAL
        )
        port map(
            clk40              => clk40,
            aresetn            => aresetn,
            TTC_ToHost_Data_in => TTC_ToHost_Data_in,
            Enable             => register_map_control.TTC_TOHOST_ENABLE(0),
            m_axis             => m_axis_aux(0),
            m_axis_prog_empty  => m_axis_aux_prog_empty(0),
            m_axis_tready      => m_axis_aux_tready(0),
            m_axis_aclk        => aclk_s
        );


    BusyVirtualElink0: entity work.BusyVirtualElink
        generic map(
            GBT_NUM => GBT_NUM,
            BLOCKSIZE => BLOCKSIZE
        )
        port map(
            clk => clk40,
            aresetn => aresetn,
            Orbit => TTC_ToHost_Data_in.orbit,
            BCID => TTC_ToHost_Data_in.BCID,
            ElinkBusyIn => ElinkBusyIn,
            SoftBusyIn => register_map_control.TTC_DEC_CTRL.MASTER_BUSY(0),
            DmaBusyIn => DmaBusyIn,
            FifoBusyIn => FifoBusyIn,
            BusySumIn => BusySumIn,
            Enable => register_map_control.BUSY_TOHOST_ENABLE(0),
            m_axis => m_axis_aux(1),
            m_axis_prog_empty => m_axis_aux_prog_empty(1),
            m_axis_tready => m_axis_aux_tready(1),
            m_axis_aclk => aclk_s
        );

    g_realignment_counters: for link in 0 to GBT_NUM-1 generate
        signal ELINK_REALIGNMENT_STATUS: std_logic_vector(STREAMS_TOHOST-1 downto 0);
        signal ELINK_REALIGNMENT_COUNTER: std_logic_vector(31 downto 0);
    begin
        realignment_cnt_proc: process(clk40)
            variable IncrementCounter: std_logic;
        begin
            if rising_edge(clk40) then
                if aresetn = '0' or to_sl(register_map_control.ELINK_REALIGNMENT.CLEAR_REALIGNMENT_STATUS) = '1' then
                    ELINK_REALIGNMENT_STATUS <= (others => '0');
                    ELINK_REALIGNMENT_COUNTER <= (others => '0');
                else
                    IncrementCounter := '0';
                    for i in 0 to STREAMS_TOHOST-1 loop
                        if RealignmentEvent(link)(i) = '1' then
                            IncrementCounter := '1';
                            ELINK_REALIGNMENT_STATUS(i) <= '1';
                        end if;
                    end loop;
                    if IncrementCounter = '1' then
                        ELINK_REALIGNMENT_COUNTER <= ELINK_REALIGNMENT_COUNTER + 1;
                    end if;
                end if;
            end if;
        end process;
        g_lt12: if link < 12 generate
            register_map_decoding_monitor.ELINK_REALIGNMENT_STATUS(link)(STREAMS_TOHOST-1 downto 0) <= ELINK_REALIGNMENT_STATUS;
            register_map_decoding_monitor.ELINK_REALIGNMENT_COUNT(link) <= ELINK_REALIGNMENT_COUNTER;
        end generate;
    end generate g_realignment_counters;


    g_gbtmode: if FIRMWARE_MODE = FIRMWARE_MODE_GBT or
                  FIRMWARE_MODE = FIRMWARE_MODE_LTDB or
                  FIRMWARE_MODE = FIRMWARE_MODE_FEI4 or
                  FIRMWARE_MODE = FIRMWARE_MODE_FELIG_GBT
                  generate
        signal reset: std_logic;
        signal AlignmentPulseAlign: std_logic;
        signal AlignmentPulseDeAlign: std_logic;
    begin
        reset <= not aresetn;
        g_GBT_Links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableAUX, EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingAUX, BitSwappingEC, BitSwappingIC : std_logic;
            signal MsbFirst : std_logic;
        begin

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto STREAMS_TOHOST) <= (others => '0');


            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            EpathEnableAUX <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).AUX_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING);
            --! The IC bits in the GBTx ASIC are reversed with respect to the other HDLC E-Links (e.g. EC channel), See FLX-2243
            BitSwappingIC <= not to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            BitSwappingAUX <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).AUX_BIT_SWAPPING);
            MsbFirst <= to_sl(register_map_control.DECODING_REVERSE_10B);

            EpathEC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING,
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingEC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_EC_TRUNCATION),
                    ElinkData  => GBT_UPLINK_USER_DATA(link)(113 downto 112),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-2),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-2),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-2),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => GBT_UPLINK_USER_DATA(link)(115 downto 114),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );

            g_ltdb: if FIRMWARE_MODE = FIRMWARE_MODE_LTDB generate
                g_ltdb_prog_empty: for i in 0 to STREAMS_TOHOST-4 generate
                    m_axis_prog_empty(link,i) <= '1';
                    m_axis(link,i) <= (
                        tdata => x"0000_0000",
                        tvalid => '0',
                        tlast => '0',
                        tkeep => "0000",
                        tuser  => "0000"
                    );
                end generate;
                EpathAUX: entity work.DecodingEpathGBT
                    generic map(
                        MAX_INPUT      => 2,
                        INCLUDE_16b    => '0',
                        INCLUDE_8b     => '0',
                        INCLUDE_4b     => '0',
                        INCLUDE_2b     => '1',
                        INCLUDE_8b10b  => '1',
                        INCLUDE_HDLC   => '1',
                        INCLUDE_DIRECT => '0',
                        BLOCKSIZE      => BLOCKSIZE,
                        USE_BUILT_IN_FIFO => '1',
                        GENERATE_FEI4B => false,
                        VERSAL => VERSAL
                    )
                    port map(
                        clk40 => clk40,
                        reset => reset,
                        EpathEnable => EpathEnableAUX,
                        EpathEncoding => "0010", --Only HDLC supported
                        ElinkWidth => "000",
                        MsbFirst   => MsbFirst,
                        ReverseInputBits => BitSwappingAUX,
                        EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_AUX_TRUNCATION),
                        ElinkData  => GBT_UPLINK_USER_DATA(link)(111 downto 110),
                        ElinkAligned => LinkAligned(link),
                        DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-3),
                        AlignmentPulseAlign => AlignmentPulseAlign,
                        AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                        AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                        RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-3),
                        FE_BUSY_out => open,
                        m_axis => m_axis(link,STREAMS_TOHOST-3),
                        m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-3),
                        m_axis_aclk => aclk_s,
                        m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-3)
                    );
            end generate;

            g_noltdb: if FIRMWARE_MODE /= FIRMWARE_MODE_LTDB and
                         FIRMWARE_MODE /= FIRMWARE_MODE_PIXEL generate
                g_Egroups: for egroup in 0 to 4 generate
                    signal ElinkWidth: std_logic_vector(2 downto 0);
                    signal PathEnable: std_logic_vector(7 downto 0);
                    signal PathEncoding : std_logic_vector(31 downto 0);
                    signal ReverseElinks : std_logic_vector(7 downto 0);
                    signal m_axis_s : axis_32_array_type(0 to 7);
                    signal m_axis_tready_s : axis_tready_array_type(0 to 7);
                    signal m_axis_prog_empty_s : axis_tready_array_type(0 to 7);
                    signal EnableEgroupTruncation : std_logic;
                    signal DecoderAligned: std_logic_vector(7 downto 0);
                begin
                    --! Map axis 32 bus for 1 E-group to the complete 2D array.
                    g_axisindex: for i in 0 to 7 generate
                        m_axis(link,egroup*8+i) <= m_axis_s(i);
                        m_axis_tready_s(i) <= m_axis_tready(link,egroup*8+i);
                        m_axis_prog_empty(link,egroup*8+i) <= m_axis_prog_empty_s(i);
                    end generate;

                    PathEncoding <= register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).PATH_ENCODING;
                    ReverseElinks <= register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).REVERSE_ELINKS;
                    ElinkWidth <= register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_WIDTH;
                    PathEnable <= register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).EPATH_ENA;
                    EnableEgroupTruncation <= to_sl(register_map_control.DECODING_EGROUP_CTRL(link mod 12)(egroup).ENABLE_TRUNCATION);

                    --! Instantiate one Egroup.
                    eGroup0: entity work.DecodingEgroupGBT
                        generic map(
                            INCLUDE_16b    => IncludeDecodingEpath16_8b10b(egroup),
                            INCLUDE_8b     => IncludeDecodingEpath8_8b10b(egroup),
                            INCLUDE_4b     => IncludeDecodingEpath4_8b10b(egroup),
                            INCLUDE_2b     => IncludeDecodingEpath2_8b10b(egroup) or IncludeDecodingEpath2_HDLC(egroup),
                            INCLUDE_8b10b  => IncludeDecodingEpath2_8b10b(egroup) or IncludeDecodingEpath4_8b10b(egroup) or IncludeDecodingEpath8_8b10b(egroup),
                            INCLUDE_HDLC   => IncludeDecodingEpath2_HDLC(egroup),
                            INCLUDE_DIRECT => IncludeDirectDecoding(egroup),
                            BLOCKSIZE      => BLOCKSIZE,
                            USE_BUILT_IN_FIFO => x"AA",
                            GENERATE_FEI4B => (FIRMWARE_MODE = FIRMWARE_MODE_FEI4),
                            VERSAL => VERSAL
                        --LOCK_PERIOD    => LOCK_PERIOD
                        )
                        port map(
                            clk40 => clk40,
                            reset => reset,

                            EpathEnable => PathEnable,
                            EpathEncoding => PathEncoding,
                            ElinkWidth => ElinkWidth,
                            MsbFirst         => MsbFirst,
                            ReverseInputBits => ReverseElinks,
                            EnableTruncation => EnableEgroupTruncation,
                            DecoderAligned => DecoderAligned,

                            EGroupData => GBT_UPLINK_USER_DATA(link)((2+egroup)*16+15 downto (2+egroup)*16),
                            GBTAligned => LinkAligned(link),

                            FE_BUSY_out => FE_BUSY_out(link)(8*egroup+7 downto 8*egroup),
                            m_axis => m_axis_s,
                            m_axis_tready => m_axis_tready_s,
                            m_axis_aclk => aclk_s,
                            m_axis_prog_empty => m_axis_prog_empty_s,
                            AlignmentPulseAlign => AlignmentPulseAlign,
                            AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                            AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                            RealignmentEvent => RealignmentEvent(link)(egroup*8+7 downto egroup*8)

                        );
                    register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(egroup*8+7 downto egroup*8) <= DecoderAligned;
                end generate;
            end generate;
        end generate;

        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );
    end generate;

    g_pixelmode: if FIRMWARE_MODE = FIRMWARE_MODE_PIXEL generate
        signal reset: std_logic;
        signal AlignmentPulseAlign: std_logic;
        signal AlignmentPulseDeAlign: std_logic;
    begin
        reset <= not aresetn;
        g_LPGBT_Links: for link in 0 to GBT_NUM-1 generate
            signal MsbFirst : std_logic;
            signal EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingEC, BitSwappingIC : std_logic;
            signal m_axis_s : axis_32_array_type(0 to STREAMS_TOHOST-3);
            signal m_axis_tready_s : axis_tready_array_type(0 to STREAMS_TOHOST-3);
            signal m_axis_prog_empty_s : axis_tready_array_type(0 to STREAMS_TOHOST-3);
            signal cnt_rx_64b66bhdr_i : std_logic_vector(31 downto 0);
            signal SoftErrorEgroupIndex: integer range 0 to 6;
            signal rx_soft_err_cnt_i               : array_32b(0 to 6);
            signal rx_soft_err_rst : std_logic;
            signal DecoderDescewed: std_logic_vector(STREAMS_TOHOST-3 downto 0);
        begin
            --TODO: Include TTC ToHost virtual E-link for link=0
            --TODO: Include XOFF/BUSY virtual E-link for link=0

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto STREAMS_TOHOST) <= (others => '0');
            --! Temporary fix to support 48 channel builds in rm-5.x, registers go up to 24 channels (12 per endpoint)
            g_limit12: if link < 12 generate
                register_map_decoding_monitor.DECODING_LINK_CB(link).DESKEWED(61 downto STREAMS_TOHOST+4) <= (others => '0');
                register_map_decoding_monitor.YARR_DEBUG_ALLEGROUP_TOHOST(link).CNT_RX_PACKET <= cnt_rx_64b66bhdr_i;
                register_map_decoding_monitor.DECODING_LINK_CB(link).DESKEWED(STREAMS_TOHOST+1 downto 4) <= DecoderDescewed;
            end generate;
            SoftErrorEgroupIndex <= to_integer(unsigned(register_map_control.LINK_ERRORS(link).EGROUP_SELECT));
            register_map_decoding_monitor.LINK_ERRORS(link).COUNT <= rx_soft_err_cnt_i(SoftErrorEgroupIndex);
            rx_soft_err_rst <= register_map_control.LINK_ERRORS(link).CLEAR_COUNTERS(register_map_control.LINK_ERRORS(link).CLEAR_COUNTERS'low);

            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            MsbFirst <= to_sl(register_map_control.DECODING_REVERSE_10B);

            EpathEC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING,
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingEC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_EC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_EC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-2),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-2),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-2),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '0',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_IC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );

            --check it against the unchanged version of decoding to see if I
            --screwed up something
            g_LPGBT_egroup: for egroup in 0 to 6 generate
            begin
                --! Map axis 32 bus for 1 E-group to the complete 2D array.
                g_axisindex: for i in 0 to 3 generate
                    m_axis(link,egroup*4+i)            <= m_axis_s(egroup*4+i);
                    m_axis_tready_s(egroup*4+i)           <= m_axis_tready(link,egroup*4+i);
                    m_axis_prog_empty(link,egroup*4+i) <= m_axis_prog_empty_s(egroup*4+i);
                end generate;

            end generate;


            Link0: entity work.DecodingPixelLinkLPGBT
                generic map(
                    CARD_TYPE => CARD_TYPE,
                    RD53Version    => RD53Version,
                    BLOCKSIZE => BLOCKSIZE,
                    LINK => link,
                    SIMU => 0,
                    STREAMS_TOHOST => STREAMS_TOHOST,
                    PCIE_ENDPOINT => PCIE_ENDPOINT,
                    VERSAL         => VERSAL
                )
                port map(
                    clk40                         => clk40,
                    reset                         => reset,
                    MsbFirst                      => MsbFirst,
                    LinkData                      => lpGBT_UPLINK_USER_DATA(link)(223 downto 0),
                    LinkAligned                   => LinkAligned(link),
                    m_axis                        => m_axis_s,
                    m_axis_tready                 => m_axis_tready_s,
                    m_axis_aclk                   => aclk_s,
                    m_axis_prog_empty             => m_axis_prog_empty_s,
                    register_map_control          => register_map_control,
                    --register_map_decoding_monitor => register_map_decoding_monitor
                    DecoderAligned_out            => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-3 downto 0),
                    DecoderDeskewed_out           => DecoderDescewed,
                    cnt_rx_64b66bhdr_out          => cnt_rx_64b66bhdr_i,
                    rx_soft_err_cnt_out           => rx_soft_err_cnt_i,
                    rx_soft_err_rst               => rx_soft_err_rst
                );

        end generate; -- g_LPGBT_Links

        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2048 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );

    end generate; --g_lpgbtmode


    g_bcmmode: if FIRMWARE_MODE = FIRMWARE_MODE_BCM_PRIME generate
        signal reset: std_logic;
        signal AlignmentPulseAlign: std_logic;
        signal AlignmentPulseDeAlign: std_logic;

    begin
        reset <= not aresetn;
        g_LPGBT_Links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingEC, BitSwappingIC : std_logic;
            signal m_axis_s : axis_32_array_type(0 to STREAMS_TOHOST-3);
            signal m_axis_tready_s : axis_tready_array_type(0 to STREAMS_TOHOST-3);
            signal m_axis_prog_empty_s : axis_tready_array_type(0 to STREAMS_TOHOST-3);
            signal MsbFirst : std_logic;
        begin

            -- TODO: ADD Register Asigments

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            -- register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto STREAMS_TOHOST) <= (others => '0');
            -- register_map_decoding_monitor.DECODING_LINK_CB(link).DESKEWED(61 downto STREAMS_TOHOST+4) <= (others => '0');
            -- register_map_decoding_monitor.YARR_DEBUG_ALLEGROUP_TOHOST(link).CNT_RX_PACKET <= cnt_rx_64b66bhdr_i;
            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            MsbFirst <= to_sl(register_map_control.DECODING_REVERSE_10B);

            EpathEC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING,
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingEC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_EC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_EC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-2),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-2),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-2),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '0',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_IC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );

            --check it against the unchanged version of decoding to see if I
            --screwed up something
            g_LPGBT_egroup: for egroup in 0 to 6 generate
            begin
                --! Map axis 32 bus for 1 E-group to the complete 2D array.
                g_axisindex: for i in 0 to 3 generate
                    m_axis(link,egroup*4+i)            <= m_axis_s(egroup*4+i);
                    m_axis_tready_s(egroup*4+i)           <= m_axis_tready(link,egroup*4+i);
                    m_axis_prog_empty(link,egroup*4+i) <= m_axis_prog_empty_s(egroup*4+i);
                end generate;

            end generate;


            Link0: entity work.DecodingBCMLinkLPGBT
                generic map(
                    --CARD_TYPE => CARD_TYPE,
                    BLOCKSIZE => BLOCKSIZE,
                    LINK => link,
                    --SIMU => 0,
                    STREAMS_TOHOST => STREAMS_TOHOST,
                    AVALIABLE_CLK_CYCLES => TOHOST_AXI_STREAM_FREQ(FIRMWARE_MODE_BCM_PRIME)/40,
                    --PCIE_ENDPOINT => PCIE_ENDPOINT,
                    VERSAL => VERSAL
                )
                port map(
                    clk40                         => clk40,
                    reset                         => reset,
                    LinkData                      => lpGBT_UPLINK_USER_DATA(link)(223 downto 0),
                    LinkAligned                   => LinkAligned(link),
                    m_axis                        => m_axis_s,
                    m_axis_tready                 => m_axis_tready_s,
                    m_axis_aclk                   => aclk_s,
                    m_axis_prog_empty             => m_axis_prog_empty_s,
                    register_map_control          => register_map_control,
                    TTC_ToHostData                => TTC_ToHost_Data_in,--,
                    L1AWindow                     => register_map_control.DECODING_BCM_PRIME_L1A(link).WINDOW,
                    L1ADelay                      => register_map_control.DECODING_BCM_PRIME_L1A(link).DELAY
                --DecoderAligned                => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-3 downto 0),
                --DecoderDeskewed               => register_map_decoding_monitor.DECODING_LINK_CB(link).DESKEWED(STREAMS_TOHOST+1 downto 4)
                );
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-3 downto 0) <= (others => '1');
            register_map_decoding_monitor.DECODING_LINK_CB(link).DESKEWED(STREAMS_TOHOST+1 downto 4) <= (others => '1');

        end generate; -- g_LPGBT_Links


        -- Ismet?? Understand what this does ??? ---
        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2048 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );

    end generate; --g_bcmmode


    g_fullmode: if FIRMWARE_MODE = FIRMWARE_MODE_FULL generate
        g_decoding: for i in 0 to GBT_NUM-1 generate
            signal fullmode_FE_BUSY: std_logic;
        begin
            FE_BUSY_SYNC: xpm_cdc_single generic map(
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                ) port map(
                    src_clk => RXUSRCLK(i),
                    src_in => fullmode_FE_BUSY,
                    dest_clk => clk40,
                    dest_out => FE_BUSY_out(i)(0)
                );

            decoding0: entity work.FullToAxis
                generic map(
                    BLOCKSIZE => BLOCKSIZE,
                    VERSAL => VERSAL
                )
                port map(
                    clk240 => RXUSRCLK(i),
                    FMdin => FULL_UPLINK_USER_DATA(i), --: in std_logic_vector(32 downto 0);
                    FMdin_is_big_endian => to_sl(register_map_control.DECODING_ENDIANNESS_FULL_MODE),
                    --LinkAligned => LinkAligned(i), --: in std_logic;
                    aclk => aclk_s, --: in std_logic;
                    aresetn => aresetn, --: in std_logic;
                    m_axis => m_axis(i,0), --: out axis_32_type;
                    m_axis_tready => m_axis_tready(i,0), --: in std_logic;
                    m_axis_prog_empty => m_axis_prog_empty(i,0),
                    FE_BUSY_out => fullmode_FE_BUSY,
                    path_ena => register_map_control.DECODING_EGROUP_CTRL(i)(0).EPATH_ENA(0), --: in std_logic;
                    super_chunk_factor_link => register_map_control.SUPER_CHUNK_FACTOR_LINK(i),
                    Use32bSOP => register_map_control.FULLMODE_32B_SOP(0)
                );
            g_NoSuperchunkforDune: if AddFULLMODEForDUNE generate
                decodingNoSC0: entity work.FullToAxis
                    generic map(
                        BLOCKSIZE => BLOCKSIZE,
                        VERSAL => VERSAL
                    )
                    port map(
                        clk240 => RXUSRCLK(i),
                        FMdin => FULL_UPLINK_USER_DATA(i), --: in std_logic_vector(32 downto 0);
                        FMdin_is_big_endian => to_sl(register_map_control.DECODING_ENDIANNESS_FULL_MODE),
                        --LinkAligned => LinkAligned(i), --: in std_logic;
                        aclk => aclk_s, --: in std_logic;
                        aresetn => aresetn, --: in std_logic;
                        m_axis => m_axis_noSC(i,0), --: out axis_32_type;
                        m_axis_tready => m_axis_noSC_tready(i,0), --: in std_logic;
                        m_axis_prog_empty => m_axis_noSC_prog_empty(i,0),
                        FE_BUSY_out => open,
                        path_ena => register_map_control.DECODING_EGROUP_CTRL(i)(0).EPATH_ENA(0), --: in std_logic;
                        super_chunk_factor_link => x"01",
                        Use32bSOP => register_map_control.FULLMODE_32B_SOP(0)
                    );
            end generate g_NoSuperchunkforDune;


            register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(57 downto 1) <= (others => '0');
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(0) <= LinkAligned(i);

        --CR_TOHOST_GBT_MON(i).EPATH0_ALMOST_FULL(0) <= not m_axis_tready(i,0);

        end generate;
    end generate;

    g_strips: if FIRMWARE_MODE = FIRMWARE_MODE_STRIP generate
        signal reset : std_logic;
        signal AlignmentPulseAlign : std_logic;
        signal AlignmentPulseDeAlign: std_logic;
        signal amac_invert_polarity_in : std_logic;
    begin
        amac_invert_polarity_in <= register_map_control.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_IN(
                                                                                            register_map_control.GLOBAL_STRIPS_CONFIG.INVERT_AMAC_IN'low);
        reset <= not aresetn;
        g_GBT_Links: for link in 0 to GBT_NUM-1 generate
            signal amac_rst, EpathEnableIC : std_logic;
            signal BitSwappingIC : std_logic;
            signal MsbFirst : std_logic;
            signal fromDeglitcher : std_logic;
            signal rx_8b10b_err_cnt_i : array_32b(0 to 6);
            signal SoftErrorEgroupIndex: integer range 0 to 6;
        begin
            --TODO: Include TTC ToHost virtual E-link for link=0
            --TODO: Include XOFF/BUSY virtual E-link for link=0

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto 42) <= (others => '0');
            amac_rst <= not to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            MsbFirst <= '1'; --to_sl(register_map_control.DECODING_REVERSE_10B);

            EDeglitcher: entity work.EndeavourDeglitcher
                port map
                (
                    clk40   => clk40,
                    rst     => amac_rst,
                    datain  => lpGBT_UPLINK_EC_DATA(link),
                    dataout => fromDeglitcher
                );

            Edecoding0: entity work.EndeavourDecoder
                generic map (
                    DEBUG_en => false,
                    VERSAL => VERSAL
                )
                port map
                (
                    clk40 => clk40,
                    m_axis_aclk => aclk_s, --: in std_logic;
                    amac_signal => fromDeglitcher, --: in std_logic_vector(32 downto 0);
                    LinkAligned => LinkAligned(link), --: in std_logic;
                    aresetn => aresetn, --: in std_logic;
                    rst => amac_rst,
                    m_axis => m_axis(link,STREAMS_TOHOST-2), --m_axis(i,0), --: out axis_32_type;
                    invert_polarity => amac_invert_polarity_in,
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2), --m_axis_tready(i,0) --: in std_logic;
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b => '0',
                    INCLUDE_4b => '0',
                    INCLUDE_2b => '1',
                    INCLUDE_8b10b => '1',
                    INCLUDE_HDLC => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_IC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );

            g_LPGBT_Egroups: for egroup in 0 to 6 generate
                signal ElinkWidth: std_logic_vector(2 downto 0);
                signal PathEnable: std_logic_vector(3 downto 0);     --4elinks
                --signal not_PathEnable : std_logic_vector(3 downto 0);
                signal ReverseElinks : std_logic_vector(3 downto 0);

                signal EGroupData : std_logic_vector(31 downto 0);
                signal m_axis_s : axis_32_array_type(0 to 3);
                signal m_axis_tready_s : axis_tready_array_type(0 to 3);
                signal m_axis_prog_empty_s : axis_tready_array_type(0 to 3);
                signal DecoderAligned : std_logic_vector(3 downto 0);

                signal PathEncoding : std_logic_vector(15 downto 0);--akamensh
            --
            begin
                ----------------------------------------------------------------------
                -- Strips data decoding is the same as lpGBT mode
                -- only 8b10b 320 Mbps (and 640 Mbps in the future) support is required
                ----------------------------------------------------------------------

                --Shouldn't we have different registers for LPGBT?
                PathEncoding <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).PATH_ENCODING(26 downto 11);
                ReverseElinks <= (others => '0'); --register_map_control.DECODING_EGROUP_CTRL(link)(egroup).REVERSE_ELINKS(46 downto 43);
                ElinkWidth <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).EPATH_WIDTH;
                PathEnable <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).EPATH_ENA(3 downto 0);
                EGroupData <= lpGBT_UPLINK_USER_DATA(link)(egroup*32+31 downto egroup*32);
                register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(egroup*4+3 downto egroup*4) <= DecoderAligned;
                --not_PathEnable <= not PathEnable;

                decoderEgroup8b10b: entity work.DecEgroup_8b10b
                    generic map(
                        BLOCKSIZE => BLOCKSIZE,
                        Support32bWidth => '0',
                        Support16bWidth => '1',
                        Support8bWidth => '1',
                        IncludeElinks => "0101",
                        VERSAL => VERSAL
                    )
                    port map(
                        clk40 => clk40,
                        reset => reset,

                        DataIn =>  EGroupData,

                        EnableIn => PathEnable,
                        LinkAligned => LinkAligned(link),
                        ElinkWidth => ElinkWidth,
                        AlignmentPulseAlign => AlignmentPulseAlign,
                        AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                        AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                        RealignmentEvent => RealignmentEvent(link)(egroup*4+3 downto egroup*4),
                        PathEncoding => PathEncoding,
                        ElinkAligned => DecoderAligned,
                        DecodingErrors => rx_8b10b_err_cnt_i(egroup),
                        MsbFirst => MsbFirst,
                        ReverseInputBits => ReverseElinks,
                        HGTD_ALTIROC_DECODING => '0',
                        FE_BUSY_out => open, --ITk strips FE don't report BUSY
                        m_axis => m_axis_s,
                        m_axis_tready => m_axis_tready_s,
                        m_axis_aclk => aclk_s,
                        m_axis_prog_empty => m_axis_prog_empty_s
                    );


                elinks_8b10b : for elink in 0 to 3 generate
                    --! Map axis 32 bus for 1 E-group to the complete 2D array.
                    m_axis(link, egroup*4 + elink) <= m_axis_s(elink);
                    m_axis_tready_s(elink) <= m_axis_tready(link, egroup*4 + elink);
                    m_axis_prog_empty(link, egroup*4 + elink) <= m_axis_prog_empty_s(elink);

                end generate;

            end generate; -- g_LPGBT_Egroups
            SoftErrorEgroupIndex <= to_integer(unsigned(register_map_control.LINK_ERRORS(link).EGROUP_SELECT));
            register_map_decoding_monitor.LINK_ERRORS(link).COUNT <= rx_8b10b_err_cnt_i(SoftErrorEgroupIndex);
        end generate; -- ITk strips decoder

        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );
    end generate;

    g_lpgbt8b10b: if FIRMWARE_MODE = FIRMWARE_MODE_LPGBT or
                     FIRMWARE_MODE = FIRMWARE_MODE_FELIG_LPGBT generate
        signal reset: std_logic;
        signal AlignmentPulseAlign: std_logic;
        signal AlignmentPulseDeAlign: std_logic;
    begin
        reset <= not aresetn;
        g_lpGBT_Links: for link in 0 to GBT_NUM-1 generate
            signal EpathEnableEC, EpathEnableIC : std_logic;
            signal BitSwappingEC, BitSwappingIC : std_logic;
            signal MsbFirst : std_logic;
            signal rx_8b10b_err_cnt_i : array_32b(0 to 6);
            signal SoftErrorEgroupIndex: integer range 0 to 6;
        begin

            --The aligned status bits have more bits than we need, to account for wide mode etc.
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(57 downto STREAMS_TOHOST) <= (others => '0');


            EpathEnableEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_ENABLE);
            EpathEnableIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_ENABLE);
            BitSwappingEC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).EC_BIT_SWAPPING);
            BitSwappingIC <= to_sl(register_map_control.MINI_EGROUP_TOHOST(link).IC_BIT_SWAPPING);
            MsbFirst <= to_sl(register_map_control.DECODING_REVERSE_10B);

            EpathEC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnableEC,
                    EpathEncoding => register_map_control.MINI_EGROUP_TOHOST(link).EC_ENCODING,
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingEC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_EC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_EC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-2),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-2),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-2),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-2),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-2)
                );

            EpathIC: entity work.DecodingEpathGBT
                generic map(
                    MAX_INPUT      => 2,
                    INCLUDE_16b    => '0',
                    INCLUDE_8b     => '0',
                    INCLUDE_4b     => '0',
                    INCLUDE_2b     => '1',
                    INCLUDE_8b10b  => '1',
                    INCLUDE_HDLC   => '1',
                    INCLUDE_DIRECT => '0',
                    BLOCKSIZE      => BLOCKSIZE,
                    USE_BUILT_IN_FIFO => '1',
                    GENERATE_FEI4B => false,
                    VERSAL => VERSAL
                )
                port map(
                    clk40 => clk40,
                    reset => reset,
                    EpathEnable => EpathEnableIC,
                    EpathEncoding => "0010", --Only support HDLC
                    ElinkWidth => "000",
                    MsbFirst   => MsbFirst,
                    ReverseInputBits => BitSwappingIC,
                    EnableTruncation => to_sl(register_map_control.MINI_EGROUP_TOHOST(link).ENABLE_IC_TRUNCATION),
                    ElinkData  => lpGBT_UPLINK_IC_DATA(link),
                    ElinkAligned => LinkAligned(link),
                    DecoderAligned => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(STREAMS_TOHOST-1),
                    AlignmentPulseAlign => AlignmentPulseAlign,
                    AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                    AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                    RealignmentEvent => RealignmentEvent(link)(STREAMS_TOHOST-1),
                    FE_BUSY_out => open,
                    m_axis => m_axis(link,STREAMS_TOHOST-1),
                    m_axis_tready => m_axis_tready(link,STREAMS_TOHOST-1),
                    m_axis_aclk => aclk_s,
                    m_axis_prog_empty => m_axis_prog_empty(link,STREAMS_TOHOST-1)
                );

            g_Egroups: for egroup in 0 to 6 generate
                signal ElinkWidth: std_logic_vector(2 downto 0);
                signal PathEnable: std_logic_vector(3 downto 0);
                signal PathEncoding : std_logic_vector(15 downto 0);
                signal ReverseElinks : std_logic_vector(3 downto 0);
                signal m_axis_s : axis_32_array_type(0 to 3);
                signal m_axis_tready_s : axis_tready_array_type(0 to 3);
                signal m_axis_prog_empty_s : axis_tready_array_type(0 to 3);
            begin
                --! Map axis 32 bus for 1 E-group to the complete 2D array.
                g_axisindex: for i in 0 to 3 generate
                    m_axis(link,egroup*4+i) <= m_axis_s(i);
                    m_axis_tready_s(i) <= m_axis_tready(link,egroup*4+i);
                    m_axis_prog_empty(link,egroup*4+i) <= m_axis_prog_empty_s(i);
                end generate;
                --TODO: Path Encoding and Reverse Elinks not implemented in DecEgroup_8b10b
                PathEncoding <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).PATH_ENCODING(26 downto 11);
                ReverseElinks <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).REVERSE_ELINKS(46 downto 43);
                ElinkWidth <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).EPATH_WIDTH;
                PathEnable <= register_map_control.DECODING_EGROUP_CTRL(link)(egroup).EPATH_ENA(3 downto 0);

                --! Instantiate one Egroup.
                eGroup0: entity work.DecEgroup_8b10b
                    generic map(
                        BLOCKSIZE => BLOCKSIZE,
                        Support32bWidth  => IncludeDecodingEpath32_8b10b(egroup),
                        Support16bWidth  => IncludeDecodingEpath16_8b10b(egroup),
                        Support8bWidth   => IncludeDecodingEpath8_8b10b(egroup),
                        IncludeElinks => "1111",
                        VERSAL => VERSAL
                    )
                    port map(
                        clk40             => clk40,
                        reset             => reset,
                        DataIn            => lpGBT_UPLINK_USER_DATA(link)(egroup*32+31 downto egroup*32),
                        EnableIn          => PathEnable,
                        LinkAligned       => LinkAligned(link),
                        ElinkWidth        => ElinkWidth,
                        AlignmentPulseAlign => AlignmentPulseAlign,
                        AlignmentPulseDeAlign => AlignmentPulseDeAlign,
                        AutoRealign => to_sl(register_map_control.ELINK_REALIGNMENT.ENABLE),
                        RealignmentEvent => RealignmentEvent(link)(egroup*4+3 downto egroup*4),
                        PathEncoding      => PathEncoding,
                        ElinkAligned      => register_map_decoding_monitor.DECODING_LINK_ALIGNED(link)(egroup*4+3 downto egroup*4),
                        DecodingErrors => rx_8b10b_err_cnt_i(egroup),
                        MsbFirst          => MsbFirst,
                        ReverseInputBits  => ReverseElinks,
                        HGTD_ALTIROC_DECODING => to_sl(register_map_control.DECODING_HGTD_ALTIROC),
                        FE_BUSY_out => FE_BUSY_out(link)(4*egroup+3 downto 4*egroup),
                        m_axis            => m_axis_s,
                        m_axis_tready     => m_axis_tready_s,
                        m_axis_aclk       => aclk_s,
                        m_axis_prog_empty => m_axis_prog_empty_s
                    );
            end generate g_Egroups;
            SoftErrorEgroupIndex <= to_integer(unsigned(register_map_control.LINK_ERRORS(link).EGROUP_SELECT));
            register_map_decoding_monitor.LINK_ERRORS(link).COUNT <= rx_8b10b_err_cnt_i(SoftErrorEgroupIndex);
        end generate g_lpGBT_Links;

        pulsegen0: entity work.AlignmentPulseGen
            generic map(
                MAX_VAL_DEALIGN => LOCK_PERIOD, --2048 bytes at 8b10b / 80 Mb/s elink.
                MAX_VAL_ALIGN => 20 --2 bytes at 8b10b / 80 Mb/s elink.
            )
            port map(
                clk40 => clk40,
                AlignmentPulseAlign => AlignmentPulseAlign,
                AlignmentPulseDeAlign => AlignmentPulseDeAlign
            );
    end generate g_lpgbt8b10b;

    g_interlaken: if FIRMWARE_MODE = FIRMWARE_MODE_INTERLAKEN generate
        signal reset: std_logic;
        signal FlowControl: slv_16_array(0 to GBT_NUM-1); -- @suppress "signal FlowControl is never read", @TODO
        signal Decoder_Lock, Descrambler_Lock, HealthLane : std_logic_vector(GBT_NUM-1 downto 0);
        signal decoder_error_sync                         : std_logic_vector(GBT_NUM-1 downto 0);
        signal descrambler_error_badsync                  : std_logic_vector(GBT_NUM-1 downto 0);
        signal descrambler_error_statemismatch            : std_logic_vector(GBT_NUM-1 downto 0);
        signal descrambler_error_nosync                   : std_logic_vector(GBT_NUM-1 downto 0);
        signal burst_crc24_error                          : std_logic_vector(GBT_NUM-1 downto 0);
        signal meta_crc32_error                           : std_logic_vector(GBT_NUM-1 downto 0);
        signal HealthInterface: std_logic_vector(0 downto 0);
        --signal Detected_Lane_Number : slv_4_array(0 to GBT_NUM-1);
        signal PacketLength : std_logic_vector(11 downto 0);
    --signal Autodetect_Lane_Number: std_logic;
    begin

        reset <= not aresetn;
        toHost_axis64_aclk_out <= clk365;

        PacketLength <= register_map_control.INTERLAKEN_CONTROL.PACKET_LENGTH;
        --Autodetect_Lane_Number <= register_map_control.INTERLAKEN_CONTROL.AUTODETECT_LANE_NUMBER(12);

        il0: entity work.Interlaken_Receiver_multiChannel generic map(
                Lanes => GBT_NUM
            )
            port map(
                clk => RXUSRCLK,
                reset => reset,
                RX_Data_In => Interlaken_RX_Data_In,
                FlowControl => FlowControl,
                RX_Datavalid => Interlaken_RX_Datavalid,
                Bitslip => Interlaken_RX_Gearboxslip,
                m_axis_deburst => open,
                m_axis_aclk => clk365,
                m_axis => m_axis64,
                m_axis_tready => m_axis64_tready,
                m_axis_prog_empty => m_axis64_prog_empty,
                Descrambler_lock => Descrambler_Lock ,
                Decoder_Lock => Decoder_Lock,
                decoder_error_sync => decoder_error_sync,
                descrambler_error_badsync => descrambler_error_badsync,
                descrambler_error_statemismatch => descrambler_error_statemismatch,
                descrambler_error_nosync => descrambler_error_nosync,
                burst_crc24_error => burst_crc24_error,
                meta_crc32_error => meta_crc32_error,
                PacketLength => PacketLength,
                --Autodetect_Lane_Number => Autodetect_Lane_Number,
                --Detected_Lane_Number => Detected_Lane_Number,
                HealthLane => HealthLane ,
                HealthInterface => HealthInterface(0)
            );

        register_map_decoding_monitor.INTERLAKEN_CONTROL.HEALTH_INTERFACE <= HealthInterface;

        g_monitor: for i in 0 to GBT_NUM-1 generate
            signal decoder_error_sync_latch, descrambler_error_badsync_latch,
                descrambler_error_statemismatch_latch, descrambler_error_nosync_latch,
                burst_crc24_error_latch, meta_crc32_error_latch: std_logic_vector(0 downto 0);
            signal clear_status: std_logic;
        begin
            --@TODO: Create dedicated interlaken registers for monitoring with a more logical meaning.
            Interlaken_Decoder_Aligned_out(i) <= Decoder_Lock(i) and Descrambler_Lock(i);
            register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(0) <= Decoder_Lock(i) and Descrambler_Lock(i);

            sync_clear_status: xpm_cdc_single generic map(
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0,
                    SRC_INPUT_REG => 0
                )
                port map(
                    src_clk => '0',
                    src_in => to_sl(register_map_control.INTERLAKEN_STATUS(i).CLEAR_STATUS),
                    dest_clk => RXUSRCLK(i),
                    dest_out => clear_status
                );

            latch_status_proc: process(RXUSRCLK(i))
            begin
                if rising_edge(RXUSRCLK(i)) then
                    if clear_status = '1' then
                        decoder_error_sync_latch <= "0";
                        descrambler_error_badsync_latch <= "0";
                        descrambler_error_statemismatch_latch <= "0";
                        descrambler_error_nosync_latch <= "0";
                        burst_crc24_error_latch <= "0";
                        meta_crc32_error_latch <= "0";
                    else
                        if decoder_error_sync(i) = '1' then
                            decoder_error_sync_latch <= "1";
                        end if;
                        if descrambler_error_badsync(i) = '1' then
                            descrambler_error_badsync_latch <= "1";
                        end if;
                        if descrambler_error_statemismatch(i) = '1' then
                            descrambler_error_statemismatch_latch <= "1";
                        end if;
                        if descrambler_error_nosync(i) = '1' then
                            descrambler_error_nosync_latch <= "1";
                        end if;
                        if burst_crc24_error(i) = '1' then
                            burst_crc24_error_latch <= "1";
                        end if;
                        if meta_crc32_error(i) = '1' then
                            meta_crc32_error_latch <= "1";
                        end if;
                    end if;
                end if;
            end process;


            --register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(1) <= Descrambler_Lock(i);
            --register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(2) <= HealthLane(i);
            --register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(3) <= HealthInterface;
            --register_map_decoding_monitor.DECODING_LINK_ALIGNED(i)(7 downto 4) <= Detected_Lane_Number(i);
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DECODER_ERROR_SYNC <= decoder_error_sync_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DESCRAMBLER_ERROR_BADSYNC <= descrambler_error_badsync_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DESCRAMBLER_ERROR_STATEMISMATCH <= descrambler_error_statemismatch_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DESCRAMBLER_ERROR_NOSYNC <= descrambler_error_nosync_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).BURST_CRC24_ERROR <= burst_crc24_error_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).META_CRC32_ERROR <= meta_crc32_error_latch;
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DECODER_ALIGNED <= Decoder_Lock(i downto i);
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).DESCRAMBLER_ALIGNED <= Descrambler_Lock(i downto i);
            register_map_decoding_monitor.INTERLAKEN_STATUS(i).HEALTH_LANE <= HealthLane(i downto i);

        --register_map_decoding_monitor.INTERLAKEN_STATUS(i).HEALTH_INTERFACE(3) <= HealthInterface;
        --register_map_decoding_monitor.INTERLAKEN_STATUS(i).DETECTED_LANE <= Detected_Lane_Number(i);

        end generate g_monitor;
    end generate g_interlaken;

end Behavioral;
