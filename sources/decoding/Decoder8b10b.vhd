--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Julia Narevicius
--!               Israel Grayzman
--!               Carsten Dülsen
--!               Frans Schreuder
--!               Ton Fleuren
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    06/19/2014
--! Module Name:    dec_8b10_wrap
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use work.centralRouter_package.all;

--! a wrap for 8b10b decoder and alignment
entity Decoder8b10b is
    generic     (
        GENERATE_FEI4B  : boolean := false;
        GENERATE_LCB_ENC : boolean := false; -- DG @ UBC
        AWAIT_SOP : boolean := true --set to false if one E-Link contains multiple decoders
    );
    port (
        DataIn : in std_logic_vector(9 downto 0); --10b Data from GearBox
        DataInValid : in std_logic; -- Data validated by GearBox
        BitSlip : out std_logic; --L For GearBox alignment
        AlignmentPulseAlign : in std_logic; --2 pulses to realign if no K28.5 found
        AlignmentPulseDeAlign : in std_logic; --2 pulses to realign if no K28.5 found
        AutoRealign : in std_logic; --In case of out of table 8b10b characters, the decoder should misalign and start realignment.
        RealignmentEvent : out std_logic; --In case of data errors, the 8b10b decoder is realigning.
        reset : in std_logic; --Active high reset
        clk40 : in std_logic; --BC clock for DataIn
        HGTD_ALTIROC_DECODING: in std_logic; --Set to 1 to use HGTD Altiroc K-Characters for decoding, otherwise use a NSW-like data format.
        DataOut : out std_logic_vector(7 downto 0); --Towards ByteToAxiStream
        DataOutValid : out std_logic; --Towards ByteToAxiStream
        EOP : out std_logic; --End of chunk, 1 clock after last valid byte.
        ElinkSOB : out std_logic; --Elink has sent SOB, indicating busy
        ElinkEOB : out std_logic; --Elink has sent EOB, indicating end-of-busy
        DecoderAligned_out : out std_logic; --Indicator that the 8b10b decoder was successfully aligned.
        DecoderAligned_in : in std_logic; -- Indicated that one of the other linked decoders has successfully aligned
        CodingError : out std_logic;
        DisparityError : out std_logic;
        dispout : out std_logic; --Disparity in/out: Loop back through register to same decoder, or to the next when using parallel decoders
        dispin : in std_logic;   --Disparity in/out: Loop back through register to same decoder, or to the next when using parallel decoders
        ISK_SOC_in : in std_logic;     --Handshake lines to parallel decoders
        ISK_SOC_out : out std_logic;   --Handshake lines to parallel decoders
        ISK_EOC_in : in std_logic;         --Handshake lines to parallel decoders
        ISK_EOC_out : out std_logic;         --Handshake lines to parallel decoders
        ISK_DelimiterA_in : in std_logic;     --Handshake lines to parallel decoders
        ISK_DelimiterB_in : in std_logic;     --Handshake lines to parallel decoders
        ISK_DelimiterA_out : out std_logic;   --Handshake lines to parallel decoders
        ISK_DelimiterB_out : out std_logic;   --Handshake lines to parallel decoders
        CharIsK_in : in std_logic;
        CharIsK_out : out std_logic;
        BitSlipIn : in std_logic --If multiple parallel decoders can perform BitSlip operation to gearbox, feed back bitslip to reset alignment.
    );
end Decoder8b10b;

architecture Behavioral of Decoder8b10b is

    signal DecoderAligned_s: std_logic;


    signal decoder_out : std_logic_vector(7 downto 0) := (others => '0');--Output of 8b10b decoder
    --Second pipeline to align EOP with last output of chunk
    signal CharIsK : std_logic; --Output of 8b10b decoder
    signal ISK_comma : std_logic;
    signal ISK_SOC, ISK_EOC: std_logic; -- FIXME why CHAR_EOC and not ISK_EOC?
    signal Char_EOC, Char_SOB, Char_EOB: std_logic;
    --signal DataInValid_p1 : std_logic; --One pipeline delayed synchronous with 8b10b decoder.
    signal decoder_out_valid : std_logic; --One pipeline delayed synchronous with 8b10b decoder.
    signal decoder_out_valid_no_ReceivingState : std_logic; --One pipeline delayed synchronous with 8b10b decoder.
    signal ISK_DelimiterC : std_logic;

    signal AlignmentCounter : integer range 0 to 3;
    signal disp_err, code_err: std_logic;
    signal DecoderAligned_4s: std_logic;
    signal DecoderAligned_pipe: std_logic_vector(4 downto 0);
begin

    GBT_mode: if (GENERATE_FEI4B = false and GENERATE_LCB_ENC = false) generate
        process(CharIsK, DataIn, decoder_out_valid, HGTD_ALTIROC_DECODING, decoder_out, ISK_comma, CharIsK_in, ISK_EOC, decoder_out_valid_no_ReceivingState)
        begin
            if HGTD_ALTIROC_DECODING = '0' then --NSW-like
                ISK_comma   <=  '1' when (DataIn = COMMAp or DataIn = COMMAn) else '0';
                ISK_SOC     <=  '1' when (DataIn = SOCp   or DataIn = SOCn) else '0';
                ISK_EOC     <=  '1' when (DataIn = EOCp   or DataIn = EOCn) else '0';

                --Char_comma  <=  '1' when (decoder_out = Kchar_comma and CharIsK = '1' and decoder_out_valid = '1') else '0';
                --Char_SOC    <=  '1' when (decoder_out = Kchar_sop   and CharIsK = '1' and decoder_out_valid = '1') else '0';
                Char_EOC    <=  '1' when (decoder_out = Kchar_eop   and CharIsK = '1' and decoder_out_valid = '1') else '0';
                Char_SOB    <=  '1' when (decoder_out = Kchar_sob   and CharIsK = '1' and decoder_out_valid_no_ReceivingState = '1') else '0';
                Char_EOB    <=  '1' when (decoder_out = Kchar_eob   and CharIsK = '1' and decoder_out_valid_no_ReceivingState = '1') else '0';
            else --HGTD Altiroc
                -- DataIn = 10 bits, use both positive and negative options; note that decoder_out is delayed by 1 cycle (with DataValid active) from DataIn
                -- K.28.5 188 BC 1011 1100 10bN 00 1111 1010 10bP 11 0000 0101
                -- K.28.7 252 FC 1111 1100 10bN 00 1111 1000 10bP 11 0000 0111
                ISK_comma   <=  '1' when (DataIn = HGTD_COMMAn or DataIn = HGTD_COMMAp) else '0';
                ISK_SOC <= '1' when ( CharIsK_in = '1' and not ( ISK_comma = '1' or ISK_EOC = '1' ) ) else '0';
                ISK_EOC    <=  '1' when ((DataIn = HGTD_EOPn or DataIn = HGTD_EOPp)) else '0';
                Char_EOC <= '1' when (decoder_out = HGTD_Kchar_eop) and (CharIsK = '1') and (CharIsK_in = '0') else '0';
                --Char_SOC <= '0';
                Char_SOB    <=  '0';
                Char_EOB    <=  '0';
            end if;
        end process;
    end generate GBT_mode;

    FEI4B: if (GENERATE_FEI4B) generate
        ISK_comma   <=  '1' when (DataIn = FEI4B_COMMAp or DataIn = FEI4B_COMMAn) else '0';
        ISK_SOC     <=  '1' when (DataIn = FEI4B_SOCp   or DataIn = FEI4B_SOCn) else '0';
        --Char_comma  <=  '1' when (decoder_out = FEI4B_Kchar_comma and CharIsK = '1' and decoder_out_valid = '1') else '0';
        --Char_SOC    <=  '1' when (decoder_out = FEI4B_Kchar_sop   and CharIsK = '1' and decoder_out_valid = '1') else '0';
        Char_EOC    <=  '1' when (decoder_out = FEI4B_Kchar_eop   and CharIsK = '1' and decoder_out_valid = '1') else '0';
        Char_SOB    <=  '0';
        Char_EOB    <=  '0';
    end generate FEI4B;

    -- DG: block below still configured for FEI4, only names were changed
    LCB: if (GENERATE_LCB_ENC) generate
        ISK_comma   <=  '1' when (DataIn = LCB_COMMAp or DataIn = LCB_COMMAn) else '0';
        ISK_SOC     <=  '1' when (DataIn = FEI4B_SOCp   or DataIn = FEI4B_SOCn) else '0';
        --Char_comma  <=  '1' when (decoder_out = LCB_Kchar_comma and CharIsK = '1' and decoder_out_valid = '1') else '0';
        --Char_SOC    <=  '1' when (decoder_out = LCB_Kchar_sop   and CharIsK = '1' and decoder_out_valid = '1') else '0';
        Char_EOC    <=  '1' when (decoder_out = LCB_Kchar_eop   and CharIsK = '1' and decoder_out_valid = '1') else '0';
        Char_SOB    <=  '0';
        Char_EOB    <=  '0';
    end generate LCB;

    ISK_SOC_out <= ISK_SOC;
    ISK_EOC_out <= ISK_EOC;
    ISK_DelimiterA_out <= ISK_EOC;
    ISK_DelimiterB_out <= ISK_comma;
    ISK_DelimiterC <= ISK_comma;


    --! Alignment of input stream.
    alignment_proc: process(clk40, reset)
        variable AlignmentPulse: std_logic;
        variable DecoderAligned_4v: std_logic;
        variable ReceivingState: std_logic;
    begin
        if reset = '1' then
            AlignmentCounter <= 3;
            DecoderAligned_s <= '0';
            BitSlip <= '0';

            ReceivingState := '0';
        elsif rising_edge(clk40) then
            if DecoderAligned_s = '1' then
                AlignmentPulse := AlignmentPulseDeAlign;
            else
                AlignmentPulse := AlignmentPulseAlign;
            end if;
            if AlignmentPulse = '1' then
                if AlignmentCounter < 2 then
                    AlignmentCounter <= AlignmentCounter + 1;
                elsif AlignmentCounter = 3 then
                    AlignmentCounter <= 0;
                end if;
            end if;
            BitSlip <= '0';
            if DataInValid = '1' then

                --Two consecutive commas come from other decoders, use parallel decoding, comma signal comes from input.
                --Two consecutive commas received, or ISK_comma_in from other decoder if more than one decoder simultaneously.
                if(( (ISK_DelimiterA_in = '1' or HGTD_ALTIROC_DECODING='0') and ISK_DelimiterB_in = '1' and ISK_DelimiterC = '1' ) or DecoderAligned_in = '1' ) then
                    AlignmentCounter <= 0;
                    DecoderAligned_s <= '1';
                elsif AlignmentCounter = 2 then  --Two pulses received, meaning alignment timeout.
                    BitSlip <= '1'; --Rotate the gearbox 1 bit
                    AlignmentCounter <= 3;
                end if;

                if BitSlip = '1' or BitSlipIn = '1' then
                    DecoderAligned_s <= '0';
                end if;
            end if;
            if (Char_EOC = '1' or ISK_EOC_in = '1' ) then
                ReceivingState := '0';
            end if;
            if ((ISK_SOC = '1' or ISK_SOC_in = '1' ) and DecoderAligned_4v = '1') or not AWAIT_SOP then
                ReceivingState := '1';
            end if;
            CodingError <= '0';
            RealignmentEvent <= '0';
            if ((code_err = '1') and (DecoderAligned_s = '1')) and AutoRealign = '1' then
                RealignmentEvent <= '1';
                DecoderAligned_s <= '0';
                if (DecoderAligned_4v = '1') then
                    CodingError <= '1';
                end if;
            end if;
            DisparityError <= '0';
            if ((disp_err = '1') and (DecoderAligned_4v = '1')) then
                DisparityError <= '1';
            end if;
            DecoderAligned_pipe <= DecoderAligned_pipe(3 downto 0) & DecoderAligned_s;
            if DecoderAligned_pipe = "11111" then -- aligned and no coding errors during the last 5 cycles
                DecoderAligned_4v := '1';
            else
                if ( DecoderAligned_4v = '1' ) then -- if previously aligned, force dealignment when 2 out of the last 5 cycles had an alignment or coding error; don't dealign on a single coding error
                    if (DecoderAligned_pipe = "11100" or DecoderAligned_pipe = "11010" or DecoderAligned_pipe = "10110" or DecoderAligned_pipe = "01110" ) then
                        DecoderAligned_4v := '0'; -- 2 coding errors; the newest bit is 0, and there was already one zero in the pipeline, so dealign
                    else
                        DecoderAligned_4v := '1'; -- only one 0 in the pipeline, could be a single bit upset, so don't dealign yet
                    end if;
                else
                    DecoderAligned_4v := '0'; -- was 0, pipeline not all 1, so stays at 0
                end if;
            end if;


            DecoderAligned_4s <= DecoderAligned_4v;
            --DataInValid_p1 <= DataInValid and DecoderAligned_4v; --Only validate signal when Elink is aligned for more than 4 clocks.
            --! Ignore RecievingState for HGTD_ALTIROC_DECODING
            decoder_out_valid <= DataInValid and DecoderAligned_4v and (ReceivingState or HGTD_ALTIROC_DECODING); --Only validate signal when Elink is aligned for more than 4 clocks.
            decoder_out_valid_no_ReceivingState <=  DataInValid and DecoderAligned_4v;
        end if;
    end process;


    DecoderAligned_out <= DecoderAligned_4s;

    -- 8b10b decoder
    dec_8b10b_INST: entity work.dec_8b10b
        PORT MAP(
            reset => reset,
            clk => clk40,
            datain => DataIn,
            datain_valid => DataInValid,
            ko => CharIsK,
            dataout  => decoder_out,
            dispout => dispout,
            dispin => dispin,
            code_err => code_err,
            disp_err => disp_err
        );

    ElinkSOB <= Char_SOB;
    ElinkEOB <= Char_EOB;

    EOP <= Char_EOC;
    DataOut <= decoder_out;
    DataOutValid <= not CharIsK and decoder_out_valid;

    CharIsK_out <= '1' when ISK_comma = '1' or ISK_EOC = '1' else '0';


end Behavioral;

