--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               mtrovato
--!               Carsten Dülsen
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 10/30/2019 03:36:26 PM
-- Design Name:
-- Module Name: EndeavourDecoder - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;
    use work.axi_stream_package.all;
library XPM;
    use XPM.VCOMPONENTS.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ByteToAxiStream is
    generic(
        BYTES: integer := 4;
        BLOCKSIZE : integer := 1024;
        USE_BUILT_IN_FIFO : std_logic := '1';
        VERSAL : boolean := false
    );
    port (
        clk40 : in std_logic; --BC clock for DataIn
        reset : in std_logic; --Acitve high reset
        EnableIn : in std_logic; --Enable epath (register map)
        DataIn : in std_logic_vector(BYTES*8-1 downto 0); --8b Data from Protocol Decoder
        DataInValid : in std_logic_vector(BYTES-1 downto 0); --Data validated by Decoder
        GearboxValid : in std_logic;
        ElinkWidth : in std_logic_vector(2 downto 0);
        EOP : in std_logic_vector(BYTES-1 downto 0); --will be converted to tlast
        SOB : in std_logic_vector(BYTES-1 downto 0); --Add to tuser
        EOB : in std_logic_vector(BYTES-1 downto 0); --Add to tuser
        TruncateIn : in std_logic_vector(BYTES-1 downto 0); --Add to tuser
        CodingErrorIn : in std_logic;
        m_axis : out axis_32_type;  --FIFO read port (axi stream)
        m_axis_tready : in std_logic; --FIFO read tready (axi stream)
        m_axis_aclk : in std_logic; --FIFO read clock (axi stream)
        m_axis_prog_empty : out std_logic --Indication that the FIFO contains a block.
    );
end ByteToAxiStream;

architecture Behavioral of ByteToAxiStream is

    signal s_axis_aresetn: std_logic;
    signal s_axis: axis_32_type;
    signal s_axis_tready: std_logic;

    signal Truncate: std_logic;
    --signal ByteIndex: integer range 0 to 3;

    signal EnableIn_aclk: std_logic;
    signal m_axis_s: axis_32_type;
    signal m_axis_prog_empty_s: std_logic;
--signal ChunkContainsData : std_logic;

begin

    --g_onebyte: if BYTES = 1 generate
    --    signal DataInValid_p1: std_logic;
    --    signal DataIn_p1: std_logic_vector(7 downto 0);
    --begin
    --toaxis_proc: process(s_axis_aresetn, clk40)
    --  variable ElinkBusy_v: std_logic;
    --  variable ClearElinkBusy_v: std_logic;
    --  variable FramingError_v : std_logic;
    --  variable TruncateHandled : std_logic;
    --begin
    --    if s_axis_aresetn = '0' then
    --        s_axis.tdata <= (others => '0');
    --        s_axis.tvalid <= '0';
    --        s_axis.tlast <= '0';
    --        s_axis.tkeep <= "0000";
    --        s_axis.tuser <= "0000";
    --        Truncate <= '0';
    --        ByteIndex <= 0;
    --        TruncateHandled := '0';
    --        ElinkBusy_v := '0';
    --        FramingError_v := '0';
    --        DataInValid_p1 <= '0';
    --        ChunkContainsData <= '0';
    --    elsif rising_edge(clk40) then
    --        if TruncateIn(0) = '1' then
    --            Truncate <= '1';
    --        end if;
    --        if SOB(0) = '1' then
    --            ElinkBusy_v := '1';
    --        end if;
    --        if EOB(0) = '1' then
    --            ClearElinkBusy_v := '1';
    --        end if;
    --        if CodingErrorIn = '1' then
    --            FramingError_v := '1';
    --        end if;
    --        if s_axis_tready = '1' then
    --            s_axis.tvalid <= '0';
    --            if GearboxValid = '1' then
    --                s_axis.tlast <= '0';
    --                s_axis.tkeep <= "1111";
    --                s_axis.tuser <= "0000";
    --                s_axis.tdata(ByteIndex*8+7 downto ByteIndex*8) <= DataIn(7 downto 0);
    --                if DataInValid(0) = '1' then
    --                    ChunkContainsData <= '1';
    --                    if ByteIndex < 3 then
    --                        ByteIndex <= ByteIndex + 1;
    --                    else
    --                        ByteIndex <= 0;
    --                        s_axis.tvalid <= '1';
    --                    end if;
    --                end if;
    --
    --                if (EOP(0) = '1' or Truncate = '1') and ChunkContainsData = '1' then
    --                    ChunkContainsData <= '0';
    --                    if Truncate = '1' then
    --                        if TruncateHandled = '0' then
    --                            TruncateHandled := '1';
    --                            s_axis.tvalid <= '1';
    --                        else
    --                            s_axis.tvalid <= '0';
    --                        end if;
    --                        if EOP(0) = '1' then
    --                            Truncate <= '0';
    --                            TruncateHandled := '0';
    --                        end if;
    --                    elsif EOP(0) = '1' then
    --                        s_axis.tvalid <= '1';
    --                    end if;
    --                    s_axis.tlast <= '1';
    --                    s_axis.tuser(3) <= Truncate;
    --                    s_axis.tuser(2) <= ElinkBusy_v;
    --                    s_axis.tuser(1) <= FramingError_v;
    --                    s_axis.tuser(0) <= '0'; --CRC not used in this mode.
    --                    s_axis.tkeep <= (others => '0');
    --                    for i in 0 to 3 loop
    --                        if i < ByteIndex or ByteIndex = 0 then
    --                            s_axis.tkeep(i) <= '1';
    --                        end if;
    --                    end loop;
    --                    if ClearElinkBusy_v = '1' then
    --                        ElinkBusy_v := '0';
    --                        ClearElinkBusy_v := '0';
    --                    end if;
    --                    FramingError_v := '0';
    --                    ByteIndex <= 0; --Start a fresh 32b word for the next packet.
    --                end if;
    --            end if;
    --        else --tready = 0
    --            if DataInValid(0) = '1' then
    --                Truncate <= '1';
    --            end if;
    --        end if;
    --    end if;
    --end process;
    --end generate;


    g_multibyte: if BYTES >= 1 generate
        signal s_axis_p0: axis_32_type;
        signal s_axis_p1: axis_32_type;
        signal s_axis_p2: axis_32_type;
        signal TruncateHandled : std_logic;
    begin

        --    s_axis.tdata <= shiftData(31 downto 0);
        --    s_axis.tvalid <= shiftValid(0);
        --    s_axis.tlast <= shiftLast(0);
        --    s_axis.tkeep <= shiftKeep(3 downto 0);
        --    s_axis.tuser <= shiftTrunc(0) & shiftBusy(0) & shiftFrErr(0) & '0';
        s_axis.tdata  <= s_axis_p2.tdata;
        s_axis.tkeep  <= s_axis_p2.tkeep;
        s_axis.tvalid <= s_axis_p2.tvalid;
        s_axis.tuser  <= s_axis_p2.tuser;
        s_axis.tlast  <= '1' when s_axis_p1.tvalid = '0' and s_axis_p1.tlast = '1' else s_axis_p2.tlast;

        toaxis_proc: process(s_axis_aresetn, clk40)
            variable shiftData_v   : std_logic_vector(31 downto 0);
            variable shiftKeep_v   : std_logic_vector(3 downto 0);
            variable ElinkBusy_v: std_logic;
            variable ClearElinkBusy_v: std_logic;
            variable FramingError_v : std_logic;
            variable outputNow, outputNext : std_logic;
            variable maxBytes: integer range 0 to BYTES-1;
            variable ChunkContainsData_v : std_logic;
        begin
            if s_axis_aresetn = '0' then
                shiftKeep_v := (others => '0');
                shiftData_v := (others => '0');
                ElinkBusy_v := '0';
                ClearElinkBusy_v := '0';
                FramingError_v := '0';
                s_axis_p0.tdata <= x"00000000";
                s_axis_p0.tkeep <= x"0";
                s_axis_p0.tuser <= x"0";
                s_axis_p0.tlast <= '0';
                s_axis_p0.tvalid <= '0';
                s_axis_p1.tdata <= x"00000000";
                s_axis_p1.tkeep <= x"0";
                s_axis_p1.tuser <= x"0";
                s_axis_p1.tlast <= '0';
                s_axis_p1.tvalid <= '0';
                s_axis_p2.tdata <= x"00000000";
                s_axis_p2.tkeep <= x"0";
                s_axis_p2.tuser <= x"0";
                s_axis_p2.tlast <= '0';
                s_axis_p2.tvalid <= '0';

                Truncate <= '0';
                ChunkContainsData_v := '0';
                TruncateHandled <= '0';
                outputNow := '0';
                outputNext := '0';
            elsif rising_edge(clk40) then
                if s_axis_tready = '1'  then
                    if ( GearboxValid = '1') then
                        s_axis_p0.tdata <= x"00000000";
                        s_axis_p0.tlast <= '0';
                        s_axis_p0.tvalid <= '0';
                        s_axis_p0.tkeep <= "0000";
                        s_axis_p0.tuser <= "0000";
                        s_axis_p1 <= s_axis_p0; --pipeline 1
                        s_axis_p2 <= s_axis_p1;    --pipeline 2
                        if BYTES = 4 and ElinkWidth = "100" then
                            maxBytes := 3;
                        elsif BYTES >= 2 and ElinkWidth = "011" then
                            maxBytes := 1;
                        else
                            maxBytes := 0;
                        end if;

                        if Truncate = '0' then
                            for i in 0 to BYTES-1 loop
                                if TruncateIn(i) = '1' then
                                    Truncate <= '1';
                                    TruncateHandled <= '0';
                                end if;
                                outputNow := '0';
                                outputNext := '0';
                                if SOB(i) = '1' then
                                    ElinkBusy_v := '1';
                                end if;
                                if EOB(i) = '1' then
                                    ClearElinkBusy_v := '1';
                                end if;
                                if CodingErrorIn = '1' then
                                    FramingError_v := '1';
                                end if;
                                if DataInValid(i) = '1' then
                                    shiftData_v(23 downto 0) := shiftData_v(31 downto 8); --Shift 1 byte
                                    shiftData_v(31 downto 24) := DataIn(i*8+7 downto i*8); --Add new data in MSB
                                    shiftKeep_v(2 downto 0) := shiftKeep_v(3 downto 1);
                                    shiftKeep_v(3) := '1';
                                    ChunkContainsData_v := '1';
                                end if;

                                if shiftKeep_v(0) = '1' and i /= maxBytes then --If we have our shift register filled somehwere in the middle, output immediately.
                                    if s_axis_p0.tvalid = '1' then --Already data in the pipeline, we have to shift it in the pipeline
                                        outputNext := '1';
                                    else
                                        outputNow := '1';
                                    end if;
                                end if;




                                if EOP(i) = '1' and ChunkContainsData_v = '1' then
                                    ChunkContainsData_v := '0';
                                    case shiftKeep_v is
                                        when "0000" =>
                                            if s_axis_p0.tvalid = '1' and i = maxBytes then --Already data in the pipeline, we have to shift it in the pipeline
                                                s_axis_p0.tlast <= '1';
                                            else
                                                s_axis_p1.tlast <= '1'; --If EOP arrives in the next cycle, put it after the pipeline as tlast
                                            end if;
                                        when "1000" =>
                                            s_axis_p0.tlast <= '1';
                                            shiftData_v := x"000000" & shiftData_v(31 downto 24);
                                            shiftKeep_v := "000" & shiftKeep_v(3 downto 3);
                                            outputNext := '1';
                                        when "1100" =>
                                            s_axis_p0.tlast <= '1';
                                            shiftData_v := x"0000" & shiftData_v(31 downto 16);
                                            shiftKeep_v := "00" & shiftKeep_v(3 downto 2);
                                            outputNext := '1';
                                        when "1110" =>
                                            s_axis_p0.tlast <= '1';
                                            shiftData_v := x"00" & shiftData_v(31 downto 8);
                                            shiftKeep_v := "0" & shiftKeep_v(3 downto 1);
                                            outputNext := '1';
                                        when others => NULL;
                                    end case;
                                end if;
                                if outputNow = '1' then
                                    s_axis_p1.tvalid <= '1';
                                    s_axis_p1.tkeep <= shiftKeep_v;
                                    s_axis_p1.tdata <= shiftData_v;
                                    s_axis_p1.tuser <= Truncate & ElinkBusy_v & FramingError_v & '0';
                                    shiftKeep_v := "0000";
                                    shiftData_v := x"00000000";
                                    FramingError_v := '0';
                                end if;
                                if shiftKeep_v(0) = '1' and i = maxBytes then --If we have our shift register filled somehwere in the middle, output immediately.
                                    outputNext := '1';
                                end if;
                                if outputNext = '1' then
                                    s_axis_p0.tvalid <= '1';
                                    s_axis_p0.tkeep <= shiftKeep_v;
                                    s_axis_p0.tdata <= shiftData_v;
                                    s_axis_p0.tuser <= Truncate & ElinkBusy_v & FramingError_v & '0';
                                    shiftKeep_v := "0000";
                                    shiftData_v := x"00000000";
                                    if ClearElinkBusy_v = '1' then
                                        ElinkBusy_v := '0';
                                    end if;
                                    FramingError_v := '0';
                                end if;
                            end loop;
                        else --Truncate = '1'
                            s_axis_p1.tdata <= shiftData_v;
                            s_axis_p1.tvalid <= (not TruncateHandled);
                            s_axis_p1.tlast <= '1';
                            s_axis_p1.tuser <= Truncate & ElinkBusy_v & FramingError_v & '0';
                            TruncateHandled <= '1';
                            if (DataInValid = (DataInValid'range => '0')) and (TruncateHandled = '1') then --If no data is coming in, we can safely go back to normal operation, and we are sure we are not in the middle of a chunk.
                                Truncate <= '0';
                                TruncateHandled <= '0';
                                shiftKeep_v := "0000";
                                shiftData_v := x"00000000";
                            end if;
                        end if; --Truncate = '0'
                    else --No input or control given
                        s_axis_p2.tvalid <= '0';
                    end if;
                else
                    for i in 0 to DataInValid'high loop
                        if DataInValid(i) = '1' or EOP(i) = '1' then
                            Truncate <= '1';
                            TruncateHandled <= '0';
                        end if;
                    end loop;
                end if;
            end if;
        end process;
    end generate;

    s_axis_aresetn <= not reset;

    fifo0: entity work.Axis32Fifo
        generic map(
            DEPTH            => BLOCKSIZE/2, -- Divide by 2 because this is 32b. Will contain 2 blocks.
            --CLOCKING_MODE    => "independent_clock",
            --RELATED_CLOCKS   => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO      => "false",
            USE_BUILT_IN_FIFO => USE_BUILT_IN_FIFO,
            VERSAL           => VERSAL,
            BLOCKSIZE        => BLOCKSIZE,
            ISPIXEL          => false
        )
        port map(
            -- axi stream slave
            s_axis_aresetn => s_axis_aresetn,
            s_axis_aclk => clk40,
            s_axis => s_axis,
            s_axis_tready => s_axis_tready,

            -- axi stream master
            m_axis_aclk => m_axis_aclk,
            m_axis => m_axis_s,
            m_axis_tready => m_axis_tready,
            m_axis_prog_empty => m_axis_prog_empty_s
        );

    --Synchronize EnableIn to m_axis_aclk domain
    xpm_cdc_enable : xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2, -- DECIMAL; range: 2-10
            INIT_SYNC_FF => 0, -- DECIMAL; integer; 0=disable simulation init values, 1=enable simulation init values
            SIM_ASSERT_CHK => 0, -- DECIMAL; integer; 0=disable simulation messages, 1=enable simulation messages
            SRC_INPUT_REG => 1   -- DECIMAL; integer; 0=do not register input, 1=register input
        )
        port map (
            src_clk => clk40,
            src_in => EnableIn,
            dest_clk => m_axis_aclk,
            dest_out => EnableIn_aclk
        );

    m_axis.tdata <= m_axis_s.tdata;
    m_axis.tvalid <= m_axis_s.tvalid and EnableIn_aclk;
    m_axis.tlast <= m_axis_s.tlast;
    m_axis.tkeep <= m_axis_s.tkeep;
    m_axis.tuser <= m_axis_s.tuser;
    m_axis_prog_empty <= m_axis_prog_empty_s or not EnableIn_aclk;

end Behavioral;
