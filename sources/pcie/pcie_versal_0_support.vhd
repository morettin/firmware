--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2.1 (lin64) Build 2729669 Thu Dec  5 04:48:12 MST 2019
--Date        : Mon Mar 30 17:01:49 2020
--Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
--Command     : generate_target pcie_versal_0_support.bd
--Design      : pcie_versal_0_support
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity pcie_versal_0_support is
  port (
    cfg_subsys_vend_id : in std_logic_vector(15 downto 0);
    cfg_dev_id_pf0     : in std_logic_vector(15 downto 0);
    cfg_vend_id        : in std_logic_vector(15 downto 0);
    m_axis_cq_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    m_axis_cq_tkeep : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_cq_tlast : out STD_LOGIC;
    m_axis_cq_tready : in STD_LOGIC;
    m_axis_cq_tuser : out STD_LOGIC_VECTOR ( 228 downto 0 );
    m_axis_cq_tvalid : out STD_LOGIC;
    m_axis_rc_tdata : out STD_LOGIC_VECTOR ( 511 downto 0 );
    m_axis_rc_tkeep : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m_axis_rc_tlast : out STD_LOGIC;
    m_axis_rc_tready : in STD_LOGIC;
    m_axis_rc_tuser : out STD_LOGIC_VECTOR ( 160 downto 0 );
    m_axis_rc_tvalid : out STD_LOGIC;
    pcie4_cfg_control_bus_number : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_control_config_space_enable : in STD_LOGIC;
    pcie4_cfg_control_ds_bus_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_control_ds_device_number : in STD_LOGIC_VECTOR ( 4 downto 0 );
    pcie4_cfg_control_ds_port_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_control_dsn : in STD_LOGIC_VECTOR ( 63 downto 0 );
    pcie4_cfg_control_err_cor_in : in STD_LOGIC;
    pcie4_cfg_control_err_uncor_in : in STD_LOGIC;
    pcie4_cfg_control_flr_done : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_control_flr_in_process : out STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_control_hot_reset_in : in STD_LOGIC;
    pcie4_cfg_control_hot_reset_out : out STD_LOGIC;
    pcie4_cfg_control_link_training_enable : in STD_LOGIC;
    pcie4_cfg_control_power_state_change_ack : in STD_LOGIC;
    pcie4_cfg_control_power_state_change_interrupt : out STD_LOGIC;
    pcie4_cfg_control_req_pm_transition_l23_ready : in STD_LOGIC;
    pcie4_cfg_control_vf_flr_done : in STD_LOGIC_VECTOR ( 0 to 0 );
    pcie4_cfg_control_vf_flr_func_num : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_control_vf_flr_in_process : out STD_LOGIC_VECTOR ( 251 downto 0 );
    pcie4_cfg_external_msix_address : in STD_LOGIC_VECTOR ( 63 downto 0 );
    pcie4_cfg_external_msix_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    pcie4_cfg_external_msix_enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_external_msix_fail : out STD_LOGIC;
    pcie4_cfg_external_msix_function_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_external_msix_int_vector : in STD_LOGIC;
    pcie4_cfg_external_msix_mask : out STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_external_msix_sent : out STD_LOGIC;
    pcie4_cfg_external_msix_vec_pending : in STD_LOGIC_VECTOR ( 1 downto 0 );
    pcie4_cfg_external_msix_vec_pending_status : out STD_LOGIC_VECTOR ( 0 to 0 );
    pcie4_cfg_external_msix_vf_enable : out STD_LOGIC_VECTOR ( 251 downto 0 );
    pcie4_cfg_external_msix_vf_mask : out STD_LOGIC_VECTOR ( 251 downto 0 );
    pcie4_cfg_fc_cpld : out STD_LOGIC_VECTOR ( 11 downto 0 );
    pcie4_cfg_fc_cplh : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_fc_npd : out STD_LOGIC_VECTOR ( 11 downto 0 );
    pcie4_cfg_fc_nph : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_fc_pd : out STD_LOGIC_VECTOR ( 11 downto 0 );
    pcie4_cfg_fc_ph : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_fc_sel : in STD_LOGIC_VECTOR ( 2 downto 0 );
    pcie4_cfg_interrupt_intx_vector : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_interrupt_pending : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_interrupt_sent : out STD_LOGIC;
    pcie4_cfg_mesg_rcvd_recd : out STD_LOGIC;
    pcie4_cfg_mesg_rcvd_recd_data : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_mesg_rcvd_recd_type : out STD_LOGIC_VECTOR ( 4 downto 0 );
    pcie4_cfg_mesg_tx_transmit : in STD_LOGIC;
    pcie4_cfg_mesg_tx_transmit_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    pcie4_cfg_mesg_tx_transmit_done : out STD_LOGIC;
    pcie4_cfg_mesg_tx_transmit_type : in STD_LOGIC_VECTOR ( 2 downto 0 );
    pcie4_cfg_mgmt_addr : in STD_LOGIC_VECTOR ( 9 downto 0 );
    pcie4_cfg_mgmt_byte_en : in STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_mgmt_debug_access : in STD_LOGIC;
    pcie4_cfg_mgmt_function_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie4_cfg_mgmt_read_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    pcie4_cfg_mgmt_read_en : in STD_LOGIC;
    pcie4_cfg_mgmt_read_write_done : out STD_LOGIC;
    pcie4_cfg_mgmt_write_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
    pcie4_cfg_mgmt_write_en : in STD_LOGIC;
    pcie4_cfg_pm_pm_aspm_l1entry_reject : in STD_LOGIC;
    pcie4_cfg_pm_pm_aspm_tx_l0s_entry_disable : in STD_LOGIC;
    pcie4_cfg_status_cq_np_req : in STD_LOGIC_VECTOR ( 1 downto 0 );
    pcie4_cfg_status_cq_np_req_count : out STD_LOGIC_VECTOR ( 5 downto 0 );
    pcie4_cfg_status_current_speed : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pcie4_cfg_status_err_cor_out : out STD_LOGIC;
    pcie4_cfg_status_err_fatal_out : out STD_LOGIC;
    pcie4_cfg_status_err_nonfatal_out : out STD_LOGIC;
    pcie4_cfg_status_function_power_state : out STD_LOGIC_VECTOR ( 11 downto 0 );
    pcie4_cfg_status_function_status : out STD_LOGIC_VECTOR ( 15 downto 0 );
    pcie4_cfg_status_link_power_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pcie4_cfg_status_local_error_out : out STD_LOGIC_VECTOR ( 4 downto 0 );
    pcie4_cfg_status_local_error_valid : out STD_LOGIC;
    pcie4_cfg_status_ltssm_state : out STD_LOGIC_VECTOR ( 5 downto 0 );
    pcie4_cfg_status_max_payload : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pcie4_cfg_status_max_read_req : out STD_LOGIC_VECTOR ( 2 downto 0 );
    pcie4_cfg_status_negotiated_width : out STD_LOGIC_VECTOR ( 2 downto 0 );
    pcie4_cfg_status_obff_enable : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pcie4_cfg_status_phy_link_down : out STD_LOGIC;
    pcie4_cfg_status_phy_link_status : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pcie4_cfg_status_pl_status_change : out STD_LOGIC;
    pcie4_cfg_status_rcb_status : out STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_status_rq_seq_num0 : out STD_LOGIC_VECTOR ( 5 downto 0 );
    pcie4_cfg_status_rq_seq_num1 : out STD_LOGIC_VECTOR ( 5 downto 0 );
    pcie4_cfg_status_rq_seq_num_vld0 : out STD_LOGIC;
    pcie4_cfg_status_rq_seq_num_vld1 : out STD_LOGIC;
    pcie4_cfg_status_rq_tag0 : out STD_LOGIC_VECTOR ( 9 downto 0 );
    pcie4_cfg_status_rq_tag1 : out STD_LOGIC_VECTOR ( 9 downto 0 );
    pcie4_cfg_status_rq_tag_av : out STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_status_rq_tag_vld0 : out STD_LOGIC;
    pcie4_cfg_status_rq_tag_vld1 : out STD_LOGIC;
    pcie4_cfg_status_rx_pm_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pcie4_cfg_status_tfc_npd_av : out STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_status_tfc_nph_av : out STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_status_tph_requester_enable : out STD_LOGIC_VECTOR ( 3 downto 0 );
    pcie4_cfg_status_tph_st_mode : out STD_LOGIC_VECTOR ( 11 downto 0 );
    pcie4_cfg_status_tx_pm_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pcie4_cfg_status_vf_power_state : out STD_LOGIC_VECTOR ( 755 downto 0 );
    pcie4_cfg_status_vf_status : out STD_LOGIC_VECTOR ( 503 downto 0 );
    pcie4_cfg_status_vf_tph_requester_enable : out STD_LOGIC_VECTOR ( 251 downto 0 );
    pcie4_cfg_status_vf_tph_st_mode : out STD_LOGIC_VECTOR ( 755 downto 0 );
    pcie_7x_mgt_rxn : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_7x_mgt_rxp : in STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_7x_mgt_txn : out STD_LOGIC_VECTOR ( 7 downto 0 );
    pcie_7x_mgt_txp : out STD_LOGIC_VECTOR ( 7 downto 0 );
    phy_rdy_out : out STD_LOGIC;
    s_axis_cc_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    s_axis_cc_tkeep : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axis_cc_tlast : in STD_LOGIC;
    s_axis_cc_tready : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_cc_tuser : in STD_LOGIC_VECTOR ( 80 downto 0 );
    s_axis_cc_tvalid : in STD_LOGIC;
    s_axis_rq_tdata : in STD_LOGIC_VECTOR ( 511 downto 0 );
    s_axis_rq_tkeep : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s_axis_rq_tlast : in STD_LOGIC;
    s_axis_rq_tready : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_rq_tuser : in STD_LOGIC_VECTOR ( 182 downto 0 );
    s_axis_rq_tvalid : in STD_LOGIC;
    sys_clk : in STD_LOGIC;
    sys_clk_gt : in STD_LOGIC;
    sys_reset : in STD_LOGIC;
    user_clk : out STD_LOGIC;
    user_lnk_up : out STD_LOGIC;
    user_reset : out STD_LOGIC
  );
end pcie_versal_0_support;

architecture STRUCTURE of pcie_versal_0_support is
  COMPONENT pcie_versal_0
  PORT (
    user_clk : OUT STD_LOGIC;
    user_reset : OUT STD_LOGIC;
    user_lnk_up : OUT STD_LOGIC;
    phy_txdata : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    phy_txdatak : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_txdata_valid : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txstart_block : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txsync_header : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rxdata : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    phy_rxdatak : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rxdata_valid : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxstart_block : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rxsync_header : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_txdetectrx_loopback : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txelecidle : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txcompliance : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxpolarity : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_powerdown : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rate : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    phy_rx_termination : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxvalid : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_phystatus : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_phystatus_rst : IN STD_LOGIC;
    phy_rxelecidle : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxstatus : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    phy_ready : IN STD_LOGIC;
    pcie_ltssm_state : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    phy_txmargin : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    phy_txswing : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txdeemph : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txprecursor : OUT STD_LOGIC_VECTOR(39 DOWNTO 0);
    phy_txmaincursor : OUT STD_LOGIC_VECTOR(55 DOWNTO 0);
    phy_txpostcursor : OUT STD_LOGIC_VECTOR(39 DOWNTO 0);
    phy_txeq_ctrl : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_txeq_preset : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    phy_txeq_coeff : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    phy_txeq_fs : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    phy_txeq_lf : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    phy_txeq_new_coeff : IN STD_LOGIC_VECTOR(143 DOWNTO 0);
    phy_txeq_done : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dbg_phy_txeq_fsm : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    phy_rxeq_ctrl : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rxeq_txpreset : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    phy_rxeq_preset : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    phy_rxeq_lffs : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    phy_rxeq_lffs_sel : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxeq_new_txcoeff : IN STD_LOGIC_VECTOR(143 DOWNTO 0);
    phy_rxeq_adapt_done : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxeq_done : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dbg_phy_rxeq_fsm : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    phy_rx_margin_req_cmd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rx_margin_req_lane_num : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    phy_rx_margin_req_payload : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rx_margin_req_req : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rx_margin_res_ack : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rx_margin_res_cmd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rx_margin_res_lane_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    phy_rx_margin_res_payload : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rx_margin_res_req : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rx_margin_req_ack : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rxeq_preset_sel : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    s_axis_rq_tdata : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_rq_tkeep : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axis_rq_tlast : IN STD_LOGIC;
    s_axis_rq_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_rq_tuser : IN STD_LOGIC_VECTOR(182 DOWNTO 0);
    s_axis_rq_tvalid : IN STD_LOGIC;
    m_axis_rc_tdata : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_rc_tkeep : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_rc_tlast : OUT STD_LOGIC;
    m_axis_rc_tready : IN STD_LOGIC;
    m_axis_rc_tuser : OUT STD_LOGIC_VECTOR(160 DOWNTO 0);
    m_axis_rc_tvalid : OUT STD_LOGIC;
    m_axis_cq_tdata : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    m_axis_cq_tkeep : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    m_axis_cq_tlast : OUT STD_LOGIC;
    m_axis_cq_tready : IN STD_LOGIC;
    m_axis_cq_tuser : OUT STD_LOGIC_VECTOR(228 DOWNTO 0);
    m_axis_cq_tvalid : OUT STD_LOGIC;
    s_axis_cc_tdata : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    s_axis_cc_tkeep : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    s_axis_cc_tlast : IN STD_LOGIC;
    s_axis_cc_tready : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axis_cc_tuser : IN STD_LOGIC_VECTOR(80 DOWNTO 0);
    s_axis_cc_tvalid : IN STD_LOGIC;
    pcie_rq_seq_num0 : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    pcie_rq_seq_num1 : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    pcie_rq_seq_num_vld0 : OUT STD_LOGIC;
    pcie_rq_seq_num_vld1 : OUT STD_LOGIC;
    pcie_rq_tag0 : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
    pcie_rq_tag1 : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
    pcie_rq_tag_av : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie_rq_tag_vld0 : OUT STD_LOGIC;
    pcie_rq_tag_vld1 : OUT STD_LOGIC;
    pcie_tfc_nph_av : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie_tfc_npd_av : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie_cq_np_req : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    pcie_cq_np_req_count : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    cfg_phy_link_down : OUT STD_LOGIC;
    cfg_phy_link_status : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_negotiated_width : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_current_speed : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_max_payload : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_max_read_req : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_function_status : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_function_power_state : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_vf_status : OUT STD_LOGIC_VECTOR(503 DOWNTO 0);
    cfg_vf_power_state : OUT STD_LOGIC_VECTOR(755 DOWNTO 0);
    cfg_link_power_state : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_mgmt_addr : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    cfg_mgmt_function_number : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_mgmt_write : IN STD_LOGIC;
    cfg_mgmt_write_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_mgmt_byte_enable : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_mgmt_read : IN STD_LOGIC;
    cfg_mgmt_read_data : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_mgmt_read_write_done : OUT STD_LOGIC;
    cfg_mgmt_debug_access : IN STD_LOGIC;
    cfg_err_cor_out : OUT STD_LOGIC;
    cfg_err_nonfatal_out : OUT STD_LOGIC;
    cfg_err_fatal_out : OUT STD_LOGIC;
    cfg_local_error_valid : OUT STD_LOGIC;
    cfg_local_error_out : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    cfg_ltssm_state : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    cfg_rx_pm_state : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_tx_pm_state : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_rcb_status : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_obff_enable : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_pl_status_change : OUT STD_LOGIC;
    cfg_tph_requester_enable : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_tph_st_mode : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_vf_tph_requester_enable : OUT STD_LOGIC_VECTOR(251 DOWNTO 0);
    cfg_vf_tph_st_mode : OUT STD_LOGIC_VECTOR(755 DOWNTO 0);
    cfg_msg_received : OUT STD_LOGIC;
    cfg_msg_received_data : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_msg_received_type : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    cfg_msg_transmit : IN STD_LOGIC;
    cfg_msg_transmit_type : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_msg_transmit_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_msg_transmit_done : OUT STD_LOGIC;
    cfg_fc_ph : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_fc_pd : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_fc_nph : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_fc_npd : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_fc_cplh : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_fc_cpld : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    cfg_fc_sel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    cfg_dsn : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    cfg_bus_number : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_power_state_change_ack : IN STD_LOGIC;
    cfg_power_state_change_interrupt : OUT STD_LOGIC;
    cfg_err_cor_in : IN STD_LOGIC;
    cfg_err_uncor_in : IN STD_LOGIC;
    cfg_flr_in_process : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_flr_done : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_vf_flr_in_process : OUT STD_LOGIC_VECTOR(251 DOWNTO 0);
    cfg_vf_flr_func_num : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_vf_flr_done : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    cfg_link_training_enable : IN STD_LOGIC;
    cfg_interrupt_int : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_pending : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_sent : OUT STD_LOGIC;
    cfg_interrupt_msi_sent : OUT STD_LOGIC;
    cfg_interrupt_msi_fail : OUT STD_LOGIC;
    cfg_interrupt_msi_function_number : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_interrupt_msix_enable : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_msix_mask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    cfg_interrupt_msix_vf_enable : OUT STD_LOGIC_VECTOR(251 DOWNTO 0);
    cfg_interrupt_msix_vf_mask : OUT STD_LOGIC_VECTOR(251 DOWNTO 0);
    cfg_pm_aspm_l1_entry_reject : IN STD_LOGIC;
    cfg_pm_aspm_tx_l0s_entry_disable : IN STD_LOGIC;
    cfg_interrupt_msix_data : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    cfg_interrupt_msix_address : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    cfg_interrupt_msix_int : IN STD_LOGIC;
    cfg_interrupt_msix_vec_pending : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    cfg_interrupt_msix_vec_pending_status : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    cfg_hot_reset_out : OUT STD_LOGIC;
    cfg_config_space_enable : IN STD_LOGIC;
    cfg_req_pm_transition_l23_ready : IN STD_LOGIC;
    cfg_hot_reset_in : IN STD_LOGIC;
    cfg_ds_port_number : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_ds_bus_number : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_ds_device_number : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    cfg_subsys_vend_id : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_dev_id_pf0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_vend_id : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    cfg_rev_id_pf0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    cfg_subsys_id_pf0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    sys_clk : IN STD_LOGIC;
    sys_clk_gt : IN STD_LOGIC;
    sys_reset : IN STD_LOGIC;
    phy_pclk : IN STD_LOGIC;
    phy_coreclk : IN STD_LOGIC;
    phy_userclk : IN STD_LOGIC;
    phy_userclk2 : IN STD_LOGIC;
    phy_mcapclk : IN STD_LOGIC;
    phy_rdy_out : OUT STD_LOGIC
  );
END COMPONENT;
COMPONENT pcie_phy_versal_0
  PORT (
    gtrefclk : OUT STD_LOGIC;
    pcie2gt0_txn : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie2gt1_txn : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie2gt0_txp : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie2gt1_txp : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie2gt0_rxn : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie2gt1_rxn : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie2gt0_rxp : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pcie2gt1_rxp : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    phy_refclk : IN STD_LOGIC;
    phy_gtrefclk : IN STD_LOGIC;
    gt_txoutclk : IN STD_LOGIC;
    gt_rxoutclk : IN STD_LOGIC;
    pcierstb : OUT STD_LOGIC;
    gt_pcieltssm : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    phy_pclk : OUT STD_LOGIC;
    phy_gt_pclk : OUT STD_LOGIC;
    phy_coreclk : OUT STD_LOGIC;
    phy_userclk : OUT STD_LOGIC;
    phy_userclk2 : OUT STD_LOGIC;
    phy_mcapclk : OUT STD_LOGIC;
    phy_rst_n : IN STD_LOGIC;
    gtpowergood : IN STD_LOGIC;
    phy_rxp : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxn : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txp : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txn : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txdata : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    phy_txdatak : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_txdata_valid : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txstart_block : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txsync_header : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rxdata : OUT STD_LOGIC_VECTOR(511 DOWNTO 0);
    phy_rxdatak : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rxdata_valid : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxstart_block : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rxsync_header : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_txdetectrx_loopback : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txelecidle : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txcompliance : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxpolarity : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_powerdown : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rate : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    phy_rx_termination : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxvalid : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_phystatus : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_phystatus_rst : OUT STD_LOGIC;
    phy_rxelecidle : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxstatus : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    phy_ready : OUT STD_LOGIC;
    pcie_ltssm_state : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    pcie_link_reach_target : IN STD_LOGIC;
    phy_txmargin : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    phy_txswing : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txdeemph : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_txprecursor : IN STD_LOGIC_VECTOR(39 DOWNTO 0);
    phy_txmaincursor : IN STD_LOGIC_VECTOR(55 DOWNTO 0);
    phy_txpostcursor : IN STD_LOGIC_VECTOR(39 DOWNTO 0);
    phy_txeq_ctrl : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_txeq_preset : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    phy_txeq_coeff : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    phy_txeq_fs : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    phy_txeq_lf : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    phy_txeq_new_coeff : OUT STD_LOGIC_VECTOR(143 DOWNTO 0);
    phy_txeq_done : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    dbg_phy_txeq_fsm : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    phy_rxeq_ctrl : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rxeq_txpreset : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    phy_rxeq_preset : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    phy_rxeq_lffs : IN STD_LOGIC_VECTOR(47 DOWNTO 0);
    phy_rxeq_lffs_sel : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxeq_new_txcoeff : OUT STD_LOGIC_VECTOR(143 DOWNTO 0);
    phy_rxeq_adapt_done : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rxeq_done : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    dbg_phy_rxeq_fsm : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    phy_rx_margin_req_cmd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rx_margin_req_lane_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    phy_rx_margin_req_payload : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rx_margin_req_req : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rx_margin_res_ack : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rx_margin_res_cmd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    phy_rx_margin_res_lane_num : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    phy_rx_margin_res_payload : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    phy_rx_margin_res_req : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rx_margin_req_ack : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    phy_rxeq_preset_sel : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_phystatus : IN STD_LOGIC;
    ch1_phystatus : IN STD_LOGIC;
    ch2_phystatus : IN STD_LOGIC;
    ch3_phystatus : IN STD_LOGIC;
    ch4_phystatus : IN STD_LOGIC;
    ch5_phystatus : IN STD_LOGIC;
    ch6_phystatus : IN STD_LOGIC;
    ch7_phystatus : IN STD_LOGIC;
    ch0_phyready : IN STD_LOGIC;
    ch1_phyready : IN STD_LOGIC;
    ch2_phyready : IN STD_LOGIC;
    ch3_phyready : IN STD_LOGIC;
    ch4_phyready : IN STD_LOGIC;
    ch5_phyready : IN STD_LOGIC;
    ch6_phyready : IN STD_LOGIC;
    ch7_phyready : IN STD_LOGIC;
    gtq0_rxmarginreqlanenum : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtq0_rxmarginreqcmd : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtq0_rxmarginreqpayld : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    gtq0_rxmarginreqreq : OUT STD_LOGIC;
    gtq0_rxmarginresack : OUT STD_LOGIC;
    gtq0_rxmarginclk : OUT STD_LOGIC;
    gtq1_rxmarginclk : OUT STD_LOGIC;
    gtq1_rxmarginreqlanenum : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtq1_rxmarginreqcmd : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtq1_rxmarginreqpayld : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    gtq1_rxmarginreqreq : OUT STD_LOGIC;
    gtq1_rxmarginresack : OUT STD_LOGIC;
    gtq0_rxmarginrescmd : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtq0_rxmarginreslanenum : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtq0_rxmarginrespayld : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    gtq0_rxmarginresreq : IN STD_LOGIC;
    gtq0_rxmarginreqack : IN STD_LOGIC;
    gtq1_rxmarginrescmd : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gtq1_rxmarginreslanenum : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    gtq1_rxmarginrespayld : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    gtq1_rxmarginresreq : IN STD_LOGIC;
    gtq1_rxmarginreqack : IN STD_LOGIC;
    ch0_txdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch0_txuserrdy : OUT STD_LOGIC;
    ch0_txdetectrx : OUT STD_LOGIC;
    ch0_txelecidle : OUT STD_LOGIC;
    ch0_txswing : OUT STD_LOGIC;
    ch0_tx10gstat : IN STD_LOGIC;
    ch0_txcomfinish : IN STD_LOGIC;
    ch0_txdccdone : IN STD_LOGIC;
    ch0_txdlyalignerr : IN STD_LOGIC;
    ch0_txdlyalignprog : IN STD_LOGIC;
    ch0_txphaligndone : IN STD_LOGIC;
    ch0_txphalignerr : IN STD_LOGIC;
    ch0_txphalignoutrsvd : IN STD_LOGIC;
    ch0_txphdlyresetdone : IN STD_LOGIC;
    ch0_txphsetinitdone : IN STD_LOGIC;
    ch0_txphshift180done : IN STD_LOGIC;
    ch0_txsyncdone : IN STD_LOGIC;
    ch0_txbufstatus : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_txctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_txdeemph : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txpd : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txmstresetdone : IN STD_LOGIC;
    ch0_txmargin : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_txpostcursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txprecursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txmaincursor : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch0_txctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_txrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_txprogdivresetdone : IN STD_LOGIC;
    ch0_txpmaresetdone : IN STD_LOGIC;
    ch0_txresetdone : IN STD_LOGIC;
    ch1_txdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch1_txuserrdy : OUT STD_LOGIC;
    ch1_txdetectrx : OUT STD_LOGIC;
    ch1_txelecidle : OUT STD_LOGIC;
    ch1_txswing : OUT STD_LOGIC;
    ch1_tx10gstat : IN STD_LOGIC;
    ch1_txcomfinish : IN STD_LOGIC;
    ch1_txdccdone : IN STD_LOGIC;
    ch1_txdlyalignerr : IN STD_LOGIC;
    ch1_txdlyalignprog : IN STD_LOGIC;
    ch1_txphaligndone : IN STD_LOGIC;
    ch1_txphalignerr : IN STD_LOGIC;
    ch1_txphalignoutrsvd : IN STD_LOGIC;
    ch1_txphdlyresetdone : IN STD_LOGIC;
    ch1_txphsetinitdone : IN STD_LOGIC;
    ch1_txphshift180done : IN STD_LOGIC;
    ch1_txsyncdone : IN STD_LOGIC;
    ch1_txbufstatus : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_txctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_txdeemph : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txpd : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txmstresetdone : IN STD_LOGIC;
    ch1_txmargin : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_txpostcursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txprecursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txmaincursor : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch1_txctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_txrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_txprogdivresetdone : IN STD_LOGIC;
    ch1_txpmaresetdone : IN STD_LOGIC;
    ch1_txresetdone : IN STD_LOGIC;
    ch2_txdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch2_txuserrdy : OUT STD_LOGIC;
    ch2_txdetectrx : OUT STD_LOGIC;
    ch2_txelecidle : OUT STD_LOGIC;
    ch2_txswing : OUT STD_LOGIC;
    ch2_tx10gstat : IN STD_LOGIC;
    ch2_txcomfinish : IN STD_LOGIC;
    ch2_txdccdone : IN STD_LOGIC;
    ch2_txdlyalignerr : IN STD_LOGIC;
    ch2_txdlyalignprog : IN STD_LOGIC;
    ch2_txphaligndone : IN STD_LOGIC;
    ch2_txphalignerr : IN STD_LOGIC;
    ch2_txphalignoutrsvd : IN STD_LOGIC;
    ch2_txphdlyresetdone : IN STD_LOGIC;
    ch2_txphsetinitdone : IN STD_LOGIC;
    ch2_txphshift180done : IN STD_LOGIC;
    ch2_txsyncdone : IN STD_LOGIC;
    ch2_txbufstatus : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_txctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_txdeemph : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txpd : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txmstresetdone : IN STD_LOGIC;
    ch2_txmargin : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_txpostcursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txprecursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txmaincursor : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch2_txctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_txrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_txprogdivresetdone : IN STD_LOGIC;
    ch2_txpmaresetdone : IN STD_LOGIC;
    ch2_txresetdone : IN STD_LOGIC;
    ch3_txdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch3_txuserrdy : OUT STD_LOGIC;
    ch3_txdetectrx : OUT STD_LOGIC;
    ch3_txelecidle : OUT STD_LOGIC;
    ch3_txswing : OUT STD_LOGIC;
    ch3_tx10gstat : IN STD_LOGIC;
    ch3_txcomfinish : IN STD_LOGIC;
    ch3_txdccdone : IN STD_LOGIC;
    ch3_txdlyalignerr : IN STD_LOGIC;
    ch3_txdlyalignprog : IN STD_LOGIC;
    ch3_txphaligndone : IN STD_LOGIC;
    ch3_txphalignerr : IN STD_LOGIC;
    ch3_txphalignoutrsvd : IN STD_LOGIC;
    ch3_txphdlyresetdone : IN STD_LOGIC;
    ch3_txphsetinitdone : IN STD_LOGIC;
    ch3_txphshift180done : IN STD_LOGIC;
    ch3_txsyncdone : IN STD_LOGIC;
    ch3_txbufstatus : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_txctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_txdeemph : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txpd : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txmstresetdone : IN STD_LOGIC;
    ch3_txmargin : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_txpostcursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txprecursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txmaincursor : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch3_txctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_txrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_txprogdivresetdone : IN STD_LOGIC;
    ch3_txpmaresetdone : IN STD_LOGIC;
    ch3_txresetdone : IN STD_LOGIC;
    ch4_txdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch4_txuserrdy : OUT STD_LOGIC;
    ch4_txdetectrx : OUT STD_LOGIC;
    ch4_txelecidle : OUT STD_LOGIC;
    ch4_txswing : OUT STD_LOGIC;
    ch4_tx10gstat : IN STD_LOGIC;
    ch4_txcomfinish : IN STD_LOGIC;
    ch4_txdccdone : IN STD_LOGIC;
    ch4_txdlyalignerr : IN STD_LOGIC;
    ch4_txdlyalignprog : IN STD_LOGIC;
    ch4_txphaligndone : IN STD_LOGIC;
    ch4_txphalignerr : IN STD_LOGIC;
    ch4_txphalignoutrsvd : IN STD_LOGIC;
    ch4_txphdlyresetdone : IN STD_LOGIC;
    ch4_txphsetinitdone : IN STD_LOGIC;
    ch4_txphshift180done : IN STD_LOGIC;
    ch4_txsyncdone : IN STD_LOGIC;
    ch4_txbufstatus : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch4_txctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch4_txctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch4_txdeemph : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch4_txpd : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch4_txmstresetdone : IN STD_LOGIC;
    ch4_txmargin : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch4_txpostcursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch4_txprecursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch4_txmaincursor : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch4_txctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch4_txrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch4_txprogdivresetdone : IN STD_LOGIC;
    ch4_txpmaresetdone : IN STD_LOGIC;
    ch4_txresetdone : IN STD_LOGIC;
    ch5_txdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch5_txuserrdy : OUT STD_LOGIC;
    ch5_txdetectrx : OUT STD_LOGIC;
    ch5_txelecidle : OUT STD_LOGIC;
    ch5_txswing : OUT STD_LOGIC;
    ch5_tx10gstat : IN STD_LOGIC;
    ch5_txcomfinish : IN STD_LOGIC;
    ch5_txdccdone : IN STD_LOGIC;
    ch5_txdlyalignerr : IN STD_LOGIC;
    ch5_txdlyalignprog : IN STD_LOGIC;
    ch5_txphaligndone : IN STD_LOGIC;
    ch5_txphalignerr : IN STD_LOGIC;
    ch5_txphalignoutrsvd : IN STD_LOGIC;
    ch5_txphdlyresetdone : IN STD_LOGIC;
    ch5_txphsetinitdone : IN STD_LOGIC;
    ch5_txphshift180done : IN STD_LOGIC;
    ch5_txsyncdone : IN STD_LOGIC;
    ch5_txbufstatus : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch5_txctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch5_txctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch5_txdeemph : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch5_txpd : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch5_txmstresetdone : IN STD_LOGIC;
    ch5_txmargin : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch5_txpostcursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch5_txprecursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch5_txmaincursor : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch5_txctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch5_txrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch5_txprogdivresetdone : IN STD_LOGIC;
    ch5_txpmaresetdone : IN STD_LOGIC;
    ch5_txresetdone : IN STD_LOGIC;
    ch6_txdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch6_txuserrdy : OUT STD_LOGIC;
    ch6_txdetectrx : OUT STD_LOGIC;
    ch6_txelecidle : OUT STD_LOGIC;
    ch6_txswing : OUT STD_LOGIC;
    ch6_tx10gstat : IN STD_LOGIC;
    ch6_txcomfinish : IN STD_LOGIC;
    ch6_txdccdone : IN STD_LOGIC;
    ch6_txdlyalignerr : IN STD_LOGIC;
    ch6_txdlyalignprog : IN STD_LOGIC;
    ch6_txphaligndone : IN STD_LOGIC;
    ch6_txphalignerr : IN STD_LOGIC;
    ch6_txphalignoutrsvd : IN STD_LOGIC;
    ch6_txphdlyresetdone : IN STD_LOGIC;
    ch6_txphsetinitdone : IN STD_LOGIC;
    ch6_txphshift180done : IN STD_LOGIC;
    ch6_txsyncdone : IN STD_LOGIC;
    ch6_txbufstatus : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch6_txctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch6_txctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch6_txdeemph : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch6_txpd : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch6_txmstresetdone : IN STD_LOGIC;
    ch6_txmargin : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch6_txpostcursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch6_txprecursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch6_txmaincursor : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch6_txctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch6_txrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch6_txprogdivresetdone : IN STD_LOGIC;
    ch6_txpmaresetdone : IN STD_LOGIC;
    ch6_txresetdone : IN STD_LOGIC;
    ch7_txdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch7_txuserrdy : OUT STD_LOGIC;
    ch7_txdetectrx : OUT STD_LOGIC;
    ch7_txelecidle : OUT STD_LOGIC;
    ch7_txswing : OUT STD_LOGIC;
    ch7_tx10gstat : IN STD_LOGIC;
    ch7_txcomfinish : IN STD_LOGIC;
    ch7_txdccdone : IN STD_LOGIC;
    ch7_txdlyalignerr : IN STD_LOGIC;
    ch7_txdlyalignprog : IN STD_LOGIC;
    ch7_txphaligndone : IN STD_LOGIC;
    ch7_txphalignerr : IN STD_LOGIC;
    ch7_txphalignoutrsvd : IN STD_LOGIC;
    ch7_txphdlyresetdone : IN STD_LOGIC;
    ch7_txphsetinitdone : IN STD_LOGIC;
    ch7_txphshift180done : IN STD_LOGIC;
    ch7_txsyncdone : IN STD_LOGIC;
    ch7_txbufstatus : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch7_txctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch7_txctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch7_txdeemph : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch7_txpd : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch7_txmstresetdone : IN STD_LOGIC;
    ch7_txmargin : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch7_txpostcursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch7_txprecursor : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch7_txmaincursor : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch7_txctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch7_txrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch7_txprogdivresetdone : IN STD_LOGIC;
    ch7_txpmaresetdone : IN STD_LOGIC;
    ch7_txresetdone : IN STD_LOGIC;
    ch0_rxdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch0_rxdatavalid : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch0_rxuserrdy : OUT STD_LOGIC;
    ch0_rxprogdivresetdone : IN STD_LOGIC;
    ch0_rxpmaresetdone : IN STD_LOGIC;
    ch0_rxresetdone : IN STD_LOGIC;
    ch0_rx10gstat : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxbufstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_rxbyteisaligned : IN STD_LOGIC;
    ch0_rxbyterealign : IN STD_LOGIC;
    ch0_rxcdrlock : IN STD_LOGIC;
    ch0_rxcdrphdone : IN STD_LOGIC;
    ch0_rxchanbondseq : IN STD_LOGIC;
    ch0_rxchanisaligned : IN STD_LOGIC;
    ch0_rxchanrealign : IN STD_LOGIC;
    ch0_rxchbondo : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_rxclkcorcnt : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxcominitdet : IN STD_LOGIC;
    ch0_rxcommadet : IN STD_LOGIC;
    ch0_rxcomsasdet : IN STD_LOGIC;
    ch0_rxcomwakedet : IN STD_LOGIC;
    ch0_rxctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_rxctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_rxctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxctrl3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxdccdone : IN STD_LOGIC;
    ch0_rxdlyalignerr : IN STD_LOGIC;
    ch0_rxdlyalignprog : IN STD_LOGIC;
    ch0_rxelecidle : IN STD_LOGIC;
    ch0_rxfinealigndone : IN STD_LOGIC;
    ch0_rxosintdone : IN STD_LOGIC;
    ch0_rxosintstarted : IN STD_LOGIC;
    ch0_rxosintstrobedone : IN STD_LOGIC;
    ch0_rxosintstrobestarted : IN STD_LOGIC;
    ch0_rxphalignerr : IN STD_LOGIC;
    ch0_rxphdlyresetdone : IN STD_LOGIC;
    ch0_rxphsetinitdone : IN STD_LOGIC;
    ch0_rxphshift180done : IN STD_LOGIC;
    ch0_rxpolarity : OUT STD_LOGIC;
    ch0_rxprbserr : IN STD_LOGIC;
    ch0_rxprbslocked : IN STD_LOGIC;
    ch0_rxrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxrecclkout : IN STD_LOGIC;
    ch0_rxmstresetdone : IN STD_LOGIC;
    ch0_rxsliderdy : IN STD_LOGIC;
    ch0_rxstartofseq : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_rxsyncdone : IN STD_LOGIC;
    ch0_rxtermination : OUT STD_LOGIC;
    ch0_rxvalid : IN STD_LOGIC;
    ch0_eyescandataerror : IN STD_LOGIC;
    ch0_cfokovwrrdy0 : IN STD_LOGIC;
    ch0_cfokovwrrdy1 : IN STD_LOGIC;
    ch1_rxdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch1_rxdatavalid : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch1_rxuserrdy : OUT STD_LOGIC;
    ch1_rxprogdivresetdone : IN STD_LOGIC;
    ch1_rxpmaresetdone : IN STD_LOGIC;
    ch1_rxresetdone : IN STD_LOGIC;
    ch1_rx10gstat : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxbufstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_rxbyteisaligned : IN STD_LOGIC;
    ch1_rxbyterealign : IN STD_LOGIC;
    ch1_rxcdrlock : IN STD_LOGIC;
    ch1_rxcdrphdone : IN STD_LOGIC;
    ch1_rxchanbondseq : IN STD_LOGIC;
    ch1_rxchanisaligned : IN STD_LOGIC;
    ch1_rxchanrealign : IN STD_LOGIC;
    ch1_rxchbondo : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_rxclkcorcnt : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxcominitdet : IN STD_LOGIC;
    ch1_rxcommadet : IN STD_LOGIC;
    ch1_rxcomsasdet : IN STD_LOGIC;
    ch1_rxcomwakedet : IN STD_LOGIC;
    ch1_rxctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_rxctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_rxctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxctrl3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxdccdone : IN STD_LOGIC;
    ch1_rxdlyalignerr : IN STD_LOGIC;
    ch1_rxdlyalignprog : IN STD_LOGIC;
    ch1_rxelecidle : IN STD_LOGIC;
    ch1_rxfinealigndone : IN STD_LOGIC;
    ch1_rxosintdone : IN STD_LOGIC;
    ch1_rxosintstarted : IN STD_LOGIC;
    ch1_rxosintstrobedone : IN STD_LOGIC;
    ch1_rxosintstrobestarted : IN STD_LOGIC;
    ch1_rxphalignerr : IN STD_LOGIC;
    ch1_rxphdlyresetdone : IN STD_LOGIC;
    ch1_rxphsetinitdone : IN STD_LOGIC;
    ch1_rxphshift180done : IN STD_LOGIC;
    ch1_rxpolarity : OUT STD_LOGIC;
    ch1_rxprbserr : IN STD_LOGIC;
    ch1_rxprbslocked : IN STD_LOGIC;
    ch1_rxrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxrecclkout : IN STD_LOGIC;
    ch1_rxmstresetdone : IN STD_LOGIC;
    ch1_rxsliderdy : IN STD_LOGIC;
    ch1_rxstartofseq : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_rxsyncdone : IN STD_LOGIC;
    ch1_rxtermination : OUT STD_LOGIC;
    ch1_rxvalid : IN STD_LOGIC;
    ch1_eyescandataerror : IN STD_LOGIC;
    ch1_cfokovwrrdy0 : IN STD_LOGIC;
    ch1_cfokovwrrdy1 : IN STD_LOGIC;
    ch2_rxdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch2_rxdatavalid : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch2_rxuserrdy : OUT STD_LOGIC;
    ch2_rxprogdivresetdone : IN STD_LOGIC;
    ch2_rxpmaresetdone : IN STD_LOGIC;
    ch2_rxresetdone : IN STD_LOGIC;
    ch2_rx10gstat : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxbufstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_rxbyteisaligned : IN STD_LOGIC;
    ch2_rxbyterealign : IN STD_LOGIC;
    ch2_rxcdrlock : IN STD_LOGIC;
    ch2_rxcdrphdone : IN STD_LOGIC;
    ch2_rxchanbondseq : IN STD_LOGIC;
    ch2_rxchanisaligned : IN STD_LOGIC;
    ch2_rxchanrealign : IN STD_LOGIC;
    ch2_rxchbondo : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_rxclkcorcnt : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxcominitdet : IN STD_LOGIC;
    ch2_rxcommadet : IN STD_LOGIC;
    ch2_rxcomsasdet : IN STD_LOGIC;
    ch2_rxcomwakedet : IN STD_LOGIC;
    ch2_rxctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_rxctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_rxctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxctrl3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxdccdone : IN STD_LOGIC;
    ch2_rxdlyalignerr : IN STD_LOGIC;
    ch2_rxdlyalignprog : IN STD_LOGIC;
    ch2_rxelecidle : IN STD_LOGIC;
    ch2_rxfinealigndone : IN STD_LOGIC;
    ch2_rxosintdone : IN STD_LOGIC;
    ch2_rxosintstarted : IN STD_LOGIC;
    ch2_rxosintstrobedone : IN STD_LOGIC;
    ch2_rxosintstrobestarted : IN STD_LOGIC;
    ch2_rxphalignerr : IN STD_LOGIC;
    ch2_rxphdlyresetdone : IN STD_LOGIC;
    ch2_rxphsetinitdone : IN STD_LOGIC;
    ch2_rxphshift180done : IN STD_LOGIC;
    ch2_rxpolarity : OUT STD_LOGIC;
    ch2_rxprbserr : IN STD_LOGIC;
    ch2_rxprbslocked : IN STD_LOGIC;
    ch2_rxrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxrecclkout : IN STD_LOGIC;
    ch2_rxmstresetdone : IN STD_LOGIC;
    ch2_rxsliderdy : IN STD_LOGIC;
    ch2_rxstartofseq : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_rxsyncdone : IN STD_LOGIC;
    ch2_rxtermination : OUT STD_LOGIC;
    ch2_rxvalid : IN STD_LOGIC;
    ch2_eyescandataerror : IN STD_LOGIC;
    ch2_cfokovwrrdy0 : IN STD_LOGIC;
    ch2_cfokovwrrdy1 : IN STD_LOGIC;
    ch3_rxdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch3_rxdatavalid : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch3_rxuserrdy : OUT STD_LOGIC;
    ch3_rxprogdivresetdone : IN STD_LOGIC;
    ch3_rxpmaresetdone : IN STD_LOGIC;
    ch3_rxresetdone : IN STD_LOGIC;
    ch3_rx10gstat : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxbufstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_rxbyteisaligned : IN STD_LOGIC;
    ch3_rxbyterealign : IN STD_LOGIC;
    ch3_rxcdrlock : IN STD_LOGIC;
    ch3_rxcdrphdone : IN STD_LOGIC;
    ch3_rxchanbondseq : IN STD_LOGIC;
    ch3_rxchanisaligned : IN STD_LOGIC;
    ch3_rxchanrealign : IN STD_LOGIC;
    ch3_rxchbondo : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_rxclkcorcnt : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxcominitdet : IN STD_LOGIC;
    ch3_rxcommadet : IN STD_LOGIC;
    ch3_rxcomsasdet : IN STD_LOGIC;
    ch3_rxcomwakedet : IN STD_LOGIC;
    ch3_rxctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_rxctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_rxctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxctrl3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxdccdone : IN STD_LOGIC;
    ch3_rxdlyalignerr : IN STD_LOGIC;
    ch3_rxdlyalignprog : IN STD_LOGIC;
    ch3_rxelecidle : IN STD_LOGIC;
    ch3_rxfinealigndone : IN STD_LOGIC;
    ch3_rxosintdone : IN STD_LOGIC;
    ch3_rxosintstarted : IN STD_LOGIC;
    ch3_rxosintstrobedone : IN STD_LOGIC;
    ch3_rxosintstrobestarted : IN STD_LOGIC;
    ch3_rxphalignerr : IN STD_LOGIC;
    ch3_rxphdlyresetdone : IN STD_LOGIC;
    ch3_rxphsetinitdone : IN STD_LOGIC;
    ch3_rxphshift180done : IN STD_LOGIC;
    ch3_rxpolarity : OUT STD_LOGIC;
    ch3_rxprbserr : IN STD_LOGIC;
    ch3_rxprbslocked : IN STD_LOGIC;
    ch3_rxrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxrecclkout : IN STD_LOGIC;
    ch3_rxmstresetdone : IN STD_LOGIC;
    ch3_rxsliderdy : IN STD_LOGIC;
    ch3_rxstartofseq : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_rxsyncdone : IN STD_LOGIC;
    ch3_rxtermination : OUT STD_LOGIC;
    ch3_rxvalid : IN STD_LOGIC;
    ch3_eyescandataerror : IN STD_LOGIC;
    ch3_cfokovwrrdy0 : IN STD_LOGIC;
    ch3_cfokovwrrdy1 : IN STD_LOGIC;
    ch4_rxdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch4_rxdatavalid : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch4_rxheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch4_rxuserrdy : OUT STD_LOGIC;
    ch4_rxprogdivresetdone : IN STD_LOGIC;
    ch4_rxpmaresetdone : IN STD_LOGIC;
    ch4_rxresetdone : IN STD_LOGIC;
    ch4_rx10gstat : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch4_rxbufstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch4_rxbyteisaligned : IN STD_LOGIC;
    ch4_rxbyterealign : IN STD_LOGIC;
    ch4_rxcdrlock : IN STD_LOGIC;
    ch4_rxcdrphdone : IN STD_LOGIC;
    ch4_rxchanbondseq : IN STD_LOGIC;
    ch4_rxchanisaligned : IN STD_LOGIC;
    ch4_rxchanrealign : IN STD_LOGIC;
    ch4_rxchbondo : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch4_rxclkcorcnt : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch4_rxcominitdet : IN STD_LOGIC;
    ch4_rxcommadet : IN STD_LOGIC;
    ch4_rxcomsasdet : IN STD_LOGIC;
    ch4_rxcomwakedet : IN STD_LOGIC;
    ch4_rxctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch4_rxctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch4_rxctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch4_rxctrl3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch4_rxdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch4_rxdccdone : IN STD_LOGIC;
    ch4_rxdlyalignerr : IN STD_LOGIC;
    ch4_rxdlyalignprog : IN STD_LOGIC;
    ch4_rxelecidle : IN STD_LOGIC;
    ch4_rxfinealigndone : IN STD_LOGIC;
    ch4_rxosintdone : IN STD_LOGIC;
    ch4_rxosintstarted : IN STD_LOGIC;
    ch4_rxosintstrobedone : IN STD_LOGIC;
    ch4_rxosintstrobestarted : IN STD_LOGIC;
    ch4_rxphalignerr : IN STD_LOGIC;
    ch4_rxphdlyresetdone : IN STD_LOGIC;
    ch4_rxphsetinitdone : IN STD_LOGIC;
    ch4_rxphshift180done : IN STD_LOGIC;
    ch4_rxpolarity : OUT STD_LOGIC;
    ch4_rxprbserr : IN STD_LOGIC;
    ch4_rxprbslocked : IN STD_LOGIC;
    ch4_rxrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch4_rxrecclkout : IN STD_LOGIC;
    ch4_rxmstresetdone : IN STD_LOGIC;
    ch4_rxsliderdy : IN STD_LOGIC;
    ch4_rxstartofseq : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch4_rxstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch4_rxsyncdone : IN STD_LOGIC;
    ch4_rxtermination : OUT STD_LOGIC;
    ch4_rxvalid : IN STD_LOGIC;
    ch4_eyescandataerror : IN STD_LOGIC;
    ch4_cfokovwrrdy0 : IN STD_LOGIC;
    ch4_cfokovwrrdy1 : IN STD_LOGIC;
    ch5_rxdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch5_rxdatavalid : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch5_rxheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch5_rxuserrdy : OUT STD_LOGIC;
    ch5_rxprogdivresetdone : IN STD_LOGIC;
    ch5_rxpmaresetdone : IN STD_LOGIC;
    ch5_rxresetdone : IN STD_LOGIC;
    ch5_rx10gstat : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch5_rxbufstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch5_rxbyteisaligned : IN STD_LOGIC;
    ch5_rxbyterealign : IN STD_LOGIC;
    ch5_rxcdrlock : IN STD_LOGIC;
    ch5_rxcdrphdone : IN STD_LOGIC;
    ch5_rxchanbondseq : IN STD_LOGIC;
    ch5_rxchanisaligned : IN STD_LOGIC;
    ch5_rxchanrealign : IN STD_LOGIC;
    ch5_rxchbondo : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch5_rxclkcorcnt : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch5_rxcominitdet : IN STD_LOGIC;
    ch5_rxcommadet : IN STD_LOGIC;
    ch5_rxcomsasdet : IN STD_LOGIC;
    ch5_rxcomwakedet : IN STD_LOGIC;
    ch5_rxctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch5_rxctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch5_rxctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch5_rxctrl3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch5_rxdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch5_rxdccdone : IN STD_LOGIC;
    ch5_rxdlyalignerr : IN STD_LOGIC;
    ch5_rxdlyalignprog : IN STD_LOGIC;
    ch5_rxelecidle : IN STD_LOGIC;
    ch5_rxfinealigndone : IN STD_LOGIC;
    ch5_rxosintdone : IN STD_LOGIC;
    ch5_rxosintstarted : IN STD_LOGIC;
    ch5_rxosintstrobedone : IN STD_LOGIC;
    ch5_rxosintstrobestarted : IN STD_LOGIC;
    ch5_rxphalignerr : IN STD_LOGIC;
    ch5_rxphdlyresetdone : IN STD_LOGIC;
    ch5_rxphsetinitdone : IN STD_LOGIC;
    ch5_rxphshift180done : IN STD_LOGIC;
    ch5_rxpolarity : OUT STD_LOGIC;
    ch5_rxprbserr : IN STD_LOGIC;
    ch5_rxprbslocked : IN STD_LOGIC;
    ch5_rxrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch5_rxrecclkout : IN STD_LOGIC;
    ch5_rxmstresetdone : IN STD_LOGIC;
    ch5_rxsliderdy : IN STD_LOGIC;
    ch5_rxstartofseq : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch5_rxstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch5_rxsyncdone : IN STD_LOGIC;
    ch5_rxtermination : OUT STD_LOGIC;
    ch5_rxvalid : IN STD_LOGIC;
    ch5_eyescandataerror : IN STD_LOGIC;
    ch5_cfokovwrrdy0 : IN STD_LOGIC;
    ch5_cfokovwrrdy1 : IN STD_LOGIC;
    ch6_rxdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch6_rxdatavalid : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch6_rxheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch6_rxuserrdy : OUT STD_LOGIC;
    ch6_rxprogdivresetdone : IN STD_LOGIC;
    ch6_rxpmaresetdone : IN STD_LOGIC;
    ch6_rxresetdone : IN STD_LOGIC;
    ch6_rx10gstat : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch6_rxbufstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch6_rxbyteisaligned : IN STD_LOGIC;
    ch6_rxbyterealign : IN STD_LOGIC;
    ch6_rxcdrlock : IN STD_LOGIC;
    ch6_rxcdrphdone : IN STD_LOGIC;
    ch6_rxchanbondseq : IN STD_LOGIC;
    ch6_rxchanisaligned : IN STD_LOGIC;
    ch6_rxchanrealign : IN STD_LOGIC;
    ch6_rxchbondo : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch6_rxclkcorcnt : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch6_rxcominitdet : IN STD_LOGIC;
    ch6_rxcommadet : IN STD_LOGIC;
    ch6_rxcomsasdet : IN STD_LOGIC;
    ch6_rxcomwakedet : IN STD_LOGIC;
    ch6_rxctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch6_rxctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch6_rxctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch6_rxctrl3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch6_rxdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch6_rxdccdone : IN STD_LOGIC;
    ch6_rxdlyalignerr : IN STD_LOGIC;
    ch6_rxdlyalignprog : IN STD_LOGIC;
    ch6_rxelecidle : IN STD_LOGIC;
    ch6_rxfinealigndone : IN STD_LOGIC;
    ch6_rxosintdone : IN STD_LOGIC;
    ch6_rxosintstarted : IN STD_LOGIC;
    ch6_rxosintstrobedone : IN STD_LOGIC;
    ch6_rxosintstrobestarted : IN STD_LOGIC;
    ch6_rxphalignerr : IN STD_LOGIC;
    ch6_rxphdlyresetdone : IN STD_LOGIC;
    ch6_rxphsetinitdone : IN STD_LOGIC;
    ch6_rxphshift180done : IN STD_LOGIC;
    ch6_rxpolarity : OUT STD_LOGIC;
    ch6_rxprbserr : IN STD_LOGIC;
    ch6_rxprbslocked : IN STD_LOGIC;
    ch6_rxrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch6_rxrecclkout : IN STD_LOGIC;
    ch6_rxmstresetdone : IN STD_LOGIC;
    ch6_rxsliderdy : IN STD_LOGIC;
    ch6_rxstartofseq : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch6_rxstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch6_rxsyncdone : IN STD_LOGIC;
    ch6_rxtermination : OUT STD_LOGIC;
    ch6_rxvalid : IN STD_LOGIC;
    ch6_eyescandataerror : IN STD_LOGIC;
    ch6_cfokovwrrdy0 : IN STD_LOGIC;
    ch6_cfokovwrrdy1 : IN STD_LOGIC;
    ch7_rxdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch7_rxdatavalid : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch7_rxheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch7_rxuserrdy : OUT STD_LOGIC;
    ch7_rxprogdivresetdone : IN STD_LOGIC;
    ch7_rxpmaresetdone : IN STD_LOGIC;
    ch7_rxresetdone : IN STD_LOGIC;
    ch7_rx10gstat : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch7_rxbufstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch7_rxbyteisaligned : IN STD_LOGIC;
    ch7_rxbyterealign : IN STD_LOGIC;
    ch7_rxcdrlock : IN STD_LOGIC;
    ch7_rxcdrphdone : IN STD_LOGIC;
    ch7_rxchanbondseq : IN STD_LOGIC;
    ch7_rxchanisaligned : IN STD_LOGIC;
    ch7_rxchanrealign : IN STD_LOGIC;
    ch7_rxchbondo : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch7_rxclkcorcnt : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch7_rxcominitdet : IN STD_LOGIC;
    ch7_rxcommadet : IN STD_LOGIC;
    ch7_rxcomsasdet : IN STD_LOGIC;
    ch7_rxcomwakedet : IN STD_LOGIC;
    ch7_rxctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch7_rxctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch7_rxctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch7_rxctrl3 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch7_rxdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch7_rxdccdone : IN STD_LOGIC;
    ch7_rxdlyalignerr : IN STD_LOGIC;
    ch7_rxdlyalignprog : IN STD_LOGIC;
    ch7_rxelecidle : IN STD_LOGIC;
    ch7_rxfinealigndone : IN STD_LOGIC;
    ch7_rxosintdone : IN STD_LOGIC;
    ch7_rxosintstarted : IN STD_LOGIC;
    ch7_rxosintstrobedone : IN STD_LOGIC;
    ch7_rxosintstrobestarted : IN STD_LOGIC;
    ch7_rxphalignerr : IN STD_LOGIC;
    ch7_rxphdlyresetdone : IN STD_LOGIC;
    ch7_rxphsetinitdone : IN STD_LOGIC;
    ch7_rxphshift180done : IN STD_LOGIC;
    ch7_rxpolarity : OUT STD_LOGIC;
    ch7_rxprbserr : IN STD_LOGIC;
    ch7_rxprbslocked : IN STD_LOGIC;
    ch7_rxrate : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch7_rxrecclkout : IN STD_LOGIC;
    ch7_rxmstresetdone : IN STD_LOGIC;
    ch7_rxsliderdy : IN STD_LOGIC;
    ch7_rxstartofseq : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch7_rxstatus : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch7_rxsyncdone : IN STD_LOGIC;
    ch7_rxtermination : OUT STD_LOGIC;
    ch7_rxvalid : IN STD_LOGIC;
    ch7_eyescandataerror : IN STD_LOGIC;
    ch7_cfokovwrrdy0 : IN STD_LOGIC;
    ch7_cfokovwrrdy1 : IN STD_LOGIC;
    gt_bufgtce : IN STD_LOGIC;
    gt_bufgtrst : IN STD_LOGIC;
    gt_bufgtcemask : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_bufgtrstmask : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    gt_bufgtdiv : IN STD_LOGIC_VECTOR(11 DOWNTO 0)
  );
END COMPONENT;
COMPONENT pcie_versal_0_support_gt_quad_0
  PORT (
    apb3clk : IN STD_LOGIC;
    axisclk : IN STD_LOGIC;
    altclk : IN STD_LOGIC;
    rxmarginclk : IN STD_LOGIC;
    apb3paddr : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    apb3penable : IN STD_LOGIC;
    apb3presetn : IN STD_LOGIC;
    apb3prdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    apb3psel : IN STD_LOGIC;
    apb3pslverr : OUT STD_LOGIC;
    apb3pready : OUT STD_LOGIC;
    apb3pwdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    apb3pwrite : IN STD_LOGIC;
    rcalenb : IN STD_LOGIC;
    hsclk0_lcpllreset : IN STD_LOGIC;
    hsclk0_rpllreset : IN STD_LOGIC;
    hsclk1_lcpllreset : IN STD_LOGIC;
    hsclk1_rpllreset : IN STD_LOGIC;
    hsclk0_lcplllock : OUT STD_LOGIC;
    hsclk1_lcplllock : OUT STD_LOGIC;
    hsclk0_rplllock : OUT STD_LOGIC;
    hsclk1_rplllock : OUT STD_LOGIC;
    gtpowergood : OUT STD_LOGIC;
    ch0_pcierstb : IN STD_LOGIC;
    ch1_pcierstb : IN STD_LOGIC;
    ch2_pcierstb : IN STD_LOGIC;
    ch3_pcierstb : IN STD_LOGIC;
    pcielinkreachtarget : IN STD_LOGIC;
    pcieltssm : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    rxmarginreqack : OUT STD_LOGIC;
    rxmarginrescmd : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxmarginreslanenum : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxmarginrespayld : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxmarginresreq : OUT STD_LOGIC;
    rxmarginreqcmd : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxmarginreqlanenum : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxmarginreqpayld : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxmarginreqreq : IN STD_LOGIC;
    rxmarginresack : IN STD_LOGIC;
    ch0_iloreset : IN STD_LOGIC;
    ch1_iloreset : IN STD_LOGIC;
    ch2_iloreset : IN STD_LOGIC;
    ch3_iloreset : IN STD_LOGIC;
    ch0_iloresetdone : OUT STD_LOGIC;
    ch1_iloresetdone : OUT STD_LOGIC;
    ch2_iloresetdone : OUT STD_LOGIC;
    ch3_iloresetdone : OUT STD_LOGIC;
    ch0_phystatus : OUT STD_LOGIC;
    ch1_phystatus : OUT STD_LOGIC;
    ch2_phystatus : OUT STD_LOGIC;
    ch3_phystatus : OUT STD_LOGIC;
    correcterr : OUT STD_LOGIC;
    debugtracetvalid : OUT STD_LOGIC;
    hsclk0_lcpllfbclklost : OUT STD_LOGIC;
    hsclk0_lcpllrefclklost : OUT STD_LOGIC;
    hsclk0_lcpllrefclkmonitor : OUT STD_LOGIC;
    hsclk0_rpllfbclklost : OUT STD_LOGIC;
    hsclk0_rpllrefclklost : OUT STD_LOGIC;
    hsclk0_rpllrefclkmonitor : OUT STD_LOGIC;
    hsclk1_lcpllfbclklost : OUT STD_LOGIC;
    hsclk1_lcpllrefclklost : OUT STD_LOGIC;
    hsclk1_lcpllrefclkmonitor : OUT STD_LOGIC;
    hsclk1_rpllfbclklost : OUT STD_LOGIC;
    hsclk1_rpllrefclklost : OUT STD_LOGIC;
    hsclk1_rpllrefclkmonitor : OUT STD_LOGIC;
    bgbypassb : IN STD_LOGIC;
    bgmonitorenb : IN STD_LOGIC;
    bgpdb : IN STD_LOGIC;
    bgrcalovrdenb : IN STD_LOGIC;
    debugtraceclk : IN STD_LOGIC;
    debugtraceready : IN STD_LOGIC;
    hsclk0_lcpllclkrsvd0 : IN STD_LOGIC;
    hsclk0_lcpllclkrsvd1 : IN STD_LOGIC;
    hsclk0_lcpllpd : IN STD_LOGIC;
    hsclk0_rpllpd : IN STD_LOGIC;
    hsclk0_lcpllresetbypassmode : IN STD_LOGIC;
    hsclk0_lcpllsdmtoggle : IN STD_LOGIC;
    hsclk0_rpllclkrsvd0 : IN STD_LOGIC;
    hsclk0_rpllclkrsvd1 : IN STD_LOGIC;
    hsclk0_rpllresetbypassmode : IN STD_LOGIC;
    hsclk0_rpllsdmtoggle : IN STD_LOGIC;
    hsclk1_lcpllclkrsvd0 : IN STD_LOGIC;
    hsclk1_lcpllclkrsvd1 : IN STD_LOGIC;
    hsclk1_lcpllpd : IN STD_LOGIC;
    hsclk1_lcpllresetbypassmode : IN STD_LOGIC;
    hsclk1_lcpllsdmtoggle : IN STD_LOGIC;
    hsclk1_rpllclkrsvd0 : IN STD_LOGIC;
    hsclk1_rpllclkrsvd1 : IN STD_LOGIC;
    hsclk1_rpllpd : IN STD_LOGIC;
    hsclk1_rpllresetbypassmode : IN STD_LOGIC;
    hsclk1_rpllsdmtoggle : IN STD_LOGIC;
    refclk0_gtrefclkpd : IN STD_LOGIC;
    refclk0_clktestsig : IN STD_LOGIC;
    refclk1_gtrefclkpd : IN STD_LOGIC;
    refclk1_clktestsig : IN STD_LOGIC;
    trigackout0 : IN STD_LOGIC;
    trigin0 : IN STD_LOGIC;
    ubenable : IN STD_LOGIC;
    ubiolmbrst : IN STD_LOGIC;
    ubmbrst : IN STD_LOGIC;
    ubrxuart : IN STD_LOGIC;
    gpo : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    debugtracetdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    bgrcalovrd : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    hsclk0_lcpllrefclksel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    hsclk1_lcpllrefclksel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    hsclk0_rpllrefclksel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    hsclk1_rpllrefclksel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    hsclk0_lcpllfbdiv : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_lcpllrsvd0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_lcpllrsvd1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_rpllfbdiv : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_rpllrsvd0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_rpllrsvd1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_lcpllfbdiv : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_lcpllrsvd0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_lcpllrsvd1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_rpllfbdiv : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_rpllrsvd0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_rpllrsvd1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    refclk0_gtrefclkpdint : OUT STD_LOGIC;
    refclk0_clktestsigint : OUT STD_LOGIC;
    refclk1_gtrefclkpdint : OUT STD_LOGIC;
    refclk1_clktestsigint : OUT STD_LOGIC;
    hsclk0_rxrecclkout0 : OUT STD_LOGIC;
    hsclk0_rxrecclkout1 : OUT STD_LOGIC;
    hsclk1_rxrecclkout0 : OUT STD_LOGIC;
    hsclk1_rxrecclkout1 : OUT STD_LOGIC;
    hsclk0_lcpllsdmdata : IN STD_LOGIC_VECTOR(25 DOWNTO 0);
    hsclk1_lcpllsdmdata : IN STD_LOGIC_VECTOR(25 DOWNTO 0);
    hsclk0_rpllsdmdata : IN STD_LOGIC_VECTOR(25 DOWNTO 0);
    hsclk1_rpllsdmdata : IN STD_LOGIC_VECTOR(25 DOWNTO 0);
    gpi : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ctrlrsvdin0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ctrlrsvdin1 : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
    ubintr : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    hsclk0_lcpllrsvdout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_lcpllrsvdout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_rpllrsvdout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_rpllrsvdout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    trigackin0 : OUT STD_LOGIC;
    trigout0 : OUT STD_LOGIC;
    ubinterrupt : OUT STD_LOGIC;
    ubtxuart : OUT STD_LOGIC;
    uncorrecterr : OUT STD_LOGIC;
    s0_axis_tlast : IN STD_LOGIC;
    s1_axis_tlast : IN STD_LOGIC;
    s2_axis_tlast : IN STD_LOGIC;
    s0_axis_tvalid : IN STD_LOGIC;
    s1_axis_tvalid : IN STD_LOGIC;
    s2_axis_tvalid : IN STD_LOGIC;
    s0_axis_tready : OUT STD_LOGIC;
    s1_axis_tready : OUT STD_LOGIC;
    s2_axis_tready : OUT STD_LOGIC;
    s0_axis_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s1_axis_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s2_axis_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    m0_axis_tready : IN STD_LOGIC;
    m1_axis_tready : IN STD_LOGIC;
    m2_axis_tready : IN STD_LOGIC;
    m0_axis_tlast : OUT STD_LOGIC;
    m1_axis_tlast : OUT STD_LOGIC;
    m2_axis_tlast : OUT STD_LOGIC;
    m0_axis_tvalid : OUT STD_LOGIC;
    m1_axis_tvalid : OUT STD_LOGIC;
    m2_axis_tvalid : OUT STD_LOGIC;
    m0_axis_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m1_axis_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m2_axis_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    hsclk0_lcpllresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ctrlrsvdout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    hsclk1_lcpllresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    hsclk0_rpllresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    hsclk1_rpllresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxp : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxn : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txp : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txn : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch0_txheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch0_txsequence : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch0_gttxreset : IN STD_LOGIC;
    ch0_txprogdivreset : IN STD_LOGIC;
    ch0_txuserrdy : IN STD_LOGIC;
    ch0_txphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txcominit : IN STD_LOGIC;
    ch0_txcomsas : IN STD_LOGIC;
    ch0_txcomwake : IN STD_LOGIC;
    ch0_txdapicodeovrden : IN STD_LOGIC;
    ch0_txdapicodereset : IN STD_LOGIC;
    ch0_txdetectrx : IN STD_LOGIC;
    ch0_txlatclk : IN STD_LOGIC;
    ch0_txphdlytstclk : IN STD_LOGIC;
    ch0_txdlyalignreq : IN STD_LOGIC;
    ch0_txelecidle : IN STD_LOGIC;
    ch0_txinhibit : IN STD_LOGIC;
    ch0_txmldchaindone : IN STD_LOGIC;
    ch0_txmldchainreq : IN STD_LOGIC;
    ch0_txoneszeros : IN STD_LOGIC;
    ch0_txpausedelayalign : IN STD_LOGIC;
    ch0_txpcsresetmask : IN STD_LOGIC;
    ch0_txphalignreq : IN STD_LOGIC;
    ch0_txphdlypd : IN STD_LOGIC;
    ch0_txphdlyreset : IN STD_LOGIC;
    ch0_txphsetinitreq : IN STD_LOGIC;
    ch0_txphshift180 : IN STD_LOGIC;
    ch0_txpicodeovrden : IN STD_LOGIC;
    ch0_txpicodereset : IN STD_LOGIC;
    ch0_txpippmen : IN STD_LOGIC;
    ch0_txpisopd : IN STD_LOGIC;
    ch0_txpolarity : IN STD_LOGIC;
    ch0_txprbsforceerr : IN STD_LOGIC;
    ch0_txswing : IN STD_LOGIC;
    ch0_txsyncallin : IN STD_LOGIC;
    ch0_tx10gstat : OUT STD_LOGIC;
    ch0_txcomfinish : OUT STD_LOGIC;
    ch0_txdccdone : OUT STD_LOGIC;
    ch0_txdlyalignerr : OUT STD_LOGIC;
    ch0_txdlyalignprog : OUT STD_LOGIC;
    ch0_txphaligndone : OUT STD_LOGIC;
    ch0_txphalignerr : OUT STD_LOGIC;
    ch0_txphalignoutrsvd : OUT STD_LOGIC;
    ch0_txphdlyresetdone : OUT STD_LOGIC;
    ch0_txphsetinitdone : OUT STD_LOGIC;
    ch0_txphshift180done : OUT STD_LOGIC;
    ch0_txsyncdone : OUT STD_LOGIC;
    ch0_txbufstatus : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_txdeemph : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txmstreset : IN STD_LOGIC;
    ch0_txmstresetdone : OUT STD_LOGIC;
    ch0_txmargin : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_txpmaresetmask : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_txprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_txdiffctrl : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txpippmstepsize : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txpostcursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txprecursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txmaincursor : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch0_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_txdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_txprogdivresetdone : OUT STD_LOGIC;
    ch0_txpmaresetdone : OUT STD_LOGIC;
    ch0_txresetdone : OUT STD_LOGIC;
    ch0_txoutclk : OUT STD_LOGIC;
    ch0_txusrclk : IN STD_LOGIC;
    ch1_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch1_txheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch1_txsequence : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch1_gttxreset : IN STD_LOGIC;
    ch1_txprogdivreset : IN STD_LOGIC;
    ch1_txuserrdy : IN STD_LOGIC;
    ch1_txphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txcominit : IN STD_LOGIC;
    ch1_txcomsas : IN STD_LOGIC;
    ch1_txcomwake : IN STD_LOGIC;
    ch1_txdapicodeovrden : IN STD_LOGIC;
    ch1_txdapicodereset : IN STD_LOGIC;
    ch1_txdetectrx : IN STD_LOGIC;
    ch1_txlatclk : IN STD_LOGIC;
    ch1_txphdlytstclk : IN STD_LOGIC;
    ch1_txdlyalignreq : IN STD_LOGIC;
    ch1_txelecidle : IN STD_LOGIC;
    ch1_txinhibit : IN STD_LOGIC;
    ch1_txmldchaindone : IN STD_LOGIC;
    ch1_txmldchainreq : IN STD_LOGIC;
    ch1_txoneszeros : IN STD_LOGIC;
    ch1_txpausedelayalign : IN STD_LOGIC;
    ch1_txpcsresetmask : IN STD_LOGIC;
    ch1_txphalignreq : IN STD_LOGIC;
    ch1_txphdlypd : IN STD_LOGIC;
    ch1_txphdlyreset : IN STD_LOGIC;
    ch1_txphsetinitreq : IN STD_LOGIC;
    ch1_txphshift180 : IN STD_LOGIC;
    ch1_txpicodeovrden : IN STD_LOGIC;
    ch1_txpicodereset : IN STD_LOGIC;
    ch1_txpippmen : IN STD_LOGIC;
    ch1_txpisopd : IN STD_LOGIC;
    ch1_txpolarity : IN STD_LOGIC;
    ch1_txprbsforceerr : IN STD_LOGIC;
    ch1_txswing : IN STD_LOGIC;
    ch1_txsyncallin : IN STD_LOGIC;
    ch1_tx10gstat : OUT STD_LOGIC;
    ch1_txcomfinish : OUT STD_LOGIC;
    ch1_txdccdone : OUT STD_LOGIC;
    ch1_txdlyalignerr : OUT STD_LOGIC;
    ch1_txdlyalignprog : OUT STD_LOGIC;
    ch1_txphaligndone : OUT STD_LOGIC;
    ch1_txphalignerr : OUT STD_LOGIC;
    ch1_txphalignoutrsvd : OUT STD_LOGIC;
    ch1_txphdlyresetdone : OUT STD_LOGIC;
    ch1_txphsetinitdone : OUT STD_LOGIC;
    ch1_txphshift180done : OUT STD_LOGIC;
    ch1_txsyncdone : OUT STD_LOGIC;
    ch1_txbufstatus : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_txdeemph : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txmstreset : IN STD_LOGIC;
    ch1_txmstresetdone : OUT STD_LOGIC;
    ch1_txmargin : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_txpmaresetmask : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_txprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch1_txdiffctrl : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txpippmstepsize : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txpostcursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txprecursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txmaincursor : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch1_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_txdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_txprogdivresetdone : OUT STD_LOGIC;
    ch1_txpmaresetdone : OUT STD_LOGIC;
    ch1_txresetdone : OUT STD_LOGIC;
    ch1_txoutclk : OUT STD_LOGIC;
    ch1_txusrclk : IN STD_LOGIC;
    ch2_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch2_txheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch2_txsequence : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch2_gttxreset : IN STD_LOGIC;
    ch2_txprogdivreset : IN STD_LOGIC;
    ch2_txuserrdy : IN STD_LOGIC;
    ch2_txphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txcominit : IN STD_LOGIC;
    ch2_txcomsas : IN STD_LOGIC;
    ch2_txcomwake : IN STD_LOGIC;
    ch2_txdapicodeovrden : IN STD_LOGIC;
    ch2_txdapicodereset : IN STD_LOGIC;
    ch2_txdetectrx : IN STD_LOGIC;
    ch2_txlatclk : IN STD_LOGIC;
    ch2_txphdlytstclk : IN STD_LOGIC;
    ch2_txdlyalignreq : IN STD_LOGIC;
    ch2_txelecidle : IN STD_LOGIC;
    ch2_txinhibit : IN STD_LOGIC;
    ch2_txmldchaindone : IN STD_LOGIC;
    ch2_txmldchainreq : IN STD_LOGIC;
    ch2_txoneszeros : IN STD_LOGIC;
    ch2_txpausedelayalign : IN STD_LOGIC;
    ch2_txpcsresetmask : IN STD_LOGIC;
    ch2_txphalignreq : IN STD_LOGIC;
    ch2_txphdlypd : IN STD_LOGIC;
    ch2_txphdlyreset : IN STD_LOGIC;
    ch2_txphsetinitreq : IN STD_LOGIC;
    ch2_txphshift180 : IN STD_LOGIC;
    ch2_txpicodeovrden : IN STD_LOGIC;
    ch2_txpicodereset : IN STD_LOGIC;
    ch2_txpippmen : IN STD_LOGIC;
    ch2_txpisopd : IN STD_LOGIC;
    ch2_txpolarity : IN STD_LOGIC;
    ch2_txprbsforceerr : IN STD_LOGIC;
    ch2_txswing : IN STD_LOGIC;
    ch2_txsyncallin : IN STD_LOGIC;
    ch2_tx10gstat : OUT STD_LOGIC;
    ch2_txcomfinish : OUT STD_LOGIC;
    ch2_txdccdone : OUT STD_LOGIC;
    ch2_txdlyalignerr : OUT STD_LOGIC;
    ch2_txdlyalignprog : OUT STD_LOGIC;
    ch2_txphaligndone : OUT STD_LOGIC;
    ch2_txphalignerr : OUT STD_LOGIC;
    ch2_txphalignoutrsvd : OUT STD_LOGIC;
    ch2_txphdlyresetdone : OUT STD_LOGIC;
    ch2_txphsetinitdone : OUT STD_LOGIC;
    ch2_txphshift180done : OUT STD_LOGIC;
    ch2_txsyncdone : OUT STD_LOGIC;
    ch2_txbufstatus : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_txdeemph : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txmstreset : IN STD_LOGIC;
    ch2_txmstresetdone : OUT STD_LOGIC;
    ch2_txmargin : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_txpmaresetmask : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_txprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch2_txdiffctrl : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txpippmstepsize : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txpostcursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txprecursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txmaincursor : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch2_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_txdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_txprogdivresetdone : OUT STD_LOGIC;
    ch2_txpmaresetdone : OUT STD_LOGIC;
    ch2_txresetdone : OUT STD_LOGIC;
    ch2_txoutclk : OUT STD_LOGIC;
    ch2_txusrclk : IN STD_LOGIC;
    ch3_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch3_txheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch3_txsequence : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch3_gttxreset : IN STD_LOGIC;
    ch3_txprogdivreset : IN STD_LOGIC;
    ch3_txuserrdy : IN STD_LOGIC;
    ch3_txphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txcominit : IN STD_LOGIC;
    ch3_txcomsas : IN STD_LOGIC;
    ch3_txcomwake : IN STD_LOGIC;
    ch3_txdapicodeovrden : IN STD_LOGIC;
    ch3_txdapicodereset : IN STD_LOGIC;
    ch3_txdetectrx : IN STD_LOGIC;
    ch3_txlatclk : IN STD_LOGIC;
    ch3_txphdlytstclk : IN STD_LOGIC;
    ch3_txdlyalignreq : IN STD_LOGIC;
    ch3_txelecidle : IN STD_LOGIC;
    ch3_txinhibit : IN STD_LOGIC;
    ch3_txmldchaindone : IN STD_LOGIC;
    ch3_txmldchainreq : IN STD_LOGIC;
    ch3_txoneszeros : IN STD_LOGIC;
    ch3_txpausedelayalign : IN STD_LOGIC;
    ch3_txpcsresetmask : IN STD_LOGIC;
    ch3_txphalignreq : IN STD_LOGIC;
    ch3_txphdlypd : IN STD_LOGIC;
    ch3_txphdlyreset : IN STD_LOGIC;
    ch3_txphsetinitreq : IN STD_LOGIC;
    ch3_txphshift180 : IN STD_LOGIC;
    ch3_txpicodeovrden : IN STD_LOGIC;
    ch3_txpicodereset : IN STD_LOGIC;
    ch3_txpippmen : IN STD_LOGIC;
    ch3_txpisopd : IN STD_LOGIC;
    ch3_txpolarity : IN STD_LOGIC;
    ch3_txprbsforceerr : IN STD_LOGIC;
    ch3_txswing : IN STD_LOGIC;
    ch3_txsyncallin : IN STD_LOGIC;
    ch3_tx10gstat : OUT STD_LOGIC;
    ch3_txcomfinish : OUT STD_LOGIC;
    ch3_txdccdone : OUT STD_LOGIC;
    ch3_txdlyalignerr : OUT STD_LOGIC;
    ch3_txdlyalignprog : OUT STD_LOGIC;
    ch3_txphaligndone : OUT STD_LOGIC;
    ch3_txphalignerr : OUT STD_LOGIC;
    ch3_txphalignoutrsvd : OUT STD_LOGIC;
    ch3_txphdlyresetdone : OUT STD_LOGIC;
    ch3_txphsetinitdone : OUT STD_LOGIC;
    ch3_txphshift180done : OUT STD_LOGIC;
    ch3_txsyncdone : OUT STD_LOGIC;
    ch3_txbufstatus : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_txdeemph : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txmstreset : IN STD_LOGIC;
    ch3_txmstresetdone : OUT STD_LOGIC;
    ch3_txmargin : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_txpmaresetmask : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_txprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch3_txdiffctrl : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txpippmstepsize : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txpostcursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txprecursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txmaincursor : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch3_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_txdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_txprogdivresetdone : OUT STD_LOGIC;
    ch3_txpmaresetdone : OUT STD_LOGIC;
    ch3_txresetdone : OUT STD_LOGIC;
    ch3_txoutclk : OUT STD_LOGIC;
    ch3_txusrclk : IN STD_LOGIC;
    ch0_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch0_rxdatavalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxheader : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch0_rxgearboxslip : IN STD_LOGIC;
    ch0_rxlatclk : IN STD_LOGIC;
    ch0_gtrxreset : IN STD_LOGIC;
    ch0_rxprogdivreset : IN STD_LOGIC;
    ch0_rxuserrdy : IN STD_LOGIC;
    ch0_rxprogdivresetdone : OUT STD_LOGIC;
    ch0_rxpmaresetdone : OUT STD_LOGIC;
    ch0_rxresetdone : OUT STD_LOGIC;
    ch0_rx10gstat : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxbufstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_rxbyteisaligned : OUT STD_LOGIC;
    ch0_rxbyterealign : OUT STD_LOGIC;
    ch0_rxcdrhold : IN STD_LOGIC;
    ch0_rxcdrlock : OUT STD_LOGIC;
    ch0_rxcdrovrden : IN STD_LOGIC;
    ch0_rxcdrphdone : OUT STD_LOGIC;
    ch0_rxcdrreset : IN STD_LOGIC;
    ch0_rxchanbondseq : OUT STD_LOGIC;
    ch0_rxchanisaligned : OUT STD_LOGIC;
    ch0_rxchanrealign : OUT STD_LOGIC;
    ch0_rxchbondi : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_rxchbondo : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_rxclkcorcnt : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxcominitdet : OUT STD_LOGIC;
    ch0_rxcommadet : OUT STD_LOGIC;
    ch0_rxcomsasdet : OUT STD_LOGIC;
    ch0_rxcomwakedet : OUT STD_LOGIC;
    ch0_rxctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_rxctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_rxctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxctrl3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxdapicodeovrden : IN STD_LOGIC;
    ch0_rxdapicodereset : IN STD_LOGIC;
    ch0_rxdataextendrsvd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxdccdone : OUT STD_LOGIC;
    ch0_rxdlyalignerr : OUT STD_LOGIC;
    ch0_rxdlyalignprog : OUT STD_LOGIC;
    ch0_rxdlyalignreq : IN STD_LOGIC;
    ch0_rxelecidle : OUT STD_LOGIC;
    ch0_rxeqtraining : IN STD_LOGIC;
    ch0_rxfinealigndone : OUT STD_LOGIC;
    ch0_rxheadervalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxlpmen : IN STD_LOGIC;
    ch0_rxmldchaindone : IN STD_LOGIC;
    ch0_rxmldchainreq : IN STD_LOGIC;
    ch0_rxmlfinealignreq : IN STD_LOGIC;
    ch0_rxoobreset : IN STD_LOGIC;
    ch0_rxosintdone : OUT STD_LOGIC;
    ch0_rxosintstarted : OUT STD_LOGIC;
    ch0_rxosintstrobedone : OUT STD_LOGIC;
    ch0_rxosintstrobestarted : OUT STD_LOGIC;
    ch0_rxpcsresetmask : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_rxpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxphaligndone : OUT STD_LOGIC;
    ch0_rxphalignerr : OUT STD_LOGIC;
    ch0_rxphalignreq : IN STD_LOGIC;
    ch0_rxphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxphdlypd : IN STD_LOGIC;
    ch0_rxphdlyreset : IN STD_LOGIC;
    ch0_rxphdlyresetdone : OUT STD_LOGIC;
    ch0_rxphsetinitdone : OUT STD_LOGIC;
    ch0_rxphsetinitreq : IN STD_LOGIC;
    ch0_rxphshift180 : IN STD_LOGIC;
    ch0_rxphshift180done : OUT STD_LOGIC;
    ch0_rxpmaresetmask : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch0_rxpolarity : IN STD_LOGIC;
    ch0_rxprbscntreset : IN STD_LOGIC;
    ch0_rxprbserr : OUT STD_LOGIC;
    ch0_rxprbslocked : OUT STD_LOGIC;
    ch0_rxprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxmstreset : IN STD_LOGIC;
    ch0_rxmstresetdone : OUT STD_LOGIC;
    ch0_rxslide : IN STD_LOGIC;
    ch0_rxsliderdy : OUT STD_LOGIC;
    ch0_rxstartofseq : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_rxsyncallin : IN STD_LOGIC;
    ch0_rxsyncdone : OUT STD_LOGIC;
    ch0_rxtermination : IN STD_LOGIC;
    ch0_rxvalid : OUT STD_LOGIC;
    ch0_cdrbmcdrreq : IN STD_LOGIC;
    ch0_cdrfreqos : IN STD_LOGIC;
    ch0_cdrincpctrl : IN STD_LOGIC;
    ch0_cdrstepdir : IN STD_LOGIC;
    ch0_cdrstepsq : IN STD_LOGIC;
    ch0_cdrstepsx : IN STD_LOGIC;
    ch0_cfokovrdfinish : IN STD_LOGIC;
    ch0_cfokovrdpulse : IN STD_LOGIC;
    ch0_cfokovrdstart : IN STD_LOGIC;
    ch0_eyescanreset : IN STD_LOGIC;
    ch0_eyescantrigger : IN STD_LOGIC;
    ch0_eyescandataerror : OUT STD_LOGIC;
    ch0_cfokovrdrdy0 : OUT STD_LOGIC;
    ch0_cfokovrdrdy1 : OUT STD_LOGIC;
    ch0_rxoutclk : OUT STD_LOGIC;
    ch0_rxusrclk : IN STD_LOGIC;
    ch1_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch1_rxdatavalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxheader : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch1_rxgearboxslip : IN STD_LOGIC;
    ch1_rxlatclk : IN STD_LOGIC;
    ch1_gtrxreset : IN STD_LOGIC;
    ch1_rxprogdivreset : IN STD_LOGIC;
    ch1_rxuserrdy : IN STD_LOGIC;
    ch1_rxprogdivresetdone : OUT STD_LOGIC;
    ch1_rxpmaresetdone : OUT STD_LOGIC;
    ch1_rxresetdone : OUT STD_LOGIC;
    ch1_rx10gstat : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxbufstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_rxbyteisaligned : OUT STD_LOGIC;
    ch1_rxbyterealign : OUT STD_LOGIC;
    ch1_rxcdrhold : IN STD_LOGIC;
    ch1_rxcdrlock : OUT STD_LOGIC;
    ch1_rxcdrovrden : IN STD_LOGIC;
    ch1_rxcdrphdone : OUT STD_LOGIC;
    ch1_rxcdrreset : IN STD_LOGIC;
    ch1_rxchanbondseq : OUT STD_LOGIC;
    ch1_rxchanisaligned : OUT STD_LOGIC;
    ch1_rxchanrealign : OUT STD_LOGIC;
    ch1_rxchbondi : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_rxchbondo : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_rxclkcorcnt : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxcominitdet : OUT STD_LOGIC;
    ch1_rxcommadet : OUT STD_LOGIC;
    ch1_rxcomsasdet : OUT STD_LOGIC;
    ch1_rxcomwakedet : OUT STD_LOGIC;
    ch1_rxctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_rxctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_rxctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxctrl3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxdapicodeovrden : IN STD_LOGIC;
    ch1_rxdapicodereset : IN STD_LOGIC;
    ch1_rxdataextendrsvd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxdccdone : OUT STD_LOGIC;
    ch1_rxdlyalignerr : OUT STD_LOGIC;
    ch1_rxdlyalignprog : OUT STD_LOGIC;
    ch1_rxdlyalignreq : IN STD_LOGIC;
    ch1_rxelecidle : OUT STD_LOGIC;
    ch1_rxeqtraining : IN STD_LOGIC;
    ch1_rxfinealigndone : OUT STD_LOGIC;
    ch1_rxheadervalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxlpmen : IN STD_LOGIC;
    ch1_rxmldchaindone : IN STD_LOGIC;
    ch1_rxmldchainreq : IN STD_LOGIC;
    ch1_rxmlfinealignreq : IN STD_LOGIC;
    ch1_rxoobreset : IN STD_LOGIC;
    ch1_rxosintdone : OUT STD_LOGIC;
    ch1_rxosintstarted : OUT STD_LOGIC;
    ch1_rxosintstrobedone : OUT STD_LOGIC;
    ch1_rxosintstrobestarted : OUT STD_LOGIC;
    ch1_rxpcsresetmask : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_rxpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxphaligndone : OUT STD_LOGIC;
    ch1_rxphalignerr : OUT STD_LOGIC;
    ch1_rxphalignreq : IN STD_LOGIC;
    ch1_rxphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxphdlypd : IN STD_LOGIC;
    ch1_rxphdlyreset : IN STD_LOGIC;
    ch1_rxphdlyresetdone : OUT STD_LOGIC;
    ch1_rxphsetinitdone : OUT STD_LOGIC;
    ch1_rxphsetinitreq : IN STD_LOGIC;
    ch1_rxphshift180 : IN STD_LOGIC;
    ch1_rxphshift180done : OUT STD_LOGIC;
    ch1_rxpmaresetmask : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch1_rxpolarity : IN STD_LOGIC;
    ch1_rxprbscntreset : IN STD_LOGIC;
    ch1_rxprbserr : OUT STD_LOGIC;
    ch1_rxprbslocked : OUT STD_LOGIC;
    ch1_rxprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch1_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxmstreset : IN STD_LOGIC;
    ch1_rxmstresetdone : OUT STD_LOGIC;
    ch1_rxslide : IN STD_LOGIC;
    ch1_rxsliderdy : OUT STD_LOGIC;
    ch1_rxstartofseq : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_rxsyncallin : IN STD_LOGIC;
    ch1_rxsyncdone : OUT STD_LOGIC;
    ch1_rxtermination : IN STD_LOGIC;
    ch1_rxvalid : OUT STD_LOGIC;
    ch1_cdrbmcdrreq : IN STD_LOGIC;
    ch1_cdrfreqos : IN STD_LOGIC;
    ch1_cdrincpctrl : IN STD_LOGIC;
    ch1_cdrstepdir : IN STD_LOGIC;
    ch1_cdrstepsq : IN STD_LOGIC;
    ch1_cdrstepsx : IN STD_LOGIC;
    ch1_cfokovrdfinish : IN STD_LOGIC;
    ch1_cfokovrdpulse : IN STD_LOGIC;
    ch1_cfokovrdstart : IN STD_LOGIC;
    ch1_eyescanreset : IN STD_LOGIC;
    ch1_eyescantrigger : IN STD_LOGIC;
    ch1_eyescandataerror : OUT STD_LOGIC;
    ch1_cfokovrdrdy0 : OUT STD_LOGIC;
    ch1_cfokovrdrdy1 : OUT STD_LOGIC;
    ch1_rxoutclk : OUT STD_LOGIC;
    ch1_rxusrclk : IN STD_LOGIC;
    ch2_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch2_rxdatavalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxheader : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch2_rxgearboxslip : IN STD_LOGIC;
    ch2_rxlatclk : IN STD_LOGIC;
    ch2_gtrxreset : IN STD_LOGIC;
    ch2_rxprogdivreset : IN STD_LOGIC;
    ch2_rxuserrdy : IN STD_LOGIC;
    ch2_rxprogdivresetdone : OUT STD_LOGIC;
    ch2_rxpmaresetdone : OUT STD_LOGIC;
    ch2_rxresetdone : OUT STD_LOGIC;
    ch2_rx10gstat : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxbufstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_rxbyteisaligned : OUT STD_LOGIC;
    ch2_rxbyterealign : OUT STD_LOGIC;
    ch2_rxcdrhold : IN STD_LOGIC;
    ch2_rxcdrlock : OUT STD_LOGIC;
    ch2_rxcdrovrden : IN STD_LOGIC;
    ch2_rxcdrphdone : OUT STD_LOGIC;
    ch2_rxcdrreset : IN STD_LOGIC;
    ch2_rxchanbondseq : OUT STD_LOGIC;
    ch2_rxchanisaligned : OUT STD_LOGIC;
    ch2_rxchanrealign : OUT STD_LOGIC;
    ch2_rxchbondi : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_rxchbondo : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_rxclkcorcnt : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxcominitdet : OUT STD_LOGIC;
    ch2_rxcommadet : OUT STD_LOGIC;
    ch2_rxcomsasdet : OUT STD_LOGIC;
    ch2_rxcomwakedet : OUT STD_LOGIC;
    ch2_rxctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_rxctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_rxctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxctrl3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxdapicodeovrden : IN STD_LOGIC;
    ch2_rxdapicodereset : IN STD_LOGIC;
    ch2_rxdataextendrsvd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxdccdone : OUT STD_LOGIC;
    ch2_rxdlyalignerr : OUT STD_LOGIC;
    ch2_rxdlyalignprog : OUT STD_LOGIC;
    ch2_rxdlyalignreq : IN STD_LOGIC;
    ch2_rxelecidle : OUT STD_LOGIC;
    ch2_rxeqtraining : IN STD_LOGIC;
    ch2_rxfinealigndone : OUT STD_LOGIC;
    ch2_rxheadervalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxlpmen : IN STD_LOGIC;
    ch2_rxmldchaindone : IN STD_LOGIC;
    ch2_rxmldchainreq : IN STD_LOGIC;
    ch2_rxmlfinealignreq : IN STD_LOGIC;
    ch2_rxoobreset : IN STD_LOGIC;
    ch2_rxosintdone : OUT STD_LOGIC;
    ch2_rxosintstarted : OUT STD_LOGIC;
    ch2_rxosintstrobedone : OUT STD_LOGIC;
    ch2_rxosintstrobestarted : OUT STD_LOGIC;
    ch2_rxpcsresetmask : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_rxpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxphaligndone : OUT STD_LOGIC;
    ch2_rxphalignerr : OUT STD_LOGIC;
    ch2_rxphalignreq : IN STD_LOGIC;
    ch2_rxphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxphdlypd : IN STD_LOGIC;
    ch2_rxphdlyreset : IN STD_LOGIC;
    ch2_rxphdlyresetdone : OUT STD_LOGIC;
    ch2_rxphsetinitdone : OUT STD_LOGIC;
    ch2_rxphsetinitreq : IN STD_LOGIC;
    ch2_rxphshift180 : IN STD_LOGIC;
    ch2_rxphshift180done : OUT STD_LOGIC;
    ch2_rxpmaresetmask : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch2_rxpolarity : IN STD_LOGIC;
    ch2_rxprbscntreset : IN STD_LOGIC;
    ch2_rxprbserr : OUT STD_LOGIC;
    ch2_rxprbslocked : OUT STD_LOGIC;
    ch2_rxprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch2_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxmstreset : IN STD_LOGIC;
    ch2_rxmstresetdone : OUT STD_LOGIC;
    ch2_rxslide : IN STD_LOGIC;
    ch2_rxsliderdy : OUT STD_LOGIC;
    ch2_rxstartofseq : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_rxsyncallin : IN STD_LOGIC;
    ch2_rxsyncdone : OUT STD_LOGIC;
    ch2_rxtermination : IN STD_LOGIC;
    ch2_rxvalid : OUT STD_LOGIC;
    ch2_cdrbmcdrreq : IN STD_LOGIC;
    ch2_cdrfreqos : IN STD_LOGIC;
    ch2_cdrincpctrl : IN STD_LOGIC;
    ch2_cdrstepdir : IN STD_LOGIC;
    ch2_cdrstepsq : IN STD_LOGIC;
    ch2_cdrstepsx : IN STD_LOGIC;
    ch2_cfokovrdfinish : IN STD_LOGIC;
    ch2_cfokovrdpulse : IN STD_LOGIC;
    ch2_cfokovrdstart : IN STD_LOGIC;
    ch2_eyescanreset : IN STD_LOGIC;
    ch2_eyescantrigger : IN STD_LOGIC;
    ch2_eyescandataerror : OUT STD_LOGIC;
    ch2_cfokovrdrdy0 : OUT STD_LOGIC;
    ch2_cfokovrdrdy1 : OUT STD_LOGIC;
    ch2_rxoutclk : OUT STD_LOGIC;
    ch2_rxusrclk : IN STD_LOGIC;
    ch3_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch3_rxdatavalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxheader : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch3_rxgearboxslip : IN STD_LOGIC;
    ch3_rxlatclk : IN STD_LOGIC;
    ch3_gtrxreset : IN STD_LOGIC;
    ch3_rxprogdivreset : IN STD_LOGIC;
    ch3_rxuserrdy : IN STD_LOGIC;
    ch3_rxprogdivresetdone : OUT STD_LOGIC;
    ch3_rxpmaresetdone : OUT STD_LOGIC;
    ch3_rxresetdone : OUT STD_LOGIC;
    ch3_rx10gstat : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxbufstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_rxbyteisaligned : OUT STD_LOGIC;
    ch3_rxbyterealign : OUT STD_LOGIC;
    ch3_rxcdrhold : IN STD_LOGIC;
    ch3_rxcdrlock : OUT STD_LOGIC;
    ch3_rxcdrovrden : IN STD_LOGIC;
    ch3_rxcdrphdone : OUT STD_LOGIC;
    ch3_rxcdrreset : IN STD_LOGIC;
    ch3_rxchanbondseq : OUT STD_LOGIC;
    ch3_rxchanisaligned : OUT STD_LOGIC;
    ch3_rxchanrealign : OUT STD_LOGIC;
    ch3_rxchbondi : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_rxchbondo : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_rxclkcorcnt : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxcominitdet : OUT STD_LOGIC;
    ch3_rxcommadet : OUT STD_LOGIC;
    ch3_rxcomsasdet : OUT STD_LOGIC;
    ch3_rxcomwakedet : OUT STD_LOGIC;
    ch3_rxctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_rxctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_rxctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxctrl3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxdapicodeovrden : IN STD_LOGIC;
    ch3_rxdapicodereset : IN STD_LOGIC;
    ch3_rxdataextendrsvd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxdccdone : OUT STD_LOGIC;
    ch3_rxdlyalignerr : OUT STD_LOGIC;
    ch3_rxdlyalignprog : OUT STD_LOGIC;
    ch3_rxdlyalignreq : IN STD_LOGIC;
    ch3_rxelecidle : OUT STD_LOGIC;
    ch3_rxeqtraining : IN STD_LOGIC;
    ch3_rxfinealigndone : OUT STD_LOGIC;
    ch3_rxheadervalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxlpmen : IN STD_LOGIC;
    ch3_rxmldchaindone : IN STD_LOGIC;
    ch3_rxmldchainreq : IN STD_LOGIC;
    ch3_rxmlfinealignreq : IN STD_LOGIC;
    ch3_rxoobreset : IN STD_LOGIC;
    ch3_rxosintdone : OUT STD_LOGIC;
    ch3_rxosintstarted : OUT STD_LOGIC;
    ch3_rxosintstrobedone : OUT STD_LOGIC;
    ch3_rxosintstrobestarted : OUT STD_LOGIC;
    ch3_rxpcsresetmask : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_rxpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxphaligndone : OUT STD_LOGIC;
    ch3_rxphalignerr : OUT STD_LOGIC;
    ch3_rxphalignreq : IN STD_LOGIC;
    ch3_rxphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxphdlypd : IN STD_LOGIC;
    ch3_rxphdlyreset : IN STD_LOGIC;
    ch3_rxphdlyresetdone : OUT STD_LOGIC;
    ch3_rxphsetinitdone : OUT STD_LOGIC;
    ch3_rxphsetinitreq : IN STD_LOGIC;
    ch3_rxphshift180 : IN STD_LOGIC;
    ch3_rxphshift180done : OUT STD_LOGIC;
    ch3_rxpmaresetmask : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch3_rxpolarity : IN STD_LOGIC;
    ch3_rxprbscntreset : IN STD_LOGIC;
    ch3_rxprbserr : OUT STD_LOGIC;
    ch3_rxprbslocked : OUT STD_LOGIC;
    ch3_rxprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch3_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxmstreset : IN STD_LOGIC;
    ch3_rxmstresetdone : OUT STD_LOGIC;
    ch3_rxslide : IN STD_LOGIC;
    ch3_rxsliderdy : OUT STD_LOGIC;
    ch3_rxstartofseq : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_rxsyncallin : IN STD_LOGIC;
    ch3_rxsyncdone : OUT STD_LOGIC;
    ch3_rxtermination : IN STD_LOGIC;
    ch3_rxvalid : OUT STD_LOGIC;
    ch3_cdrbmcdrreq : IN STD_LOGIC;
    ch3_cdrfreqos : IN STD_LOGIC;
    ch3_cdrincpctrl : IN STD_LOGIC;
    ch3_cdrstepdir : IN STD_LOGIC;
    ch3_cdrstepsq : IN STD_LOGIC;
    ch3_cdrstepsx : IN STD_LOGIC;
    ch3_cfokovrdfinish : IN STD_LOGIC;
    ch3_cfokovrdpulse : IN STD_LOGIC;
    ch3_cfokovrdstart : IN STD_LOGIC;
    ch3_eyescanreset : IN STD_LOGIC;
    ch3_eyescantrigger : IN STD_LOGIC;
    ch3_eyescandataerror : OUT STD_LOGIC;
    ch3_cfokovrdrdy0 : OUT STD_LOGIC;
    ch3_cfokovrdrdy1 : OUT STD_LOGIC;
    ch3_rxoutclk : OUT STD_LOGIC;
    ch3_rxusrclk : IN STD_LOGIC;
    ch0_bufgtce : OUT STD_LOGIC;
    ch0_bufgtrst : OUT STD_LOGIC;
    ch0_bufgtcemask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_bufgtrstmask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_bufgtdiv : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    ch0_clkrsvd0 : IN STD_LOGIC;
    ch0_clkrsvd1 : IN STD_LOGIC;
    ch0_dmonitorclk : IN STD_LOGIC;
    ch0_phyesmadaptsave : IN STD_LOGIC;
    ch0_iloresetmask : IN STD_LOGIC;
    ch0_loopback : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_dmonfiforeset : IN STD_LOGIC;
    ch0_pcsrsvdin : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_gtrsvd : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_tstin : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    ch0_pcsrsvdout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_pinrsvdas : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_dmonitoroutclk : OUT STD_LOGIC;
    ch0_resetexception : OUT STD_LOGIC;
    ch0_dmonitorout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    ch0_phyready : OUT STD_LOGIC;
    ch0_hsdppcsreset : IN STD_LOGIC;
    ch1_bufgtce : OUT STD_LOGIC;
    ch1_bufgtrst : OUT STD_LOGIC;
    ch1_bufgtcemask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch1_bufgtrstmask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch1_bufgtdiv : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    ch1_clkrsvd0 : IN STD_LOGIC;
    ch1_clkrsvd1 : IN STD_LOGIC;
    ch1_dmonitorclk : IN STD_LOGIC;
    ch1_phyesmadaptsave : IN STD_LOGIC;
    ch1_iloresetmask : IN STD_LOGIC;
    ch1_loopback : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_dmonfiforeset : IN STD_LOGIC;
    ch1_pcsrsvdin : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_gtrsvd : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_tstin : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    ch1_pcsrsvdout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_pinrsvdas : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_dmonitoroutclk : OUT STD_LOGIC;
    ch1_resetexception : OUT STD_LOGIC;
    ch1_dmonitorout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    ch1_phyready : OUT STD_LOGIC;
    ch1_hsdppcsreset : IN STD_LOGIC;
    ch2_bufgtce : OUT STD_LOGIC;
    ch2_bufgtrst : OUT STD_LOGIC;
    ch2_bufgtcemask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch2_bufgtrstmask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch2_bufgtdiv : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    ch2_clkrsvd0 : IN STD_LOGIC;
    ch2_clkrsvd1 : IN STD_LOGIC;
    ch2_dmonitorclk : IN STD_LOGIC;
    ch2_phyesmadaptsave : IN STD_LOGIC;
    ch2_iloresetmask : IN STD_LOGIC;
    ch2_loopback : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_dmonfiforeset : IN STD_LOGIC;
    ch2_pcsrsvdin : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_gtrsvd : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_tstin : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    ch2_pcsrsvdout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_pinrsvdas : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_dmonitoroutclk : OUT STD_LOGIC;
    ch2_resetexception : OUT STD_LOGIC;
    ch2_dmonitorout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    ch2_phyready : OUT STD_LOGIC;
    ch2_hsdppcsreset : IN STD_LOGIC;
    ch3_bufgtce : OUT STD_LOGIC;
    ch3_bufgtrst : OUT STD_LOGIC;
    ch3_bufgtcemask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch3_bufgtrstmask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch3_bufgtdiv : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    ch3_clkrsvd0 : IN STD_LOGIC;
    ch3_clkrsvd1 : IN STD_LOGIC;
    ch3_dmonitorclk : IN STD_LOGIC;
    ch3_phyesmadaptsave : IN STD_LOGIC;
    ch3_iloresetmask : IN STD_LOGIC;
    ch3_loopback : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_dmonfiforeset : IN STD_LOGIC;
    ch3_pcsrsvdin : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_gtrsvd : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_tstin : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    ch3_pcsrsvdout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_pinrsvdas : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_dmonitoroutclk : OUT STD_LOGIC;
    ch3_resetexception : OUT STD_LOGIC;
    ch3_dmonitorout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    ch3_phyready : OUT STD_LOGIC;
    ch3_hsdppcsreset : IN STD_LOGIC;
    resetdone_northin : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    resetdone_southin : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    resetdone_northout : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    resetdone_southout : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txpinorthin : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpinorthin : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpisouthin : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpisouthin : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    pipenorthin : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    pipesouthin : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    txpinorthout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpisouthout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpinorthout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpisouthout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pipenorthout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    pipesouthout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    GT_REFCLK0 : IN STD_LOGIC
  );
END COMPONENT;
COMPONENT pcie_versal_0_support_gt_quad_1
  PORT (
    apb3clk : IN STD_LOGIC;
    axisclk : IN STD_LOGIC;
    altclk : IN STD_LOGIC;
    rxmarginclk : IN STD_LOGIC;
    apb3paddr : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    apb3penable : IN STD_LOGIC;
    apb3presetn : IN STD_LOGIC;
    apb3prdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    apb3psel : IN STD_LOGIC;
    apb3pslverr : OUT STD_LOGIC;
    apb3pready : OUT STD_LOGIC;
    apb3pwdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    apb3pwrite : IN STD_LOGIC;
    rcalenb : IN STD_LOGIC;
    hsclk0_lcpllreset : IN STD_LOGIC;
    hsclk0_rpllreset : IN STD_LOGIC;
    hsclk1_lcpllreset : IN STD_LOGIC;
    hsclk1_rpllreset : IN STD_LOGIC;
    hsclk0_lcplllock : OUT STD_LOGIC;
    hsclk1_lcplllock : OUT STD_LOGIC;
    hsclk0_rplllock : OUT STD_LOGIC;
    hsclk1_rplllock : OUT STD_LOGIC;
    gtpowergood : OUT STD_LOGIC;
    ch0_pcierstb : IN STD_LOGIC;
    ch1_pcierstb : IN STD_LOGIC;
    ch2_pcierstb : IN STD_LOGIC;
    ch3_pcierstb : IN STD_LOGIC;
    pcielinkreachtarget : IN STD_LOGIC;
    pcieltssm : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    rxmarginreqack : OUT STD_LOGIC;
    rxmarginrescmd : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxmarginreslanenum : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxmarginrespayld : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxmarginresreq : OUT STD_LOGIC;
    rxmarginreqcmd : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxmarginreqlanenum : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxmarginreqpayld : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxmarginreqreq : IN STD_LOGIC;
    rxmarginresack : IN STD_LOGIC;
    ch0_iloreset : IN STD_LOGIC;
    ch1_iloreset : IN STD_LOGIC;
    ch2_iloreset : IN STD_LOGIC;
    ch3_iloreset : IN STD_LOGIC;
    ch0_iloresetdone : OUT STD_LOGIC;
    ch1_iloresetdone : OUT STD_LOGIC;
    ch2_iloresetdone : OUT STD_LOGIC;
    ch3_iloresetdone : OUT STD_LOGIC;
    ch0_phystatus : OUT STD_LOGIC;
    ch1_phystatus : OUT STD_LOGIC;
    ch2_phystatus : OUT STD_LOGIC;
    ch3_phystatus : OUT STD_LOGIC;
    correcterr : OUT STD_LOGIC;
    debugtracetvalid : OUT STD_LOGIC;
    hsclk0_lcpllfbclklost : OUT STD_LOGIC;
    hsclk0_lcpllrefclklost : OUT STD_LOGIC;
    hsclk0_lcpllrefclkmonitor : OUT STD_LOGIC;
    hsclk0_rpllfbclklost : OUT STD_LOGIC;
    hsclk0_rpllrefclklost : OUT STD_LOGIC;
    hsclk0_rpllrefclkmonitor : OUT STD_LOGIC;
    hsclk1_lcpllfbclklost : OUT STD_LOGIC;
    hsclk1_lcpllrefclklost : OUT STD_LOGIC;
    hsclk1_lcpllrefclkmonitor : OUT STD_LOGIC;
    hsclk1_rpllfbclklost : OUT STD_LOGIC;
    hsclk1_rpllrefclklost : OUT STD_LOGIC;
    hsclk1_rpllrefclkmonitor : OUT STD_LOGIC;
    bgbypassb : IN STD_LOGIC;
    bgmonitorenb : IN STD_LOGIC;
    bgpdb : IN STD_LOGIC;
    bgrcalovrdenb : IN STD_LOGIC;
    debugtraceclk : IN STD_LOGIC;
    debugtraceready : IN STD_LOGIC;
    hsclk0_lcpllclkrsvd0 : IN STD_LOGIC;
    hsclk0_lcpllclkrsvd1 : IN STD_LOGIC;
    hsclk0_lcpllpd : IN STD_LOGIC;
    hsclk0_rpllpd : IN STD_LOGIC;
    hsclk0_lcpllresetbypassmode : IN STD_LOGIC;
    hsclk0_lcpllsdmtoggle : IN STD_LOGIC;
    hsclk0_rpllclkrsvd0 : IN STD_LOGIC;
    hsclk0_rpllclkrsvd1 : IN STD_LOGIC;
    hsclk0_rpllresetbypassmode : IN STD_LOGIC;
    hsclk0_rpllsdmtoggle : IN STD_LOGIC;
    hsclk1_lcpllclkrsvd0 : IN STD_LOGIC;
    hsclk1_lcpllclkrsvd1 : IN STD_LOGIC;
    hsclk1_lcpllpd : IN STD_LOGIC;
    hsclk1_lcpllresetbypassmode : IN STD_LOGIC;
    hsclk1_lcpllsdmtoggle : IN STD_LOGIC;
    hsclk1_rpllclkrsvd0 : IN STD_LOGIC;
    hsclk1_rpllclkrsvd1 : IN STD_LOGIC;
    hsclk1_rpllpd : IN STD_LOGIC;
    hsclk1_rpllresetbypassmode : IN STD_LOGIC;
    hsclk1_rpllsdmtoggle : IN STD_LOGIC;
    refclk0_gtrefclkpd : IN STD_LOGIC;
    refclk0_clktestsig : IN STD_LOGIC;
    refclk1_gtrefclkpd : IN STD_LOGIC;
    refclk1_clktestsig : IN STD_LOGIC;
    trigackout0 : IN STD_LOGIC;
    trigin0 : IN STD_LOGIC;
    ubenable : IN STD_LOGIC;
    ubiolmbrst : IN STD_LOGIC;
    ubmbrst : IN STD_LOGIC;
    ubrxuart : IN STD_LOGIC;
    gpo : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    debugtracetdata : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    bgrcalovrd : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    hsclk0_lcpllrefclksel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    hsclk1_lcpllrefclksel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    hsclk0_rpllrefclksel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    hsclk1_rpllrefclksel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    hsclk0_lcpllfbdiv : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_lcpllrsvd0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_lcpllrsvd1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_rpllfbdiv : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_rpllrsvd0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_rpllrsvd1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_lcpllfbdiv : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_lcpllrsvd0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_lcpllrsvd1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_rpllfbdiv : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_rpllrsvd0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_rpllrsvd1 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    refclk0_gtrefclkpdint : OUT STD_LOGIC;
    refclk0_clktestsigint : OUT STD_LOGIC;
    refclk1_gtrefclkpdint : OUT STD_LOGIC;
    refclk1_clktestsigint : OUT STD_LOGIC;
    hsclk0_rxrecclkout0 : OUT STD_LOGIC;
    hsclk0_rxrecclkout1 : OUT STD_LOGIC;
    hsclk1_rxrecclkout0 : OUT STD_LOGIC;
    hsclk1_rxrecclkout1 : OUT STD_LOGIC;
    hsclk0_lcpllsdmdata : IN STD_LOGIC_VECTOR(25 DOWNTO 0);
    hsclk1_lcpllsdmdata : IN STD_LOGIC_VECTOR(25 DOWNTO 0);
    hsclk0_rpllsdmdata : IN STD_LOGIC_VECTOR(25 DOWNTO 0);
    hsclk1_rpllsdmdata : IN STD_LOGIC_VECTOR(25 DOWNTO 0);
    gpi : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ctrlrsvdin0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ctrlrsvdin1 : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
    ubintr : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    hsclk0_lcpllrsvdout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_lcpllrsvdout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk0_rpllrsvdout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    hsclk1_rpllrsvdout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    trigackin0 : OUT STD_LOGIC;
    trigout0 : OUT STD_LOGIC;
    ubinterrupt : OUT STD_LOGIC;
    ubtxuart : OUT STD_LOGIC;
    uncorrecterr : OUT STD_LOGIC;
    s0_axis_tlast : IN STD_LOGIC;
    s1_axis_tlast : IN STD_LOGIC;
    s2_axis_tlast : IN STD_LOGIC;
    s0_axis_tvalid : IN STD_LOGIC;
    s1_axis_tvalid : IN STD_LOGIC;
    s2_axis_tvalid : IN STD_LOGIC;
    s0_axis_tready : OUT STD_LOGIC;
    s1_axis_tready : OUT STD_LOGIC;
    s2_axis_tready : OUT STD_LOGIC;
    s0_axis_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s1_axis_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s2_axis_tdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    m0_axis_tready : IN STD_LOGIC;
    m1_axis_tready : IN STD_LOGIC;
    m2_axis_tready : IN STD_LOGIC;
    m0_axis_tlast : OUT STD_LOGIC;
    m1_axis_tlast : OUT STD_LOGIC;
    m2_axis_tlast : OUT STD_LOGIC;
    m0_axis_tvalid : OUT STD_LOGIC;
    m1_axis_tvalid : OUT STD_LOGIC;
    m2_axis_tvalid : OUT STD_LOGIC;
    m0_axis_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m1_axis_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    m2_axis_tdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    hsclk0_lcpllresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ctrlrsvdout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    hsclk1_lcpllresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    hsclk0_rpllresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    hsclk1_rpllresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    rxp : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxn : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txp : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txn : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch0_txheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch0_txsequence : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch0_gttxreset : IN STD_LOGIC;
    ch0_txprogdivreset : IN STD_LOGIC;
    ch0_txuserrdy : IN STD_LOGIC;
    ch0_txphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txcominit : IN STD_LOGIC;
    ch0_txcomsas : IN STD_LOGIC;
    ch0_txcomwake : IN STD_LOGIC;
    ch0_txdapicodeovrden : IN STD_LOGIC;
    ch0_txdapicodereset : IN STD_LOGIC;
    ch0_txdetectrx : IN STD_LOGIC;
    ch0_txlatclk : IN STD_LOGIC;
    ch0_txphdlytstclk : IN STD_LOGIC;
    ch0_txdlyalignreq : IN STD_LOGIC;
    ch0_txelecidle : IN STD_LOGIC;
    ch0_txinhibit : IN STD_LOGIC;
    ch0_txmldchaindone : IN STD_LOGIC;
    ch0_txmldchainreq : IN STD_LOGIC;
    ch0_txoneszeros : IN STD_LOGIC;
    ch0_txpausedelayalign : IN STD_LOGIC;
    ch0_txpcsresetmask : IN STD_LOGIC;
    ch0_txphalignreq : IN STD_LOGIC;
    ch0_txphdlypd : IN STD_LOGIC;
    ch0_txphdlyreset : IN STD_LOGIC;
    ch0_txphsetinitreq : IN STD_LOGIC;
    ch0_txphshift180 : IN STD_LOGIC;
    ch0_txpicodeovrden : IN STD_LOGIC;
    ch0_txpicodereset : IN STD_LOGIC;
    ch0_txpippmen : IN STD_LOGIC;
    ch0_txpisopd : IN STD_LOGIC;
    ch0_txpolarity : IN STD_LOGIC;
    ch0_txprbsforceerr : IN STD_LOGIC;
    ch0_txswing : IN STD_LOGIC;
    ch0_txsyncallin : IN STD_LOGIC;
    ch0_tx10gstat : OUT STD_LOGIC;
    ch0_txcomfinish : OUT STD_LOGIC;
    ch0_txdccdone : OUT STD_LOGIC;
    ch0_txdlyalignerr : OUT STD_LOGIC;
    ch0_txdlyalignprog : OUT STD_LOGIC;
    ch0_txphaligndone : OUT STD_LOGIC;
    ch0_txphalignerr : OUT STD_LOGIC;
    ch0_txphalignoutrsvd : OUT STD_LOGIC;
    ch0_txphdlyresetdone : OUT STD_LOGIC;
    ch0_txphsetinitdone : OUT STD_LOGIC;
    ch0_txphshift180done : OUT STD_LOGIC;
    ch0_txsyncdone : OUT STD_LOGIC;
    ch0_txbufstatus : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_txdeemph : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_txmstreset : IN STD_LOGIC;
    ch0_txmstresetdone : OUT STD_LOGIC;
    ch0_txmargin : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_txpmaresetmask : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_txprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_txdiffctrl : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txpippmstepsize : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txpostcursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txprecursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_txmaincursor : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch0_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_txdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_txprogdivresetdone : OUT STD_LOGIC;
    ch0_txpmaresetdone : OUT STD_LOGIC;
    ch0_txresetdone : OUT STD_LOGIC;
    ch0_txoutclk : OUT STD_LOGIC;
    ch0_txusrclk : IN STD_LOGIC;
    ch1_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch1_txheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch1_txsequence : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch1_gttxreset : IN STD_LOGIC;
    ch1_txprogdivreset : IN STD_LOGIC;
    ch1_txuserrdy : IN STD_LOGIC;
    ch1_txphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txcominit : IN STD_LOGIC;
    ch1_txcomsas : IN STD_LOGIC;
    ch1_txcomwake : IN STD_LOGIC;
    ch1_txdapicodeovrden : IN STD_LOGIC;
    ch1_txdapicodereset : IN STD_LOGIC;
    ch1_txdetectrx : IN STD_LOGIC;
    ch1_txlatclk : IN STD_LOGIC;
    ch1_txphdlytstclk : IN STD_LOGIC;
    ch1_txdlyalignreq : IN STD_LOGIC;
    ch1_txelecidle : IN STD_LOGIC;
    ch1_txinhibit : IN STD_LOGIC;
    ch1_txmldchaindone : IN STD_LOGIC;
    ch1_txmldchainreq : IN STD_LOGIC;
    ch1_txoneszeros : IN STD_LOGIC;
    ch1_txpausedelayalign : IN STD_LOGIC;
    ch1_txpcsresetmask : IN STD_LOGIC;
    ch1_txphalignreq : IN STD_LOGIC;
    ch1_txphdlypd : IN STD_LOGIC;
    ch1_txphdlyreset : IN STD_LOGIC;
    ch1_txphsetinitreq : IN STD_LOGIC;
    ch1_txphshift180 : IN STD_LOGIC;
    ch1_txpicodeovrden : IN STD_LOGIC;
    ch1_txpicodereset : IN STD_LOGIC;
    ch1_txpippmen : IN STD_LOGIC;
    ch1_txpisopd : IN STD_LOGIC;
    ch1_txpolarity : IN STD_LOGIC;
    ch1_txprbsforceerr : IN STD_LOGIC;
    ch1_txswing : IN STD_LOGIC;
    ch1_txsyncallin : IN STD_LOGIC;
    ch1_tx10gstat : OUT STD_LOGIC;
    ch1_txcomfinish : OUT STD_LOGIC;
    ch1_txdccdone : OUT STD_LOGIC;
    ch1_txdlyalignerr : OUT STD_LOGIC;
    ch1_txdlyalignprog : OUT STD_LOGIC;
    ch1_txphaligndone : OUT STD_LOGIC;
    ch1_txphalignerr : OUT STD_LOGIC;
    ch1_txphalignoutrsvd : OUT STD_LOGIC;
    ch1_txphdlyresetdone : OUT STD_LOGIC;
    ch1_txphsetinitdone : OUT STD_LOGIC;
    ch1_txphshift180done : OUT STD_LOGIC;
    ch1_txsyncdone : OUT STD_LOGIC;
    ch1_txbufstatus : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_txdeemph : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_txmstreset : IN STD_LOGIC;
    ch1_txmstresetdone : OUT STD_LOGIC;
    ch1_txmargin : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_txpmaresetmask : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_txprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch1_txdiffctrl : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txpippmstepsize : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txpostcursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txprecursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_txmaincursor : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch1_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_txdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_txprogdivresetdone : OUT STD_LOGIC;
    ch1_txpmaresetdone : OUT STD_LOGIC;
    ch1_txresetdone : OUT STD_LOGIC;
    ch1_txoutclk : OUT STD_LOGIC;
    ch1_txusrclk : IN STD_LOGIC;
    ch2_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch2_txheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch2_txsequence : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch2_gttxreset : IN STD_LOGIC;
    ch2_txprogdivreset : IN STD_LOGIC;
    ch2_txuserrdy : IN STD_LOGIC;
    ch2_txphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txcominit : IN STD_LOGIC;
    ch2_txcomsas : IN STD_LOGIC;
    ch2_txcomwake : IN STD_LOGIC;
    ch2_txdapicodeovrden : IN STD_LOGIC;
    ch2_txdapicodereset : IN STD_LOGIC;
    ch2_txdetectrx : IN STD_LOGIC;
    ch2_txlatclk : IN STD_LOGIC;
    ch2_txphdlytstclk : IN STD_LOGIC;
    ch2_txdlyalignreq : IN STD_LOGIC;
    ch2_txelecidle : IN STD_LOGIC;
    ch2_txinhibit : IN STD_LOGIC;
    ch2_txmldchaindone : IN STD_LOGIC;
    ch2_txmldchainreq : IN STD_LOGIC;
    ch2_txoneszeros : IN STD_LOGIC;
    ch2_txpausedelayalign : IN STD_LOGIC;
    ch2_txpcsresetmask : IN STD_LOGIC;
    ch2_txphalignreq : IN STD_LOGIC;
    ch2_txphdlypd : IN STD_LOGIC;
    ch2_txphdlyreset : IN STD_LOGIC;
    ch2_txphsetinitreq : IN STD_LOGIC;
    ch2_txphshift180 : IN STD_LOGIC;
    ch2_txpicodeovrden : IN STD_LOGIC;
    ch2_txpicodereset : IN STD_LOGIC;
    ch2_txpippmen : IN STD_LOGIC;
    ch2_txpisopd : IN STD_LOGIC;
    ch2_txpolarity : IN STD_LOGIC;
    ch2_txprbsforceerr : IN STD_LOGIC;
    ch2_txswing : IN STD_LOGIC;
    ch2_txsyncallin : IN STD_LOGIC;
    ch2_tx10gstat : OUT STD_LOGIC;
    ch2_txcomfinish : OUT STD_LOGIC;
    ch2_txdccdone : OUT STD_LOGIC;
    ch2_txdlyalignerr : OUT STD_LOGIC;
    ch2_txdlyalignprog : OUT STD_LOGIC;
    ch2_txphaligndone : OUT STD_LOGIC;
    ch2_txphalignerr : OUT STD_LOGIC;
    ch2_txphalignoutrsvd : OUT STD_LOGIC;
    ch2_txphdlyresetdone : OUT STD_LOGIC;
    ch2_txphsetinitdone : OUT STD_LOGIC;
    ch2_txphshift180done : OUT STD_LOGIC;
    ch2_txsyncdone : OUT STD_LOGIC;
    ch2_txbufstatus : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_txdeemph : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_txmstreset : IN STD_LOGIC;
    ch2_txmstresetdone : OUT STD_LOGIC;
    ch2_txmargin : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_txpmaresetmask : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_txprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch2_txdiffctrl : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txpippmstepsize : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txpostcursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txprecursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_txmaincursor : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch2_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_txdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_txprogdivresetdone : OUT STD_LOGIC;
    ch2_txpmaresetdone : OUT STD_LOGIC;
    ch2_txresetdone : OUT STD_LOGIC;
    ch2_txoutclk : OUT STD_LOGIC;
    ch2_txusrclk : IN STD_LOGIC;
    ch3_txdata : IN STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch3_txheader : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch3_txsequence : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch3_gttxreset : IN STD_LOGIC;
    ch3_txprogdivreset : IN STD_LOGIC;
    ch3_txuserrdy : IN STD_LOGIC;
    ch3_txphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txcominit : IN STD_LOGIC;
    ch3_txcomsas : IN STD_LOGIC;
    ch3_txcomwake : IN STD_LOGIC;
    ch3_txdapicodeovrden : IN STD_LOGIC;
    ch3_txdapicodereset : IN STD_LOGIC;
    ch3_txdetectrx : IN STD_LOGIC;
    ch3_txlatclk : IN STD_LOGIC;
    ch3_txphdlytstclk : IN STD_LOGIC;
    ch3_txdlyalignreq : IN STD_LOGIC;
    ch3_txelecidle : IN STD_LOGIC;
    ch3_txinhibit : IN STD_LOGIC;
    ch3_txmldchaindone : IN STD_LOGIC;
    ch3_txmldchainreq : IN STD_LOGIC;
    ch3_txoneszeros : IN STD_LOGIC;
    ch3_txpausedelayalign : IN STD_LOGIC;
    ch3_txpcsresetmask : IN STD_LOGIC;
    ch3_txphalignreq : IN STD_LOGIC;
    ch3_txphdlypd : IN STD_LOGIC;
    ch3_txphdlyreset : IN STD_LOGIC;
    ch3_txphsetinitreq : IN STD_LOGIC;
    ch3_txphshift180 : IN STD_LOGIC;
    ch3_txpicodeovrden : IN STD_LOGIC;
    ch3_txpicodereset : IN STD_LOGIC;
    ch3_txpippmen : IN STD_LOGIC;
    ch3_txpisopd : IN STD_LOGIC;
    ch3_txpolarity : IN STD_LOGIC;
    ch3_txprbsforceerr : IN STD_LOGIC;
    ch3_txswing : IN STD_LOGIC;
    ch3_txsyncallin : IN STD_LOGIC;
    ch3_tx10gstat : OUT STD_LOGIC;
    ch3_txcomfinish : OUT STD_LOGIC;
    ch3_txdccdone : OUT STD_LOGIC;
    ch3_txdlyalignerr : OUT STD_LOGIC;
    ch3_txdlyalignprog : OUT STD_LOGIC;
    ch3_txphaligndone : OUT STD_LOGIC;
    ch3_txphalignerr : OUT STD_LOGIC;
    ch3_txphalignoutrsvd : OUT STD_LOGIC;
    ch3_txphdlyresetdone : OUT STD_LOGIC;
    ch3_txphsetinitdone : OUT STD_LOGIC;
    ch3_txphshift180done : OUT STD_LOGIC;
    ch3_txsyncdone : OUT STD_LOGIC;
    ch3_txbufstatus : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txctrl0 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_txctrl1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_txdeemph : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_txmstreset : IN STD_LOGIC;
    ch3_txmstresetdone : OUT STD_LOGIC;
    ch3_txmargin : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_txpmaresetmask : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_txprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch3_txdiffctrl : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txpippmstepsize : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txpostcursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txprecursor : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_txmaincursor : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch3_txctrl2 : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_txdataextendrsvd : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_txrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_txprogdivresetdone : OUT STD_LOGIC;
    ch3_txpmaresetdone : OUT STD_LOGIC;
    ch3_txresetdone : OUT STD_LOGIC;
    ch3_txoutclk : OUT STD_LOGIC;
    ch3_txusrclk : IN STD_LOGIC;
    ch0_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch0_rxdatavalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxheader : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch0_rxgearboxslip : IN STD_LOGIC;
    ch0_rxlatclk : IN STD_LOGIC;
    ch0_gtrxreset : IN STD_LOGIC;
    ch0_rxprogdivreset : IN STD_LOGIC;
    ch0_rxuserrdy : IN STD_LOGIC;
    ch0_rxprogdivresetdone : OUT STD_LOGIC;
    ch0_rxpmaresetdone : OUT STD_LOGIC;
    ch0_rxresetdone : OUT STD_LOGIC;
    ch0_rx10gstat : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxbufstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_rxbyteisaligned : OUT STD_LOGIC;
    ch0_rxbyterealign : OUT STD_LOGIC;
    ch0_rxcdrhold : IN STD_LOGIC;
    ch0_rxcdrlock : OUT STD_LOGIC;
    ch0_rxcdrovrden : IN STD_LOGIC;
    ch0_rxcdrphdone : OUT STD_LOGIC;
    ch0_rxcdrreset : IN STD_LOGIC;
    ch0_rxchanbondseq : OUT STD_LOGIC;
    ch0_rxchanisaligned : OUT STD_LOGIC;
    ch0_rxchanrealign : OUT STD_LOGIC;
    ch0_rxchbondi : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_rxchbondo : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_rxclkcorcnt : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxcominitdet : OUT STD_LOGIC;
    ch0_rxcommadet : OUT STD_LOGIC;
    ch0_rxcomsasdet : OUT STD_LOGIC;
    ch0_rxcomwakedet : OUT STD_LOGIC;
    ch0_rxctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_rxctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_rxctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxctrl3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxdapicodeovrden : IN STD_LOGIC;
    ch0_rxdapicodereset : IN STD_LOGIC;
    ch0_rxdataextendrsvd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxdccdone : OUT STD_LOGIC;
    ch0_rxdlyalignerr : OUT STD_LOGIC;
    ch0_rxdlyalignprog : OUT STD_LOGIC;
    ch0_rxdlyalignreq : IN STD_LOGIC;
    ch0_rxelecidle : OUT STD_LOGIC;
    ch0_rxeqtraining : IN STD_LOGIC;
    ch0_rxfinealigndone : OUT STD_LOGIC;
    ch0_rxheadervalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxlpmen : IN STD_LOGIC;
    ch0_rxmldchaindone : IN STD_LOGIC;
    ch0_rxmldchainreq : IN STD_LOGIC;
    ch0_rxmlfinealignreq : IN STD_LOGIC;
    ch0_rxoobreset : IN STD_LOGIC;
    ch0_rxosintdone : OUT STD_LOGIC;
    ch0_rxosintstarted : OUT STD_LOGIC;
    ch0_rxosintstrobedone : OUT STD_LOGIC;
    ch0_rxosintstrobestarted : OUT STD_LOGIC;
    ch0_rxpcsresetmask : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch0_rxpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxphaligndone : OUT STD_LOGIC;
    ch0_rxphalignerr : OUT STD_LOGIC;
    ch0_rxphalignreq : IN STD_LOGIC;
    ch0_rxphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxphdlypd : IN STD_LOGIC;
    ch0_rxphdlyreset : IN STD_LOGIC;
    ch0_rxphdlyresetdone : OUT STD_LOGIC;
    ch0_rxphsetinitdone : OUT STD_LOGIC;
    ch0_rxphsetinitreq : IN STD_LOGIC;
    ch0_rxphshift180 : IN STD_LOGIC;
    ch0_rxphshift180done : OUT STD_LOGIC;
    ch0_rxpmaresetmask : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch0_rxpolarity : IN STD_LOGIC;
    ch0_rxprbscntreset : IN STD_LOGIC;
    ch0_rxprbserr : OUT STD_LOGIC;
    ch0_rxprbslocked : OUT STD_LOGIC;
    ch0_rxprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch0_rxresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxmstreset : IN STD_LOGIC;
    ch0_rxmstresetdone : OUT STD_LOGIC;
    ch0_rxslide : IN STD_LOGIC;
    ch0_rxsliderdy : OUT STD_LOGIC;
    ch0_rxstartofseq : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch0_rxstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_rxsyncallin : IN STD_LOGIC;
    ch0_rxsyncdone : OUT STD_LOGIC;
    ch0_rxtermination : IN STD_LOGIC;
    ch0_rxvalid : OUT STD_LOGIC;
    ch0_cdrbmcdrreq : IN STD_LOGIC;
    ch0_cdrfreqos : IN STD_LOGIC;
    ch0_cdrincpctrl : IN STD_LOGIC;
    ch0_cdrstepdir : IN STD_LOGIC;
    ch0_cdrstepsq : IN STD_LOGIC;
    ch0_cdrstepsx : IN STD_LOGIC;
    ch0_cfokovrdfinish : IN STD_LOGIC;
    ch0_cfokovrdpulse : IN STD_LOGIC;
    ch0_cfokovrdstart : IN STD_LOGIC;
    ch0_eyescanreset : IN STD_LOGIC;
    ch0_eyescantrigger : IN STD_LOGIC;
    ch0_eyescandataerror : OUT STD_LOGIC;
    ch0_cfokovrdrdy0 : OUT STD_LOGIC;
    ch0_cfokovrdrdy1 : OUT STD_LOGIC;
    ch0_rxoutclk : OUT STD_LOGIC;
    ch0_rxusrclk : IN STD_LOGIC;
    ch1_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch1_rxdatavalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxheader : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch1_rxgearboxslip : IN STD_LOGIC;
    ch1_rxlatclk : IN STD_LOGIC;
    ch1_gtrxreset : IN STD_LOGIC;
    ch1_rxprogdivreset : IN STD_LOGIC;
    ch1_rxuserrdy : IN STD_LOGIC;
    ch1_rxprogdivresetdone : OUT STD_LOGIC;
    ch1_rxpmaresetdone : OUT STD_LOGIC;
    ch1_rxresetdone : OUT STD_LOGIC;
    ch1_rx10gstat : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxbufstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_rxbyteisaligned : OUT STD_LOGIC;
    ch1_rxbyterealign : OUT STD_LOGIC;
    ch1_rxcdrhold : IN STD_LOGIC;
    ch1_rxcdrlock : OUT STD_LOGIC;
    ch1_rxcdrovrden : IN STD_LOGIC;
    ch1_rxcdrphdone : OUT STD_LOGIC;
    ch1_rxcdrreset : IN STD_LOGIC;
    ch1_rxchanbondseq : OUT STD_LOGIC;
    ch1_rxchanisaligned : OUT STD_LOGIC;
    ch1_rxchanrealign : OUT STD_LOGIC;
    ch1_rxchbondi : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_rxchbondo : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_rxclkcorcnt : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxcominitdet : OUT STD_LOGIC;
    ch1_rxcommadet : OUT STD_LOGIC;
    ch1_rxcomsasdet : OUT STD_LOGIC;
    ch1_rxcomwakedet : OUT STD_LOGIC;
    ch1_rxctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_rxctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_rxctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxctrl3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxdapicodeovrden : IN STD_LOGIC;
    ch1_rxdapicodereset : IN STD_LOGIC;
    ch1_rxdataextendrsvd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxdccdone : OUT STD_LOGIC;
    ch1_rxdlyalignerr : OUT STD_LOGIC;
    ch1_rxdlyalignprog : OUT STD_LOGIC;
    ch1_rxdlyalignreq : IN STD_LOGIC;
    ch1_rxelecidle : OUT STD_LOGIC;
    ch1_rxeqtraining : IN STD_LOGIC;
    ch1_rxfinealigndone : OUT STD_LOGIC;
    ch1_rxheadervalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxlpmen : IN STD_LOGIC;
    ch1_rxmldchaindone : IN STD_LOGIC;
    ch1_rxmldchainreq : IN STD_LOGIC;
    ch1_rxmlfinealignreq : IN STD_LOGIC;
    ch1_rxoobreset : IN STD_LOGIC;
    ch1_rxosintdone : OUT STD_LOGIC;
    ch1_rxosintstarted : OUT STD_LOGIC;
    ch1_rxosintstrobedone : OUT STD_LOGIC;
    ch1_rxosintstrobestarted : OUT STD_LOGIC;
    ch1_rxpcsresetmask : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch1_rxpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxphaligndone : OUT STD_LOGIC;
    ch1_rxphalignerr : OUT STD_LOGIC;
    ch1_rxphalignreq : IN STD_LOGIC;
    ch1_rxphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxphdlypd : IN STD_LOGIC;
    ch1_rxphdlyreset : IN STD_LOGIC;
    ch1_rxphdlyresetdone : OUT STD_LOGIC;
    ch1_rxphsetinitdone : OUT STD_LOGIC;
    ch1_rxphsetinitreq : IN STD_LOGIC;
    ch1_rxphshift180 : IN STD_LOGIC;
    ch1_rxphshift180done : OUT STD_LOGIC;
    ch1_rxpmaresetmask : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch1_rxpolarity : IN STD_LOGIC;
    ch1_rxprbscntreset : IN STD_LOGIC;
    ch1_rxprbserr : OUT STD_LOGIC;
    ch1_rxprbslocked : OUT STD_LOGIC;
    ch1_rxprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch1_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch1_rxresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxmstreset : IN STD_LOGIC;
    ch1_rxmstresetdone : OUT STD_LOGIC;
    ch1_rxslide : IN STD_LOGIC;
    ch1_rxsliderdy : OUT STD_LOGIC;
    ch1_rxstartofseq : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch1_rxstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_rxsyncallin : IN STD_LOGIC;
    ch1_rxsyncdone : OUT STD_LOGIC;
    ch1_rxtermination : IN STD_LOGIC;
    ch1_rxvalid : OUT STD_LOGIC;
    ch1_cdrbmcdrreq : IN STD_LOGIC;
    ch1_cdrfreqos : IN STD_LOGIC;
    ch1_cdrincpctrl : IN STD_LOGIC;
    ch1_cdrstepdir : IN STD_LOGIC;
    ch1_cdrstepsq : IN STD_LOGIC;
    ch1_cdrstepsx : IN STD_LOGIC;
    ch1_cfokovrdfinish : IN STD_LOGIC;
    ch1_cfokovrdpulse : IN STD_LOGIC;
    ch1_cfokovrdstart : IN STD_LOGIC;
    ch1_eyescanreset : IN STD_LOGIC;
    ch1_eyescantrigger : IN STD_LOGIC;
    ch1_eyescandataerror : OUT STD_LOGIC;
    ch1_cfokovrdrdy0 : OUT STD_LOGIC;
    ch1_cfokovrdrdy1 : OUT STD_LOGIC;
    ch1_rxoutclk : OUT STD_LOGIC;
    ch1_rxusrclk : IN STD_LOGIC;
    ch2_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch2_rxdatavalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxheader : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch2_rxgearboxslip : IN STD_LOGIC;
    ch2_rxlatclk : IN STD_LOGIC;
    ch2_gtrxreset : IN STD_LOGIC;
    ch2_rxprogdivreset : IN STD_LOGIC;
    ch2_rxuserrdy : IN STD_LOGIC;
    ch2_rxprogdivresetdone : OUT STD_LOGIC;
    ch2_rxpmaresetdone : OUT STD_LOGIC;
    ch2_rxresetdone : OUT STD_LOGIC;
    ch2_rx10gstat : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxbufstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_rxbyteisaligned : OUT STD_LOGIC;
    ch2_rxbyterealign : OUT STD_LOGIC;
    ch2_rxcdrhold : IN STD_LOGIC;
    ch2_rxcdrlock : OUT STD_LOGIC;
    ch2_rxcdrovrden : IN STD_LOGIC;
    ch2_rxcdrphdone : OUT STD_LOGIC;
    ch2_rxcdrreset : IN STD_LOGIC;
    ch2_rxchanbondseq : OUT STD_LOGIC;
    ch2_rxchanisaligned : OUT STD_LOGIC;
    ch2_rxchanrealign : OUT STD_LOGIC;
    ch2_rxchbondi : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_rxchbondo : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_rxclkcorcnt : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxcominitdet : OUT STD_LOGIC;
    ch2_rxcommadet : OUT STD_LOGIC;
    ch2_rxcomsasdet : OUT STD_LOGIC;
    ch2_rxcomwakedet : OUT STD_LOGIC;
    ch2_rxctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_rxctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_rxctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxctrl3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxdapicodeovrden : IN STD_LOGIC;
    ch2_rxdapicodereset : IN STD_LOGIC;
    ch2_rxdataextendrsvd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxdccdone : OUT STD_LOGIC;
    ch2_rxdlyalignerr : OUT STD_LOGIC;
    ch2_rxdlyalignprog : OUT STD_LOGIC;
    ch2_rxdlyalignreq : IN STD_LOGIC;
    ch2_rxelecidle : OUT STD_LOGIC;
    ch2_rxeqtraining : IN STD_LOGIC;
    ch2_rxfinealigndone : OUT STD_LOGIC;
    ch2_rxheadervalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxlpmen : IN STD_LOGIC;
    ch2_rxmldchaindone : IN STD_LOGIC;
    ch2_rxmldchainreq : IN STD_LOGIC;
    ch2_rxmlfinealignreq : IN STD_LOGIC;
    ch2_rxoobreset : IN STD_LOGIC;
    ch2_rxosintdone : OUT STD_LOGIC;
    ch2_rxosintstarted : OUT STD_LOGIC;
    ch2_rxosintstrobedone : OUT STD_LOGIC;
    ch2_rxosintstrobestarted : OUT STD_LOGIC;
    ch2_rxpcsresetmask : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch2_rxpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxphaligndone : OUT STD_LOGIC;
    ch2_rxphalignerr : OUT STD_LOGIC;
    ch2_rxphalignreq : IN STD_LOGIC;
    ch2_rxphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxphdlypd : IN STD_LOGIC;
    ch2_rxphdlyreset : IN STD_LOGIC;
    ch2_rxphdlyresetdone : OUT STD_LOGIC;
    ch2_rxphsetinitdone : OUT STD_LOGIC;
    ch2_rxphsetinitreq : IN STD_LOGIC;
    ch2_rxphshift180 : IN STD_LOGIC;
    ch2_rxphshift180done : OUT STD_LOGIC;
    ch2_rxpmaresetmask : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch2_rxpolarity : IN STD_LOGIC;
    ch2_rxprbscntreset : IN STD_LOGIC;
    ch2_rxprbserr : OUT STD_LOGIC;
    ch2_rxprbslocked : OUT STD_LOGIC;
    ch2_rxprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch2_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch2_rxresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxmstreset : IN STD_LOGIC;
    ch2_rxmstresetdone : OUT STD_LOGIC;
    ch2_rxslide : IN STD_LOGIC;
    ch2_rxsliderdy : OUT STD_LOGIC;
    ch2_rxstartofseq : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch2_rxstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_rxsyncallin : IN STD_LOGIC;
    ch2_rxsyncdone : OUT STD_LOGIC;
    ch2_rxtermination : IN STD_LOGIC;
    ch2_rxvalid : OUT STD_LOGIC;
    ch2_cdrbmcdrreq : IN STD_LOGIC;
    ch2_cdrfreqos : IN STD_LOGIC;
    ch2_cdrincpctrl : IN STD_LOGIC;
    ch2_cdrstepdir : IN STD_LOGIC;
    ch2_cdrstepsq : IN STD_LOGIC;
    ch2_cdrstepsx : IN STD_LOGIC;
    ch2_cfokovrdfinish : IN STD_LOGIC;
    ch2_cfokovrdpulse : IN STD_LOGIC;
    ch2_cfokovrdstart : IN STD_LOGIC;
    ch2_eyescanreset : IN STD_LOGIC;
    ch2_eyescantrigger : IN STD_LOGIC;
    ch2_eyescandataerror : OUT STD_LOGIC;
    ch2_cfokovrdrdy0 : OUT STD_LOGIC;
    ch2_cfokovrdrdy1 : OUT STD_LOGIC;
    ch2_rxoutclk : OUT STD_LOGIC;
    ch2_rxusrclk : IN STD_LOGIC;
    ch3_rxdata : OUT STD_LOGIC_VECTOR(127 DOWNTO 0);
    ch3_rxdatavalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxheader : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    ch3_rxgearboxslip : IN STD_LOGIC;
    ch3_rxlatclk : IN STD_LOGIC;
    ch3_gtrxreset : IN STD_LOGIC;
    ch3_rxprogdivreset : IN STD_LOGIC;
    ch3_rxuserrdy : IN STD_LOGIC;
    ch3_rxprogdivresetdone : OUT STD_LOGIC;
    ch3_rxpmaresetdone : OUT STD_LOGIC;
    ch3_rxresetdone : OUT STD_LOGIC;
    ch3_rx10gstat : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxbufstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_rxbyteisaligned : OUT STD_LOGIC;
    ch3_rxbyterealign : OUT STD_LOGIC;
    ch3_rxcdrhold : IN STD_LOGIC;
    ch3_rxcdrlock : OUT STD_LOGIC;
    ch3_rxcdrovrden : IN STD_LOGIC;
    ch3_rxcdrphdone : OUT STD_LOGIC;
    ch3_rxcdrreset : IN STD_LOGIC;
    ch3_rxchanbondseq : OUT STD_LOGIC;
    ch3_rxchanisaligned : OUT STD_LOGIC;
    ch3_rxchanrealign : OUT STD_LOGIC;
    ch3_rxchbondi : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_rxchbondo : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_rxclkcorcnt : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxcominitdet : OUT STD_LOGIC;
    ch3_rxcommadet : OUT STD_LOGIC;
    ch3_rxcomsasdet : OUT STD_LOGIC;
    ch3_rxcomwakedet : OUT STD_LOGIC;
    ch3_rxctrl0 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_rxctrl1 : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_rxctrl2 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxctrl3 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxdapicodeovrden : IN STD_LOGIC;
    ch3_rxdapicodereset : IN STD_LOGIC;
    ch3_rxdataextendrsvd : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxdccdone : OUT STD_LOGIC;
    ch3_rxdlyalignerr : OUT STD_LOGIC;
    ch3_rxdlyalignprog : OUT STD_LOGIC;
    ch3_rxdlyalignreq : IN STD_LOGIC;
    ch3_rxelecidle : OUT STD_LOGIC;
    ch3_rxeqtraining : IN STD_LOGIC;
    ch3_rxfinealigndone : OUT STD_LOGIC;
    ch3_rxheadervalid : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxlpmen : IN STD_LOGIC;
    ch3_rxmldchaindone : IN STD_LOGIC;
    ch3_rxmldchainreq : IN STD_LOGIC;
    ch3_rxmlfinealignreq : IN STD_LOGIC;
    ch3_rxoobreset : IN STD_LOGIC;
    ch3_rxosintdone : OUT STD_LOGIC;
    ch3_rxosintstarted : OUT STD_LOGIC;
    ch3_rxosintstrobedone : OUT STD_LOGIC;
    ch3_rxosintstrobestarted : OUT STD_LOGIC;
    ch3_rxpcsresetmask : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    ch3_rxpd : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxphaligndone : OUT STD_LOGIC;
    ch3_rxphalignerr : OUT STD_LOGIC;
    ch3_rxphalignreq : IN STD_LOGIC;
    ch3_rxphalignresetmask : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxphdlypd : IN STD_LOGIC;
    ch3_rxphdlyreset : IN STD_LOGIC;
    ch3_rxphdlyresetdone : OUT STD_LOGIC;
    ch3_rxphsetinitdone : OUT STD_LOGIC;
    ch3_rxphsetinitreq : IN STD_LOGIC;
    ch3_rxphshift180 : IN STD_LOGIC;
    ch3_rxphshift180done : OUT STD_LOGIC;
    ch3_rxpmaresetmask : IN STD_LOGIC_VECTOR(6 DOWNTO 0);
    ch3_rxpolarity : IN STD_LOGIC;
    ch3_rxprbscntreset : IN STD_LOGIC;
    ch3_rxprbserr : OUT STD_LOGIC;
    ch3_rxprbslocked : OUT STD_LOGIC;
    ch3_rxprbssel : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch3_rxrate : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    ch3_rxresetmode : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxmstreset : IN STD_LOGIC;
    ch3_rxmstresetdone : OUT STD_LOGIC;
    ch3_rxslide : IN STD_LOGIC;
    ch3_rxsliderdy : OUT STD_LOGIC;
    ch3_rxstartofseq : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    ch3_rxstatus : OUT STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_rxsyncallin : IN STD_LOGIC;
    ch3_rxsyncdone : OUT STD_LOGIC;
    ch3_rxtermination : IN STD_LOGIC;
    ch3_rxvalid : OUT STD_LOGIC;
    ch3_cdrbmcdrreq : IN STD_LOGIC;
    ch3_cdrfreqos : IN STD_LOGIC;
    ch3_cdrincpctrl : IN STD_LOGIC;
    ch3_cdrstepdir : IN STD_LOGIC;
    ch3_cdrstepsq : IN STD_LOGIC;
    ch3_cdrstepsx : IN STD_LOGIC;
    ch3_cfokovrdfinish : IN STD_LOGIC;
    ch3_cfokovrdpulse : IN STD_LOGIC;
    ch3_cfokovrdstart : IN STD_LOGIC;
    ch3_eyescanreset : IN STD_LOGIC;
    ch3_eyescantrigger : IN STD_LOGIC;
    ch3_eyescandataerror : OUT STD_LOGIC;
    ch3_cfokovrdrdy0 : OUT STD_LOGIC;
    ch3_cfokovrdrdy1 : OUT STD_LOGIC;
    ch3_rxoutclk : OUT STD_LOGIC;
    ch3_rxusrclk : IN STD_LOGIC;
    ch0_bufgtce : OUT STD_LOGIC;
    ch0_bufgtrst : OUT STD_LOGIC;
    ch0_bufgtcemask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_bufgtrstmask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch0_bufgtdiv : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    ch0_clkrsvd0 : IN STD_LOGIC;
    ch0_clkrsvd1 : IN STD_LOGIC;
    ch0_dmonitorclk : IN STD_LOGIC;
    ch0_phyesmadaptsave : IN STD_LOGIC;
    ch0_iloresetmask : IN STD_LOGIC;
    ch0_loopback : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch0_dmonfiforeset : IN STD_LOGIC;
    ch0_pcsrsvdin : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_gtrsvd : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_tstin : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    ch0_pcsrsvdout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_pinrsvdas : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch0_dmonitoroutclk : OUT STD_LOGIC;
    ch0_resetexception : OUT STD_LOGIC;
    ch0_dmonitorout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    ch0_phyready : OUT STD_LOGIC;
    ch0_hsdppcsreset : IN STD_LOGIC;
    ch1_bufgtce : OUT STD_LOGIC;
    ch1_bufgtrst : OUT STD_LOGIC;
    ch1_bufgtcemask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch1_bufgtrstmask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch1_bufgtdiv : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    ch1_clkrsvd0 : IN STD_LOGIC;
    ch1_clkrsvd1 : IN STD_LOGIC;
    ch1_dmonitorclk : IN STD_LOGIC;
    ch1_phyesmadaptsave : IN STD_LOGIC;
    ch1_iloresetmask : IN STD_LOGIC;
    ch1_loopback : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch1_dmonfiforeset : IN STD_LOGIC;
    ch1_pcsrsvdin : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_gtrsvd : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_tstin : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    ch1_pcsrsvdout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_pinrsvdas : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch1_dmonitoroutclk : OUT STD_LOGIC;
    ch1_resetexception : OUT STD_LOGIC;
    ch1_dmonitorout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    ch1_phyready : OUT STD_LOGIC;
    ch1_hsdppcsreset : IN STD_LOGIC;
    ch2_bufgtce : OUT STD_LOGIC;
    ch2_bufgtrst : OUT STD_LOGIC;
    ch2_bufgtcemask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch2_bufgtrstmask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch2_bufgtdiv : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    ch2_clkrsvd0 : IN STD_LOGIC;
    ch2_clkrsvd1 : IN STD_LOGIC;
    ch2_dmonitorclk : IN STD_LOGIC;
    ch2_phyesmadaptsave : IN STD_LOGIC;
    ch2_iloresetmask : IN STD_LOGIC;
    ch2_loopback : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch2_dmonfiforeset : IN STD_LOGIC;
    ch2_pcsrsvdin : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_gtrsvd : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_tstin : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    ch2_pcsrsvdout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_pinrsvdas : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch2_dmonitoroutclk : OUT STD_LOGIC;
    ch2_resetexception : OUT STD_LOGIC;
    ch2_dmonitorout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    ch2_phyready : OUT STD_LOGIC;
    ch2_hsdppcsreset : IN STD_LOGIC;
    ch3_bufgtce : OUT STD_LOGIC;
    ch3_bufgtrst : OUT STD_LOGIC;
    ch3_bufgtcemask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch3_bufgtrstmask : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    ch3_bufgtdiv : OUT STD_LOGIC_VECTOR(11 DOWNTO 0);
    ch3_clkrsvd0 : IN STD_LOGIC;
    ch3_clkrsvd1 : IN STD_LOGIC;
    ch3_dmonitorclk : IN STD_LOGIC;
    ch3_phyesmadaptsave : IN STD_LOGIC;
    ch3_iloresetmask : IN STD_LOGIC;
    ch3_loopback : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    ch3_dmonfiforeset : IN STD_LOGIC;
    ch3_pcsrsvdin : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_gtrsvd : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_tstin : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    ch3_pcsrsvdout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_pinrsvdas : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ch3_dmonitoroutclk : OUT STD_LOGIC;
    ch3_resetexception : OUT STD_LOGIC;
    ch3_dmonitorout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    ch3_phyready : OUT STD_LOGIC;
    ch3_hsdppcsreset : IN STD_LOGIC;
    resetdone_northin : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    resetdone_southin : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    resetdone_northout : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    resetdone_southout : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    txpinorthin : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpinorthin : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpisouthin : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpisouthin : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    pipenorthin : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    pipesouthin : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    txpinorthout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    txpisouthout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpinorthout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    rxpisouthout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    pipenorthout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    pipesouthout : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    GT_REFCLK0 : IN STD_LOGIC
  );
END COMPONENT;
  signal pcie4_cfg_control_1_bus_number : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie4_cfg_control_1_config_space_enable : STD_LOGIC;
  signal pcie4_cfg_control_1_ds_bus_number : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie4_cfg_control_1_ds_device_number : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie4_cfg_control_1_ds_port_number : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie4_cfg_control_1_dsn : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal pcie4_cfg_control_1_err_cor_in : STD_LOGIC;
  signal pcie4_cfg_control_1_err_uncor_in : STD_LOGIC;
  signal pcie4_cfg_control_1_flr_done : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie4_cfg_control_1_flr_in_process : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie4_cfg_control_1_hot_reset_in : STD_LOGIC;
  signal pcie4_cfg_control_1_hot_reset_out : STD_LOGIC;
  signal pcie4_cfg_control_1_link_training_enable : STD_LOGIC;
  signal pcie4_cfg_control_1_power_state_change_ack : STD_LOGIC;
  signal pcie4_cfg_control_1_power_state_change_interrupt : STD_LOGIC;
  signal pcie4_cfg_control_1_req_pm_transition_l23_ready : STD_LOGIC;
  signal pcie4_cfg_control_1_vf_flr_done : STD_LOGIC_VECTOR ( 0 to 0 );
  signal pcie4_cfg_control_1_vf_flr_func_num : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie4_cfg_control_1_vf_flr_in_process : STD_LOGIC_VECTOR ( 251 downto 0 );
  signal pcie4_cfg_external_msix_1_address : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal pcie4_cfg_external_msix_1_data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pcie4_cfg_external_msix_1_enable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie4_cfg_external_msix_1_fail : STD_LOGIC;
  signal pcie4_cfg_external_msix_1_function_number : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie4_cfg_external_msix_1_int_vector : STD_LOGIC;
  signal pcie4_cfg_external_msix_1_mask : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie4_cfg_external_msix_1_sent : STD_LOGIC;
  signal pcie4_cfg_external_msix_1_vec_pending : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie4_cfg_external_msix_1_vec_pending_status : STD_LOGIC_VECTOR ( 0 to 0 );
  signal pcie4_cfg_external_msix_1_vf_enable : STD_LOGIC_VECTOR ( 251 downto 0 );
  signal pcie4_cfg_external_msix_1_vf_mask : STD_LOGIC_VECTOR ( 251 downto 0 );
  signal pcie4_cfg_interrupt_1_INTx_VECTOR : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie4_cfg_interrupt_1_PENDING : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie4_cfg_interrupt_1_SENT : STD_LOGIC;
  signal pcie4_cfg_mgmt_1_ADDR : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal pcie4_cfg_mgmt_1_BYTE_EN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie4_cfg_mgmt_1_DEBUG_ACCESS : STD_LOGIC;
  signal pcie4_cfg_mgmt_1_FUNCTION_NUMBER : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie4_cfg_mgmt_1_READ_DATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pcie4_cfg_mgmt_1_READ_EN : STD_LOGIC;
  signal pcie4_cfg_mgmt_1_READ_WRITE_DONE : STD_LOGIC;
  signal pcie4_cfg_mgmt_1_WRITE_DATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pcie4_cfg_mgmt_1_WRITE_EN : STD_LOGIC;
  signal pcie4_cfg_pm_1_pm_aspm_l1entry_reject : STD_LOGIC;
  signal pcie4_cfg_pm_1_pm_aspm_tx_l0s_entry_disable : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtce : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtcemask : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtdiv : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtrst : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtrstmask : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_0_GT_Serial_RX_RXN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_0_GT_Serial_RX_RXP : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_0_GT_Serial_TX_TXN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_0_GT_Serial_TX_TXP : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_0_ch0_phyready : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_ch0_phystatus : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_ch0_rxoutclk : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_ch0_txoutclk : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_ch1_phyready : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_ch1_phystatus : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_ch2_phyready : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_ch2_phystatus : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_ch3_phyready : STD_LOGIC;
  signal pcie_versal_0_gt_quad_0_ch3_phystatus : STD_LOGIC;
  signal pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_PIPENORTHIN : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_PIPESOUTHOUT : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RESETDONE_NORTHIN : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RESETDONE_SOUTHOUT : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RXPINORTHIN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RXPISOUTHOUT : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_TXPINORTHIN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_TXPISOUTHOUT : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_Serial_RX_RXN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_Serial_RX_RXP : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_Serial_TX_TXN : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_1_GT_Serial_TX_TXP : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_gt_quad_1_ch0_phyready : STD_LOGIC;
  signal pcie_versal_0_gt_quad_1_ch0_phystatus : STD_LOGIC;
  signal pcie_versal_0_gt_quad_1_ch1_phyready : STD_LOGIC;
  signal pcie_versal_0_gt_quad_1_ch1_phystatus : STD_LOGIC;
  signal pcie_versal_0_gt_quad_1_ch2_phyready : STD_LOGIC;
  signal pcie_versal_0_gt_quad_1_ch2_phystatus : STD_LOGIC;
  signal pcie_versal_0_gt_quad_1_ch3_phyready : STD_LOGIC;
  signal pcie_versal_0_gt_quad_1_ch3_phystatus : STD_LOGIC;
  signal pcie_versal_0_m_axis_cq_TDATA : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal pcie_versal_0_m_axis_cq_TKEEP : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_m_axis_cq_TLAST : STD_LOGIC;
  signal pcie_versal_0_m_axis_cq_TREADY : STD_LOGIC;
  signal pcie_versal_0_m_axis_cq_TUSER : STD_LOGIC_VECTOR ( 228 downto 0 );
  signal pcie_versal_0_m_axis_cq_TVALID : STD_LOGIC;
  signal pcie_versal_0_m_axis_rc_TDATA : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal pcie_versal_0_m_axis_rc_TKEEP : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_m_axis_rc_TLAST : STD_LOGIC;
  signal pcie_versal_0_m_axis_rc_TREADY : STD_LOGIC;
  signal pcie_versal_0_m_axis_rc_TUSER : STD_LOGIC_VECTOR ( 160 downto 0 );
  signal pcie_versal_0_m_axis_rc_TVALID : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_fc_CPLD : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal pcie_versal_0_pcie4_cfg_fc_CPLH : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_pcie4_cfg_fc_NPD : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal pcie_versal_0_pcie4_cfg_fc_NPH : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_pcie4_cfg_fc_PD : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal pcie_versal_0_pcie4_cfg_fc_PH : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_pcie4_cfg_fc_SEL : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_pcie4_cfg_mesg_rcvd_recd : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_mesg_rcvd_recd_data : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_pcie4_cfg_mesg_rcvd_recd_type : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT_DATA : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT_DONE : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT_TYPE : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_cq_np_req : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_cq_np_req_count : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_current_speed : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_err_cor_out : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_err_fatal_out : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_err_nonfatal_out : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_function_power_state : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_function_status : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_link_power_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_local_error_out : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_local_error_valid : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_ltssm_state : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_max_payload : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_max_read_req : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_negotiated_width : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_obff_enable : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_phy_link_down : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_phy_link_status : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_pl_status_change : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_rcb_status : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_rq_seq_num0 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_rq_seq_num1 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_rq_seq_num_vld0 : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_rq_seq_num_vld1 : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_rq_tag0 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_rq_tag1 : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_rq_tag_av : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_rq_tag_vld0 : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_rq_tag_vld1 : STD_LOGIC;
  signal pcie_versal_0_pcie4_cfg_status_rx_pm_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_tfc_npd_av : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_tfc_nph_av : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_tph_requester_enable : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_tph_st_mode : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_tx_pm_state : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_vf_power_state : STD_LOGIC_VECTOR ( 755 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_vf_status : STD_LOGIC_VECTOR ( 503 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_vf_tph_requester_enable : STD_LOGIC_VECTOR ( 251 downto 0 );
  signal pcie_versal_0_pcie4_cfg_status_vf_tph_st_mode : STD_LOGIC_VECTOR ( 755 downto 0 );
  signal pcie_versal_0_pcie_ltssm_state : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_eyescandataerror : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxbyteisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxbyterealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxcdrlock : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxcdrphdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxchanbondseq : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxchanisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxchanrealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxcominitdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxcommadet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxcomsasdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxcomwakedet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxfinealigndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxosintdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxosintstarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxosintstrobedone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxosintstrobestarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxpolarity : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxprbserr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxprbslocked : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxsliderdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX0_ch_rxsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxtermination : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX0_ch_rxvalid : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_eyescandataerror : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxbyteisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxbyterealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxcdrlock : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxcdrphdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxchanbondseq : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxchanisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxchanrealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxcominitdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxcommadet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxcomsasdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxcomwakedet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxfinealigndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxosintdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxosintstarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxosintstrobedone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxosintstrobestarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxpolarity : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxprbserr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxprbslocked : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxsliderdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX1_ch_rxsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxtermination : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX1_ch_rxvalid : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_eyescandataerror : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxbyteisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxbyterealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxcdrlock : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxcdrphdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxchanbondseq : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxchanisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxchanrealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxcominitdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxcommadet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxcomsasdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxcomwakedet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxfinealigndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxosintdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxosintstarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxosintstrobedone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxosintstrobestarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxpolarity : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxprbserr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxprbslocked : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxsliderdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX2_ch_rxsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxtermination : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX2_ch_rxvalid : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_eyescandataerror : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxbyteisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxbyterealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxcdrlock : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxcdrphdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxchanbondseq : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxchanisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxchanrealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxcominitdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxcommadet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxcomsasdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxcomwakedet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxfinealigndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxosintdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxosintstarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxosintstrobedone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxosintstrobestarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxpolarity : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxprbserr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxprbslocked : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxsliderdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX3_ch_rxsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxtermination : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX3_ch_rxvalid : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_eyescandataerror : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxbyteisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxbyterealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxcdrlock : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxcdrphdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxchanbondseq : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxchanisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxchanrealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxcominitdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxcommadet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxcomsasdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxcomwakedet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxfinealigndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxosintdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxosintstarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxosintstrobedone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxosintstrobestarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxpolarity : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxprbserr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxprbslocked : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxsliderdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX4_ch_rxsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxtermination : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX4_ch_rxvalid : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_eyescandataerror : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxbyteisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxbyterealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxcdrlock : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxcdrphdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxchanbondseq : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxchanisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxchanrealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxcominitdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxcommadet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxcomsasdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxcomwakedet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxfinealigndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxosintdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxosintstarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxosintstrobedone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxosintstrobestarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxpolarity : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxprbserr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxprbslocked : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxsliderdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX5_ch_rxsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxtermination : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX5_ch_rxvalid : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_eyescandataerror : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxbyteisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxbyterealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxcdrlock : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxcdrphdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxchanbondseq : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxchanisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxchanrealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxcominitdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxcommadet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxcomsasdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxcomwakedet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxfinealigndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxosintdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxosintstarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxosintstrobedone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxosintstrobestarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxpolarity : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxprbserr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxprbslocked : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxsliderdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX6_ch_rxsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxtermination : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX6_ch_rxvalid : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_eyescandataerror : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rx10gstat : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxbufstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxbyteisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxbyterealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxcdrlock : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxcdrphdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxchanbondseq : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxchanisaligned : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxchanrealign : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxchbondo : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxclkcorcnt : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxcominitdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxcommadet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxcomsasdet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxcomwakedet : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxctrl3 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxdataextendrsvd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxdatavalid : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxfinealigndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxheader : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxosintdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxosintstarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxosintstrobedone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxosintstrobestarted : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxpolarity : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxprbserr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxprbslocked : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxsliderdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxstartofseq : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxstatus : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_RX7_ch_rxsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxtermination : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_RX7_ch_rxvalid : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_tx10gstat : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txcomfinish : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txdetectrx : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txphaligndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txphalignoutrsvd : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX0_ch_txresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txswing : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX0_ch_txuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_tx10gstat : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txcomfinish : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txdetectrx : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txphaligndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txphalignoutrsvd : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX1_ch_txresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txswing : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX1_ch_txuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_tx10gstat : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txcomfinish : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txdetectrx : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txphaligndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txphalignoutrsvd : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX2_ch_txresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txswing : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX2_ch_txuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_tx10gstat : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txcomfinish : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txdetectrx : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txphaligndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txphalignoutrsvd : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX3_ch_txresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txswing : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX3_ch_txuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_tx10gstat : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txcomfinish : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txdetectrx : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txphaligndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txphalignoutrsvd : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX4_ch_txresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txswing : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX4_ch_txuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_tx10gstat : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txcomfinish : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txdetectrx : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txphaligndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txphalignoutrsvd : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX5_ch_txresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txswing : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX5_ch_txuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_tx10gstat : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txcomfinish : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txdetectrx : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txphaligndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txphalignoutrsvd : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX6_ch_txresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txswing : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX6_ch_txuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_tx10gstat : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txbufstatus : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txcomfinish : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txctrl0 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txctrl1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txctrl2 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txdccdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txdeemph : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txdetectrx : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txdlyalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txdlyalignprog : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txelecidle : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txmaincursor : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txmargin : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txmstresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txpd : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txphaligndone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txphalignerr : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txphalignoutrsvd : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txphdlyresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txphsetinitdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txphshift180done : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txpmaresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txpostcursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txprecursor : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txprogdivresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txrate : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_GT_TX7_ch_txresetdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txswing : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txsyncdone : STD_LOGIC;
  signal pcie_versal_0_phy_GT_TX7_ch_txuserrdy : STD_LOGIC;
  signal pcie_versal_0_phy_gt_pcieltssm : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginclk : STD_LOGIC;
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqack : STD_LOGIC;
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqcmd : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqlanenum : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqpayld : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqreq : STD_LOGIC;
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginresack : STD_LOGIC;
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginrescmd : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreslanenum : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginrespayld : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q0_rxmarginresreq : STD_LOGIC;
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginclk : STD_LOGIC;
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqack : STD_LOGIC;
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqcmd : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqlanenum : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqpayld : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqreq : STD_LOGIC;
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginresack : STD_LOGIC;
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginrescmd : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreslanenum : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginrespayld : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_gt_rxmargin_q1_rxmarginresreq : STD_LOGIC;
  signal pcie_versal_0_phy_gtrefclk : STD_LOGIC;
  signal pcie_versal_0_phy_mac_rx_phy_rxdata : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal pcie_versal_0_phy_mac_rx_phy_rxdata_valid : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_mac_rx_phy_rxdatak : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_mac_rx_phy_rxstart_block : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_mac_rx_phy_rxsync_header : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_mac_tx_phy_txdata : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal pcie_versal_0_phy_mac_tx_phy_txdata_valid : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_mac_tx_phy_txdatak : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_mac_tx_phy_txstart_block : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_mac_tx_phy_txsync_header : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_pcie_mgt_rxn : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_pcie_mgt_rxp : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_pcie_mgt_txn : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_pcie_mgt_txp : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_pcierstb : STD_LOGIC;
  signal pcie_versal_0_phy_phy_coreclk : STD_LOGIC;
  signal pcie_versal_0_phy_phy_mac_command_phy_powerdown : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_phy_mac_command_phy_rate : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal pcie_versal_0_phy_phy_mac_command_phy_rx_termination : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_command_phy_rxpolarity : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_command_phy_txcompliance : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_command_phy_txdetectrx_loopback : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_command_phy_txelecidle : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_dbg_phy_rxeq_fsm : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_adapt_done : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_ctrl : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_done : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_lffs : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_lffs_sel : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_new_txcoeff : STD_LOGIC_VECTOR ( 143 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_preset : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_preset_sel : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_txpreset : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_ack : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_cmd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_lane_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_payload : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_req : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_ack : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_cmd : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_lane_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_payload : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_req : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal pcie_versal_0_phy_phy_mac_status_phy_phystatus : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_status_phy_phystatus_rst : STD_LOGIC;
  signal pcie_versal_0_phy_phy_mac_status_phy_ready : STD_LOGIC;
  signal pcie_versal_0_phy_phy_mac_status_phy_rxelecidle : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_status_phy_rxstatus : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal pcie_versal_0_phy_phy_mac_status_phy_rxvalid : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_drive_phy_txdeemph : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_drive_phy_txmargin : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_drive_phy_txswing : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_3rd_dbg_phy_txeq_fsm : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_coeff : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_ctrl : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_done : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_fs : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_lf : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_new_coeff : STD_LOGIC_VECTOR ( 143 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_preset : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_phy_txmaincursor : STD_LOGIC_VECTOR ( 55 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_phy_txpostcursor : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal pcie_versal_0_phy_phy_mac_tx_eq_phy_txprecursor : STD_LOGIC_VECTOR ( 39 downto 0 );
  signal pcie_versal_0_phy_phy_mcapclk : STD_LOGIC;
  signal pcie_versal_0_phy_phy_pclk : STD_LOGIC;
  signal pcie_versal_0_phy_phy_userclk : STD_LOGIC;
  signal pcie_versal_0_phy_phy_userclk2 : STD_LOGIC;
  signal pcie_versal_0_phy_rdy_out : STD_LOGIC;
  signal pcie_versal_0_user_clk : STD_LOGIC;
  signal pcie_versal_0_user_lnk_up : STD_LOGIC;
  signal pcie_versal_0_user_reset : STD_LOGIC;
  signal s_axis_cc_1_TDATA : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal s_axis_cc_1_TKEEP : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axis_cc_1_TLAST : STD_LOGIC;
  signal s_axis_cc_1_TREADY : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axis_cc_1_TUSER : STD_LOGIC_VECTOR ( 80 downto 0 );
  signal s_axis_cc_1_TVALID : STD_LOGIC;
  signal s_axis_rq_1_TDATA : STD_LOGIC_VECTOR ( 511 downto 0 );
  signal s_axis_rq_1_TKEEP : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal s_axis_rq_1_TLAST : STD_LOGIC;
  signal s_axis_rq_1_TREADY : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal s_axis_rq_1_TUSER : STD_LOGIC_VECTOR ( 182 downto 0 );
  signal s_axis_rq_1_TVALID : STD_LOGIC;
  signal sys_clk_1 : STD_LOGIC;
  signal sys_clk_gt_1 : STD_LOGIC;
  signal sys_reset_1 : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_apb3pready_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_apb3pslverr_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch0_cfokovrdrdy0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch0_cfokovrdrdy1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch0_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch0_iloresetdone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch0_resetexception_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch0_rxphaligndone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_bufgtce_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_bufgtrst_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_cfokovrdrdy0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_cfokovrdrdy1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_iloresetdone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_resetexception_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_rxoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_rxphaligndone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch1_txoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_bufgtce_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_bufgtrst_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_cfokovrdrdy0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_cfokovrdrdy1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_iloresetdone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_resetexception_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_rxoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_rxphaligndone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch2_txoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_bufgtce_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_bufgtrst_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_cfokovrdrdy0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_cfokovrdrdy1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_iloresetdone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_resetexception_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_rxoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_rxphaligndone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ch3_txoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_correcterr_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_debugtracetvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_gtpowergood_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_lcpllfbclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_lcplllock_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_lcpllrefclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_lcpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_rpllfbclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_rplllock_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_rpllrefclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_rpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_rxrecclkout0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_rxrecclkout1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_lcpllfbclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_lcplllock_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_lcpllrefclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_lcpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_rpllfbclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_rplllock_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_rpllrefclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_rpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_rxrecclkout0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_rxrecclkout1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_m0_axis_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_m0_axis_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_m1_axis_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_m1_axis_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_m2_axis_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_m2_axis_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_refclk0_clktestsigint_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_refclk0_gtrefclkpdint_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_refclk1_clktestsigint_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_refclk1_gtrefclkpdint_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_s0_axis_tready_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_s1_axis_tready_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_s2_axis_tready_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_trigackin0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_trigout0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ubinterrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_ubtxuart_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_uncorrecterr_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_0_apb3prdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch0_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch0_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch0_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch0_rxheadervalid_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch1_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch1_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch1_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch1_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch1_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch1_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch1_rxheadervalid_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch2_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch2_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch2_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch2_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch2_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch2_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch2_rxheadervalid_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch3_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch3_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch3_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch3_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch3_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch3_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ch3_rxheadervalid_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_ctrlrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_debugtracetdata_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_gpo_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_lcpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_hsclk0_rpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_lcpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_hsclk1_rpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_m0_axis_tdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_m1_axis_tdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_m2_axis_tdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_pipesouthout_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_resetdone_southout_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_rxpisouthout_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_0_txpisouthout_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_apb3pready_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_apb3pslverr_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_bufgtce_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_bufgtrst_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_cfokovrdrdy0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_cfokovrdrdy1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_iloresetdone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_resetexception_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_rxoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_rxphaligndone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch0_txoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_bufgtce_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_bufgtrst_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_cfokovrdrdy0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_cfokovrdrdy1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_iloresetdone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_resetexception_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_rxoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_rxphaligndone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch1_txoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_bufgtce_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_bufgtrst_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_cfokovrdrdy0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_cfokovrdrdy1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_iloresetdone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_resetexception_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_rxoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_rxphaligndone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch2_txoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_bufgtce_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_bufgtrst_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_cfokovrdrdy0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_cfokovrdrdy1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_dmonitoroutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_iloresetdone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_resetexception_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_rxoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_rxphaligndone_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ch3_txoutclk_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_correcterr_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_debugtracetvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_gtpowergood_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_lcpllfbclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_lcplllock_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_lcpllrefclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_lcpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_rpllfbclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_rplllock_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_rpllrefclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_rpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_rxrecclkout0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_rxrecclkout1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_lcpllfbclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_lcplllock_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_lcpllrefclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_lcpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_rpllfbclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_rplllock_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_rpllrefclklost_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_rpllrefclkmonitor_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_rxrecclkout0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_rxrecclkout1_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_m0_axis_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_m0_axis_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_m1_axis_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_m1_axis_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_m2_axis_tlast_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_m2_axis_tvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_refclk0_clktestsigint_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_refclk0_gtrefclkpdint_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_refclk1_clktestsigint_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_refclk1_gtrefclkpdint_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_s0_axis_tready_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_s1_axis_tready_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_s2_axis_tready_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_trigackin0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_trigout0_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ubinterrupt_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_ubtxuart_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_uncorrecterr_UNCONNECTED : STD_LOGIC;
  signal NLW_pcie_versal_0_gt_quad_1_apb3prdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch0_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch0_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch0_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch0_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch0_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch0_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch0_rxheadervalid_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch1_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch1_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch1_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch1_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch1_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch1_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch1_rxheadervalid_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch2_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch2_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch2_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch2_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch2_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch2_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch2_rxheadervalid_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch3_bufgtcemask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch3_bufgtdiv_UNCONNECTED : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch3_bufgtrstmask_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch3_dmonitorout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch3_pcsrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch3_pinrsvdas_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ch3_rxheadervalid_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_ctrlrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_debugtracetdata_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_gpo_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_lcpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_hsclk0_rpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_lcpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_hsclk1_rpllrsvdout_UNCONNECTED : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_m0_axis_tdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_m1_axis_tdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_m2_axis_tdata_UNCONNECTED : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_pipenorthout_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_resetdone_northout_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_rxpinorthout_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_gt_quad_1_txpinorthout_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_pcie_versal_0_phy_phy_gt_pclk_UNCONNECTED : STD_LOGIC;
begin
  m_axis_cq_tdata(511 downto 0) <= pcie_versal_0_m_axis_cq_TDATA(511 downto 0);
  m_axis_cq_tkeep(15 downto 0) <= pcie_versal_0_m_axis_cq_TKEEP(15 downto 0);
  m_axis_cq_tlast <= pcie_versal_0_m_axis_cq_TLAST;
  m_axis_cq_tuser(228 downto 0) <= pcie_versal_0_m_axis_cq_TUSER(228 downto 0);
  m_axis_cq_tvalid <= pcie_versal_0_m_axis_cq_TVALID;
  m_axis_rc_tdata(511 downto 0) <= pcie_versal_0_m_axis_rc_TDATA(511 downto 0);
  m_axis_rc_tkeep(15 downto 0) <= pcie_versal_0_m_axis_rc_TKEEP(15 downto 0);
  m_axis_rc_tlast <= pcie_versal_0_m_axis_rc_TLAST;
  m_axis_rc_tuser(160 downto 0) <= pcie_versal_0_m_axis_rc_TUSER(160 downto 0);
  m_axis_rc_tvalid <= pcie_versal_0_m_axis_rc_TVALID;
  pcie4_cfg_control_1_config_space_enable <= pcie4_cfg_control_config_space_enable;
  pcie4_cfg_control_1_ds_bus_number(7 downto 0) <= pcie4_cfg_control_ds_bus_number(7 downto 0);
  pcie4_cfg_control_1_ds_device_number(4 downto 0) <= pcie4_cfg_control_ds_device_number(4 downto 0);
  pcie4_cfg_control_1_ds_port_number(7 downto 0) <= pcie4_cfg_control_ds_port_number(7 downto 0);
  pcie4_cfg_control_1_dsn(63 downto 0) <= pcie4_cfg_control_dsn(63 downto 0);
  pcie4_cfg_control_1_err_cor_in <= pcie4_cfg_control_err_cor_in;
  pcie4_cfg_control_1_err_uncor_in <= pcie4_cfg_control_err_uncor_in;
  pcie4_cfg_control_1_flr_done(3 downto 0) <= pcie4_cfg_control_flr_done(3 downto 0);
  pcie4_cfg_control_1_hot_reset_in <= pcie4_cfg_control_hot_reset_in;
  pcie4_cfg_control_1_link_training_enable <= pcie4_cfg_control_link_training_enable;
  pcie4_cfg_control_1_power_state_change_ack <= pcie4_cfg_control_power_state_change_ack;
  pcie4_cfg_control_1_req_pm_transition_l23_ready <= pcie4_cfg_control_req_pm_transition_l23_ready;
  pcie4_cfg_control_1_vf_flr_done(0) <= pcie4_cfg_control_vf_flr_done(0);
  pcie4_cfg_control_1_vf_flr_func_num(7 downto 0) <= pcie4_cfg_control_vf_flr_func_num(7 downto 0);
  pcie4_cfg_control_bus_number(7 downto 0) <= pcie4_cfg_control_1_bus_number(7 downto 0);
  pcie4_cfg_control_flr_in_process(3 downto 0) <= pcie4_cfg_control_1_flr_in_process(3 downto 0);
  pcie4_cfg_control_hot_reset_out <= pcie4_cfg_control_1_hot_reset_out;
  pcie4_cfg_control_power_state_change_interrupt <= pcie4_cfg_control_1_power_state_change_interrupt;
  pcie4_cfg_control_vf_flr_in_process(251 downto 0) <= pcie4_cfg_control_1_vf_flr_in_process(251 downto 0);
  pcie4_cfg_external_msix_1_address(63 downto 0) <= pcie4_cfg_external_msix_address(63 downto 0);
  pcie4_cfg_external_msix_1_data(31 downto 0) <= pcie4_cfg_external_msix_data(31 downto 0);
  pcie4_cfg_external_msix_1_function_number(7 downto 0) <= pcie4_cfg_external_msix_function_number(7 downto 0);
  pcie4_cfg_external_msix_1_int_vector <= pcie4_cfg_external_msix_int_vector;
  pcie4_cfg_external_msix_1_vec_pending(1 downto 0) <= pcie4_cfg_external_msix_vec_pending(1 downto 0);
  pcie4_cfg_external_msix_enable(3 downto 0) <= pcie4_cfg_external_msix_1_enable(3 downto 0);
  pcie4_cfg_external_msix_fail <= pcie4_cfg_external_msix_1_fail;
  pcie4_cfg_external_msix_mask(3 downto 0) <= pcie4_cfg_external_msix_1_mask(3 downto 0);
  pcie4_cfg_external_msix_sent <= pcie4_cfg_external_msix_1_sent;
  pcie4_cfg_external_msix_vec_pending_status(0) <= pcie4_cfg_external_msix_1_vec_pending_status(0);
  pcie4_cfg_external_msix_vf_enable(251 downto 0) <= pcie4_cfg_external_msix_1_vf_enable(251 downto 0);
  pcie4_cfg_external_msix_vf_mask(251 downto 0) <= pcie4_cfg_external_msix_1_vf_mask(251 downto 0);
  pcie4_cfg_fc_cpld(11 downto 0) <= pcie_versal_0_pcie4_cfg_fc_CPLD(11 downto 0);
  pcie4_cfg_fc_cplh(7 downto 0) <= pcie_versal_0_pcie4_cfg_fc_CPLH(7 downto 0);
  pcie4_cfg_fc_npd(11 downto 0) <= pcie_versal_0_pcie4_cfg_fc_NPD(11 downto 0);
  pcie4_cfg_fc_nph(7 downto 0) <= pcie_versal_0_pcie4_cfg_fc_NPH(7 downto 0);
  pcie4_cfg_fc_pd(11 downto 0) <= pcie_versal_0_pcie4_cfg_fc_PD(11 downto 0);
  pcie4_cfg_fc_ph(7 downto 0) <= pcie_versal_0_pcie4_cfg_fc_PH(7 downto 0);
  pcie4_cfg_interrupt_1_INTx_VECTOR(3 downto 0) <= pcie4_cfg_interrupt_intx_vector(3 downto 0);
  pcie4_cfg_interrupt_1_PENDING(3 downto 0) <= pcie4_cfg_interrupt_pending(3 downto 0);
  pcie4_cfg_interrupt_sent <= pcie4_cfg_interrupt_1_SENT;
  pcie4_cfg_mesg_rcvd_recd <= pcie_versal_0_pcie4_cfg_mesg_rcvd_recd;
  pcie4_cfg_mesg_rcvd_recd_data(7 downto 0) <= pcie_versal_0_pcie4_cfg_mesg_rcvd_recd_data(7 downto 0);
  pcie4_cfg_mesg_rcvd_recd_type(4 downto 0) <= pcie_versal_0_pcie4_cfg_mesg_rcvd_recd_type(4 downto 0);
  pcie4_cfg_mesg_tx_transmit_done <= pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT_DONE;
  pcie4_cfg_mgmt_1_ADDR(9 downto 0) <= pcie4_cfg_mgmt_addr(9 downto 0);
  pcie4_cfg_mgmt_1_BYTE_EN(3 downto 0) <= pcie4_cfg_mgmt_byte_en(3 downto 0);
  pcie4_cfg_mgmt_1_DEBUG_ACCESS <= pcie4_cfg_mgmt_debug_access;
  pcie4_cfg_mgmt_1_FUNCTION_NUMBER(7 downto 0) <= pcie4_cfg_mgmt_function_number(7 downto 0);
  pcie4_cfg_mgmt_1_READ_EN <= pcie4_cfg_mgmt_read_en;
  pcie4_cfg_mgmt_1_WRITE_DATA(31 downto 0) <= pcie4_cfg_mgmt_write_data(31 downto 0);
  pcie4_cfg_mgmt_1_WRITE_EN <= pcie4_cfg_mgmt_write_en;
  pcie4_cfg_mgmt_read_data(31 downto 0) <= pcie4_cfg_mgmt_1_READ_DATA(31 downto 0);
  pcie4_cfg_mgmt_read_write_done <= pcie4_cfg_mgmt_1_READ_WRITE_DONE;
  pcie4_cfg_pm_1_pm_aspm_l1entry_reject <= pcie4_cfg_pm_pm_aspm_l1entry_reject;
  pcie4_cfg_pm_1_pm_aspm_tx_l0s_entry_disable <= pcie4_cfg_pm_pm_aspm_tx_l0s_entry_disable;
  pcie4_cfg_status_cq_np_req_count(5 downto 0) <= pcie_versal_0_pcie4_cfg_status_cq_np_req_count(5 downto 0);
  pcie4_cfg_status_current_speed(1 downto 0) <= pcie_versal_0_pcie4_cfg_status_current_speed(1 downto 0);
  pcie4_cfg_status_err_cor_out <= pcie_versal_0_pcie4_cfg_status_err_cor_out;
  pcie4_cfg_status_err_fatal_out <= pcie_versal_0_pcie4_cfg_status_err_fatal_out;
  pcie4_cfg_status_err_nonfatal_out <= pcie_versal_0_pcie4_cfg_status_err_nonfatal_out;
  pcie4_cfg_status_function_power_state(11 downto 0) <= pcie_versal_0_pcie4_cfg_status_function_power_state(11 downto 0);
  pcie4_cfg_status_function_status(15 downto 0) <= pcie_versal_0_pcie4_cfg_status_function_status(15 downto 0);
  pcie4_cfg_status_link_power_state(1 downto 0) <= pcie_versal_0_pcie4_cfg_status_link_power_state(1 downto 0);
  pcie4_cfg_status_local_error_out(4 downto 0) <= pcie_versal_0_pcie4_cfg_status_local_error_out(4 downto 0);
  pcie4_cfg_status_local_error_valid <= pcie_versal_0_pcie4_cfg_status_local_error_valid;
  pcie4_cfg_status_ltssm_state(5 downto 0) <= pcie_versal_0_pcie4_cfg_status_ltssm_state(5 downto 0);
  pcie4_cfg_status_max_payload(1 downto 0) <= pcie_versal_0_pcie4_cfg_status_max_payload(1 downto 0);
  pcie4_cfg_status_max_read_req(2 downto 0) <= pcie_versal_0_pcie4_cfg_status_max_read_req(2 downto 0);
  pcie4_cfg_status_negotiated_width(2 downto 0) <= pcie_versal_0_pcie4_cfg_status_negotiated_width(2 downto 0);
  pcie4_cfg_status_obff_enable(1 downto 0) <= pcie_versal_0_pcie4_cfg_status_obff_enable(1 downto 0);
  pcie4_cfg_status_phy_link_down <= pcie_versal_0_pcie4_cfg_status_phy_link_down;
  pcie4_cfg_status_phy_link_status(1 downto 0) <= pcie_versal_0_pcie4_cfg_status_phy_link_status(1 downto 0);
  pcie4_cfg_status_pl_status_change <= pcie_versal_0_pcie4_cfg_status_pl_status_change;
  pcie4_cfg_status_rcb_status(3 downto 0) <= pcie_versal_0_pcie4_cfg_status_rcb_status(3 downto 0);
  pcie4_cfg_status_rq_seq_num0(5 downto 0) <= pcie_versal_0_pcie4_cfg_status_rq_seq_num0(5 downto 0);
  pcie4_cfg_status_rq_seq_num1(5 downto 0) <= pcie_versal_0_pcie4_cfg_status_rq_seq_num1(5 downto 0);
  pcie4_cfg_status_rq_seq_num_vld0 <= pcie_versal_0_pcie4_cfg_status_rq_seq_num_vld0;
  pcie4_cfg_status_rq_seq_num_vld1 <= pcie_versal_0_pcie4_cfg_status_rq_seq_num_vld1;
  pcie4_cfg_status_rq_tag0(9 downto 0) <= pcie_versal_0_pcie4_cfg_status_rq_tag0(9 downto 0);
  pcie4_cfg_status_rq_tag1(9 downto 0) <= pcie_versal_0_pcie4_cfg_status_rq_tag1(9 downto 0);
  pcie4_cfg_status_rq_tag_av(3 downto 0) <= pcie_versal_0_pcie4_cfg_status_rq_tag_av(3 downto 0);
  pcie4_cfg_status_rq_tag_vld0 <= pcie_versal_0_pcie4_cfg_status_rq_tag_vld0;
  pcie4_cfg_status_rq_tag_vld1 <= pcie_versal_0_pcie4_cfg_status_rq_tag_vld1;
  pcie4_cfg_status_rx_pm_state(1 downto 0) <= pcie_versal_0_pcie4_cfg_status_rx_pm_state(1 downto 0);
  pcie4_cfg_status_tfc_npd_av(3 downto 0) <= pcie_versal_0_pcie4_cfg_status_tfc_npd_av(3 downto 0);
  pcie4_cfg_status_tfc_nph_av(3 downto 0) <= pcie_versal_0_pcie4_cfg_status_tfc_nph_av(3 downto 0);
  pcie4_cfg_status_tph_requester_enable(3 downto 0) <= pcie_versal_0_pcie4_cfg_status_tph_requester_enable(3 downto 0);
  pcie4_cfg_status_tph_st_mode(11 downto 0) <= pcie_versal_0_pcie4_cfg_status_tph_st_mode(11 downto 0);
  pcie4_cfg_status_tx_pm_state(1 downto 0) <= pcie_versal_0_pcie4_cfg_status_tx_pm_state(1 downto 0);
  pcie4_cfg_status_vf_power_state(755 downto 0) <= pcie_versal_0_pcie4_cfg_status_vf_power_state(755 downto 0);
  pcie4_cfg_status_vf_status(503 downto 0) <= pcie_versal_0_pcie4_cfg_status_vf_status(503 downto 0);
  pcie4_cfg_status_vf_tph_requester_enable(251 downto 0) <= pcie_versal_0_pcie4_cfg_status_vf_tph_requester_enable(251 downto 0);
  pcie4_cfg_status_vf_tph_st_mode(755 downto 0) <= pcie_versal_0_pcie4_cfg_status_vf_tph_st_mode(755 downto 0);
  pcie_7x_mgt_txn(7 downto 0) <= pcie_versal_0_phy_pcie_mgt_txn(7 downto 0);
  pcie_7x_mgt_txp(7 downto 0) <= pcie_versal_0_phy_pcie_mgt_txp(7 downto 0);
  pcie_versal_0_m_axis_cq_TREADY <= m_axis_cq_tready;
  pcie_versal_0_m_axis_rc_TREADY <= m_axis_rc_tready;
  pcie_versal_0_pcie4_cfg_fc_SEL(2 downto 0) <= pcie4_cfg_fc_sel(2 downto 0);
  pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT <= pcie4_cfg_mesg_tx_transmit;
  pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT_DATA(31 downto 0) <= pcie4_cfg_mesg_tx_transmit_data(31 downto 0);
  pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT_TYPE(2 downto 0) <= pcie4_cfg_mesg_tx_transmit_type(2 downto 0);
  pcie_versal_0_pcie4_cfg_status_cq_np_req(1 downto 0) <= pcie4_cfg_status_cq_np_req(1 downto 0);
  pcie_versal_0_phy_pcie_mgt_rxn(7 downto 0) <= pcie_7x_mgt_rxn(7 downto 0);
  pcie_versal_0_phy_pcie_mgt_rxp(7 downto 0) <= pcie_7x_mgt_rxp(7 downto 0);
  phy_rdy_out <= pcie_versal_0_phy_rdy_out;
  s_axis_cc_1_TDATA(511 downto 0) <= s_axis_cc_tdata(511 downto 0);
  s_axis_cc_1_TKEEP(15 downto 0) <= s_axis_cc_tkeep(15 downto 0);
  s_axis_cc_1_TLAST <= s_axis_cc_tlast;
  s_axis_cc_1_TUSER(80 downto 0) <= s_axis_cc_tuser(80 downto 0);
  s_axis_cc_1_TVALID <= s_axis_cc_tvalid;
  s_axis_cc_tready(3 downto 0) <= s_axis_cc_1_TREADY(3 downto 0);
  s_axis_rq_1_TDATA(511 downto 0) <= s_axis_rq_tdata(511 downto 0);
  s_axis_rq_1_TKEEP(15 downto 0) <= s_axis_rq_tkeep(15 downto 0);
  s_axis_rq_1_TLAST <= s_axis_rq_tlast;
  s_axis_rq_1_TUSER(182 downto 0) <= s_axis_rq_tuser(182 downto 0);
  s_axis_rq_1_TVALID <= s_axis_rq_tvalid;
  s_axis_rq_tready(3 downto 0) <= s_axis_rq_1_TREADY(3 downto 0);
  sys_clk_1 <= sys_clk;
  sys_clk_gt_1 <= sys_clk_gt;
  sys_reset_1 <= sys_reset;
  user_clk <= pcie_versal_0_user_clk;
  user_lnk_up <= pcie_versal_0_user_lnk_up;
  user_reset <= pcie_versal_0_user_reset;
  
pcie_versal_0_0: pcie_versal_0
     port map (
      cfg_bus_number(7 downto 0) => pcie4_cfg_control_1_bus_number(7 downto 0),
      cfg_config_space_enable => pcie4_cfg_control_1_config_space_enable,
      cfg_current_speed(1 downto 0) => pcie_versal_0_pcie4_cfg_status_current_speed(1 downto 0),
      cfg_dev_id_pf0(15 downto 0) => cfg_dev_id_pf0,
      cfg_ds_bus_number(7 downto 0) => pcie4_cfg_control_1_ds_bus_number(7 downto 0),
      cfg_ds_device_number(4 downto 0) => pcie4_cfg_control_1_ds_device_number(4 downto 0),
      cfg_ds_port_number(7 downto 0) => pcie4_cfg_control_1_ds_port_number(7 downto 0),
      cfg_dsn(63 downto 0) => pcie4_cfg_control_1_dsn(63 downto 0),
      cfg_err_cor_in => pcie4_cfg_control_1_err_cor_in,
      cfg_err_cor_out => pcie_versal_0_pcie4_cfg_status_err_cor_out,
      cfg_err_fatal_out => pcie_versal_0_pcie4_cfg_status_err_fatal_out,
      cfg_err_nonfatal_out => pcie_versal_0_pcie4_cfg_status_err_nonfatal_out,
      cfg_err_uncor_in => pcie4_cfg_control_1_err_uncor_in,
      cfg_fc_cpld(11 downto 0) => pcie_versal_0_pcie4_cfg_fc_CPLD(11 downto 0),
      cfg_fc_cplh(7 downto 0) => pcie_versal_0_pcie4_cfg_fc_CPLH(7 downto 0),
      cfg_fc_npd(11 downto 0) => pcie_versal_0_pcie4_cfg_fc_NPD(11 downto 0),
      cfg_fc_nph(7 downto 0) => pcie_versal_0_pcie4_cfg_fc_NPH(7 downto 0),
      cfg_fc_pd(11 downto 0) => pcie_versal_0_pcie4_cfg_fc_PD(11 downto 0),
      cfg_fc_ph(7 downto 0) => pcie_versal_0_pcie4_cfg_fc_PH(7 downto 0),
      cfg_fc_sel(2 downto 0) => pcie_versal_0_pcie4_cfg_fc_SEL(2 downto 0),
      cfg_flr_done(3 downto 0) => pcie4_cfg_control_1_flr_done(3 downto 0),
      cfg_flr_in_process(3 downto 0) => pcie4_cfg_control_1_flr_in_process(3 downto 0),
      cfg_function_power_state(11 downto 0) => pcie_versal_0_pcie4_cfg_status_function_power_state(11 downto 0),
      cfg_function_status(15 downto 0) => pcie_versal_0_pcie4_cfg_status_function_status(15 downto 0),
      cfg_hot_reset_in => pcie4_cfg_control_1_hot_reset_in,
      cfg_hot_reset_out => pcie4_cfg_control_1_hot_reset_out,
      cfg_interrupt_int(3 downto 0) => pcie4_cfg_interrupt_1_INTx_VECTOR(3 downto 0),
      cfg_interrupt_msi_fail => pcie4_cfg_external_msix_1_fail,
      cfg_interrupt_msi_function_number(7 downto 0) => pcie4_cfg_external_msix_1_function_number(7 downto 0),
      cfg_interrupt_msi_sent => pcie4_cfg_external_msix_1_sent,
      cfg_interrupt_msix_address(63 downto 0) => pcie4_cfg_external_msix_1_address(63 downto 0),
      cfg_interrupt_msix_data(31 downto 0) => pcie4_cfg_external_msix_1_data(31 downto 0),
      cfg_interrupt_msix_enable(3 downto 0) => pcie4_cfg_external_msix_1_enable(3 downto 0),
      cfg_interrupt_msix_int => pcie4_cfg_external_msix_1_int_vector,
      cfg_interrupt_msix_mask(3 downto 0) => pcie4_cfg_external_msix_1_mask(3 downto 0),
      cfg_interrupt_msix_vec_pending(1 downto 0) => pcie4_cfg_external_msix_1_vec_pending(1 downto 0),
      cfg_interrupt_msix_vec_pending_status(0) => pcie4_cfg_external_msix_1_vec_pending_status(0),
      cfg_interrupt_msix_vf_enable(251 downto 0) => pcie4_cfg_external_msix_1_vf_enable(251 downto 0),
      cfg_interrupt_msix_vf_mask(251 downto 0) => pcie4_cfg_external_msix_1_vf_mask(251 downto 0),
      cfg_interrupt_pending(3 downto 0) => pcie4_cfg_interrupt_1_PENDING(3 downto 0),
      cfg_interrupt_sent => pcie4_cfg_interrupt_1_SENT,
      cfg_link_power_state(1 downto 0) => pcie_versal_0_pcie4_cfg_status_link_power_state(1 downto 0),
      cfg_link_training_enable => pcie4_cfg_control_1_link_training_enable,
      cfg_local_error_out(4 downto 0) => pcie_versal_0_pcie4_cfg_status_local_error_out(4 downto 0),
      cfg_local_error_valid => pcie_versal_0_pcie4_cfg_status_local_error_valid,
      cfg_ltssm_state(5 downto 0) => pcie_versal_0_pcie4_cfg_status_ltssm_state(5 downto 0),
      cfg_max_payload(1 downto 0) => pcie_versal_0_pcie4_cfg_status_max_payload(1 downto 0),
      cfg_max_read_req(2 downto 0) => pcie_versal_0_pcie4_cfg_status_max_read_req(2 downto 0),
      cfg_mgmt_addr(9 downto 0) => pcie4_cfg_mgmt_1_ADDR(9 downto 0),
      cfg_mgmt_byte_enable(3 downto 0) => pcie4_cfg_mgmt_1_BYTE_EN(3 downto 0),
      cfg_mgmt_debug_access => pcie4_cfg_mgmt_1_DEBUG_ACCESS,
      cfg_mgmt_function_number(7 downto 0) => pcie4_cfg_mgmt_1_FUNCTION_NUMBER(7 downto 0),
      cfg_mgmt_read => pcie4_cfg_mgmt_1_READ_EN,
      cfg_mgmt_read_data(31 downto 0) => pcie4_cfg_mgmt_1_READ_DATA(31 downto 0),
      cfg_mgmt_read_write_done => pcie4_cfg_mgmt_1_READ_WRITE_DONE,
      cfg_mgmt_write => pcie4_cfg_mgmt_1_WRITE_EN,
      cfg_mgmt_write_data(31 downto 0) => pcie4_cfg_mgmt_1_WRITE_DATA(31 downto 0),
      cfg_msg_received => pcie_versal_0_pcie4_cfg_mesg_rcvd_recd,
      cfg_msg_received_data(7 downto 0) => pcie_versal_0_pcie4_cfg_mesg_rcvd_recd_data(7 downto 0),
      cfg_msg_received_type(4 downto 0) => pcie_versal_0_pcie4_cfg_mesg_rcvd_recd_type(4 downto 0),
      cfg_msg_transmit => pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT,
      cfg_msg_transmit_data(31 downto 0) => pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT_DATA(31 downto 0),
      cfg_msg_transmit_done => pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT_DONE,
      cfg_msg_transmit_type(2 downto 0) => pcie_versal_0_pcie4_cfg_mesg_tx_TRANSMIT_TYPE(2 downto 0),
      cfg_negotiated_width(2 downto 0) => pcie_versal_0_pcie4_cfg_status_negotiated_width(2 downto 0),
      cfg_obff_enable(1 downto 0) => pcie_versal_0_pcie4_cfg_status_obff_enable(1 downto 0),
      cfg_phy_link_down => pcie_versal_0_pcie4_cfg_status_phy_link_down,
      cfg_phy_link_status(1 downto 0) => pcie_versal_0_pcie4_cfg_status_phy_link_status(1 downto 0),
      cfg_pl_status_change => pcie_versal_0_pcie4_cfg_status_pl_status_change,
      cfg_pm_aspm_l1_entry_reject => pcie4_cfg_pm_1_pm_aspm_l1entry_reject,
      cfg_pm_aspm_tx_l0s_entry_disable => pcie4_cfg_pm_1_pm_aspm_tx_l0s_entry_disable,
      cfg_power_state_change_ack => pcie4_cfg_control_1_power_state_change_ack,
      cfg_power_state_change_interrupt => pcie4_cfg_control_1_power_state_change_interrupt,
      cfg_rcb_status(3 downto 0) => pcie_versal_0_pcie4_cfg_status_rcb_status(3 downto 0),
      cfg_req_pm_transition_l23_ready => pcie4_cfg_control_1_req_pm_transition_l23_ready,
      cfg_rev_id_pf0(7 downto 0) => B"00000000",
      cfg_rx_pm_state(1 downto 0) => pcie_versal_0_pcie4_cfg_status_rx_pm_state(1 downto 0),
      cfg_subsys_id_pf0(15 downto 0) => B"0000000000000111",
      cfg_subsys_vend_id(15 downto 0) => cfg_subsys_vend_id,
      cfg_tph_requester_enable(3 downto 0) => pcie_versal_0_pcie4_cfg_status_tph_requester_enable(3 downto 0),
      cfg_tph_st_mode(11 downto 0) => pcie_versal_0_pcie4_cfg_status_tph_st_mode(11 downto 0),
      cfg_tx_pm_state(1 downto 0) => pcie_versal_0_pcie4_cfg_status_tx_pm_state(1 downto 0),
      cfg_vend_id(15 downto 0) => cfg_vend_id,
      cfg_vf_flr_done(0) => pcie4_cfg_control_1_vf_flr_done(0),
      cfg_vf_flr_func_num(7 downto 0) => pcie4_cfg_control_1_vf_flr_func_num(7 downto 0),
      cfg_vf_flr_in_process(251 downto 0) => pcie4_cfg_control_1_vf_flr_in_process(251 downto 0),
      cfg_vf_power_state(755 downto 0) => pcie_versal_0_pcie4_cfg_status_vf_power_state(755 downto 0),
      cfg_vf_status(503 downto 0) => pcie_versal_0_pcie4_cfg_status_vf_status(503 downto 0),
      cfg_vf_tph_requester_enable(251 downto 0) => pcie_versal_0_pcie4_cfg_status_vf_tph_requester_enable(251 downto 0),
      cfg_vf_tph_st_mode(755 downto 0) => pcie_versal_0_pcie4_cfg_status_vf_tph_st_mode(755 downto 0),
      dbg_phy_rxeq_fsm(2 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_dbg_phy_rxeq_fsm(2 downto 0),
      dbg_phy_txeq_fsm(2 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_dbg_phy_txeq_fsm(2 downto 0),
      m_axis_cq_tdata(511 downto 0) => pcie_versal_0_m_axis_cq_TDATA(511 downto 0),
      m_axis_cq_tkeep(15 downto 0) => pcie_versal_0_m_axis_cq_TKEEP(15 downto 0),
      m_axis_cq_tlast => pcie_versal_0_m_axis_cq_TLAST,
      m_axis_cq_tready => pcie_versal_0_m_axis_cq_TREADY,
      m_axis_cq_tuser(228 downto 0) => pcie_versal_0_m_axis_cq_TUSER(228 downto 0),
      m_axis_cq_tvalid => pcie_versal_0_m_axis_cq_TVALID,
      m_axis_rc_tdata(511 downto 0) => pcie_versal_0_m_axis_rc_TDATA(511 downto 0),
      m_axis_rc_tkeep(15 downto 0) => pcie_versal_0_m_axis_rc_TKEEP(15 downto 0),
      m_axis_rc_tlast => pcie_versal_0_m_axis_rc_TLAST,
      m_axis_rc_tready => pcie_versal_0_m_axis_rc_TREADY,
      m_axis_rc_tuser(160 downto 0) => pcie_versal_0_m_axis_rc_TUSER(160 downto 0),
      m_axis_rc_tvalid => pcie_versal_0_m_axis_rc_TVALID,
      pcie_cq_np_req(1 downto 0) => pcie_versal_0_pcie4_cfg_status_cq_np_req(1 downto 0),
      pcie_cq_np_req_count(5 downto 0) => pcie_versal_0_pcie4_cfg_status_cq_np_req_count(5 downto 0),
      pcie_ltssm_state(5 downto 0) => pcie_versal_0_pcie_ltssm_state(5 downto 0),
      pcie_rq_seq_num0(5 downto 0) => pcie_versal_0_pcie4_cfg_status_rq_seq_num0(5 downto 0),
      pcie_rq_seq_num1(5 downto 0) => pcie_versal_0_pcie4_cfg_status_rq_seq_num1(5 downto 0),
      pcie_rq_seq_num_vld0 => pcie_versal_0_pcie4_cfg_status_rq_seq_num_vld0,
      pcie_rq_seq_num_vld1 => pcie_versal_0_pcie4_cfg_status_rq_seq_num_vld1,
      pcie_rq_tag0(9 downto 0) => pcie_versal_0_pcie4_cfg_status_rq_tag0(9 downto 0),
      pcie_rq_tag1(9 downto 0) => pcie_versal_0_pcie4_cfg_status_rq_tag1(9 downto 0),
      pcie_rq_tag_av(3 downto 0) => pcie_versal_0_pcie4_cfg_status_rq_tag_av(3 downto 0),
      pcie_rq_tag_vld0 => pcie_versal_0_pcie4_cfg_status_rq_tag_vld0,
      pcie_rq_tag_vld1 => pcie_versal_0_pcie4_cfg_status_rq_tag_vld1,
      pcie_tfc_npd_av(3 downto 0) => pcie_versal_0_pcie4_cfg_status_tfc_npd_av(3 downto 0),
      pcie_tfc_nph_av(3 downto 0) => pcie_versal_0_pcie4_cfg_status_tfc_nph_av(3 downto 0),
      phy_coreclk => pcie_versal_0_phy_phy_coreclk,
      phy_mcapclk => pcie_versal_0_phy_phy_mcapclk,
      phy_pclk => pcie_versal_0_phy_phy_pclk,
      phy_phystatus(7 downto 0) => pcie_versal_0_phy_phy_mac_status_phy_phystatus(7 downto 0),
      phy_phystatus_rst => pcie_versal_0_phy_phy_mac_status_phy_phystatus_rst,
      phy_powerdown(15 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_powerdown(15 downto 0),
      phy_rate(2 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_rate(2 downto 0),
      phy_rdy_out => pcie_versal_0_phy_rdy_out,
      phy_ready => pcie_versal_0_phy_phy_mac_status_phy_ready,
      phy_rx_margin_req_ack(1 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_ack(1 downto 0),
      phy_rx_margin_req_cmd(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_cmd(7 downto 0),
      phy_rx_margin_req_lane_num(3 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_lane_num(3 downto 0),
      phy_rx_margin_req_payload(15 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_payload(15 downto 0),
      phy_rx_margin_req_req(1 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_req(1 downto 0),
      phy_rx_margin_res_ack(1 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_ack(1 downto 0),
      phy_rx_margin_res_cmd(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_cmd(7 downto 0),
      phy_rx_margin_res_lane_num(3 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_lane_num(3 downto 0),
      phy_rx_margin_res_payload(15 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_payload(15 downto 0),
      phy_rx_margin_res_req(1 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_req(1 downto 0),
      phy_rx_termination(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_rx_termination(7 downto 0),
      phy_rxdata(511 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxdata(511 downto 0),
      phy_rxdata_valid(7 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxdata_valid(7 downto 0),
      phy_rxdatak(15 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxdatak(15 downto 0),
      phy_rxelecidle(7 downto 0) => pcie_versal_0_phy_phy_mac_status_phy_rxelecidle(7 downto 0),
      phy_rxeq_adapt_done(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_adapt_done(7 downto 0),
      phy_rxeq_ctrl(15 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_ctrl(15 downto 0),
      phy_rxeq_done(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_done(7 downto 0),
      phy_rxeq_lffs(47 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_lffs(47 downto 0),
      phy_rxeq_lffs_sel(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_lffs_sel(7 downto 0),
      phy_rxeq_new_txcoeff(143 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_new_txcoeff(143 downto 0),
      phy_rxeq_preset(23 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_preset(23 downto 0),
      phy_rxeq_preset_sel(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_preset_sel(7 downto 0),
      phy_rxeq_txpreset(31 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_txpreset(31 downto 0),
      phy_rxpolarity(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_rxpolarity(7 downto 0),
      phy_rxstart_block(15 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxstart_block(15 downto 0),
      phy_rxstatus(23 downto 0) => pcie_versal_0_phy_phy_mac_status_phy_rxstatus(23 downto 0),
      phy_rxsync_header(15 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxsync_header(15 downto 0),
      phy_rxvalid(7 downto 0) => pcie_versal_0_phy_phy_mac_status_phy_rxvalid(7 downto 0),
      phy_txcompliance(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_txcompliance(7 downto 0),
      phy_txdata(511 downto 0) => pcie_versal_0_phy_mac_tx_phy_txdata(511 downto 0),
      phy_txdata_valid(7 downto 0) => pcie_versal_0_phy_mac_tx_phy_txdata_valid(7 downto 0),
      phy_txdatak(15 downto 0) => pcie_versal_0_phy_mac_tx_phy_txdatak(15 downto 0),
      phy_txdeemph(7 downto 0) => pcie_versal_0_phy_phy_mac_tx_drive_phy_txdeemph(7 downto 0),
      phy_txdetectrx_loopback(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_txdetectrx_loopback(7 downto 0),
      phy_txelecidle(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_txelecidle(7 downto 0),
      phy_txeq_coeff(47 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_coeff(47 downto 0),
      phy_txeq_ctrl(15 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_ctrl(15 downto 0),
      phy_txeq_done(7 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_done(7 downto 0),
      phy_txeq_fs(5 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_fs(5 downto 0),
      phy_txeq_lf(5 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_lf(5 downto 0),
      phy_txeq_new_coeff(143 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_new_coeff(143 downto 0),
      phy_txeq_preset(31 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_preset(31 downto 0),
      phy_txmaincursor(55 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_phy_txmaincursor(55 downto 0),
      phy_txmargin(23 downto 0) => pcie_versal_0_phy_phy_mac_tx_drive_phy_txmargin(23 downto 0),
      phy_txpostcursor(39 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_phy_txpostcursor(39 downto 0),
      phy_txprecursor(39 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_phy_txprecursor(39 downto 0),
      phy_txstart_block(7 downto 0) => pcie_versal_0_phy_mac_tx_phy_txstart_block(7 downto 0),
      phy_txswing(7 downto 0) => pcie_versal_0_phy_phy_mac_tx_drive_phy_txswing(7 downto 0),
      phy_txsync_header(15 downto 0) => pcie_versal_0_phy_mac_tx_phy_txsync_header(15 downto 0),
      phy_userclk => pcie_versal_0_phy_phy_userclk,
      phy_userclk2 => pcie_versal_0_phy_phy_userclk2,
      s_axis_cc_tdata(511 downto 0) => s_axis_cc_1_TDATA(511 downto 0),
      s_axis_cc_tkeep(15 downto 0) => s_axis_cc_1_TKEEP(15 downto 0),
      s_axis_cc_tlast => s_axis_cc_1_TLAST,
      s_axis_cc_tready(3 downto 0) => s_axis_cc_1_TREADY(3 downto 0),
      s_axis_cc_tuser(80 downto 0) => s_axis_cc_1_TUSER(80 downto 0),
      s_axis_cc_tvalid => s_axis_cc_1_TVALID,
      s_axis_rq_tdata(511 downto 0) => s_axis_rq_1_TDATA(511 downto 0),
      s_axis_rq_tkeep(15 downto 0) => s_axis_rq_1_TKEEP(15 downto 0),
      s_axis_rq_tlast => s_axis_rq_1_TLAST,
      s_axis_rq_tready(3 downto 0) => s_axis_rq_1_TREADY(3 downto 0),
      s_axis_rq_tuser(182 downto 0) => s_axis_rq_1_TUSER(182 downto 0),
      s_axis_rq_tvalid => s_axis_rq_1_TVALID,
      sys_clk => sys_clk_1,
      sys_clk_gt => sys_clk_gt_1,
      sys_reset => sys_reset_1,
      user_clk => pcie_versal_0_user_clk,
      user_lnk_up => pcie_versal_0_user_lnk_up,
      user_reset => pcie_versal_0_user_reset
    );
pcie_versal_0_gt_quad_0: pcie_versal_0_support_gt_quad_0
     port map (
      GT_REFCLK0 => pcie_versal_0_phy_gtrefclk,
      altclk => '0',
      apb3clk => sys_clk_1,
      apb3paddr(15 downto 0) => B"0000000000000000",
      apb3penable => '0',
      apb3prdata(31 downto 0) => NLW_pcie_versal_0_gt_quad_0_apb3prdata_UNCONNECTED(31 downto 0),
      apb3pready => NLW_pcie_versal_0_gt_quad_0_apb3pready_UNCONNECTED,
      apb3presetn => '1',
      apb3psel => '0',
      apb3pslverr => NLW_pcie_versal_0_gt_quad_0_apb3pslverr_UNCONNECTED,
      apb3pwdata(31 downto 0) => B"00000000000000000000000000000000",
      apb3pwrite => '0',
      axisclk => sys_clk_1,
      bgbypassb => '0',
      bgmonitorenb => '0',
      bgpdb => '0',
      bgrcalovrd(4 downto 0) => B"00000",
      bgrcalovrdenb => '0',
      ch0_bufgtce => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtce,
      ch0_bufgtcemask(3 downto 0) => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtcemask(3 downto 0),
      ch0_bufgtdiv(11 downto 0) => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtdiv(11 downto 0),
      ch0_bufgtrst => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtrst,
      ch0_bufgtrstmask(3 downto 0) => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtrstmask(3 downto 0),
      ch0_cdrbmcdrreq => '0',
      ch0_cdrfreqos => '0',
      ch0_cdrincpctrl => '0',
      ch0_cdrstepdir => '0',
      ch0_cdrstepsq => '0',
      ch0_cdrstepsx => '0',
      ch0_cfokovrdfinish => '0',
      ch0_cfokovrdpulse => '0',
      ch0_cfokovrdrdy0 => NLW_pcie_versal_0_gt_quad_0_ch0_cfokovrdrdy0_UNCONNECTED,
      ch0_cfokovrdrdy1 => NLW_pcie_versal_0_gt_quad_0_ch0_cfokovrdrdy1_UNCONNECTED,
      ch0_cfokovrdstart => '0',
      ch0_clkrsvd0 => '0',
      ch0_clkrsvd1 => '0',
      ch0_dmonfiforeset => '0',
      ch0_dmonitorclk => '0',
      ch0_dmonitorout(31 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch0_dmonitorout_UNCONNECTED(31 downto 0),
      ch0_dmonitoroutclk => NLW_pcie_versal_0_gt_quad_0_ch0_dmonitoroutclk_UNCONNECTED,
      ch0_eyescandataerror => pcie_versal_0_phy_GT_RX0_ch_eyescandataerror,
      ch0_eyescanreset => '0',
      ch0_eyescantrigger => '0',
      ch0_gtrsvd(15 downto 0) => B"0000000000000000",
      ch0_gtrxreset => '0',
      ch0_gttxreset => '0',
      ch0_hsdppcsreset => '0',
      ch0_iloreset => '0',
      ch0_iloresetdone => NLW_pcie_versal_0_gt_quad_0_ch0_iloresetdone_UNCONNECTED,
      ch0_iloresetmask => '1',
      ch0_loopback(2 downto 0) => B"000",
      ch0_pcierstb => pcie_versal_0_phy_pcierstb,
      ch0_pcsrsvdin(15 downto 0) => B"0000001001000000",
      ch0_pcsrsvdout(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch0_pcsrsvdout_UNCONNECTED(15 downto 0),
      ch0_phyesmadaptsave => '0',
      ch0_phyready => pcie_versal_0_gt_quad_0_ch0_phyready,
      ch0_phystatus => pcie_versal_0_gt_quad_0_ch0_phystatus,
      ch0_pinrsvdas(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch0_pinrsvdas_UNCONNECTED(15 downto 0),
      ch0_resetexception => NLW_pcie_versal_0_gt_quad_0_ch0_resetexception_UNCONNECTED,
      ch0_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rx10gstat(7 downto 0),
      ch0_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxbufstatus(2 downto 0),
      ch0_rxbyteisaligned => pcie_versal_0_phy_GT_RX0_ch_rxbyteisaligned,
      ch0_rxbyterealign => pcie_versal_0_phy_GT_RX0_ch_rxbyterealign,
      ch0_rxcdrhold => '0',
      ch0_rxcdrlock => pcie_versal_0_phy_GT_RX0_ch_rxcdrlock,
      ch0_rxcdrovrden => '0',
      ch0_rxcdrphdone => pcie_versal_0_phy_GT_RX0_ch_rxcdrphdone,
      ch0_rxcdrreset => '0',
      ch0_rxchanbondseq => pcie_versal_0_phy_GT_RX0_ch_rxchanbondseq,
      ch0_rxchanisaligned => pcie_versal_0_phy_GT_RX0_ch_rxchanisaligned,
      ch0_rxchanrealign => pcie_versal_0_phy_GT_RX0_ch_rxchanrealign,
      ch0_rxchbondi(4 downto 0) => B"00000",
      ch0_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxchbondo(4 downto 0),
      ch0_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxclkcorcnt(1 downto 0),
      ch0_rxcominitdet => pcie_versal_0_phy_GT_RX0_ch_rxcominitdet,
      ch0_rxcommadet => pcie_versal_0_phy_GT_RX0_ch_rxcommadet,
      ch0_rxcomsasdet => pcie_versal_0_phy_GT_RX0_ch_rxcomsasdet,
      ch0_rxcomwakedet => pcie_versal_0_phy_GT_RX0_ch_rxcomwakedet,
      ch0_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxctrl0(15 downto 0),
      ch0_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxctrl1(15 downto 0),
      ch0_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxctrl2(7 downto 0),
      ch0_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxctrl3(7 downto 0),
      ch0_rxdapicodeovrden => '0',
      ch0_rxdapicodereset => '0',
      ch0_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxdata(127 downto 0),
      ch0_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxdataextendrsvd(7 downto 0),
      ch0_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxdatavalid(1 downto 0),
      ch0_rxdccdone => pcie_versal_0_phy_GT_RX0_ch_rxdccdone,
      ch0_rxdlyalignerr => pcie_versal_0_phy_GT_RX0_ch_rxdlyalignerr,
      ch0_rxdlyalignprog => pcie_versal_0_phy_GT_RX0_ch_rxdlyalignprog,
      ch0_rxdlyalignreq => '0',
      ch0_rxelecidle => pcie_versal_0_phy_GT_RX0_ch_rxelecidle,
      ch0_rxeqtraining => '0',
      ch0_rxfinealigndone => pcie_versal_0_phy_GT_RX0_ch_rxfinealigndone,
      ch0_rxgearboxslip => '0',
      ch0_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxheader(5 downto 0),
      ch0_rxheadervalid(1 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch0_rxheadervalid_UNCONNECTED(1 downto 0),
      ch0_rxlatclk => '0',
      ch0_rxlpmen => '0',
      ch0_rxmldchaindone => '0',
      ch0_rxmldchainreq => '0',
      ch0_rxmlfinealignreq => '0',
      ch0_rxmstreset => '0',
      ch0_rxmstresetdone => pcie_versal_0_phy_GT_RX0_ch_rxmstresetdone,
      ch0_rxoobreset => '0',
      ch0_rxosintdone => pcie_versal_0_phy_GT_RX0_ch_rxosintdone,
      ch0_rxosintstarted => pcie_versal_0_phy_GT_RX0_ch_rxosintstarted,
      ch0_rxosintstrobedone => pcie_versal_0_phy_GT_RX0_ch_rxosintstrobedone,
      ch0_rxosintstrobestarted => pcie_versal_0_phy_GT_RX0_ch_rxosintstrobestarted,
      ch0_rxoutclk => pcie_versal_0_gt_quad_0_ch0_rxoutclk,
      ch0_rxpcsresetmask(4 downto 0) => B"11111",
      ch0_rxpd(1 downto 0) => B"00",
      ch0_rxphaligndone => NLW_pcie_versal_0_gt_quad_0_ch0_rxphaligndone_UNCONNECTED,
      ch0_rxphalignerr => pcie_versal_0_phy_GT_RX0_ch_rxphalignerr,
      ch0_rxphalignreq => '0',
      ch0_rxphalignresetmask(1 downto 0) => B"11",
      ch0_rxphdlypd => '0',
      ch0_rxphdlyreset => '0',
      ch0_rxphdlyresetdone => pcie_versal_0_phy_GT_RX0_ch_rxphdlyresetdone,
      ch0_rxphsetinitdone => pcie_versal_0_phy_GT_RX0_ch_rxphsetinitdone,
      ch0_rxphsetinitreq => '0',
      ch0_rxphshift180 => '0',
      ch0_rxphshift180done => pcie_versal_0_phy_GT_RX0_ch_rxphshift180done,
      ch0_rxpmaresetdone => pcie_versal_0_phy_GT_RX0_ch_rxpmaresetdone,
      ch0_rxpmaresetmask(6 downto 0) => B"1111111",
      ch0_rxpolarity => pcie_versal_0_phy_GT_RX0_ch_rxpolarity,
      ch0_rxprbscntreset => '0',
      ch0_rxprbserr => pcie_versal_0_phy_GT_RX0_ch_rxprbserr,
      ch0_rxprbslocked => pcie_versal_0_phy_GT_RX0_ch_rxprbslocked,
      ch0_rxprbssel(3 downto 0) => B"0000",
      ch0_rxprogdivreset => '0',
      ch0_rxprogdivresetdone => pcie_versal_0_phy_GT_RX0_ch_rxprogdivresetdone,
      ch0_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxrate(7 downto 0),
      ch0_rxresetdone => pcie_versal_0_phy_GT_RX0_ch_rxresetdone,
      ch0_rxresetmode(1 downto 0) => B"00",
      ch0_rxslide => '0',
      ch0_rxsliderdy => pcie_versal_0_phy_GT_RX0_ch_rxsliderdy,
      ch0_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxstartofseq(1 downto 0),
      ch0_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxstatus(2 downto 0),
      ch0_rxsyncallin => '0',
      ch0_rxsyncdone => pcie_versal_0_phy_GT_RX0_ch_rxsyncdone,
      ch0_rxtermination => pcie_versal_0_phy_GT_RX0_ch_rxtermination,
      ch0_rxuserrdy => pcie_versal_0_phy_GT_RX0_ch_rxuserrdy,
      ch0_rxusrclk => pcie_versal_0_phy_phy_pclk,
      ch0_rxvalid => pcie_versal_0_phy_GT_RX0_ch_rxvalid,
      ch0_tstin(19 downto 0) => B"00000000000000000000",
      ch0_tx10gstat => pcie_versal_0_phy_GT_TX0_ch_tx10gstat,
      ch0_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txbufstatus(1 downto 0),
      ch0_txcomfinish => pcie_versal_0_phy_GT_TX0_ch_txcomfinish,
      ch0_txcominit => '0',
      ch0_txcomsas => '0',
      ch0_txcomwake => '0',
      ch0_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txctrl0(15 downto 0),
      ch0_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txctrl1(15 downto 0),
      ch0_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txctrl2(7 downto 0),
      ch0_txdapicodeovrden => '0',
      ch0_txdapicodereset => '0',
      ch0_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txdata(127 downto 0),
      ch0_txdataextendrsvd(7 downto 0) => B"00000000",
      ch0_txdccdone => pcie_versal_0_phy_GT_TX0_ch_txdccdone,
      ch0_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txdeemph(1 downto 0),
      ch0_txdetectrx => pcie_versal_0_phy_GT_TX0_ch_txdetectrx,
      ch0_txdiffctrl(4 downto 0) => B"11001",
      ch0_txdlyalignerr => pcie_versal_0_phy_GT_TX0_ch_txdlyalignerr,
      ch0_txdlyalignprog => pcie_versal_0_phy_GT_TX0_ch_txdlyalignprog,
      ch0_txdlyalignreq => '0',
      ch0_txelecidle => pcie_versal_0_phy_GT_TX0_ch_txelecidle,
      ch0_txheader(5 downto 0) => B"000000",
      ch0_txinhibit => '0',
      ch0_txlatclk => '0',
      ch0_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txmaincursor(6 downto 0),
      ch0_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txmargin(2 downto 0),
      ch0_txmldchaindone => '0',
      ch0_txmldchainreq => '0',
      ch0_txmstreset => '0',
      ch0_txmstresetdone => pcie_versal_0_phy_GT_TX0_ch_txmstresetdone,
      ch0_txoneszeros => '0',
      ch0_txoutclk => pcie_versal_0_gt_quad_0_ch0_txoutclk,
      ch0_txpausedelayalign => '0',
      ch0_txpcsresetmask => '1',
      ch0_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txpd(1 downto 0),
      ch0_txphaligndone => pcie_versal_0_phy_GT_TX0_ch_txphaligndone,
      ch0_txphalignerr => pcie_versal_0_phy_GT_TX0_ch_txphalignerr,
      ch0_txphalignoutrsvd => pcie_versal_0_phy_GT_TX0_ch_txphalignoutrsvd,
      ch0_txphalignreq => '0',
      ch0_txphalignresetmask(1 downto 0) => B"11",
      ch0_txphdlypd => '0',
      ch0_txphdlyreset => '0',
      ch0_txphdlyresetdone => pcie_versal_0_phy_GT_TX0_ch_txphdlyresetdone,
      ch0_txphdlytstclk => '0',
      ch0_txphsetinitdone => pcie_versal_0_phy_GT_TX0_ch_txphsetinitdone,
      ch0_txphsetinitreq => '0',
      ch0_txphshift180 => '0',
      ch0_txphshift180done => pcie_versal_0_phy_GT_TX0_ch_txphshift180done,
      ch0_txpicodeovrden => '0',
      ch0_txpicodereset => '0',
      ch0_txpippmen => '0',
      ch0_txpippmstepsize(4 downto 0) => B"00000",
      ch0_txpisopd => '0',
      ch0_txpmaresetdone => pcie_versal_0_phy_GT_TX0_ch_txpmaresetdone,
      ch0_txpmaresetmask(2 downto 0) => B"111",
      ch0_txpolarity => '0',
      ch0_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txpostcursor(4 downto 0),
      ch0_txprbsforceerr => '0',
      ch0_txprbssel(3 downto 0) => B"0000",
      ch0_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txprecursor(4 downto 0),
      ch0_txprogdivreset => '0',
      ch0_txprogdivresetdone => pcie_versal_0_phy_GT_TX0_ch_txprogdivresetdone,
      ch0_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txrate(7 downto 0),
      ch0_txresetdone => pcie_versal_0_phy_GT_TX0_ch_txresetdone,
      ch0_txresetmode(1 downto 0) => B"00",
      ch0_txsequence(6 downto 0) => B"0000000",
      ch0_txswing => pcie_versal_0_phy_GT_TX0_ch_txswing,
      ch0_txsyncallin => '0',
      ch0_txsyncdone => pcie_versal_0_phy_GT_TX0_ch_txsyncdone,
      ch0_txuserrdy => pcie_versal_0_phy_GT_TX0_ch_txuserrdy,
      ch0_txusrclk => pcie_versal_0_phy_phy_pclk,
      ch1_bufgtce => NLW_pcie_versal_0_gt_quad_0_ch1_bufgtce_UNCONNECTED,
      ch1_bufgtcemask(3 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch1_bufgtcemask_UNCONNECTED(3 downto 0),
      ch1_bufgtdiv(11 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch1_bufgtdiv_UNCONNECTED(11 downto 0),
      ch1_bufgtrst => NLW_pcie_versal_0_gt_quad_0_ch1_bufgtrst_UNCONNECTED,
      ch1_bufgtrstmask(3 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch1_bufgtrstmask_UNCONNECTED(3 downto 0),
      ch1_cdrbmcdrreq => '0',
      ch1_cdrfreqos => '0',
      ch1_cdrincpctrl => '0',
      ch1_cdrstepdir => '0',
      ch1_cdrstepsq => '0',
      ch1_cdrstepsx => '0',
      ch1_cfokovrdfinish => '0',
      ch1_cfokovrdpulse => '0',
      ch1_cfokovrdrdy0 => NLW_pcie_versal_0_gt_quad_0_ch1_cfokovrdrdy0_UNCONNECTED,
      ch1_cfokovrdrdy1 => NLW_pcie_versal_0_gt_quad_0_ch1_cfokovrdrdy1_UNCONNECTED,
      ch1_cfokovrdstart => '0',
      ch1_clkrsvd0 => '0',
      ch1_clkrsvd1 => '0',
      ch1_dmonfiforeset => '0',
      ch1_dmonitorclk => '0',
      ch1_dmonitorout(31 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch1_dmonitorout_UNCONNECTED(31 downto 0),
      ch1_dmonitoroutclk => NLW_pcie_versal_0_gt_quad_0_ch1_dmonitoroutclk_UNCONNECTED,
      ch1_eyescandataerror => pcie_versal_0_phy_GT_RX1_ch_eyescandataerror,
      ch1_eyescanreset => '0',
      ch1_eyescantrigger => '0',
      ch1_gtrsvd(15 downto 0) => B"0000000000000000",
      ch1_gtrxreset => '0',
      ch1_gttxreset => '0',
      ch1_hsdppcsreset => '0',
      ch1_iloreset => '0',
      ch1_iloresetdone => NLW_pcie_versal_0_gt_quad_0_ch1_iloresetdone_UNCONNECTED,
      ch1_iloresetmask => '1',
      ch1_loopback(2 downto 0) => B"000",
      ch1_pcierstb => pcie_versal_0_phy_pcierstb,
      ch1_pcsrsvdin(15 downto 0) => B"0000001001000000",
      ch1_pcsrsvdout(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch1_pcsrsvdout_UNCONNECTED(15 downto 0),
      ch1_phyesmadaptsave => '0',
      ch1_phyready => pcie_versal_0_gt_quad_0_ch1_phyready,
      ch1_phystatus => pcie_versal_0_gt_quad_0_ch1_phystatus,
      ch1_pinrsvdas(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch1_pinrsvdas_UNCONNECTED(15 downto 0),
      ch1_resetexception => NLW_pcie_versal_0_gt_quad_0_ch1_resetexception_UNCONNECTED,
      ch1_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rx10gstat(7 downto 0),
      ch1_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxbufstatus(2 downto 0),
      ch1_rxbyteisaligned => pcie_versal_0_phy_GT_RX1_ch_rxbyteisaligned,
      ch1_rxbyterealign => pcie_versal_0_phy_GT_RX1_ch_rxbyterealign,
      ch1_rxcdrhold => '0',
      ch1_rxcdrlock => pcie_versal_0_phy_GT_RX1_ch_rxcdrlock,
      ch1_rxcdrovrden => '0',
      ch1_rxcdrphdone => pcie_versal_0_phy_GT_RX1_ch_rxcdrphdone,
      ch1_rxcdrreset => '0',
      ch1_rxchanbondseq => pcie_versal_0_phy_GT_RX1_ch_rxchanbondseq,
      ch1_rxchanisaligned => pcie_versal_0_phy_GT_RX1_ch_rxchanisaligned,
      ch1_rxchanrealign => pcie_versal_0_phy_GT_RX1_ch_rxchanrealign,
      ch1_rxchbondi(4 downto 0) => B"00000",
      ch1_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxchbondo(4 downto 0),
      ch1_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxclkcorcnt(1 downto 0),
      ch1_rxcominitdet => pcie_versal_0_phy_GT_RX1_ch_rxcominitdet,
      ch1_rxcommadet => pcie_versal_0_phy_GT_RX1_ch_rxcommadet,
      ch1_rxcomsasdet => pcie_versal_0_phy_GT_RX1_ch_rxcomsasdet,
      ch1_rxcomwakedet => pcie_versal_0_phy_GT_RX1_ch_rxcomwakedet,
      ch1_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxctrl0(15 downto 0),
      ch1_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxctrl1(15 downto 0),
      ch1_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxctrl2(7 downto 0),
      ch1_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxctrl3(7 downto 0),
      ch1_rxdapicodeovrden => '0',
      ch1_rxdapicodereset => '0',
      ch1_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxdata(127 downto 0),
      ch1_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxdataextendrsvd(7 downto 0),
      ch1_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxdatavalid(1 downto 0),
      ch1_rxdccdone => pcie_versal_0_phy_GT_RX1_ch_rxdccdone,
      ch1_rxdlyalignerr => pcie_versal_0_phy_GT_RX1_ch_rxdlyalignerr,
      ch1_rxdlyalignprog => pcie_versal_0_phy_GT_RX1_ch_rxdlyalignprog,
      ch1_rxdlyalignreq => '0',
      ch1_rxelecidle => pcie_versal_0_phy_GT_RX1_ch_rxelecidle,
      ch1_rxeqtraining => '0',
      ch1_rxfinealigndone => pcie_versal_0_phy_GT_RX1_ch_rxfinealigndone,
      ch1_rxgearboxslip => '0',
      ch1_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxheader(5 downto 0),
      ch1_rxheadervalid(1 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch1_rxheadervalid_UNCONNECTED(1 downto 0),
      ch1_rxlatclk => '0',
      ch1_rxlpmen => '0',
      ch1_rxmldchaindone => '0',
      ch1_rxmldchainreq => '0',
      ch1_rxmlfinealignreq => '0',
      ch1_rxmstreset => '0',
      ch1_rxmstresetdone => pcie_versal_0_phy_GT_RX1_ch_rxmstresetdone,
      ch1_rxoobreset => '0',
      ch1_rxosintdone => pcie_versal_0_phy_GT_RX1_ch_rxosintdone,
      ch1_rxosintstarted => pcie_versal_0_phy_GT_RX1_ch_rxosintstarted,
      ch1_rxosintstrobedone => pcie_versal_0_phy_GT_RX1_ch_rxosintstrobedone,
      ch1_rxosintstrobestarted => pcie_versal_0_phy_GT_RX1_ch_rxosintstrobestarted,
      ch1_rxoutclk => NLW_pcie_versal_0_gt_quad_0_ch1_rxoutclk_UNCONNECTED,
      ch1_rxpcsresetmask(4 downto 0) => B"11111",
      ch1_rxpd(1 downto 0) => B"00",
      ch1_rxphaligndone => NLW_pcie_versal_0_gt_quad_0_ch1_rxphaligndone_UNCONNECTED,
      ch1_rxphalignerr => pcie_versal_0_phy_GT_RX1_ch_rxphalignerr,
      ch1_rxphalignreq => '0',
      ch1_rxphalignresetmask(1 downto 0) => B"11",
      ch1_rxphdlypd => '0',
      ch1_rxphdlyreset => '0',
      ch1_rxphdlyresetdone => pcie_versal_0_phy_GT_RX1_ch_rxphdlyresetdone,
      ch1_rxphsetinitdone => pcie_versal_0_phy_GT_RX1_ch_rxphsetinitdone,
      ch1_rxphsetinitreq => '0',
      ch1_rxphshift180 => '0',
      ch1_rxphshift180done => pcie_versal_0_phy_GT_RX1_ch_rxphshift180done,
      ch1_rxpmaresetdone => pcie_versal_0_phy_GT_RX1_ch_rxpmaresetdone,
      ch1_rxpmaresetmask(6 downto 0) => B"1111111",
      ch1_rxpolarity => pcie_versal_0_phy_GT_RX1_ch_rxpolarity,
      ch1_rxprbscntreset => '0',
      ch1_rxprbserr => pcie_versal_0_phy_GT_RX1_ch_rxprbserr,
      ch1_rxprbslocked => pcie_versal_0_phy_GT_RX1_ch_rxprbslocked,
      ch1_rxprbssel(3 downto 0) => B"0000",
      ch1_rxprogdivreset => '0',
      ch1_rxprogdivresetdone => pcie_versal_0_phy_GT_RX1_ch_rxprogdivresetdone,
      ch1_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxrate(7 downto 0),
      ch1_rxresetdone => pcie_versal_0_phy_GT_RX1_ch_rxresetdone,
      ch1_rxresetmode(1 downto 0) => B"00",
      ch1_rxslide => '0',
      ch1_rxsliderdy => pcie_versal_0_phy_GT_RX1_ch_rxsliderdy,
      ch1_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxstartofseq(1 downto 0),
      ch1_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxstatus(2 downto 0),
      ch1_rxsyncallin => '0',
      ch1_rxsyncdone => pcie_versal_0_phy_GT_RX1_ch_rxsyncdone,
      ch1_rxtermination => pcie_versal_0_phy_GT_RX1_ch_rxtermination,
      ch1_rxuserrdy => pcie_versal_0_phy_GT_RX1_ch_rxuserrdy,
      ch1_rxusrclk => pcie_versal_0_phy_phy_pclk,
      ch1_rxvalid => pcie_versal_0_phy_GT_RX1_ch_rxvalid,
      ch1_tstin(19 downto 0) => B"00000000000000000000",
      ch1_tx10gstat => pcie_versal_0_phy_GT_TX1_ch_tx10gstat,
      ch1_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txbufstatus(1 downto 0),
      ch1_txcomfinish => pcie_versal_0_phy_GT_TX1_ch_txcomfinish,
      ch1_txcominit => '0',
      ch1_txcomsas => '0',
      ch1_txcomwake => '0',
      ch1_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txctrl0(15 downto 0),
      ch1_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txctrl1(15 downto 0),
      ch1_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txctrl2(7 downto 0),
      ch1_txdapicodeovrden => '0',
      ch1_txdapicodereset => '0',
      ch1_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txdata(127 downto 0),
      ch1_txdataextendrsvd(7 downto 0) => B"00000000",
      ch1_txdccdone => pcie_versal_0_phy_GT_TX1_ch_txdccdone,
      ch1_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txdeemph(1 downto 0),
      ch1_txdetectrx => pcie_versal_0_phy_GT_TX1_ch_txdetectrx,
      ch1_txdiffctrl(4 downto 0) => B"11001",
      ch1_txdlyalignerr => pcie_versal_0_phy_GT_TX1_ch_txdlyalignerr,
      ch1_txdlyalignprog => pcie_versal_0_phy_GT_TX1_ch_txdlyalignprog,
      ch1_txdlyalignreq => '0',
      ch1_txelecidle => pcie_versal_0_phy_GT_TX1_ch_txelecidle,
      ch1_txheader(5 downto 0) => B"000000",
      ch1_txinhibit => '0',
      ch1_txlatclk => '0',
      ch1_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txmaincursor(6 downto 0),
      ch1_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txmargin(2 downto 0),
      ch1_txmldchaindone => '0',
      ch1_txmldchainreq => '0',
      ch1_txmstreset => '0',
      ch1_txmstresetdone => pcie_versal_0_phy_GT_TX1_ch_txmstresetdone,
      ch1_txoneszeros => '0',
      ch1_txoutclk => NLW_pcie_versal_0_gt_quad_0_ch1_txoutclk_UNCONNECTED,
      ch1_txpausedelayalign => '0',
      ch1_txpcsresetmask => '1',
      ch1_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txpd(1 downto 0),
      ch1_txphaligndone => pcie_versal_0_phy_GT_TX1_ch_txphaligndone,
      ch1_txphalignerr => pcie_versal_0_phy_GT_TX1_ch_txphalignerr,
      ch1_txphalignoutrsvd => pcie_versal_0_phy_GT_TX1_ch_txphalignoutrsvd,
      ch1_txphalignreq => '0',
      ch1_txphalignresetmask(1 downto 0) => B"11",
      ch1_txphdlypd => '0',
      ch1_txphdlyreset => '0',
      ch1_txphdlyresetdone => pcie_versal_0_phy_GT_TX1_ch_txphdlyresetdone,
      ch1_txphdlytstclk => '0',
      ch1_txphsetinitdone => pcie_versal_0_phy_GT_TX1_ch_txphsetinitdone,
      ch1_txphsetinitreq => '0',
      ch1_txphshift180 => '0',
      ch1_txphshift180done => pcie_versal_0_phy_GT_TX1_ch_txphshift180done,
      ch1_txpicodeovrden => '0',
      ch1_txpicodereset => '0',
      ch1_txpippmen => '0',
      ch1_txpippmstepsize(4 downto 0) => B"00000",
      ch1_txpisopd => '0',
      ch1_txpmaresetdone => pcie_versal_0_phy_GT_TX1_ch_txpmaresetdone,
      ch1_txpmaresetmask(2 downto 0) => B"111",
      ch1_txpolarity => '0',
      ch1_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txpostcursor(4 downto 0),
      ch1_txprbsforceerr => '0',
      ch1_txprbssel(3 downto 0) => B"0000",
      ch1_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txprecursor(4 downto 0),
      ch1_txprogdivreset => '0',
      ch1_txprogdivresetdone => pcie_versal_0_phy_GT_TX1_ch_txprogdivresetdone,
      ch1_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txrate(7 downto 0),
      ch1_txresetdone => pcie_versal_0_phy_GT_TX1_ch_txresetdone,
      ch1_txresetmode(1 downto 0) => B"00",
      ch1_txsequence(6 downto 0) => B"0000000",
      ch1_txswing => pcie_versal_0_phy_GT_TX1_ch_txswing,
      ch1_txsyncallin => '0',
      ch1_txsyncdone => pcie_versal_0_phy_GT_TX1_ch_txsyncdone,
      ch1_txuserrdy => pcie_versal_0_phy_GT_TX1_ch_txuserrdy,
      ch1_txusrclk => pcie_versal_0_phy_phy_pclk,
      ch2_bufgtce => NLW_pcie_versal_0_gt_quad_0_ch2_bufgtce_UNCONNECTED,
      ch2_bufgtcemask(3 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch2_bufgtcemask_UNCONNECTED(3 downto 0),
      ch2_bufgtdiv(11 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch2_bufgtdiv_UNCONNECTED(11 downto 0),
      ch2_bufgtrst => NLW_pcie_versal_0_gt_quad_0_ch2_bufgtrst_UNCONNECTED,
      ch2_bufgtrstmask(3 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch2_bufgtrstmask_UNCONNECTED(3 downto 0),
      ch2_cdrbmcdrreq => '0',
      ch2_cdrfreqos => '0',
      ch2_cdrincpctrl => '0',
      ch2_cdrstepdir => '0',
      ch2_cdrstepsq => '0',
      ch2_cdrstepsx => '0',
      ch2_cfokovrdfinish => '0',
      ch2_cfokovrdpulse => '0',
      ch2_cfokovrdrdy0 => NLW_pcie_versal_0_gt_quad_0_ch2_cfokovrdrdy0_UNCONNECTED,
      ch2_cfokovrdrdy1 => NLW_pcie_versal_0_gt_quad_0_ch2_cfokovrdrdy1_UNCONNECTED,
      ch2_cfokovrdstart => '0',
      ch2_clkrsvd0 => '0',
      ch2_clkrsvd1 => '0',
      ch2_dmonfiforeset => '0',
      ch2_dmonitorclk => '0',
      ch2_dmonitorout(31 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch2_dmonitorout_UNCONNECTED(31 downto 0),
      ch2_dmonitoroutclk => NLW_pcie_versal_0_gt_quad_0_ch2_dmonitoroutclk_UNCONNECTED,
      ch2_eyescandataerror => pcie_versal_0_phy_GT_RX2_ch_eyescandataerror,
      ch2_eyescanreset => '0',
      ch2_eyescantrigger => '0',
      ch2_gtrsvd(15 downto 0) => B"0000000000000000",
      ch2_gtrxreset => '0',
      ch2_gttxreset => '0',
      ch2_hsdppcsreset => '0',
      ch2_iloreset => '0',
      ch2_iloresetdone => NLW_pcie_versal_0_gt_quad_0_ch2_iloresetdone_UNCONNECTED,
      ch2_iloresetmask => '1',
      ch2_loopback(2 downto 0) => B"000",
      ch2_pcierstb => pcie_versal_0_phy_pcierstb,
      ch2_pcsrsvdin(15 downto 0) => B"0000001001000000",
      ch2_pcsrsvdout(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch2_pcsrsvdout_UNCONNECTED(15 downto 0),
      ch2_phyesmadaptsave => '0',
      ch2_phyready => pcie_versal_0_gt_quad_0_ch2_phyready,
      ch2_phystatus => pcie_versal_0_gt_quad_0_ch2_phystatus,
      ch2_pinrsvdas(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch2_pinrsvdas_UNCONNECTED(15 downto 0),
      ch2_resetexception => NLW_pcie_versal_0_gt_quad_0_ch2_resetexception_UNCONNECTED,
      ch2_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rx10gstat(7 downto 0),
      ch2_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxbufstatus(2 downto 0),
      ch2_rxbyteisaligned => pcie_versal_0_phy_GT_RX2_ch_rxbyteisaligned,
      ch2_rxbyterealign => pcie_versal_0_phy_GT_RX2_ch_rxbyterealign,
      ch2_rxcdrhold => '0',
      ch2_rxcdrlock => pcie_versal_0_phy_GT_RX2_ch_rxcdrlock,
      ch2_rxcdrovrden => '0',
      ch2_rxcdrphdone => pcie_versal_0_phy_GT_RX2_ch_rxcdrphdone,
      ch2_rxcdrreset => '0',
      ch2_rxchanbondseq => pcie_versal_0_phy_GT_RX2_ch_rxchanbondseq,
      ch2_rxchanisaligned => pcie_versal_0_phy_GT_RX2_ch_rxchanisaligned,
      ch2_rxchanrealign => pcie_versal_0_phy_GT_RX2_ch_rxchanrealign,
      ch2_rxchbondi(4 downto 0) => B"00000",
      ch2_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxchbondo(4 downto 0),
      ch2_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxclkcorcnt(1 downto 0),
      ch2_rxcominitdet => pcie_versal_0_phy_GT_RX2_ch_rxcominitdet,
      ch2_rxcommadet => pcie_versal_0_phy_GT_RX2_ch_rxcommadet,
      ch2_rxcomsasdet => pcie_versal_0_phy_GT_RX2_ch_rxcomsasdet,
      ch2_rxcomwakedet => pcie_versal_0_phy_GT_RX2_ch_rxcomwakedet,
      ch2_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxctrl0(15 downto 0),
      ch2_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxctrl1(15 downto 0),
      ch2_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxctrl2(7 downto 0),
      ch2_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxctrl3(7 downto 0),
      ch2_rxdapicodeovrden => '0',
      ch2_rxdapicodereset => '0',
      ch2_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxdata(127 downto 0),
      ch2_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxdataextendrsvd(7 downto 0),
      ch2_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxdatavalid(1 downto 0),
      ch2_rxdccdone => pcie_versal_0_phy_GT_RX2_ch_rxdccdone,
      ch2_rxdlyalignerr => pcie_versal_0_phy_GT_RX2_ch_rxdlyalignerr,
      ch2_rxdlyalignprog => pcie_versal_0_phy_GT_RX2_ch_rxdlyalignprog,
      ch2_rxdlyalignreq => '0',
      ch2_rxelecidle => pcie_versal_0_phy_GT_RX2_ch_rxelecidle,
      ch2_rxeqtraining => '0',
      ch2_rxfinealigndone => pcie_versal_0_phy_GT_RX2_ch_rxfinealigndone,
      ch2_rxgearboxslip => '0',
      ch2_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxheader(5 downto 0),
      ch2_rxheadervalid(1 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch2_rxheadervalid_UNCONNECTED(1 downto 0),
      ch2_rxlatclk => '0',
      ch2_rxlpmen => '0',
      ch2_rxmldchaindone => '0',
      ch2_rxmldchainreq => '0',
      ch2_rxmlfinealignreq => '0',
      ch2_rxmstreset => '0',
      ch2_rxmstresetdone => pcie_versal_0_phy_GT_RX2_ch_rxmstresetdone,
      ch2_rxoobreset => '0',
      ch2_rxosintdone => pcie_versal_0_phy_GT_RX2_ch_rxosintdone,
      ch2_rxosintstarted => pcie_versal_0_phy_GT_RX2_ch_rxosintstarted,
      ch2_rxosintstrobedone => pcie_versal_0_phy_GT_RX2_ch_rxosintstrobedone,
      ch2_rxosintstrobestarted => pcie_versal_0_phy_GT_RX2_ch_rxosintstrobestarted,
      ch2_rxoutclk => NLW_pcie_versal_0_gt_quad_0_ch2_rxoutclk_UNCONNECTED,
      ch2_rxpcsresetmask(4 downto 0) => B"11111",
      ch2_rxpd(1 downto 0) => B"00",
      ch2_rxphaligndone => NLW_pcie_versal_0_gt_quad_0_ch2_rxphaligndone_UNCONNECTED,
      ch2_rxphalignerr => pcie_versal_0_phy_GT_RX2_ch_rxphalignerr,
      ch2_rxphalignreq => '0',
      ch2_rxphalignresetmask(1 downto 0) => B"11",
      ch2_rxphdlypd => '0',
      ch2_rxphdlyreset => '0',
      ch2_rxphdlyresetdone => pcie_versal_0_phy_GT_RX2_ch_rxphdlyresetdone,
      ch2_rxphsetinitdone => pcie_versal_0_phy_GT_RX2_ch_rxphsetinitdone,
      ch2_rxphsetinitreq => '0',
      ch2_rxphshift180 => '0',
      ch2_rxphshift180done => pcie_versal_0_phy_GT_RX2_ch_rxphshift180done,
      ch2_rxpmaresetdone => pcie_versal_0_phy_GT_RX2_ch_rxpmaresetdone,
      ch2_rxpmaresetmask(6 downto 0) => B"1111111",
      ch2_rxpolarity => pcie_versal_0_phy_GT_RX2_ch_rxpolarity,
      ch2_rxprbscntreset => '0',
      ch2_rxprbserr => pcie_versal_0_phy_GT_RX2_ch_rxprbserr,
      ch2_rxprbslocked => pcie_versal_0_phy_GT_RX2_ch_rxprbslocked,
      ch2_rxprbssel(3 downto 0) => B"0000",
      ch2_rxprogdivreset => '0',
      ch2_rxprogdivresetdone => pcie_versal_0_phy_GT_RX2_ch_rxprogdivresetdone,
      ch2_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxrate(7 downto 0),
      ch2_rxresetdone => pcie_versal_0_phy_GT_RX2_ch_rxresetdone,
      ch2_rxresetmode(1 downto 0) => B"00",
      ch2_rxslide => '0',
      ch2_rxsliderdy => pcie_versal_0_phy_GT_RX2_ch_rxsliderdy,
      ch2_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxstartofseq(1 downto 0),
      ch2_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxstatus(2 downto 0),
      ch2_rxsyncallin => '0',
      ch2_rxsyncdone => pcie_versal_0_phy_GT_RX2_ch_rxsyncdone,
      ch2_rxtermination => pcie_versal_0_phy_GT_RX2_ch_rxtermination,
      ch2_rxuserrdy => pcie_versal_0_phy_GT_RX2_ch_rxuserrdy,
      ch2_rxusrclk => pcie_versal_0_phy_phy_pclk,
      ch2_rxvalid => pcie_versal_0_phy_GT_RX2_ch_rxvalid,
      ch2_tstin(19 downto 0) => B"00000000000000000000",
      ch2_tx10gstat => pcie_versal_0_phy_GT_TX2_ch_tx10gstat,
      ch2_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txbufstatus(1 downto 0),
      ch2_txcomfinish => pcie_versal_0_phy_GT_TX2_ch_txcomfinish,
      ch2_txcominit => '0',
      ch2_txcomsas => '0',
      ch2_txcomwake => '0',
      ch2_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txctrl0(15 downto 0),
      ch2_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txctrl1(15 downto 0),
      ch2_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txctrl2(7 downto 0),
      ch2_txdapicodeovrden => '0',
      ch2_txdapicodereset => '0',
      ch2_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txdata(127 downto 0),
      ch2_txdataextendrsvd(7 downto 0) => B"00000000",
      ch2_txdccdone => pcie_versal_0_phy_GT_TX2_ch_txdccdone,
      ch2_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txdeemph(1 downto 0),
      ch2_txdetectrx => pcie_versal_0_phy_GT_TX2_ch_txdetectrx,
      ch2_txdiffctrl(4 downto 0) => B"11001",
      ch2_txdlyalignerr => pcie_versal_0_phy_GT_TX2_ch_txdlyalignerr,
      ch2_txdlyalignprog => pcie_versal_0_phy_GT_TX2_ch_txdlyalignprog,
      ch2_txdlyalignreq => '0',
      ch2_txelecidle => pcie_versal_0_phy_GT_TX2_ch_txelecidle,
      ch2_txheader(5 downto 0) => B"000000",
      ch2_txinhibit => '0',
      ch2_txlatclk => '0',
      ch2_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txmaincursor(6 downto 0),
      ch2_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txmargin(2 downto 0),
      ch2_txmldchaindone => '0',
      ch2_txmldchainreq => '0',
      ch2_txmstreset => '0',
      ch2_txmstresetdone => pcie_versal_0_phy_GT_TX2_ch_txmstresetdone,
      ch2_txoneszeros => '0',
      ch2_txoutclk => NLW_pcie_versal_0_gt_quad_0_ch2_txoutclk_UNCONNECTED,
      ch2_txpausedelayalign => '0',
      ch2_txpcsresetmask => '1',
      ch2_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txpd(1 downto 0),
      ch2_txphaligndone => pcie_versal_0_phy_GT_TX2_ch_txphaligndone,
      ch2_txphalignerr => pcie_versal_0_phy_GT_TX2_ch_txphalignerr,
      ch2_txphalignoutrsvd => pcie_versal_0_phy_GT_TX2_ch_txphalignoutrsvd,
      ch2_txphalignreq => '0',
      ch2_txphalignresetmask(1 downto 0) => B"11",
      ch2_txphdlypd => '0',
      ch2_txphdlyreset => '0',
      ch2_txphdlyresetdone => pcie_versal_0_phy_GT_TX2_ch_txphdlyresetdone,
      ch2_txphdlytstclk => '0',
      ch2_txphsetinitdone => pcie_versal_0_phy_GT_TX2_ch_txphsetinitdone,
      ch2_txphsetinitreq => '0',
      ch2_txphshift180 => '0',
      ch2_txphshift180done => pcie_versal_0_phy_GT_TX2_ch_txphshift180done,
      ch2_txpicodeovrden => '0',
      ch2_txpicodereset => '0',
      ch2_txpippmen => '0',
      ch2_txpippmstepsize(4 downto 0) => B"00000",
      ch2_txpisopd => '0',
      ch2_txpmaresetdone => pcie_versal_0_phy_GT_TX2_ch_txpmaresetdone,
      ch2_txpmaresetmask(2 downto 0) => B"111",
      ch2_txpolarity => '0',
      ch2_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txpostcursor(4 downto 0),
      ch2_txprbsforceerr => '0',
      ch2_txprbssel(3 downto 0) => B"0000",
      ch2_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txprecursor(4 downto 0),
      ch2_txprogdivreset => '0',
      ch2_txprogdivresetdone => pcie_versal_0_phy_GT_TX2_ch_txprogdivresetdone,
      ch2_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txrate(7 downto 0),
      ch2_txresetdone => pcie_versal_0_phy_GT_TX2_ch_txresetdone,
      ch2_txresetmode(1 downto 0) => B"00",
      ch2_txsequence(6 downto 0) => B"0000000",
      ch2_txswing => pcie_versal_0_phy_GT_TX2_ch_txswing,
      ch2_txsyncallin => '0',
      ch2_txsyncdone => pcie_versal_0_phy_GT_TX2_ch_txsyncdone,
      ch2_txuserrdy => pcie_versal_0_phy_GT_TX2_ch_txuserrdy,
      ch2_txusrclk => pcie_versal_0_phy_phy_pclk,
      ch3_bufgtce => NLW_pcie_versal_0_gt_quad_0_ch3_bufgtce_UNCONNECTED,
      ch3_bufgtcemask(3 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch3_bufgtcemask_UNCONNECTED(3 downto 0),
      ch3_bufgtdiv(11 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch3_bufgtdiv_UNCONNECTED(11 downto 0),
      ch3_bufgtrst => NLW_pcie_versal_0_gt_quad_0_ch3_bufgtrst_UNCONNECTED,
      ch3_bufgtrstmask(3 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch3_bufgtrstmask_UNCONNECTED(3 downto 0),
      ch3_cdrbmcdrreq => '0',
      ch3_cdrfreqos => '0',
      ch3_cdrincpctrl => '0',
      ch3_cdrstepdir => '0',
      ch3_cdrstepsq => '0',
      ch3_cdrstepsx => '0',
      ch3_cfokovrdfinish => '0',
      ch3_cfokovrdpulse => '0',
      ch3_cfokovrdrdy0 => NLW_pcie_versal_0_gt_quad_0_ch3_cfokovrdrdy0_UNCONNECTED,
      ch3_cfokovrdrdy1 => NLW_pcie_versal_0_gt_quad_0_ch3_cfokovrdrdy1_UNCONNECTED,
      ch3_cfokovrdstart => '0',
      ch3_clkrsvd0 => '0',
      ch3_clkrsvd1 => '0',
      ch3_dmonfiforeset => '0',
      ch3_dmonitorclk => '0',
      ch3_dmonitorout(31 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch3_dmonitorout_UNCONNECTED(31 downto 0),
      ch3_dmonitoroutclk => NLW_pcie_versal_0_gt_quad_0_ch3_dmonitoroutclk_UNCONNECTED,
      ch3_eyescandataerror => pcie_versal_0_phy_GT_RX3_ch_eyescandataerror,
      ch3_eyescanreset => '0',
      ch3_eyescantrigger => '0',
      ch3_gtrsvd(15 downto 0) => B"0000000000000000",
      ch3_gtrxreset => '0',
      ch3_gttxreset => '0',
      ch3_hsdppcsreset => '0',
      ch3_iloreset => '0',
      ch3_iloresetdone => NLW_pcie_versal_0_gt_quad_0_ch3_iloresetdone_UNCONNECTED,
      ch3_iloresetmask => '1',
      ch3_loopback(2 downto 0) => B"000",
      ch3_pcierstb => pcie_versal_0_phy_pcierstb,
      ch3_pcsrsvdin(15 downto 0) => B"0000001001000000",
      ch3_pcsrsvdout(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch3_pcsrsvdout_UNCONNECTED(15 downto 0),
      ch3_phyesmadaptsave => '0',
      ch3_phyready => pcie_versal_0_gt_quad_0_ch3_phyready,
      ch3_phystatus => pcie_versal_0_gt_quad_0_ch3_phystatus,
      ch3_pinrsvdas(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch3_pinrsvdas_UNCONNECTED(15 downto 0),
      ch3_resetexception => NLW_pcie_versal_0_gt_quad_0_ch3_resetexception_UNCONNECTED,
      ch3_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rx10gstat(7 downto 0),
      ch3_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxbufstatus(2 downto 0),
      ch3_rxbyteisaligned => pcie_versal_0_phy_GT_RX3_ch_rxbyteisaligned,
      ch3_rxbyterealign => pcie_versal_0_phy_GT_RX3_ch_rxbyterealign,
      ch3_rxcdrhold => '0',
      ch3_rxcdrlock => pcie_versal_0_phy_GT_RX3_ch_rxcdrlock,
      ch3_rxcdrovrden => '0',
      ch3_rxcdrphdone => pcie_versal_0_phy_GT_RX3_ch_rxcdrphdone,
      ch3_rxcdrreset => '0',
      ch3_rxchanbondseq => pcie_versal_0_phy_GT_RX3_ch_rxchanbondseq,
      ch3_rxchanisaligned => pcie_versal_0_phy_GT_RX3_ch_rxchanisaligned,
      ch3_rxchanrealign => pcie_versal_0_phy_GT_RX3_ch_rxchanrealign,
      ch3_rxchbondi(4 downto 0) => B"00000",
      ch3_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxchbondo(4 downto 0),
      ch3_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxclkcorcnt(1 downto 0),
      ch3_rxcominitdet => pcie_versal_0_phy_GT_RX3_ch_rxcominitdet,
      ch3_rxcommadet => pcie_versal_0_phy_GT_RX3_ch_rxcommadet,
      ch3_rxcomsasdet => pcie_versal_0_phy_GT_RX3_ch_rxcomsasdet,
      ch3_rxcomwakedet => pcie_versal_0_phy_GT_RX3_ch_rxcomwakedet,
      ch3_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxctrl0(15 downto 0),
      ch3_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxctrl1(15 downto 0),
      ch3_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxctrl2(7 downto 0),
      ch3_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxctrl3(7 downto 0),
      ch3_rxdapicodeovrden => '0',
      ch3_rxdapicodereset => '0',
      ch3_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxdata(127 downto 0),
      ch3_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxdataextendrsvd(7 downto 0),
      ch3_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxdatavalid(1 downto 0),
      ch3_rxdccdone => pcie_versal_0_phy_GT_RX3_ch_rxdccdone,
      ch3_rxdlyalignerr => pcie_versal_0_phy_GT_RX3_ch_rxdlyalignerr,
      ch3_rxdlyalignprog => pcie_versal_0_phy_GT_RX3_ch_rxdlyalignprog,
      ch3_rxdlyalignreq => '0',
      ch3_rxelecidle => pcie_versal_0_phy_GT_RX3_ch_rxelecidle,
      ch3_rxeqtraining => '0',
      ch3_rxfinealigndone => pcie_versal_0_phy_GT_RX3_ch_rxfinealigndone,
      ch3_rxgearboxslip => '0',
      ch3_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxheader(5 downto 0),
      ch3_rxheadervalid(1 downto 0) => NLW_pcie_versal_0_gt_quad_0_ch3_rxheadervalid_UNCONNECTED(1 downto 0),
      ch3_rxlatclk => '0',
      ch3_rxlpmen => '0',
      ch3_rxmldchaindone => '0',
      ch3_rxmldchainreq => '0',
      ch3_rxmlfinealignreq => '0',
      ch3_rxmstreset => '0',
      ch3_rxmstresetdone => pcie_versal_0_phy_GT_RX3_ch_rxmstresetdone,
      ch3_rxoobreset => '0',
      ch3_rxosintdone => pcie_versal_0_phy_GT_RX3_ch_rxosintdone,
      ch3_rxosintstarted => pcie_versal_0_phy_GT_RX3_ch_rxosintstarted,
      ch3_rxosintstrobedone => pcie_versal_0_phy_GT_RX3_ch_rxosintstrobedone,
      ch3_rxosintstrobestarted => pcie_versal_0_phy_GT_RX3_ch_rxosintstrobestarted,
      ch3_rxoutclk => NLW_pcie_versal_0_gt_quad_0_ch3_rxoutclk_UNCONNECTED,
      ch3_rxpcsresetmask(4 downto 0) => B"11111",
      ch3_rxpd(1 downto 0) => B"00",
      ch3_rxphaligndone => NLW_pcie_versal_0_gt_quad_0_ch3_rxphaligndone_UNCONNECTED,
      ch3_rxphalignerr => pcie_versal_0_phy_GT_RX3_ch_rxphalignerr,
      ch3_rxphalignreq => '0',
      ch3_rxphalignresetmask(1 downto 0) => B"11",
      ch3_rxphdlypd => '0',
      ch3_rxphdlyreset => '0',
      ch3_rxphdlyresetdone => pcie_versal_0_phy_GT_RX3_ch_rxphdlyresetdone,
      ch3_rxphsetinitdone => pcie_versal_0_phy_GT_RX3_ch_rxphsetinitdone,
      ch3_rxphsetinitreq => '0',
      ch3_rxphshift180 => '0',
      ch3_rxphshift180done => pcie_versal_0_phy_GT_RX3_ch_rxphshift180done,
      ch3_rxpmaresetdone => pcie_versal_0_phy_GT_RX3_ch_rxpmaresetdone,
      ch3_rxpmaresetmask(6 downto 0) => B"1111111",
      ch3_rxpolarity => pcie_versal_0_phy_GT_RX3_ch_rxpolarity,
      ch3_rxprbscntreset => '0',
      ch3_rxprbserr => pcie_versal_0_phy_GT_RX3_ch_rxprbserr,
      ch3_rxprbslocked => pcie_versal_0_phy_GT_RX3_ch_rxprbslocked,
      ch3_rxprbssel(3 downto 0) => B"0000",
      ch3_rxprogdivreset => '0',
      ch3_rxprogdivresetdone => pcie_versal_0_phy_GT_RX3_ch_rxprogdivresetdone,
      ch3_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxrate(7 downto 0),
      ch3_rxresetdone => pcie_versal_0_phy_GT_RX3_ch_rxresetdone,
      ch3_rxresetmode(1 downto 0) => B"00",
      ch3_rxslide => '0',
      ch3_rxsliderdy => pcie_versal_0_phy_GT_RX3_ch_rxsliderdy,
      ch3_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxstartofseq(1 downto 0),
      ch3_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxstatus(2 downto 0),
      ch3_rxsyncallin => '0',
      ch3_rxsyncdone => pcie_versal_0_phy_GT_RX3_ch_rxsyncdone,
      ch3_rxtermination => pcie_versal_0_phy_GT_RX3_ch_rxtermination,
      ch3_rxuserrdy => pcie_versal_0_phy_GT_RX3_ch_rxuserrdy,
      ch3_rxusrclk => pcie_versal_0_phy_phy_pclk,
      ch3_rxvalid => pcie_versal_0_phy_GT_RX3_ch_rxvalid,
      ch3_tstin(19 downto 0) => B"00000000000000000000",
      ch3_tx10gstat => pcie_versal_0_phy_GT_TX3_ch_tx10gstat,
      ch3_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txbufstatus(1 downto 0),
      ch3_txcomfinish => pcie_versal_0_phy_GT_TX3_ch_txcomfinish,
      ch3_txcominit => '0',
      ch3_txcomsas => '0',
      ch3_txcomwake => '0',
      ch3_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txctrl0(15 downto 0),
      ch3_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txctrl1(15 downto 0),
      ch3_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txctrl2(7 downto 0),
      ch3_txdapicodeovrden => '0',
      ch3_txdapicodereset => '0',
      ch3_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txdata(127 downto 0),
      ch3_txdataextendrsvd(7 downto 0) => B"00000000",
      ch3_txdccdone => pcie_versal_0_phy_GT_TX3_ch_txdccdone,
      ch3_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txdeemph(1 downto 0),
      ch3_txdetectrx => pcie_versal_0_phy_GT_TX3_ch_txdetectrx,
      ch3_txdiffctrl(4 downto 0) => B"11001",
      ch3_txdlyalignerr => pcie_versal_0_phy_GT_TX3_ch_txdlyalignerr,
      ch3_txdlyalignprog => pcie_versal_0_phy_GT_TX3_ch_txdlyalignprog,
      ch3_txdlyalignreq => '0',
      ch3_txelecidle => pcie_versal_0_phy_GT_TX3_ch_txelecidle,
      ch3_txheader(5 downto 0) => B"000000",
      ch3_txinhibit => '0',
      ch3_txlatclk => '0',
      ch3_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txmaincursor(6 downto 0),
      ch3_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txmargin(2 downto 0),
      ch3_txmldchaindone => '0',
      ch3_txmldchainreq => '0',
      ch3_txmstreset => '0',
      ch3_txmstresetdone => pcie_versal_0_phy_GT_TX3_ch_txmstresetdone,
      ch3_txoneszeros => '0',
      ch3_txoutclk => NLW_pcie_versal_0_gt_quad_0_ch3_txoutclk_UNCONNECTED,
      ch3_txpausedelayalign => '0',
      ch3_txpcsresetmask => '1',
      ch3_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txpd(1 downto 0),
      ch3_txphaligndone => pcie_versal_0_phy_GT_TX3_ch_txphaligndone,
      ch3_txphalignerr => pcie_versal_0_phy_GT_TX3_ch_txphalignerr,
      ch3_txphalignoutrsvd => pcie_versal_0_phy_GT_TX3_ch_txphalignoutrsvd,
      ch3_txphalignreq => '0',
      ch3_txphalignresetmask(1 downto 0) => B"11",
      ch3_txphdlypd => '0',
      ch3_txphdlyreset => '0',
      ch3_txphdlyresetdone => pcie_versal_0_phy_GT_TX3_ch_txphdlyresetdone,
      ch3_txphdlytstclk => '0',
      ch3_txphsetinitdone => pcie_versal_0_phy_GT_TX3_ch_txphsetinitdone,
      ch3_txphsetinitreq => '0',
      ch3_txphshift180 => '0',
      ch3_txphshift180done => pcie_versal_0_phy_GT_TX3_ch_txphshift180done,
      ch3_txpicodeovrden => '0',
      ch3_txpicodereset => '0',
      ch3_txpippmen => '0',
      ch3_txpippmstepsize(4 downto 0) => B"00000",
      ch3_txpisopd => '0',
      ch3_txpmaresetdone => pcie_versal_0_phy_GT_TX3_ch_txpmaresetdone,
      ch3_txpmaresetmask(2 downto 0) => B"111",
      ch3_txpolarity => '0',
      ch3_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txpostcursor(4 downto 0),
      ch3_txprbsforceerr => '0',
      ch3_txprbssel(3 downto 0) => B"0000",
      ch3_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txprecursor(4 downto 0),
      ch3_txprogdivreset => '0',
      ch3_txprogdivresetdone => pcie_versal_0_phy_GT_TX3_ch_txprogdivresetdone,
      ch3_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txrate(7 downto 0),
      ch3_txresetdone => pcie_versal_0_phy_GT_TX3_ch_txresetdone,
      ch3_txresetmode(1 downto 0) => B"00",
      ch3_txsequence(6 downto 0) => B"0000000",
      ch3_txswing => pcie_versal_0_phy_GT_TX3_ch_txswing,
      ch3_txsyncallin => '0',
      ch3_txsyncdone => pcie_versal_0_phy_GT_TX3_ch_txsyncdone,
      ch3_txuserrdy => pcie_versal_0_phy_GT_TX3_ch_txuserrdy,
      ch3_txusrclk => pcie_versal_0_phy_phy_pclk,
      correcterr => NLW_pcie_versal_0_gt_quad_0_correcterr_UNCONNECTED,
      ctrlrsvdin0(15 downto 0) => B"0000000000000000",
      ctrlrsvdin1(13 downto 0) => B"00000000000000",
      ctrlrsvdout(31 downto 0) => NLW_pcie_versal_0_gt_quad_0_ctrlrsvdout_UNCONNECTED(31 downto 0),
      debugtraceclk => '0',
      debugtraceready => '0',
      debugtracetdata(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_debugtracetdata_UNCONNECTED(15 downto 0),
      debugtracetvalid => NLW_pcie_versal_0_gt_quad_0_debugtracetvalid_UNCONNECTED,
      gpi(15 downto 0) => B"0000000000000000",
      gpo(15 downto 0) => NLW_pcie_versal_0_gt_quad_0_gpo_UNCONNECTED(15 downto 0),
      gtpowergood => NLW_pcie_versal_0_gt_quad_0_gtpowergood_UNCONNECTED,
      hsclk0_lcpllclkrsvd0 => '0',
      hsclk0_lcpllclkrsvd1 => '0',
      hsclk0_lcpllfbclklost => NLW_pcie_versal_0_gt_quad_0_hsclk0_lcpllfbclklost_UNCONNECTED,
      hsclk0_lcpllfbdiv(7 downto 0) => B"00000000",
      hsclk0_lcplllock => NLW_pcie_versal_0_gt_quad_0_hsclk0_lcplllock_UNCONNECTED,
      hsclk0_lcpllpd => '0',
      hsclk0_lcpllrefclklost => NLW_pcie_versal_0_gt_quad_0_hsclk0_lcpllrefclklost_UNCONNECTED,
      hsclk0_lcpllrefclkmonitor => NLW_pcie_versal_0_gt_quad_0_hsclk0_lcpllrefclkmonitor_UNCONNECTED,
      hsclk0_lcpllrefclksel(2 downto 0) => B"000",
      hsclk0_lcpllreset => '0',
      hsclk0_lcpllresetbypassmode => '0',
      hsclk0_lcpllresetmask(1 downto 0) => B"11",
      hsclk0_lcpllrsvd0(7 downto 0) => B"00000000",
      hsclk0_lcpllrsvd1(7 downto 0) => B"00000000",
      hsclk0_lcpllrsvdout(7 downto 0) => NLW_pcie_versal_0_gt_quad_0_hsclk0_lcpllrsvdout_UNCONNECTED(7 downto 0),
      hsclk0_lcpllsdmdata(25 downto 0) => B"01000111011100101111000001",
      hsclk0_lcpllsdmtoggle => '0',
      hsclk0_rpllclkrsvd0 => '0',
      hsclk0_rpllclkrsvd1 => '0',
      hsclk0_rpllfbclklost => NLW_pcie_versal_0_gt_quad_0_hsclk0_rpllfbclklost_UNCONNECTED,
      hsclk0_rpllfbdiv(7 downto 0) => B"00000000",
      hsclk0_rplllock => NLW_pcie_versal_0_gt_quad_0_hsclk0_rplllock_UNCONNECTED,
      hsclk0_rpllpd => '0',
      hsclk0_rpllrefclklost => NLW_pcie_versal_0_gt_quad_0_hsclk0_rpllrefclklost_UNCONNECTED,
      hsclk0_rpllrefclkmonitor => NLW_pcie_versal_0_gt_quad_0_hsclk0_rpllrefclkmonitor_UNCONNECTED,
      hsclk0_rpllrefclksel(2 downto 0) => B"001",
      hsclk0_rpllreset => '0',
      hsclk0_rpllresetbypassmode => '0',
      hsclk0_rpllresetmask(1 downto 0) => B"11",
      hsclk0_rpllrsvd0(7 downto 0) => B"00000000",
      hsclk0_rpllrsvd1(7 downto 0) => B"00000000",
      hsclk0_rpllrsvdout(7 downto 0) => NLW_pcie_versal_0_gt_quad_0_hsclk0_rpllrsvdout_UNCONNECTED(7 downto 0),
      hsclk0_rpllsdmdata(25 downto 0) => B"00010001010011010001000000",
      hsclk0_rpllsdmtoggle => '0',
      hsclk0_rxrecclkout0 => NLW_pcie_versal_0_gt_quad_0_hsclk0_rxrecclkout0_UNCONNECTED,
      hsclk0_rxrecclkout1 => NLW_pcie_versal_0_gt_quad_0_hsclk0_rxrecclkout1_UNCONNECTED,
      hsclk1_lcpllclkrsvd0 => '0',
      hsclk1_lcpllclkrsvd1 => '0',
      hsclk1_lcpllfbclklost => NLW_pcie_versal_0_gt_quad_0_hsclk1_lcpllfbclklost_UNCONNECTED,
      hsclk1_lcpllfbdiv(7 downto 0) => B"00000000",
      hsclk1_lcplllock => NLW_pcie_versal_0_gt_quad_0_hsclk1_lcplllock_UNCONNECTED,
      hsclk1_lcpllpd => '0',
      hsclk1_lcpllrefclklost => NLW_pcie_versal_0_gt_quad_0_hsclk1_lcpllrefclklost_UNCONNECTED,
      hsclk1_lcpllrefclkmonitor => NLW_pcie_versal_0_gt_quad_0_hsclk1_lcpllrefclkmonitor_UNCONNECTED,
      hsclk1_lcpllrefclksel(2 downto 0) => B"000",
      hsclk1_lcpllreset => '0',
      hsclk1_lcpllresetbypassmode => '0',
      hsclk1_lcpllresetmask(1 downto 0) => B"11",
      hsclk1_lcpllrsvd0(7 downto 0) => B"00000000",
      hsclk1_lcpllrsvd1(7 downto 0) => B"00000000",
      hsclk1_lcpllrsvdout(7 downto 0) => NLW_pcie_versal_0_gt_quad_0_hsclk1_lcpllrsvdout_UNCONNECTED(7 downto 0),
      hsclk1_lcpllsdmdata(25 downto 0) => B"01000111011100101111000001",
      hsclk1_lcpllsdmtoggle => '0',
      hsclk1_rpllclkrsvd0 => '0',
      hsclk1_rpllclkrsvd1 => '0',
      hsclk1_rpllfbclklost => NLW_pcie_versal_0_gt_quad_0_hsclk1_rpllfbclklost_UNCONNECTED,
      hsclk1_rpllfbdiv(7 downto 0) => B"00000000",
      hsclk1_rplllock => NLW_pcie_versal_0_gt_quad_0_hsclk1_rplllock_UNCONNECTED,
      hsclk1_rpllpd => '0',
      hsclk1_rpllrefclklost => NLW_pcie_versal_0_gt_quad_0_hsclk1_rpllrefclklost_UNCONNECTED,
      hsclk1_rpllrefclkmonitor => NLW_pcie_versal_0_gt_quad_0_hsclk1_rpllrefclkmonitor_UNCONNECTED,
      hsclk1_rpllrefclksel(2 downto 0) => B"001",
      hsclk1_rpllreset => '0',
      hsclk1_rpllresetbypassmode => '0',
      hsclk1_rpllresetmask(1 downto 0) => B"11",
      hsclk1_rpllrsvd0(7 downto 0) => B"00000000",
      hsclk1_rpllrsvd1(7 downto 0) => B"00000000",
      hsclk1_rpllrsvdout(7 downto 0) => NLW_pcie_versal_0_gt_quad_0_hsclk1_rpllrsvdout_UNCONNECTED(7 downto 0),
      hsclk1_rpllsdmdata(25 downto 0) => B"00010001010011010001000000",
      hsclk1_rpllsdmtoggle => '0',
      hsclk1_rxrecclkout0 => NLW_pcie_versal_0_gt_quad_0_hsclk1_rxrecclkout0_UNCONNECTED,
      hsclk1_rxrecclkout1 => NLW_pcie_versal_0_gt_quad_0_hsclk1_rxrecclkout1_UNCONNECTED,
      m0_axis_tdata(31 downto 0) => NLW_pcie_versal_0_gt_quad_0_m0_axis_tdata_UNCONNECTED(31 downto 0),
      m0_axis_tlast => NLW_pcie_versal_0_gt_quad_0_m0_axis_tlast_UNCONNECTED,
      m0_axis_tready => '1',
      m0_axis_tvalid => NLW_pcie_versal_0_gt_quad_0_m0_axis_tvalid_UNCONNECTED,
      m1_axis_tdata(31 downto 0) => NLW_pcie_versal_0_gt_quad_0_m1_axis_tdata_UNCONNECTED(31 downto 0),
      m1_axis_tlast => NLW_pcie_versal_0_gt_quad_0_m1_axis_tlast_UNCONNECTED,
      m1_axis_tready => '1',
      m1_axis_tvalid => NLW_pcie_versal_0_gt_quad_0_m1_axis_tvalid_UNCONNECTED,
      m2_axis_tdata(31 downto 0) => NLW_pcie_versal_0_gt_quad_0_m2_axis_tdata_UNCONNECTED(31 downto 0),
      m2_axis_tlast => NLW_pcie_versal_0_gt_quad_0_m2_axis_tlast_UNCONNECTED,
      m2_axis_tready => '1',
      m2_axis_tvalid => NLW_pcie_versal_0_gt_quad_0_m2_axis_tvalid_UNCONNECTED,
      pcielinkreachtarget => '0',
      pcieltssm(5 downto 0) => pcie_versal_0_phy_gt_pcieltssm(5 downto 0),
      pipenorthin(5 downto 0) => B"000000",
      pipenorthout(5 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_PIPENORTHIN(5 downto 0),
      pipesouthin(5 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_PIPESOUTHOUT(5 downto 0),
      pipesouthout(5 downto 0) => NLW_pcie_versal_0_gt_quad_0_pipesouthout_UNCONNECTED(5 downto 0),
      rcalenb => '0',
      refclk0_clktestsig => '0',
      refclk0_clktestsigint => NLW_pcie_versal_0_gt_quad_0_refclk0_clktestsigint_UNCONNECTED,
      refclk0_gtrefclkpd => '0',
      refclk0_gtrefclkpdint => NLW_pcie_versal_0_gt_quad_0_refclk0_gtrefclkpdint_UNCONNECTED,
      refclk1_clktestsig => '0',
      refclk1_clktestsigint => NLW_pcie_versal_0_gt_quad_0_refclk1_clktestsigint_UNCONNECTED,
      refclk1_gtrefclkpd => '0',
      refclk1_gtrefclkpdint => NLW_pcie_versal_0_gt_quad_0_refclk1_gtrefclkpdint_UNCONNECTED,
      resetdone_northin(1 downto 0) => B"00",
      resetdone_northout(1 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RESETDONE_NORTHIN(1 downto 0),
      resetdone_southin(1 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RESETDONE_SOUTHOUT(1 downto 0),
      resetdone_southout(1 downto 0) => NLW_pcie_versal_0_gt_quad_0_resetdone_southout_UNCONNECTED(1 downto 0),
      rxmarginclk => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginclk,
      rxmarginreqack => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqack,
      rxmarginreqcmd(3 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqcmd(3 downto 0),
      rxmarginreqlanenum(1 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqlanenum(1 downto 0),
      rxmarginreqpayld(7 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqpayld(7 downto 0),
      rxmarginreqreq => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqreq,
      rxmarginresack => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginresack,
      rxmarginrescmd(3 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginrescmd(3 downto 0),
      rxmarginreslanenum(1 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreslanenum(1 downto 0),
      rxmarginrespayld(7 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginrespayld(7 downto 0),
      rxmarginresreq => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginresreq,
      rxn(3 downto 0) => pcie_versal_0_gt_quad_0_GT_Serial_RX_RXN(3 downto 0),
      rxp(3 downto 0) => pcie_versal_0_gt_quad_0_GT_Serial_RX_RXP(3 downto 0),
      rxpinorthin(3 downto 0) => B"0000",
      rxpinorthout(3 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RXPINORTHIN(3 downto 0),
      rxpisouthin(3 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RXPISOUTHOUT(3 downto 0),
      rxpisouthout(3 downto 0) => NLW_pcie_versal_0_gt_quad_0_rxpisouthout_UNCONNECTED(3 downto 0),
      s0_axis_tdata(31 downto 0) => B"00000000000000000000000000000000",
      s0_axis_tlast => '0',
      s0_axis_tready => NLW_pcie_versal_0_gt_quad_0_s0_axis_tready_UNCONNECTED,
      s0_axis_tvalid => '0',
      s1_axis_tdata(31 downto 0) => B"00000000000000000000000000000000",
      s1_axis_tlast => '0',
      s1_axis_tready => NLW_pcie_versal_0_gt_quad_0_s1_axis_tready_UNCONNECTED,
      s1_axis_tvalid => '0',
      s2_axis_tdata(31 downto 0) => B"00000000000000000000000000000000",
      s2_axis_tlast => '0',
      s2_axis_tready => NLW_pcie_versal_0_gt_quad_0_s2_axis_tready_UNCONNECTED,
      s2_axis_tvalid => '0',
      trigackin0 => NLW_pcie_versal_0_gt_quad_0_trigackin0_UNCONNECTED,
      trigackout0 => '0',
      trigin0 => '0',
      trigout0 => NLW_pcie_versal_0_gt_quad_0_trigout0_UNCONNECTED,
      txn(3 downto 0) => pcie_versal_0_gt_quad_0_GT_Serial_TX_TXN(3 downto 0),
      txp(3 downto 0) => pcie_versal_0_gt_quad_0_GT_Serial_TX_TXP(3 downto 0),
      txpinorthin(3 downto 0) => B"0000",
      txpinorthout(3 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_TXPINORTHIN(3 downto 0),
      txpisouthin(3 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_TXPISOUTHOUT(3 downto 0),
      txpisouthout(3 downto 0) => NLW_pcie_versal_0_gt_quad_0_txpisouthout_UNCONNECTED(3 downto 0),
      ubenable => '1',
      ubinterrupt => NLW_pcie_versal_0_gt_quad_0_ubinterrupt_UNCONNECTED,
      ubintr(11 downto 0) => B"000000000000",
      ubiolmbrst => '0',
      ubmbrst => '0',
      ubrxuart => '0',
      ubtxuart => NLW_pcie_versal_0_gt_quad_0_ubtxuart_UNCONNECTED,
      uncorrecterr => NLW_pcie_versal_0_gt_quad_0_uncorrecterr_UNCONNECTED
    );
pcie_versal_0_gt_quad_1: component pcie_versal_0_support_gt_quad_1
     port map (
      GT_REFCLK0 => pcie_versal_0_phy_gtrefclk,
      altclk => '0',
      apb3clk => sys_clk_1,
      apb3paddr(15 downto 0) => B"0000000000000000",
      apb3penable => '0',
      apb3prdata(31 downto 0) => NLW_pcie_versal_0_gt_quad_1_apb3prdata_UNCONNECTED(31 downto 0),
      apb3pready => NLW_pcie_versal_0_gt_quad_1_apb3pready_UNCONNECTED,
      apb3presetn => '1',
      apb3psel => '0',
      apb3pslverr => NLW_pcie_versal_0_gt_quad_1_apb3pslverr_UNCONNECTED,
      apb3pwdata(31 downto 0) => B"00000000000000000000000000000000",
      apb3pwrite => '0',
      axisclk => sys_clk_1,
      bgbypassb => '0',
      bgmonitorenb => '0',
      bgpdb => '0',
      bgrcalovrd(4 downto 0) => B"00000",
      bgrcalovrdenb => '0',
      ch0_bufgtce => NLW_pcie_versal_0_gt_quad_1_ch0_bufgtce_UNCONNECTED,
      ch0_bufgtcemask(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch0_bufgtcemask_UNCONNECTED(3 downto 0),
      ch0_bufgtdiv(11 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch0_bufgtdiv_UNCONNECTED(11 downto 0),
      ch0_bufgtrst => NLW_pcie_versal_0_gt_quad_1_ch0_bufgtrst_UNCONNECTED,
      ch0_bufgtrstmask(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch0_bufgtrstmask_UNCONNECTED(3 downto 0),
      ch0_cdrbmcdrreq => '0',
      ch0_cdrfreqos => '0',
      ch0_cdrincpctrl => '0',
      ch0_cdrstepdir => '0',
      ch0_cdrstepsq => '0',
      ch0_cdrstepsx => '0',
      ch0_cfokovrdfinish => '0',
      ch0_cfokovrdpulse => '0',
      ch0_cfokovrdrdy0 => NLW_pcie_versal_0_gt_quad_1_ch0_cfokovrdrdy0_UNCONNECTED,
      ch0_cfokovrdrdy1 => NLW_pcie_versal_0_gt_quad_1_ch0_cfokovrdrdy1_UNCONNECTED,
      ch0_cfokovrdstart => '0',
      ch0_clkrsvd0 => '0',
      ch0_clkrsvd1 => '0',
      ch0_dmonfiforeset => '0',
      ch0_dmonitorclk => '0',
      ch0_dmonitorout(31 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch0_dmonitorout_UNCONNECTED(31 downto 0),
      ch0_dmonitoroutclk => NLW_pcie_versal_0_gt_quad_1_ch0_dmonitoroutclk_UNCONNECTED,
      ch0_eyescandataerror => pcie_versal_0_phy_GT_RX4_ch_eyescandataerror,
      ch0_eyescanreset => '0',
      ch0_eyescantrigger => '0',
      ch0_gtrsvd(15 downto 0) => B"0000000000000000",
      ch0_gtrxreset => '0',
      ch0_gttxreset => '0',
      ch0_hsdppcsreset => '0',
      ch0_iloreset => '0',
      ch0_iloresetdone => NLW_pcie_versal_0_gt_quad_1_ch0_iloresetdone_UNCONNECTED,
      ch0_iloresetmask => '1',
      ch0_loopback(2 downto 0) => B"000",
      ch0_pcierstb => pcie_versal_0_phy_pcierstb,
      ch0_pcsrsvdin(15 downto 0) => B"0000001001000000",
      ch0_pcsrsvdout(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch0_pcsrsvdout_UNCONNECTED(15 downto 0),
      ch0_phyesmadaptsave => '0',
      ch0_phyready => pcie_versal_0_gt_quad_1_ch0_phyready,
      ch0_phystatus => pcie_versal_0_gt_quad_1_ch0_phystatus,
      ch0_pinrsvdas(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch0_pinrsvdas_UNCONNECTED(15 downto 0),
      ch0_resetexception => NLW_pcie_versal_0_gt_quad_1_ch0_resetexception_UNCONNECTED,
      ch0_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rx10gstat(7 downto 0),
      ch0_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxbufstatus(2 downto 0),
      ch0_rxbyteisaligned => pcie_versal_0_phy_GT_RX4_ch_rxbyteisaligned,
      ch0_rxbyterealign => pcie_versal_0_phy_GT_RX4_ch_rxbyterealign,
      ch0_rxcdrhold => '0',
      ch0_rxcdrlock => pcie_versal_0_phy_GT_RX4_ch_rxcdrlock,
      ch0_rxcdrovrden => '0',
      ch0_rxcdrphdone => pcie_versal_0_phy_GT_RX4_ch_rxcdrphdone,
      ch0_rxcdrreset => '0',
      ch0_rxchanbondseq => pcie_versal_0_phy_GT_RX4_ch_rxchanbondseq,
      ch0_rxchanisaligned => pcie_versal_0_phy_GT_RX4_ch_rxchanisaligned,
      ch0_rxchanrealign => pcie_versal_0_phy_GT_RX4_ch_rxchanrealign,
      ch0_rxchbondi(4 downto 0) => B"00000",
      ch0_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxchbondo(4 downto 0),
      ch0_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxclkcorcnt(1 downto 0),
      ch0_rxcominitdet => pcie_versal_0_phy_GT_RX4_ch_rxcominitdet,
      ch0_rxcommadet => pcie_versal_0_phy_GT_RX4_ch_rxcommadet,
      ch0_rxcomsasdet => pcie_versal_0_phy_GT_RX4_ch_rxcomsasdet,
      ch0_rxcomwakedet => pcie_versal_0_phy_GT_RX4_ch_rxcomwakedet,
      ch0_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxctrl0(15 downto 0),
      ch0_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxctrl1(15 downto 0),
      ch0_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxctrl2(7 downto 0),
      ch0_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxctrl3(7 downto 0),
      ch0_rxdapicodeovrden => '0',
      ch0_rxdapicodereset => '0',
      ch0_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxdata(127 downto 0),
      ch0_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxdataextendrsvd(7 downto 0),
      ch0_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxdatavalid(1 downto 0),
      ch0_rxdccdone => pcie_versal_0_phy_GT_RX4_ch_rxdccdone,
      ch0_rxdlyalignerr => pcie_versal_0_phy_GT_RX4_ch_rxdlyalignerr,
      ch0_rxdlyalignprog => pcie_versal_0_phy_GT_RX4_ch_rxdlyalignprog,
      ch0_rxdlyalignreq => '0',
      ch0_rxelecidle => pcie_versal_0_phy_GT_RX4_ch_rxelecidle,
      ch0_rxeqtraining => '0',
      ch0_rxfinealigndone => pcie_versal_0_phy_GT_RX4_ch_rxfinealigndone,
      ch0_rxgearboxslip => '0',
      ch0_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxheader(5 downto 0),
      ch0_rxheadervalid(1 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch0_rxheadervalid_UNCONNECTED(1 downto 0),
      ch0_rxlatclk => '0',
      ch0_rxlpmen => '0',
      ch0_rxmldchaindone => '0',
      ch0_rxmldchainreq => '0',
      ch0_rxmlfinealignreq => '0',
      ch0_rxmstreset => '0',
      ch0_rxmstresetdone => pcie_versal_0_phy_GT_RX4_ch_rxmstresetdone,
      ch0_rxoobreset => '0',
      ch0_rxosintdone => pcie_versal_0_phy_GT_RX4_ch_rxosintdone,
      ch0_rxosintstarted => pcie_versal_0_phy_GT_RX4_ch_rxosintstarted,
      ch0_rxosintstrobedone => pcie_versal_0_phy_GT_RX4_ch_rxosintstrobedone,
      ch0_rxosintstrobestarted => pcie_versal_0_phy_GT_RX4_ch_rxosintstrobestarted,
      ch0_rxoutclk => NLW_pcie_versal_0_gt_quad_1_ch0_rxoutclk_UNCONNECTED,
      ch0_rxpcsresetmask(4 downto 0) => B"11111",
      ch0_rxpd(1 downto 0) => B"00",
      ch0_rxphaligndone => NLW_pcie_versal_0_gt_quad_1_ch0_rxphaligndone_UNCONNECTED,
      ch0_rxphalignerr => pcie_versal_0_phy_GT_RX4_ch_rxphalignerr,
      ch0_rxphalignreq => '0',
      ch0_rxphalignresetmask(1 downto 0) => B"11",
      ch0_rxphdlypd => '0',
      ch0_rxphdlyreset => '0',
      ch0_rxphdlyresetdone => pcie_versal_0_phy_GT_RX4_ch_rxphdlyresetdone,
      ch0_rxphsetinitdone => pcie_versal_0_phy_GT_RX4_ch_rxphsetinitdone,
      ch0_rxphsetinitreq => '0',
      ch0_rxphshift180 => '0',
      ch0_rxphshift180done => pcie_versal_0_phy_GT_RX4_ch_rxphshift180done,
      ch0_rxpmaresetdone => pcie_versal_0_phy_GT_RX4_ch_rxpmaresetdone,
      ch0_rxpmaresetmask(6 downto 0) => B"1111111",
      ch0_rxpolarity => pcie_versal_0_phy_GT_RX4_ch_rxpolarity,
      ch0_rxprbscntreset => '0',
      ch0_rxprbserr => pcie_versal_0_phy_GT_RX4_ch_rxprbserr,
      ch0_rxprbslocked => pcie_versal_0_phy_GT_RX4_ch_rxprbslocked,
      ch0_rxprbssel(3 downto 0) => B"0000",
      ch0_rxprogdivreset => '0',
      ch0_rxprogdivresetdone => pcie_versal_0_phy_GT_RX4_ch_rxprogdivresetdone,
      ch0_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxrate(7 downto 0),
      ch0_rxresetdone => pcie_versal_0_phy_GT_RX4_ch_rxresetdone,
      ch0_rxresetmode(1 downto 0) => B"00",
      ch0_rxslide => '0',
      ch0_rxsliderdy => pcie_versal_0_phy_GT_RX4_ch_rxsliderdy,
      ch0_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxstartofseq(1 downto 0),
      ch0_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxstatus(2 downto 0),
      ch0_rxsyncallin => '0',
      ch0_rxsyncdone => pcie_versal_0_phy_GT_RX4_ch_rxsyncdone,
      ch0_rxtermination => pcie_versal_0_phy_GT_RX4_ch_rxtermination,
      ch0_rxuserrdy => pcie_versal_0_phy_GT_RX4_ch_rxuserrdy,
      ch0_rxusrclk => pcie_versal_0_phy_phy_pclk,
      ch0_rxvalid => pcie_versal_0_phy_GT_RX4_ch_rxvalid,
      ch0_tstin(19 downto 0) => B"00000000000000000000",
      ch0_tx10gstat => pcie_versal_0_phy_GT_TX4_ch_tx10gstat,
      ch0_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txbufstatus(1 downto 0),
      ch0_txcomfinish => pcie_versal_0_phy_GT_TX4_ch_txcomfinish,
      ch0_txcominit => '0',
      ch0_txcomsas => '0',
      ch0_txcomwake => '0',
      ch0_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txctrl0(15 downto 0),
      ch0_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txctrl1(15 downto 0),
      ch0_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txctrl2(7 downto 0),
      ch0_txdapicodeovrden => '0',
      ch0_txdapicodereset => '0',
      ch0_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txdata(127 downto 0),
      ch0_txdataextendrsvd(7 downto 0) => B"00000000",
      ch0_txdccdone => pcie_versal_0_phy_GT_TX4_ch_txdccdone,
      ch0_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txdeemph(1 downto 0),
      ch0_txdetectrx => pcie_versal_0_phy_GT_TX4_ch_txdetectrx,
      ch0_txdiffctrl(4 downto 0) => B"11001",
      ch0_txdlyalignerr => pcie_versal_0_phy_GT_TX4_ch_txdlyalignerr,
      ch0_txdlyalignprog => pcie_versal_0_phy_GT_TX4_ch_txdlyalignprog,
      ch0_txdlyalignreq => '0',
      ch0_txelecidle => pcie_versal_0_phy_GT_TX4_ch_txelecidle,
      ch0_txheader(5 downto 0) => B"000000",
      ch0_txinhibit => '0',
      ch0_txlatclk => '0',
      ch0_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txmaincursor(6 downto 0),
      ch0_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txmargin(2 downto 0),
      ch0_txmldchaindone => '0',
      ch0_txmldchainreq => '0',
      ch0_txmstreset => '0',
      ch0_txmstresetdone => pcie_versal_0_phy_GT_TX4_ch_txmstresetdone,
      ch0_txoneszeros => '0',
      ch0_txoutclk => NLW_pcie_versal_0_gt_quad_1_ch0_txoutclk_UNCONNECTED,
      ch0_txpausedelayalign => '0',
      ch0_txpcsresetmask => '1',
      ch0_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txpd(1 downto 0),
      ch0_txphaligndone => pcie_versal_0_phy_GT_TX4_ch_txphaligndone,
      ch0_txphalignerr => pcie_versal_0_phy_GT_TX4_ch_txphalignerr,
      ch0_txphalignoutrsvd => pcie_versal_0_phy_GT_TX4_ch_txphalignoutrsvd,
      ch0_txphalignreq => '0',
      ch0_txphalignresetmask(1 downto 0) => B"11",
      ch0_txphdlypd => '0',
      ch0_txphdlyreset => '0',
      ch0_txphdlyresetdone => pcie_versal_0_phy_GT_TX4_ch_txphdlyresetdone,
      ch0_txphdlytstclk => '0',
      ch0_txphsetinitdone => pcie_versal_0_phy_GT_TX4_ch_txphsetinitdone,
      ch0_txphsetinitreq => '0',
      ch0_txphshift180 => '0',
      ch0_txphshift180done => pcie_versal_0_phy_GT_TX4_ch_txphshift180done,
      ch0_txpicodeovrden => '0',
      ch0_txpicodereset => '0',
      ch0_txpippmen => '0',
      ch0_txpippmstepsize(4 downto 0) => B"00000",
      ch0_txpisopd => '0',
      ch0_txpmaresetdone => pcie_versal_0_phy_GT_TX4_ch_txpmaresetdone,
      ch0_txpmaresetmask(2 downto 0) => B"111",
      ch0_txpolarity => '0',
      ch0_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txpostcursor(4 downto 0),
      ch0_txprbsforceerr => '0',
      ch0_txprbssel(3 downto 0) => B"0000",
      ch0_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txprecursor(4 downto 0),
      ch0_txprogdivreset => '0',
      ch0_txprogdivresetdone => pcie_versal_0_phy_GT_TX4_ch_txprogdivresetdone,
      ch0_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txrate(7 downto 0),
      ch0_txresetdone => pcie_versal_0_phy_GT_TX4_ch_txresetdone,
      ch0_txresetmode(1 downto 0) => B"00",
      ch0_txsequence(6 downto 0) => B"0000000",
      ch0_txswing => pcie_versal_0_phy_GT_TX4_ch_txswing,
      ch0_txsyncallin => '0',
      ch0_txsyncdone => pcie_versal_0_phy_GT_TX4_ch_txsyncdone,
      ch0_txuserrdy => pcie_versal_0_phy_GT_TX4_ch_txuserrdy,
      ch0_txusrclk => pcie_versal_0_phy_phy_pclk,
      ch1_bufgtce => NLW_pcie_versal_0_gt_quad_1_ch1_bufgtce_UNCONNECTED,
      ch1_bufgtcemask(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch1_bufgtcemask_UNCONNECTED(3 downto 0),
      ch1_bufgtdiv(11 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch1_bufgtdiv_UNCONNECTED(11 downto 0),
      ch1_bufgtrst => NLW_pcie_versal_0_gt_quad_1_ch1_bufgtrst_UNCONNECTED,
      ch1_bufgtrstmask(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch1_bufgtrstmask_UNCONNECTED(3 downto 0),
      ch1_cdrbmcdrreq => '0',
      ch1_cdrfreqos => '0',
      ch1_cdrincpctrl => '0',
      ch1_cdrstepdir => '0',
      ch1_cdrstepsq => '0',
      ch1_cdrstepsx => '0',
      ch1_cfokovrdfinish => '0',
      ch1_cfokovrdpulse => '0',
      ch1_cfokovrdrdy0 => NLW_pcie_versal_0_gt_quad_1_ch1_cfokovrdrdy0_UNCONNECTED,
      ch1_cfokovrdrdy1 => NLW_pcie_versal_0_gt_quad_1_ch1_cfokovrdrdy1_UNCONNECTED,
      ch1_cfokovrdstart => '0',
      ch1_clkrsvd0 => '0',
      ch1_clkrsvd1 => '0',
      ch1_dmonfiforeset => '0',
      ch1_dmonitorclk => '0',
      ch1_dmonitorout(31 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch1_dmonitorout_UNCONNECTED(31 downto 0),
      ch1_dmonitoroutclk => NLW_pcie_versal_0_gt_quad_1_ch1_dmonitoroutclk_UNCONNECTED,
      ch1_eyescandataerror => pcie_versal_0_phy_GT_RX5_ch_eyescandataerror,
      ch1_eyescanreset => '0',
      ch1_eyescantrigger => '0',
      ch1_gtrsvd(15 downto 0) => B"0000000000000000",
      ch1_gtrxreset => '0',
      ch1_gttxreset => '0',
      ch1_hsdppcsreset => '0',
      ch1_iloreset => '0',
      ch1_iloresetdone => NLW_pcie_versal_0_gt_quad_1_ch1_iloresetdone_UNCONNECTED,
      ch1_iloresetmask => '1',
      ch1_loopback(2 downto 0) => B"000",
      ch1_pcierstb => pcie_versal_0_phy_pcierstb,
      ch1_pcsrsvdin(15 downto 0) => B"0000001001000000",
      ch1_pcsrsvdout(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch1_pcsrsvdout_UNCONNECTED(15 downto 0),
      ch1_phyesmadaptsave => '0',
      ch1_phyready => pcie_versal_0_gt_quad_1_ch1_phyready,
      ch1_phystatus => pcie_versal_0_gt_quad_1_ch1_phystatus,
      ch1_pinrsvdas(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch1_pinrsvdas_UNCONNECTED(15 downto 0),
      ch1_resetexception => NLW_pcie_versal_0_gt_quad_1_ch1_resetexception_UNCONNECTED,
      ch1_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rx10gstat(7 downto 0),
      ch1_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxbufstatus(2 downto 0),
      ch1_rxbyteisaligned => pcie_versal_0_phy_GT_RX5_ch_rxbyteisaligned,
      ch1_rxbyterealign => pcie_versal_0_phy_GT_RX5_ch_rxbyterealign,
      ch1_rxcdrhold => '0',
      ch1_rxcdrlock => pcie_versal_0_phy_GT_RX5_ch_rxcdrlock,
      ch1_rxcdrovrden => '0',
      ch1_rxcdrphdone => pcie_versal_0_phy_GT_RX5_ch_rxcdrphdone,
      ch1_rxcdrreset => '0',
      ch1_rxchanbondseq => pcie_versal_0_phy_GT_RX5_ch_rxchanbondseq,
      ch1_rxchanisaligned => pcie_versal_0_phy_GT_RX5_ch_rxchanisaligned,
      ch1_rxchanrealign => pcie_versal_0_phy_GT_RX5_ch_rxchanrealign,
      ch1_rxchbondi(4 downto 0) => B"00000",
      ch1_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxchbondo(4 downto 0),
      ch1_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxclkcorcnt(1 downto 0),
      ch1_rxcominitdet => pcie_versal_0_phy_GT_RX5_ch_rxcominitdet,
      ch1_rxcommadet => pcie_versal_0_phy_GT_RX5_ch_rxcommadet,
      ch1_rxcomsasdet => pcie_versal_0_phy_GT_RX5_ch_rxcomsasdet,
      ch1_rxcomwakedet => pcie_versal_0_phy_GT_RX5_ch_rxcomwakedet,
      ch1_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxctrl0(15 downto 0),
      ch1_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxctrl1(15 downto 0),
      ch1_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxctrl2(7 downto 0),
      ch1_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxctrl3(7 downto 0),
      ch1_rxdapicodeovrden => '0',
      ch1_rxdapicodereset => '0',
      ch1_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxdata(127 downto 0),
      ch1_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxdataextendrsvd(7 downto 0),
      ch1_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxdatavalid(1 downto 0),
      ch1_rxdccdone => pcie_versal_0_phy_GT_RX5_ch_rxdccdone,
      ch1_rxdlyalignerr => pcie_versal_0_phy_GT_RX5_ch_rxdlyalignerr,
      ch1_rxdlyalignprog => pcie_versal_0_phy_GT_RX5_ch_rxdlyalignprog,
      ch1_rxdlyalignreq => '0',
      ch1_rxelecidle => pcie_versal_0_phy_GT_RX5_ch_rxelecidle,
      ch1_rxeqtraining => '0',
      ch1_rxfinealigndone => pcie_versal_0_phy_GT_RX5_ch_rxfinealigndone,
      ch1_rxgearboxslip => '0',
      ch1_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxheader(5 downto 0),
      ch1_rxheadervalid(1 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch1_rxheadervalid_UNCONNECTED(1 downto 0),
      ch1_rxlatclk => '0',
      ch1_rxlpmen => '0',
      ch1_rxmldchaindone => '0',
      ch1_rxmldchainreq => '0',
      ch1_rxmlfinealignreq => '0',
      ch1_rxmstreset => '0',
      ch1_rxmstresetdone => pcie_versal_0_phy_GT_RX5_ch_rxmstresetdone,
      ch1_rxoobreset => '0',
      ch1_rxosintdone => pcie_versal_0_phy_GT_RX5_ch_rxosintdone,
      ch1_rxosintstarted => pcie_versal_0_phy_GT_RX5_ch_rxosintstarted,
      ch1_rxosintstrobedone => pcie_versal_0_phy_GT_RX5_ch_rxosintstrobedone,
      ch1_rxosintstrobestarted => pcie_versal_0_phy_GT_RX5_ch_rxosintstrobestarted,
      ch1_rxoutclk => NLW_pcie_versal_0_gt_quad_1_ch1_rxoutclk_UNCONNECTED,
      ch1_rxpcsresetmask(4 downto 0) => B"11111",
      ch1_rxpd(1 downto 0) => B"00",
      ch1_rxphaligndone => NLW_pcie_versal_0_gt_quad_1_ch1_rxphaligndone_UNCONNECTED,
      ch1_rxphalignerr => pcie_versal_0_phy_GT_RX5_ch_rxphalignerr,
      ch1_rxphalignreq => '0',
      ch1_rxphalignresetmask(1 downto 0) => B"11",
      ch1_rxphdlypd => '0',
      ch1_rxphdlyreset => '0',
      ch1_rxphdlyresetdone => pcie_versal_0_phy_GT_RX5_ch_rxphdlyresetdone,
      ch1_rxphsetinitdone => pcie_versal_0_phy_GT_RX5_ch_rxphsetinitdone,
      ch1_rxphsetinitreq => '0',
      ch1_rxphshift180 => '0',
      ch1_rxphshift180done => pcie_versal_0_phy_GT_RX5_ch_rxphshift180done,
      ch1_rxpmaresetdone => pcie_versal_0_phy_GT_RX5_ch_rxpmaresetdone,
      ch1_rxpmaresetmask(6 downto 0) => B"1111111",
      ch1_rxpolarity => pcie_versal_0_phy_GT_RX5_ch_rxpolarity,
      ch1_rxprbscntreset => '0',
      ch1_rxprbserr => pcie_versal_0_phy_GT_RX5_ch_rxprbserr,
      ch1_rxprbslocked => pcie_versal_0_phy_GT_RX5_ch_rxprbslocked,
      ch1_rxprbssel(3 downto 0) => B"0000",
      ch1_rxprogdivreset => '0',
      ch1_rxprogdivresetdone => pcie_versal_0_phy_GT_RX5_ch_rxprogdivresetdone,
      ch1_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxrate(7 downto 0),
      ch1_rxresetdone => pcie_versal_0_phy_GT_RX5_ch_rxresetdone,
      ch1_rxresetmode(1 downto 0) => B"00",
      ch1_rxslide => '0',
      ch1_rxsliderdy => pcie_versal_0_phy_GT_RX5_ch_rxsliderdy,
      ch1_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxstartofseq(1 downto 0),
      ch1_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxstatus(2 downto 0),
      ch1_rxsyncallin => '0',
      ch1_rxsyncdone => pcie_versal_0_phy_GT_RX5_ch_rxsyncdone,
      ch1_rxtermination => pcie_versal_0_phy_GT_RX5_ch_rxtermination,
      ch1_rxuserrdy => pcie_versal_0_phy_GT_RX5_ch_rxuserrdy,
      ch1_rxusrclk => pcie_versal_0_phy_phy_pclk,
      ch1_rxvalid => pcie_versal_0_phy_GT_RX5_ch_rxvalid,
      ch1_tstin(19 downto 0) => B"00000000000000000000",
      ch1_tx10gstat => pcie_versal_0_phy_GT_TX5_ch_tx10gstat,
      ch1_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txbufstatus(1 downto 0),
      ch1_txcomfinish => pcie_versal_0_phy_GT_TX5_ch_txcomfinish,
      ch1_txcominit => '0',
      ch1_txcomsas => '0',
      ch1_txcomwake => '0',
      ch1_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txctrl0(15 downto 0),
      ch1_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txctrl1(15 downto 0),
      ch1_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txctrl2(7 downto 0),
      ch1_txdapicodeovrden => '0',
      ch1_txdapicodereset => '0',
      ch1_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txdata(127 downto 0),
      ch1_txdataextendrsvd(7 downto 0) => B"00000000",
      ch1_txdccdone => pcie_versal_0_phy_GT_TX5_ch_txdccdone,
      ch1_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txdeemph(1 downto 0),
      ch1_txdetectrx => pcie_versal_0_phy_GT_TX5_ch_txdetectrx,
      ch1_txdiffctrl(4 downto 0) => B"11001",
      ch1_txdlyalignerr => pcie_versal_0_phy_GT_TX5_ch_txdlyalignerr,
      ch1_txdlyalignprog => pcie_versal_0_phy_GT_TX5_ch_txdlyalignprog,
      ch1_txdlyalignreq => '0',
      ch1_txelecidle => pcie_versal_0_phy_GT_TX5_ch_txelecidle,
      ch1_txheader(5 downto 0) => B"000000",
      ch1_txinhibit => '0',
      ch1_txlatclk => '0',
      ch1_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txmaincursor(6 downto 0),
      ch1_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txmargin(2 downto 0),
      ch1_txmldchaindone => '0',
      ch1_txmldchainreq => '0',
      ch1_txmstreset => '0',
      ch1_txmstresetdone => pcie_versal_0_phy_GT_TX5_ch_txmstresetdone,
      ch1_txoneszeros => '0',
      ch1_txoutclk => NLW_pcie_versal_0_gt_quad_1_ch1_txoutclk_UNCONNECTED,
      ch1_txpausedelayalign => '0',
      ch1_txpcsresetmask => '1',
      ch1_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txpd(1 downto 0),
      ch1_txphaligndone => pcie_versal_0_phy_GT_TX5_ch_txphaligndone,
      ch1_txphalignerr => pcie_versal_0_phy_GT_TX5_ch_txphalignerr,
      ch1_txphalignoutrsvd => pcie_versal_0_phy_GT_TX5_ch_txphalignoutrsvd,
      ch1_txphalignreq => '0',
      ch1_txphalignresetmask(1 downto 0) => B"11",
      ch1_txphdlypd => '0',
      ch1_txphdlyreset => '0',
      ch1_txphdlyresetdone => pcie_versal_0_phy_GT_TX5_ch_txphdlyresetdone,
      ch1_txphdlytstclk => '0',
      ch1_txphsetinitdone => pcie_versal_0_phy_GT_TX5_ch_txphsetinitdone,
      ch1_txphsetinitreq => '0',
      ch1_txphshift180 => '0',
      ch1_txphshift180done => pcie_versal_0_phy_GT_TX5_ch_txphshift180done,
      ch1_txpicodeovrden => '0',
      ch1_txpicodereset => '0',
      ch1_txpippmen => '0',
      ch1_txpippmstepsize(4 downto 0) => B"00000",
      ch1_txpisopd => '0',
      ch1_txpmaresetdone => pcie_versal_0_phy_GT_TX5_ch_txpmaresetdone,
      ch1_txpmaresetmask(2 downto 0) => B"111",
      ch1_txpolarity => '0',
      ch1_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txpostcursor(4 downto 0),
      ch1_txprbsforceerr => '0',
      ch1_txprbssel(3 downto 0) => B"0000",
      ch1_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txprecursor(4 downto 0),
      ch1_txprogdivreset => '0',
      ch1_txprogdivresetdone => pcie_versal_0_phy_GT_TX5_ch_txprogdivresetdone,
      ch1_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txrate(7 downto 0),
      ch1_txresetdone => pcie_versal_0_phy_GT_TX5_ch_txresetdone,
      ch1_txresetmode(1 downto 0) => B"00",
      ch1_txsequence(6 downto 0) => B"0000000",
      ch1_txswing => pcie_versal_0_phy_GT_TX5_ch_txswing,
      ch1_txsyncallin => '0',
      ch1_txsyncdone => pcie_versal_0_phy_GT_TX5_ch_txsyncdone,
      ch1_txuserrdy => pcie_versal_0_phy_GT_TX5_ch_txuserrdy,
      ch1_txusrclk => pcie_versal_0_phy_phy_pclk,
      ch2_bufgtce => NLW_pcie_versal_0_gt_quad_1_ch2_bufgtce_UNCONNECTED,
      ch2_bufgtcemask(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch2_bufgtcemask_UNCONNECTED(3 downto 0),
      ch2_bufgtdiv(11 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch2_bufgtdiv_UNCONNECTED(11 downto 0),
      ch2_bufgtrst => NLW_pcie_versal_0_gt_quad_1_ch2_bufgtrst_UNCONNECTED,
      ch2_bufgtrstmask(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch2_bufgtrstmask_UNCONNECTED(3 downto 0),
      ch2_cdrbmcdrreq => '0',
      ch2_cdrfreqos => '0',
      ch2_cdrincpctrl => '0',
      ch2_cdrstepdir => '0',
      ch2_cdrstepsq => '0',
      ch2_cdrstepsx => '0',
      ch2_cfokovrdfinish => '0',
      ch2_cfokovrdpulse => '0',
      ch2_cfokovrdrdy0 => NLW_pcie_versal_0_gt_quad_1_ch2_cfokovrdrdy0_UNCONNECTED,
      ch2_cfokovrdrdy1 => NLW_pcie_versal_0_gt_quad_1_ch2_cfokovrdrdy1_UNCONNECTED,
      ch2_cfokovrdstart => '0',
      ch2_clkrsvd0 => '0',
      ch2_clkrsvd1 => '0',
      ch2_dmonfiforeset => '0',
      ch2_dmonitorclk => '0',
      ch2_dmonitorout(31 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch2_dmonitorout_UNCONNECTED(31 downto 0),
      ch2_dmonitoroutclk => NLW_pcie_versal_0_gt_quad_1_ch2_dmonitoroutclk_UNCONNECTED,
      ch2_eyescandataerror => pcie_versal_0_phy_GT_RX6_ch_eyescandataerror,
      ch2_eyescanreset => '0',
      ch2_eyescantrigger => '0',
      ch2_gtrsvd(15 downto 0) => B"0000000000000000",
      ch2_gtrxreset => '0',
      ch2_gttxreset => '0',
      ch2_hsdppcsreset => '0',
      ch2_iloreset => '0',
      ch2_iloresetdone => NLW_pcie_versal_0_gt_quad_1_ch2_iloresetdone_UNCONNECTED,
      ch2_iloresetmask => '1',
      ch2_loopback(2 downto 0) => B"000",
      ch2_pcierstb => pcie_versal_0_phy_pcierstb,
      ch2_pcsrsvdin(15 downto 0) => B"0000001001000000",
      ch2_pcsrsvdout(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch2_pcsrsvdout_UNCONNECTED(15 downto 0),
      ch2_phyesmadaptsave => '0',
      ch2_phyready => pcie_versal_0_gt_quad_1_ch2_phyready,
      ch2_phystatus => pcie_versal_0_gt_quad_1_ch2_phystatus,
      ch2_pinrsvdas(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch2_pinrsvdas_UNCONNECTED(15 downto 0),
      ch2_resetexception => NLW_pcie_versal_0_gt_quad_1_ch2_resetexception_UNCONNECTED,
      ch2_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rx10gstat(7 downto 0),
      ch2_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxbufstatus(2 downto 0),
      ch2_rxbyteisaligned => pcie_versal_0_phy_GT_RX6_ch_rxbyteisaligned,
      ch2_rxbyterealign => pcie_versal_0_phy_GT_RX6_ch_rxbyterealign,
      ch2_rxcdrhold => '0',
      ch2_rxcdrlock => pcie_versal_0_phy_GT_RX6_ch_rxcdrlock,
      ch2_rxcdrovrden => '0',
      ch2_rxcdrphdone => pcie_versal_0_phy_GT_RX6_ch_rxcdrphdone,
      ch2_rxcdrreset => '0',
      ch2_rxchanbondseq => pcie_versal_0_phy_GT_RX6_ch_rxchanbondseq,
      ch2_rxchanisaligned => pcie_versal_0_phy_GT_RX6_ch_rxchanisaligned,
      ch2_rxchanrealign => pcie_versal_0_phy_GT_RX6_ch_rxchanrealign,
      ch2_rxchbondi(4 downto 0) => B"00000",
      ch2_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxchbondo(4 downto 0),
      ch2_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxclkcorcnt(1 downto 0),
      ch2_rxcominitdet => pcie_versal_0_phy_GT_RX6_ch_rxcominitdet,
      ch2_rxcommadet => pcie_versal_0_phy_GT_RX6_ch_rxcommadet,
      ch2_rxcomsasdet => pcie_versal_0_phy_GT_RX6_ch_rxcomsasdet,
      ch2_rxcomwakedet => pcie_versal_0_phy_GT_RX6_ch_rxcomwakedet,
      ch2_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxctrl0(15 downto 0),
      ch2_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxctrl1(15 downto 0),
      ch2_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxctrl2(7 downto 0),
      ch2_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxctrl3(7 downto 0),
      ch2_rxdapicodeovrden => '0',
      ch2_rxdapicodereset => '0',
      ch2_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxdata(127 downto 0),
      ch2_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxdataextendrsvd(7 downto 0),
      ch2_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxdatavalid(1 downto 0),
      ch2_rxdccdone => pcie_versal_0_phy_GT_RX6_ch_rxdccdone,
      ch2_rxdlyalignerr => pcie_versal_0_phy_GT_RX6_ch_rxdlyalignerr,
      ch2_rxdlyalignprog => pcie_versal_0_phy_GT_RX6_ch_rxdlyalignprog,
      ch2_rxdlyalignreq => '0',
      ch2_rxelecidle => pcie_versal_0_phy_GT_RX6_ch_rxelecidle,
      ch2_rxeqtraining => '0',
      ch2_rxfinealigndone => pcie_versal_0_phy_GT_RX6_ch_rxfinealigndone,
      ch2_rxgearboxslip => '0',
      ch2_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxheader(5 downto 0),
      ch2_rxheadervalid(1 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch2_rxheadervalid_UNCONNECTED(1 downto 0),
      ch2_rxlatclk => '0',
      ch2_rxlpmen => '0',
      ch2_rxmldchaindone => '0',
      ch2_rxmldchainreq => '0',
      ch2_rxmlfinealignreq => '0',
      ch2_rxmstreset => '0',
      ch2_rxmstresetdone => pcie_versal_0_phy_GT_RX6_ch_rxmstresetdone,
      ch2_rxoobreset => '0',
      ch2_rxosintdone => pcie_versal_0_phy_GT_RX6_ch_rxosintdone,
      ch2_rxosintstarted => pcie_versal_0_phy_GT_RX6_ch_rxosintstarted,
      ch2_rxosintstrobedone => pcie_versal_0_phy_GT_RX6_ch_rxosintstrobedone,
      ch2_rxosintstrobestarted => pcie_versal_0_phy_GT_RX6_ch_rxosintstrobestarted,
      ch2_rxoutclk => NLW_pcie_versal_0_gt_quad_1_ch2_rxoutclk_UNCONNECTED,
      ch2_rxpcsresetmask(4 downto 0) => B"11111",
      ch2_rxpd(1 downto 0) => B"00",
      ch2_rxphaligndone => NLW_pcie_versal_0_gt_quad_1_ch2_rxphaligndone_UNCONNECTED,
      ch2_rxphalignerr => pcie_versal_0_phy_GT_RX6_ch_rxphalignerr,
      ch2_rxphalignreq => '0',
      ch2_rxphalignresetmask(1 downto 0) => B"11",
      ch2_rxphdlypd => '0',
      ch2_rxphdlyreset => '0',
      ch2_rxphdlyresetdone => pcie_versal_0_phy_GT_RX6_ch_rxphdlyresetdone,
      ch2_rxphsetinitdone => pcie_versal_0_phy_GT_RX6_ch_rxphsetinitdone,
      ch2_rxphsetinitreq => '0',
      ch2_rxphshift180 => '0',
      ch2_rxphshift180done => pcie_versal_0_phy_GT_RX6_ch_rxphshift180done,
      ch2_rxpmaresetdone => pcie_versal_0_phy_GT_RX6_ch_rxpmaresetdone,
      ch2_rxpmaresetmask(6 downto 0) => B"1111111",
      ch2_rxpolarity => pcie_versal_0_phy_GT_RX6_ch_rxpolarity,
      ch2_rxprbscntreset => '0',
      ch2_rxprbserr => pcie_versal_0_phy_GT_RX6_ch_rxprbserr,
      ch2_rxprbslocked => pcie_versal_0_phy_GT_RX6_ch_rxprbslocked,
      ch2_rxprbssel(3 downto 0) => B"0000",
      ch2_rxprogdivreset => '0',
      ch2_rxprogdivresetdone => pcie_versal_0_phy_GT_RX6_ch_rxprogdivresetdone,
      ch2_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxrate(7 downto 0),
      ch2_rxresetdone => pcie_versal_0_phy_GT_RX6_ch_rxresetdone,
      ch2_rxresetmode(1 downto 0) => B"00",
      ch2_rxslide => '0',
      ch2_rxsliderdy => pcie_versal_0_phy_GT_RX6_ch_rxsliderdy,
      ch2_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxstartofseq(1 downto 0),
      ch2_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxstatus(2 downto 0),
      ch2_rxsyncallin => '0',
      ch2_rxsyncdone => pcie_versal_0_phy_GT_RX6_ch_rxsyncdone,
      ch2_rxtermination => pcie_versal_0_phy_GT_RX6_ch_rxtermination,
      ch2_rxuserrdy => pcie_versal_0_phy_GT_RX6_ch_rxuserrdy,
      ch2_rxusrclk => pcie_versal_0_phy_phy_pclk,
      ch2_rxvalid => pcie_versal_0_phy_GT_RX6_ch_rxvalid,
      ch2_tstin(19 downto 0) => B"00000000000000000000",
      ch2_tx10gstat => pcie_versal_0_phy_GT_TX6_ch_tx10gstat,
      ch2_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txbufstatus(1 downto 0),
      ch2_txcomfinish => pcie_versal_0_phy_GT_TX6_ch_txcomfinish,
      ch2_txcominit => '0',
      ch2_txcomsas => '0',
      ch2_txcomwake => '0',
      ch2_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txctrl0(15 downto 0),
      ch2_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txctrl1(15 downto 0),
      ch2_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txctrl2(7 downto 0),
      ch2_txdapicodeovrden => '0',
      ch2_txdapicodereset => '0',
      ch2_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txdata(127 downto 0),
      ch2_txdataextendrsvd(7 downto 0) => B"00000000",
      ch2_txdccdone => pcie_versal_0_phy_GT_TX6_ch_txdccdone,
      ch2_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txdeemph(1 downto 0),
      ch2_txdetectrx => pcie_versal_0_phy_GT_TX6_ch_txdetectrx,
      ch2_txdiffctrl(4 downto 0) => B"11001",
      ch2_txdlyalignerr => pcie_versal_0_phy_GT_TX6_ch_txdlyalignerr,
      ch2_txdlyalignprog => pcie_versal_0_phy_GT_TX6_ch_txdlyalignprog,
      ch2_txdlyalignreq => '0',
      ch2_txelecidle => pcie_versal_0_phy_GT_TX6_ch_txelecidle,
      ch2_txheader(5 downto 0) => B"000000",
      ch2_txinhibit => '0',
      ch2_txlatclk => '0',
      ch2_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txmaincursor(6 downto 0),
      ch2_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txmargin(2 downto 0),
      ch2_txmldchaindone => '0',
      ch2_txmldchainreq => '0',
      ch2_txmstreset => '0',
      ch2_txmstresetdone => pcie_versal_0_phy_GT_TX6_ch_txmstresetdone,
      ch2_txoneszeros => '0',
      ch2_txoutclk => NLW_pcie_versal_0_gt_quad_1_ch2_txoutclk_UNCONNECTED,
      ch2_txpausedelayalign => '0',
      ch2_txpcsresetmask => '1',
      ch2_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txpd(1 downto 0),
      ch2_txphaligndone => pcie_versal_0_phy_GT_TX6_ch_txphaligndone,
      ch2_txphalignerr => pcie_versal_0_phy_GT_TX6_ch_txphalignerr,
      ch2_txphalignoutrsvd => pcie_versal_0_phy_GT_TX6_ch_txphalignoutrsvd,
      ch2_txphalignreq => '0',
      ch2_txphalignresetmask(1 downto 0) => B"11",
      ch2_txphdlypd => '0',
      ch2_txphdlyreset => '0',
      ch2_txphdlyresetdone => pcie_versal_0_phy_GT_TX6_ch_txphdlyresetdone,
      ch2_txphdlytstclk => '0',
      ch2_txphsetinitdone => pcie_versal_0_phy_GT_TX6_ch_txphsetinitdone,
      ch2_txphsetinitreq => '0',
      ch2_txphshift180 => '0',
      ch2_txphshift180done => pcie_versal_0_phy_GT_TX6_ch_txphshift180done,
      ch2_txpicodeovrden => '0',
      ch2_txpicodereset => '0',
      ch2_txpippmen => '0',
      ch2_txpippmstepsize(4 downto 0) => B"00000",
      ch2_txpisopd => '0',
      ch2_txpmaresetdone => pcie_versal_0_phy_GT_TX6_ch_txpmaresetdone,
      ch2_txpmaresetmask(2 downto 0) => B"111",
      ch2_txpolarity => '0',
      ch2_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txpostcursor(4 downto 0),
      ch2_txprbsforceerr => '0',
      ch2_txprbssel(3 downto 0) => B"0000",
      ch2_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txprecursor(4 downto 0),
      ch2_txprogdivreset => '0',
      ch2_txprogdivresetdone => pcie_versal_0_phy_GT_TX6_ch_txprogdivresetdone,
      ch2_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txrate(7 downto 0),
      ch2_txresetdone => pcie_versal_0_phy_GT_TX6_ch_txresetdone,
      ch2_txresetmode(1 downto 0) => B"00",
      ch2_txsequence(6 downto 0) => B"0000000",
      ch2_txswing => pcie_versal_0_phy_GT_TX6_ch_txswing,
      ch2_txsyncallin => '0',
      ch2_txsyncdone => pcie_versal_0_phy_GT_TX6_ch_txsyncdone,
      ch2_txuserrdy => pcie_versal_0_phy_GT_TX6_ch_txuserrdy,
      ch2_txusrclk => pcie_versal_0_phy_phy_pclk,
      ch3_bufgtce => NLW_pcie_versal_0_gt_quad_1_ch3_bufgtce_UNCONNECTED,
      ch3_bufgtcemask(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch3_bufgtcemask_UNCONNECTED(3 downto 0),
      ch3_bufgtdiv(11 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch3_bufgtdiv_UNCONNECTED(11 downto 0),
      ch3_bufgtrst => NLW_pcie_versal_0_gt_quad_1_ch3_bufgtrst_UNCONNECTED,
      ch3_bufgtrstmask(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch3_bufgtrstmask_UNCONNECTED(3 downto 0),
      ch3_cdrbmcdrreq => '0',
      ch3_cdrfreqos => '0',
      ch3_cdrincpctrl => '0',
      ch3_cdrstepdir => '0',
      ch3_cdrstepsq => '0',
      ch3_cdrstepsx => '0',
      ch3_cfokovrdfinish => '0',
      ch3_cfokovrdpulse => '0',
      ch3_cfokovrdrdy0 => NLW_pcie_versal_0_gt_quad_1_ch3_cfokovrdrdy0_UNCONNECTED,
      ch3_cfokovrdrdy1 => NLW_pcie_versal_0_gt_quad_1_ch3_cfokovrdrdy1_UNCONNECTED,
      ch3_cfokovrdstart => '0',
      ch3_clkrsvd0 => '0',
      ch3_clkrsvd1 => '0',
      ch3_dmonfiforeset => '0',
      ch3_dmonitorclk => '0',
      ch3_dmonitorout(31 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch3_dmonitorout_UNCONNECTED(31 downto 0),
      ch3_dmonitoroutclk => NLW_pcie_versal_0_gt_quad_1_ch3_dmonitoroutclk_UNCONNECTED,
      ch3_eyescandataerror => pcie_versal_0_phy_GT_RX7_ch_eyescandataerror,
      ch3_eyescanreset => '0',
      ch3_eyescantrigger => '0',
      ch3_gtrsvd(15 downto 0) => B"0000000000000000",
      ch3_gtrxreset => '0',
      ch3_gttxreset => '0',
      ch3_hsdppcsreset => '0',
      ch3_iloreset => '0',
      ch3_iloresetdone => NLW_pcie_versal_0_gt_quad_1_ch3_iloresetdone_UNCONNECTED,
      ch3_iloresetmask => '1',
      ch3_loopback(2 downto 0) => B"000",
      ch3_pcierstb => pcie_versal_0_phy_pcierstb,
      ch3_pcsrsvdin(15 downto 0) => B"0000001001000000",
      ch3_pcsrsvdout(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch3_pcsrsvdout_UNCONNECTED(15 downto 0),
      ch3_phyesmadaptsave => '0',
      ch3_phyready => pcie_versal_0_gt_quad_1_ch3_phyready,
      ch3_phystatus => pcie_versal_0_gt_quad_1_ch3_phystatus,
      ch3_pinrsvdas(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch3_pinrsvdas_UNCONNECTED(15 downto 0),
      ch3_resetexception => NLW_pcie_versal_0_gt_quad_1_ch3_resetexception_UNCONNECTED,
      ch3_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rx10gstat(7 downto 0),
      ch3_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxbufstatus(2 downto 0),
      ch3_rxbyteisaligned => pcie_versal_0_phy_GT_RX7_ch_rxbyteisaligned,
      ch3_rxbyterealign => pcie_versal_0_phy_GT_RX7_ch_rxbyterealign,
      ch3_rxcdrhold => '0',
      ch3_rxcdrlock => pcie_versal_0_phy_GT_RX7_ch_rxcdrlock,
      ch3_rxcdrovrden => '0',
      ch3_rxcdrphdone => pcie_versal_0_phy_GT_RX7_ch_rxcdrphdone,
      ch3_rxcdrreset => '0',
      ch3_rxchanbondseq => pcie_versal_0_phy_GT_RX7_ch_rxchanbondseq,
      ch3_rxchanisaligned => pcie_versal_0_phy_GT_RX7_ch_rxchanisaligned,
      ch3_rxchanrealign => pcie_versal_0_phy_GT_RX7_ch_rxchanrealign,
      ch3_rxchbondi(4 downto 0) => B"00000",
      ch3_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxchbondo(4 downto 0),
      ch3_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxclkcorcnt(1 downto 0),
      ch3_rxcominitdet => pcie_versal_0_phy_GT_RX7_ch_rxcominitdet,
      ch3_rxcommadet => pcie_versal_0_phy_GT_RX7_ch_rxcommadet,
      ch3_rxcomsasdet => pcie_versal_0_phy_GT_RX7_ch_rxcomsasdet,
      ch3_rxcomwakedet => pcie_versal_0_phy_GT_RX7_ch_rxcomwakedet,
      ch3_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxctrl0(15 downto 0),
      ch3_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxctrl1(15 downto 0),
      ch3_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxctrl2(7 downto 0),
      ch3_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxctrl3(7 downto 0),
      ch3_rxdapicodeovrden => '0',
      ch3_rxdapicodereset => '0',
      ch3_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxdata(127 downto 0),
      ch3_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxdataextendrsvd(7 downto 0),
      ch3_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxdatavalid(1 downto 0),
      ch3_rxdccdone => pcie_versal_0_phy_GT_RX7_ch_rxdccdone,
      ch3_rxdlyalignerr => pcie_versal_0_phy_GT_RX7_ch_rxdlyalignerr,
      ch3_rxdlyalignprog => pcie_versal_0_phy_GT_RX7_ch_rxdlyalignprog,
      ch3_rxdlyalignreq => '0',
      ch3_rxelecidle => pcie_versal_0_phy_GT_RX7_ch_rxelecidle,
      ch3_rxeqtraining => '0',
      ch3_rxfinealigndone => pcie_versal_0_phy_GT_RX7_ch_rxfinealigndone,
      ch3_rxgearboxslip => '0',
      ch3_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxheader(5 downto 0),
      ch3_rxheadervalid(1 downto 0) => NLW_pcie_versal_0_gt_quad_1_ch3_rxheadervalid_UNCONNECTED(1 downto 0),
      ch3_rxlatclk => '0',
      ch3_rxlpmen => '0',
      ch3_rxmldchaindone => '0',
      ch3_rxmldchainreq => '0',
      ch3_rxmlfinealignreq => '0',
      ch3_rxmstreset => '0',
      ch3_rxmstresetdone => pcie_versal_0_phy_GT_RX7_ch_rxmstresetdone,
      ch3_rxoobreset => '0',
      ch3_rxosintdone => pcie_versal_0_phy_GT_RX7_ch_rxosintdone,
      ch3_rxosintstarted => pcie_versal_0_phy_GT_RX7_ch_rxosintstarted,
      ch3_rxosintstrobedone => pcie_versal_0_phy_GT_RX7_ch_rxosintstrobedone,
      ch3_rxosintstrobestarted => pcie_versal_0_phy_GT_RX7_ch_rxosintstrobestarted,
      ch3_rxoutclk => NLW_pcie_versal_0_gt_quad_1_ch3_rxoutclk_UNCONNECTED,
      ch3_rxpcsresetmask(4 downto 0) => B"11111",
      ch3_rxpd(1 downto 0) => B"00",
      ch3_rxphaligndone => NLW_pcie_versal_0_gt_quad_1_ch3_rxphaligndone_UNCONNECTED,
      ch3_rxphalignerr => pcie_versal_0_phy_GT_RX7_ch_rxphalignerr,
      ch3_rxphalignreq => '0',
      ch3_rxphalignresetmask(1 downto 0) => B"11",
      ch3_rxphdlypd => '0',
      ch3_rxphdlyreset => '0',
      ch3_rxphdlyresetdone => pcie_versal_0_phy_GT_RX7_ch_rxphdlyresetdone,
      ch3_rxphsetinitdone => pcie_versal_0_phy_GT_RX7_ch_rxphsetinitdone,
      ch3_rxphsetinitreq => '0',
      ch3_rxphshift180 => '0',
      ch3_rxphshift180done => pcie_versal_0_phy_GT_RX7_ch_rxphshift180done,
      ch3_rxpmaresetdone => pcie_versal_0_phy_GT_RX7_ch_rxpmaresetdone,
      ch3_rxpmaresetmask(6 downto 0) => B"1111111",
      ch3_rxpolarity => pcie_versal_0_phy_GT_RX7_ch_rxpolarity,
      ch3_rxprbscntreset => '0',
      ch3_rxprbserr => pcie_versal_0_phy_GT_RX7_ch_rxprbserr,
      ch3_rxprbslocked => pcie_versal_0_phy_GT_RX7_ch_rxprbslocked,
      ch3_rxprbssel(3 downto 0) => B"0000",
      ch3_rxprogdivreset => '0',
      ch3_rxprogdivresetdone => pcie_versal_0_phy_GT_RX7_ch_rxprogdivresetdone,
      ch3_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxrate(7 downto 0),
      ch3_rxresetdone => pcie_versal_0_phy_GT_RX7_ch_rxresetdone,
      ch3_rxresetmode(1 downto 0) => B"00",
      ch3_rxslide => '0',
      ch3_rxsliderdy => pcie_versal_0_phy_GT_RX7_ch_rxsliderdy,
      ch3_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxstartofseq(1 downto 0),
      ch3_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxstatus(2 downto 0),
      ch3_rxsyncallin => '0',
      ch3_rxsyncdone => pcie_versal_0_phy_GT_RX7_ch_rxsyncdone,
      ch3_rxtermination => pcie_versal_0_phy_GT_RX7_ch_rxtermination,
      ch3_rxuserrdy => pcie_versal_0_phy_GT_RX7_ch_rxuserrdy,
      ch3_rxusrclk => pcie_versal_0_phy_phy_pclk,
      ch3_rxvalid => pcie_versal_0_phy_GT_RX7_ch_rxvalid,
      ch3_tstin(19 downto 0) => B"00000000000000000000",
      ch3_tx10gstat => pcie_versal_0_phy_GT_TX7_ch_tx10gstat,
      ch3_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txbufstatus(1 downto 0),
      ch3_txcomfinish => pcie_versal_0_phy_GT_TX7_ch_txcomfinish,
      ch3_txcominit => '0',
      ch3_txcomsas => '0',
      ch3_txcomwake => '0',
      ch3_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txctrl0(15 downto 0),
      ch3_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txctrl1(15 downto 0),
      ch3_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txctrl2(7 downto 0),
      ch3_txdapicodeovrden => '0',
      ch3_txdapicodereset => '0',
      ch3_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txdata(127 downto 0),
      ch3_txdataextendrsvd(7 downto 0) => B"00000000",
      ch3_txdccdone => pcie_versal_0_phy_GT_TX7_ch_txdccdone,
      ch3_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txdeemph(1 downto 0),
      ch3_txdetectrx => pcie_versal_0_phy_GT_TX7_ch_txdetectrx,
      ch3_txdiffctrl(4 downto 0) => B"11001",
      ch3_txdlyalignerr => pcie_versal_0_phy_GT_TX7_ch_txdlyalignerr,
      ch3_txdlyalignprog => pcie_versal_0_phy_GT_TX7_ch_txdlyalignprog,
      ch3_txdlyalignreq => '0',
      ch3_txelecidle => pcie_versal_0_phy_GT_TX7_ch_txelecidle,
      ch3_txheader(5 downto 0) => B"000000",
      ch3_txinhibit => '0',
      ch3_txlatclk => '0',
      ch3_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txmaincursor(6 downto 0),
      ch3_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txmargin(2 downto 0),
      ch3_txmldchaindone => '0',
      ch3_txmldchainreq => '0',
      ch3_txmstreset => '0',
      ch3_txmstresetdone => pcie_versal_0_phy_GT_TX7_ch_txmstresetdone,
      ch3_txoneszeros => '0',
      ch3_txoutclk => NLW_pcie_versal_0_gt_quad_1_ch3_txoutclk_UNCONNECTED,
      ch3_txpausedelayalign => '0',
      ch3_txpcsresetmask => '1',
      ch3_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txpd(1 downto 0),
      ch3_txphaligndone => pcie_versal_0_phy_GT_TX7_ch_txphaligndone,
      ch3_txphalignerr => pcie_versal_0_phy_GT_TX7_ch_txphalignerr,
      ch3_txphalignoutrsvd => pcie_versal_0_phy_GT_TX7_ch_txphalignoutrsvd,
      ch3_txphalignreq => '0',
      ch3_txphalignresetmask(1 downto 0) => B"11",
      ch3_txphdlypd => '0',
      ch3_txphdlyreset => '0',
      ch3_txphdlyresetdone => pcie_versal_0_phy_GT_TX7_ch_txphdlyresetdone,
      ch3_txphdlytstclk => '0',
      ch3_txphsetinitdone => pcie_versal_0_phy_GT_TX7_ch_txphsetinitdone,
      ch3_txphsetinitreq => '0',
      ch3_txphshift180 => '0',
      ch3_txphshift180done => pcie_versal_0_phy_GT_TX7_ch_txphshift180done,
      ch3_txpicodeovrden => '0',
      ch3_txpicodereset => '0',
      ch3_txpippmen => '0',
      ch3_txpippmstepsize(4 downto 0) => B"00000",
      ch3_txpisopd => '0',
      ch3_txpmaresetdone => pcie_versal_0_phy_GT_TX7_ch_txpmaresetdone,
      ch3_txpmaresetmask(2 downto 0) => B"111",
      ch3_txpolarity => '0',
      ch3_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txpostcursor(4 downto 0),
      ch3_txprbsforceerr => '0',
      ch3_txprbssel(3 downto 0) => B"0000",
      ch3_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txprecursor(4 downto 0),
      ch3_txprogdivreset => '0',
      ch3_txprogdivresetdone => pcie_versal_0_phy_GT_TX7_ch_txprogdivresetdone,
      ch3_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txrate(7 downto 0),
      ch3_txresetdone => pcie_versal_0_phy_GT_TX7_ch_txresetdone,
      ch3_txresetmode(1 downto 0) => B"00",
      ch3_txsequence(6 downto 0) => B"0000000",
      ch3_txswing => pcie_versal_0_phy_GT_TX7_ch_txswing,
      ch3_txsyncallin => '0',
      ch3_txsyncdone => pcie_versal_0_phy_GT_TX7_ch_txsyncdone,
      ch3_txuserrdy => pcie_versal_0_phy_GT_TX7_ch_txuserrdy,
      ch3_txusrclk => pcie_versal_0_phy_phy_pclk,
      correcterr => NLW_pcie_versal_0_gt_quad_1_correcterr_UNCONNECTED,
      ctrlrsvdin0(15 downto 0) => B"0000000000000000",
      ctrlrsvdin1(13 downto 0) => B"00000000000000",
      ctrlrsvdout(31 downto 0) => NLW_pcie_versal_0_gt_quad_1_ctrlrsvdout_UNCONNECTED(31 downto 0),
      debugtraceclk => '0',
      debugtraceready => '0',
      debugtracetdata(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_debugtracetdata_UNCONNECTED(15 downto 0),
      debugtracetvalid => NLW_pcie_versal_0_gt_quad_1_debugtracetvalid_UNCONNECTED,
      gpi(15 downto 0) => B"0000000000000000",
      gpo(15 downto 0) => NLW_pcie_versal_0_gt_quad_1_gpo_UNCONNECTED(15 downto 0),
      gtpowergood => NLW_pcie_versal_0_gt_quad_1_gtpowergood_UNCONNECTED,
      hsclk0_lcpllclkrsvd0 => '0',
      hsclk0_lcpllclkrsvd1 => '0',
      hsclk0_lcpllfbclklost => NLW_pcie_versal_0_gt_quad_1_hsclk0_lcpllfbclklost_UNCONNECTED,
      hsclk0_lcpllfbdiv(7 downto 0) => B"00000000",
      hsclk0_lcplllock => NLW_pcie_versal_0_gt_quad_1_hsclk0_lcplllock_UNCONNECTED,
      hsclk0_lcpllpd => '0',
      hsclk0_lcpllrefclklost => NLW_pcie_versal_0_gt_quad_1_hsclk0_lcpllrefclklost_UNCONNECTED,
      hsclk0_lcpllrefclkmonitor => NLW_pcie_versal_0_gt_quad_1_hsclk0_lcpllrefclkmonitor_UNCONNECTED,
      hsclk0_lcpllrefclksel(2 downto 0) => B"000",
      hsclk0_lcpllreset => '0',
      hsclk0_lcpllresetbypassmode => '0',
      hsclk0_lcpllresetmask(1 downto 0) => B"11",
      hsclk0_lcpllrsvd0(7 downto 0) => B"00000000",
      hsclk0_lcpllrsvd1(7 downto 0) => B"00000000",
      hsclk0_lcpllrsvdout(7 downto 0) => NLW_pcie_versal_0_gt_quad_1_hsclk0_lcpllrsvdout_UNCONNECTED(7 downto 0),
      hsclk0_lcpllsdmdata(25 downto 0) => B"01000111011100101111000001",
      hsclk0_lcpllsdmtoggle => '0',
      hsclk0_rpllclkrsvd0 => '0',
      hsclk0_rpllclkrsvd1 => '0',
      hsclk0_rpllfbclklost => NLW_pcie_versal_0_gt_quad_1_hsclk0_rpllfbclklost_UNCONNECTED,
      hsclk0_rpllfbdiv(7 downto 0) => B"00000000",
      hsclk0_rplllock => NLW_pcie_versal_0_gt_quad_1_hsclk0_rplllock_UNCONNECTED,
      hsclk0_rpllpd => '0',
      hsclk0_rpllrefclklost => NLW_pcie_versal_0_gt_quad_1_hsclk0_rpllrefclklost_UNCONNECTED,
      hsclk0_rpllrefclkmonitor => NLW_pcie_versal_0_gt_quad_1_hsclk0_rpllrefclkmonitor_UNCONNECTED,
      hsclk0_rpllrefclksel(2 downto 0) => B"001",
      hsclk0_rpllreset => '0',
      hsclk0_rpllresetbypassmode => '0',
      hsclk0_rpllresetmask(1 downto 0) => B"11",
      hsclk0_rpllrsvd0(7 downto 0) => B"00000000",
      hsclk0_rpllrsvd1(7 downto 0) => B"00000000",
      hsclk0_rpllrsvdout(7 downto 0) => NLW_pcie_versal_0_gt_quad_1_hsclk0_rpllrsvdout_UNCONNECTED(7 downto 0),
      hsclk0_rpllsdmdata(25 downto 0) => B"00010001010011010001000000",
      hsclk0_rpllsdmtoggle => '0',
      hsclk0_rxrecclkout0 => NLW_pcie_versal_0_gt_quad_1_hsclk0_rxrecclkout0_UNCONNECTED,
      hsclk0_rxrecclkout1 => NLW_pcie_versal_0_gt_quad_1_hsclk0_rxrecclkout1_UNCONNECTED,
      hsclk1_lcpllclkrsvd0 => '0',
      hsclk1_lcpllclkrsvd1 => '0',
      hsclk1_lcpllfbclklost => NLW_pcie_versal_0_gt_quad_1_hsclk1_lcpllfbclklost_UNCONNECTED,
      hsclk1_lcpllfbdiv(7 downto 0) => B"00000000",
      hsclk1_lcplllock => NLW_pcie_versal_0_gt_quad_1_hsclk1_lcplllock_UNCONNECTED,
      hsclk1_lcpllpd => '0',
      hsclk1_lcpllrefclklost => NLW_pcie_versal_0_gt_quad_1_hsclk1_lcpllrefclklost_UNCONNECTED,
      hsclk1_lcpllrefclkmonitor => NLW_pcie_versal_0_gt_quad_1_hsclk1_lcpllrefclkmonitor_UNCONNECTED,
      hsclk1_lcpllrefclksel(2 downto 0) => B"000",
      hsclk1_lcpllreset => '0',
      hsclk1_lcpllresetbypassmode => '0',
      hsclk1_lcpllresetmask(1 downto 0) => B"11",
      hsclk1_lcpllrsvd0(7 downto 0) => B"00000000",
      hsclk1_lcpllrsvd1(7 downto 0) => B"00000000",
      hsclk1_lcpllrsvdout(7 downto 0) => NLW_pcie_versal_0_gt_quad_1_hsclk1_lcpllrsvdout_UNCONNECTED(7 downto 0),
      hsclk1_lcpllsdmdata(25 downto 0) => B"01000111011100101111000001",
      hsclk1_lcpllsdmtoggle => '0',
      hsclk1_rpllclkrsvd0 => '0',
      hsclk1_rpllclkrsvd1 => '0',
      hsclk1_rpllfbclklost => NLW_pcie_versal_0_gt_quad_1_hsclk1_rpllfbclklost_UNCONNECTED,
      hsclk1_rpllfbdiv(7 downto 0) => B"00000000",
      hsclk1_rplllock => NLW_pcie_versal_0_gt_quad_1_hsclk1_rplllock_UNCONNECTED,
      hsclk1_rpllpd => '0',
      hsclk1_rpllrefclklost => NLW_pcie_versal_0_gt_quad_1_hsclk1_rpllrefclklost_UNCONNECTED,
      hsclk1_rpllrefclkmonitor => NLW_pcie_versal_0_gt_quad_1_hsclk1_rpllrefclkmonitor_UNCONNECTED,
      hsclk1_rpllrefclksel(2 downto 0) => B"001",
      hsclk1_rpllreset => '0',
      hsclk1_rpllresetbypassmode => '0',
      hsclk1_rpllresetmask(1 downto 0) => B"11",
      hsclk1_rpllrsvd0(7 downto 0) => B"00000000",
      hsclk1_rpllrsvd1(7 downto 0) => B"00000000",
      hsclk1_rpllrsvdout(7 downto 0) => NLW_pcie_versal_0_gt_quad_1_hsclk1_rpllrsvdout_UNCONNECTED(7 downto 0),
      hsclk1_rpllsdmdata(25 downto 0) => B"00010001010011010001000000",
      hsclk1_rpllsdmtoggle => '0',
      hsclk1_rxrecclkout0 => NLW_pcie_versal_0_gt_quad_1_hsclk1_rxrecclkout0_UNCONNECTED,
      hsclk1_rxrecclkout1 => NLW_pcie_versal_0_gt_quad_1_hsclk1_rxrecclkout1_UNCONNECTED,
      m0_axis_tdata(31 downto 0) => NLW_pcie_versal_0_gt_quad_1_m0_axis_tdata_UNCONNECTED(31 downto 0),
      m0_axis_tlast => NLW_pcie_versal_0_gt_quad_1_m0_axis_tlast_UNCONNECTED,
      m0_axis_tready => '1',
      m0_axis_tvalid => NLW_pcie_versal_0_gt_quad_1_m0_axis_tvalid_UNCONNECTED,
      m1_axis_tdata(31 downto 0) => NLW_pcie_versal_0_gt_quad_1_m1_axis_tdata_UNCONNECTED(31 downto 0),
      m1_axis_tlast => NLW_pcie_versal_0_gt_quad_1_m1_axis_tlast_UNCONNECTED,
      m1_axis_tready => '1',
      m1_axis_tvalid => NLW_pcie_versal_0_gt_quad_1_m1_axis_tvalid_UNCONNECTED,
      m2_axis_tdata(31 downto 0) => NLW_pcie_versal_0_gt_quad_1_m2_axis_tdata_UNCONNECTED(31 downto 0),
      m2_axis_tlast => NLW_pcie_versal_0_gt_quad_1_m2_axis_tlast_UNCONNECTED,
      m2_axis_tready => '1',
      m2_axis_tvalid => NLW_pcie_versal_0_gt_quad_1_m2_axis_tvalid_UNCONNECTED,
      pcielinkreachtarget => '0',
      pcieltssm(5 downto 0) => pcie_versal_0_phy_gt_pcieltssm(5 downto 0),
      pipenorthin(5 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_PIPENORTHIN(5 downto 0),
      pipenorthout(5 downto 0) => NLW_pcie_versal_0_gt_quad_1_pipenorthout_UNCONNECTED(5 downto 0),
      pipesouthin(5 downto 0) => B"000000",
      pipesouthout(5 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_PIPESOUTHOUT(5 downto 0),
      rcalenb => '0',
      refclk0_clktestsig => '0',
      refclk0_clktestsigint => NLW_pcie_versal_0_gt_quad_1_refclk0_clktestsigint_UNCONNECTED,
      refclk0_gtrefclkpd => '0',
      refclk0_gtrefclkpdint => NLW_pcie_versal_0_gt_quad_1_refclk0_gtrefclkpdint_UNCONNECTED,
      refclk1_clktestsig => '0',
      refclk1_clktestsigint => NLW_pcie_versal_0_gt_quad_1_refclk1_clktestsigint_UNCONNECTED,
      refclk1_gtrefclkpd => '0',
      refclk1_gtrefclkpdint => NLW_pcie_versal_0_gt_quad_1_refclk1_gtrefclkpdint_UNCONNECTED,
      resetdone_northin(1 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RESETDONE_NORTHIN(1 downto 0),
      resetdone_northout(1 downto 0) => NLW_pcie_versal_0_gt_quad_1_resetdone_northout_UNCONNECTED(1 downto 0),
      resetdone_southin(1 downto 0) => B"00",
      resetdone_southout(1 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RESETDONE_SOUTHOUT(1 downto 0),
      rxmarginclk => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginclk,
      rxmarginreqack => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqack,
      rxmarginreqcmd(3 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqcmd(3 downto 0),
      rxmarginreqlanenum(1 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqlanenum(1 downto 0),
      rxmarginreqpayld(7 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqpayld(7 downto 0),
      rxmarginreqreq => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqreq,
      rxmarginresack => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginresack,
      rxmarginrescmd(3 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginrescmd(3 downto 0),
      rxmarginreslanenum(1 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreslanenum(1 downto 0),
      rxmarginrespayld(7 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginrespayld(7 downto 0),
      rxmarginresreq => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginresreq,
      rxn(3 downto 0) => pcie_versal_0_gt_quad_1_GT_Serial_RX_RXN(3 downto 0),
      rxp(3 downto 0) => pcie_versal_0_gt_quad_1_GT_Serial_RX_RXP(3 downto 0),
      rxpinorthin(3 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RXPINORTHIN(3 downto 0),
      rxpinorthout(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_rxpinorthout_UNCONNECTED(3 downto 0),
      rxpisouthin(3 downto 0) => B"0000",
      rxpisouthout(3 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_RXPISOUTHOUT(3 downto 0),
      s0_axis_tdata(31 downto 0) => B"00000000000000000000000000000000",
      s0_axis_tlast => '0',
      s0_axis_tready => NLW_pcie_versal_0_gt_quad_1_s0_axis_tready_UNCONNECTED,
      s0_axis_tvalid => '0',
      s1_axis_tdata(31 downto 0) => B"00000000000000000000000000000000",
      s1_axis_tlast => '0',
      s1_axis_tready => NLW_pcie_versal_0_gt_quad_1_s1_axis_tready_UNCONNECTED,
      s1_axis_tvalid => '0',
      s2_axis_tdata(31 downto 0) => B"00000000000000000000000000000000",
      s2_axis_tlast => '0',
      s2_axis_tready => NLW_pcie_versal_0_gt_quad_1_s2_axis_tready_UNCONNECTED,
      s2_axis_tvalid => '0',
      trigackin0 => NLW_pcie_versal_0_gt_quad_1_trigackin0_UNCONNECTED,
      trigackout0 => '0',
      trigin0 => '0',
      trigout0 => NLW_pcie_versal_0_gt_quad_1_trigout0_UNCONNECTED,
      txn(3 downto 0) => pcie_versal_0_gt_quad_1_GT_Serial_TX_TXN(3 downto 0),
      txp(3 downto 0) => pcie_versal_0_gt_quad_1_GT_Serial_TX_TXP(3 downto 0),
      txpinorthin(3 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_TXPINORTHIN(3 downto 0),
      txpinorthout(3 downto 0) => NLW_pcie_versal_0_gt_quad_1_txpinorthout_UNCONNECTED(3 downto 0),
      txpisouthin(3 downto 0) => B"0000",
      txpisouthout(3 downto 0) => pcie_versal_0_gt_quad_1_GT_NORTHIN_SOUTHOUT_TXPISOUTHOUT(3 downto 0),
      ubenable => '1',
      ubinterrupt => NLW_pcie_versal_0_gt_quad_1_ubinterrupt_UNCONNECTED,
      ubintr(11 downto 0) => B"000000000000",
      ubiolmbrst => '0',
      ubmbrst => '0',
      ubrxuart => '0',
      ubtxuart => NLW_pcie_versal_0_gt_quad_1_ubtxuart_UNCONNECTED,
      uncorrecterr => NLW_pcie_versal_0_gt_quad_1_uncorrecterr_UNCONNECTED
    );
pcie_versal_0_phy: component pcie_phy_versal_0
     port map (
      ch0_cfokovwrrdy0 => '0',
      ch0_cfokovwrrdy1 => '0',
      ch0_eyescandataerror => pcie_versal_0_phy_GT_RX0_ch_eyescandataerror,
      ch0_phyready => pcie_versal_0_gt_quad_0_ch0_phyready,
      ch0_phystatus => pcie_versal_0_gt_quad_0_ch0_phystatus,
      ch0_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rx10gstat(7 downto 0),
      ch0_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxbufstatus(2 downto 0),
      ch0_rxbyteisaligned => pcie_versal_0_phy_GT_RX0_ch_rxbyteisaligned,
      ch0_rxbyterealign => pcie_versal_0_phy_GT_RX0_ch_rxbyterealign,
      ch0_rxcdrlock => pcie_versal_0_phy_GT_RX0_ch_rxcdrlock,
      ch0_rxcdrphdone => pcie_versal_0_phy_GT_RX0_ch_rxcdrphdone,
      ch0_rxchanbondseq => pcie_versal_0_phy_GT_RX0_ch_rxchanbondseq,
      ch0_rxchanisaligned => pcie_versal_0_phy_GT_RX0_ch_rxchanisaligned,
      ch0_rxchanrealign => pcie_versal_0_phy_GT_RX0_ch_rxchanrealign,
      ch0_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxchbondo(4 downto 0),
      ch0_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxclkcorcnt(1 downto 0),
      ch0_rxcominitdet => pcie_versal_0_phy_GT_RX0_ch_rxcominitdet,
      ch0_rxcommadet => pcie_versal_0_phy_GT_RX0_ch_rxcommadet,
      ch0_rxcomsasdet => pcie_versal_0_phy_GT_RX0_ch_rxcomsasdet,
      ch0_rxcomwakedet => pcie_versal_0_phy_GT_RX0_ch_rxcomwakedet,
      ch0_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxctrl0(15 downto 0),
      ch0_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxctrl1(15 downto 0),
      ch0_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxctrl2(7 downto 0),
      ch0_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxctrl3(7 downto 0),
      ch0_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxdata(127 downto 0),
      ch0_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxdataextendrsvd(7 downto 0),
      ch0_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxdatavalid(1 downto 0),
      ch0_rxdccdone => pcie_versal_0_phy_GT_RX0_ch_rxdccdone,
      ch0_rxdlyalignerr => pcie_versal_0_phy_GT_RX0_ch_rxdlyalignerr,
      ch0_rxdlyalignprog => pcie_versal_0_phy_GT_RX0_ch_rxdlyalignprog,
      ch0_rxelecidle => pcie_versal_0_phy_GT_RX0_ch_rxelecidle,
      ch0_rxfinealigndone => pcie_versal_0_phy_GT_RX0_ch_rxfinealigndone,
      ch0_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxheader(5 downto 0),
      ch0_rxmstresetdone => pcie_versal_0_phy_GT_RX0_ch_rxmstresetdone,
      ch0_rxosintdone => pcie_versal_0_phy_GT_RX0_ch_rxosintdone,
      ch0_rxosintstarted => pcie_versal_0_phy_GT_RX0_ch_rxosintstarted,
      ch0_rxosintstrobedone => pcie_versal_0_phy_GT_RX0_ch_rxosintstrobedone,
      ch0_rxosintstrobestarted => pcie_versal_0_phy_GT_RX0_ch_rxosintstrobestarted,
      ch0_rxphalignerr => pcie_versal_0_phy_GT_RX0_ch_rxphalignerr,
      ch0_rxphdlyresetdone => pcie_versal_0_phy_GT_RX0_ch_rxphdlyresetdone,
      ch0_rxphsetinitdone => pcie_versal_0_phy_GT_RX0_ch_rxphsetinitdone,
      ch0_rxphshift180done => pcie_versal_0_phy_GT_RX0_ch_rxphshift180done,
      ch0_rxpmaresetdone => pcie_versal_0_phy_GT_RX0_ch_rxpmaresetdone,
      ch0_rxpolarity => pcie_versal_0_phy_GT_RX0_ch_rxpolarity,
      ch0_rxprbserr => pcie_versal_0_phy_GT_RX0_ch_rxprbserr,
      ch0_rxprbslocked => pcie_versal_0_phy_GT_RX0_ch_rxprbslocked,
      ch0_rxprogdivresetdone => pcie_versal_0_phy_GT_RX0_ch_rxprogdivresetdone,
      ch0_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxrate(7 downto 0),
      ch0_rxrecclkout => '0',
      ch0_rxresetdone => pcie_versal_0_phy_GT_RX0_ch_rxresetdone,
      ch0_rxsliderdy => pcie_versal_0_phy_GT_RX0_ch_rxsliderdy,
      ch0_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxstartofseq(1 downto 0),
      ch0_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX0_ch_rxstatus(2 downto 0),
      ch0_rxsyncdone => pcie_versal_0_phy_GT_RX0_ch_rxsyncdone,
      ch0_rxtermination => pcie_versal_0_phy_GT_RX0_ch_rxtermination,
      ch0_rxuserrdy => pcie_versal_0_phy_GT_RX0_ch_rxuserrdy,
      ch0_rxvalid => pcie_versal_0_phy_GT_RX0_ch_rxvalid,
      ch0_tx10gstat => pcie_versal_0_phy_GT_TX0_ch_tx10gstat,
      ch0_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txbufstatus(1 downto 0),
      ch0_txcomfinish => pcie_versal_0_phy_GT_TX0_ch_txcomfinish,
      ch0_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txctrl0(15 downto 0),
      ch0_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txctrl1(15 downto 0),
      ch0_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txctrl2(7 downto 0),
      ch0_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txdata(127 downto 0),
      ch0_txdccdone => pcie_versal_0_phy_GT_TX0_ch_txdccdone,
      ch0_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txdeemph(1 downto 0),
      ch0_txdetectrx => pcie_versal_0_phy_GT_TX0_ch_txdetectrx,
      ch0_txdlyalignerr => pcie_versal_0_phy_GT_TX0_ch_txdlyalignerr,
      ch0_txdlyalignprog => pcie_versal_0_phy_GT_TX0_ch_txdlyalignprog,
      ch0_txelecidle => pcie_versal_0_phy_GT_TX0_ch_txelecidle,
      ch0_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txmaincursor(6 downto 0),
      ch0_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txmargin(2 downto 0),
      ch0_txmstresetdone => pcie_versal_0_phy_GT_TX0_ch_txmstresetdone,
      ch0_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txpd(1 downto 0),
      ch0_txphaligndone => pcie_versal_0_phy_GT_TX0_ch_txphaligndone,
      ch0_txphalignerr => pcie_versal_0_phy_GT_TX0_ch_txphalignerr,
      ch0_txphalignoutrsvd => pcie_versal_0_phy_GT_TX0_ch_txphalignoutrsvd,
      ch0_txphdlyresetdone => pcie_versal_0_phy_GT_TX0_ch_txphdlyresetdone,
      ch0_txphsetinitdone => pcie_versal_0_phy_GT_TX0_ch_txphsetinitdone,
      ch0_txphshift180done => pcie_versal_0_phy_GT_TX0_ch_txphshift180done,
      ch0_txpmaresetdone => pcie_versal_0_phy_GT_TX0_ch_txpmaresetdone,
      ch0_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txpostcursor(4 downto 0),
      ch0_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txprecursor(4 downto 0),
      ch0_txprogdivresetdone => pcie_versal_0_phy_GT_TX0_ch_txprogdivresetdone,
      ch0_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX0_ch_txrate(7 downto 0),
      ch0_txresetdone => pcie_versal_0_phy_GT_TX0_ch_txresetdone,
      ch0_txswing => pcie_versal_0_phy_GT_TX0_ch_txswing,
      ch0_txsyncdone => pcie_versal_0_phy_GT_TX0_ch_txsyncdone,
      ch0_txuserrdy => pcie_versal_0_phy_GT_TX0_ch_txuserrdy,
      ch1_cfokovwrrdy0 => '0',
      ch1_cfokovwrrdy1 => '0',
      ch1_eyescandataerror => pcie_versal_0_phy_GT_RX1_ch_eyescandataerror,
      ch1_phyready => pcie_versal_0_gt_quad_0_ch1_phyready,
      ch1_phystatus => pcie_versal_0_gt_quad_0_ch1_phystatus,
      ch1_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rx10gstat(7 downto 0),
      ch1_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxbufstatus(2 downto 0),
      ch1_rxbyteisaligned => pcie_versal_0_phy_GT_RX1_ch_rxbyteisaligned,
      ch1_rxbyterealign => pcie_versal_0_phy_GT_RX1_ch_rxbyterealign,
      ch1_rxcdrlock => pcie_versal_0_phy_GT_RX1_ch_rxcdrlock,
      ch1_rxcdrphdone => pcie_versal_0_phy_GT_RX1_ch_rxcdrphdone,
      ch1_rxchanbondseq => pcie_versal_0_phy_GT_RX1_ch_rxchanbondseq,
      ch1_rxchanisaligned => pcie_versal_0_phy_GT_RX1_ch_rxchanisaligned,
      ch1_rxchanrealign => pcie_versal_0_phy_GT_RX1_ch_rxchanrealign,
      ch1_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxchbondo(4 downto 0),
      ch1_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxclkcorcnt(1 downto 0),
      ch1_rxcominitdet => pcie_versal_0_phy_GT_RX1_ch_rxcominitdet,
      ch1_rxcommadet => pcie_versal_0_phy_GT_RX1_ch_rxcommadet,
      ch1_rxcomsasdet => pcie_versal_0_phy_GT_RX1_ch_rxcomsasdet,
      ch1_rxcomwakedet => pcie_versal_0_phy_GT_RX1_ch_rxcomwakedet,
      ch1_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxctrl0(15 downto 0),
      ch1_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxctrl1(15 downto 0),
      ch1_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxctrl2(7 downto 0),
      ch1_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxctrl3(7 downto 0),
      ch1_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxdata(127 downto 0),
      ch1_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxdataextendrsvd(7 downto 0),
      ch1_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxdatavalid(1 downto 0),
      ch1_rxdccdone => pcie_versal_0_phy_GT_RX1_ch_rxdccdone,
      ch1_rxdlyalignerr => pcie_versal_0_phy_GT_RX1_ch_rxdlyalignerr,
      ch1_rxdlyalignprog => pcie_versal_0_phy_GT_RX1_ch_rxdlyalignprog,
      ch1_rxelecidle => pcie_versal_0_phy_GT_RX1_ch_rxelecidle,
      ch1_rxfinealigndone => pcie_versal_0_phy_GT_RX1_ch_rxfinealigndone,
      ch1_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxheader(5 downto 0),
      ch1_rxmstresetdone => pcie_versal_0_phy_GT_RX1_ch_rxmstresetdone,
      ch1_rxosintdone => pcie_versal_0_phy_GT_RX1_ch_rxosintdone,
      ch1_rxosintstarted => pcie_versal_0_phy_GT_RX1_ch_rxosintstarted,
      ch1_rxosintstrobedone => pcie_versal_0_phy_GT_RX1_ch_rxosintstrobedone,
      ch1_rxosintstrobestarted => pcie_versal_0_phy_GT_RX1_ch_rxosintstrobestarted,
      ch1_rxphalignerr => pcie_versal_0_phy_GT_RX1_ch_rxphalignerr,
      ch1_rxphdlyresetdone => pcie_versal_0_phy_GT_RX1_ch_rxphdlyresetdone,
      ch1_rxphsetinitdone => pcie_versal_0_phy_GT_RX1_ch_rxphsetinitdone,
      ch1_rxphshift180done => pcie_versal_0_phy_GT_RX1_ch_rxphshift180done,
      ch1_rxpmaresetdone => pcie_versal_0_phy_GT_RX1_ch_rxpmaresetdone,
      ch1_rxpolarity => pcie_versal_0_phy_GT_RX1_ch_rxpolarity,
      ch1_rxprbserr => pcie_versal_0_phy_GT_RX1_ch_rxprbserr,
      ch1_rxprbslocked => pcie_versal_0_phy_GT_RX1_ch_rxprbslocked,
      ch1_rxprogdivresetdone => pcie_versal_0_phy_GT_RX1_ch_rxprogdivresetdone,
      ch1_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxrate(7 downto 0),
      ch1_rxrecclkout => '0',
      ch1_rxresetdone => pcie_versal_0_phy_GT_RX1_ch_rxresetdone,
      ch1_rxsliderdy => pcie_versal_0_phy_GT_RX1_ch_rxsliderdy,
      ch1_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxstartofseq(1 downto 0),
      ch1_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX1_ch_rxstatus(2 downto 0),
      ch1_rxsyncdone => pcie_versal_0_phy_GT_RX1_ch_rxsyncdone,
      ch1_rxtermination => pcie_versal_0_phy_GT_RX1_ch_rxtermination,
      ch1_rxuserrdy => pcie_versal_0_phy_GT_RX1_ch_rxuserrdy,
      ch1_rxvalid => pcie_versal_0_phy_GT_RX1_ch_rxvalid,
      ch1_tx10gstat => pcie_versal_0_phy_GT_TX1_ch_tx10gstat,
      ch1_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txbufstatus(1 downto 0),
      ch1_txcomfinish => pcie_versal_0_phy_GT_TX1_ch_txcomfinish,
      ch1_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txctrl0(15 downto 0),
      ch1_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txctrl1(15 downto 0),
      ch1_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txctrl2(7 downto 0),
      ch1_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txdata(127 downto 0),
      ch1_txdccdone => pcie_versal_0_phy_GT_TX1_ch_txdccdone,
      ch1_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txdeemph(1 downto 0),
      ch1_txdetectrx => pcie_versal_0_phy_GT_TX1_ch_txdetectrx,
      ch1_txdlyalignerr => pcie_versal_0_phy_GT_TX1_ch_txdlyalignerr,
      ch1_txdlyalignprog => pcie_versal_0_phy_GT_TX1_ch_txdlyalignprog,
      ch1_txelecidle => pcie_versal_0_phy_GT_TX1_ch_txelecidle,
      ch1_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txmaincursor(6 downto 0),
      ch1_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txmargin(2 downto 0),
      ch1_txmstresetdone => pcie_versal_0_phy_GT_TX1_ch_txmstresetdone,
      ch1_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txpd(1 downto 0),
      ch1_txphaligndone => pcie_versal_0_phy_GT_TX1_ch_txphaligndone,
      ch1_txphalignerr => pcie_versal_0_phy_GT_TX1_ch_txphalignerr,
      ch1_txphalignoutrsvd => pcie_versal_0_phy_GT_TX1_ch_txphalignoutrsvd,
      ch1_txphdlyresetdone => pcie_versal_0_phy_GT_TX1_ch_txphdlyresetdone,
      ch1_txphsetinitdone => pcie_versal_0_phy_GT_TX1_ch_txphsetinitdone,
      ch1_txphshift180done => pcie_versal_0_phy_GT_TX1_ch_txphshift180done,
      ch1_txpmaresetdone => pcie_versal_0_phy_GT_TX1_ch_txpmaresetdone,
      ch1_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txpostcursor(4 downto 0),
      ch1_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txprecursor(4 downto 0),
      ch1_txprogdivresetdone => pcie_versal_0_phy_GT_TX1_ch_txprogdivresetdone,
      ch1_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX1_ch_txrate(7 downto 0),
      ch1_txresetdone => pcie_versal_0_phy_GT_TX1_ch_txresetdone,
      ch1_txswing => pcie_versal_0_phy_GT_TX1_ch_txswing,
      ch1_txsyncdone => pcie_versal_0_phy_GT_TX1_ch_txsyncdone,
      ch1_txuserrdy => pcie_versal_0_phy_GT_TX1_ch_txuserrdy,
      ch2_cfokovwrrdy0 => '0',
      ch2_cfokovwrrdy1 => '0',
      ch2_eyescandataerror => pcie_versal_0_phy_GT_RX2_ch_eyescandataerror,
      ch2_phyready => pcie_versal_0_gt_quad_0_ch2_phyready,
      ch2_phystatus => pcie_versal_0_gt_quad_0_ch2_phystatus,
      ch2_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rx10gstat(7 downto 0),
      ch2_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxbufstatus(2 downto 0),
      ch2_rxbyteisaligned => pcie_versal_0_phy_GT_RX2_ch_rxbyteisaligned,
      ch2_rxbyterealign => pcie_versal_0_phy_GT_RX2_ch_rxbyterealign,
      ch2_rxcdrlock => pcie_versal_0_phy_GT_RX2_ch_rxcdrlock,
      ch2_rxcdrphdone => pcie_versal_0_phy_GT_RX2_ch_rxcdrphdone,
      ch2_rxchanbondseq => pcie_versal_0_phy_GT_RX2_ch_rxchanbondseq,
      ch2_rxchanisaligned => pcie_versal_0_phy_GT_RX2_ch_rxchanisaligned,
      ch2_rxchanrealign => pcie_versal_0_phy_GT_RX2_ch_rxchanrealign,
      ch2_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxchbondo(4 downto 0),
      ch2_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxclkcorcnt(1 downto 0),
      ch2_rxcominitdet => pcie_versal_0_phy_GT_RX2_ch_rxcominitdet,
      ch2_rxcommadet => pcie_versal_0_phy_GT_RX2_ch_rxcommadet,
      ch2_rxcomsasdet => pcie_versal_0_phy_GT_RX2_ch_rxcomsasdet,
      ch2_rxcomwakedet => pcie_versal_0_phy_GT_RX2_ch_rxcomwakedet,
      ch2_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxctrl0(15 downto 0),
      ch2_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxctrl1(15 downto 0),
      ch2_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxctrl2(7 downto 0),
      ch2_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxctrl3(7 downto 0),
      ch2_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxdata(127 downto 0),
      ch2_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxdataextendrsvd(7 downto 0),
      ch2_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxdatavalid(1 downto 0),
      ch2_rxdccdone => pcie_versal_0_phy_GT_RX2_ch_rxdccdone,
      ch2_rxdlyalignerr => pcie_versal_0_phy_GT_RX2_ch_rxdlyalignerr,
      ch2_rxdlyalignprog => pcie_versal_0_phy_GT_RX2_ch_rxdlyalignprog,
      ch2_rxelecidle => pcie_versal_0_phy_GT_RX2_ch_rxelecidle,
      ch2_rxfinealigndone => pcie_versal_0_phy_GT_RX2_ch_rxfinealigndone,
      ch2_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxheader(5 downto 0),
      ch2_rxmstresetdone => pcie_versal_0_phy_GT_RX2_ch_rxmstresetdone,
      ch2_rxosintdone => pcie_versal_0_phy_GT_RX2_ch_rxosintdone,
      ch2_rxosintstarted => pcie_versal_0_phy_GT_RX2_ch_rxosintstarted,
      ch2_rxosintstrobedone => pcie_versal_0_phy_GT_RX2_ch_rxosintstrobedone,
      ch2_rxosintstrobestarted => pcie_versal_0_phy_GT_RX2_ch_rxosintstrobestarted,
      ch2_rxphalignerr => pcie_versal_0_phy_GT_RX2_ch_rxphalignerr,
      ch2_rxphdlyresetdone => pcie_versal_0_phy_GT_RX2_ch_rxphdlyresetdone,
      ch2_rxphsetinitdone => pcie_versal_0_phy_GT_RX2_ch_rxphsetinitdone,
      ch2_rxphshift180done => pcie_versal_0_phy_GT_RX2_ch_rxphshift180done,
      ch2_rxpmaresetdone => pcie_versal_0_phy_GT_RX2_ch_rxpmaresetdone,
      ch2_rxpolarity => pcie_versal_0_phy_GT_RX2_ch_rxpolarity,
      ch2_rxprbserr => pcie_versal_0_phy_GT_RX2_ch_rxprbserr,
      ch2_rxprbslocked => pcie_versal_0_phy_GT_RX2_ch_rxprbslocked,
      ch2_rxprogdivresetdone => pcie_versal_0_phy_GT_RX2_ch_rxprogdivresetdone,
      ch2_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxrate(7 downto 0),
      ch2_rxrecclkout => '0',
      ch2_rxresetdone => pcie_versal_0_phy_GT_RX2_ch_rxresetdone,
      ch2_rxsliderdy => pcie_versal_0_phy_GT_RX2_ch_rxsliderdy,
      ch2_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxstartofseq(1 downto 0),
      ch2_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX2_ch_rxstatus(2 downto 0),
      ch2_rxsyncdone => pcie_versal_0_phy_GT_RX2_ch_rxsyncdone,
      ch2_rxtermination => pcie_versal_0_phy_GT_RX2_ch_rxtermination,
      ch2_rxuserrdy => pcie_versal_0_phy_GT_RX2_ch_rxuserrdy,
      ch2_rxvalid => pcie_versal_0_phy_GT_RX2_ch_rxvalid,
      ch2_tx10gstat => pcie_versal_0_phy_GT_TX2_ch_tx10gstat,
      ch2_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txbufstatus(1 downto 0),
      ch2_txcomfinish => pcie_versal_0_phy_GT_TX2_ch_txcomfinish,
      ch2_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txctrl0(15 downto 0),
      ch2_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txctrl1(15 downto 0),
      ch2_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txctrl2(7 downto 0),
      ch2_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txdata(127 downto 0),
      ch2_txdccdone => pcie_versal_0_phy_GT_TX2_ch_txdccdone,
      ch2_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txdeemph(1 downto 0),
      ch2_txdetectrx => pcie_versal_0_phy_GT_TX2_ch_txdetectrx,
      ch2_txdlyalignerr => pcie_versal_0_phy_GT_TX2_ch_txdlyalignerr,
      ch2_txdlyalignprog => pcie_versal_0_phy_GT_TX2_ch_txdlyalignprog,
      ch2_txelecidle => pcie_versal_0_phy_GT_TX2_ch_txelecidle,
      ch2_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txmaincursor(6 downto 0),
      ch2_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txmargin(2 downto 0),
      ch2_txmstresetdone => pcie_versal_0_phy_GT_TX2_ch_txmstresetdone,
      ch2_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txpd(1 downto 0),
      ch2_txphaligndone => pcie_versal_0_phy_GT_TX2_ch_txphaligndone,
      ch2_txphalignerr => pcie_versal_0_phy_GT_TX2_ch_txphalignerr,
      ch2_txphalignoutrsvd => pcie_versal_0_phy_GT_TX2_ch_txphalignoutrsvd,
      ch2_txphdlyresetdone => pcie_versal_0_phy_GT_TX2_ch_txphdlyresetdone,
      ch2_txphsetinitdone => pcie_versal_0_phy_GT_TX2_ch_txphsetinitdone,
      ch2_txphshift180done => pcie_versal_0_phy_GT_TX2_ch_txphshift180done,
      ch2_txpmaresetdone => pcie_versal_0_phy_GT_TX2_ch_txpmaresetdone,
      ch2_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txpostcursor(4 downto 0),
      ch2_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txprecursor(4 downto 0),
      ch2_txprogdivresetdone => pcie_versal_0_phy_GT_TX2_ch_txprogdivresetdone,
      ch2_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX2_ch_txrate(7 downto 0),
      ch2_txresetdone => pcie_versal_0_phy_GT_TX2_ch_txresetdone,
      ch2_txswing => pcie_versal_0_phy_GT_TX2_ch_txswing,
      ch2_txsyncdone => pcie_versal_0_phy_GT_TX2_ch_txsyncdone,
      ch2_txuserrdy => pcie_versal_0_phy_GT_TX2_ch_txuserrdy,
      ch3_cfokovwrrdy0 => '0',
      ch3_cfokovwrrdy1 => '0',
      ch3_eyescandataerror => pcie_versal_0_phy_GT_RX3_ch_eyescandataerror,
      ch3_phyready => pcie_versal_0_gt_quad_0_ch3_phyready,
      ch3_phystatus => pcie_versal_0_gt_quad_0_ch3_phystatus,
      ch3_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rx10gstat(7 downto 0),
      ch3_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxbufstatus(2 downto 0),
      ch3_rxbyteisaligned => pcie_versal_0_phy_GT_RX3_ch_rxbyteisaligned,
      ch3_rxbyterealign => pcie_versal_0_phy_GT_RX3_ch_rxbyterealign,
      ch3_rxcdrlock => pcie_versal_0_phy_GT_RX3_ch_rxcdrlock,
      ch3_rxcdrphdone => pcie_versal_0_phy_GT_RX3_ch_rxcdrphdone,
      ch3_rxchanbondseq => pcie_versal_0_phy_GT_RX3_ch_rxchanbondseq,
      ch3_rxchanisaligned => pcie_versal_0_phy_GT_RX3_ch_rxchanisaligned,
      ch3_rxchanrealign => pcie_versal_0_phy_GT_RX3_ch_rxchanrealign,
      ch3_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxchbondo(4 downto 0),
      ch3_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxclkcorcnt(1 downto 0),
      ch3_rxcominitdet => pcie_versal_0_phy_GT_RX3_ch_rxcominitdet,
      ch3_rxcommadet => pcie_versal_0_phy_GT_RX3_ch_rxcommadet,
      ch3_rxcomsasdet => pcie_versal_0_phy_GT_RX3_ch_rxcomsasdet,
      ch3_rxcomwakedet => pcie_versal_0_phy_GT_RX3_ch_rxcomwakedet,
      ch3_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxctrl0(15 downto 0),
      ch3_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxctrl1(15 downto 0),
      ch3_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxctrl2(7 downto 0),
      ch3_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxctrl3(7 downto 0),
      ch3_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxdata(127 downto 0),
      ch3_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxdataextendrsvd(7 downto 0),
      ch3_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxdatavalid(1 downto 0),
      ch3_rxdccdone => pcie_versal_0_phy_GT_RX3_ch_rxdccdone,
      ch3_rxdlyalignerr => pcie_versal_0_phy_GT_RX3_ch_rxdlyalignerr,
      ch3_rxdlyalignprog => pcie_versal_0_phy_GT_RX3_ch_rxdlyalignprog,
      ch3_rxelecidle => pcie_versal_0_phy_GT_RX3_ch_rxelecidle,
      ch3_rxfinealigndone => pcie_versal_0_phy_GT_RX3_ch_rxfinealigndone,
      ch3_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxheader(5 downto 0),
      ch3_rxmstresetdone => pcie_versal_0_phy_GT_RX3_ch_rxmstresetdone,
      ch3_rxosintdone => pcie_versal_0_phy_GT_RX3_ch_rxosintdone,
      ch3_rxosintstarted => pcie_versal_0_phy_GT_RX3_ch_rxosintstarted,
      ch3_rxosintstrobedone => pcie_versal_0_phy_GT_RX3_ch_rxosintstrobedone,
      ch3_rxosintstrobestarted => pcie_versal_0_phy_GT_RX3_ch_rxosintstrobestarted,
      ch3_rxphalignerr => pcie_versal_0_phy_GT_RX3_ch_rxphalignerr,
      ch3_rxphdlyresetdone => pcie_versal_0_phy_GT_RX3_ch_rxphdlyresetdone,
      ch3_rxphsetinitdone => pcie_versal_0_phy_GT_RX3_ch_rxphsetinitdone,
      ch3_rxphshift180done => pcie_versal_0_phy_GT_RX3_ch_rxphshift180done,
      ch3_rxpmaresetdone => pcie_versal_0_phy_GT_RX3_ch_rxpmaresetdone,
      ch3_rxpolarity => pcie_versal_0_phy_GT_RX3_ch_rxpolarity,
      ch3_rxprbserr => pcie_versal_0_phy_GT_RX3_ch_rxprbserr,
      ch3_rxprbslocked => pcie_versal_0_phy_GT_RX3_ch_rxprbslocked,
      ch3_rxprogdivresetdone => pcie_versal_0_phy_GT_RX3_ch_rxprogdivresetdone,
      ch3_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxrate(7 downto 0),
      ch3_rxrecclkout => '0',
      ch3_rxresetdone => pcie_versal_0_phy_GT_RX3_ch_rxresetdone,
      ch3_rxsliderdy => pcie_versal_0_phy_GT_RX3_ch_rxsliderdy,
      ch3_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxstartofseq(1 downto 0),
      ch3_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX3_ch_rxstatus(2 downto 0),
      ch3_rxsyncdone => pcie_versal_0_phy_GT_RX3_ch_rxsyncdone,
      ch3_rxtermination => pcie_versal_0_phy_GT_RX3_ch_rxtermination,
      ch3_rxuserrdy => pcie_versal_0_phy_GT_RX3_ch_rxuserrdy,
      ch3_rxvalid => pcie_versal_0_phy_GT_RX3_ch_rxvalid,
      ch3_tx10gstat => pcie_versal_0_phy_GT_TX3_ch_tx10gstat,
      ch3_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txbufstatus(1 downto 0),
      ch3_txcomfinish => pcie_versal_0_phy_GT_TX3_ch_txcomfinish,
      ch3_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txctrl0(15 downto 0),
      ch3_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txctrl1(15 downto 0),
      ch3_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txctrl2(7 downto 0),
      ch3_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txdata(127 downto 0),
      ch3_txdccdone => pcie_versal_0_phy_GT_TX3_ch_txdccdone,
      ch3_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txdeemph(1 downto 0),
      ch3_txdetectrx => pcie_versal_0_phy_GT_TX3_ch_txdetectrx,
      ch3_txdlyalignerr => pcie_versal_0_phy_GT_TX3_ch_txdlyalignerr,
      ch3_txdlyalignprog => pcie_versal_0_phy_GT_TX3_ch_txdlyalignprog,
      ch3_txelecidle => pcie_versal_0_phy_GT_TX3_ch_txelecidle,
      ch3_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txmaincursor(6 downto 0),
      ch3_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txmargin(2 downto 0),
      ch3_txmstresetdone => pcie_versal_0_phy_GT_TX3_ch_txmstresetdone,
      ch3_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txpd(1 downto 0),
      ch3_txphaligndone => pcie_versal_0_phy_GT_TX3_ch_txphaligndone,
      ch3_txphalignerr => pcie_versal_0_phy_GT_TX3_ch_txphalignerr,
      ch3_txphalignoutrsvd => pcie_versal_0_phy_GT_TX3_ch_txphalignoutrsvd,
      ch3_txphdlyresetdone => pcie_versal_0_phy_GT_TX3_ch_txphdlyresetdone,
      ch3_txphsetinitdone => pcie_versal_0_phy_GT_TX3_ch_txphsetinitdone,
      ch3_txphshift180done => pcie_versal_0_phy_GT_TX3_ch_txphshift180done,
      ch3_txpmaresetdone => pcie_versal_0_phy_GT_TX3_ch_txpmaresetdone,
      ch3_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txpostcursor(4 downto 0),
      ch3_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txprecursor(4 downto 0),
      ch3_txprogdivresetdone => pcie_versal_0_phy_GT_TX3_ch_txprogdivresetdone,
      ch3_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX3_ch_txrate(7 downto 0),
      ch3_txresetdone => pcie_versal_0_phy_GT_TX3_ch_txresetdone,
      ch3_txswing => pcie_versal_0_phy_GT_TX3_ch_txswing,
      ch3_txsyncdone => pcie_versal_0_phy_GT_TX3_ch_txsyncdone,
      ch3_txuserrdy => pcie_versal_0_phy_GT_TX3_ch_txuserrdy,
      ch4_cfokovwrrdy0 => '0',
      ch4_cfokovwrrdy1 => '0',
      ch4_eyescandataerror => pcie_versal_0_phy_GT_RX4_ch_eyescandataerror,
      ch4_phyready => pcie_versal_0_gt_quad_1_ch0_phyready,
      ch4_phystatus => pcie_versal_0_gt_quad_1_ch0_phystatus,
      ch4_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rx10gstat(7 downto 0),
      ch4_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxbufstatus(2 downto 0),
      ch4_rxbyteisaligned => pcie_versal_0_phy_GT_RX4_ch_rxbyteisaligned,
      ch4_rxbyterealign => pcie_versal_0_phy_GT_RX4_ch_rxbyterealign,
      ch4_rxcdrlock => pcie_versal_0_phy_GT_RX4_ch_rxcdrlock,
      ch4_rxcdrphdone => pcie_versal_0_phy_GT_RX4_ch_rxcdrphdone,
      ch4_rxchanbondseq => pcie_versal_0_phy_GT_RX4_ch_rxchanbondseq,
      ch4_rxchanisaligned => pcie_versal_0_phy_GT_RX4_ch_rxchanisaligned,
      ch4_rxchanrealign => pcie_versal_0_phy_GT_RX4_ch_rxchanrealign,
      ch4_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxchbondo(4 downto 0),
      ch4_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxclkcorcnt(1 downto 0),
      ch4_rxcominitdet => pcie_versal_0_phy_GT_RX4_ch_rxcominitdet,
      ch4_rxcommadet => pcie_versal_0_phy_GT_RX4_ch_rxcommadet,
      ch4_rxcomsasdet => pcie_versal_0_phy_GT_RX4_ch_rxcomsasdet,
      ch4_rxcomwakedet => pcie_versal_0_phy_GT_RX4_ch_rxcomwakedet,
      ch4_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxctrl0(15 downto 0),
      ch4_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxctrl1(15 downto 0),
      ch4_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxctrl2(7 downto 0),
      ch4_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxctrl3(7 downto 0),
      ch4_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxdata(127 downto 0),
      ch4_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxdataextendrsvd(7 downto 0),
      ch4_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxdatavalid(1 downto 0),
      ch4_rxdccdone => pcie_versal_0_phy_GT_RX4_ch_rxdccdone,
      ch4_rxdlyalignerr => pcie_versal_0_phy_GT_RX4_ch_rxdlyalignerr,
      ch4_rxdlyalignprog => pcie_versal_0_phy_GT_RX4_ch_rxdlyalignprog,
      ch4_rxelecidle => pcie_versal_0_phy_GT_RX4_ch_rxelecidle,
      ch4_rxfinealigndone => pcie_versal_0_phy_GT_RX4_ch_rxfinealigndone,
      ch4_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxheader(5 downto 0),
      ch4_rxmstresetdone => pcie_versal_0_phy_GT_RX4_ch_rxmstresetdone,
      ch4_rxosintdone => pcie_versal_0_phy_GT_RX4_ch_rxosintdone,
      ch4_rxosintstarted => pcie_versal_0_phy_GT_RX4_ch_rxosintstarted,
      ch4_rxosintstrobedone => pcie_versal_0_phy_GT_RX4_ch_rxosintstrobedone,
      ch4_rxosintstrobestarted => pcie_versal_0_phy_GT_RX4_ch_rxosintstrobestarted,
      ch4_rxphalignerr => pcie_versal_0_phy_GT_RX4_ch_rxphalignerr,
      ch4_rxphdlyresetdone => pcie_versal_0_phy_GT_RX4_ch_rxphdlyresetdone,
      ch4_rxphsetinitdone => pcie_versal_0_phy_GT_RX4_ch_rxphsetinitdone,
      ch4_rxphshift180done => pcie_versal_0_phy_GT_RX4_ch_rxphshift180done,
      ch4_rxpmaresetdone => pcie_versal_0_phy_GT_RX4_ch_rxpmaresetdone,
      ch4_rxpolarity => pcie_versal_0_phy_GT_RX4_ch_rxpolarity,
      ch4_rxprbserr => pcie_versal_0_phy_GT_RX4_ch_rxprbserr,
      ch4_rxprbslocked => pcie_versal_0_phy_GT_RX4_ch_rxprbslocked,
      ch4_rxprogdivresetdone => pcie_versal_0_phy_GT_RX4_ch_rxprogdivresetdone,
      ch4_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxrate(7 downto 0),
      ch4_rxrecclkout => '0',
      ch4_rxresetdone => pcie_versal_0_phy_GT_RX4_ch_rxresetdone,
      ch4_rxsliderdy => pcie_versal_0_phy_GT_RX4_ch_rxsliderdy,
      ch4_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxstartofseq(1 downto 0),
      ch4_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX4_ch_rxstatus(2 downto 0),
      ch4_rxsyncdone => pcie_versal_0_phy_GT_RX4_ch_rxsyncdone,
      ch4_rxtermination => pcie_versal_0_phy_GT_RX4_ch_rxtermination,
      ch4_rxuserrdy => pcie_versal_0_phy_GT_RX4_ch_rxuserrdy,
      ch4_rxvalid => pcie_versal_0_phy_GT_RX4_ch_rxvalid,
      ch4_tx10gstat => pcie_versal_0_phy_GT_TX4_ch_tx10gstat,
      ch4_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txbufstatus(1 downto 0),
      ch4_txcomfinish => pcie_versal_0_phy_GT_TX4_ch_txcomfinish,
      ch4_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txctrl0(15 downto 0),
      ch4_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txctrl1(15 downto 0),
      ch4_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txctrl2(7 downto 0),
      ch4_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txdata(127 downto 0),
      ch4_txdccdone => pcie_versal_0_phy_GT_TX4_ch_txdccdone,
      ch4_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txdeemph(1 downto 0),
      ch4_txdetectrx => pcie_versal_0_phy_GT_TX4_ch_txdetectrx,
      ch4_txdlyalignerr => pcie_versal_0_phy_GT_TX4_ch_txdlyalignerr,
      ch4_txdlyalignprog => pcie_versal_0_phy_GT_TX4_ch_txdlyalignprog,
      ch4_txelecidle => pcie_versal_0_phy_GT_TX4_ch_txelecidle,
      ch4_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txmaincursor(6 downto 0),
      ch4_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txmargin(2 downto 0),
      ch4_txmstresetdone => pcie_versal_0_phy_GT_TX4_ch_txmstresetdone,
      ch4_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txpd(1 downto 0),
      ch4_txphaligndone => pcie_versal_0_phy_GT_TX4_ch_txphaligndone,
      ch4_txphalignerr => pcie_versal_0_phy_GT_TX4_ch_txphalignerr,
      ch4_txphalignoutrsvd => pcie_versal_0_phy_GT_TX4_ch_txphalignoutrsvd,
      ch4_txphdlyresetdone => pcie_versal_0_phy_GT_TX4_ch_txphdlyresetdone,
      ch4_txphsetinitdone => pcie_versal_0_phy_GT_TX4_ch_txphsetinitdone,
      ch4_txphshift180done => pcie_versal_0_phy_GT_TX4_ch_txphshift180done,
      ch4_txpmaresetdone => pcie_versal_0_phy_GT_TX4_ch_txpmaresetdone,
      ch4_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txpostcursor(4 downto 0),
      ch4_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txprecursor(4 downto 0),
      ch4_txprogdivresetdone => pcie_versal_0_phy_GT_TX4_ch_txprogdivresetdone,
      ch4_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX4_ch_txrate(7 downto 0),
      ch4_txresetdone => pcie_versal_0_phy_GT_TX4_ch_txresetdone,
      ch4_txswing => pcie_versal_0_phy_GT_TX4_ch_txswing,
      ch4_txsyncdone => pcie_versal_0_phy_GT_TX4_ch_txsyncdone,
      ch4_txuserrdy => pcie_versal_0_phy_GT_TX4_ch_txuserrdy,
      ch5_cfokovwrrdy0 => '0',
      ch5_cfokovwrrdy1 => '0',
      ch5_eyescandataerror => pcie_versal_0_phy_GT_RX5_ch_eyescandataerror,
      ch5_phyready => pcie_versal_0_gt_quad_1_ch1_phyready,
      ch5_phystatus => pcie_versal_0_gt_quad_1_ch1_phystatus,
      ch5_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rx10gstat(7 downto 0),
      ch5_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxbufstatus(2 downto 0),
      ch5_rxbyteisaligned => pcie_versal_0_phy_GT_RX5_ch_rxbyteisaligned,
      ch5_rxbyterealign => pcie_versal_0_phy_GT_RX5_ch_rxbyterealign,
      ch5_rxcdrlock => pcie_versal_0_phy_GT_RX5_ch_rxcdrlock,
      ch5_rxcdrphdone => pcie_versal_0_phy_GT_RX5_ch_rxcdrphdone,
      ch5_rxchanbondseq => pcie_versal_0_phy_GT_RX5_ch_rxchanbondseq,
      ch5_rxchanisaligned => pcie_versal_0_phy_GT_RX5_ch_rxchanisaligned,
      ch5_rxchanrealign => pcie_versal_0_phy_GT_RX5_ch_rxchanrealign,
      ch5_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxchbondo(4 downto 0),
      ch5_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxclkcorcnt(1 downto 0),
      ch5_rxcominitdet => pcie_versal_0_phy_GT_RX5_ch_rxcominitdet,
      ch5_rxcommadet => pcie_versal_0_phy_GT_RX5_ch_rxcommadet,
      ch5_rxcomsasdet => pcie_versal_0_phy_GT_RX5_ch_rxcomsasdet,
      ch5_rxcomwakedet => pcie_versal_0_phy_GT_RX5_ch_rxcomwakedet,
      ch5_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxctrl0(15 downto 0),
      ch5_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxctrl1(15 downto 0),
      ch5_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxctrl2(7 downto 0),
      ch5_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxctrl3(7 downto 0),
      ch5_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxdata(127 downto 0),
      ch5_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxdataextendrsvd(7 downto 0),
      ch5_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxdatavalid(1 downto 0),
      ch5_rxdccdone => pcie_versal_0_phy_GT_RX5_ch_rxdccdone,
      ch5_rxdlyalignerr => pcie_versal_0_phy_GT_RX5_ch_rxdlyalignerr,
      ch5_rxdlyalignprog => pcie_versal_0_phy_GT_RX5_ch_rxdlyalignprog,
      ch5_rxelecidle => pcie_versal_0_phy_GT_RX5_ch_rxelecidle,
      ch5_rxfinealigndone => pcie_versal_0_phy_GT_RX5_ch_rxfinealigndone,
      ch5_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxheader(5 downto 0),
      ch5_rxmstresetdone => pcie_versal_0_phy_GT_RX5_ch_rxmstresetdone,
      ch5_rxosintdone => pcie_versal_0_phy_GT_RX5_ch_rxosintdone,
      ch5_rxosintstarted => pcie_versal_0_phy_GT_RX5_ch_rxosintstarted,
      ch5_rxosintstrobedone => pcie_versal_0_phy_GT_RX5_ch_rxosintstrobedone,
      ch5_rxosintstrobestarted => pcie_versal_0_phy_GT_RX5_ch_rxosintstrobestarted,
      ch5_rxphalignerr => pcie_versal_0_phy_GT_RX5_ch_rxphalignerr,
      ch5_rxphdlyresetdone => pcie_versal_0_phy_GT_RX5_ch_rxphdlyresetdone,
      ch5_rxphsetinitdone => pcie_versal_0_phy_GT_RX5_ch_rxphsetinitdone,
      ch5_rxphshift180done => pcie_versal_0_phy_GT_RX5_ch_rxphshift180done,
      ch5_rxpmaresetdone => pcie_versal_0_phy_GT_RX5_ch_rxpmaresetdone,
      ch5_rxpolarity => pcie_versal_0_phy_GT_RX5_ch_rxpolarity,
      ch5_rxprbserr => pcie_versal_0_phy_GT_RX5_ch_rxprbserr,
      ch5_rxprbslocked => pcie_versal_0_phy_GT_RX5_ch_rxprbslocked,
      ch5_rxprogdivresetdone => pcie_versal_0_phy_GT_RX5_ch_rxprogdivresetdone,
      ch5_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxrate(7 downto 0),
      ch5_rxrecclkout => '0',
      ch5_rxresetdone => pcie_versal_0_phy_GT_RX5_ch_rxresetdone,
      ch5_rxsliderdy => pcie_versal_0_phy_GT_RX5_ch_rxsliderdy,
      ch5_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxstartofseq(1 downto 0),
      ch5_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX5_ch_rxstatus(2 downto 0),
      ch5_rxsyncdone => pcie_versal_0_phy_GT_RX5_ch_rxsyncdone,
      ch5_rxtermination => pcie_versal_0_phy_GT_RX5_ch_rxtermination,
      ch5_rxuserrdy => pcie_versal_0_phy_GT_RX5_ch_rxuserrdy,
      ch5_rxvalid => pcie_versal_0_phy_GT_RX5_ch_rxvalid,
      ch5_tx10gstat => pcie_versal_0_phy_GT_TX5_ch_tx10gstat,
      ch5_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txbufstatus(1 downto 0),
      ch5_txcomfinish => pcie_versal_0_phy_GT_TX5_ch_txcomfinish,
      ch5_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txctrl0(15 downto 0),
      ch5_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txctrl1(15 downto 0),
      ch5_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txctrl2(7 downto 0),
      ch5_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txdata(127 downto 0),
      ch5_txdccdone => pcie_versal_0_phy_GT_TX5_ch_txdccdone,
      ch5_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txdeemph(1 downto 0),
      ch5_txdetectrx => pcie_versal_0_phy_GT_TX5_ch_txdetectrx,
      ch5_txdlyalignerr => pcie_versal_0_phy_GT_TX5_ch_txdlyalignerr,
      ch5_txdlyalignprog => pcie_versal_0_phy_GT_TX5_ch_txdlyalignprog,
      ch5_txelecidle => pcie_versal_0_phy_GT_TX5_ch_txelecidle,
      ch5_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txmaincursor(6 downto 0),
      ch5_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txmargin(2 downto 0),
      ch5_txmstresetdone => pcie_versal_0_phy_GT_TX5_ch_txmstresetdone,
      ch5_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txpd(1 downto 0),
      ch5_txphaligndone => pcie_versal_0_phy_GT_TX5_ch_txphaligndone,
      ch5_txphalignerr => pcie_versal_0_phy_GT_TX5_ch_txphalignerr,
      ch5_txphalignoutrsvd => pcie_versal_0_phy_GT_TX5_ch_txphalignoutrsvd,
      ch5_txphdlyresetdone => pcie_versal_0_phy_GT_TX5_ch_txphdlyresetdone,
      ch5_txphsetinitdone => pcie_versal_0_phy_GT_TX5_ch_txphsetinitdone,
      ch5_txphshift180done => pcie_versal_0_phy_GT_TX5_ch_txphshift180done,
      ch5_txpmaresetdone => pcie_versal_0_phy_GT_TX5_ch_txpmaresetdone,
      ch5_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txpostcursor(4 downto 0),
      ch5_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txprecursor(4 downto 0),
      ch5_txprogdivresetdone => pcie_versal_0_phy_GT_TX5_ch_txprogdivresetdone,
      ch5_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX5_ch_txrate(7 downto 0),
      ch5_txresetdone => pcie_versal_0_phy_GT_TX5_ch_txresetdone,
      ch5_txswing => pcie_versal_0_phy_GT_TX5_ch_txswing,
      ch5_txsyncdone => pcie_versal_0_phy_GT_TX5_ch_txsyncdone,
      ch5_txuserrdy => pcie_versal_0_phy_GT_TX5_ch_txuserrdy,
      ch6_cfokovwrrdy0 => '0',
      ch6_cfokovwrrdy1 => '0',
      ch6_eyescandataerror => pcie_versal_0_phy_GT_RX6_ch_eyescandataerror,
      ch6_phyready => pcie_versal_0_gt_quad_1_ch2_phyready,
      ch6_phystatus => pcie_versal_0_gt_quad_1_ch2_phystatus,
      ch6_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rx10gstat(7 downto 0),
      ch6_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxbufstatus(2 downto 0),
      ch6_rxbyteisaligned => pcie_versal_0_phy_GT_RX6_ch_rxbyteisaligned,
      ch6_rxbyterealign => pcie_versal_0_phy_GT_RX6_ch_rxbyterealign,
      ch6_rxcdrlock => pcie_versal_0_phy_GT_RX6_ch_rxcdrlock,
      ch6_rxcdrphdone => pcie_versal_0_phy_GT_RX6_ch_rxcdrphdone,
      ch6_rxchanbondseq => pcie_versal_0_phy_GT_RX6_ch_rxchanbondseq,
      ch6_rxchanisaligned => pcie_versal_0_phy_GT_RX6_ch_rxchanisaligned,
      ch6_rxchanrealign => pcie_versal_0_phy_GT_RX6_ch_rxchanrealign,
      ch6_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxchbondo(4 downto 0),
      ch6_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxclkcorcnt(1 downto 0),
      ch6_rxcominitdet => pcie_versal_0_phy_GT_RX6_ch_rxcominitdet,
      ch6_rxcommadet => pcie_versal_0_phy_GT_RX6_ch_rxcommadet,
      ch6_rxcomsasdet => pcie_versal_0_phy_GT_RX6_ch_rxcomsasdet,
      ch6_rxcomwakedet => pcie_versal_0_phy_GT_RX6_ch_rxcomwakedet,
      ch6_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxctrl0(15 downto 0),
      ch6_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxctrl1(15 downto 0),
      ch6_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxctrl2(7 downto 0),
      ch6_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxctrl3(7 downto 0),
      ch6_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxdata(127 downto 0),
      ch6_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxdataextendrsvd(7 downto 0),
      ch6_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxdatavalid(1 downto 0),
      ch6_rxdccdone => pcie_versal_0_phy_GT_RX6_ch_rxdccdone,
      ch6_rxdlyalignerr => pcie_versal_0_phy_GT_RX6_ch_rxdlyalignerr,
      ch6_rxdlyalignprog => pcie_versal_0_phy_GT_RX6_ch_rxdlyalignprog,
      ch6_rxelecidle => pcie_versal_0_phy_GT_RX6_ch_rxelecidle,
      ch6_rxfinealigndone => pcie_versal_0_phy_GT_RX6_ch_rxfinealigndone,
      ch6_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxheader(5 downto 0),
      ch6_rxmstresetdone => pcie_versal_0_phy_GT_RX6_ch_rxmstresetdone,
      ch6_rxosintdone => pcie_versal_0_phy_GT_RX6_ch_rxosintdone,
      ch6_rxosintstarted => pcie_versal_0_phy_GT_RX6_ch_rxosintstarted,
      ch6_rxosintstrobedone => pcie_versal_0_phy_GT_RX6_ch_rxosintstrobedone,
      ch6_rxosintstrobestarted => pcie_versal_0_phy_GT_RX6_ch_rxosintstrobestarted,
      ch6_rxphalignerr => pcie_versal_0_phy_GT_RX6_ch_rxphalignerr,
      ch6_rxphdlyresetdone => pcie_versal_0_phy_GT_RX6_ch_rxphdlyresetdone,
      ch6_rxphsetinitdone => pcie_versal_0_phy_GT_RX6_ch_rxphsetinitdone,
      ch6_rxphshift180done => pcie_versal_0_phy_GT_RX6_ch_rxphshift180done,
      ch6_rxpmaresetdone => pcie_versal_0_phy_GT_RX6_ch_rxpmaresetdone,
      ch6_rxpolarity => pcie_versal_0_phy_GT_RX6_ch_rxpolarity,
      ch6_rxprbserr => pcie_versal_0_phy_GT_RX6_ch_rxprbserr,
      ch6_rxprbslocked => pcie_versal_0_phy_GT_RX6_ch_rxprbslocked,
      ch6_rxprogdivresetdone => pcie_versal_0_phy_GT_RX6_ch_rxprogdivresetdone,
      ch6_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxrate(7 downto 0),
      ch6_rxrecclkout => '0',
      ch6_rxresetdone => pcie_versal_0_phy_GT_RX6_ch_rxresetdone,
      ch6_rxsliderdy => pcie_versal_0_phy_GT_RX6_ch_rxsliderdy,
      ch6_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxstartofseq(1 downto 0),
      ch6_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX6_ch_rxstatus(2 downto 0),
      ch6_rxsyncdone => pcie_versal_0_phy_GT_RX6_ch_rxsyncdone,
      ch6_rxtermination => pcie_versal_0_phy_GT_RX6_ch_rxtermination,
      ch6_rxuserrdy => pcie_versal_0_phy_GT_RX6_ch_rxuserrdy,
      ch6_rxvalid => pcie_versal_0_phy_GT_RX6_ch_rxvalid,
      ch6_tx10gstat => pcie_versal_0_phy_GT_TX6_ch_tx10gstat,
      ch6_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txbufstatus(1 downto 0),
      ch6_txcomfinish => pcie_versal_0_phy_GT_TX6_ch_txcomfinish,
      ch6_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txctrl0(15 downto 0),
      ch6_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txctrl1(15 downto 0),
      ch6_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txctrl2(7 downto 0),
      ch6_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txdata(127 downto 0),
      ch6_txdccdone => pcie_versal_0_phy_GT_TX6_ch_txdccdone,
      ch6_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txdeemph(1 downto 0),
      ch6_txdetectrx => pcie_versal_0_phy_GT_TX6_ch_txdetectrx,
      ch6_txdlyalignerr => pcie_versal_0_phy_GT_TX6_ch_txdlyalignerr,
      ch6_txdlyalignprog => pcie_versal_0_phy_GT_TX6_ch_txdlyalignprog,
      ch6_txelecidle => pcie_versal_0_phy_GT_TX6_ch_txelecidle,
      ch6_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txmaincursor(6 downto 0),
      ch6_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txmargin(2 downto 0),
      ch6_txmstresetdone => pcie_versal_0_phy_GT_TX6_ch_txmstresetdone,
      ch6_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txpd(1 downto 0),
      ch6_txphaligndone => pcie_versal_0_phy_GT_TX6_ch_txphaligndone,
      ch6_txphalignerr => pcie_versal_0_phy_GT_TX6_ch_txphalignerr,
      ch6_txphalignoutrsvd => pcie_versal_0_phy_GT_TX6_ch_txphalignoutrsvd,
      ch6_txphdlyresetdone => pcie_versal_0_phy_GT_TX6_ch_txphdlyresetdone,
      ch6_txphsetinitdone => pcie_versal_0_phy_GT_TX6_ch_txphsetinitdone,
      ch6_txphshift180done => pcie_versal_0_phy_GT_TX6_ch_txphshift180done,
      ch6_txpmaresetdone => pcie_versal_0_phy_GT_TX6_ch_txpmaresetdone,
      ch6_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txpostcursor(4 downto 0),
      ch6_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txprecursor(4 downto 0),
      ch6_txprogdivresetdone => pcie_versal_0_phy_GT_TX6_ch_txprogdivresetdone,
      ch6_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX6_ch_txrate(7 downto 0),
      ch6_txresetdone => pcie_versal_0_phy_GT_TX6_ch_txresetdone,
      ch6_txswing => pcie_versal_0_phy_GT_TX6_ch_txswing,
      ch6_txsyncdone => pcie_versal_0_phy_GT_TX6_ch_txsyncdone,
      ch6_txuserrdy => pcie_versal_0_phy_GT_TX6_ch_txuserrdy,
      ch7_cfokovwrrdy0 => '0',
      ch7_cfokovwrrdy1 => '0',
      ch7_eyescandataerror => pcie_versal_0_phy_GT_RX7_ch_eyescandataerror,
      ch7_phyready => pcie_versal_0_gt_quad_1_ch3_phyready,
      ch7_phystatus => pcie_versal_0_gt_quad_1_ch3_phystatus,
      ch7_rx10gstat(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rx10gstat(7 downto 0),
      ch7_rxbufstatus(2 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxbufstatus(2 downto 0),
      ch7_rxbyteisaligned => pcie_versal_0_phy_GT_RX7_ch_rxbyteisaligned,
      ch7_rxbyterealign => pcie_versal_0_phy_GT_RX7_ch_rxbyterealign,
      ch7_rxcdrlock => pcie_versal_0_phy_GT_RX7_ch_rxcdrlock,
      ch7_rxcdrphdone => pcie_versal_0_phy_GT_RX7_ch_rxcdrphdone,
      ch7_rxchanbondseq => pcie_versal_0_phy_GT_RX7_ch_rxchanbondseq,
      ch7_rxchanisaligned => pcie_versal_0_phy_GT_RX7_ch_rxchanisaligned,
      ch7_rxchanrealign => pcie_versal_0_phy_GT_RX7_ch_rxchanrealign,
      ch7_rxchbondo(4 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxchbondo(4 downto 0),
      ch7_rxclkcorcnt(1 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxclkcorcnt(1 downto 0),
      ch7_rxcominitdet => pcie_versal_0_phy_GT_RX7_ch_rxcominitdet,
      ch7_rxcommadet => pcie_versal_0_phy_GT_RX7_ch_rxcommadet,
      ch7_rxcomsasdet => pcie_versal_0_phy_GT_RX7_ch_rxcomsasdet,
      ch7_rxcomwakedet => pcie_versal_0_phy_GT_RX7_ch_rxcomwakedet,
      ch7_rxctrl0(15 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxctrl0(15 downto 0),
      ch7_rxctrl1(15 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxctrl1(15 downto 0),
      ch7_rxctrl2(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxctrl2(7 downto 0),
      ch7_rxctrl3(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxctrl3(7 downto 0),
      ch7_rxdata(127 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxdata(127 downto 0),
      ch7_rxdataextendrsvd(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxdataextendrsvd(7 downto 0),
      ch7_rxdatavalid(1 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxdatavalid(1 downto 0),
      ch7_rxdccdone => pcie_versal_0_phy_GT_RX7_ch_rxdccdone,
      ch7_rxdlyalignerr => pcie_versal_0_phy_GT_RX7_ch_rxdlyalignerr,
      ch7_rxdlyalignprog => pcie_versal_0_phy_GT_RX7_ch_rxdlyalignprog,
      ch7_rxelecidle => pcie_versal_0_phy_GT_RX7_ch_rxelecidle,
      ch7_rxfinealigndone => pcie_versal_0_phy_GT_RX7_ch_rxfinealigndone,
      ch7_rxheader(5 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxheader(5 downto 0),
      ch7_rxmstresetdone => pcie_versal_0_phy_GT_RX7_ch_rxmstresetdone,
      ch7_rxosintdone => pcie_versal_0_phy_GT_RX7_ch_rxosintdone,
      ch7_rxosintstarted => pcie_versal_0_phy_GT_RX7_ch_rxosintstarted,
      ch7_rxosintstrobedone => pcie_versal_0_phy_GT_RX7_ch_rxosintstrobedone,
      ch7_rxosintstrobestarted => pcie_versal_0_phy_GT_RX7_ch_rxosintstrobestarted,
      ch7_rxphalignerr => pcie_versal_0_phy_GT_RX7_ch_rxphalignerr,
      ch7_rxphdlyresetdone => pcie_versal_0_phy_GT_RX7_ch_rxphdlyresetdone,
      ch7_rxphsetinitdone => pcie_versal_0_phy_GT_RX7_ch_rxphsetinitdone,
      ch7_rxphshift180done => pcie_versal_0_phy_GT_RX7_ch_rxphshift180done,
      ch7_rxpmaresetdone => pcie_versal_0_phy_GT_RX7_ch_rxpmaresetdone,
      ch7_rxpolarity => pcie_versal_0_phy_GT_RX7_ch_rxpolarity,
      ch7_rxprbserr => pcie_versal_0_phy_GT_RX7_ch_rxprbserr,
      ch7_rxprbslocked => pcie_versal_0_phy_GT_RX7_ch_rxprbslocked,
      ch7_rxprogdivresetdone => pcie_versal_0_phy_GT_RX7_ch_rxprogdivresetdone,
      ch7_rxrate(7 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxrate(7 downto 0),
      ch7_rxrecclkout => '0',
      ch7_rxresetdone => pcie_versal_0_phy_GT_RX7_ch_rxresetdone,
      ch7_rxsliderdy => pcie_versal_0_phy_GT_RX7_ch_rxsliderdy,
      ch7_rxstartofseq(1 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxstartofseq(1 downto 0),
      ch7_rxstatus(2 downto 0) => pcie_versal_0_phy_GT_RX7_ch_rxstatus(2 downto 0),
      ch7_rxsyncdone => pcie_versal_0_phy_GT_RX7_ch_rxsyncdone,
      ch7_rxtermination => pcie_versal_0_phy_GT_RX7_ch_rxtermination,
      ch7_rxuserrdy => pcie_versal_0_phy_GT_RX7_ch_rxuserrdy,
      ch7_rxvalid => pcie_versal_0_phy_GT_RX7_ch_rxvalid,
      ch7_tx10gstat => pcie_versal_0_phy_GT_TX7_ch_tx10gstat,
      ch7_txbufstatus(1 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txbufstatus(1 downto 0),
      ch7_txcomfinish => pcie_versal_0_phy_GT_TX7_ch_txcomfinish,
      ch7_txctrl0(15 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txctrl0(15 downto 0),
      ch7_txctrl1(15 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txctrl1(15 downto 0),
      ch7_txctrl2(7 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txctrl2(7 downto 0),
      ch7_txdata(127 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txdata(127 downto 0),
      ch7_txdccdone => pcie_versal_0_phy_GT_TX7_ch_txdccdone,
      ch7_txdeemph(1 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txdeemph(1 downto 0),
      ch7_txdetectrx => pcie_versal_0_phy_GT_TX7_ch_txdetectrx,
      ch7_txdlyalignerr => pcie_versal_0_phy_GT_TX7_ch_txdlyalignerr,
      ch7_txdlyalignprog => pcie_versal_0_phy_GT_TX7_ch_txdlyalignprog,
      ch7_txelecidle => pcie_versal_0_phy_GT_TX7_ch_txelecidle,
      ch7_txmaincursor(6 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txmaincursor(6 downto 0),
      ch7_txmargin(2 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txmargin(2 downto 0),
      ch7_txmstresetdone => pcie_versal_0_phy_GT_TX7_ch_txmstresetdone,
      ch7_txpd(1 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txpd(1 downto 0),
      ch7_txphaligndone => pcie_versal_0_phy_GT_TX7_ch_txphaligndone,
      ch7_txphalignerr => pcie_versal_0_phy_GT_TX7_ch_txphalignerr,
      ch7_txphalignoutrsvd => pcie_versal_0_phy_GT_TX7_ch_txphalignoutrsvd,
      ch7_txphdlyresetdone => pcie_versal_0_phy_GT_TX7_ch_txphdlyresetdone,
      ch7_txphsetinitdone => pcie_versal_0_phy_GT_TX7_ch_txphsetinitdone,
      ch7_txphshift180done => pcie_versal_0_phy_GT_TX7_ch_txphshift180done,
      ch7_txpmaresetdone => pcie_versal_0_phy_GT_TX7_ch_txpmaresetdone,
      ch7_txpostcursor(4 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txpostcursor(4 downto 0),
      ch7_txprecursor(4 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txprecursor(4 downto 0),
      ch7_txprogdivresetdone => pcie_versal_0_phy_GT_TX7_ch_txprogdivresetdone,
      ch7_txrate(7 downto 0) => pcie_versal_0_phy_GT_TX7_ch_txrate(7 downto 0),
      ch7_txresetdone => pcie_versal_0_phy_GT_TX7_ch_txresetdone,
      ch7_txswing => pcie_versal_0_phy_GT_TX7_ch_txswing,
      ch7_txsyncdone => pcie_versal_0_phy_GT_TX7_ch_txsyncdone,
      ch7_txuserrdy => pcie_versal_0_phy_GT_TX7_ch_txuserrdy,
      dbg_phy_rxeq_fsm(23 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_dbg_phy_rxeq_fsm(23 downto 0),
      dbg_phy_txeq_fsm(23 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_dbg_phy_txeq_fsm(23 downto 0),
      gt_bufgtce => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtce,
      gt_bufgtcemask(3 downto 0) => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtcemask(3 downto 0),
      gt_bufgtdiv(11 downto 0) => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtdiv(11 downto 0),
      gt_bufgtrst => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtrst,
      gt_bufgtrstmask(3 downto 0) => pcie_versal_0_gt_quad_0_GT0_BUFGT_ch_bufgtrstmask(3 downto 0),
      gt_pcieltssm(5 downto 0) => pcie_versal_0_phy_gt_pcieltssm(5 downto 0),
      gt_rxoutclk => pcie_versal_0_gt_quad_0_ch0_rxoutclk,
      gt_txoutclk => pcie_versal_0_gt_quad_0_ch0_txoutclk,
      gtpowergood => '0',
      gtq0_rxmarginclk => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginclk,
      gtq0_rxmarginreqack => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqack,
      gtq0_rxmarginreqcmd(3 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqcmd(3 downto 0),
      gtq0_rxmarginreqlanenum(1 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqlanenum(1 downto 0),
      gtq0_rxmarginreqpayld(7 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqpayld(7 downto 0),
      gtq0_rxmarginreqreq => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreqreq,
      gtq0_rxmarginresack => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginresack,
      gtq0_rxmarginrescmd(3 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginrescmd(3 downto 0),
      gtq0_rxmarginreslanenum(1 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginreslanenum(1 downto 0),
      gtq0_rxmarginrespayld(7 downto 0) => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginrespayld(7 downto 0),
      gtq0_rxmarginresreq => pcie_versal_0_phy_gt_rxmargin_q0_rxmarginresreq,
      gtq1_rxmarginclk => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginclk,
      gtq1_rxmarginreqack => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqack,
      gtq1_rxmarginreqcmd(3 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqcmd(3 downto 0),
      gtq1_rxmarginreqlanenum(1 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqlanenum(1 downto 0),
      gtq1_rxmarginreqpayld(7 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqpayld(7 downto 0),
      gtq1_rxmarginreqreq => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreqreq,
      gtq1_rxmarginresack => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginresack,
      gtq1_rxmarginrescmd(3 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginrescmd(3 downto 0),
      gtq1_rxmarginreslanenum(1 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginreslanenum(1 downto 0),
      gtq1_rxmarginrespayld(7 downto 0) => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginrespayld(7 downto 0),
      gtq1_rxmarginresreq => pcie_versal_0_phy_gt_rxmargin_q1_rxmarginresreq,
      gtrefclk => pcie_versal_0_phy_gtrefclk,
      pcie2gt0_rxn(3 downto 0) => pcie_versal_0_gt_quad_0_GT_Serial_RX_RXN(3 downto 0),
      pcie2gt0_rxp(3 downto 0) => pcie_versal_0_gt_quad_0_GT_Serial_RX_RXP(3 downto 0),
      pcie2gt0_txn(3 downto 0) => pcie_versal_0_gt_quad_0_GT_Serial_TX_TXN(3 downto 0),
      pcie2gt0_txp(3 downto 0) => pcie_versal_0_gt_quad_0_GT_Serial_TX_TXP(3 downto 0),
      pcie2gt1_rxn(3 downto 0) => pcie_versal_0_gt_quad_1_GT_Serial_RX_RXN(3 downto 0),
      pcie2gt1_rxp(3 downto 0) => pcie_versal_0_gt_quad_1_GT_Serial_RX_RXP(3 downto 0),
      pcie2gt1_txn(3 downto 0) => pcie_versal_0_gt_quad_1_GT_Serial_TX_TXN(3 downto 0),
      pcie2gt1_txp(3 downto 0) => pcie_versal_0_gt_quad_1_GT_Serial_TX_TXP(3 downto 0),
      pcie_link_reach_target => '0',
      pcie_ltssm_state(5 downto 0) => pcie_versal_0_pcie_ltssm_state(5 downto 0),
      pcierstb => pcie_versal_0_phy_pcierstb,
      phy_coreclk => pcie_versal_0_phy_phy_coreclk,
      phy_gt_pclk => NLW_pcie_versal_0_phy_phy_gt_pclk_UNCONNECTED,
      phy_gtrefclk => sys_clk_gt_1,
      phy_mcapclk => pcie_versal_0_phy_phy_mcapclk,
      phy_pclk => pcie_versal_0_phy_phy_pclk,
      phy_phystatus(7 downto 0) => pcie_versal_0_phy_phy_mac_status_phy_phystatus(7 downto 0),
      phy_phystatus_rst => pcie_versal_0_phy_phy_mac_status_phy_phystatus_rst,
      phy_powerdown(15 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_powerdown(15 downto 0),
      phy_rate(2 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_rate(2 downto 0),
      phy_ready => pcie_versal_0_phy_phy_mac_status_phy_ready,
      phy_refclk => sys_clk_1,
      phy_rst_n => sys_reset_1,
      phy_rx_margin_req_ack(1 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_ack(1 downto 0),
      phy_rx_margin_req_cmd(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_cmd(7 downto 0),
      phy_rx_margin_req_lane_num(3 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_lane_num(3 downto 0),
      phy_rx_margin_req_payload(15 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_payload(15 downto 0),
      phy_rx_margin_req_req(1 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_req_req(1 downto 0),
      phy_rx_margin_res_ack(1 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_ack(1 downto 0),
      phy_rx_margin_res_cmd(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_cmd(7 downto 0),
      phy_rx_margin_res_lane_num(3 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_lane_num(3 downto 0),
      phy_rx_margin_res_payload(15 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_payload(15 downto 0),
      phy_rx_margin_res_req(1 downto 0) => pcie_versal_0_phy_phy_mac_rx_margining_phy_rx_margin_res_req(1 downto 0),
      phy_rx_termination(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_rx_termination(7 downto 0),
      phy_rxdata(511 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxdata(511 downto 0),
      phy_rxdata_valid(7 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxdata_valid(7 downto 0),
      phy_rxdatak(15 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxdatak(15 downto 0),
      phy_rxelecidle(7 downto 0) => pcie_versal_0_phy_phy_mac_status_phy_rxelecidle(7 downto 0),
      phy_rxeq_adapt_done(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_adapt_done(7 downto 0),
      phy_rxeq_ctrl(15 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_ctrl(15 downto 0),
      phy_rxeq_done(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_done(7 downto 0),
      phy_rxeq_lffs(47 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_lffs(47 downto 0),
      phy_rxeq_lffs_sel(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_lffs_sel(7 downto 0),
      phy_rxeq_new_txcoeff(143 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_new_txcoeff(143 downto 0),
      phy_rxeq_preset(23 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_preset(23 downto 0),
      phy_rxeq_preset_sel(7 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_preset_sel(7 downto 0),
      phy_rxeq_txpreset(31 downto 0) => pcie_versal_0_phy_phy_mac_rx_eq_3rd_phy_rxeq_txpreset(31 downto 0),
      phy_rxn(7 downto 0) => pcie_versal_0_phy_pcie_mgt_rxp(7 downto 0),
      phy_rxp(7 downto 0) => pcie_versal_0_phy_pcie_mgt_rxn(7 downto 0),
      phy_rxpolarity(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_rxpolarity(7 downto 0),
      phy_rxstart_block(15 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxstart_block(15 downto 0),
      phy_rxstatus(23 downto 0) => pcie_versal_0_phy_phy_mac_status_phy_rxstatus(23 downto 0),
      phy_rxsync_header(15 downto 0) => pcie_versal_0_phy_mac_rx_phy_rxsync_header(15 downto 0),
      phy_rxvalid(7 downto 0) => pcie_versal_0_phy_phy_mac_status_phy_rxvalid(7 downto 0),
      phy_txcompliance(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_txcompliance(7 downto 0),
      phy_txdata(511 downto 0) => pcie_versal_0_phy_mac_tx_phy_txdata(511 downto 0),
      phy_txdata_valid(7 downto 0) => pcie_versal_0_phy_mac_tx_phy_txdata_valid(7 downto 0),
      phy_txdatak(15 downto 0) => pcie_versal_0_phy_mac_tx_phy_txdatak(15 downto 0),
      phy_txdeemph(7 downto 0) => pcie_versal_0_phy_phy_mac_tx_drive_phy_txdeemph(7 downto 0),
      phy_txdetectrx_loopback(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_txdetectrx_loopback(7 downto 0),
      phy_txelecidle(7 downto 0) => pcie_versal_0_phy_phy_mac_command_phy_txelecidle(7 downto 0),
      phy_txeq_coeff(47 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_coeff(47 downto 0),
      phy_txeq_ctrl(15 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_ctrl(15 downto 0),
      phy_txeq_done(7 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_done(7 downto 0),
      phy_txeq_fs(5 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_fs(5 downto 0),
      phy_txeq_lf(5 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_lf(5 downto 0),
      phy_txeq_new_coeff(143 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_new_coeff(143 downto 0),
      phy_txeq_preset(31 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_3rd_phy_txeq_preset(31 downto 0),
      phy_txmaincursor(55 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_phy_txmaincursor(55 downto 0),
      phy_txmargin(23 downto 0) => pcie_versal_0_phy_phy_mac_tx_drive_phy_txmargin(23 downto 0),
      phy_txn(7 downto 0) => pcie_versal_0_phy_pcie_mgt_txp(7 downto 0),
      phy_txp(7 downto 0) => pcie_versal_0_phy_pcie_mgt_txn(7 downto 0),
      phy_txpostcursor(39 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_phy_txpostcursor(39 downto 0),
      phy_txprecursor(39 downto 0) => pcie_versal_0_phy_phy_mac_tx_eq_phy_txprecursor(39 downto 0),
      phy_txstart_block(7 downto 0) => pcie_versal_0_phy_mac_tx_phy_txstart_block(7 downto 0),
      phy_txswing(7 downto 0) => pcie_versal_0_phy_phy_mac_tx_drive_phy_txswing(7 downto 0),
      phy_txsync_header(15 downto 0) => pcie_versal_0_phy_mac_tx_phy_txsync_header(15 downto 0),
      phy_userclk => pcie_versal_0_phy_phy_userclk,
      phy_userclk2 => pcie_versal_0_phy_phy_userclk2
    );
end STRUCTURE;

