--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    --use IEEE.NUMERIC_STD.ALL;
    use work.pcie_package.all;

entity versal_cpm_pcie_endpoints is
    Generic (
        CARD_TYPE: integer
    );
    Port (
        CH0_DDR4_0_0_act_n : out STD_LOGIC_VECTOR ( 0 to 0 );
        CH0_DDR4_0_0_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
        CH0_DDR4_0_0_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
        CH0_DDR4_0_0_bg : out STD_LOGIC_VECTOR ( 1 downto 0 );
        CH0_DDR4_0_0_ck_c : out STD_LOGIC_VECTOR ( 1 downto 0 );
        CH0_DDR4_0_0_ck_t : out STD_LOGIC_VECTOR ( 1 downto 0 );
        CH0_DDR4_0_0_cke : out STD_LOGIC_VECTOR ( 1 downto 0 );
        CH0_DDR4_0_0_cs_n : out STD_LOGIC_VECTOR ( 1 downto 0 );
        CH0_DDR4_0_0_dm_n : inout STD_LOGIC_VECTOR ( 8 downto 0 );
        CH0_DDR4_0_0_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
        CH0_DDR4_0_0_dqs_c : inout STD_LOGIC_VECTOR ( 8 downto 0 );
        CH0_DDR4_0_0_dqs_t : inout STD_LOGIC_VECTOR ( 8 downto 0 );
        CH0_DDR4_0_0_odt : out STD_LOGIC_VECTOR ( 1 downto 0 );
        CH0_DDR4_0_0_reset_n : out STD_LOGIC_VECTOR ( 0 to 0 );
        scl_i : in STD_LOGIC;
        scl_o : out STD_LOGIC;
        sda_i : in STD_LOGIC;
        sda_o : out STD_LOGIC;
        gpio2_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
        gpio2_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
        gpio_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
        gpio_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
        pl0_resetn : out STD_LOGIC_VECTOR ( 0 to 0 );
        sys_clk0_0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
        sys_clk0_0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
        pl0_ref_clk : out STD_LOGIC;
        WupperToCPM: in WupperToCPM_array_type(0 to 1);
        CPMToWupper: out CPMToWupper_array_type(0 to 1);
        clk100_out : out std_logic
    );
end versal_cpm_pcie_endpoints;

architecture Behavioral of versal_cpm_pcie_endpoints is
    --COMPONENT axis_cc_cq_ila
    --    PORT (
    --        clk : IN STD_LOGIC;
    --        probe0 : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    --        probe1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    --        probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe3 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe4 : IN STD_LOGIC_VECTOR(182 DOWNTO 0);
    --        probe5 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe6 : IN STD_LOGIC_VECTOR(511 DOWNTO 0);
    --        probe7 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    --        probe8 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe9 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe10 : IN STD_LOGIC_VECTOR(80 DOWNTO 0);
    --        probe11 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe12 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --        probe13 : IN STD_LOGIC_VECTOR(0 DOWNTO 0)
    --    );
    --END COMPONENT;

    component FLX155_cips_wrapper
        port (
            CH0_DDR4_0_0_act_n : out STD_LOGIC_VECTOR ( 0 to 0 );
            CH0_DDR4_0_0_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
            CH0_DDR4_0_0_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_bg : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_ck_c : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_ck_t : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_cke : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_cs_n : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_dm_n : inout STD_LOGIC_VECTOR ( 8 downto 0 );
            CH0_DDR4_0_0_dq : inout STD_LOGIC_VECTOR ( 71 downto 0 );
            CH0_DDR4_0_0_dqs_c : inout STD_LOGIC_VECTOR ( 8 downto 0 );
            CH0_DDR4_0_0_dqs_t : inout STD_LOGIC_VECTOR ( 8 downto 0 );
            CH0_DDR4_0_0_odt : out STD_LOGIC_VECTOR ( 1 downto 0 );
            CH0_DDR4_0_0_reset_n : out STD_LOGIC_VECTOR ( 0 to 0 );
            PCIE0_GT_0_grx_n : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_grx_p : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_gtx_n : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE0_GT_0_gtx_p : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_grx_n : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_grx_p : in STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_gtx_n : out STD_LOGIC_VECTOR ( 7 downto 0 );
            PCIE1_GT_0_gtx_p : out STD_LOGIC_VECTOR ( 7 downto 0 );
            clk100_out : out STD_LOGIC;
            gpio2_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio2_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio_io_i_0 : in STD_LOGIC_VECTOR ( 20 downto 0 );
            gpio_io_o_0 : out STD_LOGIC_VECTOR ( 20 downto 0 );
            gt_refclk0_0_clk_n : in STD_LOGIC;
            gt_refclk0_0_clk_p : in STD_LOGIC;
            gt_refclk1_0_clk_n : in STD_LOGIC;
            gt_refclk1_0_clk_p : in STD_LOGIC;
            pcie0_cfg_interrupt_0_intx_vector : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_interrupt_0_pending : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_cfg_interrupt_0_sent : out STD_LOGIC;
            pcie0_cfg_msi_0_fail : out STD_LOGIC;
            pcie0_cfg_msi_0_function_number : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie0_cfg_msi_0_int_vector : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_cfg_msi_0_select : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie0_cfg_msi_0_sent : out STD_LOGIC;
            pcie0_cfg_msix_0_address : in STD_LOGIC_VECTOR ( 63 downto 0 );
            pcie0_cfg_msix_0_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_cfg_msix_0_enable : out STD_LOGIC;
            pcie0_cfg_msix_0_int_vector : in STD_LOGIC;
            pcie0_cfg_msix_0_mask : out STD_LOGIC;
            pcie0_cfg_msix_0_vec_pending : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie0_cfg_msix_0_vec_pending_status : out STD_LOGIC;
            pcie0_m_axis_cq_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_m_axis_cq_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_m_axis_cq_0_tlast : out STD_LOGIC;
            pcie0_m_axis_cq_0_tready : in STD_LOGIC;
            pcie0_m_axis_cq_0_tuser : out STD_LOGIC_VECTOR ( 464 downto 0 );
            pcie0_m_axis_cq_0_tvalid : out STD_LOGIC;
            pcie0_m_axis_rc_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_m_axis_rc_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_m_axis_rc_0_tlast : out STD_LOGIC;
            pcie0_m_axis_rc_0_tready : in STD_LOGIC;
            pcie0_m_axis_rc_0_tuser : out STD_LOGIC_VECTOR ( 336 downto 0 );
            pcie0_m_axis_rc_0_tvalid : out STD_LOGIC;
            pcie0_s_axis_cc_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_s_axis_cc_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_s_axis_cc_0_tlast : in STD_LOGIC;
            pcie0_s_axis_cc_0_tready : out STD_LOGIC;
            pcie0_s_axis_cc_0_tuser : in STD_LOGIC_VECTOR ( 164 downto 0 );
            pcie0_s_axis_cc_0_tvalid : in STD_LOGIC;
            pcie0_s_axis_rq_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie0_s_axis_rq_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie0_s_axis_rq_0_tlast : in STD_LOGIC;
            pcie0_s_axis_rq_0_tready : out STD_LOGIC;
            pcie0_s_axis_rq_0_tuser : in STD_LOGIC_VECTOR ( 372 downto 0 );
            pcie0_s_axis_rq_0_tvalid : in STD_LOGIC;
            pcie0_user_clk_0 : out STD_LOGIC;
            pcie0_user_lnk_up_0 : out STD_LOGIC;
            pcie0_user_reset_0 : out STD_LOGIC;
            pcie1_cfg_interrupt_0_intx_vector : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_interrupt_0_pending : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_cfg_interrupt_0_sent : out STD_LOGIC;
            pcie1_cfg_msi_0_fail : out STD_LOGIC;
            pcie1_cfg_msi_0_function_number : in STD_LOGIC_VECTOR ( 15 downto 0 );
            pcie1_cfg_msi_0_int_vector : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_cfg_msi_0_select : in STD_LOGIC_VECTOR ( 3 downto 0 );
            pcie1_cfg_msi_0_sent : out STD_LOGIC;
            pcie1_cfg_msix_0_address : in STD_LOGIC_VECTOR ( 63 downto 0 );
            pcie1_cfg_msix_0_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_cfg_msix_0_enable : out STD_LOGIC;
            pcie1_cfg_msix_0_int_vector : in STD_LOGIC;
            pcie1_cfg_msix_0_mask : out STD_LOGIC;
            pcie1_cfg_msix_0_vec_pending : in STD_LOGIC_VECTOR ( 1 downto 0 );
            pcie1_cfg_msix_0_vec_pending_status : out STD_LOGIC;
            pcie1_m_axis_cq_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_m_axis_cq_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_m_axis_cq_0_tlast : out STD_LOGIC;
            pcie1_m_axis_cq_0_tready : in STD_LOGIC;
            pcie1_m_axis_cq_0_tuser : out STD_LOGIC_VECTOR ( 464 downto 0 );
            pcie1_m_axis_cq_0_tvalid : out STD_LOGIC;
            pcie1_m_axis_rc_0_tdata : out STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_m_axis_rc_0_tkeep : out STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_m_axis_rc_0_tlast : out STD_LOGIC;
            pcie1_m_axis_rc_0_tready : in STD_LOGIC;
            pcie1_m_axis_rc_0_tuser : out STD_LOGIC_VECTOR ( 336 downto 0 );
            pcie1_m_axis_rc_0_tvalid : out STD_LOGIC;
            pcie1_s_axis_cc_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_s_axis_cc_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_s_axis_cc_0_tlast : in STD_LOGIC;
            pcie1_s_axis_cc_0_tready : out STD_LOGIC;
            pcie1_s_axis_cc_0_tuser : in STD_LOGIC_VECTOR ( 164 downto 0 );
            pcie1_s_axis_cc_0_tvalid : in STD_LOGIC;
            pcie1_s_axis_rq_0_tdata : in STD_LOGIC_VECTOR ( 1023 downto 0 );
            pcie1_s_axis_rq_0_tkeep : in STD_LOGIC_VECTOR ( 31 downto 0 );
            pcie1_s_axis_rq_0_tlast : in STD_LOGIC;
            pcie1_s_axis_rq_0_tready : out STD_LOGIC;
            pcie1_s_axis_rq_0_tuser : in STD_LOGIC_VECTOR ( 372 downto 0 );
            pcie1_s_axis_rq_0_tvalid : in STD_LOGIC;
            pcie1_user_clk_0 : out STD_LOGIC;
            pcie1_user_lnk_up_0 : out STD_LOGIC;
            pcie1_user_reset_0 : out STD_LOGIC;
            pl0_ref_clk_0 : out STD_LOGIC;
            pl0_resetn_0 : out STD_LOGIC;
            scl_i : in STD_LOGIC;
            scl_o : out STD_LOGIC;
            sda_i : in STD_LOGIC;
            sda_o : out STD_LOGIC;
            sys_clk0_0_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
            sys_clk0_0_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 )
        );
    end component FLX155_cips_wrapper;

    signal CPMToWupper_s:  CPMToWupper_array_type(0 to 1);
    signal user_reset_0, user_reset_1: std_logic;
begin

    CPMToWupper <= CPMToWupper_s;
    --g_ilas: for i in 0 to 1 generate
    --    ila0 : axis_cc_cq_ila
    --        PORT MAP (
    --            clk => CPMToWupper_s(i).user_clk_0,
    --            probe0 => CPMToWupper_s(i).m_axis_cq_0_tdata ,
    --            probe1 => CPMToWupper_s(i).m_axis_cq_0_tkeep ,
    --            probe2(0) => CPMToWupper_s(i).m_axis_cq_0_tlast ,
    --            probe3(0) => WupperToCPM(i).m_axis_cq_0_tready  ,
    --            probe4 => CPMToWupper_s(i).m_axis_cq_0_tuser ,
    --            probe5(0) => CPMToWupper_s(i).m_axis_cq_0_tvalid,
    --            probe6 => WupperToCPM(i).s_axis_cc_0_tdata   ,
    --            probe7 => WupperToCPM(i).s_axis_cc_0_tkeep   ,
    --            probe8(0) => WupperToCPM(i).s_axis_cc_0_tlast   ,
    --            probe9(0) => CPMToWupper_s(i).s_axis_cc_0_tready,
    --            probe10 =>WupperToCPM(i).s_axis_cc_0_tuser   ,
    --            probe11(0) =>WupperToCPM(i).s_axis_cc_0_tvalid,
    --            probe12(0) => CPMToWupper_s(i).user_reset_0,
    --            probe13(0) => CPMToWupper_s(i).user_lnk_up_0
    --        );
    --end generate;
    g_flx182: if CARD_TYPE = 182 generate
    --! TODO: Re-enable CPM in case of Versal Prime if we need it, but with DDR, I2C etc.
    --cpm0: entity work.versal_cpm_pcie_wrapper --@suppress
    --    port map(
    --        PCIE0_GT_0_grx_n                    => WupperToCPM(0).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
    --        PCIE0_GT_0_grx_p                    => WupperToCPM(0).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
    --        PCIE0_GT_0_gtx_n                    => CPMToWupper_s(0).PCIE_tx_n,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
    --        PCIE0_GT_0_gtx_p                    => CPMToWupper_s(0).PCIE_tx_p,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
    --        PCIE1_GT_0_grx_n                    => WupperToCPM(1).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
    --        PCIE1_GT_0_grx_p                    => WupperToCPM(1).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
    --        PCIE1_GT_0_gtx_n                    => CPMToWupper_s(1).PCIE_tx_n, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
    --        PCIE1_GT_0_gtx_p                    => CPMToWupper_s(1).PCIE_tx_p, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
    --        gt_refclk0_0_clk_n                  => WupperToCPM(0).gtrefclk_n,--: in STD_LOGIC;
    --        gt_refclk0_0_clk_p                  => WupperToCPM(0).gtrefclk_p,--: in STD_LOGIC;
    --        gt_refclk1_0_clk_n                  => WupperToCPM(1).gtrefclk_n,--: in STD_LOGIC;
    --        gt_refclk1_0_clk_p                  => WupperToCPM(1).gtrefclk_p,--: in STD_LOGIC;
    --        pcie0_cfg_interrupt_0_intx_vector   => WupperToCPM(0).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
    --        pcie0_cfg_interrupt_0_pending       => WupperToCPM(0).cfg_interrupt_0_pending      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
    --        pcie0_cfg_interrupt_0_sent          => CPMToWupper_s(0).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
    --        pcie0_cfg_msix_0_address            => WupperToCPM(0).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
    --        pcie0_cfg_msix_0_data               => WupperToCPM(0).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
    --        pcie0_cfg_msix_0_enable             => CPMToWupper_s(0).cfg_msix_0_enable            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
    --        pcie0_cfg_msix_0_fail               => open, --CPMToWupper_s(0).cfg_msix_0_fail              ,--: out STD_LOGIC;
    --        pcie0_cfg_msix_0_function_number    => WupperToCPM(0).cfg_msix_0_function_number   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
    --        pcie0_cfg_msix_0_int_vector         => WupperToCPM(0).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
    --        pcie0_cfg_msix_0_mask               => CPMToWupper_s(0).cfg_msix_0_mask              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
    --        pcie0_cfg_msix_0_mint_vector        => WupperToCPM(0).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
    --        pcie0_cfg_msix_0_sent               => open, --CPMToWupper_s(0).cfg_msix_0_sent              ,--: out STD_LOGIC;
    --        pcie0_cfg_msix_0_vec_pending        => WupperToCPM(0).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
    --        pcie0_cfg_msix_0_vec_pending_status => open, --CPMToWupper_s(0).cfg_msix_0_vec_pending_status,--: out STD_LOGIC;
    --        pcie0_m_axis_cq_0_tdata             => CPMToWupper_s(0).m_axis_cq_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
    --        pcie0_m_axis_cq_0_tkeep             => CPMToWupper_s(0).m_axis_cq_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
    --        pcie0_m_axis_cq_0_tlast             => CPMToWupper_s(0).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
    --        pcie0_m_axis_cq_0_tready            => WupperToCPM(0).m_axis_cq_0_tready           ,--: in STD_LOGIC;
    --        pcie0_m_axis_cq_0_tuser             => CPMToWupper_s(0).m_axis_cq_0_tuser            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
    --        pcie0_m_axis_cq_0_tvalid            => CPMToWupper_s(0).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
    --        pcie0_m_axis_rc_0_tdata             => CPMToWupper_s(0).m_axis_rc_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
    --        pcie0_m_axis_rc_0_tkeep             => CPMToWupper_s(0).m_axis_rc_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
    --        pcie0_m_axis_rc_0_tlast             => CPMToWupper_s(0).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
    --        pcie0_m_axis_rc_0_tready            => WupperToCPM(0).m_axis_rc_0_tready           ,--: in STD_LOGIC;
    --        pcie0_m_axis_rc_0_tuser             => CPMToWupper_s(0).m_axis_rc_0_tuser            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
    --        pcie0_m_axis_rc_0_tvalid            => CPMToWupper_s(0).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
    --        pcie0_s_axis_cc_0_tdata             => WupperToCPM(0).s_axis_cc_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
    --        pcie0_s_axis_cc_0_tkeep             => WupperToCPM(0).s_axis_cc_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
    --        pcie0_s_axis_cc_0_tlast             => WupperToCPM(0).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
    --        pcie0_s_axis_cc_0_tready            => CPMToWupper_s(0).s_axis_cc_0_tready           ,--: out STD_LOGIC;
    --        pcie0_s_axis_cc_0_tuser             => WupperToCPM(0).s_axis_cc_0_tuser            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
    --        pcie0_s_axis_cc_0_tvalid            => WupperToCPM(0).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
    --        pcie0_s_axis_rq_0_tdata             => WupperToCPM(0).s_axis_rq_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
    --        pcie0_s_axis_rq_0_tkeep             => WupperToCPM(0).s_axis_rq_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
    --        pcie0_s_axis_rq_0_tlast             => WupperToCPM(0).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
    --        pcie0_s_axis_rq_0_tready            => CPMToWupper_s(0).s_axis_rq_0_tready           ,--: out STD_LOGIC;
    --        pcie0_s_axis_rq_0_tuser             => WupperToCPM(0).s_axis_rq_0_tuser            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
    --        pcie0_s_axis_rq_0_tvalid            => WupperToCPM(0).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
    --        pcie0_user_clk_0                    => CPMToWupper_s(0).user_clk_0                   ,--: out STD_LOGIC;
    --        pcie0_user_lnk_up_0                 => CPMToWupper_s(0).user_lnk_up_0                ,--: out STD_LOGIC;
    --        pcie0_user_reset_0                  => user_reset_0                 ,--: out STD_LOGIC;
    --        pcie1_cfg_interrupt_0_intx_vector   => WupperToCPM(1).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
    --        pcie1_cfg_interrupt_0_pending       => WupperToCPM(1).cfg_interrupt_0_pending      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
    --        pcie1_cfg_interrupt_0_sent          => CPMToWupper_s(1).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
    --        pcie1_cfg_msix_0_address            => WupperToCPM(1).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
    --        pcie1_cfg_msix_0_data               => WupperToCPM(1).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
    --        pcie1_cfg_msix_0_enable             => CPMToWupper_s(1).cfg_msix_0_enable            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
    --        --pcie1_cfg_msix_0_fail               => CPMToWupper_s(1).cfg_msix_0_fail              ,
    --        pcie1_cfg_msix_0_function_number    => WupperToCPM(1).cfg_msix_0_function_number   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
    --        pcie1_cfg_msix_0_int_vector         => WupperToCPM(1).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
    --        pcie1_cfg_msix_0_mask               => CPMToWupper_s(1).cfg_msix_0_mask              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
    --        pcie1_cfg_msix_0_mint_vector        => WupperToCPM(1).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
    --        --pcie1_cfg_msix_0_sent               => CPMToWupper_s(1).cfg_msix_0_sent             ,
    --        pcie1_cfg_msix_0_vec_pending        => WupperToCPM(1).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
    --        --pcie1_cfg_msix_0_vec_pending_status => CPMToWupper_s(1).cfg_msix_0_vec_pending_status,
    --        pcie1_m_axis_cq_0_tdata             => CPMToWupper_s(1).m_axis_cq_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
    --        pcie1_m_axis_cq_0_tkeep             => CPMToWupper_s(1).m_axis_cq_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
    --        pcie1_m_axis_cq_0_tlast             => CPMToWupper_s(1).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
    --        pcie1_m_axis_cq_0_tready            => WupperToCPM(1).m_axis_cq_0_tready           ,--: in STD_LOGIC;
    --        pcie1_m_axis_cq_0_tuser             => CPMToWupper_s(1).m_axis_cq_0_tuser            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
    --        pcie1_m_axis_cq_0_tvalid            => CPMToWupper_s(1).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
    --        pcie1_m_axis_rc_0_tdata             => CPMToWupper_s(1).m_axis_rc_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
    --        pcie1_m_axis_rc_0_tkeep             => CPMToWupper_s(1).m_axis_rc_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
    --        pcie1_m_axis_rc_0_tlast             => CPMToWupper_s(1).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
    --        pcie1_m_axis_rc_0_tready            => WupperToCPM(1).m_axis_rc_0_tready           ,--: in STD_LOGIC;
    --        pcie1_m_axis_rc_0_tuser             => CPMToWupper_s(1).m_axis_rc_0_tuser            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
    --        pcie1_m_axis_rc_0_tvalid            => CPMToWupper_s(1).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
    --        pcie1_s_axis_cc_0_tdata             => WupperToCPM(1).s_axis_cc_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
    --        pcie1_s_axis_cc_0_tkeep             => WupperToCPM(1).s_axis_cc_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
    --        pcie1_s_axis_cc_0_tlast             => WupperToCPM(1).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
    --        pcie1_s_axis_cc_0_tready            => CPMToWupper_s(1).s_axis_cc_0_tready           ,--: out STD_LOGIC;
    --        pcie1_s_axis_cc_0_tuser             => WupperToCPM(1).s_axis_cc_0_tuser            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
    --        pcie1_s_axis_cc_0_tvalid            => WupperToCPM(1).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
    --        pcie1_s_axis_rq_0_tdata             => WupperToCPM(1).s_axis_rq_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
    --        pcie1_s_axis_rq_0_tkeep             => WupperToCPM(1).s_axis_rq_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
    --        pcie1_s_axis_rq_0_tlast             => WupperToCPM(1).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
    --        pcie1_s_axis_rq_0_tready            => CPMToWupper_s(1).s_axis_rq_0_tready           ,--: out STD_LOGIC;
    --        pcie1_s_axis_rq_0_tuser             => WupperToCPM(1).s_axis_rq_0_tuser            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
    --        pcie1_s_axis_rq_0_tvalid            => WupperToCPM(1).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
    --        pcie1_user_clk_0                    => CPMToWupper_s(1).user_clk_0                   ,--: out STD_LOGIC;
    --        pcie1_user_lnk_up_0                 => CPMToWupper_s(1).user_lnk_up_0                ,--: out STD_LOGIC;
    --        pcie1_user_reset_0                  => user_reset_1                 , --: out STD_LOGIC
    --        pl0_ref_clk_0                       => clk100_out
    --    );
    end generate g_flx182;

    g_flx155: if CARD_TYPE = 155 generate
        cpm0: FLX155_cips_wrapper
            port map(
                CH0_DDR4_0_0_act_n => CH0_DDR4_0_0_act_n,
                CH0_DDR4_0_0_adr => CH0_DDR4_0_0_adr,
                CH0_DDR4_0_0_ba => CH0_DDR4_0_0_ba,
                CH0_DDR4_0_0_bg => CH0_DDR4_0_0_bg,
                CH0_DDR4_0_0_ck_c => CH0_DDR4_0_0_ck_c(1 downto 0),
                CH0_DDR4_0_0_ck_t => CH0_DDR4_0_0_ck_t(1 downto 0),
                CH0_DDR4_0_0_cke => CH0_DDR4_0_0_cke(1 downto 0),
                CH0_DDR4_0_0_cs_n => CH0_DDR4_0_0_cs_n(1 downto 0),
                CH0_DDR4_0_0_dm_n => CH0_DDR4_0_0_dm_n,
                CH0_DDR4_0_0_dq => CH0_DDR4_0_0_dq,
                CH0_DDR4_0_0_dqs_c => CH0_DDR4_0_0_dqs_c,
                CH0_DDR4_0_0_dqs_t => CH0_DDR4_0_0_dqs_t,
                CH0_DDR4_0_0_odt => CH0_DDR4_0_0_odt(1 downto 0),
                CH0_DDR4_0_0_reset_n => CH0_DDR4_0_0_reset_n,
                PCIE0_GT_0_grx_n                    => WupperToCPM(0).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_grx_p                    => WupperToCPM(0).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_gtx_n                    => CPMToWupper_s(0).PCIE_tx_n,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE0_GT_0_gtx_p                    => CPMToWupper_s(0).PCIE_tx_p,--: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_grx_n                    => WupperToCPM(1).PCIE_rx_n,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_grx_p                    => WupperToCPM(1).PCIE_rx_p,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_gtx_n                    => CPMToWupper_s(1).PCIE_tx_n, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
                PCIE1_GT_0_gtx_p                    => CPMToWupper_s(1).PCIE_tx_p, --: out STD_LOGIC_VECTOR ( 7 downto 0 );
                clk100_out => clk100_out,
                gpio2_io_i_0 => gpio2_io_i_0,
                gpio2_io_o_0 => gpio2_io_o_0,
                gpio_io_i_0 => gpio_io_i_0,
                gpio_io_o_0 => gpio_io_o_0,
                gt_refclk0_0_clk_n                  => WupperToCPM(0).gtrefclk_n,--: in STD_LOGIC;
                gt_refclk0_0_clk_p                  => WupperToCPM(0).gtrefclk_p,--: in STD_LOGIC;
                gt_refclk1_0_clk_n                  => WupperToCPM(1).gtrefclk_n,--: in STD_LOGIC;
                gt_refclk1_0_clk_p                  => WupperToCPM(1).gtrefclk_p,--: in STD_LOGIC;
                pcie0_cfg_interrupt_0_intx_vector   => WupperToCPM(0).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_interrupt_0_pending       => WupperToCPM(0).cfg_interrupt_0_pending      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_interrupt_0_sent          => CPMToWupper_s(0).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
                pcie0_cfg_msi_0_fail               => open, --CPMToWupper_s(0).cfg_msix_0_fail              ,--: out STD_LOGIC;
                pcie0_cfg_msi_0_function_number     => WupperToCPM(0).cfg_msix_0_function_number   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                pcie0_cfg_msi_0_int_vector => (others => '0'),
                pcie0_cfg_msi_0_select => "0000",
                pcie0_cfg_msi_0_sent => open,
                pcie0_cfg_msix_0_address            => WupperToCPM(0).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
                pcie0_cfg_msix_0_data               => WupperToCPM(0).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                pcie0_cfg_msix_0_enable             => CPMToWupper_s(0).cfg_msix_0_enable(0)            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie0_cfg_msix_0_int_vector         => WupperToCPM(0).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
                pcie0_cfg_msix_0_mask               => CPMToWupper_s(0).cfg_msix_0_mask(0)              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                --pcie0_cfg_msix_0_mint_vector        => WupperToCPM(0).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                --pcie0_cfg_msix_0_sent               => open, --CPMToWupper_s(0).cfg_msix_0_sent              ,--: out STD_LOGIC;
                pcie0_cfg_msix_0_vec_pending        => WupperToCPM(0).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
                pcie0_cfg_msix_0_vec_pending_status => open, --CPMToWupper_s(0).cfg_msix_0_vec_pending_status,--: out STD_LOGIC;
                pcie0_m_axis_cq_0_tdata             => CPMToWupper_s(0).m_axis_cq_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_m_axis_cq_0_tkeep             => CPMToWupper_s(0).m_axis_cq_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_m_axis_cq_0_tlast             => CPMToWupper_s(0).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
                pcie0_m_axis_cq_0_tready            => WupperToCPM(0).m_axis_cq_0_tready           ,--: in STD_LOGIC;
                pcie0_m_axis_cq_0_tuser             => CPMToWupper_s(0).m_axis_cq_0_tuser            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
                pcie0_m_axis_cq_0_tvalid            => CPMToWupper_s(0).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
                pcie0_m_axis_rc_0_tdata             => CPMToWupper_s(0).m_axis_rc_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_m_axis_rc_0_tkeep             => CPMToWupper_s(0).m_axis_rc_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_m_axis_rc_0_tlast             => CPMToWupper_s(0).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
                pcie0_m_axis_rc_0_tready            => WupperToCPM(0).m_axis_rc_0_tready           ,--: in STD_LOGIC;
                pcie0_m_axis_rc_0_tuser             => CPMToWupper_s(0).m_axis_rc_0_tuser            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
                pcie0_m_axis_rc_0_tvalid            => CPMToWupper_s(0).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
                pcie0_s_axis_cc_0_tdata             => WupperToCPM(0).s_axis_cc_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_s_axis_cc_0_tkeep             => WupperToCPM(0).s_axis_cc_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_s_axis_cc_0_tlast             => WupperToCPM(0).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
                pcie0_s_axis_cc_0_tready            => CPMToWupper_s(0).s_axis_cc_0_tready           ,--: out STD_LOGIC;
                pcie0_s_axis_cc_0_tuser             => WupperToCPM(0).s_axis_cc_0_tuser            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
                pcie0_s_axis_cc_0_tvalid            => WupperToCPM(0).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
                pcie0_s_axis_rq_0_tdata             => WupperToCPM(0).s_axis_rq_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie0_s_axis_rq_0_tkeep             => WupperToCPM(0).s_axis_rq_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie0_s_axis_rq_0_tlast             => WupperToCPM(0).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
                pcie0_s_axis_rq_0_tready            => CPMToWupper_s(0).s_axis_rq_0_tready           ,--: out STD_LOGIC;
                pcie0_s_axis_rq_0_tuser             => WupperToCPM(0).s_axis_rq_0_tuser            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
                pcie0_s_axis_rq_0_tvalid            => WupperToCPM(0).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
                pcie0_user_clk_0                    => CPMToWupper_s(0).user_clk_0                   ,--: out STD_LOGIC;
                pcie0_user_lnk_up_0                 => CPMToWupper_s(0).user_lnk_up_0                ,--: out STD_LOGIC;
                pcie0_user_reset_0                  => user_reset_0                 ,--: out STD_LOGIC;
                pcie1_cfg_interrupt_0_intx_vector   => WupperToCPM(1).cfg_interrupt_0_intx_vector  ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_interrupt_0_pending       => WupperToCPM(1).cfg_interrupt_0_pending      ,--: in STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_interrupt_0_sent          => CPMToWupper_s(1).cfg_interrupt_0_sent         ,--: out STD_LOGIC;
                pcie1_cfg_msi_0_fail => open,
                --pcie1_cfg_msix_0_fail               => CPMToWupper_s(1).cfg_msix_0_fail              ,
                pcie1_cfg_msi_0_function_number     => WupperToCPM(1).cfg_msix_0_function_number   ,--: in STD_LOGIC_VECTOR ( 7 downto 0 );
                pcie1_cfg_msi_0_int_vector => (others => '0'),
                pcie1_cfg_msi_0_select => "0000",
                pcie1_cfg_msi_0_sent => open,
                pcie1_cfg_msix_0_address            => WupperToCPM(1).cfg_msix_0_address           ,--: in STD_LOGIC_VECTOR ( 63 downto 0 );
                pcie1_cfg_msix_0_data               => WupperToCPM(1).cfg_msix_0_data              ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                pcie1_cfg_msix_0_enable             => CPMToWupper_s(1).cfg_msix_0_enable(0)            ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                pcie1_cfg_msix_0_int_vector         => WupperToCPM(1).cfg_msix_0_int_vector        ,--: in STD_LOGIC;
                pcie1_cfg_msix_0_mask               => CPMToWupper_s(1).cfg_msix_0_mask(0)              ,--: out STD_LOGIC_VECTOR ( 3 downto 0 );
                --pcie1_cfg_msix_0_mint_vector        => WupperToCPM(1).cfg_msix_0_mint_vector       ,--: in STD_LOGIC_VECTOR ( 31 downto 0 );
                --pcie1_cfg_msix_0_sent               => CPMToWupper_s(1).cfg_msix_0_sent             ,
                pcie1_cfg_msix_0_vec_pending        => WupperToCPM(1).cfg_msix_0_vec_pending       ,--: in STD_LOGIC_VECTOR ( 1 downto 0 );
                pcie1_cfg_msix_0_vec_pending_status => open,
                --pcie1_cfg_msix_0_vec_pending_status => CPMToWupper_s(1).cfg_msix_0_vec_pending_status,
                pcie1_m_axis_cq_0_tdata             => CPMToWupper_s(1).m_axis_cq_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_m_axis_cq_0_tkeep             => CPMToWupper_s(1).m_axis_cq_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_m_axis_cq_0_tlast             => CPMToWupper_s(1).m_axis_cq_0_tlast            ,--: out STD_LOGIC;
                pcie1_m_axis_cq_0_tready            => WupperToCPM(1).m_axis_cq_0_tready           ,--: in STD_LOGIC;
                pcie1_m_axis_cq_0_tuser             => CPMToWupper_s(1).m_axis_cq_0_tuser            ,--: out STD_LOGIC_VECTOR ( 182 downto 0 );
                pcie1_m_axis_cq_0_tvalid            => CPMToWupper_s(1).m_axis_cq_0_tvalid           ,--: out STD_LOGIC;
                pcie1_m_axis_rc_0_tdata             => CPMToWupper_s(1).m_axis_rc_0_tdata            ,--: out STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_m_axis_rc_0_tkeep             => CPMToWupper_s(1).m_axis_rc_0_tkeep            ,--: out STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_m_axis_rc_0_tlast             => CPMToWupper_s(1).m_axis_rc_0_tlast            ,--: out STD_LOGIC;
                pcie1_m_axis_rc_0_tready            => WupperToCPM(1).m_axis_rc_0_tready           ,--: in STD_LOGIC;
                pcie1_m_axis_rc_0_tuser             => CPMToWupper_s(1).m_axis_rc_0_tuser            ,--: out STD_LOGIC_VECTOR ( 160 downto 0 );
                pcie1_m_axis_rc_0_tvalid            => CPMToWupper_s(1).m_axis_rc_0_tvalid           ,--: out STD_LOGIC;
                pcie1_s_axis_cc_0_tdata             => WupperToCPM(1).s_axis_cc_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_s_axis_cc_0_tkeep             => WupperToCPM(1).s_axis_cc_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_s_axis_cc_0_tlast             => WupperToCPM(1).s_axis_cc_0_tlast            ,--: in STD_LOGIC;
                pcie1_s_axis_cc_0_tready            => CPMToWupper_s(1).s_axis_cc_0_tready           ,--: out STD_LOGIC;
                pcie1_s_axis_cc_0_tuser             => WupperToCPM(1).s_axis_cc_0_tuser            ,--: in STD_LOGIC_VECTOR ( 80 downto 0 );
                pcie1_s_axis_cc_0_tvalid            => WupperToCPM(1).s_axis_cc_0_tvalid           ,--: in STD_LOGIC;
                pcie1_s_axis_rq_0_tdata             => WupperToCPM(1).s_axis_rq_0_tdata            ,--: in STD_LOGIC_VECTOR ( 511 downto 0 );
                pcie1_s_axis_rq_0_tkeep             => WupperToCPM(1).s_axis_rq_0_tkeep            ,--: in STD_LOGIC_VECTOR ( 15 downto 0 );
                pcie1_s_axis_rq_0_tlast             => WupperToCPM(1).s_axis_rq_0_tlast            ,--: in STD_LOGIC;
                pcie1_s_axis_rq_0_tready            => CPMToWupper_s(1).s_axis_rq_0_tready           ,--: out STD_LOGIC;
                pcie1_s_axis_rq_0_tuser             => WupperToCPM(1).s_axis_rq_0_tuser            ,--: in STD_LOGIC_VECTOR ( 178 downto 0 );
                pcie1_s_axis_rq_0_tvalid            => WupperToCPM(1).s_axis_rq_0_tvalid           ,--: in STD_LOGIC;
                pcie1_user_clk_0                    => CPMToWupper_s(1).user_clk_0                   ,--: out STD_LOGIC;
                pcie1_user_lnk_up_0                 => CPMToWupper_s(1).user_lnk_up_0                ,--: out STD_LOGIC;
                pcie1_user_reset_0                  => user_reset_1                 , --: out STD_LOGIC
                pl0_ref_clk_0                       => pl0_ref_clk,
                pl0_resetn_0 => pl0_resetn(0),
                scl_i => scl_i,
                scl_o => scl_o,
                sda_i => sda_i,
                sda_o => sda_o,
                sys_clk0_0_clk_n => sys_clk0_0_clk_n,
                sys_clk0_0_clk_p => sys_clk0_0_clk_p
            );
    end generate g_flx155;
    CPMToWupper_s(0).user_reset_0 <= user_reset_0;
    CPMToWupper_s(1).user_reset_0 <= user_reset_1;

end Behavioral;
