--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--
-------------------------------------------------------------------------------------------
-- Copyright � 2011-2012, Xilinx, Inc.
-- This file contains confidential and proprietary information of Xilinx, Inc. and is
-- protected under U.S. and international copyright and other intellectual property laws.
-------------------------------------------------------------------------------------------
--
-- Disclaimer:
-- This disclaimer is not a license and does not grant any rights to the materials
-- distributed herewith. Except as otherwise provided in a valid license issued to
-- you by Xilinx, and to the maximum extent permitted by applicable law: (1) THESE
-- MATERIALS ARE MADE AVAILABLE "AS IS" AND WITH ALL FAULTS, AND XILINX HEREBY
-- DISCLAIMS ALL WARRANTIES AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY,
-- INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT,
-- OR FITNESS FOR ANY PARTICULAR PURPOSE; and (2) Xilinx shall not be liable
-- (whether in contract or tort, including negligence, or under any other theory
-- of liability) for any loss or damage of any kind or nature related to, arising
-- under or in connection with these materials, including for any direct, or any
-- indirect, special, incidental, or consequential loss or damage (including loss
-- of data, profits, goodwill, or any type of loss or damage suffered as a result
-- of any action brought by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the possibility of the same.
--
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-safe, or for use in any
-- application requiring fail-safe performance, such as life-support or safety
-- devices or systems, Class III medical devices, nuclear facilities, applications
-- related to the deployment of airbags, or any other applications that could lead
-- to death, personal injury, or severe property or environmental damage
-- (individually and collectively, "Critical Applications"). Customer assumes the
-- sole risk and liability of any use of Xilinx products in Critical Applications,
-- subject only to applicable laws and regulations governing limitations on product
-- liability.
--
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS PART OF THIS FILE AT ALL TIMES.
--
-------------------------------------------------------------------------------------------
--
-- Traditional 16:1 Multiplexer in one Slice.
--
-- Suitable for Spartan-6, Virtex-6 and 7-Series devices.
--
-- This is an example of a multiplexer implemented using the techniques described
-- in XAPP522.
--
-- 24th May 2012.
--
-------------------------------------------------------------------------------------------
--
-- Format of this file.
--
-- The module defines the implementation of the logic using Xilinx primitives.
-- These ensure predictable synthesis results and maximise the density of the
-- implementation. The Unisim Library is used to define Xilinx primitives. It is also
-- used during simulation.
-- The source can be viewed at %XILINX%\vhdl\src\unisims\unisim_VCOMP.vhd
--
-------------------------------------------------------------------------------------------
--
-- Library declarations
--
-- Standard IEEE libraries
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--
library unisim;
use unisim.vcomponents.all;
--
-------------------------------------------------------------------------------------------
--
-- Main Entity for
--
entity standard_mux16 is
  Port (  data_in : in std_logic_vector(15 downto 0);
              sel : in std_logic_vector(3 downto 0);
         data_out : out std_logic);
  end standard_mux16;
--
-------------------------------------------------------------------------------------------
--
-- Start of Main Architecture for standard_mux16
--
architecture low_level_definition of standard_mux16 is
--
-------------------------------------------------------------------------------------------
--
-- Signals used in standard_mux16
--
-------------------------------------------------------------------------------------------
--
signal data_selection : std_logic_vector(3 downto 0);
signal       combiner : std_logic_vector(1 downto 0);
--
-------------------------------------------------------------------------------------------
--
-- Start of standard_mux16 circuit description
--
-------------------------------------------------------------------------------------------
--
begin

  selection0_lut: LUT6
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data_in(0),
            I1 => data_in(1),
            I2 => data_in(2),
            I3 => data_in(3),
            I4 => sel(0),
            I5 => sel(1),
             O => data_selection(0));


  selection1_lut: LUT6
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data_in(4),
            I1 => data_in(5),
            I2 => data_in(6),
            I3 => data_in(7),
            I4 => sel(0),
            I5 => sel(1),
             O => data_selection(1));


  combiner0_muxf7: MUXF7
  port map( I0 => data_selection(0),
            I1 => data_selection(1),
             S => sel(2),
             O => combiner(0));


  selection2_lut: LUT6
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data_in(8),
            I1 => data_in(9),
            I2 => data_in(10),
            I3 => data_in(11),
            I4 => sel(0),
            I5 => sel(1),
             O => data_selection(2));


  selection3_lut: LUT6
  generic map (INIT => X"FF00F0F0CCCCAAAA")
  port map( I0 => data_in(12),
            I1 => data_in(13),
            I2 => data_in(14),
            I3 => data_in(15),
            I4 => sel(0),
            I5 => sel(1),
             O => data_selection(3));


  combiner1_muxf7: MUXF7
  port map( I0 => data_selection(2),
            I1 => data_selection(3),
             S => sel(2),
             O => combiner(1));


  combiner_muxf8: MUXF8
  port map( I0 => combiner(0),
            I1 => combiner(1),
             S => sel(3),
             O => data_out);


end low_level_definition;

-------------------------------------------------------------------------------------------
--
-- END OF FILE standard_mux16.vhd
--
-------------------------------------------------------------------------------------------


