--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!------------------------------------------------------------------------------
--!
--!           NIKHEF - National Institute for Subatomic Physics
--!
--!                       Electronics Department
--!
--!-----------------------------------------------------------------------------
--! @class card_type_specific_ios
--!
--!
--! @author      Andrea Borga    (andrea.borga@nikhef.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)
--!
--!
--! @date        17/08/2015    created
--!
--! @version     1.0
--!
--! @brief
--! This module maps the card specific IO pins
--! ** VC-709
--! ** HTG-710
--!
--! @detail
--!
--!-----------------------------------------------------------------------------
--! @TODO
--!
--!
--! ------------------------------------------------------------------------------

--! @brief ieee

library work, ieee, UNISIM;
    use work.pcie_package.all;
    use work.centralRouter_package.all;
    use ieee.numeric_std.all;
    use UNISIM.VCOMPONENTS.all;
    use ieee.std_logic_unsigned.all;
    use ieee.std_logic_1164.all;

entity card_type_specific_ios is
    generic(
        CARD_TYPE            : integer := 710;
        OPTO_TRX             : integer := 2);
    port (
        rst_hw               : in      std_logic;
        rst_soft             : in      std_logic;
        opto_los             : in      std_logic_vector(OPTO_TRX-1 downto 0);
        i2cmux_rst           : out     std_logic;
        opto_inhibit         : out     std_logic_vector(OPTO_TRX-1 downto 0);
        opto_los_s           : out     std_logic_vector(OPTO_TRX-1 downto 0)
    );

end entity card_type_specific_ios;

architecture rtl of card_type_specific_ios is

    signal reset : std_logic;

begin

    reset <= rst_hw or rst_soft;

    -- HTG-711

    HTG711: if CARD_TYPE = 711 or CARD_TYPE = 712 generate
        i2cmux_rst      <= not reset;
        opto_inhibit    <= (others => '1');  -- MiniPOD_nRST: 0 = RESET
        opto_los_s      <= (others => '0');  -- randomly assigned
    end generate;

    -- HTG-710

    HTG710: if CARD_TYPE = 710 generate
        i2cmux_rst      <= reset;
        opto_inhibit    <= (others => '1'); -- CXP_RST_L: 0 = RESET
        opto_los_s      <= (others => '0');  -- randomly assigned
    end generate;


    -- VC-709

    VC709: if CARD_TYPE = 709 generate
        i2cmux_rst       <= not reset;
        opto_inhibit     <= (others => '0');  -- SFP_TX_DISABLE_H: 1 = DISABLE
        opto_los_s       <= opto_los;
    end generate ;

end architecture rtl ; -- of card_type_specific_ios

