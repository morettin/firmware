library ieee, xpm;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use xpm.vcomponents.all;

--! Wrapper for xpm_fifo_sync and xpm_fifo_async with some processes to act like
--! a true async fifo with ultraram.
--!
--! ***for now only supports WRITE_DATA_WIDTH <= READ_DATA_WIDTH***

entity xpm_uram_fifo_async is
    generic (
        -- Common module generics
        FIFO_MEMORY_TYPE         : string   := "auto";
        FIFO_WRITE_DEPTH         : integer  := 2048;
        CASCADE_HEIGHT           : integer  := 0; -- @suppress "Unused generic: CASCADE_HEIGHT is not used in work.xpm_uram_fifo_async(rtl)"
        RELATED_CLOCKS           : integer  := 0;
        WRITE_DATA_WIDTH         : integer  := 32;
        READ_MODE                : string   :="std";
        FIFO_READ_LATENCY        : integer  := 1;
        FULL_RESET_VALUE         : integer  := 0;
        USE_ADV_FEATURES         : string   :="0707";
        READ_DATA_WIDTH          : integer  := 32;
        CDC_SYNC_STAGES          : integer  := 2;
        WR_DATA_COUNT_WIDTH      : integer  := 1;
        PROG_FULL_THRESH         : integer  := 10;
        RD_DATA_COUNT_WIDTH      : integer  := 1;
        PROG_EMPTY_THRESH        : integer  := 10;
        DOUT_RESET_VALUE         : string   := "0";
        ECC_MODE                 : string   :="no_ecc";
        SIM_ASSERT_CHK           : integer  := 0    ; -- @suppress "Unused generic: SIM_ASSERT_CHK is not used in work.xpm_uram_fifo_async(rtl)"
        WAKEUP_TIME              : integer  := 0;
        USE_URAM                 : boolean
    );
    port (

        sleep          : in std_logic;
        rst            : in std_logic;
        wr_clk         : in std_logic;
        wr_en          : in std_logic;
        din            : in std_logic_vector(WRITE_DATA_WIDTH-1 downto 0);
        full           : out std_logic;
        prog_full      : out std_logic;
        wr_data_count  : out std_logic_vector(WR_DATA_COUNT_WIDTH-1 downto 0);
        overflow       : out std_logic;
        wr_rst_busy    : out std_logic;
        almost_full    : out std_logic;
        wr_ack         : out std_logic;
        rd_clk         : in std_logic;
        rd_en          : in std_logic;
        dout           : out std_logic_vector(READ_DATA_WIDTH-1 downto 0);
        empty          : out std_logic;
        prog_empty     : out std_logic;
        rd_data_count  : out std_logic_vector(RD_DATA_COUNT_WIDTH-1 downto 0);
        underflow      : out std_logic;
        rd_rst_busy    : out std_logic;
        almost_empty   : out std_logic;
        data_valid     : out std_logic;
        injectsbiterr  : in std_logic;
        injectdbiterr  : in std_logic;
        sbiterr        : out std_logic;
        dbiterr        : out std_logic
    );
end entity xpm_uram_fifo_async;

architecture rtl of xpm_uram_fifo_async is


begin
    g_USE_URAM: if USE_URAM generate
        g_WRITE_DATA_WIDTH_64: if WRITE_DATA_WIDTH = 64 generate --When WRITE_DATA_WIDTH = 64, the clock speed is high and we need a distributed (or block) fifo to separate the clock domains
            signal rst_rd_clk : std_logic;
            signal uram_fifo_din: std_logic_vector(READ_DATA_WIDTH-1 downto 0);
            signal uram_fifo_din_sreg: std_logic_vector((READ_DATA_WIDTH-WRITE_DATA_WIDTH)-1 downto 0);
            signal distr_fifo_dout: std_logic_vector(WRITE_DATA_WIDTH-1 downto 0);
            signal uram_fifo_wr_en : std_logic;
            signal distr_fifo_dvalid : std_logic;
            signal distr_fifo_rd_en : std_logic;
            signal distr_fifo_empty : std_logic;
            signal uram_fifo_full : std_logic;
            constant write_cycles: integer := READ_DATA_WIDTH / WRITE_DATA_WIDTH;
            signal write_cnt: integer range 0 to write_cycles-1;
            signal wr_data_count_rd_clk: std_logic_vector(WR_DATA_COUNT_WIDTH-1 downto 0);

        begin

            xpm_cdc_sync_rst_rdclk: xpm_cdc_sync_rst
                generic map (
                    DEST_SYNC_FF => CDC_SYNC_STAGES,
                    INIT => 1,
                    INIT_SYNC_FF => 0,
                    SIM_ASSERT_CHK => 0
                )
                port map (
                    src_rst =>  rst,
                    dest_clk => rd_clk,
                    dest_rst => rst_rd_clk
                );
            --Ultraram can't be asynchronous, so we place a small distributed ram fifo in front for clock domain crossing
            FifoDistr0 : xpm_fifo_async
                generic map (  -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                    FIFO_MEMORY_TYPE => "auto", --string; "auto", "block", or "distributed";
                    FIFO_WRITE_DEPTH => 16,
                    RELATED_CLOCKS => RELATED_CLOCKS,
                    WRITE_DATA_WIDTH => WRITE_DATA_WIDTH,
                    READ_MODE => "std", --string; "std" or "fwft";
                    FIFO_READ_LATENCY => 1, --positive integer
                    FULL_RESET_VALUE => FULL_RESET_VALUE, --positive integer; 0 or 1;
                    USE_ADV_FEATURES => "001F", -- String
                    READ_DATA_WIDTH => WRITE_DATA_WIDTH, --positive integer
                    CDC_SYNC_STAGES => CDC_SYNC_STAGES, --positive integer
                    WR_DATA_COUNT_WIDTH => 4,
                    PROG_FULL_THRESH => 13, --positive integer
                    RD_DATA_COUNT_WIDTH => 4,
                    PROG_EMPTY_THRESH => 5, --positive integer
                    DOUT_RESET_VALUE => "0", --string
                    ECC_MODE => "no_ecc", --string; "no_ecc" or "en_ecc";
                    WAKEUP_TIME => 0 --positive integer; 0 or 2;
                )
                port map (
                    sleep => sleep,
                    rst => rst,
                    wr_clk => wr_clk,
                    wr_en => wr_en,
                    din => din,
                    full => full,
                    prog_full => prog_full,
                    wr_data_count => open,
                    overflow => overflow,
                    wr_rst_busy => open,
                    almost_full => almost_full,
                    wr_ack => wr_ack,
                    rd_clk => rd_clk,
                    rd_en => distr_fifo_rd_en,
                    dout => distr_fifo_dout,
                    empty => distr_fifo_empty,
                    prog_empty => open,
                    rd_data_count => open,
                    underflow => open,
                    rd_rst_busy => open,
                    almost_empty => open,
                    data_valid => open,
                    injectsbiterr => '0',
                    injectdbiterr => '0',
                    sbiterr => open,
                    dbiterr => open
                );

            distr_fifo_rd_en <= not distr_fifo_empty and not uram_fifo_full;

            dvalid_proc: process(rd_clk)
            begin
                if rising_edge(rd_clk) then
                    distr_fifo_dvalid <= distr_fifo_rd_en;
                end if;
            end process;

            uram_fifo_din <= distr_fifo_dout & uram_fifo_din_sreg;
            uram_fifo_wr_en <= '1' when distr_fifo_dvalid = '1' and write_cnt = (write_cycles-1) else '0';

            datamux_proc: process(rd_clk)
            begin
                if rising_edge(rd_clk) then
                    if rst_rd_clk = '1' then
                        write_cnt <= 0;
                    else
                        if distr_fifo_dvalid = '1' then
                            uram_fifo_din_sreg <= uram_fifo_din(READ_DATA_WIDTH-1 downto WRITE_DATA_WIDTH);
                            if write_cnt /= write_cycles-1 then
                                write_cnt <= write_cnt + 1;
                            else
                                write_cnt <= 0;
                            end if;
                        end if;
                    end if;
                end if;
            end process;

            wr_data_count_rd_clk((WR_DATA_COUNT_WIDTH-RD_DATA_COUNT_WIDTH)-1 downto 0) <= std_logic_vector(to_unsigned(write_cnt, (WR_DATA_COUNT_WIDTH-RD_DATA_COUNT_WIDTH)));

            xpm_cdc_gray_inst_wr_data_cht : xpm_cdc_gray
                generic map (
                    DEST_SYNC_FF => 2,
                    INIT_SYNC_FF => 0,
                    REG_OUTPUT => 0,
                    SIM_ASSERT_CHK => 0,
                    SIM_LOSSLESS_GRAY_CHK => 0,
                    WIDTH => (WR_DATA_COUNT_WIDTH-RD_DATA_COUNT_WIDTH)
                )
                port map (
                    src_clk => rd_clk,
                    src_in_bin => wr_data_count_rd_clk((WR_DATA_COUNT_WIDTH-RD_DATA_COUNT_WIDTH)-1 downto 0),
                    dest_clk => wr_clk,
                    dest_out_bin => wr_data_count((WR_DATA_COUNT_WIDTH-RD_DATA_COUNT_WIDTH)-1 downto 0)
                );

            FifoUram0 : xpm_fifo_sync
                generic map (  -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                    FIFO_MEMORY_TYPE => "ultra", --string; "auto", "block", or "distributed";
                    FIFO_WRITE_DEPTH => (FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH)/READ_DATA_WIDTH, --positive integer
                    WRITE_DATA_WIDTH => READ_DATA_WIDTH, --positive integer
                    READ_MODE => READ_MODE,
                    FIFO_READ_LATENCY => FIFO_READ_LATENCY, --positive integer
                    FULL_RESET_VALUE => 1, --positive integer; 0 or 1;
                    USE_ADV_FEATURES => USE_ADV_FEATURES, -- String
                    READ_DATA_WIDTH => READ_DATA_WIDTH,
                    WR_DATA_COUNT_WIDTH => RD_DATA_COUNT_WIDTH, --positive integer
                    PROG_FULL_THRESH => (FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH)/READ_DATA_WIDTH - 5, --positive integer
                    RD_DATA_COUNT_WIDTH => RD_DATA_COUNT_WIDTH, --positive integer
                    PROG_EMPTY_THRESH => PROG_EMPTY_THRESH, --positive integer
                    DOUT_RESET_VALUE => DOUT_RESET_VALUE, --string
                    ECC_MODE => ECC_MODE, --string; "no_ecc" or "en_ecc";
                    WAKEUP_TIME => 2 --positive integer; 0 or 2;
                )
                port map (
                    sleep => sleep,
                    rst => rst_rd_clk,
                    wr_clk => rd_clk,
                    wr_en => uram_fifo_wr_en,
                    din => uram_fifo_din,
                    full => uram_fifo_full,
                    prog_full => open,
                    wr_data_count => open, --wr_data_count(WR_DATA_COUNT_WIDTH-1 downto (WR_DATA_COUNT_WIDTH-RD_DATA_COUNT_WIDTH)),
                    overflow => open,
                    wr_rst_busy => wr_rst_busy,
                    almost_full => open,
                    wr_ack => open,
                    rd_en => rd_en,
                    dout => dout,
                    empty => empty,
                    prog_empty => prog_empty,
                    rd_data_count => rd_data_count,
                    underflow => underflow,
                    rd_rst_busy => rd_rst_busy,
                    almost_empty => almost_empty,
                    data_valid => data_valid,
                    injectsbiterr => injectsbiterr,
                    injectdbiterr => injectdbiterr,
                    sbiterr => sbiterr,
                    dbiterr => dbiterr
                );
        else generate
            --!If the ratio between read/write width is < 8 we can use a single URAM fifo, otherwise we use 2
            g_DATAWIDTH_LT512: if (READ_DATA_WIDTH < 512 ) generate
                fifo0: xpm_fifo_sync
                    generic map( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                        -- Common module generics
                        FIFO_MEMORY_TYPE        => "ULTRA"   ,
                        FIFO_WRITE_DEPTH        => FIFO_WRITE_DEPTH    ,
                        WRITE_DATA_WIDTH        => WRITE_DATA_WIDTH    ,
                        READ_MODE               => READ_MODE           ,
                        FIFO_READ_LATENCY       => FIFO_READ_LATENCY   ,
                        FULL_RESET_VALUE        => FULL_RESET_VALUE    ,
                        USE_ADV_FEATURES        => USE_ADV_FEATURES    ,
                        READ_DATA_WIDTH         => READ_DATA_WIDTH     ,
                        WR_DATA_COUNT_WIDTH     => WR_DATA_COUNT_WIDTH ,
                        PROG_FULL_THRESH        => PROG_FULL_THRESH    ,
                        RD_DATA_COUNT_WIDTH     => RD_DATA_COUNT_WIDTH ,
                        PROG_EMPTY_THRESH       => PROG_EMPTY_THRESH   ,
                        DOUT_RESET_VALUE        => DOUT_RESET_VALUE    ,
                        ECC_MODE                => ECC_MODE            ,
                        WAKEUP_TIME             => WAKEUP_TIME
                    )
                    port map(
                        sleep          => sleep        ,
                        rst            => rst          ,
                        wr_clk         => wr_clk       ,
                        wr_en          => wr_en        ,
                        din            => din          ,
                        full           => full         ,
                        prog_full      => prog_full    ,
                        wr_data_count  => wr_data_count,
                        overflow       => overflow     ,
                        wr_rst_busy    => wr_rst_busy  ,
                        almost_full    => almost_full  ,
                        wr_ack         => wr_ack       ,
                        rd_en          => rd_en        ,
                        dout           => dout         ,
                        empty          => empty        ,
                        prog_empty     => prog_empty   ,
                        rd_data_count  => rd_data_count,
                        underflow      => underflow    ,
                        rd_rst_busy    => rd_rst_busy  ,
                        almost_empty   => almost_empty ,
                        data_valid     => data_valid   ,
                        injectsbiterr  => injectsbiterr,
                        injectdbiterr  => injectdbiterr,
                        sbiterr        => sbiterr      ,
                        dbiterr        => dbiterr
                    );
            else generate
                signal fifo0_rd_en: std_logic;
                signal fifo0_empty: std_logic;
                signal fifo1_full: std_logic;
                signal fifo0_dout: std_logic_vector(WRITE_DATA_WIDTH*4-1 downto 0);
            begin
                --First go from 32b to 128b
                fifo0: xpm_fifo_sync
                    generic map( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                        -- Common module generics
                        FIFO_MEMORY_TYPE        => "ULTRA"   ,
                        FIFO_WRITE_DEPTH        => FIFO_WRITE_DEPTH/2    ,
                        WRITE_DATA_WIDTH        => WRITE_DATA_WIDTH    ,
                        READ_MODE               => "FWFT"           ,
                        FIFO_READ_LATENCY       => FIFO_READ_LATENCY   ,
                        FULL_RESET_VALUE        => FULL_RESET_VALUE    ,
                        USE_ADV_FEATURES        => USE_ADV_FEATURES    ,
                        READ_DATA_WIDTH         => WRITE_DATA_WIDTH*4  ,
                        WR_DATA_COUNT_WIDTH     => WR_DATA_COUNT_WIDTH-1 ,
                        PROG_FULL_THRESH        => FIFO_WRITE_DEPTH/2 - 15,
                        RD_DATA_COUNT_WIDTH     => 1 ,
                        PROG_EMPTY_THRESH       => 6   ,
                        DOUT_RESET_VALUE        => DOUT_RESET_VALUE    ,
                        ECC_MODE                => ECC_MODE            ,
                        WAKEUP_TIME             => WAKEUP_TIME
                    )
                    port map(
                        sleep          => sleep        ,
                        rst            => rst          ,
                        wr_clk         => wr_clk       ,
                        wr_en          => wr_en        ,
                        din            => din          ,
                        full           => full         ,
                        prog_full      => prog_full    ,
                        wr_data_count  => wr_data_count(WR_DATA_COUNT_WIDTH-1 downto 1),
                        overflow       => overflow     ,
                        wr_rst_busy    => wr_rst_busy  ,
                        almost_full    => almost_full  ,
                        wr_ack         => wr_ack       ,
                        rd_en          => fifo0_rd_en        ,
                        dout           => fifo0_dout         ,
                        empty          => fifo0_empty        ,
                        prog_empty     => open   ,
                        rd_data_count  => open,
                        underflow      => open    ,
                        rd_rst_busy    => open,
                        almost_empty   => open,
                        data_valid     => open,
                        injectsbiterr  => '0',
                        injectdbiterr  => '0',
                        sbiterr        => open      ,
                        dbiterr        => open
                    );
                wr_data_count(0) <= '0';

                fifo0_rd_en <= not fifo0_empty and not fifo1_full;

                fifo1: xpm_fifo_sync
                    generic map( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                        -- Common module generics
                        FIFO_MEMORY_TYPE        => "ULTRA"   ,
                        FIFO_WRITE_DEPTH        => FIFO_WRITE_DEPTH/8    ,
                        WRITE_DATA_WIDTH        => WRITE_DATA_WIDTH*4    ,
                        READ_MODE               => READ_MODE           ,
                        FIFO_READ_LATENCY       => FIFO_READ_LATENCY   ,
                        FULL_RESET_VALUE        => FULL_RESET_VALUE    ,
                        USE_ADV_FEATURES        => USE_ADV_FEATURES    ,
                        READ_DATA_WIDTH         => READ_DATA_WIDTH     ,
                        WR_DATA_COUNT_WIDTH     => 1 ,
                        PROG_FULL_THRESH        => FIFO_WRITE_DEPTH/16    , --somewhere half way, prog_full is open.
                        RD_DATA_COUNT_WIDTH     => RD_DATA_COUNT_WIDTH-1 ,
                        PROG_EMPTY_THRESH       => PROG_EMPTY_THRESH   ,
                        DOUT_RESET_VALUE        => DOUT_RESET_VALUE    ,
                        ECC_MODE                => ECC_MODE            ,
                        WAKEUP_TIME             => WAKEUP_TIME
                    )
                    port map(
                        sleep          => sleep        ,
                        rst            => rst          ,
                        wr_clk         => wr_clk       ,
                        wr_en          => fifo0_rd_en  ,
                        din            => fifo0_dout    ,
                        full           => fifo1_full   ,
                        prog_full      => open   ,
                        wr_data_count  => open,
                        overflow       => open     ,
                        wr_rst_busy    => open  ,
                        almost_full    => open  ,
                        wr_ack         => open       ,
                        rd_en          => rd_en        ,
                        dout           => dout         ,
                        empty          => empty        ,
                        prog_empty     => prog_empty   ,
                        rd_data_count  => rd_data_count(RD_DATA_COUNT_WIDTH-2 downto 0),
                        underflow      => underflow    ,
                        rd_rst_busy    => rd_rst_busy  ,
                        almost_empty   => almost_empty ,
                        data_valid     => data_valid   ,
                        injectsbiterr  => injectsbiterr,
                        injectdbiterr  => injectdbiterr,
                        sbiterr        => sbiterr      ,
                        dbiterr        => dbiterr
                    );
                rd_data_count(RD_DATA_COUNT_WIDTH-1) <= '0';
            end generate;
        end generate;
    end generate;
    g_BRAM: if not USE_URAM generate
        fifo0: xpm_fifo_async
            generic map( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
                -- Common module generics
                FIFO_MEMORY_TYPE        =>FIFO_MEMORY_TYPE    ,
                FIFO_WRITE_DEPTH        =>FIFO_WRITE_DEPTH    ,
                RELATED_CLOCKS          =>RELATED_CLOCKS      ,
                WRITE_DATA_WIDTH        =>WRITE_DATA_WIDTH    ,
                READ_MODE               =>READ_MODE           ,
                FIFO_READ_LATENCY       =>FIFO_READ_LATENCY   ,
                FULL_RESET_VALUE        =>FULL_RESET_VALUE    ,
                USE_ADV_FEATURES        =>USE_ADV_FEATURES    ,
                READ_DATA_WIDTH         =>READ_DATA_WIDTH     ,
                CDC_SYNC_STAGES         =>CDC_SYNC_STAGES     ,
                WR_DATA_COUNT_WIDTH     =>WR_DATA_COUNT_WIDTH ,
                PROG_FULL_THRESH        =>PROG_FULL_THRESH    ,
                RD_DATA_COUNT_WIDTH     =>RD_DATA_COUNT_WIDTH ,
                PROG_EMPTY_THRESH       =>PROG_EMPTY_THRESH   ,
                DOUT_RESET_VALUE        =>DOUT_RESET_VALUE    ,
                ECC_MODE                =>ECC_MODE            ,
                WAKEUP_TIME             =>WAKEUP_TIME
            )
            port map(
                sleep          => sleep        ,
                rst            => rst          ,
                wr_clk         => wr_clk       ,
                wr_en          => wr_en        ,
                din            => din          ,
                full           => full         ,
                prog_full      => prog_full    ,
                wr_data_count  => wr_data_count,
                overflow       => overflow     ,
                wr_rst_busy    => wr_rst_busy  ,
                almost_full    => almost_full  ,
                wr_ack         => wr_ack       ,
                rd_clk         => rd_clk       ,
                rd_en          => rd_en        ,
                dout           => dout         ,
                empty          => empty        ,
                prog_empty     => prog_empty   ,
                rd_data_count  => rd_data_count,
                underflow      => underflow    ,
                rd_rst_busy    => rd_rst_busy  ,
                almost_empty   => almost_empty ,
                data_valid     => data_valid   ,
                injectsbiterr  => injectsbiterr,
                injectdbiterr  => injectdbiterr,
                sbiterr        => sbiterr      ,
                dbiterr        => dbiterr
            );
    end generate;

end architecture rtl;
