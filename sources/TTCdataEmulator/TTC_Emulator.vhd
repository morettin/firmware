--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Israel Grayzman
--!               Thei Wijnen
--!               Alessandra Camplani
--!               Frans Schreuder
--!               Ohad Shaked
--!               Ali Skaf
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Design     : ttc_emulator_v2.1
-- Author     : Alessandra Camplani
-- Email      : alessandra.camplani@cern.ch
-- Created    : 22.01.2020
-- Revised by : Ali Skaf
-- Email      : ali.skaf@uni-goettingen.de
-- V2.1: provide OCR and long Bchannel support
-- Last edited; 27.06.2022

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

    use work.pcie_package.all;
    use work.FELIX_package.all;

Library xpm;
    use xpm.vcomponents.all;

entity TTC_Emulator is
    port(
        Clock                   : in  std_logic;
        Reset                   : in  std_logic;
        register_map_control    : in  register_map_control_type;

        add_s8                  : out std_logic_vector(7 downto 0) := (others => '0');
        add_d8                  : out std_logic_vector(7 downto 0) := (others => '0');
        add_strobe              : out std_logic                    := '0';
        add_e                   : out std_logic                    := '0';

        TTCout                  : out std_logic_vector(9 downto 0) := (others => '0');
        BUSYIn                  : in std_logic
    );
end TTC_Emulator;

architecture Behavioral of TTC_Emulator is

    signal      input_tpulse_period    : unsigned(31 downto 0) := to_unsigned(64, 32); --default value is 64;
    -- (unused) signal      input_tpulse_period_r  : unsigned(31 downto 0);
    signal      input_l1a_period    : unsigned(31 downto 0);
    -- (unused) signal      input_l1a_period_r  : unsigned(31 downto 0);
    signal      input_ecr_period    : unsigned(31 downto 0);
    signal      input_ecr_period_r  : unsigned(31 downto 0);
    signal      input_bcr_period    : unsigned(31 downto 0);
    --signal      input_bcr_period_r  : unsigned(31 downto 0);

    signal      input_long_Bch      : std_logic_vector(31 downto 0);    -- default ...
    signal      input_broadcast     : std_logic_vector(5 downto 0);
    signal      input_broadcast_r   : std_logic_vector(5 downto 0);

    signal      ECR_BCR_DELAY       : std_logic_vector(3 downto 0); --TODO, do we need this to be configurable? They were connected to BROADCAST(4:1), now constant 0.


    signal      set_default         : std_logic;
    -- signal      cycle_mode          : std_logic;

    signal      single_l1a_long     : std_logic := '0';
    signal      single_l1a          : std_logic := '0';
    signal      single_ecr_long     : std_logic := '0';
    signal      single_ecr          : std_logic := '0';
    signal      single_bcr_long     : std_logic := '0';
    signal      single_bcr          : std_logic := '0';

    signal         test_pulse, tpmode : std_logic := '0';   -- AS: Test_Pulse sent to output before L1A by a number of BC's specified in a tpulse_cnt "register" TBD (TTC_EMU_TP_PERIOD)
    -- AS: the tpulse_en (enable) is derived from rising edge of BROADCAST(5)

    signal      en          : std_logic := '0';
    signal      user_reset          : std_logic := '0';

    signal      l1a_cnt             : unsigned(31 downto 0) := (others => '0');
    signal      bcr_cnt             : unsigned(31 downto 0) := to_unsigned(3563, 32);
    signal      ecr_cnt             : unsigned(31 downto 0) := (others => '0');

    signal      prepare_bcr         : std_logic := '0';
    signal      prepare_bcr_r       : std_logic := '0';
    signal      prepare_ecr         : std_logic := '0';

    signal      Bch_long                : std_logic_vector(41 downto 0)  := (others => '0');
    signal      Bch_long_r              : std_logic_vector(41 downto 0)  := (others => '0');

    signal      Bchannel_short_b        : std_logic_vector(15 downto 0)  := (others => '1');
    signal      store_short_b           : std_logic_vector(15 downto 0)  := (others => '1');
    signal      short_b                 : std_logic_vector(15 downto 0)  := (others => '1'); -- @suppress "signal short_b is never read"

    signal      Bchannel_short_e        : std_logic_vector(15 downto 0)  := (others => '1');

    signal      Bchannel_fifo_in        : std_logic_vector(41 downto 0)  := (others => '0');
    signal      Bchannel_fifo_out       : std_logic_vector(41 downto 0)  := (others => '0');
    signal      Bchannel_fifo_out_r     : std_logic_vector(41 downto 0)  := (others => '1');
    --signal      Bchannel_low_prio       : std_logic_vector(41 downto 0)  := (others => '1');

    signal      Bch_e_fifo_wr           : std_logic;
    signal      Bch_l1_fifo_wr          : std_logic;
    signal      short_info              : std_logic_vector(7 downto 0);
    signal      short_hamming           : std_logic_vector(4 downto 0);
    signal      long_hamming            : std_logic_vector(6 downto 0);

    signal      fifo_wr                 : std_logic;
    signal      fifo_full               : std_logic; -- @suppress "signal fifo_full is never read"

    signal      trigger_read            : std_logic;
    signal      fifo_read               : std_logic;
    signal      fifo_read_r             : std_logic;
    signal      fifo_empty              : std_logic;
    signal      wr_rst_busy             : std_logic; -- @suppress "signal wr_rst_busy is never read"
    signal      rd_rst_busy             : std_logic; -- @suppress "signal rd_rst_busy is never read"

    signal      short_b_bchan_valid     : std_logic;
    signal      fifo_bchan_valid        : std_logic;
    constant    Bch_short_cnt_init      : unsigned(4 downto 0) := "10000";
    constant    fifo_cnt_init           : unsigned(6 downto 0) := "1000000";

    signal      short_bch_cnt           : unsigned(4 downto 0) := Bch_short_cnt_init; --AS: "10000";
    signal      fifo_bch_cnt            : unsigned(6 downto 0) := fifo_cnt_init ; --AS: "1000000";
    signal      serialization_process   : std_logic := '0';
    signal      Serial_Bchannel         : std_logic := '1';
    signal      Serial_Bchannel_r       : std_logic := '1';

    type        state_type is (idle, shortB, longB);--(idle, init, shortB, longB);
    signal      state                   : state_type;
    signal      sm_short_cnt            : unsigned(3 downto 0);
    signal      sm_long_cnt             : unsigned(6 downto 0);

    signal      l1_accept               : std_logic := '0';
    signal      l1_accept_r             : std_logic := '0';
    signal      bcr_dec                 : std_logic := '0';
    signal      ecr_dec                 : std_logic := '0';
    signal      bcr                     : std_logic := '0';
    signal      ecr                     : std_logic := '0';
    signal      broad_done              : std_logic := '0';
    signal      broad_dec               : unsigned(5 downto 0)          := (others => '0');
    signal      broad_dec_v             : std_logic_vector(5 downto 0)  := (others => '0');
    signal      broad                   : std_logic_vector(5 downto 0)  := (others => '0');

    signal      comb_ttype_in           : std_logic_vector(17 downto 0) := (others => '0');
    signal      comb_ttype_out          : std_logic_vector(17 downto 0) := (others => '0');
    signal      add_s8_i                : std_logic_vector(7 downto 0)  := (others => '0');
    signal      add_d8_i                : std_logic_vector(7 downto 0)  := (others => '0');
    signal      add_strobe_i            : std_logic                     := '0';
    signal      add_e_i                 : std_logic                     := '0';

    signal ocr_half_bcr_period: std_logic; --OCR, triggered on BROADCAST(5), but output half way the BCR period

begin

    -- if both ttc selector and ttc enable are high, than the enable will be active
    en <= to_sl(register_map_control.TTC_EMU.SEL) and to_sl(register_map_control.TTC_EMU.ENA);

    -- temporary here, I am not yet sure I want to keep it..
    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' then
                input_long_Bch      <= (others => '1');
                input_broadcast     <= (others => '0');
                set_default         <= '0';
                single_l1a_long     <= '0';
                single_ecr_long     <= '0';
                single_bcr_long     <= '0';
                input_l1a_period    <= (others => '0');
                input_ecr_period    <= (others => '0');
                input_bcr_period    <= to_unsigned(3564, 32);
                input_tpulse_period <= (others => '0');
                ECR_BCR_DELAY       <= x"0";
            elsif en = '1' then

                input_tpulse_period <= unsigned(register_map_control.TTC_EMU_TP_DELAY); --AS: added to pcie-package..
                input_l1a_period    <= unsigned(register_map_control.TTC_EMU_L1A_PERIOD);
                input_ecr_period    <= unsigned(register_map_control.TTC_EMU_ECR_PERIOD);
                input_bcr_period    <= unsigned(register_map_control.TTC_EMU_BCR_PERIOD);

                input_long_Bch      <= (register_map_control.TTC_EMU_LONG_CHANNEL_DATA); --AS: used for individually addressed command
                input_broadcast     <= (register_map_control.TTC_EMU_CONTROL.BROADCAST);

                set_default         <= to_sl(register_map_control.TTC_EMU_RESET);

                single_l1a_long     <= to_sl(register_map_control.TTC_EMU_CONTROL.L1A);
                single_ecr_long     <= to_sl(register_map_control.TTC_EMU_CONTROL.ECR);
                single_bcr_long     <= to_sl(register_map_control.TTC_EMU_CONTROL.BCR);
                ECR_BCR_DELAY       <= x"0"; --TODO: Create a proper register for this if needed.
            end if;
        end if;
    end process;


    ------------------------------------------------------------------------
    -- Low to high detection for single requests of L1A, ECR and BCR -------
    -- as well as reset. ---------------------------------------------------
    ------------------------------------------------------------------------
    --    tp_detect: entity work.hilo_detect
    --    generic map (
    --        lohi    => true
    --    )
    --    port map (
    --        clk     => Clock,     -- clock
    --        sig_in  => input_broadcast(5),     -- input signal
    --        sig_out => tpulse_en      -- output signal
    --    );
    -- enable test pulse when input_broadcast(5) = 1 (level
    tpmode <= input_broadcast(0);

    l1a_detect: entity work.hilo_detect
        generic map (
            lohi    => true
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => single_l1a_long,     -- input signal
            sig_out => single_l1a      -- output signal
        );

    ecr_detect: entity work.hilo_detect
        generic map (
            lohi    => true
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => single_ecr_long,     -- input signal
            sig_out => single_ecr      -- output signal
        );

    bcr_detect: entity work.hilo_detect
        generic map (
            lohi    => true
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => single_bcr_long,     -- input signal
            sig_out => single_bcr      -- output signal
        );

    reset_detect: entity work.hilo_detect
        generic map (
            lohi    => true
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => set_default,     -- input signal
            sig_out => user_reset      -- output signal
        );

    ------------------------------------------------------------------------------------------
    ----- test pulse logic; L1A single mode and cycle mode with adjustable frequency ---------
    ------------------------------------------------------------------------------------------

    process(Clock)
    begin
        if rising_edge(Clock) then
            test_pulse <= '0';                --AS: test_pulse is set only for 1 clock! (can be modified if required)
            l1_accept <= '0';
            if Reset = '1' or user_reset = '1' then
                l1_accept <= '0';
                l1a_cnt <= input_l1a_period;

            elsif en = '1' and (BUSYIn = '0' or register_map_control.TTC_EMU_CONTROL.BUSY_IN_ENABLE = "0") then
                if input_l1a_period /= x"0000_0000" then
                    if tpmode = '1' then
                        if l1a_cnt = (input_tpulse_period + 58) then --It takes 58 clock cycles to transmit the Test pulse through the fifo and serializer.
                            test_pulse <= '1';
                        end if;
                    end if;
                    if l1a_cnt > x"0000_0001" then
                        l1a_cnt <= l1a_cnt -1;
                    else
                        l1_accept <= '1';
                        l1a_cnt <= input_l1a_period;
                    end if;
                else --signle L1A mode
                    test_pulse <= '0';
                    l1_accept <= '0';
                    if tpmode = '1' then
                        if single_l1a = '1' then
                            test_pulse <= '1';
                            l1a_cnt <= input_tpulse_period + 59; --It takes 58 clock cycles to transmit the Test pulse through the fifo and serializer.
                        else
                            if l1a_cnt /= x"0000_0000" then
                                l1a_cnt <= l1a_cnt - 1;
                            end if;
                            if l1a_cnt = x"0000_0001" then
                                l1_accept <= '1';
                            end if;
                        end if;
                    else
                        l1_accept <= single_l1a;
                    end if;
                end if;
            end if;
        end if;
    end process;

    ---------------------------------------------------------------
    ----- Short Bchannel generation 1st step ----------------------
    ---------------------------------------------------------------



    -- This process is used to create a generate signal
    process(Clock)
        variable ocr_half_bcr_period_trig: std_logic;
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1' then
                prepare_bcr <= '0';
                bcr_cnt <= input_bcr_period;
                ocr_half_bcr_period_trig := '0';
                ocr_half_bcr_period <= '0';
            else
                ocr_half_bcr_period <= '0';
                if en = '1' then
                    if input_bcr_period /= x"0000_0000" then
                        if (input_broadcast(5) = '1' and (input_broadcast_r(5) = '0')) then
                            ocr_half_bcr_period_trig := '1';
                        end if;
                        if bcr_cnt = x"0000_0000" then
                            bcr_cnt <= input_bcr_period - 1;
                            prepare_bcr <= '1';
                        else
                            bcr_cnt <= bcr_cnt - 1;
                            prepare_bcr <= '0';
                        end if;
                        if bcr_cnt = ('0' & input_bcr_period(31 downto 1)) then --At half the BCR period, issue an OCR if triggered.
                            if ocr_half_bcr_period_trig = '1' then
                                ocr_half_bcr_period_trig := '0';
                                ocr_half_bcr_period <= '1';
                            end if;
                        end if;
                    else
                        prepare_bcr <= single_bcr;
                        ocr_half_bcr_period <= (input_broadcast(5) and (not input_broadcast_r(5)));
                    end if;
                else
                    prepare_bcr <= '0';
                end if;
            end if;
        end if;
    end process;

    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1' then
                prepare_ecr <= '0';
                ecr_cnt <= input_ecr_period + 1;
            else
                if en = '1' then
                    if input_ecr_period /= x"0000_0000" then
                        input_ecr_period_r <= input_ecr_period;
                        if input_ecr_period = input_ecr_period_r then
                            if ecr_cnt = x"0000_0001" then
                                ecr_cnt <= input_ecr_period + 1;
                                prepare_ecr <= '1';
                            else
                                ecr_cnt <= ecr_cnt - 1;
                                prepare_ecr <= '0';
                            end if;
                        else
                            ecr_cnt <= input_ecr_period + 1;
                        end if;
                    else
                        prepare_ecr <= single_ecr;
                    end if;
                else
                    prepare_ecr <= '0';
                end if;
            end if;
        end if;
    end process;

    ---------------------------------------------------------------
    ----- Short Bchannel generation 2nd step ----------------------
    ---------------------------------------------------------------
    --  IDLE=111111111111
    --  Short bchannel, 16 bits:
    --  00TTDDDDEBHHHHH1:
    --      T=test command, 2 bits
    --      D=Command/Data, 4 bits
    --      E=Event Counter Reset, 1 bit
    --      B=Bunch Counter Reset, 1

    -- TODO: Insert broadcast bits here.
    short_info      <= test_pulse &
                       (input_broadcast(1) and (not input_broadcast_r(1))) &
                       (input_broadcast(2) and (not input_broadcast_r(2))) &
                       (input_broadcast(3) and (not input_broadcast_r(3))) &
                       (input_broadcast(4) and (not input_broadcast_r(4))) &
                       ocr_half_bcr_period &
                       prepare_ecr & prepare_bcr; -- 8 bit T & D & BCR & ECR
    short_hamming   <= (others => '0');

    -- we want to distinguish between the short bchannel that
    -- contain the bcr and the ones that do not.
    -- This is extremely important to keep the correct periodicity on the
    -- bcr (less critical is the periodicity of the ecr.
    process(Clock)
    begin
        if rising_edge(Clock) then
            input_broadcast_r <= input_broadcast;
            if Reset = '1' or user_reset = '1' then
                Bchannel_short_b    <= (others => '1');
                Bchannel_short_e    <= (others => '1');
                Bch_e_fifo_wr       <= '0';

            elsif prepare_bcr = '1' then
                Bchannel_short_b    <= "00" & short_info & short_hamming & '1';
                Bchannel_short_e    <= (others => '1');
                Bch_e_fifo_wr       <= '0';
            elsif short_info /= x"00" then --Any rising edge of any broadcast bit or ECR, write into FIFO.
                Bchannel_short_b    <= (others => '1');
                Bchannel_short_e    <= "00" & short_info & short_hamming & '1';
                Bch_e_fifo_wr       <= '1';
            else
                Bchannel_short_b    <= (others => '1');
                Bchannel_short_e    <= (others => '1');
                Bch_e_fifo_wr       <= '0';
            end if;
        end if;
    end process;

    ---------------------------------------------------------------
    ----- Long B channel ------------------------------------------
    ---------------------------------------------------------------
    --  IDLE=111111111111
    --  Long Addressed, 42 bits
    --  01AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDDHHHHHHH1:
    --      A= TTCrx address, 14 bits
    --      E= internal(0)/External(1), 1 bit
    --      S= SubAddress, 8 bits
    --      D= Data, 8 bits
    --      H= Hamming Code, 7 bits
    --
    -- AS:  input_long_Bch= AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD
    --      We need to calculate corresponding Hamming code for input_long_Bch = d[31..0]
    --
    -- h[0] = d[0]^d[1]^d[2]^d[3]^d[4]^d[5];
    -- h[1] = d[6]^d[7]^d[8]^d[9]^d[10]^d[11]^d[12]^d[13]^d[14]^d[15]^d[16]^d[17]^d[18]^d[19]^d[20];
    -- h[2] = d[6]^d[7]^d[8]^d[9]^d[10]^d[11]^d[12]^d[13]^d[21]^d[22]^d[23]^d[24]^d[25]^d[26]^d[27];
    -- h[3] = d[0]^d[1]^d[2]^d[6]^d[7]^d[8]^d[9]^d[14]^d[15]^d[16]^d[17]^d[21]^d[22]^d[23]^d[24]^d[28]^d[29]^d[30];
    -- h[4] = d[0]^d[3]^d[4]^d[6]^d[7]^d[10]^d[11]^d[14]^d[15]^d[18]^d[19]^d[21]^d[22]^d[25]^d[26]^d[28]^d[29]^d[31];
    -- h[5] = d[1]^d[3]^d[5]^d[6]^d[8]^d[10]^d[12]^d[14]^d[16]^d[18]^d[20]^d[21]^d[23]^d[25]^d[27]^d[28]^d[30]^d[31];
    -- h[6] = hmg[0]^hmg[1]^hmg[2]^hmg[3]^hmg[4]^hmg[5]^d[0]^d[1]^d[2]^d[3]^d[4]^d[5]^d[6]^d[7]^d[8]^d[9]^d[10]^d[11]^d[12]^d[13]^d[14]^d[15]^d[16]^d[17]^d[18]^d[19]^d[20]^d[21]^d[22]^d[23]^d[24]^d[25]^d[26]^d[27]^d[28]^d[29]^d[30]^d[31];

    long_hamming(0) <= input_long_Bch(0) xor input_long_Bch(1) xor input_long_Bch(2) xor input_long_Bch(3) xor input_long_Bch(4) xor input_long_Bch(5);

    long_hamming(1) <= input_long_Bch(6) xor input_long_Bch(7) xor input_long_Bch(8) xor input_long_Bch(9) xor input_long_Bch(10) xor input_long_Bch(11) xor
                       input_long_Bch(12) xor input_long_Bch(13) xor input_long_Bch(14) xor input_long_Bch(15) xor input_long_Bch(16) xor
                       input_long_Bch(17) xor input_long_Bch(18) xor input_long_Bch(19) xor input_long_Bch(20);

    long_hamming(2) <= input_long_Bch(6) xor input_long_Bch(7) xor input_long_Bch(8) xor input_long_Bch(9) xor input_long_Bch(10) xor input_long_Bch(11) xor
                       input_long_Bch(12) xor input_long_Bch(13) xor input_long_Bch(21) xor input_long_Bch(22) xor input_long_Bch(23) xor
                       input_long_Bch(24) xor input_long_Bch(25) xor input_long_Bch(26) xor input_long_Bch(27);

    long_hamming(3) <= input_long_Bch(0) xor input_long_Bch(1) xor input_long_Bch(2) xor input_long_Bch(6) xor input_long_Bch(7) xor input_long_Bch(8) xor input_long_Bch(9) xor
                       input_long_Bch(11) xor input_long_Bch(14) xor input_long_Bch(15) xor input_long_Bch(16) xor input_long_Bch(17) xor input_long_Bch(21) xor
                       input_long_Bch(22) xor input_long_Bch(23) xor input_long_Bch(24) xor input_long_Bch(28) xor input_long_Bch(29) xor input_long_Bch(30);

    long_hamming(4) <= input_long_Bch(0) xor input_long_Bch(3) xor input_long_Bch(4) xor input_long_Bch(6) xor input_long_Bch(7) xor input_long_Bch(10) xor
                       input_long_Bch(11) xor input_long_Bch(14) xor input_long_Bch(15) xor input_long_Bch(18) xor input_long_Bch(19) xor input_long_Bch(21) xor
                       input_long_Bch(22) xor input_long_Bch(25) xor input_long_Bch(26) xor input_long_Bch(28) xor input_long_Bch(29) xor input_long_Bch(31);

    long_hamming(5) <= input_long_Bch(1) xor input_long_Bch(3) xor input_long_Bch(5) xor input_long_Bch(6) xor input_long_Bch(8) xor input_long_Bch(10) xor
                       input_long_Bch(12) xor input_long_Bch(14) xor input_long_Bch(16) xor input_long_Bch(18) xor input_long_Bch(20) xor input_long_Bch(21) xor
                       input_long_Bch(23) xor input_long_Bch(25) xor input_long_Bch(27) xor input_long_Bch(28) xor input_long_Bch(30) xor input_long_Bch(31);

    long_hamming(6) <= input_long_Bch(2) xor input_long_Bch(4) xor input_long_Bch(5) xor input_long_Bch(7) xor input_long_Bch(8) xor input_long_Bch(10) xor
                       input_long_Bch(11) xor input_long_Bch(13) xor input_long_Bch(14) xor input_long_Bch(17) xor input_long_Bch(19) xor input_long_Bch(20) xor
                       input_long_Bch(21) xor input_long_Bch(24) xor input_long_Bch(26) xor input_long_Bch(27) xor input_long_Bch(29) xor input_long_Bch(30) xor input_long_Bch(31);

    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1' then

            elsif l1_accept = '1' then
                Bch_long       <= '0' & '1' & input_long_Bch & long_hamming & "1" ;
                Bch_l1_fifo_wr <= '1';

                add_s8_i         <= input_long_Bch(15 downto 8);
                add_strobe_i     <= '1';
                add_e_i          <= input_long_Bch(16);
                add_d8_i         <= input_long_Bch(7 downto 0);

            else
                Bch_long <= (others => '1');
                Bch_l1_fifo_wr <= '0';

                add_s8_i         <= (others => '0');
                add_strobe_i     <= '0';
                add_e_i          <= '0';
                add_d8_i         <= (others => '0');
            end if;
        end if;
    end process;

    -- This is done as the wrapper handle the ttype value when the fifo_empty is at zero,
    -- few clock cycles later than the current transmission
    comb_ttype_in   <= add_s8_i & add_strobe_i & add_e_i & add_d8_i;
    add_s8          <= comb_ttype_out (17 downto 10);
    add_strobe      <= comb_ttype_out (9);
    add_e           <= comb_ttype_out (8);
    add_d8          <= comb_ttype_out (7 downto 0);

    ttype_delay_info: entity work.delay_chain
        generic map (
            d_width => 18, -- width of the signal to be delayed
            d_depth => 5, -- number of clock cycles it shell be delayed
            on_reset => '0',
            ram_style => "auto"  )
        port map (
            clk         => Clock,                             -- clock
            rst         => Reset,                             -- sync reset
            sig_in      => comb_ttype_in,  -- input signal
            sig_out     => comb_ttype_out   -- delayed output signal
        );

    ---------------------------------------------------------------
    ----- Short ecr Bchannel (maybe) combined with Long Bchannel --
    ---------------------------------------------------------------
    ----- Logic to write the FIFO
    ---------------------------------------------------------------
    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1' or wr_rst_busy = '1' then

            elsif Bch_e_fifo_wr = '1' and Bch_l1_fifo_wr = '0' and l1_accept_r = '0' then
                Bchannel_fifo_in <= x"FFFFFF" & "11" & Bchannel_short_e;
                fifo_wr <= '1';
                Bch_long_r <= (others => '1');
            elsif Bch_e_fifo_wr = '0' and Bch_l1_fifo_wr = '1' and l1_accept_r = '0' then
                Bchannel_fifo_in <= Bch_long;
                fifo_wr <= '1';
                Bch_long_r <= (others => '1');
            elsif prepare_ecr = '1' and l1_accept = '1' and l1_accept_r = '0' then
                Bch_long_r <= Bch_long;
                l1_accept_r <= l1_accept;
                Bchannel_fifo_in <= x"FFFFFF" & "11" & Bchannel_short_e;
                fifo_wr <= '1';
            elsif prepare_ecr = '0' and l1_accept = '0' and l1_accept_r = '1' then
                Bchannel_fifo_in <= Bch_long_r;
                fifo_wr <= '1';
                Bch_long_r <= (others => '1');
            else
                fifo_wr <= '0';
                Bchannel_fifo_in <= (others => '1');
                Bch_long_r <= (others => '1');
            end if;
        end if;
    end process;



    ---- din is for the moment 42 bit and the FIFO is 512 deep.
    ---- No other special setting is in place.
    inst_fifo : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
            DOUT_RESET_VALUE      => "0",    -- String
            ECC_MODE              => "no_ecc",       -- String
            FIFO_MEMORY_TYPE      => "auto", -- String
            FIFO_READ_LATENCY     => 1,     -- DECIMAL
            FIFO_WRITE_DEPTH      => 512,    -- DECIMAL
            FULL_RESET_VALUE      => 0,      -- DECIMAL
            PROG_EMPTY_THRESH     => 5,     -- DECIMAL
            PROG_FULL_THRESH      => 507,      -- DECIMAL
            RD_DATA_COUNT_WIDTH   => 1,   -- DECIMAL
            READ_DATA_WIDTH       => 42,      -- DECIMAL
            READ_MODE             => "std",         -- String
            USE_ADV_FEATURES      => "0707", -- String
            WAKEUP_TIME           => 0,           -- DECIMAL
            WRITE_DATA_WIDTH      => 42,     -- DECIMAL
            WR_DATA_COUNT_WIDTH   => 1    -- DECIMAL
        )
        port map (
            sleep => '0',
            rst => Reset,
            wr_clk => Clock,
            wr_en => fifo_wr,
            din => Bchannel_fifo_in,
            full => fifo_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => wr_rst_busy,
            almost_full => open,
            wr_ack => open,
            rd_en => fifo_read,
            dout => Bchannel_fifo_out,
            empty => fifo_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => rd_rst_busy,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    ---------------------------------------------------------------
    ----- Logic to read the FIFO
    ---------------------------------------------------------------

    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1'then
                trigger_read <= '0';
            elsif ((bcr_cnt < (input_bcr_period-20)) and (bcr_cnt > 44)) or input_bcr_period = x"00000000" then
                if (fifo_empty = '0') and (serialization_process = '0') then
                    trigger_read <= '1';
                else
                    trigger_read <= '0';
                end if;
            else
                trigger_read <= '0';
            end if;
        end if;
    end process;

    read_signal: entity work.hilo_detect
        generic map (
            lohi    => true   -- switch sense to low -> high
        )
        port map (
            clk     => Clock,     -- clock
            sig_in  => trigger_read,     -- input signal
            sig_out => fifo_read      -- output signal
        );

    ---------------------------------------------------------------
    ----- Bchannel combination and serialization
    ---------------------------------------------------------------
    -- A windowd around the BCR has to be inserted such that
    -- Bchannels are not overlapping
    -- 3563 minus (slightly more than) 42 -> 3520
    -- 0 plus (slightly more than) 16 -> 20
    -- .... |3520...3563,0...20| ....
    -- .... |   safe window    | ....


    short_valid_b: entity work.PULSE_EXTENDER
        generic map (
            PULSE_LENGTH    => 17    -- number of counts
        )
        port map (
            CLK             => Clock,     -- clock
            RST             => Reset,     -- sync reset
            SIG_IN          => prepare_bcr,     -- input signal
            SIG_OUT         => short_b_bchan_valid      -- output signal
        );

    short_valid_e: entity work.PULSE_EXTENDER
        generic map (
            PULSE_LENGTH    => 42    -- number of counts
        )
        port map (
            CLK             => Clock,     -- clock
            RST             => Reset,     -- sync reset
            SIG_IN          => fifo_read_r,     -- input signal
            SIG_OUT         => fifo_bchan_valid      -- output signal
        );

    --FS: For 1 clock delay, we simply push our signals through a single flipflop.

    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' then
                fifo_read_r <= '0';
                prepare_bcr_r <= '0';
            else
                fifo_read_r <= fifo_read;
                prepare_bcr_r <= prepare_bcr;
            end if;
        end if;
    end process;

    -- Depending on what is triggered, with the shortBchannel for the BCR or the fifo output are registered
    -- The 2 have different lengths
    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1'then
                store_short_b       <= (others => '1');
                Bchannel_fifo_out_r <= (others => '1');
            elsif prepare_bcr_r  = '1' then
                store_short_b       <= Bchannel_short_b;
            elsif fifo_read_r = '1' then
                Bchannel_fifo_out_r <= Bchannel_fifo_out;
            else
                store_short_b       <= store_short_b;
                Bchannel_fifo_out_r <= Bchannel_fifo_out_r;
            end if;
        end if;
    end process;

    -- Serialization process for both long and short Bchannel
    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' or user_reset = '1'then
                --Bch_short_cnt_init                  <= "10000";
                --fifo_cnt_init                       <= "1000000";
                short_b                             <= (others => '1');
                short_bch_cnt                       <= Bch_short_cnt_init-1;
                fifo_bch_cnt                        <= fifo_cnt_init-1;
            else

                if short_b_bchan_valid = '1' then
                    if short_bch_cnt(short_bch_cnt'left) = '1' then
                        short_bch_cnt           <= Bch_short_cnt_init-1;
                        Serial_Bchannel         <= Serial_Bchannel;
                        serialization_process   <= '0';
                    elsif short_bch_cnt = "00000" then
                        short_bch_cnt           <= Bch_short_cnt_init;
                        Serial_Bchannel         <= '1';
                        serialization_process   <= '0';
                    else
                        short_bch_cnt           <= short_bch_cnt - 1;
                        Serial_Bchannel         <= store_short_b(to_integer(short_bch_cnt));
                        serialization_process   <= '1';
                    end if;

                elsif fifo_bchan_valid = '1' then
                    short_b                     <= (others => '1');
                    if fifo_bch_cnt(fifo_bch_cnt'left) = '1' then
                        fifo_bch_cnt            <= fifo_cnt_init-1;
                        Serial_Bchannel         <= '1';
                        serialization_process   <= '0';
                    -- as I don't have 64 bit but only 42 I stopped the counter before the zero
                    elsif fifo_bch_cnt = "0010110" then
                        fifo_bch_cnt            <= fifo_cnt_init;
                        Serial_Bchannel         <= '1';
                        serialization_process   <= '0';
                    else
                        fifo_bch_cnt            <= fifo_bch_cnt - 1;
                        Serial_Bchannel         <= Bchannel_fifo_out_r(to_integer(fifo_bch_cnt)-22);
                        serialization_process   <= '1';
                    end if;

                else
                    Serial_Bchannel             <= '1';
                    serialization_process       <= '0';
                    short_b                     <= (others => '1');
                    short_bch_cnt               <= Bch_short_cnt_init;
                    fifo_bch_cnt                <= fifo_cnt_init;
                end if;
            end if;
        end if;
    end process;

    ---------------------------------------------------------------
    ----- ECR, BCR decoding ---------------------------------------
    ---------------------------------------------------------------
    -- State machine to prerly decoded ECR/BCR from the short Bchannel
    -- States are (i) IDLE and then, depeding on the serialized
    -- info it can be either (ii) ShortB or (iii) LongB.
    process(Clock)
    begin
        if rising_edge(Clock) then

            Serial_Bchannel_r <= Serial_Bchannel;

            if Reset = '1' or user_reset = '1'then
                sm_short_cnt <= (others => '0');
                sm_long_cnt  <= (others => '0');
                state <= idle;
            else
                Serial_Bchannel_r <= Serial_Bchannel;

                case state is

                    -- IDLE STATE
                    when idle =>
                        --                    sm_short_cnt <= (others => '0');
                        --                    sm_long_cnt  <= (others => '0');
                        if Serial_Bchannel = '0' and Serial_Bchannel_r = '0' then --FS: Start bit 0, FMT bit 0.
                            state <= shortB;     --init
                        elsif Serial_Bchannel = '1' and Serial_Bchannel_r = '0' then --FS: Start bit 0, FMT bit 1
                            state <= longB;
                        else
                            state <= idle;
                        end if;

                    -- INIT STATE  -- AS: Removed as no need to have a specific state for this!

                    --SHORT B STATE
                    when shortB =>
                        sm_short_cnt <= sm_short_cnt + 1;

                        if ((sm_short_cnt >= 0) and (sm_short_cnt <= 5)) then
                            broad_dec(to_integer(sm_short_cnt)) <= Serial_Bchannel;
                            broad_done <= '0';
                            state <= shortB;
                            ecr_dec <= '0';
                            bcr_dec <= '0';

                        elsif sm_short_cnt = 6 then
                            broad_dec <= broad_dec;
                            broad_done <= '1';
                            ecr_dec <= Serial_Bchannel;
                            state <= shortB;
                            bcr_dec <= '0';

                        elsif sm_short_cnt = 7 then
                            broad_dec <= (others => '0');
                            broad_done <= '0';
                            bcr_dec <= Serial_Bchannel;
                            state <= shortB;
                            ecr_dec <= '0';

                        elsif sm_short_cnt = 14 then
                            sm_short_cnt <= (others => '0');
                            state <= idle;
                            bcr_dec <= '0';
                            ecr_dec <= '0';
                            broad_dec <= (others => '0');
                            broad_done <= '0';

                        else
                            state <= shortB;
                            bcr_dec <= '0';
                            ecr_dec <= '0';
                            broad_dec <= (others => '0');
                            broad_done <= '0';
                        end if;

                    -- LONG B STATE
                    when longB =>
                        sm_long_cnt <= sm_long_cnt + 1;

                        if sm_long_cnt = 40 then
                            sm_long_cnt <= (others => '0');
                            state <= idle;

                        else
                            state <= longB;
                        end if;
                end case;
            end if;
        end if;
    end process;


    -- In the normal decoding, these signals always appear after the Bchannel
    -- (but related to the appearance of the broadcast bits (4:1))
    bcr_delay: entity work.signal_delay
        port map (
            clk         => Clock,     -- clock
            count_in    => ECR_BCR_DELAY,
            sig_in      => bcr_dec,     -- input signal
            sig_out     => bcr      -- delayed output signal
        );

    ecr_delay: entity work.signal_delay
        port map (
            clk         => Clock,     -- clock
            count_in    => ECR_BCR_DELAY,
            sig_in   => ecr_dec,     -- input signal
            sig_out  => ecr      -- delayed output signal
        );


    -- Serialization process for both long and short Bchannel
    process(Clock)
    begin
        if rising_edge(Clock) then
            if Reset = '1' then
                broad_dec_v <= (others => '0');
            elsif broad_done = '1' then
                broad_dec_v <= std_logic_vector(broad_dec);
            else
                broad_dec_v <= (others => '0');
            end if;
        end if;
    end process;

    broad_delay: entity work.delay_chain
        generic map (
            d_width => 6,
            d_depth => 15,
            on_reset => '0',
            ram_style => "auto"
        )
        port map (
            clk         => Clock,     -- clock
            rst         => Reset,     -- sync reset
            sig_in      => broad_dec_v,     -- input signal
            sig_out     => broad      -- delayed output signal
        );

    ---------------------------------------------------------------
    ----- TTC signal output ---------------------------------------
    ---------------------------------------------------------------

    TTCout(0) <= l1_accept;
    TTCout(1) <= Serial_Bchannel;
    TTCout(2) <= single_bcr when (input_bcr_period = X"0000_0000") else bcr or single_bcr;--
    TTCout(3) <= single_ecr when (input_ecr_period = X"0000_0000") else ecr or single_ecr; --
    TTCout(4) <= broad(0); -- d(0), testpulse
    TTCout(5) <= broad(1); -- d(1) -- AS treated as soft reset
    TTCout(6) <= broad(2); -- d(2)
    TTCout(7) <= broad(3); -- d(3) -- AS treated as SCA reset
    TTCout(8) <= broad(4); -- t(0)
    TTCout(9) <= broad(5); -- t(1), OCR


end Behavioral;
