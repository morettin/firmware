--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Israel Grayzman
--!               Alessandra Camplani
--!               Frans Schreuder
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.pcie_package.all;


entity TTC_Emulator is
    generic     (
                    generateTTCemu : boolean := false
                );
    Port    (
                Clock                       : in  std_logic;
                Reset                       : in  std_logic;

                register_map_control        : in  register_map_control_type; 
                register_map_ttc_monitor    : out register_map_ttc_monitor_type; 	

                TTCin                       : in  std_logic_vector(9 downto 0); -- from TTC decoder
                TTCout                      : out std_logic_vector(9 downto 0)
            );
end TTC_Emulator;

architecture Behavioral of TTC_Emulator is

    constant SPARE          : std_logic := '0';

    signal TTC_EMU_SEL                  : std_logic;                        -- Select TTC data source 1 TTC Emu | 0 TTC Decoder
    signal TTC_EMU_ENA                  : std_logic;                        -- Clear to load into the TTC emulator’s memory the required sequence, Set to run the TTC emulator sequence
    signal TTC_EMU_CONTROL_Write_En     : std_logic;                        -- Write Enable, a pulse every time a value written to TTC_EMU_CONTROL register
    signal TTC_EMU_CONTROL_LAST_LINE    : std_logic;                        -- Last line of the sequence
    signal TTC_EMU_CONTROL_REPEAT       : std_logic;                        -- Repeat the sequence
    signal TTC_EMU_CONTROL_BROADCAST5   : std_logic;                        -- Broadcast 5
    signal TTC_EMU_CONTROL_BROADCAST4   : std_logic;                        -- Broadcast 4
    signal TTC_EMU_CONTROL_BROADCAST3   : std_logic;                        -- Broadcast 3
    signal TTC_EMU_CONTROL_BROADCAST2   : std_logic;                        -- Broadcast 2
    signal TTC_EMU_CONTROL_BROADCAST1   : std_logic;                        -- Broadcast 1
    signal TTC_EMU_CONTROL_BROADCAST0   : std_logic;                        -- Broadcast 0
    signal TTC_EMU_CONTROL_ECR          : std_logic;                        -- Event counter reset
    signal TTC_EMU_CONTROL_BCR          : std_logic;                        -- Bunch counter reset
    signal TTC_EMU_CONTROL_L1A          : std_logic;                        -- Level 1 Accept
    signal TTC_EMU_CONTROL_STEP_COUNTER : std_logic_vector(21 downto 0);    -- Step counter value

    signal WriteAddress                 : std_logic_vector( 9 downto 0);    -- memory write address
    signal MemoryFull                   : std_logic;                        -- memory full
    signal MemoryDin                    : std_logic_vector(35 downto 0);    -- memory data in
    signal ReadAddress                  : std_logic_vector( 9 downto 0);    -- memory read address
    signal MemoryDout                   : std_logic_vector(35 downto 0);    -- memory data out
    signal StepCounter                  : std_logic_vector(21 downto 0);    -- count down the step wait value
    signal LastLineToRun                : std_logic;                        -- last line that should run from the one that loaded into the memory: '0' running sequence, '1' last line of the sequence
    signal RepeatSequence               : std_logic;                        -- does should the steps run from the begining? '1' yes, '0' no
begin



TTC_EMULATOR_ENABLE: if (generateTTCemu = true) generate

--  TTC_EMU register
--  #2 - FULL: TTC Emulator memory full indication (read only)
--  #1 - SEL: Select TTC data source 1 TTC Emu, 0 TTC Decoder [default - '0']

--  #0 - ENA: Clear to load into the TTC emulator's memory the required sequence, Set to run the TTC emulator sequence [default - '0']
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |  63  |  62  |  61  |  60  |  59  |  58  |  57  |  56  |  55  |  54  |  53  |  52  |  51  |  50  |  49  |  48  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |  47  |  46  |  45  |  44  |  43  |  42  |  41  |  40  |  39  |  38  |  37  |  36  |  35  |  34  |  33  |  32  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |  31  |  30  |  29  |  28  |  27  |  26  |  25  |  24  |  23  |  22  |  21  |  20  |  19  |  18  |  17  |  16  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |  15  |  14  |  13  |  12  |  11  |  10  |   9  |   8  |   7  |   6  |   5  |   4  |   3  |   2  |   1  |   0  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |      |      |      |      |      |      |      |      |      |      |      |      |      | FULL | SEL  | ENA  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+

--  TTC_EMU_CONTROL register
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |  79  |  78  |  77  |  76  |  75  |  74  |  73  |  72  |  71  |  70  |  69  |  68  |  67  |  66  |  65  |  64  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |  WE  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |  63  |  62  |  61  |  60  |  59  |  58  |  57  |  56  |  55  |  54  |  53  |  52  |  51  |  50  |  49  |  48  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |      |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |  47  |  46  |  45  |  44  |  43  |  42  |  41  |  40  |  39  |  38  |  37  |  36  |  35  |  34  |  33  |  32  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |      |      |      |      |      |      |      |      |      |      |      |      | LAST | RPT  |      | BR5  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |  31  |  30  |  29  |  28  |  27  |  26  |  25  |  24  |  23  |  22  |  21  |  20  |  19  |  18  |  17  |  16  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  | BR4  | BR3  | BR2  | BR1  | BR0  | ECR  | BCR  | L1A  |      |      |              Step Counter Value
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--  |  15  |  14  |  13  |  12  |  11  |  10  |   9  |   8  |   7  |   6  |   5  |   4  |   3  |   2  |   1  |   0  |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+
--                                             Step Counter Value                                                   |
--  +------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+------+

    TTC_EMU_SEL                     <= to_sl(register_map_control.TTC_EMU.SEL);
    TTC_EMU_ENA                     <= to_sl(register_map_control.TTC_EMU.ENA);
    TTC_EMU_CONTROL_Write_En        <= to_sl(register_map_control.TTC_EMU_CONTROL.WE);          --Bit #64
    TTC_EMU_CONTROL_LAST_LINE       <= to_sl(register_map_control.TTC_EMU_CONTROL.LAST_LINE);   --Bit #35
    TTC_EMU_CONTROL_REPEAT          <= to_sl(register_map_control.TTC_EMU_CONTROL.REPEAT);      --Bit #34
    TTC_EMU_CONTROL_BROADCAST5      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST5);  --Bit #32
    TTC_EMU_CONTROL_BROADCAST4      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST4);  --Bit #31
    TTC_EMU_CONTROL_BROADCAST3      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST3);  --Bit #30
    TTC_EMU_CONTROL_BROADCAST2      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST2);  --Bit #29
    TTC_EMU_CONTROL_BROADCAST1      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST1);  --Bit #28
    TTC_EMU_CONTROL_BROADCAST0      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST0);  --Bit #27
    TTC_EMU_CONTROL_ECR             <= to_sl(register_map_control.TTC_EMU_CONTROL.ECR);         --Bit #26
    TTC_EMU_CONTROL_BCR             <= to_sl(register_map_control.TTC_EMU_CONTROL.BCR);         --Bit #25
    TTC_EMU_CONTROL_L1A             <= to_sl(register_map_control.TTC_EMU_CONTROL.L1A);         --Bit #24
    TTC_EMU_CONTROL_STEP_COUNTER    <= register_map_control.TTC_EMU_CONTROL.STEP_COUNTER;       --Bit #21:0

    -- handling the write memory address
    process (Reset, Clock)
    begin
        if (Reset = '1') then
            WriteAddress    <= (others => '0');
            MemoryFull      <= '0';
        elsif rising_edge(Clock) then
            -- TTC emulator memory loading enabled
            if (TTC_EMU_ENA = '0') then
                -- new data from the user
                if (TTC_EMU_CONTROL_Write_En = '1') then
                    -- last address, signal memory full
                    if (WriteAddress = X"3FF") then
                        WriteAddress    <= WriteAddress;
                        MemoryFull      <= '1';
                    else
                        WriteAddress    <= WriteAddress + 1;
                        MemoryFull      <= '0';
                    end if;
                -- Default values
                else
                    WriteAddress    <= WriteAddress;
                    MemoryFull      <= '0';
                end if;
            -- TTC emulator memory loading disabled
            else
                WriteAddress    <= (others => '0');
                MemoryFull      <= '0';
            end if;
        end if;
    end process;

    -- FULL bit in the record define as std_logic_vector and MemoryFull is an std_logic
    register_map_ttc_monitor.TTC_EMU.FULL   <= (others => MemoryFull);

    -- set the memory data in vector (36 bits)
    MemoryDin   <=  TTC_EMU_CONTROL_LAST_LINE & TTC_EMU_CONTROL_REPEAT & SPARE & TTC_EMU_CONTROL_BROADCAST5 & TTC_EMU_CONTROL_BROADCAST4 &
                    TTC_EMU_CONTROL_BROADCAST3 & TTC_EMU_CONTROL_BROADCAST2 & TTC_EMU_CONTROL_BROADCAST1 & TTC_EMU_CONTROL_BROADCAST0 &
                    TTC_EMU_CONTROL_ECR & TTC_EMU_CONTROL_BCR & TTC_EMU_CONTROL_L1A & SPARE & SPARE & TTC_EMU_CONTROL_STEP_COUNTER;


    --                    START
    --                      |
    --    +---------------->|<--------------+
    --    |                 |               |                 
    --  Read                |               |
    --    |                 |               | NO
    --   SET                |               |
    --    |         Step Counter Done? -----+
    --  Load                |
    --    |                 | YES
    --    |   NO            |
    --    +<------- Last Line To Run?
    --    |                 |
    --    |                 | YES
    --    |   YES           |
    --    +<------- Repeat Sequence?
    --                      |
    --                      | NO
    --                      |
    --                     END
    --
    -- handling the read memory address
    process (Reset, Clock)
    begin
        if (Reset = '1') then
            StepCounter     <= (others => '0');
            LastLineToRun   <= '0';
            RepeatSequence  <= '0';
            ReadAddress     <= (others => '0');
        elsif rising_edge(Clock) then
            -- run the content of the memory
            if (TTC_EMU_ENA = '1') then
                -- read the next step from the memory
                if (StepCounter = X"000000") then
                    -- last step read from the memory
                    if (LastLineToRun = '1') then
                        -- memory's content should run only once
                        if (RepeatSequence = '0') then
                            StepCounter     <= (others => '0');         -- clear the value of StepCounter to avoid the counter from ticking, until GO signal will clear
                            LastLineToRun   <= LastLineToRun;           -- preserve the value of LastLineToRun
                            RepeatSequence  <= RepeatSequence;          -- preserve the value of RepeatSequence
                            ReadAddress     <= ReadAddress;             -- preserve the value of ReadAddress
                        -- memory's content should run repetitively
                        else
                            StepCounter     <= MemoryDout(21 downto 0); -- set the value of StepCounter for the current step
                            LastLineToRun   <= MemoryDout(35);          -- set the LastLineToRun value (check once the current step over)
                            RepeatSequence  <= MemoryDout(34);          -- set the RepeatSequence value (check once the current step over) 
                            ReadAddress     <= ReadAddress + 1;         -- increamt the ReadAddress of the memory
                        end if;
                    -- still running the current sequence
                    else
                        StepCounter     <= MemoryDout(21 downto 0);     -- set the value of StepCounter for the current step
                        LastLineToRun   <= MemoryDout(35);              -- set the LastLineToRun value (check once the current step over)
                        RepeatSequence  <= MemoryDout(34);              -- set the RepeatSequence value (check once the current step over) 
                        ReadAddress     <= ReadAddress + 1;             -- increamt the ReadAddress of the memory
                    end if;
                -- wait: count down the step value
                else
                    StepCounter     <= StepCounter - 1;                 -- counting back to zero the value that required by the currect step 
                    LastLineToRun   <= LastLineToRun;                   -- preserve the value of LastLineToRun
                    RepeatSequence  <= RepeatSequence;                  -- preserve the value of RepeatSequence
                    ReadAddress     <= ReadAddress;                     -- preserve the value of ReadAddress
                end if;
            -- the content of the memory is inaccessible (GO signal is clear)
            else
                StepCounter     <= (others => '0');                     -- reset the value of StepCounter
                LastLineToRun   <= '0';                                 -- reset the value of LastLineToRun
                RepeatSequence  <= '0';                                 -- reset the value of RepeatSequence
                ReadAddress     <= (others => '0');                     -- reset the value of ReadAddress
            end if;
        end if;
    end process;

    TTC_Emulator_Memory : entity work.ttc_emulator_memory
        port map(
                    clka    => Clock,
                    wea     => register_map_control.TTC_EMU_CONTROL.WE,
                    addra   => WriteAddress,
                    dina    => MemoryDin,
                    douta   => open,
                    clkb    => Clock,
                    web     => "0",
                    addrb   => ReadAddress,
                    dinb    => (others => '0'),
                    doutb   => MemoryDout
                );

-- TTCin settings:
-- TTCin(0)    <= l1a;
-- TTCin(1)    <= channelB; 
-- TTCin(2)    <= brc_b; *BCR*
-- TTCin(3)    <= brc_e; *ECR*
-- TTCin(4)    <= brc_d4(0);
-- TTCin(5)    <= brc_d4(1);
-- TTCin(6)    <= brc_d4(2);
-- TTCin(7)    <= brc_d4(3);
-- TTCin(8)    <= brc_t2(0);
-- TTCin(9)    <= brc_t2(1);

-- Memory Din/Dout oredring:
-- TTC_EMU_CONTROL_BROADCAST5      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST5);  --Bit #32
-- TTC_EMU_CONTROL_BROADCAST4      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST4);  --Bit #31
-- TTC_EMU_CONTROL_BROADCAST3      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST3);  --Bit #30
-- TTC_EMU_CONTROL_BROADCAST2      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST2);  --Bit #29
-- TTC_EMU_CONTROL_BROADCAST1      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST1);  --Bit #28
-- TTC_EMU_CONTROL_BROADCAST0      <= to_sl(register_map_control.TTC_EMU_CONTROL.BROADCAST0);  --Bit #27
-- TTC_EMU_CONTROL_ECR             <= to_sl(register_map_control.TTC_EMU_CONTROL.ECR);         --Bit #26
-- TTC_EMU_CONTROL_BCR             <= to_sl(register_map_control.TTC_EMU_CONTROL.BCR);         --Bit #25
-- TTC_EMU_CONTROL_L1A             <= to_sl(register_map_control.TTC_EMU_CONTROL.L1A);         --Bit #24

-- SPARE is replacing the channel B data
TTCout  <=  (MemoryDout(32 downto 25) & SPARE & MemoryDout(24)) when (TTC_EMU_SEL = '1') else
            TTCin;

end generate TTC_EMULATOR_ENABLE;


TTC_EMULATOR_DISABLE: if (generateTTCemu = false) generate
-- FULL bit in the record define as std_logic_vector and MemoryFull is an std_logic
register_map_ttc_monitor.TTC_EMU.FULL   <= (others => '1'); -- when TTC_EMULATOR_DISABLE set the FULL indication high 

TTCout  <=  TTCin;
end generate TTC_EMULATOR_DISABLE;

end Behavioral;
