--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Kai Chen
--!               Frans Schreuder
--!               Thei Wijnen
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  EDAQ WIS.
--! Engineer: juna
--!
--! Create Date:    12/11/2015
--! Module Name:    TTCdataSwitch
--! Project Name:   FELIX
----------------------------------------------------------------------------------
--! Use standard library
library work, IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use ieee.numeric_std.all;
    use ieee.std_logic_unsigned.all;
    use work.pcie_package.all;
    use work.centralRouter_package.all;
    use work.FELIX_gbt_package.all;

--! selects TTC data source: TTC data emulator (10-bit counter) / TTC decoder
entity TTCdataSwitch is
    generic (
        generateTTCemu : boolean := false
    );
    port  (
        clk40   : in  std_logic;
        rst     : in  std_logic;
        register_map_control : in  register_map_control_type;
        TTCin   : in  std_logic_vector (9 downto 0); -- from TTC decoder
        TTCout  : out std_logic_vector (9 downto 0)
    );
end TTCdataSwitch;
--
architecture Behavioral of TTCdataSwitch is
    --
    signal TTCdata : std_logic_vector (9 downto 0) := (others=>'0');
    signal TTCemu_ena : std_logic := '0';
    signal bcr_emu    : std_logic := '0';
    signal bcr_cnt    : std_logic_vector(11 downto 0) := x"000";
--
begin
    --
    -- TTC elulator output enable uses GBT_EMU_ENA(0)
    process(clk40, rst)
    begin
        if rst = '1' then
            TTCemu_ena <= '0';
        elsif clk40'event and clk40 = '1' then
            TTCemu_ena <= to_sl(register_map_control.TTC_EMU.ENA);
        end if;
    end process;
    --
    ---
    DOgenerateTTCemu: if generateTTCemu = true generate
        ----
        ttc_emu: process(clk40, rst)
        begin
            if rst = '1' then
                TTCdata <= (others=>'0');
                bcr_cnt <= x"000";
                bcr_emu <= '0';
            elsif clk40'event and clk40 = '1' then
                if TTCemu_ena = '1' then
                    TTCdata <= TTCdata + 1;
                else
                    TTCdata <= (others=>'0');
                end if;
                if bcr_cnt = x"DEB" then
                    bcr_cnt <= x"000";
                    bcr_emu <= '1';
                else
                    bcr_emu <= '0';
                    bcr_cnt <= bcr_cnt + '1';
                end if;
            end if;
        end process;
    ----
    end generate DOgenerateTTCemu;
    ---
    ----
    ttc_out_selector: process(TTCemu_ena, TTCdata, TTCin, register_map_control, bcr_emu)
    begin
        if to_sl(register_map_control.TTC_EMU.SEL) = '1' then
            TTCout <= TTCdata(9 downto 3) & bcr_emu & TTCdata (1 downto 0);              -- Emulator
        else
            TTCout <= TTCin;                -- Decoder
        end if;
    end process;
----


end Behavioral;
