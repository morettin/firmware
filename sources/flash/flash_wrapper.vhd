--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!               Kai Chen
--!               Andrea Borga
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2017/03/18 04:43:14 PM
-- Design Name: FELIX BNL-711 FLASH IP CORE
-- Module Name: flash_wrapper for BNL-711
-- Project Name:
-- Target Devices: KCU
-- Tool Versions: Vivado
-- Description:
--              This module is the wrapper for flash operation
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------


library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.numeric_std.ALL;
    use IEEE.numeric_std_unsigned.ALL;

library UNISIM;
    use UNISIM.VComponents.all;

--library WORK;
    use work.pcie_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity flash_wrapper is
    Port (
        --system reset
        rst                 : in std_logic;

        -- EMC clock
        clk                 : in std_logic;

        -- flash port
        flash_a_msb         : inout std_logic_vector(1 downto 0);
        flash_a             : out std_logic_vector(24 downto 0);
        flash_d             : inout std_logic_vector(15 downto 0);
        flash_adv           : out std_logic;
        flash_cclk          : out std_logic;
        flash_ce            : out std_logic;
        flash_we            : out std_logic;
        flash_re            : out std_logic;
        register_map_control: in register_map_control_type;
        config_flash_rd     : out bitfield_config_flash_rd_r_type;
        -------
        -- use JTAG or FW to do the flash control
        flash_SEL : out std_logic
    );
end ;

architecture Behavioral of flash_wrapper is

    --signal flash_ce:std_logic;
    --signal clk : std_logic;
    signal flash_rd_data          : std_logic_vector(15 downto 0);
    signal flash_wr_data          : std_logic_vector(15 downto 0);
    signal flash_adr              : std_logic_vector(26 downto 0);
    signal flash_a_i              : std_logic_vector(26 downto 0);


    --signal  do_rst_i              : std_logic;
    --signal  do_init_i             : std_logic;
    signal do_readstatus_i        : std_logic;
    signal do_clearstatus_i       : std_logic;
    signal do_eraseblock_i        : std_logic;
    signal do_unlockblock_i       : std_logic;
    signal do_write_i             : std_logic;
    signal do_read_i              : std_logic;
    signal do_readdeviceident_i   : std_logic;
    signal do_cfiquery_i          : std_logic;
    signal flash_req_done_o       : std_logic;
    signal flash_busy_o           : std_logic;
    signal flash_sel_i            : std_logic;



    signal do_cfiquery_i_r        : std_logic;
    signal do_cfiquery_i_2r       : std_logic;
    signal do_cfiquery_i_rising   : std_logic;

    signal  do_write_i_r          : std_logic;
    signal  do_write_i_2r         : std_logic;
    signal  do_write_i_rising     : std_logic;

    signal  do_read_i_r           : std_logic;
    signal  do_read_i_2r          : std_logic;
    signal  do_read_i_rising      : std_logic;

    signal  do_readdeviceident_i_r        : std_logic;
    signal  do_readdeviceident_i_2r       : std_logic;
    signal  do_readdeviceident_i_rising   : std_logic;

    signal  do_unlockblock_i_r            : std_logic;
    signal  do_unlockblock_i_2r           : std_logic;
    signal  do_unlockblock_i_rising       : std_logic;

    signal  do_eraseblock_i_r             : std_logic;
    signal  do_eraseblock_i_2r            : std_logic;
    signal  do_eraseblock_i_rising        : std_logic;

    signal  do_clearstatus_i_r            : std_logic;
    signal  do_clearstatus_i_2r           : std_logic;
    signal  do_clearstatus_i_rising       : std_logic;

    signal  do_readstatus_i_r             : std_logic;
    signal  do_readstatus_i_2r            : std_logic;
    signal  do_readstatus_i_rising        : std_logic;

    --signal  do_init_i_r                   : std_logic;
    --signal  do_init_i_2r                  : std_logic;
    --signal  do_init_i_rising              : std_logic;

    --signal  do_rst_i_r                    : std_logic;
    --signal  do_rst_i_2r                   : std_logic;
    --signal  do_rst_i_rising               : std_logic;

    signal do_special_rdstatus_i_r        : std_logic;
    signal do_special_rdstatus_i          : std_logic;
    signal do_special_rdstatus_i_2r       : std_logic;
    signal do_special_rdstatus_i_rising   : std_logic;

    signal do_special_write_i             : std_logic;
    signal do_special_write_i_r           : std_logic;
    signal do_special_write_i_2r          : std_logic;
    signal do_special_write_i_rising      : std_logic;

    signal flash_par_sel_i                : std_logic:='0';

    signal flash_par_wr                   : std_logic_vector(1 downto 0);
    signal flash_par_rd                   : std_logic_vector(1 downto 0);

begin

    flash_a         <= flash_a_i(24 downto 0);
    flash_SEL       <= flash_sel_i;

    flash_a_msb     <= flash_par_wr when flash_par_sel_i = '1' else "ZZ";
    flash_par_rd    <= flash_a_msb when flash_par_sel_i = '0' else flash_par_wr;

    do_eraseblock_i      <= to_sl(register_map_control.CONFIG_FLASH_WR.DO_ERASEBLOCK);
    do_write_i           <= to_sl(register_map_control.CONFIG_FLASH_WR.DO_WRITE);
    do_read_i            <= to_sl(register_map_control.CONFIG_FLASH_WR.DO_READ);
    do_clearstatus_i     <= to_sl(register_map_control.CONFIG_FLASH_WR.DO_CLEARSTATUS);
    do_readstatus_i      <= to_sl(register_map_control.CONFIG_FLASH_WR.DO_READSTATUS);
    do_unlockblock_i     <= to_sl(register_map_control.CONFIG_FLASH_WR.DO_UNLOCK_BLOCK);
    do_special_write_i   <= to_sl(register_map_control.CONFIG_FLASH_WR.FAST_WRITE);
    do_special_rdstatus_i<= to_sl(register_map_control.CONFIG_FLASH_WR.FAST_READ);

    do_cfiquery_i        <= '0';
    do_readdeviceident_i <= to_sl(register_map_control.CONFIG_FLASH_WR.DO_READDEVICEID);
    --do_init_i            <= to_sl(register_map_control.CONFIG_FLASH_WR.DO_INIT);
    --do_rst_i             <= to_sl(register_map_control.CONFIG_FLASH_WR.DO_RESET);

    flash_wr_data        <= register_map_control.CONFIG_FLASH_WR.WRITE_DATA;
    flash_adr            <= register_map_control.CONFIG_FLASH_WR.ADDRESS;

    flash_sel_i          <= to_sl(register_map_control.CONFIG_FLASH_WR.FLASH_SEL);
    flash_par_sel_i      <= to_sl(register_map_control.CONFIG_FLASH_WR.PAR_CTRL);
    flash_par_wr         <= register_map_control.CONFIG_FLASH_WR.PAR_WR;

    config_flash_rd.PAR_RD          <= flash_par_rd;

    config_flash_rd.READ_DATA       <= flash_rd_data;
    config_flash_rd.FLASH_BUSY      <= (others => flash_busy_o);
    config_flash_rd.FLASH_REQ_DONE  <= (others => flash_req_done_o);
    --

    process(clk)
    begin
        if clk'event and clk='1' then
            do_cfiquery_i_r           <= do_cfiquery_i;
            do_cfiquery_i_2r          <= do_cfiquery_i_r;
            do_cfiquery_i_rising      <= do_cfiquery_i_r and (not do_cfiquery_i_2r);

            do_write_i_r              <= do_write_i;
            do_write_i_2r             <= do_write_i_r;
            do_write_i_rising         <= do_write_i_r and (not do_write_i_2r);

            do_read_i_r               <= do_read_i;
            do_read_i_2r              <= do_read_i_r;
            do_read_i_rising          <= do_read_i_r and (not do_read_i_2r);

            do_readdeviceident_i_r    <= do_readdeviceident_i;
            do_readdeviceident_i_2r   <= do_readdeviceident_i_r;
            do_readdeviceident_i_rising <= do_readdeviceident_i_r and (not do_readdeviceident_i_2r);

            do_unlockblock_i_r        <= do_unlockblock_i;
            do_unlockblock_i_2r       <= do_unlockblock_i_r;
            do_unlockblock_i_rising   <= do_unlockblock_i_r and (not do_unlockblock_i_2r);

            do_eraseblock_i_r         <= do_eraseblock_i;
            do_eraseblock_i_2r        <= do_eraseblock_i_r;
            do_eraseblock_i_rising    <= do_eraseblock_i_r and (not do_eraseblock_i_2r);

            do_clearstatus_i_r        <= do_clearstatus_i;
            do_clearstatus_i_2r       <= do_clearstatus_i_r;
            do_clearstatus_i_rising   <= do_clearstatus_i_r and (not do_clearstatus_i_2r);

            do_readstatus_i_r         <= do_readstatus_i;
            do_readstatus_i_2r        <= do_readstatus_i_r;
            do_readstatus_i_rising    <= do_readstatus_i_r and (not do_readstatus_i_2r);

            --do_init_i_r               <= do_init_i;
            --do_init_i_2r              <= do_init_i_r;
            --do_init_i_rising          <= do_init_i_r and (not do_init_i_2r);

            --do_rst_i_r                <= do_rst_i;
            --do_rst_i_2r               <= do_rst_i_r;
            --do_rst_i_rising           <= do_rst_i_r and (not do_rst_i_2r);

            do_special_rdstatus_i_r   <= do_special_rdstatus_i;
            do_special_rdstatus_i_2r  <= do_special_rdstatus_i_r;
            do_special_rdstatus_i_rising <= do_special_rdstatus_i_r and (not do_special_rdstatus_i_2r);

            do_special_write_i_r      <= do_special_write_i;
            do_special_write_i_2r     <= do_special_write_i_r;
            do_special_write_i_rising <= do_special_write_i_r and (not do_special_write_i_2r);

        end if;
    end process;


    cfi: entity work.flash_ipcore_bnl
        generic map(
            ADDR_WIDTH => 27
        )
        port map(
            clk_i => clk,
            rst_i => rst,
            --init_i              => do_init_i_rising,
            readstatus_i => do_readstatus_i_rising,
            clearstatus_i => do_clearstatus_i_rising,
            eraseblock_i => do_eraseblock_i_rising,
            unlockblock_i => do_unlockblock_i_rising,
            write_word_i => do_write_i_rising,
            special_write_i => do_special_write_i_rising,
            read_i => do_read_i_rising,
            readdevid_i => do_readdeviceident_i_rising,
            cfiquery_i => do_cfiquery_i_rising,
            special_rdstatus_i => do_special_rdstatus_i_rising,
            flash_data_rd_o => flash_rd_data,
            flash_data_wr_i => flash_wr_data,
            flash_d_debug => open,
            flash_addr_cmd_i => flash_adr,
            ipcore_op_done_o => flash_req_done_o,
            ipcore_busy_o => flash_busy_o,
            flash_dq_io => flash_d,
            flash_addr_o => flash_a_i,
            flash_adv_n_o => flash_adv,
            flash_ce_n_o => flash_ce,
            flash_clk_o => flash_cclk,
            flash_re_n_o => flash_re,
            --flash_rst_n_o       => open,
            --flash_wait_i        => '0',
            flash_we_n_o => flash_we,
            flash_wp_n_o => open

        );


end Behavioral;
