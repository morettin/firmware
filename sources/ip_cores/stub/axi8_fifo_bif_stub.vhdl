-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (lin64) Build 2902540 Wed May 27 19:54:35 MDT 2020
-- Date        : Mon Sep 28 15:34:59 2020
-- Host        : tarfa running 64-bit CentOS Linux release 7.6.1810 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /localstore/et/franss/felix/firmware2/Projects/FLX712_FELIX/FLX712_FELIX.srcs/sources_1/ip/axi8_fifo_bif/axi8_fifo_bif_stub.vhdl
-- Design      : axi8_fifo_bif
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;

entity axi8_fifo_bif is
    Port (
        wr_rst_busy : out STD_LOGIC; --@suppress
        rd_rst_busy : out STD_LOGIC; --@suppress
        m_aclk : in STD_LOGIC; --@suppress
        s_aclk : in STD_LOGIC; --@suppress
        s_aresetn : in STD_LOGIC; --@suppress
        s_axis_tvalid : in STD_LOGIC; --@suppress
        s_axis_tready : out STD_LOGIC; --@suppress
        s_axis_tdata : in STD_LOGIC_VECTOR ( 7 downto 0 ); --@suppress
        s_axis_tlast : in STD_LOGIC; --@suppress
        m_axis_tvalid : out STD_LOGIC; --@suppress
        m_axis_tready : in STD_LOGIC; --@suppress
        m_axis_tdata : out STD_LOGIC_VECTOR ( 7 downto 0 ); --@suppress
        m_axis_tlast : out STD_LOGIC; --@suppress
        axis_prog_full : out STD_LOGIC --@suppress
    );

end axi8_fifo_bif;

architecture stub of axi8_fifo_bif is
    attribute syn_black_box : boolean;
    attribute black_box_pad_pin : string;
    attribute syn_black_box of stub : architecture is true;
    attribute black_box_pad_pin of stub : architecture is "wr_rst_busy,rd_rst_busy,m_aclk,s_aclk,s_aresetn,s_axis_tvalid,s_axis_tready,s_axis_tdata[7:0],s_axis_tlast,m_axis_tvalid,m_axis_tready,m_axis_tdata[7:0],m_axis_tlast,axis_prog_full";
    attribute x_core_info : string;
    attribute x_core_info of stub : architecture is "fifo_generator_v13_2_5,Vivado 2020.1";
begin
end;
