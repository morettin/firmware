-- Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2021.2 (lin64) Build 3367213 Tue Oct 19 02:47:39 MDT 2021
-- Date        : Mon Sep 26 14:33:41 2022
-- Host        : DSKT-GST-001 running 64-bit Ubuntu 22.04.1 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/franss/felix/firmware_clone/Projects/FLX712_FELIX/FLX712_FELIX.gen/sources_1/ip/debug_bridge_0/debug_bridge_0_sim_netlist.vhdl
-- Design      : debug_bridge_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xcku115-flvf1924-2-e
-- --------------------------------------------------------------------------------
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
entity debug_bridge_0_bd_54be is
    port (
        clk : in STD_LOGIC;
        pcie3_cfg_ext_function_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
        pcie3_cfg_ext_read_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
        pcie3_cfg_ext_read_data_valid : out STD_LOGIC;
        pcie3_cfg_ext_read_received : in STD_LOGIC;
        pcie3_cfg_ext_register_number : in STD_LOGIC_VECTOR ( 9 downto 0 );
        pcie3_cfg_ext_write_byte_enable : in STD_LOGIC_VECTOR ( 3 downto 0 );
        pcie3_cfg_ext_write_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
        pcie3_cfg_ext_write_received : in STD_LOGIC
    );
    attribute ORIG_REF_NAME : string;
    attribute ORIG_REF_NAME of debug_bridge_0_bd_54be : entity is "bd_54be";
    attribute hw_handoff : string;
    attribute hw_handoff of debug_bridge_0_bd_54be : entity is "debug_bridge_0.hwdef";
end debug_bridge_0_bd_54be;

architecture STRUCTURE of debug_bridge_0_bd_54be is
    component debug_bridge_0_bd_54be_bsip_0 is
        port (
            drck : out STD_LOGIC;
            reset : out STD_LOGIC;
            sel : out STD_LOGIC;
            shift : out STD_LOGIC;
            tdi : out STD_LOGIC;
            update : out STD_LOGIC;
            capture : out STD_LOGIC;
            runtest : out STD_LOGIC;
            tck : out STD_LOGIC;
            tms : out STD_LOGIC;
            tap_tdo : out STD_LOGIC;
            tdo : in STD_LOGIC;
            tap_tdi : in STD_LOGIC;
            tap_tms : in STD_LOGIC;
            tap_tck : in STD_LOGIC
        );
    end component debug_bridge_0_bd_54be_bsip_0;
    component debug_bridge_0_bd_54be_pcie_jtag_0 is
        port (
            clk : in STD_LOGIC;
            read_received : in STD_LOGIC;
            write_received : in STD_LOGIC;
            register_number : in STD_LOGIC_VECTOR ( 9 downto 0 );
            function_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
            write_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
            write_byte_enable : in STD_LOGIC_VECTOR ( 3 downto 0 );
            read_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
            read_data_valid : out STD_LOGIC;
            tck : out STD_LOGIC;
            tms : out STD_LOGIC;
            tdi : out STD_LOGIC;
            tdo : in STD_LOGIC
        );
    end component debug_bridge_0_bd_54be_pcie_jtag_0;
    signal bsip_tap_tdo : STD_LOGIC;
    signal pcie_jtag_tck : STD_LOGIC;
    signal pcie_jtag_tdi : STD_LOGIC;
    signal pcie_jtag_tms : STD_LOGIC;
    signal NLW_bsip_capture_UNCONNECTED : STD_LOGIC;
    signal NLW_bsip_drck_UNCONNECTED : STD_LOGIC;
    signal NLW_bsip_reset_UNCONNECTED : STD_LOGIC;
    signal NLW_bsip_runtest_UNCONNECTED : STD_LOGIC;
    signal NLW_bsip_sel_UNCONNECTED : STD_LOGIC;
    signal NLW_bsip_shift_UNCONNECTED : STD_LOGIC;
    signal NLW_bsip_tck_UNCONNECTED : STD_LOGIC;
    signal NLW_bsip_tdi_UNCONNECTED : STD_LOGIC;
    signal NLW_bsip_tms_UNCONNECTED : STD_LOGIC;
    signal NLW_bsip_update_UNCONNECTED : STD_LOGIC;
    attribute syn_black_box : string;
    attribute syn_black_box of bsip : label is "TRUE";
    attribute x_core_info : string;
    attribute x_core_info of bsip : label is "bsip_v1_1_0_bsip,Vivado 2021.2";
    attribute syn_black_box of pcie_jtag : label is "TRUE";
    attribute x_core_info of pcie_jtag : label is "pcie_jtag_v1_0_0_pcie_jtag,Vivado 2021.2";
    attribute x_interface_info : string;
    attribute x_interface_info of clk : signal is "xilinx.com:signal:clock:1.0 CLK.CLK CLK";
    attribute x_interface_parameter : string;
    attribute x_interface_parameter of clk : signal is "XIL_INTERFACENAME CLK.CLK, ASSOCIATED_BUSIF pcie3_cfg_ext, CLK_DOMAIN bd_54be_clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, INSERT_VIP 0, PHASE 0.0";
    attribute x_interface_info of pcie3_cfg_ext_read_data_valid : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext read_data_valid";
    attribute x_interface_info of pcie3_cfg_ext_read_received : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext read_received";
    attribute x_interface_info of pcie3_cfg_ext_write_received : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext write_received";
    attribute x_interface_info of pcie3_cfg_ext_function_number : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext function_number";
    attribute x_interface_info of pcie3_cfg_ext_read_data : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext read_data";
    attribute x_interface_info of pcie3_cfg_ext_register_number : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext register_number";
    attribute x_interface_info of pcie3_cfg_ext_write_byte_enable : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext write_byte_enable";
    attribute x_interface_info of pcie3_cfg_ext_write_data : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext write_data";
begin
    bsip: component debug_bridge_0_bd_54be_bsip_0
        port map (
            capture => NLW_bsip_capture_UNCONNECTED,
            drck => NLW_bsip_drck_UNCONNECTED,
            reset => NLW_bsip_reset_UNCONNECTED,
            runtest => NLW_bsip_runtest_UNCONNECTED,
            sel => NLW_bsip_sel_UNCONNECTED,
            shift => NLW_bsip_shift_UNCONNECTED,
            tap_tck => pcie_jtag_tck,
            tap_tdi => pcie_jtag_tdi,
            tap_tdo => bsip_tap_tdo,
            tap_tms => pcie_jtag_tms,
            tck => NLW_bsip_tck_UNCONNECTED,
            tdi => NLW_bsip_tdi_UNCONNECTED,
            tdo => '0',
            tms => NLW_bsip_tms_UNCONNECTED,
            update => NLW_bsip_update_UNCONNECTED
        );
    pcie_jtag: component debug_bridge_0_bd_54be_pcie_jtag_0
        port map (
            clk => clk,
            function_number(7 downto 0) => pcie3_cfg_ext_function_number(7 downto 0),
            read_data(31 downto 0) => pcie3_cfg_ext_read_data(31 downto 0),
            read_data_valid => pcie3_cfg_ext_read_data_valid,
            read_received => pcie3_cfg_ext_read_received,
            register_number(9 downto 0) => pcie3_cfg_ext_register_number(9 downto 0),
            tck => pcie_jtag_tck,
            tdi => pcie_jtag_tdi,
            tdo => bsip_tap_tdo,
            tms => pcie_jtag_tms,
            write_byte_enable(3 downto 0) => pcie3_cfg_ext_write_byte_enable(3 downto 0),
            write_data(31 downto 0) => pcie3_cfg_ext_write_data(31 downto 0),
            write_received => pcie3_cfg_ext_write_received
        );
end STRUCTURE;
library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;
entity debug_bridge_0 is
    port (
        clk : in STD_LOGIC;
        pcie3_cfg_ext_function_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
        pcie3_cfg_ext_read_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
        pcie3_cfg_ext_read_data_valid : out STD_LOGIC;
        pcie3_cfg_ext_read_received : in STD_LOGIC;
        pcie3_cfg_ext_register_number : in STD_LOGIC_VECTOR ( 9 downto 0 );
        pcie3_cfg_ext_write_byte_enable : in STD_LOGIC_VECTOR ( 3 downto 0 );
        pcie3_cfg_ext_write_data : in STD_LOGIC_VECTOR ( 31 downto 0 );
        pcie3_cfg_ext_write_received : in STD_LOGIC
    );
    attribute NotValidForBitStream : boolean;
    attribute NotValidForBitStream of debug_bridge_0 : entity is true;
    attribute CHECK_LICENSE_TYPE : string;
    attribute CHECK_LICENSE_TYPE of debug_bridge_0 : entity is "debug_bridge_0,bd_54be,{}";
    attribute downgradeipidentifiedwarnings : string;
    attribute downgradeipidentifiedwarnings of debug_bridge_0 : entity is "yes";
    attribute x_core_info : string;
    attribute x_core_info of debug_bridge_0 : entity is "bd_54be,Vivado 2021.2";
end debug_bridge_0;

architecture STRUCTURE of debug_bridge_0 is
    attribute hw_handoff : string;
    attribute hw_handoff of U0 : label is "debug_bridge_0.hwdef";
    attribute x_interface_info : string;
    attribute x_interface_info of clk : signal is "xilinx.com:signal:clock:1.0 CLK.clk CLK";
    attribute x_interface_parameter : string;
    attribute x_interface_parameter of clk : signal is "XIL_INTERFACENAME CLK.clk, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN bd_54be_clk, ASSOCIATED_BUSIF pcie3_cfg_ext, INSERT_VIP 0";
    attribute x_interface_info of pcie3_cfg_ext_read_data_valid : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext read_data_valid";
    attribute x_interface_info of pcie3_cfg_ext_read_received : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext read_received";
    attribute x_interface_info of pcie3_cfg_ext_write_received : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext write_received";
    attribute x_interface_info of pcie3_cfg_ext_function_number : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext function_number";
    attribute x_interface_info of pcie3_cfg_ext_read_data : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext read_data";
    attribute x_interface_info of pcie3_cfg_ext_register_number : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext register_number";
    attribute x_interface_info of pcie3_cfg_ext_write_byte_enable : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext write_byte_enable";
    attribute x_interface_info of pcie3_cfg_ext_write_data : signal is "xilinx.com:interface:pcie3_cfg_ext:1.0 pcie3_cfg_ext write_data";
begin
    U0: entity work.debug_bridge_0_bd_54be
        port map (
            clk => clk,
            pcie3_cfg_ext_function_number(7 downto 0) => pcie3_cfg_ext_function_number(7 downto 0),
            pcie3_cfg_ext_read_data(31 downto 0) => pcie3_cfg_ext_read_data(31 downto 0),
            pcie3_cfg_ext_read_data_valid => pcie3_cfg_ext_read_data_valid,
            pcie3_cfg_ext_read_received => pcie3_cfg_ext_read_received,
            pcie3_cfg_ext_register_number(9 downto 0) => pcie3_cfg_ext_register_number(9 downto 0),
            pcie3_cfg_ext_write_byte_enable(3 downto 0) => pcie3_cfg_ext_write_byte_enable(3 downto 0),
            pcie3_cfg_ext_write_data(31 downto 0) => pcie3_cfg_ext_write_data(31 downto 0),
            pcie3_cfg_ext_write_received => pcie3_cfg_ext_write_received
        );
end STRUCTURE;
