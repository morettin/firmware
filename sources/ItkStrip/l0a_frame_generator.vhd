--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : l0a_frame_generator.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Fri Jan 30 16:31:36 2020
-- Last update : Thu May 21 18:37:58 2020
-- Platform    : Default Part Number
-- Standard    : VHDL-1993
--------------------------------------------------------------------------------
-- Copyright (c) 2020 BNL
-------------------------------------------------------------------------------
-- Description:
-- 1. Generates L0A frames using the signals from the TTC system
-- 2. Provides frame_start_pulse synchronized to the BCR signals
--    to lcb_scheduler_encoder module for synchronizing LCB frames
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;

entity l0a_frame_generator is
    port(
        -- 40 MHz BC clock
        clk                 : in  std_logic;
        -- reset the module (doesn't affect the frame_start_pulse phase)
        rst                 : in  std_logic;
        -- input BCR signal from TTC block
        -- frame_start_pulse_o will synchronize to this pulse (even if rst='1')
        -- the number of clk cycles between bcr must be divisible by 4
        -- otherwise the period between frame_start_pulse_o will be
        -- inconsistent, and the LCB frame scheduled during the inconsistency will be corrupt
        bcr_i               : in  std_logic;
        -- input L0A signal from TTC block
        l0a_i               : in  std_logic;
        -- by how many clk cycles to delay the BCR signal before sending it to the scheduler
        -- Please note that this must be less than BCR repetition rate. Repeated BCRs separated
        -- by smaller time interval than bcr_delay_i will not propagate correctly
        bcr_delay_i         : in  unsigned(full_bcid'range);
        -- determines the phase of the L0A frame with respect to the BCR pulse
        -- if the BCR has not yet been received, the phase is random
        -- The BCR will be detected (for the purposes of setting LCB frame phase only) when rst='1'
        -- changing this value after rst is deasserted will result in corrupted LCB frames
        frame_phase_i       : in  unsigned(frame_phase'range);
        -- input L0tag from TTC block; the earliers l0ID in the frame is latched
        l0id_i              : in  std_logic_vector(6 downto 0);
        -- outputs strobe to start L0A frame; the strobe phase is determined by frame_phase_i
        frame_start_pulse_o : out std_logic;
        -- outputs formed L0A frame
        -- valid frame will have non-zero BCR or L0A mask
        -- if both BCR or L0A mask are zero, the frame is ignored by the scheduler
        l0a_frame_o         : out std_logic_vector(11 downto 0)
    );
end entity l0a_frame_generator;

architecture RTL of l0a_frame_generator is
    signal bcr_delay_counter       : unsigned(full_bcid'range)   := (others => '0');
    signal l0a_mask_reg            : std_logic_vector(2 downto 0);
    signal frame_start_pulse_reg   : std_logic;
    signal bcr_reg           : std_logic; -- should this frame have a BCR flag set this frame?
    signal bcr_received            : std_logic                   := '0'; -- was there an external BCR pulse?
    signal frame_phase_counter     : unsigned(frame_phase'range) := (others => '0');
    signal l0a_frame_reg           : std_logic_vector(11 downto 0);
    signal l0id_reg           : std_logic_vector(l0id_i'range);

    -- define BCID delays
    constant C_ZERO_DELAY : unsigned(full_bcid'range) := (others => '0');
    constant C_ONE_DELAY  : unsigned(full_bcid'range) := (0 => '1', others => '0');

    -- define LCB frame phases
    --constant C_FRAME_PHASE_0 : unsigned(frame_phase_counter'range) := "00";
    --constant C_FRAME_PHASE_1 : unsigned(frame_phase_counter'range) := "01";
    --constant C_FRAME_PHASE_2 : unsigned(frame_phase_counter'range) := "10";
    constant C_FRAME_PHASE_3 : unsigned(frame_phase_counter'range) := "11";

begin
    l0a_frame_o <= l0a_frame_reg;
    frame_start_pulse_o <= frame_start_pulse_reg;

    -- serializes l0a_i to form l0a mask in l0a_frame_o
    update_l0a_mask : process(clk) is
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                l0a_mask_reg <= (others => '0');
            else
                l0a_mask_reg(2 downto 1) <= l0a_mask_reg(1 downto 0);
                l0a_mask_reg(0) <= l0a_i;
            end if;
        end if;
    end process;


    -- delays BCR signal by a configurable number of clk cycles
    delayed_bcr_logic : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                bcr_reg           <= '0';
                bcr_received      <= '0';
                bcr_delay_counter <= C_ONE_DELAY;
            else
                if frame_phase_counter = frame_phase_i then
                    bcr_reg      <= '0';
                end if;

                if bcr_i = '1' then
                    if bcr_delay_i = C_ZERO_DELAY then
                        if frame_phase_counter /= frame_phase_i then
                            bcr_reg      <= '1';
                        end if;
                        bcr_received <= '0';
                    else
                        bcr_received      <= '1';
                        bcr_delay_counter <= C_ONE_DELAY;
                    end if;
                end if;

                if bcr_received = '1' then
                    if bcr_delay_counter = bcr_delay_i then
                        bcr_received <= '0';
                        if frame_phase_counter /= frame_phase_i then
                            bcr_reg      <= '1';
                        end if;
                    end if;
                    bcr_delay_counter <= bcr_delay_counter + 1;
                end if;
            end if;
        end if;
    end process;


    -- sets phase of LCB frame with respect to bcr_i signal
    frame_phase_logic : process(clk) is
    begin
        if rising_edge(clk) then
            if bcr_i = '1' then
                -- synchronize to the BCR signal
                frame_phase_counter <= (others => '0');
            else
                if frame_phase_counter = C_FRAME_PHASE_3 then
                    frame_phase_counter <= (others => '0');
                else
                    frame_phase_counter <= frame_phase_counter + 1;
                end if;
            end if;
        end if;
    end process;


    -- forms l0a_frame from the internal signals
    -- issues LCB frame start pulse
    -- determines when I0ID is latched
    frame_update_logic : process(clk) is
        variable L0A_mask_tmp : std_logic_vector(L0A_mask_bits'range);
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                frame_start_pulse_reg <= '0';
                l0a_frame_reg <= (others => '0');
            else
                if frame_phase_counter = frame_phase_i then
                    frame_start_pulse_reg <= '1';
                    L0A_mask_tmp(L0A_mask_tmp'left downto L0A_mask_tmp'right + 1) := l0a_mask_reg;
                    L0A_mask_tmp(L0A_mask_tmp'right) := l0a_i;
                    if bcr_i = '1' and bcr_delay_i = C_ZERO_DELAY then
                        l0a_frame_reg(BCR_bits'range) <= (others => '1');
                    elsif bcr_received = '1' and bcr_delay_counter = bcr_delay_i then
                        l0a_frame_reg(BCR_bits'range) <= (others => '1');
                    else
                        l0a_frame_reg(BCR_bits'range) <= (others => bcr_reg);
                    end if;
                    l0a_frame_reg(L0A_mask_bits'range) <= L0A_mask_tmp;
                    l0a_frame_reg(L0tag_bits'range) <= l0id_reg;
                else
                    frame_start_pulse_reg <= '0';
                end if;
            end if;
        end if;
    end process;


    l0id_latch_logic: process(clk) is
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                l0id_reg <= (others => '0');
            else
                if frame_start_pulse_reg = '1' then
                    l0id_reg <= l0id_i;
                end if;
            end if;
        end if;
    end process;

end architecture RTL;
