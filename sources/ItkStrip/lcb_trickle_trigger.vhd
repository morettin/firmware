--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;

-- Forms a trickle trigger and register guard pulses given bc_start and bc_stop
-- The pulse starts at clk cycle after bc_start
-- the pulse stops at clk cycle after bc_stop
-- if bc_start = bc_stop there will be no trickle_trigger pulse
-- BC 00 = is BC 1 clk cycle after BCR is received

entity lcb_trickle_trigger is
    -- guard window size before the end of the allowed BC range for the trickle configuration commands
    -- during this window, scheduling new register read/write commands is not allowed
    -- (completing already active register commands, elink-originated fast, IDLE and L0A commands are allowed)
    generic( guard_window : integer range 0 to 4095 := 64 );
    port(
        clk               : in  std_logic; -- 40 MHz BC clk
        rst               : in  std_logic; -- synchronous reset
        en_i              : in  std_logic; -- enable trigger generation
        bcr_i             : in  std_logic; -- BCR from TTC module
        bc_start_i        : in  t_bcid; -- BC at which the trigger pulse start
        bc_stop_i         : in  t_bcid; -- BC at which the trigger pulse stops
        trickle_trigger_o : out std_logic; -- allow trickle trigger frames when this is '1'
        trickle_allow_register_cmd_o : out std_logic -- allow dequeuing next register read/write command when this is '1'
    );
end entity lcb_trickle_trigger;

architecture RTL of lcb_trickle_trigger is
    -- trickle_counter is set to 0 when BCR is received, then counts up
    -- when trickle_counter=BC_START trickle_trigger_o and trickle_allow_register_cmd_o are set to '1'
    -- when trickle_counter=BC_STOP trickle_trigger_o is set to '0'
    signal trickle_counter    : t_bcid    := (others => '0');
    signal trickle_counter_en : std_logic := '0';

    -- register_counter starts counting up from guard_window when BCR is received
    -- when register_counter=BC_STOP (or trickle_counter=BCID_STOP) trickle_allow_register_cmd_o is set to '0'
    signal register_counter    : t_bcid    := (others => '0');
    signal register_counter_en : std_logic := '0';

    -- counter will stop when max_bcid is reached
    constant bc_max  : t_bcid := (others => '1');
--constant bc_zero : t_bcid := (others => '0');

begin
    process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' or en_i = '0' then
                trickle_counter      <= (others => '0');
                register_counter     <= (others => '0');
                trickle_trigger_o <= '0';
                trickle_counter_en   <= '0';
                register_counter_en <= '0';
                trickle_allow_register_cmd_o <= '0';
            else
                -- start of the pulse
                if (trickle_counter = bc_start_i) and (trickle_counter_en = '1') then
                    trickle_trigger_o <= '1';
                    trickle_allow_register_cmd_o <= '1';
                end if;

                -- end of the trickle pulse
                if trickle_counter = bc_stop_i then
                    trickle_trigger_o <= '0';
                    trickle_allow_register_cmd_o <= '0';
                end if;

                -- end of the read command pulse
                if register_counter = bc_stop_i then
                    trickle_allow_register_cmd_o <= '0';
                end if;

                -- count BC up for trickle pulse
                if trickle_counter_en = '1' then
                    trickle_counter <= std_logic_vector(
                                                        unsigned(trickle_counter) + to_unsigned(1, t_bcid'length));
                end if;

                -- count BC up for register pulse
                if register_counter_en = '1' then
                    register_counter <= std_logic_vector(
                                                         unsigned(register_counter) + to_unsigned(1, t_bcid'length));
                end if;

                -- next BCR pulse was never received (trickle pulse)
                if trickle_counter = bc_max then
                    trickle_counter_en <= '0';
                end if;

                -- next BCR pulse was never received (register pulse)
                if register_counter = bc_max then
                    register_counter_en <= '0';
                end if;

                -- start trigger sequence
                if bcr_i = '1' then
                    trickle_counter_en <= '1';
                    register_counter_en <= '1';
                    trickle_counter    <= (others => '0');
                    register_counter   <= std_logic_vector(to_unsigned(guard_window, t_bcid'length));
                    -- interrupt active sequence, if any
                    trickle_trigger_o <= '0';
                    trickle_allow_register_cmd_o <= '0';
                end if;
            end if;
        end if;
    end process;
end architecture RTL;
