--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
--! Company:  BNL
--! Engineer: Elena Zhivun <ezhivun@bnl.gov>
--!
-- Created     : Fri Oct 18 09:51:14 2019
-- Last update : Wed Aug 11 18:50:59 2021
--!
--! Module Name:    lcb_frame_generator
--! Project Name:   FELIX
--!
--! Generates unencoded LCB frames and places them on the low-priority LCB frame FIFO.
--! The FIFO is contained inside this module and is FWFT.
--!
--!     Acceptable commands are defined in t_lcb_command:
--!   LCB_CMD_IDLE, LCB_CMD_FAST, LCB_CMD_L0A, LCB_CMD_ABC_REG_READ,
--!    LCB_CMD_ABC_REG_WRITE, LCB_CMD_HCC_REG_READ, LCB_CMD_HCC_REG_WRITE
--!
--!    To insert a command, set the corresponding command and parameter
--!    registers, and pulse cmd_start_pulse_i for 1 clk cycle
--!
--!    LCB_CMD_IDLE - insert an idle frame (no parameters)
--!    LCB_CMD_FAST - insert fast command (fast_cmd_data_i)
--!      fast_cmd_data_i = bc & fast_command
--!    LCB_CMD_L0A - insert L0A frame (l0a_data_i)
--!      l0a_data_i = bcr & mask & tag
--!      The command will be invalid and ignored if the first 5 bits of l0a_data_i are 0
--!      R3 commands can be sent through this path (L1 commands will be ignored because of the marker)
--!      R3 frame: l0a_data_i = mask & tag
--!
--!    LCB_CMD_ABC_REG_READ, LCB_CMD_HCC_REG_READ
--!    Parameters:
--!    abc_id_i - ABC* ID (0xF = broadcast)
--!    hcc_id_i - HCC* ID (0xF = broadcast)
--!    reg_addr_i - register address to read
--!    hcc_abc_mask_i - mask for HCC* & ABC* IDs
--!
--!    LCB_CMD_ABC_REG_WRITE, LCB_CMD_HCC_REG_WRITE
--!    Parameters:
--!    abc_id_i - ABC* ID (0xF = broadcast)
--!    hcc_id_i - HCC* ID (0xF = broadcast)
--!    reg_addr_i - register address to write
--!   reg_data_i - register data to write
--!    hcc_abc_mask_i - mask for HCC* & ABC* IDs
--!
--!
--!    Timing requirements for new command insertion:
--!
--!    0. ready_o flag has 1 clk cycle latency. Because of this:
--!    1. Commands that result in a single frame (L0A, FAST, IDLE)
--!      can be inserted every clk cycle as long as lcb_frame_fifo_almost_full_o = '0'
--!     2. No command can be inserted if lcb_frame_fifo_almost_full_o = '1' and
--!      another command has been inserted on the previous clk cycle
--!    3. Commands that result in multiple frames (i.e. register read/write commands)
--!      cannot be inserted every clk cycle under any circumstances:
--!      Register read commands can be inserted every 4 clk cycles
--!      Register write commands can be inserted every 9 clk cycles
--!
--!   Output frame format:
--!
--!     lcb_frame_o(12): if '1', this frame starts with a comma (or it's an idle frame).
--!    lcb_frame_o(11 downto 6):
--!      if lcb_frame_o(12) = '1',
--!      "000000" = K2 comma, "000001" = K3 comma, "000010" - idle frame
--!      if lcb_frame_o(12) = '0', contains first half of the frame
--!         lcb_frame_o(5 downto 0): second half of the frame
--!
----------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;

Library xpm;
    use xpm.vcomponents.all;

entity lcb_frame_generator is
    port(
        clk                      : in  std_logic; -- 40 MHz BC clk
        rst                      : in  std_logic;
        cmd_i                    : in  t_lcb_command; -- which command to execute
        cmd_start_pulse_i        : in  std_logic; -- pulse for 1 clk cycle to start command
        fast_cmd_data_i          : in  std_logic_vector(5 downto 0); -- bc & cmd
        hcc_mask_i         : in  std_logic_vector(15 downto 0);
        abc_mask_i          : in  t_abc_mask; -- mask for HCC and ABC IDs
        l0a_data_i               : in  std_logic_vector(11 downto 0); -- bcr & L0A mask & L0 tag
        abc_id_i                 : in  std_logic_vector(3 downto 0); -- ABC* address (0xF = broadcast)
        hcc_id_i                 : in  std_logic_vector(3 downto 0); -- HCC* address (0xF = broadcast)
        reg_addr_i               : in  std_logic_vector(7 downto 0); -- register address for reg. read/write
        reg_data_i               : in  std_logic_vector(31 downto 0); -- register data for reg. write
        ready_o                  : out std_logic; -- indicates it's ready for a new command
        lcb_frame_o              : out STD_LOGIC_VECTOR(12 downto 0); -- FIFO data out
        lcb_frame_i_rd_en        : in  std_logic; -- FIFO rd_en signal
        lcb_frame_o_empty        : out std_logic; -- FIFO empty signal
        lcb_frame_o_almost_empty : out std_logic; -- FIFO contains one word
        lcb_frame_fifo_almost_full_o : out std_logic -- FIFO is almost full
    );
end entity lcb_frame_generator;

architecture RTL of lcb_frame_generator is

    constant COMMA      : std_logic_vector := "1";
    constant NO_COMMA   : std_logic_vector := "0";
    constant K2         : std_logic_vector := "000000";
    constant K3         : std_logic_vector := "000001";
    constant IDLE_FRAME : std_logic_vector := "1000010000000";
    constant MARKER     : std_logic_vector := "00000";

    signal cmd      : t_lcb_command;
    signal abc_id   : std_logic_vector(3 downto 0);
    signal hcc_id   : std_logic_vector(3 downto 0);
    signal reg_addr : std_logic_vector(7 downto 0);
    signal reg_data : std_logic_vector(31 downto 0);

    type t_state is (s_init, s_wait_for_command, s_form_l0a, s_wait_for_fifo,
        s_form_rw_header_1, s_form_rw_header_2, s_form_rw_stop,
        s_form_rw_data_1, s_form_rw_data_2,
        s_form_rw_data_3, s_form_rw_data_4, s_form_rw_data_5);

    signal lcb_frame_fifo_empty       : std_logic;
    signal lcb_frame_fifo_full        : std_logic;
    signal lcb_frame_fifo_almost_full : std_logic;
    signal lcb_frame_fifo_wr_rst_busy : std_logic;
    signal lcb_frame_fifo_rd_rst_busy : std_logic;
    signal lcb_frame_fifo_wr_en       : std_logic;
    signal lcb_frame_reg              : STD_LOGIC_VECTOR(12 downto 0);
    signal lcb_frame_fifo_ready       : std_logic;
    signal state, return_state        : t_state;
    signal is_masked          : boolean;
begin

    lcb_frame_fifo_almost_full_o <= lcb_frame_fifo_almost_full;

    -- is destination chip of this register r/w command disabled?
    p_chip_masked : process(cmd_i, hcc_id_i, abc_id_i, hcc_mask_i, abc_mask_i) is
    begin
        if cmd_i = LCB_CMD_ABC_REG_READ or cmd_i = LCB_CMD_ABC_REG_WRITE then
            is_masked <= abc_mask_i(to_integer(unsigned(hcc_id_i)))(to_integer(unsigned(abc_id_i))) = '1'
                         or hcc_mask_i(to_integer(unsigned(hcc_id_i))) = '1';
        elsif cmd_i = LCB_CMD_HCC_REG_READ or cmd_i = LCB_CMD_HCC_REG_WRITE then
            is_masked <= hcc_mask_i(to_integer(unsigned(hcc_id_i))) = '1';
        else
            is_masked <= false;
        end if;
    end process;

    state_update_logic : process(clk) is
        procedure next_if_fifo_ready(
            next_state : t_state
        ) is
        begin
            if lcb_frame_fifo_ready = '1' then
                state <= next_state;
            else
                return_state <= next_state;
                state        <= s_wait_for_fifo;
            end if;
        end;

    begin
        if rising_edge(clk) then
            if rst = '1' then
                state        <= s_init;
                return_state <= s_init;
            else
                case state is
                    when s_init =>
                        state        <= s_wait_for_command;
                        return_state <= s_init;

                    when s_wait_for_command =>
                        if cmd_start_pulse_i = '1' then
                            case cmd_i is
                                when LCB_CMD_L0A =>
                                    if l0a_data_i(11 downto 7) = MARKER then
                                        state <= s_wait_for_command; -- Ignore invalid command
                                    else
                                        next_if_fifo_ready(s_wait_for_command);
                                    end if;

                                when LCB_CMD_IDLE | LCB_CMD_FAST =>
                                    next_if_fifo_ready(s_wait_for_command);

                                when LCB_CMD_ABC_REG_READ | LCB_CMD_ABC_REG_WRITE
								| LCB_CMD_HCC_REG_READ | LCB_CMD_HCC_REG_WRITE =>
                                    if is_masked then
                                        -- ignore register command if the chip is masked
                                        state <= s_wait_for_command;
                                    else
                                        -- issue register command if the chip is not masked
                                        next_if_fifo_ready(s_form_rw_header_1);
                                    end if;

                                when others =>
                                    report "Invalid LCB command" severity failure;
                            end case;
                        end if;

                    when s_wait_for_fifo => -- @suppress "Dead state 's_wait_for_fifo': state does not have outgoing transitions"
                        if lcb_frame_fifo_ready = '1' then
                            state <= return_state;
                        else
                            state <= s_wait_for_fifo;
                        end if;

                    when s_form_l0a         => next_if_fifo_ready(s_wait_for_command);
                    when s_form_rw_header_1 => next_if_fifo_ready(s_form_rw_header_2);

                    when s_form_rw_header_2 =>
                        if cmd = LCB_CMD_ABC_REG_READ or cmd = LCB_CMD_HCC_REG_READ then
                            next_if_fifo_ready(s_form_rw_stop);
                        else
                            next_if_fifo_ready(s_form_rw_data_1);
                        end if;

                    when s_form_rw_data_1 => next_if_fifo_ready(s_form_rw_data_2);
                    when s_form_rw_data_2 => next_if_fifo_ready(s_form_rw_data_3);
                    when s_form_rw_data_3 => next_if_fifo_ready(s_form_rw_data_4);
                    when s_form_rw_data_4 => next_if_fifo_ready(s_form_rw_data_5);
                    when s_form_rw_data_5 => next_if_fifo_ready(s_form_rw_stop);
                    when s_form_rw_stop   => next_if_fifo_ready(s_wait_for_command);

                    when others => report "Invalid FSM state of LCB frame generator" severity failure;
                end case;
            end if;
        end if;
    end process;

    output_update_logic : process(clk) is
        variable reg_rw, abc_hcc : std_logic;
    begin
        if rising_edge(clk) then
            case (state) is
                when s_init =>
                    lcb_frame_reg        <= (others => '0');
                    lcb_frame_fifo_wr_en <= '0';
                    cmd                  <= LCB_CMD_IDLE;
                    abc_id               <= (others => '0');
                    hcc_id               <= (others => '0');
                    reg_addr             <= (others => '0');
                    reg_data             <= (others => '0');
                    ready_o              <= '0';

                when s_wait_for_command => -- wait for next command and form single-frame commands
                    lcb_frame_fifo_wr_en <= '0';
                    ready_o              <= '1';

                    if cmd_start_pulse_i = '1' then
                        case cmd_i is
                            when LCB_CMD_IDLE =>
                                lcb_frame_reg        <= IDLE_FRAME;
                                ready_o              <= lcb_frame_fifo_ready;
                                lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;
                            when LCB_CMD_FAST =>
                                lcb_frame_reg        <= COMMA & K3 & fast_cmd_data_i;
                                ready_o              <= lcb_frame_fifo_ready;
                                lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;
                            when LCB_CMD_L0A =>
                                if l0a_data_i(11 downto 7) = MARKER then
                                    ready_o              <= '1'; -- Ignore invalid command
                                    lcb_frame_fifo_wr_en <= '0';
                                else
                                    lcb_frame_reg        <= NO_COMMA & l0a_data_i;
                                    ready_o              <= lcb_frame_fifo_ready;
                                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;
                                end if;
                            when LCB_CMD_ABC_REG_READ | LCB_CMD_ABC_REG_WRITE
							| LCB_CMD_HCC_REG_READ | LCB_CMD_HCC_REG_WRITE =>
                                if is_masked then
                                    ready_o              <= '1';
                                    lcb_frame_fifo_wr_en <= '0';
                                else
                                    if cmd_i = LCB_CMD_ABC_REG_READ or cmd_i = LCB_CMD_ABC_REG_WRITE then
                                        abc_hcc := '1';
                                    else
                                        abc_hcc := '0';
                                    end if;
                                    lcb_frame_reg        <= COMMA & K2 & abc_hcc & "1" & hcc_id_i;
                                    ready_o              <= '0';
                                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;
                                    cmd                  <= cmd_i;
                                    abc_id               <= abc_id_i;
                                    hcc_id               <= hcc_id_i;
                                    reg_addr             <= reg_addr_i;
                                    reg_data             <= reg_data_i;
                                end if;

                            when others =>
                                report "Invalid LCB command" severity failure;
                        end case;

                    end if;

                when s_wait_for_fifo => -- wait for FIFO, write buffered data when ready
                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;
                    if lcb_frame_fifo_ready = '1' and return_state = s_wait_for_command then
                        ready_o <= '1';
                    else
                        ready_o <= '0';
                    end if;

                when s_form_rw_header_1 => -- form Register command header frame #1
                    if cmd = LCB_CMD_ABC_REG_READ or cmd = LCB_CMD_HCC_REG_READ then
                        reg_rw := '1';
                    else
                        reg_rw := '0';
                    end if;
                    lcb_frame_reg        <= NO_COMMA & MARKER & reg_rw & abc_id & reg_addr(7 downto 6);
                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;

                when s_form_rw_header_2 => -- form Register command header frame #2
                    lcb_frame_reg        <= NO_COMMA & MARKER & reg_addr(5 downto 0) & "0";
                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;

                when s_form_rw_data_1 => -- form Register data frame #1
                    lcb_frame_reg        <= NO_COMMA & MARKER & "000" & reg_data(31 downto 28);
                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;

                when s_form_rw_data_2 => -- form Register data frame #2
                    lcb_frame_reg        <= NO_COMMA & MARKER & reg_data(27 downto 21);
                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;

                when s_form_rw_data_3 => -- form Register data frame #3
                    lcb_frame_reg        <= NO_COMMA & MARKER & reg_data(20 downto 14);
                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;

                when s_form_rw_data_4 => -- form Register data frame #4
                    lcb_frame_reg        <= NO_COMMA & MARKER & reg_data(13 downto 7);
                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;

                when s_form_rw_data_5 => -- form Register data frame #5
                    lcb_frame_reg        <= NO_COMMA & MARKER & reg_data(6 downto 0);
                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;

                when s_form_rw_stop =>  -- form register r/w Stop frame
                    lcb_frame_reg        <= COMMA & K2 & abc_hcc & "0" & hcc_id;
                    lcb_frame_fifo_wr_en <= lcb_frame_fifo_ready;
                    ready_o              <= lcb_frame_fifo_ready;

                when others => report "Invalid FSM state of LCB frame generator" severity failure;
            end case;
        end if;
    end process;

    lcb_frame_o_empty    <= lcb_frame_fifo_empty or lcb_frame_fifo_rd_rst_busy;
    lcb_frame_fifo_ready <= not (lcb_frame_fifo_wr_rst_busy or lcb_frame_fifo_almost_full);


    --pragma synthesis_off
    -- These things should never happen:
    sanity_check : process(clk)
    begin
        if rising_edge(clk) then
            if lcb_frame_i_rd_en = '1' then
                assert (lcb_frame_fifo_empty = '0' and lcb_frame_fifo_rd_rst_busy = '0')
                    report "LCB frame - invalid FIFO read operation" severity FAILURE;
            end if;

            if lcb_frame_fifo_wr_en = '1' then
                assert (lcb_frame_fifo_full = '0' and lcb_frame_fifo_wr_rst_busy = '0')
                    report "LCB frame - invalid FIFO write operation" severity FAILURE;
            end if;
        end if;
    end process;
    --pragma synthesis_on


    -- FIFO containing unencoded frames
    --lcb_frame_fifo : entity work.hcc_13b_fwft_fifo
    --  port map(
    --    clk          => clk,
    --    srst         => rst,
    --    din          => lcb_frame_reg,
    --    wr_en        => lcb_frame_fifo_wr_en,
    --    rd_en        => lcb_frame_i_rd_en,
    --    dout         => lcb_frame_o,
    --    full         => lcb_frame_fifo_full,
    --    almost_full  => lcb_frame_fifo_almost_full,
    --    almost_empty => lcb_frame_o_almost_empty,
    --    empty        => lcb_frame_fifo_empty
    --    wr_rst_busy  => lcb_frame_fifo_wr_rst_busy,
    --    rd_rst_busy  => lcb_frame_fifo_rd_rst_busy
    --);

    xpm_fifo_sync_inst : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            DOUT_RESET_VALUE => "0",    -- String
            ECC_MODE => "no_ecc",       -- String
            FIFO_MEMORY_TYPE => "distributed", -- String
            FIFO_READ_LATENCY => 0,     -- DECIMAL
            FIFO_WRITE_DEPTH => 64,   -- DECIMAL
            FULL_RESET_VALUE => 0,      -- DECIMAL
            PROG_EMPTY_THRESH => 4,    -- DECIMAL
            PROG_FULL_THRESH => 63,     -- DECIMAL
            RD_DATA_COUNT_WIDTH => 7,   -- DECIMAL = log2(FIFO_READ_DEPTH)+1.
            -- FIFO_READ_DEPTH =  FIFO_WRITE_DEPTH*WRITE_DATA_WIDTH / READ_DATA_WIDTH
            READ_DATA_WIDTH => 13,      -- DECIMAL
            READ_MODE => "fwft",         -- String
            SIM_ASSERT_CHK => 0,        -- DECIMAL; 0=disable simulation messages, 1=enable simulation messages
            USE_ADV_FEATURES => "0808", -- String
            WAKEUP_TIME => 0,           -- DECIMAL
            WRITE_DATA_WIDTH => 13,     -- DECIMAL
            WR_DATA_COUNT_WIDTH => 7    -- DECIMAL = log2(FIFO_WRITE_DEPTH)+1
        )
        port map ( -- @suppress "The order of the associations is different from the declaration order"
            almost_empty => lcb_frame_o_almost_empty,   -- 1-bit output: Almost Empty : When asserted, this signal indicates that
            -- only one more read can be performed before the FIFO goes to empty.

            almost_full => lcb_frame_fifo_almost_full,     -- 1-bit output: Almost Full: When asserted, this signal indicates that
            -- only one more write can be performed before the FIFO is full.

            data_valid => open,       -- 1-bit output: Read Data Valid: When asserted, this signal indicates
            -- that valid data is available on the output bus (dout).

            dbiterr => open,             -- 1-bit output: Double Bit Error: Indicates that the ECC decoder
            -- detected a double-bit error and data in the FIFO core is corrupted.

            dout => lcb_frame_o,                   -- READ_DATA_WIDTH-bit output: Read Data: The output data bus is driven
            -- when reading the FIFO.

            empty => lcb_frame_fifo_empty,                 -- 1-bit output: Empty Flag: When asserted, this signal indicates that
            -- the FIFO is empty. Read requests are ignored when the FIFO is empty,
            -- initiating a read while empty is not destructive to the FIFO.

            full => lcb_frame_fifo_full,                   -- 1-bit output: Full Flag: When asserted, this signal indicates that the
            -- FIFO is full. Write requests are ignored when the FIFO is full,
            -- initiating a write when the FIFO is full is not destructive to the
            -- contents of the FIFO.

            overflow => open,           -- 1-bit output: Overflow: This signal indicates that a write request
            -- (wren) during the prior clock cycle was rejected, because the FIFO is
            -- full. Overflowing the FIFO is not destructive to the contents of the
            -- FIFO.

            prog_empty => open,       -- 1-bit output: Programmable Empty: This signal is asserted when the
            -- number of words in the FIFO is less than or equal to the programmable
            -- empty threshold value. It is de-asserted when the number of words in
            -- the FIFO exceeds the programmable empty threshold value.

            prog_full => open,         -- 1-bit output: Programmable Full: This signal is asserted when the
            -- number of words in the FIFO is greater than or equal to the
            -- programmable full threshold value. It is de-asserted when the number
            -- of words in the FIFO is less than the programmable full threshold
            -- value.

            rd_data_count => open, -- RD_DATA_COUNT_WIDTH-bit output: Read Data Count: This bus indicates
            -- the number of words read from the FIFO.

            rd_rst_busy => lcb_frame_fifo_rd_rst_busy,     -- 1-bit output: Read Reset Busy: Active-High indicator that the FIFO
            -- read domain is currently in a reset state.

            sbiterr => open,             -- 1-bit output: Single Bit Error: Indicates that the ECC decoder
            -- detected and fixed a single-bit error.

            underflow => open,         -- 1-bit output: Underflow: Indicates that the read request (rd_en)
            -- during the previous clock cycle was rejected because the FIFO is
            -- empty. Under flowing the FIFO is not destructive to the FIFO.

            wr_ack => open,               -- 1-bit output: Write Acknowledge: This signal indicates that a write
            -- request (wr_en) during the prior clock cycle is succeeded.

            wr_data_count => open, -- WR_DATA_COUNT_WIDTH-bit output: Write Data Count: This bus indicates
            -- the number of words written into the FIFO.

            wr_rst_busy => lcb_frame_fifo_wr_rst_busy,     -- 1-bit output: Write Reset Busy: Active-High indicator that the FIFO
            -- write domain is currently in a reset state.

            din => lcb_frame_reg,                     -- WRITE_DATA_WIDTH-bit input: Write Data: The input data bus used when
            -- writing the FIFO.

            injectdbiterr => '0', -- 1-bit input: Double Bit Error Injection: Injects a double bit error if
            -- the ECC feature is used on block RAMs or UltraRAM macros.

            injectsbiterr => '0', -- 1-bit input: Single Bit Error Injection: Injects a single bit error if
            -- the ECC feature is used on block RAMs or UltraRAM macros.

            rd_en => lcb_frame_i_rd_en,                 -- 1-bit input: Read Enable: If the FIFO is not empty, asserting this
            -- signal causes data (on dout) to be read from the FIFO. Must be held
            -- active-low when rd_rst_busy is active high.

            rst => rst,                     -- 1-bit input: Reset: Must be synchronous to wr_clk. The clock(s) can be
            -- unstable at the time of applying reset, but reset must be released
            -- only after the clock(s) is/are stable.

            sleep => '0',                 -- 1-bit input: Dynamic power saving- If sleep is High, the memory/fifo
            -- block is in power saving mode.

            wr_clk => clk,               -- 1-bit input: Write clock: Used for write operation. wr_clk must be a
            -- free running clock.

            wr_en => lcb_frame_fifo_wr_en                  -- 1-bit input: Write Enable: If the FIFO is not full, asserting this
        -- signal causes data (on din) to be written to the FIFO Must be held
        -- active-low when rst or wr_rst_busy or rd_rst_busy is active high

        );


end architecture RTL;
