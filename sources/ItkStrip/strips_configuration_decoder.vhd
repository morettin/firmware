--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Elena Zhivun
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

--------------------------------------------------------------------------------
-- Title       : ITk Strips package
-- Project     : FELIX
--------------------------------------------------------------------------------
-- File        : strips_configuration_decoder.vhd
-- Author      : Elena Zhivun <ezhivun@bnl.gov>
-- Company     : BNL
-- Created     : Thu May 21 18:39:02 2020
-- Last update : Wed Nov 16 12:51:13 2022
-- Platform    : Default Part Number
-- Standard    : < VHDL-1993 >
--------------------------------------------------------------------------------
-- Copyright (c) 2020 User Company Name
-------------------------------------------------------------------------------
-- Description: Decodes commands from host and sends them to
-- LCB frame generator.
--
-- In the past it used to control trickle configuration memory
-- and local register map, but now these functions are obsolete
--------------------------------------------------------------------------------
-- Revisions:  Revisions and documentation are controlled by
-- the revision control system (RCS).  The RCS should be consulted
-- on revision history.
-------------------------------------------------------------------------------



library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.strips_package.all;

entity strips_configuration_decoder is
    generic (
        -- incomplete commands will be dropped if the complete
        -- arguments are not received before the timeout (4000 BC = 100 ms)
        TIMEOUT : integer := 4000
    );
    port(
        clk                          : in  std_logic; -- 40 MHz BC clk
        rst                          : in  std_logic;
        -- elink command source
        config_data_i                 : in  std_logic_vector(7 downto 0); -- elink data
        config_valid_i                : in  std_logic; -- elink data is valid this clk cycle
        config_ready_o                : out std_logic; -- this module is ready for the next elink data

        -- register map control interface
        regmap_wr_en_o               : out std_logic; -- pulse this field to update
        regmap_data_o                : out std_logic_vector(15 downto 0); -- new register value
        regmap_addr_o                : out std_logic_vector(7 downto 0) -- register address
    );
end entity strips_configuration_decoder;

architecture RTL of strips_configuration_decoder is
    type t_state is (s_init, s_wait_for_cmd, s_read_opcode,
        s_wait_for_args, s_read_args);
    signal state                  : t_state                                      := s_init;
    constant max_parameter_length : positive                                     := 6;
    signal args_counter           : integer range -1 to max_parameter_length;
    type t_args_buffer is array (natural range <>) of std_logic_vector(7 downto 0);
    signal args_buffer            : t_args_buffer(0 to max_parameter_length - 1) := (others => (others => '0'));
    signal opcode                 : t_itk_command;
    signal ready_o       : std_logic;
    signal timeout_reg    : unsigned(15 downto 0);

begin
    regmap_data_o   <= args_buffer(2) & args_buffer(1);
    regmap_addr_o   <= args_buffer(0);
    config_ready_o  <= ready_o;

    state_update_logic : process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                state <= s_init;
            else
                case state is
                    when s_init =>
                        state <= s_wait_for_cmd;

                    when s_wait_for_cmd =>
                        if config_valid_i = '1' then
                            state <= s_read_opcode;
                        else
                            state <= s_wait_for_cmd;
                        end if;

                    when s_read_opcode =>
                        case t_itk_command'(config_data_i) is
                            when ITK_CMD_REGMAP_WRITE =>
                                if config_valid_i = '1' then
                                    state <= s_read_args;
                                else
                                    state <= s_wait_for_args;
                                end if;
                            when ITK_CMD_NOOP =>
                                state <= s_wait_for_cmd;
                            when others =>
                                state <= s_init;
                        -- synthesis translate_off
                        -- report "Unknown ITk command" severity FAILURE;
                        -- synthesis translate_on
                        end case;

                    when s_read_args =>
                        if timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                            state <= s_wait_for_cmd;
                        else
                            if args_counter = 0 then
                                state <= s_wait_for_cmd;
                            else
                                state <= s_read_args;
                            end if;
                        end if;

                    when others =>
                        state <= s_init;
                        -- synthesis translate_off
                        report "Unknown ITk configuration decoder state" severity FAILURE;
                -- synthesis translate_on
                end case;
            end if;
        end if;
    end process;

    output_update_logic : process(clk) is
    begin
        if rising_edge(clk) then

            if rst = '1' then
                ready_o  <= '1';
                regmap_wr_en_o  <= '0';
                opcode       <= ITK_CMD_NOOP;
            else
                case state is
                    when s_init =>
                        ready_o  <= '0';
                        regmap_wr_en_o  <= '0';
                        opcode       <= ITK_CMD_NOOP;

                    when s_wait_for_cmd =>
                        ready_o  <= '0';
                        regmap_wr_en_o  <= '0';
                        if config_valid_i = '1' then
                            timeout_reg <= (others => '0');
                            ready_o     <= '1';
                            opcode      <= t_itk_command'(config_data_i);
                        end if;

                    when s_read_opcode =>
                        ready_o  <= '0';
                        regmap_wr_en_o  <= '0';
                        timeout_reg <= (others => '0');
                        case opcode is
                            when ITK_CMD_REGMAP_WRITE =>
                                args_counter <= 3; -- addr, data x 2
                            when ITK_CMD_NOOP =>
                                null;
                            when others =>
                        -- synthesis translate_off
                        -- report "Unknown ITk command (configuration decoder)" severity FAILURE;
                        -- synthesis translate_on
                        end case;

                    when s_read_args =>
                        regmap_wr_en_o   <= '0';
                        ready_o   <= '0';
                        timeout_reg <= timeout_reg + to_unsigned(1, timeout_reg'length);

                        if timeout_reg = to_unsigned(TIMEOUT, timeout_reg'length) then
                            ready_o   <= '0';
                        else
                            ready_o   <= config_valid_i;
                            if config_valid_i = '1' and ready_o = '1' then
                                args_buffer(0)                     <= config_data_i;
                                args_buffer(1 to args_buffer'high) <= args_buffer(0 to args_buffer'high - 1);
                                args_counter                       <= args_counter - 1;
                                if args_counter = 1 then
                                    ready_o <= '0';
                                end if;
                            end if;
                            if args_counter = 0 then
                                ready_o <= '0';
                                case opcode is
                                    -- the commands w/o arguments shouldn't be here
                                    when ITK_CMD_REGMAP_WRITE =>
                                        regmap_wr_en_o <= '1';
                                    when others =>
                                        -- synthesis translate_off
                                        report "Unknown ITk configuration (after reading arguments)" severity FAILURE;
                                -- synthesis translate_on
                                end case;
                            end if;
                        end if;

                    when others =>
                        -- synthesis translate_off
                        report "Unknown ITK configuration decoder state" severity FAILURE;
                -- synthesis translate_on
                end case;
            end if;

        end if;
    end process;
end architecture RTL;
