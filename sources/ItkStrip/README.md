# ITk Strip Package

This folder contains source files for ITk Strips modules, which support for ITk Strip HCC* and ABC* chips.

# Using the Strips links

The detailed description and user manual for the Strips modules can be found here  in sections titled "ITk Strips LCB encoder" and "ITk Strips R3L1 encoder". You need to be in `atlas-tdaq-felix-users` egroup to access the file.

# Source files and their contents

## LCB encoder
  * lcb_axi_encoder.vhd - LCB encoder with AXI-stream interface
  * lcb_wrapper.vhd - wraps LCB link functionality to be used inside a phase1- or phase2-type elink
  * l0a_frame_generator.vhd - converts TTC block signals into L0A frames; controls LCB frame phase  
  * lcb_command_decoder.vhd - mini-sequencer for interpreting higer-level LCB link commands
  * lcb_frame_generator.vhd - generates LCB command sequences
  * lcb_regmap.vhd - local configuration storage implementation for LCB module
  * lcb_regmap_package.vhd - local configuration storage mapping and data types for LCB module
  * lcb_scheduler_encoder.vhd - combines the LCB frames from all data sources according to their priority, and encodes them in 6b8b as needed
  * lcb_trickle_trigger.vhd - generates the "enable" signals for low-priority commands during trickle configuration
  * playback_controller.vhd - trickle configuration memory storage and retrieval

## R3L1 encoder  
  * r3l1_axi_encoder.vhd - R3L1 encoder with AXI-stream interface
  * r3l1_wrapper.vhd - wraps R3L1 link functionality to be used inside a phase1- or phase2-type elink
  * r3l1_frame_generator.vhd - generates R3 or L1 frames in response to TTC signals
  * r3l1_frame_synchronizer.vhd - controls phase of R3L1 frame
  * r3l1_scheduler_encoder.vhd - combines the R3L1 frames from all data sources according to their priority, and encodes them in 6b8b as needed
  * r3l1_regmap.vhd - local configuration storage implementation for R3L1 module
  * r3l1_regmap_package.vhd - local configuration storage mapping and data types for R3L1 module

## Shared
  * strips_bypass_frame_aggregator.vhd - combines two byte sequences from host into LCB or R3L1 frames
  * strips_configuration_decoder.vhd - mini-sequencer for interpreting local configuration update commands
  * strips_package.vhd - shared data types and definitions

# UVVM testbenches

  All test files are located in ../../simulation/ItkStrip, and are executed by ../../simulation/UVVMtests/ci-strips.do

# Software

  * Basic integration test scripts using low-level FELIX tools is available here: https://gitlab.cern.ch/ezhivun/hcc-star-python-readout
  * fstrips - low-level FELIX tool for updating local configuration of LCB and R3L1 encoders

