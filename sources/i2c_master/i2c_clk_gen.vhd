--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Kai
-- 
-- Create Date: 03/11/2014 12:09:03 AM
-- Design Name: 
-- Module Name: i2c_clk_gen - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
--  I2C CLOCK GENERATION
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


LIBRARY ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity i2c_clk_gen is
  Port (
    clk : in std_logic;
    i2c_clk : out std_logic   
    );
end i2c_clk_gen;

architecture Behavioral of i2c_clk_gen is


  CONSTANT divider  :  INTEGER := 400;
--FREQ=156M/1600
  signal data_clk : std_logic;

begin

  process(clk)
    variable count : integer range 0 to divider*4-1; 
  begin
    if clk'event and clk = '1' then
      if count = divider*4-1 then      
        count := 0;                     
      else        
        count := count + 1;             
      end if;
      case count is
        when 0 to divider*2-1 =>           
          data_clk <= '0';               
        when others =>                           
          data_clk <= '1';
      end case;
    end if;
  end process;
  
     bufg_i2c : BUFG
     port map
      (O   => i2c_clk,
       I   => data_clk);
  	 
 -- i2c_clk <=data_clk;

end Behavioral;
