--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer:   Kai Chen
-- 
-- Create Date:    21:01:22 12/05/2014 
-- Design Name: 
-- Module Name:    clkcfg - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description:  HTG-710 CLOCK CHIPS configuration
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- Modified @ 2015/01/15, to add configuration of U36. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity clkcfg is
  PORT(
    clk156m : in std_logic;
    i2c_rst : in std_logic;
    clkfreq_sel : in std_logic; --'0': 240MHz, '1': 120MHz
    ack_error : out std_logic; 
    config_trig : in std_logic;
    SDA : inout std_logic;
    SCL : out std_logic 
    );   
end clkcfg;

architecture Behavioral of clkcfg is

  signal rd, wr, rd_r, wr_r, rd_s, wr_s: std_logic;
  signal sda_i_debug, sda_o_debug : std_logic;

  signal wr_mod : std_logic := '1';
  signal clk100k: std_logic;
  signal step: std_logic_vector(1 downto 0):="00";
  signal i2c_op, configclk_start_r, configclk_start_2r, configclk_start : std_logic:='0';
  signal configclk_start_s: std_logic:='0';
  signal i2c_op_done, i2c_op_done_r: std_logic;
  signal device_addr7b : std_logic_vector(6 downto 0);
  signal state_display : std_logic_vector(4 downto 0);
  signal data8b_in, addr8b_in, data8b_out:std_logic_vector(7 downto 0);

  TYPE machine IS(ready, clock_05,  clock_06, clock_07, clock_04, clock_03, 
clock_02, clock_01, mux_03_wait, mux_02_wait, mux_01_wait); --needed states
  SIGNAL  state     :  machine:=ready;                          --state machine


begin

  configclk_start <= config_trig;

  i2cclkgen: entity work.i2c_clk_gen 
    port map(           
      clk        => clk156m,
      i2c_clk   => clk100k
      ); 
  
  idt_cfg: entity work.i2c_master
    port map(
      data_clk => clk100k,
      wr_mod => wr_mod, --'1' WR, '0' RD
      reset_n => not i2c_rst,                   --active low reset
      ena   => i2c_op,                    --start a operation
      i2c_process_finished => i2c_op_done,
    
    --debug
      sda_i_debug => sda_i_debug,
      sda_o_debug => sda_o_debug,
      state_display => state_display, 
      command_cnt_o   => open,
                
      wr_data_update   => open, 
      rd_data_refresh => open,
      rd_number_in => "0000",
      wr_number_in => "0000",
    
      addr      => device_addr7b, 
      data_wr   => data8b_in, 
      addr_wr   => addr8b_in, 
      busy      => open,                  
      data_rd   => data8b_out, 
    
      ack_error_o   => ack_error,
      sda         => SDA,                    
      scl         => SCL  
  ); 

        
  process(clk100k)
  begin
    if clk100k'event and clk100k='1' then
      configclk_start_r <= configclk_start;
      configclk_start_2r <= configclk_start_r;
      configclk_start_s <= configclk_start_r and (not configclk_start_2r);
      case state is
        when ready =>
          step<="00";
          if configclk_start_s='1' then
            state <= mux_01_wait;
            device_addr7b <="1110000"; -- I2C SWITCH
            addr8b_in <= "00000100"; -- REGISTER VALUE
            data8b_in <= "00000100"; --NOT USED
            wr <='1';
          else
            state <= ready;
            wr <='0';
          end if;
        when mux_01_wait =>
          step<="00";
          if i2c_op = '0' then
            wr <='1';
            state <= clock_01;
            device_addr7b <="1101110";  -- clock chips
            addr8b_in <= "00010010";
            data8b_in <= "10101000";
          else
            wr <='0';
            state <= mux_01_wait;
          end if;
        when clock_01 =>
                
          if i2c_op = '0' then
            state <= clock_02;
            wr <='1';
            device_addr7b <="1101110";
            addr8b_in <= "00000000";
            data8b_in <= "00101010";
          else
            wr <='0';
            state <= clock_01;
          end if;
        when clock_02 =>
                
          if i2c_op = '0' then
            state <= clock_03;
            wr <='1';
            device_addr7b <="1101110";
            addr8b_in <= "00000100";
            data8b_in <= "00000000";
          else
            wr <='0';
            state <= clock_02;
          end if;        
        when clock_03 =>
                
          if i2c_op = '0' then
            state <= clock_04;
            wr <='1';
            device_addr7b <="1101110";
            addr8b_in <= "00001000";
            data8b_in <= "00010001";
          else
            wr <='0';
            state <= clock_03;
          end if;
        when clock_04 =>
                
          if i2c_op = '0' then
            state <= clock_05;
            wr <='1';
            device_addr7b <="1101110";
            addr8b_in <= "00001100";
            if step(1) = '0' then
              if clkfreq_sel='0' then
                 data8b_in <= "00001010";  -- 240MHz (NOT 240.472 MHz)
                 ----to get 240.472, the value of some reg should be recalculated)
               else
                 data8b_in <= "00010100";  -- 120MHz (NOT 120.236 MHz)
               end if;
             else
                 data8b_in <= "00001100";  -- 200MHz (NOT 200.395 MHz)
             end if;
          else
            wr <='0';
            state <= clock_04;
          end if;
        when clock_05 =>
                
          if i2c_op = '0' then
            state <= clock_06;
            wr <='1';
            device_addr7b <="1101110";
            addr8b_in <= "00010100";
            data8b_in <= "00011111";
          else
            wr <='0';
            state <= clock_05;
          end if;
        when clock_06 =>
                
          if i2c_op = '0' then
            state <= clock_07;
            wr <='1';
            device_addr7b <="1101110";
            addr8b_in <= "00010010";
            data8b_in <= "10100000";
          else
            wr <='0';
            state <= clock_06;
          end if;
        when clock_07 =>
                
          if i2c_op = '0' then
            if step="00" then
              state <= mux_02_wait;
              wr <='1';
              device_addr7b <="1110000";  -- I2C SWITCH
              addr8b_in <= "00001000"; -- REGISTER VALUE
              data8b_in <= "00001000"; --NOT USED
            elsif step="01" then
              state <= mux_03_wait;
              wr <='1';
              device_addr7b <="1110000";  -- I2C SWITCH
              addr8b_in <= "00000001"; -- REGISTER VALUE
              data8b_in <= "00000001"; --NOT USED
              
            else
              state <= ready;
              wr <='0';
            end if;
          else
            wr <='0';
            state <= clock_07;
          end if;
  
        when mux_02_wait =>
          step<="01";
          if i2c_op = '0' then
            wr <='1';
            state <= clock_01;
            device_addr7b <="1101110";
            addr8b_in <= "00010010";
            data8b_in <= "10101000";
          else
            wr <='0';
            state <= mux_02_wait;
          end if;

        when mux_03_wait =>
          step<="10";
          if i2c_op = '0' then
            wr <='1';
            state <= clock_01;
            device_addr7b <="1101110";
            addr8b_in <= "00010010";
            data8b_in <= "10101000";
          else
            wr <='0';
            state <= mux_03_wait;
          end if; 

       when others => 
          state <= ready;
          wr <='0';

      end case;

    end if;
  end process;


  process (clk156m)
  begin
    if clk156m'event and clk156m='1' then
      rd_r <= rd;
      wr_r <= wr;
      i2c_op_done_r <= i2c_op_done;
      rd_s <= rd and (not rd_r);
      wr_s <= wr and (not wr_r);
      if rd_s = '1' then
        wr_mod <= '0';
        i2c_op <= '1';
      elsif wr_s = '1' then
        wr_mod <= '1';
        i2c_op <='1';
      elsif i2c_op_done_r='1' and i2c_op_done='0' then
        i2c_op <='0';
        wr_mod <= wr_mod;
      end if;  

    end if;
  end process;

end Behavioral;

