--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               RHabraken
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


--!------------------------------------------------------------------------------
--!                                                             
--!           NIKHEF - National Institute for Subatomic Physics 
--!
--!                       Electronics Department                
--!                                                             
--!-----------------------------------------------------------------------------
--! @class application
--! 
--!
--! @author      Andrea Borga    (andrea.borga@nikhef.nl)<br>
--!              Frans Schreuder (frans.schreuder@nikhef.nl)<br>
--!              Oussama el Kharraz Alami<br>
--!
--!
--! @date        05/10/2015    created
--!
--! @version     1.0
--!
--! @brief 
--! This example application fills the downfifo (PCIe -> PC) with pseudo random data by using
--! a LFSR and multiplies the data from upfifo (PC -> PCIe) back to the downfifo. The size
--! of the randomdata is 256 bits. The DMA core will take care of the data and writes it into PC memory
--! according to the DMA descriptors.
--! 
--! @detail
--! We are discarding any DMA data sent by the PC, otherwise a second fifo could be connected to these ports: <br>
--! fifo_din <br>
--! fifo_we <br>
--! fifo_full <br>
--!
--!-----------------------------------------------------------------------------
--! @TODO
--!  
--!
--! ------------------------------------------------------------------------------

--! @brief ieee 



library ieee, UNISIM;
use ieee.numeric_std.all;
use UNISIM.VCOMPONENTS.all;
use ieee.std_logic_unsigned.all;-- @suppress "Deprecated package"
use ieee.std_logic_1164.all;
use work.pcie_package.all;

entity application is
  port (
    clk250               : in     std_logic;
    flush_fifo           : in     std_logic;
    --fromHostFifo_dout    : in     std_logic_vector(511 downto 0);
    --fromHostFifo_empty   : in     std_logic;
    fromHostFifo_rd_clk  : out    std_logic;
    --fromHostFifo_rd_en   : out    std_logic;
    --fromHostFifo_rst     : out    std_logic;
    register_map_control : in     register_map_control_type; --! contains all read/write registers that control the application. The record members are described in pcie_package.vhd
    reset_hard           : in     std_logic;
    --reset_soft           : in     std_logic;
    toHostFifo_din       : out    std_logic_vector(511 downto 0);
    toHostFifo_prog_full : in     std_logic;
    toHostFifo_rst       : out    std_logic;
    toHostFifo_wr_clk    : out    std_logic;
    toHostFifo_wr_en     : out    std_logic;
    leds                 : out    std_logic_vector(7 downto 0));
end entity application;

architecture rtl of application is

  signal cnt: std_logic_vector(31 downto 0);
begin

    
    fromHostFifo_rd_clk <= clk250;
    toHostFifo_wr_clk   <= clk250;
    
    

  leds <= register_map_control.STATUS_LEDS(7 downto 0);
  
  
  
  --! Write the fifo if it's not full
  toHostFifo_rst <= flush_fifo or reset_hard;
  toHostFifo_wr_en <= not toHostFifo_prog_full;
  --! Add some constants and mix it with the counter value.
  toHostFifo_din <= x"DEADBEEF"&cnt&x"00001111"&cnt&x"22223333"&cnt&x"44445555"&cnt&x"DEADBEEF"&cnt&x"00001111"&cnt&x"22223333"&cnt&x"44445555"&cnt;
  toHostFifo_wr_clk <= clk250;
  
  --! write fifo with a counter and a constant at 250MHz
  process(clk250, reset_hard)
  begin
    if(reset_hard = '1') then
      cnt <= (others => '0');
    elsif (rising_edge(clk250)) then
      if(toHostFifo_prog_full = '0') then
        -- Make a 32 bit counter, it will be mixed with some constants and written into the fifo.
        cnt <= cnt + 1;
      else
        cnt <= cnt;
      end if;
    end if;
  end process;
  

  
  
  
end architecture rtl ; -- of application

