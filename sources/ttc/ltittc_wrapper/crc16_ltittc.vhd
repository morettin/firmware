-------------------------------------------------------------------------------
-- Copyright (C) 2009 OutputLogic.com
-- This source file may be used and distributed without restriction
-- provided that this copyright statement is not removed from the file
-- and that any derivative work contains the original copyright notice
-- and the associated disclaimer.
--
-- THIS SOURCE FILE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS
-- OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
-- WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
-------------------------------------------------------------------------------
-- CRC module for data(15:0)
-------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;

entity crc16_ltittc is
    port ( data_in : in std_logic_vector (15 downto 0);
        crc_en , rst, clk : in std_logic;
        crc_out : out std_logic_vector (15 downto 0));
end crc16_ltittc;

architecture imp_crc of crc16_ltittc is

    constant Nbits :  positive   := 16;
    constant CRC_Width      :  positive   := 16;
    constant G_Poly: Std_Logic_Vector :=x"359f";
    constant G_InitVal: std_logic_vector:=x"ffff";


    function ToIndirectInitVal(Direct:std_logic_vector; CRC_Width: positive; Poly: std_logic_vector) return std_logic_vector is
        variable InDirect: std_logic_vector(Direct'high downto Direct'low);
    begin
        for k in 0 to CRC_Width loop
            if(k = 0) then
                InDirect := Direct;
            else
                if(InDirect(0)='1') then
                    InDirect := (('0'&InDirect(CRC_Width-1 downto 1)) xor ('1'&Poly(CRC_Width-1 downto 1)));
                else
                    InDirect := '0'&InDirect(CRC_Width-1 downto 1);
                end if;
            end if;
        end loop;
        return InDirect;
    end function ToIndirectInitVal;

    constant Poly: Std_Logic_Vector(CRC_Width-1 downto 0) := G_Poly;
    constant InitVal: Std_Logic_Vector(CRC_Width-1 downto 0) := G_InitVal;

    constant InDirectInitVal: std_logic_vector(CRC_Width-1 downto 0):=ToIndirectInitVal(InitVal, CRC_Width, Poly);
    signal Reg_s : std_logic_vector(CRC_Width-1 downto 0);
begin


    process (clk)
        variable Reg, Reg2: Std_Logic_Vector (CRC_Width-1 downto 0);
        variable ApplyPoly: std_logic;
    begin

        if rising_Edge(clk) then
            if rst = '1' then
                if(crc_en = '1') then
                    for k In 0 to Nbits loop
                        if(k = 0) then
                            Reg := (InDirectInitVal);--(CRC_Width-1 downto 0)&dinP(k))xor ('0'&Poly);
                        else
                            if Reg(CRC_Width-1) = '1' then
                                Reg := (Reg(CRC_Width-2 downto 0)&data_in(Nbits - k))xor (Poly);
                            else
                                Reg := Reg(CRC_Width-2 downto 0)&data_in(Nbits - k);
                            end if;
                        end if;
                    end loop;
                else
                    Reg := InDirectInitVal;
                end if;
            else
                if crc_en = '1' then
                    for k In 1 to Nbits loop
                        if Reg(CRC_Width-1) = '1' then
                            Reg := (Reg(CRC_Width-2 downto 0)&data_in(Nbits - k))xor (Poly);
                        else
                            Reg := Reg(CRC_Width-2 downto 0)&data_in(Nbits - k);
                        end if;
                    end loop;
                else
                    Reg := Reg;
                end if;
            end if;

            Reg_s <= Reg;
            Reg2 := Reg_s;
            --we need one more loop to output the crc_out register to the output.
            for k In 1 to CRC_Width loop
                if Reg2(CRC_Width-1) = '1' then
                    Reg2 := (Reg2(CRC_Width-2 downto 0)&'0')xor (Poly);
                else
                    Reg2:= Reg2(CRC_Width-2 downto 0)&'0';
                end if;
            end loop;
            crc_out <= Reg2;--(CRC_width-1 downto 0);
        end if;
    end process;
end architecture imp_crc;
