----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
----------------------------------------------------------------------------------

--MGT with Aurora8b10b presets show no disperr when using TX_DATA_gt_32b_tmp= x"bc" & x"000000" as
--soon as alignment is achieved. When using wavegen data, some disperr right
--after alignment. After a bit no disperr anymore
--MGT from scratch disperr also with TX_DATA_gt_32b_tmp
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
library UNISIM;
    use UNISIM.VCOMPONENTS.ALL;


--***************************** Entity Declaration ****************************
entity gth_ltittclink_wrapper_ku is
    generic(
        SIMULATION                  : boolean := false
    );
    port
(
        --from SI5345 to TX REFCLK. Can be either clk40_xtal or clk40_ttc
        GTREFCLK0_P_IN                  : in std_logic;
        GTREFCLK0_N_IN                  : in std_logic;
        --from LMK to RX REFCLK. Always clk40_xtal to ensure clock recovery which genetrates clk40_ttc
        GTREFCLK1_P_IN                  : in std_logic;
        GTREFCLK1_N_IN                  : in std_logic;
        DRP_CLK_IN                  : in   std_logic;

        --- RX clock, for each channel
        gt_rxusrclk_in             : in   std_logic;
        gt_rxoutclk_out            : out  std_logic;
        --simu or loopback
        --gt_txusrclk_in             : in   std_logic;
        gt_txoutclk_out            : out  std_logic;
        --
        -----------------------------------------
        ---- Control signals
        -----------------------------------------
        loopback_in                  : in std_logic_vector(2 downto 0);
        reset_all_in                 : in std_logic;
        cpllreset_in                 : in std_logic;
        reset_tx_pll_and_datapath_in : in std_logic;
        reset_tx_datapath_in         : in std_logic;
        reset_rx_pll_and_datapath_in : in std_logic;
        reset_rx_datapath_in         : in std_logic;
        -----------------------------------------
        ---- STATUS signals
        -----------------------------------------
        gt_rxbyteisaligned_out      : out  std_logic;
        gt_rxdisperr_out            : out  std_logic_vector(3 downto 0);
        gt_rxkdet_out               : out  std_logic_vector(3 downto 0);
        gt_rxcommadet_out           : out  std_logic_vector(3 downto 0);
        gt_rxnotintable_out         : out  std_logic_vector(3 downto 0);

        gt_rxresetdone_out          : out  std_logic;

        gt_cpllfbclklost_out        : out  std_logic;
        gt_cplllock_out             : out  std_logic;
        gt_cpllrefclklost_out       : out  std_logic;
        gt_rxcdrlock_out            : out  std_logic;

        gt_qpll1refclklost_out      : out std_logic;
        gt_qpll1lock_out            : out std_logic;
        ----------------------------------------------------------------
        ----------RESET SIGNALs
        ----------------------------------------------------------------
        --    SOFT_RESET_IN               : in     std_logic;
        --    GTRX_RESET_IN               : in   std_logic;
        --    CPLL_RESET_IN               : in   std_logic;
        --    QPLL_RESET_IN               : in   std_logic;

        --    SOFT_RXRST_GT               : in   std_logic;

        --    SOFT_RXRST_ALL              : in   std_logic;

        -----------------------------------------------------------
        ----------- Data and RX Ports
        -----------------------------------------------------------
        RX_DATA_gt_33b             : out std_logic_vector(32 downto 0); --[32]= disperr; [31:0]=payload
        TX_DATA_gt_16b             : in std_logic_vector(15 downto 0); --simu only or loopback
        TX_isK_gt                  : in std_logic_vector(1 downto 0);
        TXN_OUT                    : out   std_logic_vector(0 downto 0);
        TXP_OUT                    : out   std_logic_vector(0 downto 0);
        RXN_IN                      : in   std_logic_vector(0 downto 0);
        RXP_IN                      : in   std_logic_vector(0 downto 0)

    );
end gth_ltittclink_wrapper_ku;

architecture RTL of gth_ltittclink_wrapper_ku is


    COMPONENT gtwizard_ttc_rxcpll -- @suppress "Component declaration 'gtwizard_ttc_rxcpll' has none or multiple matching entity declarations"
        PORT (
            gtwiz_userclk_tx_reset_in           : in STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_userclk_tx_srcclk_out         : out STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_userclk_tx_usrclk_out         : out STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_userclk_tx_usrclk2_out        : out STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_userclk_tx_active_out         : out STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_userclk_rx_reset_in           : in STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_userclk_rx_srcclk_out         : out STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_userclk_rx_usrclk_out         : out STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_userclk_rx_usrclk2_out        : out STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_userclk_rx_active_out         : out STD_LOGIC_VECTOR ( 0 to 0 );
            gtwiz_buffbypass_tx_reset_in        : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_start_user_in   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_done_out        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_buffbypass_tx_error_out       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_clk_freerun_in          : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_all_in                  : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_pll_and_datapath_in  : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_datapath_in          : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_pll_and_datapath_in  : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_datapath_in          : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_cdr_stable_out       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_tx_done_out             : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_reset_rx_done_out             : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtwiz_userdata_tx_in                : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            gtwiz_userdata_rx_out               : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
            cplllockdetclk_in                   : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            cpllreset_in                        : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            --            drpclk_in                           : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxn_in                           : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthrxp_in                           : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            --            gtrefclk0_in                        : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            loopback_in                         : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
            rx8b10ben_in                        : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcommadeten_in                     : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxmcommaalignen_in                  : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpcommaalignen_in                  : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            tx8b10ben_in                        : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
            txctrl0_in                          : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            txctrl1_in                          : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
            txctrl2_in                          : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
            cpllfbclklost_out                   : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            cplllock_out                        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthtxn_out                          : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gthtxp_out                          : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtpowergood_out                     : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxbyteisaligned_out                 : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxbyterealign_out                   : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcdrlock_out                       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxcommadet_out                      : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxctrl0_out                         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            rxctrl1_out                         : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
            rxctrl2_out                         : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            rxctrl3_out                         : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
            --            rxoutclk_out                        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxpmaresetdone_out                  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            rxresetdone_out                     : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            --            txoutclk_out                        : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txpmaresetdone_out                  : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
            txresetdone_out                     : out STD_LOGIC_VECTOR(0 DOWNTO 0);
            gtrefclk01_in                       : in  STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outclk_out                     : out STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1outrefclk_out                  : out STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1fbclklost_out                  : out STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1lock_out                       : out STD_LOGIC_VECTOR(0 DOWNTO 0);
            qpll1refclklost_out                 : out STD_LOGIC_VECTOR(0 DOWNTO 0);
            txprgdivresetdone_out               : out STD_LOGIC_VECTOR(0 DOWNTO 0)
        );
    END COMPONENT;

    signal    GTH_RefClk0       : std_logic; --SI5345/TX
    signal    GTH_RefClk1             : std_logic; --LMK/RX


    signal rxctrl0_out                 : std_logic_vector(15 downto 0);
    signal rxctrl1_out                 : std_logic_vector(15 downto 0);
    signal rxctrl2_out                 : std_logic_vector(7 downto 0);
    signal rxctrl3_out                 : std_logic_vector(7 downto 0);
    signal rxctrl0_d_out                 : std_logic_vector(15 downto 0);
    signal rxctrl1_d_out                 : std_logic_vector(15 downto 0);
    signal rxctrl2_d_out                 : std_logic_vector(7 downto 0);
    signal rxctrl3_d_out                 : std_logic_vector(7 downto 0);
    signal rxcommadet_out              : std_logic_vector(0 downto 0); -- @suppress "signal rxcommadet_out is never read"
    signal rxcdrlock_out               : std_logic_vector(0 downto 0);
    signal rxbyteisaligned_out         : std_logic_vector(0 downto 0);

    --signal userclk_rx_active_out       : std_logic_vector(0 downto 0);
    --signal userclk_rx_active_out_p     : std_logic_vector(0 downto 0);

    --signal rxpmaresetdone_out          : std_logic_vector(0 downto 0);
    signal cplllock_out                : std_logic_vector(0 downto 0);
    signal cpllfbclklost_out           : std_logic_vector(0 downto 0);
    signal rxresetdone_out             : std_logic_vector(0 downto 0);

    signal qpll1lock_out               : std_logic_vector(0 downto 0);
    signal qpll1refclklost_out               : std_logic_vector(0 downto 0);

    signal reset_all                   : std_logic_vector(0 downto 0);

    signal userdata_rx_out             : std_logic_vector(31 downto 0);

    signal reset_rx_pll_and_datapath   : std_logic_vector(0 downto 0);
    signal reset_rx_datapath           : std_logic_vector(0 downto 0);

    signal gt_drp_clk_in               : std_logic_vector(0 downto 0);

    signal gtrefclk0_in                : std_logic_vector(0 downto 0);
    signal gtrefclk1_in                : std_logic_vector(0 downto 0);

    signal rxusrclk_in                 : std_logic_vector(0 downto 0);
    signal rxoutclk_out                : std_logic_vector(0 downto 0);

    signal gt_drp_clk_vec              : std_logic_vector(0 downto 0);

    --signal cpllrefclklost_out          : std_logic_vector(0 downto 0);



    --signal txusrclk_in                 : std_logic_vector(0 downto 0);
    signal txoutclk_out                : std_logic_vector(0 downto 0);
    --signal userclk_tx_active_out       : std_logic_vector(0 downto 0) := "0"; --0ifnot simu
    --signal userclk_tx_active_out_p     : std_logic_vector(0 downto 0) := "0";
    signal reset_tx_pll_and_datapath   : std_logic_vector(0 downto 0);
    signal reset_tx_datapath           : std_logic_vector(0 downto 0);
    --signal txpmaresetdone_out          : std_logic_vector(0 downto 0);
    --signal txpd_i                      : std_logic_vector(1 downto 0) := "11";
    signal rxbyterealign_i             : std_logic_vector(0 downto 0) := "0"; -- @suppress "signal rxbyterealign_i is never read"

    --for simu only and loopback=0
    signal TXN_i                       : std_logic_vector(0 downto 0) := "0";
    signal TXP_i                       : std_logic_vector(0 downto 0) := "0";
    signal RXN_i                       : std_logic_vector(0 downto 0) := "0";
    signal RXP_i                       : std_logic_vector(0 downto 0) := "0";
    --
    signal TX_DATA_gt_32b_tmp             :  std_logic_vector(31 downto 0); --simu only or loopback -- @suppress "signal TX_DATA_gt_32b_tmp is never read"
    signal TX_isK_gt_tmp                  :  std_logic_vector(3 downto 0); --simu only or loopback -- @suppress "signal TX_isK_gt_tmp is never read"

--signal commaalignen_i : std_logic_vector(0 downto 0) := "1";
--**************************** Main Body of Code *******************************


--COMPONENT debug_gth_rx
--PORT (
--  clk : IN STD_LOGIC;
--  probe0 : in STD_LOGIC_VECTOR(31 DOWNTO 0);
--  probe1 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
--  probe2 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
--  probe3 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
--  probe4 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
--  probe5 : in STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe6 : in STD_LOGIC_VECTOR(0 DOWNTO 0);
--  probe7 : in STD_LOGIC_VECTOR(0 DOWNTO 0)
--    );
--END COMPONENT;


begin

    --        comp_ila1 : debug_gth_rx
    --          PORT MAP (
    --            clk => gt_rxusrclk_in,
    --            probe0  =>  userdata_rx_out,
    --            probe1  =>  rxctrl0_d_out(3 downto 0),
    --            probe2  =>  rxctrl1_d_out(3 downto 0),
    --            probe3  =>  rxctrl2_d_out(3 downto 0),
    --            probe4  =>  rxctrl3_d_out(3 downto 0),
    --            probe5  =>  rxresetdone_out,
    --            probe6  =>  rxbyteisaligned_out,
    --            probe7  =>  rxcdrlock_out
    --          );


    ibufds_gtrefclk0_inst: IBUFDS_GTE3
        generic map (
            REFCLK_EN_TX_PATH => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX => "00"
        )
        port map (
            O     => GTH_RefClk0,
            ODIV2 => open,
            CEB   => '0',
            I     => GTREFCLK0_P_IN,
            IB    => GTREFCLK0_N_IN
        );

    ibufds_gtrefclk1_inst: IBUFDS_GTE3
        generic map (
            REFCLK_EN_TX_PATH => '0',
            REFCLK_HROW_CK_SEL => "00",
            REFCLK_ICNTL_RX => "00"
        )
        port map (
            O     => GTH_RefClk1,
            ODIV2 => open,
            CEB   => '0',
            I     => GTREFCLK1_P_IN,
            IB    => GTREFCLK1_N_IN
        );

    --------------------------------
    ---- Reset
    reset_all(0)                    <= reset_all_in;
    reset_tx_pll_and_datapath(0)    <= reset_tx_pll_and_datapath_in;
    reset_tx_datapath(0)            <= reset_tx_datapath_in;
    reset_rx_pll_and_datapath(0)    <= reset_rx_pll_and_datapath_in;
    reset_rx_datapath(0)            <= reset_rx_datapath_in;

    --------------------------------
    ---- Monitor
    gt_rxcdrlock_out                <= rxcdrlock_out(0);
    gt_rxbyteisaligned_out          <= rxcdrlock_out(0) and rxresetdone_out(0); --rxbyteisaligned_out(0);
    gt_rxdisperr_out                <= rxctrl1_out(3 downto 0); --[i] disp err found in byte i. Only [0] should be relevant because of the MGT configuration
    gt_rxkdet_out                   <= rxctrl0_out(3 downto 0); --[i] k character found in byte i. Only [0] should be relevant because of the MGT configuration
    gt_rxcommadet_out               <= rxctrl2_out(3 downto 0); --[i] comma character found in byte i. Only [0] should be relevant because of the MGT configuration
    gt_rxnotintable_out             <= rxctrl3_out(3 downto 0); --[i] not valid character found in byte i

    gt_rxresetdone_out              <= rxresetdone_out(0);

    gt_cpllfbclklost_out            <= cpllfbclklost_out(0);
    gt_cplllock_out                 <= cplllock_out(0);
    gt_cpllrefclklost_out           <= '1'; --cpllrefclklost_out(0);

    gt_rxcdrlock_out                <= rxcdrlock_out(0);

    gt_qpll1refclklost_out          <= qpll1refclklost_out(0);
    gt_qpll1lock_out                <= qpll1lock_out(0);

    --------------------------------
    --MT why 2 clock latency: simplify?
    process(gt_rxusrclk_in)
    begin
        if rising_edge(gt_rxusrclk_in) then
            rxctrl0_out <= rxctrl0_d_out;
            rxctrl1_out <= rxctrl1_d_out;
            rxctrl2_out <= rxctrl2_d_out;
            rxctrl3_out <= rxctrl3_d_out;
            if(rxctrl1_d_out(3 downto 0) = "0000" and rxctrl3_d_out(3 downto 0) = "0000") then
                RX_DATA_gt_33b <= '0' & userdata_rx_out(31 downto 0); --should I include also rxctrl1_out(3,2,1)
            else
                RX_DATA_gt_33b <= RX_DATA_gt_33b;
            end if;
        end if;
    end process;


    gt_drp_clk_in(0)              <= DRP_CLK_IN;
    gtrefclk0_in(0)               <= GTH_RefClk0;
    gtrefclk1_in(0)               <= GTH_RefClk1;

    gt_drp_clk_vec(0)             <= DRP_CLK_IN;

    rxusrclk_in(0)                <= gt_rxusrclk_in;
    gt_rxoutclk_out              <= rxoutclk_out(0);
    --txusrclk_in(0)                <= gt_txusrclk_in;
    gt_txoutclk_out              <= txoutclk_out(0);


    --  gt_gtrxreset_in <= GTRX_RESET_IN(0);


    --MT imported from gth_fullmode_wrapper_ku.vhd
    --process(rxpmaresetdone_out(0), rxusrclk_in(0))
    --begin
    --    if rxpmaresetdone_out(0) = '0' then
    --        --userclk_rx_active_out(0) <= '0';
    --        --userclk_rx_active_out_p(0) <= '0';
    --    elsif rxusrclk_in(0)'event and rxusrclk_in(0) = '1' then
    --        --userclk_rx_active_out_p(0) <= '1';
    --        --userclk_rx_active_out(0) <= userclk_rx_active_out_p(0);
    --    end if;
    --end process;

    --MT only for SIMU
    --process(txpmaresetdone_out(0), txusrclk_in(0))
    --begin
    --    if txpmaresetdone_out(0) = '0' then
    --        --userclk_tx_active_out(0) <= '0';
    --        userclk_tx_active_out_p(0) <= '0';
    --    elsif txusrclk_in(0)'event and txusrclk_in(0) = '1' then
    --        userclk_tx_active_out_p(0) <= '1';
    --        --userclk_tx_active_out(0) <= userclk_tx_active_out_p(0);
    --    end if;
    --end process;

    gtwizard_ttc_rxcpll_inst: gtwizard_ttc_rxcpll
        PORT MAP (
            gtwiz_userclk_tx_reset_in           => "0",
            gtwiz_userclk_tx_srcclk_out         => open,
            gtwiz_userclk_tx_usrclk_out         => open,
            gtwiz_userclk_tx_usrclk2_out        => txoutclk_out,
            gtwiz_userclk_tx_active_out         => open,
            gtwiz_userclk_rx_reset_in           => "0",
            gtwiz_userclk_rx_srcclk_out         => open,
            gtwiz_userclk_rx_usrclk_out         => open,
            gtwiz_userclk_rx_usrclk2_out        => rxoutclk_out,
            gtwiz_userclk_rx_active_out         => open,
            gtwiz_buffbypass_tx_reset_in        => "0",
            gtwiz_buffbypass_tx_start_user_in   => "0",
            gtwiz_buffbypass_tx_done_out        => open,
            gtwiz_buffbypass_tx_error_out       => open,
            gtwiz_reset_clk_freerun_in          => gt_drp_clk_in,
            gtwiz_reset_all_in                  => reset_all,
            gtwiz_reset_tx_pll_and_datapath_in  => reset_tx_pll_and_datapath,
            gtwiz_reset_tx_datapath_in          => reset_tx_datapath,
            gtwiz_reset_rx_pll_and_datapath_in  => reset_rx_pll_and_datapath,
            gtwiz_reset_rx_datapath_in          => reset_rx_datapath,
            gtwiz_reset_rx_cdr_stable_out       => open,
            gtwiz_reset_tx_done_out             => open,
            gtwiz_reset_rx_done_out             => open,
            gtwiz_userdata_tx_in                => TX_DATA_gt_16b,
            gtwiz_userdata_rx_out               => userdata_rx_out,
            cplllockdetclk_in                   => gt_drp_clk_vec,
            cpllreset_in(0)                     => cpllreset_in,
            --            drpclk_in                           => gt_drp_clk_in,
            gthrxn_in                           => RXN_i,
            gthrxp_in                           => RXP_i,
            --            gtrefclk0_in                        => gtrefclk0_in,
            loopback_in                         => loopback_in,
            rx8b10ben_in                        => "1",
            rxcommadeten_in                     => "1",
            rxmcommaalignen_in                  => "1", --commaalignen_i,
            rxpcommaalignen_in                  => "1", --commaalignen_i,
            tx8b10ben_in                        => "1",
            txctrl0_in                          => (others => '0'),
            txctrl1_in                          => (others => '0'),
            txctrl2_in                          => "000000" & TX_isK_gt,
            cpllfbclklost_out                   => cpllfbclklost_out,
            cplllock_out                        => cplllock_out,
            gthtxn_out                          => TXN_i,
            gthtxp_out                          => TXP_i,
            gtpowergood_out                     => open,
            rxbyteisaligned_out                 => rxbyteisaligned_out,
            rxbyterealign_out                   => rxbyterealign_i,
            rxcdrlock_out                       => rxcdrlock_out,
            rxcommadet_out                      => rxcommadet_out,
            rxctrl0_out                         => rxctrl0_d_out,
            rxctrl1_out                         => rxctrl1_d_out,
            rxctrl2_out                         => rxctrl2_d_out,
            rxctrl3_out                         => rxctrl3_d_out,
            --            rxoutclk_out                        => rxoutclk_out,
            rxpmaresetdone_out                  => open, --rxpmaresetdone_out,
            rxresetdone_out                     => rxresetdone_out,
            --            txoutclk_out                        => txoutclk_out,
            txpmaresetdone_out                  => open, --txpmaresetdone_out,
            txresetdone_out                     => open,
            gtrefclk01_in                       => gtrefclk1_in,
            qpll1outclk_out                     => open,
            qpll1outrefclk_out                  => open,
            qpll1fbclklost_out                  => open,
            qpll1lock_out                       => qpll1lock_out,
            qpll1refclklost_out                 => qpll1refclklost_out,
            txprgdivresetdone_out               => open
        );

    TX_DATA_gt_32b_tmp <= x"bc" & x"000000";
    TX_isK_gt_tmp <= "1000";

    --MT make it a SM: think about disabling the alignment at some point.
    --Commented for now
    process(rxusrclk_in(0))
        variable cnt : integer range 0 to 100;
    begin
        if rxusrclk_in(0)'event and rxusrclk_in(0) = '1' then
            if (rxbyteisaligned_out = "1") then
                if(cnt = 50) then
                    cnt := cnt;
                else
                    cnt := cnt+1;
                end if;
            else
                cnt := 0;
            end if;
        --if(cnt = 50) then
        --    commaalignen_i <= "0";
        --else
        --    commaalignen_i <= "1";
        --end if;
        end if;
    end process;



    loopback_simu: if SIMULATION generate
        RXN_i <= TXN_i; --when (loopback_in = "000");
        RXP_i <= TXP_i; -- when (loopback_in = "000");
    --MT tmp: needs to have a dedicate reg for that
    --txpd_i <= "00";
    --
    end generate;
    noloopback: if SIMULATION = false generate
        TXN_OUT <= TXN_i;
        TXP_OUT <= TXP_i;
        RXN_i <= RXN_IN;
        RXP_i <= RXP_IN;
    --txpd_i <= "11" when (loopback_in = "000"); --TX no needed for real usecase
    end generate;


--vio_gbt_chk_inst: vio_gbt_chk
--PORT MAP(
--clk           => DRP_CLK_IN,
--probe_in0     => disp_err0,
--probe_in1     => disp_err1,
--probe_in2     => txresetdone_out,
--probe_in3     => rxresetdone_out,
--probe_in4     => txpmaresetdone_out,
--probe_in5     => rxpmaresetdone_out,
--probe_in6     => rxbyteisaligned_out,
--probe_in7     => rxbyterealign_out,
--probe_in8     => rxcdrlock_out,
--probe_in9     => cplllock_out,
--probe_in10    => cpllfbclklost_out,
--probe_in11    => reset_tx_done_out,
--probe_in12    => reset_rx_done_out,
--probe_in13    => reset_cdr_stable,
--probe_in14    => buffbypass_tx_error,
--probe_in15    => buffbypass_tx_done,
--probe_in16    => qpll1lock_out,
--probe_in17     => disp_err2,
--probe_in18     => disp_err3,
--probe_in19     => comma_not_tab0,
--probe_in20     => comma_not_tab1,
--probe_in21     => comma_not_tab2,
--probe_in22     => comma_not_tab3,

--probe_out0    => reset_all,
--probe_out1    => buffbypass_tx_reset_in,
--probe_out2    => cpllreset_in,
--probe_out3    => reset_tx_pll_and_datapath,
--probe_out4    => reset_tx_datapath,
--probe_out5    => reset_rx_pll_and_datapath,
--probe_out6    => reset_rx_datapath,
--probe_out7    => lmk_reset_out
--);


--ila_gbt_chk_inst: ila_gbt_chk
--PORT MAP(
--clk      => rxusrclk_in(0),
--probe0   => rxdata0,
--probe1   => rxdata1,
--probe2   => rxdata2,
--probe3   => rxdata3,
--probe4   => comma_8b10b_deted0,
--probe5   => comma_8b10b_deted1,
--probe6   => comma_8b10b_deted2,
--probe7   => comma_8b10b_deted3,
--probe8   => comma_deted0,
--probe9   => comma_deted1,
--probe10  => comma_deted2,
--probe11  => comma_deted3
--);


end RTL;
