----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
----------------------------------------------------------------------------------

--TO DO: check that all signals (_40, etc) are retimed in the same way (thus
--preserving the latency
--make naming differently between local_ttc_clk and clk40

--naming convention: _40 refers to signal @ clk40_ttc
--counters and comparators with DSPs: check resources

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.NUMERIC_STD.ALL;
    use ieee.numeric_std_unsigned.all;

library UNISIM;
    use UNISIM.VComponents.all;

    use work.FELIX_package.all;
    use work.centralRouter_package.all;
    use work.pcie_package.all;

--=================================================================================================--
--======================================= Module Body =============================================--
--=================================================================================================--
entity ltittc_wrapper is
    generic(
        CARD_TYPE                   : integer := 712;
        ITK_TRIGTAG_MODE            : integer := -1    -- -1: not implemented, 0: RD53A, 1: ITkPixV1, 2: ABC*
    );
    port (
        reset_in                    : in std_logic;
        register_map_control        : in   register_map_control_type;
        register_map_ltittc_monitor : out  register_map_ltittc_monitor_type;
        --== to Central Router ==---
        TTC_out                     : out TTC_data_type;
        clk40_ttc_out               : out std_logic; --TTC 40 MHz clock from clk240_rx_ttc_in
        clk40_in                    : in std_logic;             --clock source to synchronize TTC_out to the rest of the logic
        clk40_xtal_in               : in std_logic;             --to RX REFCLK
        BUSY                        : out std_logic;            --TTC decode errors
        cdrlocked_out               : out std_logic;
        TTC_ToHost_Data_out         : out TTC_ToHost_data_type;
        --FS: These two records are not necessary, as all information is in TTC_out.
        --TTCMSG_ToHost_Data_out      : out TTCMSG_ToHost_data_type; --@clk40
        --USRMSG_ToHost_Data_out      : out USRMSG_ToHost_data_type; --@clk40
        TTC_BUSY_mon_array          : in busyOut_array_type(23 downto 0);
        BUSY_IN                     : in std_logic;
        GTREFCLK0_P_IN              : in std_logic;
        GTREFCLK0_N_IN              : in std_logic;
        GTREFCLK1_P_IN              : in std_logic;
        GTREFCLK1_N_IN              : in std_logic;
        RX_P_LTITTC                 : in std_logic;
        RX_N_LTITTC                 : in std_logic;
        TX_P_LTITTC                 : out std_logic;
        TX_N_LTITTC                 : out std_logic

    );

end ltittc_wrapper;


architecture top of ltittc_wrapper is

    signal clk40_ttc_i            : std_logic;
    signal busy_sync              : std_logic;
    signal ready                  : std_logic;

    --control registers
    signal rst_TTCtoHost          : std_logic := '0'; -- @suppress "signal rst_TTCtoHost is never read"
    signal clr_ttype_mntr         : std_logic := '0';
    signal clr_bcid_mntr          : std_logic := '0';
    signal clr_sl0id_mntr         : std_logic := '0';
    signal clr_sorb_mntr          : std_logic := '0';
    signal clr_grst_mntr          : std_logic := '0';
    signal clr_sync_mntr          : std_logic := '0';
    signal clr_l0id_err_mntr      : std_logic := '0';
    signal clr_bcr_err_mntr       : std_logic := '0';


    --TTC frame signals
    signal TTC_data_decoder_40    : std_logic_vector(191 downto 0);
    signal typemsg_40             : std_logic := '0';
    --signal partn_40               : std_logic_vector(2 downto 0)  := (others=>'0');
    signal bcid_40                : std_logic_vector(11 downto 0) := (others=>'0');
    --signal syncusrdata_40         : std_logic_vector(15 downto 0) := (others => '0');
    signal l0a_40                 : std_logic := '0';
    signal sl0id_40               : std_logic := '0';
    signal sorb_40                : std_logic := '0';
    signal sync_40                : std_logic := '0';
    signal grst_40                : std_logic := '0';
    signal l0id_40                : std_logic_vector(37 downto 0) := (others=>'0');
    signal orbitid_40             : std_logic_vector(31 downto 0) := (others=>'0');
    signal ttype_40               : std_logic_vector(15 downto 0) := (others => '0');
    signal lbid_40                : std_logic_vector(15 downto 0) := (others => '0');
    --signal crc_40                 : std_logic_vector(15 downto 0) := (others => '0');
    signal asyncusrdata_40        : std_logic_vector(63 downto 0) := (others => '0');
    --signal syncglobaldata_40      : std_logic_vector(15 downto 0) := (others => '0');
    --signal turnsignal_40          : std_logic := '0';
    --signal errorflags_40          : std_logic_vector(3 downto 0) := (others => '0');

    signal brc_b_40 : std_logic   := '0'; --=1 every time bcid_40=3654-1 -- @suppress "signal brc_b_40 is never read"

    signal TTC_data_decoder_i     : std_logic_vector(191 downto 0) := (others=>'0');
    signal typemsg                : std_logic := '0';
    signal partn                  : std_logic_vector(2 downto 0)  := (others=>'0');
    signal bcid                   : std_logic_vector(11 downto 0) := (others=>'0');
    signal syncusrdata            : std_logic_vector(15 downto 0) := (others => '0');
    signal l0a                    : std_logic := '0';
    signal sl0id                  : std_logic := '0';
    signal sorb                   : std_logic := '0';
    signal sync                   : std_logic := '0';
    signal grst                   : std_logic := '0';
    signal l0id                   : std_logic_vector(37 downto 0) := (others=>'0');
    signal orbitid                : std_logic_vector(31 downto 0) := (others=>'0');
    signal ttype                  : std_logic_vector(15 downto 0) := (others => '0');
    signal lbid                   : std_logic_vector(15 downto 0) := (others => '0');
    --signal crc                    : std_logic_vector(15 downto 0) := (others => '0');
    signal asyncusrdata           : std_logic_vector(63 downto 0) := (others => '0');
    signal syncglobaldata         : std_logic_vector(15 downto 0) := (others => '0');
    signal turnsignal             : std_logic := '0';
    signal errorflags             : std_logic_vector(3 downto 0) := (others => '0');


    signal l0id_r               : std_logic_vector(37 downto 0) := (others => '0');
    signal orbitid_r            : std_logic_vector(31 downto 0) := (others => '0');
    signal ttype_r              : std_logic_vector(15 downto 0) := (others => '0');
    signal lbid_r               : std_logic_vector(15 downto 0) := (others => '0');
    signal asyncusrdata_r       : std_logic_vector(63 downto 0) := (others => '0');

    signal l0id_40_r            : std_logic_vector(37 downto 0) := (others => '0');
    signal orbitid_40_r         : std_logic_vector(31 downto 0) := (others => '0');
    signal ttype_40_r           : std_logic_vector(15 downto 0) := (others => '0');
    signal lbid_40_r            : std_logic_vector(15 downto 0) := (others => '0');
    signal asyncusrdata_40_r    : std_logic_vector(63 downto 0) := (others => '0');

    --signal rsvd                   : std_logic_vector(15 downto 0)  := (others => '0');
    --signal brc_b                  : std_logic := '0';

    --monitoring counters and re  sets
    signal l0id_40_int            : std_logic_vector(37 downto 0)  := (others => '0');
    signal ttype_counter_40       :  std_logic_vector(31 downto 0) := (others => '0');
    signal bcid_40_int            : std_logic_vector(11 downto 0)  := (others => '0');
    signal brc_b_40_int           : std_logic := '0'; --internally generated. =1 every time bcr_period=3654-1
    signal sl0id_counter_40       : std_logic_vector(31 downto 0)  := (others => '0');
    signal sorb_counter_40        : std_logic_vector(31 downto 0)  := (others => '0');
    signal sync_counter_40        : std_logic_vector(31 downto 0)  := (others => '0');
    signal grst_counter_40        : std_logic_vector(31 downto 0)  := (others => '0');

    signal ttype_cntr_reset_40    : std_logic_vector(1 downto 0);
    signal bcid_cntr_reset_40     : std_logic_vector(1 downto 0);
    signal l0id_cntr_reset_40     : std_logic_vector(1 downto 0);
    signal sl0id_cntr_reset_40    : std_logic_vector(1 downto 0);
    signal sorb_cntr_reset_40     : std_logic_vector(1 downto 0);
    signal sync_cntr_reset_40     : std_logic_vector(1 downto 0);
    signal grst_cntr_reset_40     : std_logic_vector(1 downto 0);
    signal bcr_err_cntr_reset_40  : std_logic_vector(1 downto 0);
    signal l0id_err_cntr_reset_40 : std_logic_vector(1 downto 0);
    signal bcr_err_counter_40     :  std_logic_vector(31 downto 0) := (others => '0');
    signal l0id_err_counter_40    :  std_logic_vector(31 downto 0) := (others => '0');
    signal crc_err_counter_40     :  std_logic_vector(31 downto 0) := (others => '0'); -- @suppress "signal crc_err_counter_40 is never written"
    signal ttype_counter          :  std_logic_vector(31 downto 0) := (others => '0');
    signal sl0id_counter          : std_logic_vector(31 downto 0)  := (others => '0');
    signal sorb_counter           : std_logic_vector(31 downto 0)  := (others => '0');
    signal sync_counter           : std_logic_vector(31 downto 0)  := (others => '0');
    signal grst_counter           : std_logic_vector(31 downto 0)  := (others => '0');
    signal bcr_err_counter        :  std_logic_vector(31 downto 0) := (others => '0');
    signal l0id_err_counter       :  std_logic_vector(31 downto 0) := (others => '0');
    signal crc_err_counter        :  std_logic_vector(31 downto 0) := (others => '0'); -- @suppress "signal crc_err_counter is never read"

    --from register
    signal ttype_reg_40           :  std_logic_vector(15 downto 0) := (others => '0');
    signal ttype_reg              :  std_logic_vector(15 downto 0) := (others => '0');

    --error from the decoder
    signal ltittc_bit_err         : std_logic_vector(2 downto 0);
    signal ltittc_bit_err_40      : std_logic_vector(2 downto 0);

    --data out
    signal TTC_out_i              : TTC_data_type;
    --signal ToHostData_i           : std_logic_vector(191 downto 0);

    signal RXUSRCLK_TTC           : std_logic; --240 Mhz clock rcovered from the TTC signal
    signal RX_DATA_TTC            : std_logic_vector(32 downto 0); -- from MGT
    signal LinkAligned_TTC        : std_logic;

    signal TXUSRCLK_TTC           : std_logic; --TXUSRCLK from MGT TO emulator
    signal TX_DATA_TTC            : std_logic_vector(15 downto 0); --emulated data to MGT
    signal ISK_TTC                : std_logic_vector(1 downto 0);
    signal RX_IsK_TTC             : std_logic_vector(3 downto 0);

    --======================--
    --- Trigger Tag for ITk
    --======================--
    signal itk_sync : std_logic;
    signal itk_trig : std_logic_vector(3 downto 0);
    signal itk_tag : std_logic_vector(6 downto 0);
    signal lti_decoder_aligned : std_logic;
    signal crc_valid : std_logic;
    signal lti_decoder_aligned_40 : std_logic;
    signal crc_valid_40 : std_logic;
--signal itk_ttc2h_tag : std_logic_vector(7 downto 0);

begin


    clk40_ttc_out  <= clk40_ttc_i;

    --=====================--
    -- ttc wrapper control --
    --=====================--
    --these registers are needed: comment out below once registers are created

    rst_TTCtoHost     <= to_sl(register_map_control.LTITTC_CTRL.TOHOST_RST   );
    clr_ttype_mntr    <= to_sl(register_map_control.LTITTC_TTYPE_MONITOR.CLEAR   );
    clr_bcid_mntr     <= to_sl(register_map_control.LTITTC_BCR_ERR_MONITOR.CLEAR    );
    clr_sl0id_mntr    <= to_sl(register_map_control.LTITTC_SL0ID_MONITOR.CLEAR   );
    clr_sorb_mntr     <= to_sl(register_map_control.LTITTC_SORB_MONITOR.CLEAR    );
    clr_grst_mntr     <= to_sl(register_map_control.LTITTC_GRST_MONITOR.CLEAR    );
    clr_sync_mntr     <= to_sl(register_map_control.LTITTC_SYNC_MONITOR.CLEAR    );
    clr_l0id_err_mntr <= to_sl(register_map_control.LTITTC_L0ID_ERR_MONITOR.CLEAR);
    clr_bcr_err_mntr  <= to_sl(register_map_control.LTITTC_BCR_ERR_MONITOR.CLEAR );
    --  clr_crc_err_mntr  <= to_sl(register_map_control.LTITTC_CRR_ERR_MONITOR.CLEAR );
    ttype_reg         <= register_map_control.LTITTC_TTYPE_MONITOR.REFVALUE;

    ----=====================--
    ---- ttc wrapper monitor --
    ----=====================--

    register_map_ltittc_monitor.LTITTC_MON.LTITTC_BIT_ERR     <= ltittc_bit_err;
    g_links: for i in 0 to 23 generate
        register_map_ltittc_monitor.LTITTC_BUSY_ACCEPTED(i)   <= TTC_BUSY_mon_array(i);
    end generate;
    register_map_ltittc_monitor.LTITTC_MON.BUSY_OUTPUT_STATUS <= (others => BUSY_IN);
    register_map_ltittc_monitor.LTITTC_TTYPE_MONITOR.VALUE    <= ttype_counter;
    register_map_ltittc_monitor.LTITTC_SL0ID_MONITOR.VALUE    <= sl0id_counter;
    register_map_ltittc_monitor.LTITTC_SORB_MONITOR.VALUE     <= sorb_counter;
    register_map_ltittc_monitor.LTITTC_GRST_MONITOR.VALUE     <= grst_counter;
    register_map_ltittc_monitor.LTITTC_SYNC_MONITOR.VALUE     <= sync_counter;
    register_map_ltittc_monitor.LTITTC_BCR_ERR_MONITOR.VALUE  <= bcr_err_counter;
    register_map_ltittc_monitor.LTITTC_L0ID_ERR_MONITOR.VALUE <= l0id_err_counter;
    --  register_map_ttc_monitor.LTITTC_CRCERR_MONITOR.VALUE           <= crc_mimsmatch_counter;

    --=====================--
    -- RETIMING CONTROL REGISTERS: from clk40 to clk_ttc
    --=====================--

    RETIMING_REG: process(clk40_ttc_i)
    begin
        if rising_edge(clk40_ttc_i) then
            ttype_reg_40 <= ttype_reg;
        end if;
    end process;

    --=====================--
    -- RETIMING MONITOR REGISTERS : 40 MHZ clk40_ttc_i to clk40
    --=====================--

    RETIMING_CNTER: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            ttype_counter         <= ttype_counter_40;
            sl0id_counter         <= sl0id_counter_40;
            sorb_counter          <= sorb_counter_40;
            grst_counter          <= grst_counter_40;
            sync_counter          <= sync_counter_40;
            bcr_err_counter       <= bcr_err_counter_40;
            l0id_err_counter      <= l0id_err_counter_40;
            crc_err_counter       <= crc_err_counter_40;
        end if;
    end process;

    --=====================--
    -- COUNTERS and RESETS: l0id, ttype, bcid and BCR, S0LID, S0Orb, Grst, Sync
    --=====================--
    L0ID_gen: process (clk40_ttc_i)
    begin
        if (rising_edge(clk40_ttc_i)) then
            l0id_cntr_reset_40(0) <= clr_l0id_err_mntr;
            l0id_cntr_reset_40(1) <= l0id_cntr_reset_40(0);

            if (l0id_cntr_reset_40(0) = '1' and l0id_cntr_reset_40(1) = '0') then
                l0id_40_int <= l0id_40;
            elsif (sl0id_40 = '1') then
                l0id_40_int <= l0id_40;
            elsif (l0a_40 = '1' and l0id_40_int /= x"3FFFFFFFFF") then
                l0id_40_int  <= l0id_40_int + 1;
            end if;
        end if; --clock
    end process;

    --Trigger TYPE counter for monitoring:
    TTYPE_CNT: process (clk40_ttc_i)
    begin
        if (rising_edge(clk40_ttc_i)) then
            ttype_cntr_reset_40(0) <= clr_ttype_mntr;
            ttype_cntr_reset_40(1) <= ttype_cntr_reset_40(0);

            if (ttype_cntr_reset_40(0) = '1' and ttype_cntr_reset_40(1) = '0') then
                ttype_counter_40 <= (others => '0');
            elsif ( typemsg_40 = '0' and ttype_40 = ttype_reg_40) then
                ttype_counter_40 <= ttype_counter_40 + 1;
            end if;
        end if;
    end process;

    BCID_CNT: process (clk40_ttc_i)
    begin
        if (rising_edge(clk40_ttc_i)) then
            bcid_cntr_reset_40(0) <= clr_bcid_mntr;
            bcid_cntr_reset_40(1) <= bcid_cntr_reset_40(0);

            if (bcid_cntr_reset_40(1) = '0' and bcid_cntr_reset_40(0) = '1') then
                bcid_40_int            <= bcid;
            elsif (brc_b_40_int = '0') then
                bcid_40_int <= bcid_40_int + 1;
            else
                bcid_40_int <= (others=>'0');
            end if;
        end if;
    end process;

    BCR_B_proc: process (clk40_ttc_i)
    begin
        if (rising_edge(clk40_ttc_i)) then
            if (bcid_40_int = "110111101011") then  --3564-1, where 3564 is the number of buckets in an LHC orbit.
                brc_b_40_int <= '1';
            else
                brc_b_40_int <= '0';
            end if;

            if (bcid_40 = "110111101011") then  --3564-1, where 3564 is the number of buckets in an LHC orbit.
                brc_b_40 <= '1';
            else
                brc_b_40 <= '0';
            end if;
        end if;
    end process;

    SL0ID_CNT: process (clk40_ttc_i)
    begin
        if (rising_edge(clk40_ttc_i)) then
            sl0id_cntr_reset_40(0) <= clr_sl0id_mntr;
            sl0id_cntr_reset_40(1) <= sl0id_cntr_reset_40(0);

            if (sl0id_cntr_reset_40(1) = '0' and sl0id_cntr_reset_40(0) = '1') then
                sl0id_counter_40 <= (others=>'0');
            elsif (sl0id_40 = '1') then
                sl0id_counter_40 <= sl0id_counter_40 + 1;
            end if;
        end if;
    end process;

    SORB_CNT: process (clk40_ttc_i)
    begin
        if (rising_edge(clk40_ttc_i)) then
            sorb_cntr_reset_40(0) <= clr_sorb_mntr;
            sorb_cntr_reset_40(1) <= sorb_cntr_reset_40(0);

            if (sorb_cntr_reset_40(1) = '0' and sorb_cntr_reset_40(0) = '1') then
                sorb_counter_40 <= (others=>'0');
            elsif (sorb_40 = '1') then
                sorb_counter_40 <= sorb_counter_40 + 1;
            end if;
        end if;
    end process;

    GRST_CNT: process (clk40_ttc_i)
    begin
        if (rising_edge(clk40_ttc_i)) then
            grst_cntr_reset_40(0) <= clr_grst_mntr;
            grst_cntr_reset_40(1) <= grst_cntr_reset_40(0);

            if (grst_cntr_reset_40(1) = '0' and grst_cntr_reset_40(0) = '1') then
                grst_counter_40 <= (others=>'0');
            elsif (grst_40 = '1') then
                grst_counter_40 <= grst_counter_40 + 1;
            end if;
        end if;
    end process;

    SYNC_CNT: process (clk40_ttc_i)
    begin
        if (rising_edge(clk40_ttc_i)) then
            sync_cntr_reset_40(0) <= clr_sync_mntr;
            sync_cntr_reset_40(1) <= sync_cntr_reset_40(0);

            if (sync_cntr_reset_40(1) = '0' and sync_cntr_reset_40(0) = '1') then
                sync_counter_40 <= (others=>'0');
            elsif (sync_40 = '1') then
                sync_counter_40 <= sync_counter_40 + 1;
            end if;
        end if;
    end process;

    --=====================--
    -- ERROR COUNTERS: l0id, BCR, TO DO: crc
    --=====================--
    L0ID_ERR_CNT: process (clk40_ttc_i)
    begin
        if (rising_edge(clk40_ttc_i)) then
            l0id_err_cntr_reset_40(0) <= clr_l0id_err_mntr;
            l0id_err_cntr_reset_40(1) <= l0id_err_cntr_reset_40(0);

            if (l0id_err_cntr_reset_40(1) = '0' and l0id_err_cntr_reset_40(0) = '1') then
                l0id_err_counter_40  <= (others=>'0');
            elsif (l0id_40 /= l0id_40_int) then
                if (l0id_err_counter_40 /= x"FFFFFFFF") then
                    l0id_err_counter_40  <= l0id_err_counter_40 + 1;
                end if;
            end if;
        end if;
    end process;


    BCR_ERR_CNT: process (clk40_ttc_i) --at the rollover
    begin
        if (rising_edge(clk40_ttc_i)) then
            bcr_err_cntr_reset_40(0) <= clr_bcr_err_mntr;
            bcr_err_cntr_reset_40(1) <= bcr_err_cntr_reset_40(0);

            if (bcr_err_cntr_reset_40(1) = '0' and bcr_err_cntr_reset_40(0) = '1') then
                bcr_err_counter_40  <= (others=>'0');
            elsif (brc_b_40_int = '1' and bcid_40 /= bcid_40_int) then --bcid_40_int="110111101011"
                if (bcr_err_counter_40 /= x"FFFFFFFF") then
                    bcr_err_counter_40  <= bcr_err_counter_40 + 1;
                end if;
            end if;
        end if;
    end process;

    --=====================--
    -- RETIMING TOHOST DATA : clk40_ttc to clk40_in
    --=====================--
    --RETIMING_TOHOSTDATA: process(clk40_in)
    --begin
    --    if rising_edge(clk40_in) then
    --        if (rst_TTCtoHost = '1') then
    --            ToHostData_i <= (others => '0');
    --        else
    --            ToHostData_i <= TTC_data_decoder_40;
    --        end if;
    --    end if;
    --end process;

    --!FS: These signals are all contained in TTC_out, do we need another record?
    --=====================--
    -- ASSIGN TOHOST DATA :
    --=====================--
    --TTCMSG_ToHost_Data_out.TYPEMSG        <= typemsg;
    --TTCMSG_ToHost_Data_out.PartN          <= partn;
    --TTCMSG_ToHost_Data_out.BCID           <= bcid;
    --TTCMSG_ToHost_Data_out.SYNCUSRDATA    <= syncusrdata;
    --TTCMSG_ToHost_Data_out.SYNCGLOBALDATA <= syncglobaldata;
    --TTCMSG_ToHost_Data_out.TURNSIGNAL     <= turnsignal;
    --TTCMSG_ToHost_Data_out.ERRORFLAGS     <= errorflags;
    --TTCMSG_ToHost_Data_out.SL0ID          <= sl0id;
    --TTCMSG_ToHost_Data_out.SORB           <= sorb;
    --TTCMSG_ToHost_Data_out.SYNC           <= sync;
    --TTCMSG_ToHost_Data_out.GRST           <= grst;
    --TTCMSG_ToHost_Data_out.L0A            <= l0a;
    --TTCMSG_ToHost_Data_out.L0ID           <= l0id;
    --TTCMSG_ToHost_Data_out.ORBITID        <= orbitid;
    --TTCMSG_ToHost_Data_out.TTYPE          <= ttype;
    --TTCMSG_ToHost_Data_out.LBID           <= lbid;
    --TTCMSG_ToHost_Data_out.CRC            <= crc;
    --
    --USRMSG_ToHost_Data_out.TYPEMSG        <= typemsg;
    --USRMSG_ToHost_Data_out.PartN          <= partn;
    --USRMSG_ToHost_Data_out.BCID           <= bcid;
    --USRMSG_ToHost_Data_out.SYNCUSRDATA    <= syncusrdata;
    --USRMSG_ToHost_Data_out.SYNCGLOBALDATA <= syncglobaldata;
    --USRMSG_ToHost_Data_out.TURNSIGNAL     <= turnsignal;
    --USRMSG_ToHost_Data_out.ERRORFLAGS     <= errorflags;
    --USRMSG_ToHost_Data_out.RSVD0          <= rsvd0;
    --USRMSG_ToHost_Data_out.ASYNCUSRDATA   <= asyncusrdata;
    --USRMSG_ToHost_Data_out.RSVD1          <= rsvd1;
    --USRMSG_ToHost_Data_out.CRC            <= crc;
    --*************** For the timing been. This is temp solution for TTC to HOST
    TTC_ToHost_Data_out.FMT          <= X"03"; --: std_logic_vector(7 downto 0);  --byte0
    TTC_ToHost_Data_out.LEN          <= X"1b"; --: std_logic_vector(7 downto 0);  --byte1
    TTC_ToHost_Data_out.reserved0    <= X"0"; --: std_logic_vector(3 downto 0);  --byte2
    TTC_ToHost_Data_out.BCID         <= bcid; --: std_logic_vector(11 downto 0); --byte2,3
    TTC_ToHost_Data_out.XL1ID        <= l0id(31 downto 24); --: std_logic_vector(7 downto 0);  --byte4
    TTC_ToHost_Data_out.L1ID         <= l0id(23 downto 0); --: std_logic_vector(23 downto 0); --byte 5,6,7
    TTC_ToHost_Data_out.orbit        <= orbitid; --: std_logic_vector(31 downto 0); --byte 8,9,10,11
    TTC_ToHost_Data_out.trigger_type <= ttype; --: std_logic_vector(15 downto 0); --byte 12,13
    TTC_ToHost_Data_out.reserved1    <= (others => '0'); --: std_logic_vector(15 downto 0); --byte 14,15
    TTC_ToHost_Data_out.L0ID         <= l0id(31 downto 0); --: std_logic_vector(31 downto 0); --byte 16,17,18,19
    TTC_ToHost_Data_out.TAG          <= '0' & itk_tag; --: std_logic_vector(15 downto 0)
    TTC_ToHost_Data_out.data_rdy     <= l0a;
    --***************End temp solution



    --=====================--
    -- RETIMING TTC DATA : clk40_ttc to clk40_in
    --=====================--

    RETIMING_TTCOUT: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            TTC_data_decoder_i <= TTC_data_decoder_40; --MT to be fixed
            --brc_b <= brc_b_40;
            ltittc_bit_err <= ltittc_bit_err_40;
            lti_decoder_aligned <= lti_decoder_aligned_40;
            crc_valid <= crc_valid_40;
            if (ltittc_bit_err = x"7") then
                busy_sync <= '1';
            else
                busy_sync <= '0';
            end if;
        end if;
    end process;

    BUSY    <= busy_sync; --MT to define properly
    TTC_out <= TTC_out_i;

    --routing the whole TTC message minus the comma (192b-8b) rather than 136b mentioned in Table 2.2 of https://edms.cern.ch/ui/file/1563801/1/RequirementsPhaseII_v1.1.0.pdf
    --TTC_out_i.MT               <= typemsg;
    TTC_out_i.PT               <= partn(2);
    TTC_out_i.Partition        <= partn(1 downto 0);
    TTC_out_i.BCID             <= bcid;
    TTC_out_i.SyncUserData     <= syncusrdata;
    TTC_out_i.SyncGlobalData   <= syncglobaldata;
    TTC_out_i.TS               <= turnsignal;
    TTC_out_i.ErrorFlags       <= errorflags;
    TTC_out_i.SL0ID            <= sl0id;
    TTC_out_i.SOrb             <= sorb;
    TTC_out_i.Sync             <= sync;
    TTC_out_i.GRst             <= grst;
    TTC_out_i.L0A              <= l0a;
    TTC_out_i.L0ID             <= l0id;
    TTC_out_i.OrbitID          <= orbitid;
    TTC_out_i.TriggerType      <= ttype;
    TTC_out_i.LBID             <= lbid;
    --! FS: No need to distribute LTI trailer info through the firmware
    --TTC_out_i.CRC              <= crc;
    --TTC_out_i.D16_2            <= d16_2;
    TTC_out_i.LTI_CRC_valid <= crc_valid;
    TTC_out_i.LTI_decoder_aligned <= lti_decoder_aligned;
    TTC_out_i.AsyncUserData    <= asyncusrdata;

    --Phase 1 signals for backward compatibility:
    TTC_out_i.L1A <= l0a;
    TTC_out_i.Bchan <= '0';
    TTC_out_i.BCR <= turnsignal;
    TTC_out_i.Brcst <= syncusrdata(5 downto 0);
    TTC_out_i.Brcst_latched <= syncusrdata(5 downto 0);
    TTC_out_i.L1Id <= l0id(23 downto 0);

    ExtendingTestPulse: entity work.ExtendedTestPulse (Behavioral)
        port map(
            clk40                => clk40_in,
            reset                => reset_in,

            TTCin_TestPulse      => syncusrdata(0),
            ExtendedTestPulse    => TTC_out_i.ExtendedTestPulse
        );

    TTC_out_i.ITk_sync <= itk_sync;
    TTC_out_i.ITk_tag <= itk_tag;
    TTC_out_i.ITk_trig <= itk_trig;


    --=====================--
    -- Trigger Tag for ITk
    --=====================--

    tag_gen_enable: if ITK_TRIGTAG_MODE >= 0 generate
        tag_gen: entity work.itk_trigtag_generator
            generic map (
                MODE => ITK_TRIGTAG_MODE
            )
            port map (
                clk => clk40_in,
                rst => reset_in,
                enc_trig => itk_trig,
                enc_sync => itk_sync,
                enc_tag => itk_tag,
                ttc2h_tag => open, --@TODO: itk_ttc2h_tag,
                ttc2h_tag_valid => open,
                ttc_l0a => l0a,
                ttc_bcr => turnsignal,
                ttc_phase => "00"           -- TODO: connect to something meaningful in the regbank
            );
    end generate;

    tag_gen_disable: if ITK_TRIGTAG_MODE < 0 generate
        itk_trig <= "0000";
        itk_sync <= '0';
        itk_tag <= (others => '0');
    --itk_ttc2h_tag <= (others => '0');
    end generate;

    --=====================--
    -- DECODER
    --=====================--

    ltittc_dec: entity work.ltittc_decoder
        port map (
            reset_in        => reset_in,
            clk240_in       => RXUSRCLK_TTC,
            LinkAligned_in  => LinkAligned_TTC,
            IsK_in          => RX_IsK_TTC,
            data_in         => RX_DATA_TTC,
            data_out        => TTC_data_decoder_40,
            clk40_out       => clk40_ttc_i,
            clk40_ready_out => ready,  -- the ttc_clk_gated is present
            cnt_error_out   => ltittc_bit_err_40,
            decoder_aligned_out => lti_decoder_aligned_40,
            crc_valid_out   => crc_valid_40
        );


    cdrlocked_out <= ready; --MT phase2 drive ready = 1 when ttc_clk_gated is present



    --=====================--
    -- WAVE GENERATOR
    --=====================--
    --emulates data to be sent to this wrapper
    --does not emulate from this wrapper
    ltittc_wavegen: entity work.ltittc_wavegen
        generic map(
            GENCOUNTER => FALSE
        )
        port map
    (
            reset_in       => reset_in,
            reset_cnt_in   => reset_in,
            clk40_in       => clk40_in, --_ttc_i,
            clk240_in      => TXUSRCLK_TTC,
            data_out       => open, --TX_DATA_TTC,
            isK_out        => open, --ISK_TTC,
            --simu only
            tx_data_40MHZ_out => open
        );

    --=====================--
    -- LTITTC DATA GENERATOR
    --=====================--
    ltittc_transmitter: entity work.ltittc_transmitter
        port map
    (
            reset_in       => reset_in,
            clk40_in       => clk40_in, --_ttc_i,
            clk240_in      => TXUSRCLK_TTC,
            busy_in        => TXUSRCLK_TTC,
            data_out       => TX_DATA_TTC,
            isK_out        => ISK_TTC
        );


    --FS: Preserve ID/counter values, pulse signals should not be preserved.

    --As per Table 2.3 of https://edms.cern.ch/document/2379978/1 (v1.1)
    typemsg           <= TTC_data_decoder_i(31+0*32);
    partn             <= TTC_data_decoder_i(30+0*32 downto 28+0*32);
    bcid              <= TTC_data_decoder_i(27+0*32 downto 16+0*32);
    syncusrdata       <= TTC_data_decoder_i(15+0*32 downto  0+0*32);
    syncglobaldata    <= TTC_data_decoder_i(31+1*32 downto 16+1*32);
    turnsignal        <= TTC_data_decoder_i(15+1*32);
    errorflags        <= TTC_data_decoder_i(14+1*32 downto 11+1*32);
    sl0id             <= TTC_data_decoder_i(10+1*32)                when typemsg = '0' else '0';
    sorb              <= TTC_data_decoder_i( 9+1*32)                when typemsg = '0' else '0';
    sync              <= TTC_data_decoder_i( 8+1*32)                when typemsg = '0' else '0';
    grst              <= TTC_data_decoder_i( 7+1*32)                when typemsg = '0' else '0';
    l0a               <= TTC_data_decoder_i( 6+1*32)                when typemsg = '0' else '0';
    l0id              <= TTC_data_decoder_i(31+2*32 downto  0+2*32) &
                         TTC_data_decoder_i(5+1*32 downto 0+1*32)   when typemsg = '0' else l0id_r;
    orbitid           <= TTC_data_decoder_i(31+3*32 downto  0+3*32) when typemsg = '0' else orbitid_r;
    ttype             <= TTC_data_decoder_i(31+4*32 downto 16+4*32) when typemsg = '0' else ttype_r;
    lbid              <= TTC_data_decoder_i(15+4*32 downto  0+4*32) when typemsg = '0' else lbid_r;
    asyncusrdata      <= TTC_data_decoder_i(31+4*32 downto  0+3*32) when typemsg = '1' else asyncusrdata_r;
    --CRC is checked in ltittc_decoder
    --crc               <= TTC_data_decoder_i(31+5*32 downto 16+5*32);

    reg_proc: process(clk40_in)
    begin
        if rising_edge(clk40_in) then
            l0id_r          <= l0id;
            orbitid_r       <= orbitid;
            ttype_r         <= ttype;
            lbid_r          <= lbid;
            asyncusrdata_r  <= asyncusrdata;
        end if;
    end process;

    typemsg_40        <= TTC_data_decoder_40(31+0*32);
    --partn_40          <= TTC_data_decoder_40(30+0*32 downto 28+0*32);
    bcid_40           <= TTC_data_decoder_40(27+0*32 downto 16+0*32);
    --syncusrdata_40    <= TTC_data_decoder_40(15+0*32 downto  0+0*32);
    --syncglobaldata_40 <= TTC_data_decoder_40(31+1*32 downto 16+1*32);
    --turnsignal_40     <= TTC_data_decoder_40(15+1*32);
    --errorflags_40     <= TTC_data_decoder_40(14+1*32 downto 11+1*32);
    sl0id_40          <= TTC_data_decoder_40(10+1*32)                when typemsg_40 = '0' else '0';
    sorb_40           <= TTC_data_decoder_40( 9+1*32)                when typemsg_40 = '0' else '0';
    sync_40           <= TTC_data_decoder_40( 8+1*32)                when typemsg_40 = '0' else '0';
    grst_40           <= TTC_data_decoder_40( 7+1*32)                when typemsg_40 = '0' else '0';
    l0a_40            <= TTC_data_decoder_40( 6+1*32)                when typemsg_40 = '0' else '0';
    l0id_40           <= TTC_data_decoder_40(31+2*32 downto  0+2*32) &
                         TTC_data_decoder_40(5+1*32 downto 0+1*32)   when typemsg_40 = '0' else l0id_40_r;
    orbitid_40        <= TTC_data_decoder_40(31+3*32 downto  0+3*32) when typemsg_40 = '0' else orbitid_40_r;
    ttype_40          <= TTC_data_decoder_40(31+4*32 downto 16+4*32) when typemsg_40 = '0' else ttype_40_r;
    lbid_40           <= TTC_data_decoder_40(15+4*32 downto  0+4*32) when typemsg_40 = '0' else lbid_40_r;
    asyncusrdata_40   <= TTC_data_decoder_40(31+4*32 downto  0+3*32) when typemsg_40 = '1' else asyncusrdata_40_r;
    --crc_40            <= TTC_data_decoder_40(31+5*32 downto 16+5*32);

    reg_40_proc: process(clk40_ttc_i)
    begin
        if rising_edge(clk40_ttc_i) then
            l0id_40_r          <= l0id_40;
            orbitid_40_r       <= orbitid_40;
            ttype_40_r         <= ttype_40;
            lbid_40_r          <= lbid_40;
            asyncusrdata_40_r  <= asyncusrdata_40;
        end if;
    end process;

    --=====================--
    -- TTC Link Wrapper--
    --=====================--
    g_TTC_712: if CARD_TYPE = 711 or CARD_TYPE = 712 generate
        u2: entity work.FLX_LTITTCLink_Wrapper
            generic map(
                SIMULATION => FALSE
            )
            Port map(
                rst_hw               => reset_in,
                register_map_control => register_map_control,
                register_map_ltittc_monitor => register_map_ltittc_monitor,
                RXP_IN(0)            => RX_P_LTITTC,  --RX(24) bank 232
                RXN_IN(0)            => RX_N_LTITTC,

                clk40_in             => clk40_in,

                DRP_CLK_IN           => clk40_xtal_in,
                GTREFCLK0_P_IN         => GTREFCLK0_P_IN,
                GTREFCLK0_N_IN         => GTREFCLK0_N_IN,
                GTREFCLK1_P_IN         => GTREFCLK1_P_IN,
                GTREFCLK1_N_IN         => GTREFCLK1_N_IN,

                RXUSERCLK_OUT        => RXUSRCLK_TTC,
                RX_DATA_33b_out      => RX_DATA_TTC, --1b(disperr)+32b @ 240 MHz out
                RX_DATA_33b_rdy_out  => LinkAligned_TTC,
                RX_IsK_OUT           => RX_IsK_TTC,

                --for testing only (either simu or loopback)
                TXUSERCLK_OUT        => TXUSRCLK_TTC ,
                TX_DATA_16b_in       => TX_DATA_TTC,
                ISK_in               => ISK_TTC,               --

                TXN_OUT(0) => TX_N_LTITTC,
                TXP_OUT(0) => TX_P_LTITTC
            );
    end generate;

end top;

