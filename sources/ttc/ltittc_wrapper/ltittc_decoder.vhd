----------------------------------------------------------------------------------
--! Company:  Argonne National Laboratory
--! Engineer: Marco Trovato (mtrovato@anl.gov)
--! Update 18-08-2023
--! Company:  NIKHEF
--! Engineer: Mesfin Gebyehu (mgebyehu@nikhef.nl)
----------------------------------------------------------------------------------
----------------------------------------------------------------------------------

--Assumption: data_in(7 downto 0) = K28.5


library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.centralRouter_package.all;

library UNISIM;
    use UNISIM.VComponents.all;


--use work.all;
entity ltittc_decoder is
    port
    (
        reset_in                  : in std_logic;
        clk240_in                 : in std_logic;
        --@clk240_in
        LinkAligned_in            : in std_logic;
        IsK_in                    : in std_logic_vector(3 downto 0);
        data_in                   : in std_logic_vector(32 downto 0);
        --@clk40_out
        data_out                  : out std_logic_vector(191 downto 0);
        clk40_out                 : out std_logic;
        clk40_ready_out           : out std_logic; --@clk240_in
        cnt_error_out             : out std_logic_vector(2 downto 0);
        decoder_aligned_out       : out std_logic;
        crc_valid_out             : out std_logic


    );

end ltittc_decoder;

architecture top of ltittc_decoder is

    --    type   smtype  is (CLKSTART,ALIGN,COMMA_FOUND);
    --    constant   smtype : std_logic_vector(2 downto 0); -- is (CLKSTART,ALIGN,COMMA_FOUND);
    constant   CLKSTART : std_logic_vector(1 downto 0) := "00";
    constant   ALIGN : std_logic_vector(1 downto 0) := "01";
    constant   COMMA_FOUND : std_logic_vector(1 downto 0) := "10";
    signal detector_state           : std_logic_vector(1 downto 0) := "00";
    signal data_in32       : std_logic_vector(31 downto 0);
    signal disperr_in      : std_logic; -- @suppress "signal disperr_in is never read"
    signal cnt             : integer range 0 to 5;
    signal cnt_error_i     : std_logic_vector(2 downto 0);
    signal clk40_ready_i   : std_logic                      := '0';
    signal clk40_i         : std_logic                      := '1';
    signal state_i         : std_logic_vector(1 downto 0)   := "00";
    signal clk40_out_i     : std_logic := '0';
    signal data_out_i      : std_logic_vector(191 downto 0) := (others => '0');
    signal crc_reset, crc_en                                        : std_logic;
    signal crc_out                                                  : std_logic_vector(15 downto 0);
    signal crc_in: std_logic_vector(31 downto 0);

--  COMPONENT debug_decoder
--  PORT (
--    clk : IN STD_LOGIC;
--    probe0 : in STD_LOGIC_VECTOR(191 DOWNTO 0);
--    probe1 : in STD_LOGIC_VECTOR(31 DOWNTO 0);
--    probe2 : in STD_LOGIC_VECTOR(3 DOWNTO 0);
--    probe3 : in STD_LOGIC_VECTOR(1 DOWNTO 0)
--      );
--  END COMPONENT;


begin

    --        comp_ila1 : debug_decoder
    --        PORT MAP (
    --          clk => clk240_in,
    --         probe0  =>  data_out_i,
    --         probe1  =>  data_in32,
    --         probe2  =>  IsK_in,
    --        probe3  =>  detector_state
    --        );
    --

    data_in32  <= data_in(31 downto 0);
    disperr_in <= data_in(32);
    ---------------------------
    ---*********DATA GEARBOX and CLK START*********
    ----------------------------
    state_i <= "00" when detector_state = CLKSTART else
               "01" when detector_state = ALIGN else
               "10" when detector_state = COMMA_FOUND else
               "11";
    decoder_aligned_out <= '1' when detector_state = COMMA_FOUND else '0';

    crc_reset <= '1' when cnt = 5 else '0';
    crc_en <= not crc_reset;
    crc_in <= data_in32 when cnt /= 5 else (others => '1');

    CRC16_0 : entity work.crc16_lti
        port map(
            data_in => crc_in,
            crc_en  => crc_en,
            rst     => crc_reset,
            clk     => clk240_in,
            crc_out => crc_out
        );

    GEARBOX_CLKST_proc : process(clk240_in)
        variable data_i        : std_logic_vector(191 downto 0);
    begin
        if clk240_in'event and clk240_in='1' then
            if (reset_in = '1' or LinkAligned_in = '0') then
                detector_state                                   <= ALIGN;
                data_i                                  := (others=>'0');
                cnt                                     <= 5;
                crc_valid_out               <= '0';
            else
                case detector_state is
                    when ALIGN =>
                        data_i                              := (others=>'0');
                        cnt                               <= 0;
                        crc_valid_out <= '0';
                        if (data_in32(7 downto 0) = Kchar_comma and IsK_in(0) = '1') then
                            detector_state                             <= COMMA_FOUND;
                        else
                            detector_state                             <= ALIGN;
                        end if;
                    when COMMA_FOUND =>
                        if (cnt /= 5) then
                            detector_state                             <= COMMA_FOUND;
                            data_i(32*cnt+31 downto 32*cnt)   := data_in32; --writing word 0 to 4
                            cnt                               <= cnt+1;
                        else
                            cnt                           <= 0;
                            if (data_in32(7 downto 0) = Kchar_comma and IsK_in(0) = '1') then         --double check that comma appear again
                                detector_state                           <= COMMA_FOUND;
                                data_i(32*cnt+31 downto 32*cnt) := data_in32(31 downto 8) & x"00"; --writing word 5, dropping comma of next bunch (since MGT is 4 byte aligned to comma it will have comma in the LSByte)
                            else --comma not found again in the same place.
                                data_i                          := (others=>'0');
                                detector_state                         <= ALIGN;
                            end if; --if (data_in32(7 downto 0) = Kchar_comma)
                            data_out_i                        <= data_i; --every 6 clocks


                            if(crc_out = data_in32(31 downto 16)) then
                                crc_valid_out <= '1';
                            else
                                crc_valid_out <= '0';
                            end if;

                        end if;

                    when others => detector_state <= ALIGN;

                end case;
            end if;

        end if;
    end process;

    ---------------------------
    ---*********CLK*********
    ----------------------------

    CLK_proc: process(clk240_in)
    begin
        if clk240_in'event and clk240_in='1' then
            if ((cnt = 2 or cnt = 5) and state_i = "10") then
                clk40_i       <= not clk40_i;
                clk40_ready_i <= '1';
            end if;
        end if;
    end process;


    bufg_ttc : BUFG_GT --CE
        generic map(
            SIM_DEVICE => "ULTRASCALE",
            STARTUP_SYNC => "FALSE"
        )
        port map(
            O  => clk40_out_i,
            CE => clk40_ready_i,
            CEMASK => '0',
            CLR => '0',
            CLRMASK => '0',
            DIV => "000",
            I  => clk40_i
        );
    clk40_out       <= clk40_out_i;
    clk40_ready_out <= clk40_ready_i;

    RETIMING : process(clk40_out_i)
    begin
        if rising_edge(clk40_out_i) then
            --double register needed for simu
            data_out         <= data_out_i;
            cnt_error_out    <= cnt_error_i;
        end if;
    end process;



end top;


