--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Andrea Borga
--!               Frans Schreuder
--!               Thei Wijnen
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/04/2016 02:41:30 PM
-- Design Name: 
-- Module Name: ttc_emulator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity ttc_emulator is
    Port ( clk_in : in STD_LOGIC;
           reset  : in STD_LOGIC;
           freq   : in STD_LOGIC_VECTOR(2 downto 0);
           l1a_trigger : out std_logic;
           l1id : out std_logic_vector(23 downto 0);
--         ttc_frame_out : out STD_LOGIC_VECTOR (119 downto 0);
           bcr  : out STD_LOGIC;
--         l1a : out STD_LOGIC;
           bcid : out STD_LOGIC_VECTOR (11 downto 0);
           period_o : out std_logic_vector(1 downto 0);
           state : out std_logic_vector(2 downto 0)
         );
end ttc_emulator;

architecture Behavioral of ttc_emulator is
  type state_type is (s0, run72, idle8, idle38);
  signal bc_state : state_type;
  signal clk_count : std_logic_vector(11 downto 0); -- 12 bits 2^12-1 = 4095  bc maximum =
  signal bc_count : std_logic_vector(11 downto 0);
  signal clk_inc, clk_reg   : unsigned(11 downto 0);
  signal period : unsigned(1 downto 0);
  signal bcs, bcreset : std_logic;
  signal l1a_counter : std_logic_vector(23 downto 0);
  signal scale  : integer;
begin

  scale <= 40     when freq = "001" else 
           400    when freq = "010" else
           4000   when freq = "011" else
           40000  when freq = "100" else
           400; 

  process (clk_in, reset, clk_count)
  begin
     if reset = '1' then
        bc_state <= s0;
        period <=  (others => '0');
        clk_reg <= (others => '0');
     elsif (clk_in'event and clk_in = '1') then
        clk_reg <= clk_reg + 1;   
        case bc_state is
           when s0    => 
             bc_state  <= run72;
             clk_reg   <= (others => '0');
             period    <= period + 1;
             bcr <= '0'; bcreset <= '0'; 
                 
           when run72   =>
             bcr <= '0'; bcreset <= '0';           
           
             if    clk_reg = "0001001000" then -- 72th clk
                bc_state <= idle8;
             elsif clk_reg = "0010011000" then -- 152th clk
                bc_state <= idle8;   
             elsif clk_reg = "0011101000" then -- 232th clk
                bc_state <= idle38;            
             elsif clk_reg = "0101010110" then -- 342th clk
                bc_state <= idle8;            
             elsif clk_reg = "0110100110" then -- 422th clk
                bc_state <= idle8;            
             elsif clk_reg = "0111110110" then -- 502th clk
                bc_state <= idle38;
             elsif clk_reg = "1001100100" then -- 612th clk
                bc_state <= idle8;
             elsif clk_reg = "1010110100" then -- 692th clk
                bc_state <= idle8;
             elsif clk_reg = "1100000100" then -- 772th clk
                bc_state <= idle8;
             elsif clk_reg = "1101010100" then -- 852th clk
                bc_state <= idle38;
             end if;            
             
           when idle8 =>
             if    clk_reg = "0001010000" then  -- 80th clk
                bc_state <= run72;
             elsif clk_reg = "0010100000" then  -- 160th clk
                bc_state <= run72;
             elsif clk_reg = "0101011110" then  -- 350th clk
                bc_state <= run72;
             elsif clk_reg = "0110101110" then  -- 430th clk
                bc_state <= run72;
             elsif clk_reg = "1001101100" then  -- 620th clk
                bc_state <= run72;
             elsif clk_reg = "1010111100" then  -- 700th clk
                bc_state <= run72;
             elsif clk_reg = "1100001100" then  -- 780th clk
                bc_state <= run72;
             end if;
            
           when idle38 =>
             if    clk_reg = "0100001110" then  -- 270th clk
                bc_state <= run72;
             elsif clk_reg = "1000011100" then  -- 540th clk
                bc_state <= run72;
             elsif clk_reg = "1101111001" then  -- 889th clk
                bc_state <= s0;
                if (period = "00") then
                   bcr <= '1'; bcreset <= '1';      
                end if;                               
             end if;
        end case;
     end if;
  end process;
    
  --clk_inc   <= clk_reg + 1;
  clk_count <= std_logic_vector(clk_reg);
  bcid <= bc_count;
  period_o <= std_logic_vector(period);
             
  
  process (bc_state)
  begin
     case bc_state is
        when s0 => 
             state <= (others =>'0');
             bcs <= '0';
        when run72 =>
             state <= "001"; bcs <= '1';
        when idle8 =>
             state <= "010"; bcs <= '0';
        when idle38 =>
             state <= "100"; bcs <= '0';
     end case;
  end process;
  
  bunch_counter : process (clk_in, reset, bcs, bcreset)
  begin
    if reset = '1' then
      bc_count <= (others=>'0');
    elsif (bcreset = '1') then
      bc_count <= (others=>'0');
    elsif (clk_in'event and clk_in = '1') then
      if (bcs = '1') then
        bc_count <= std_logic_vector(unsigned(bc_count)+1);
      end if;
    end if;
  end process;
  
  L1a_generator : process (clk_in, reset, bcs)
    variable cnt : integer := 0;
    variable mode : integer;
  begin
    if reset = '1' then
       l1a_trigger <= '0';
       mode := scale;
       l1a_counter <= (others => '0');
    elsif (clk_in'event and clk_in='1') then
       if (bcs='1') then
          cnt := cnt + 1;
          if (cnt mod mode = 0) then
             l1a_trigger <= '1';
             l1a_counter <= std_logic_vector(unsigned(l1a_counter)+1);
          else
             l1a_trigger <= '0';
          end if;
       end if;
    end if;
  end process;
  
  l1id <= l1a_counter;
  
  
  
end Behavioral;

