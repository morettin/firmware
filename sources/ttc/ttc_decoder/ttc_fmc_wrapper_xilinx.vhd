--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Soo Ryu
--!               Israel Grayzman
--!               Kai Chen
--!               Ricardo Luz
--!               Thei Wijnen
--!               Alessandra Camplani
--!               Ohad Shaked
--!               Alexander Paramonov
--!               Ali Skaf
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std_unsigned.all;
    use IEEE.NUMERIC_STD.ALL;

library UNISIM;
    use UNISIM.VComponents.all;
library xpm;
    use xpm.vcomponents.all;

    use work.centralRouter_package.all;
    use work.pcie_package.all;
    use work.FELIX_package.all;
--=================================================================================================--
--======================================= Module Body =============================================--
--=================================================================================================--
entity ttc_wrapper is
    generic(
        CARD_TYPE     : integer;
        FIRMWARE_MODE : integer  :=0;
        ISTESTBEAM    : boolean := true;
        ITK_TRIGTAG_MODE : integer := -1    -- -1: not implemented, 0: RD53A, 1: ITkPixV1, 2: ABC*
    );
    port (
        --== ttc fmc interface ==--
        CLK_TTC_P                   : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR 160MHz clock output
        CLK_TTC_N                   : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR 160MHz clock output
        DATA_TTC_P                  : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR Serial Data output
        DATA_TTC_N                  : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR Serial Data output
        TB_trigger                  : in   std_logic_vector(NUM_TB_TRIGGERS(CARD_TYPE)-1 downto 0);
        LOL_ADN                     : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR Loss of Lock output
        LOS_ADN                     : in   std_logic_vector(NUM_TTC_INPUTS(CARD_TYPE)-1 downto 0);           -- ADN2812 CDR Loss of Signal output
        --RESET_N                     : in   std_logic;           -- if low, stop PLL inside wrapper
        register_map_control        : in   register_map_control_type;
        register_map_ttc_monitor    : out  register_map_ttc_monitor_type;
        register_map_control_appreg_clk : in register_map_control_type; --For writing from RM into FIFO for AsyncUserData
        appreg_clk                  : in std_logic;                     --For writing from RM into FIFO for AsyncUserData
        --== to Central Router ==---
        TTC_out                     : out TTC_data_type;

        clk_adn_160                 : out std_logic;
        clk_ttc_40                  : out std_logic;            --40Mhz clock rcovered from the TTC signal

        clk40                       : in std_logic;             --clock source to synchronize TTC_out to the rest of the logic

        BUSY                        : out std_logic;            --TTC decode errors
        cdrlocked_out               : out std_logic;

        TTC_ToHost_Data_out         : out TTC_ToHost_data_type;
        TTC_BUSY_mon_array          : in busyOut_array_type(23 downto 0);
        BUSY_IN                     : in std_logic
    );

end ttc_wrapper;


architecture top of ttc_wrapper is

    signal TTCEmu_TTCout          : std_logic_vector(9 downto 0);
    signal TTCEmu_add_s8          : std_logic_vector(7 downto 0);
    signal TTCEmu_add_strobe      : std_logic;
    signal TTCEmu_add_e           : std_logic;
    signal TTCEMu_add_d8          : std_logic_vector(7 downto 0);
    signal xTTCin: std_logic_vector(28 downto 0); --Original TTC signals extended with "extended test pulse"
    signal xTTCin_delayed: std_logic_vector(28 downto 0); --Delayed (Using SRL16E) version of xTTCin

    --signal RESET            : std_logic;
    --========================= Signal Declarations ==========================--
    --signal cdrbad         : std_logic;
    --signal pll_clk        : std_logic;
    --signal pll_locked     : std_logic;
    --====================--
    -- ttc wrapper control
    --====================--

    signal rst_TTCtoHost                : std_logic;
    signal rst_TTCtoHost_40             : std_logic; -- in the TTC clock domain (40 MHz)
    --signal MMCM_MAIN_LCLK_SEL           : std_logic;
    --signal master_BUSY                  : std_logic; -- to throtlle the L1A
    --signal L1A_throttle                 : std_logic;
    signal TTC_BIT_ERR_REG              : std_logic_vector(2 downto 0);

    --==============--
    -- ttc decoder
    --==============--
    signal cdrclk_in                    : std_logic;
    signal cdrdata_in                   : std_logic;
    signal xcdrdata_in                  : std_logic;
    signal l1a                          : std_logic;
    signal l1a_40                       : std_logic;
    signal l1a_tmp                      : std_logic;
    --signal l1a_dec                      : std_logic;
    signal l1a_dec_ttcdec               : std_logic;
    signal channelB                     : std_logic;
    signal channelB_ttcdec              : std_logic;
    signal brc_e, brc_ei, brc_e_40      : std_logic;
    signal brc_ei_ttcdec                : std_logic;
    signal brc_b, brc_bi, brc_b_40      : std_logic;
    signal brc_bi_ttcdec                : std_logic;
    signal brcst7_40                    : std_logic;
    signal brc_t2                       : std_logic_vector(1 downto 0);
    signal brc_t2_d4_40                 : std_logic_vector(5 downto 0);
    signal brc_t2_ttcdec                : std_logic_vector(1 downto 0);
    signal brc_d4                       : std_logic_vector(3 downto 0);
    signal brc_d4_ttcdec                : std_logic_vector(3 downto 0);
    signal single_bit_error             : std_logic;
    signal single_bit_error_ttcdec      : std_logic;
    signal double_bit_error             : std_logic;
    signal double_bit_error_ttcdec      : std_logic;
    signal communication_error          : std_logic;
    signal communication_error_ttcdec   : std_logic;
    signal brc_strobe                   : std_logic;
    signal brc_strobe_40                : std_logic;
    --signal brc_strobe_ttcdec            : std_logic;
    signal add_strobe                   : std_logic;
    signal add_strobe_ttcdec            : std_logic;
    signal add_strobe_40                : std_logic;
    signal add_a14                      : std_logic_vector(13 downto 0);
    signal add_a14_40                   : std_logic_vector(13 downto 0);
    --signal add_a14_ttcdec               : std_logic_vector(13 downto 0);
    signal add_e                        : std_logic;
    signal add_e_ttcdec                 : std_logic;
    signal add_e_40                     : std_logic;
    signal add_s8                       : std_logic_vector(7 downto 0);
    signal add_s8_ttcdec                : std_logic_vector(7 downto 0);
    signal add_s8_40                    : std_logic_vector(7 downto 0);
    signal add_d8                       : std_logic_vector(7 downto 0);
    signal add_d8_ttcdec                : std_logic_vector(7 downto 0);
    signal add_d8_40                    : std_logic_vector(7 downto 0);
    signal ready                        : std_logic;        --- indicates PLL Locked
    signal cdrlocked                    : std_logic; --ready signal synchronized to clk40
    signal ttc_clk_gated                : std_logic;        -- gated 40MHz clock, for comparison only
    signal cdrclk_in_locked             : std_logic;
    signal div_nrst                     : std_logic;
    signal div_nrst_ttcdec              : std_logic;

    signal busy_unsync                  : std_logic;
    signal busy_sync                    : std_logic;
    signal TTC_Out_unsync               : std_logic_vector(27 downto 0);
    signal TTC_Out_sync                 : std_logic_vector(27 downto 0);
    signal TTC_out_delayed              : std_logic_vector(27 downto 0);
    signal Brcst_latched            : std_logic_vector(5 downto 0) := "000000"; -- needed for the NSW. Controlled with the long commands

    --======================--
    --- Trigger Tag for ITk
    --======================--
    signal itk_sync : std_logic;
    signal itk_trig : std_logic_vector(3 downto 0);
    signal itk_tag : std_logic_vector(6 downto 0);
    signal itk_ttc2h_tag : std_logic_vector(7 downto 0);

    --======================--
    --- TTC-To-Host Design ---
    --======================--
    -- "TT" stands for trigger type

    -- To-Host control registers --
    signal XL1ID_RST                    : std_logic;  -- Reset XL1ID
    signal XL1ID_RST_latch              : std_logic;  -- delayed Reset XL1ID
    signal XL1ID_SW                     : std_logic_vector(7 downto 0);  --XL1ID is set to this value when reset
    signal TT_Bch_En_Reg                : std_logic;  -- Enabling this will make use of trigger type information from TTC B channel

    -- To-Host data format and initial values --
    constant FMT                        : std_logic_vector(7 downto 0) := x"03";  --byte0, represents version
    constant LEN                        : std_logic_vector(7 downto 0) := x"1B";  --byte1, length of packet, always 26
    constant reserved0                  : std_logic_vector(3 downto 0) := x"0";  --byte2, set to all 0
    signal BCID                         : std_logic_vector(11 downto 0):= x"000"; --byte2,3
    signal BCID_reg                     : std_logic_vector(11 downto 0):= x"000"; -- BCID is set to this when reset
    signal XL1ID                        : std_logic_vector(7 downto 0) := x"00";  --byte4
    signal L1ID                         : std_logic_vector(23 downto 0):= x"000000"; --byte 5,6,7
    signal orbit                        : std_logic_vector(31 downto 0):= x"00000000"; --byte 8,9,10,11
    signal trigger_type                 : std_logic_vector(15 downto 0):= x"0000"; --byte 12,13
    --signal trigger_type_align           : std_logic_vector(15 downto 0); --byte 12,13
    constant reserved1                  : std_logic_vector(15 downto 0):= x"0000"; --byte 14,15
    signal L0ID                         : std_logic_vector(31 downto 0):= x"00000000"; --byte 16,17,18,19

    signal dbw_bcr                      : std_logic;
    signal orbit_reset_latch            : std_logic;

    --signal ToHostData           : std_logic_vector(143 downto 0);   -- 144 bit = 20 bytes of ToHostData
    signal ToHostData_in                : std_logic_vector(144 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d1             : std_logic_vector(144 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d2             : std_logic_vector(144 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d3             : std_logic_vector(144 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d4             : std_logic_vector(144 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_d5             : std_logic_vector(144 downto 0);   -- 1 bit for indicating an ignored L1 trigger + ToHostData
    signal ToHostData_in_fifo           : std_logic_vector(152 downto 0);   -- same as above + 8-bit trigger tag
    signal ToHostData_out               : std_logic_vector(152 downto 0);   -- same as just above
    signal ToHostData_tmp               : std_logic_vector(168 downto 0);   -- same as just above
    signal ToHostData_fifo              : std_logic_vector(168 downto 0);   -- same as just above


    --- To-Host Fifo control ---
    signal ToHostData_count             : std_logic_vector(10 downto 0);
    signal ToHostData_full              : std_logic;
    signal ToHostData_empty             : std_logic;
    signal wr_en_ToHostData             : std_logic;
    signal wr_en_ToHostData_d1          : std_logic;
    signal wr_en_ToHostData_d2          : std_logic;
    signal wr_en_ToHostData_d3          : std_logic;
    signal wr_en_ToHostData_d4          : std_logic;
    signal wr_en_ToHostData_d5          : std_logic;
    signal ToHostData_ready             : std_logic;
    signal ToHostData_ready_d1          : std_logic;
    signal ToHostData_ready_d2          : std_logic;
    signal ToHostData_ready_d3          : std_logic;
    signal ToHostData_ready_d4          : std_logic;
    signal ToHostData_ready_d5          : std_logic;
    signal rd_en_ToHostData             : std_logic;

    -- To-Host Monitoring --
    signal TH_FF_EMPTY            : std_logic_vector(0 downto 0);
    signal TH_FF_FULL             : std_logic_vector(0 downto 0);
    signal TH_FF_COUNT            : std_logic_vector(10 downto 0);

    --needed for LAr; TODO: this needs to be done inside the central router to avoid confusing all the other pieces of the design that use TTC
    signal ECR_BCR_SWAP           : std_logic;

    --signal cdrclk_en   : std_logic; --reduced the 160 MHz clock to the TTC-synchronous 40 MHz --not used

    signal Bch_timeout_cnt      : integer range 0 to 511 := 0;
    --signal local_ttc_clk        : std_logic; -- local TTC clock
    --signal TTC_EMU_SEL          : std_logic; --clk domain

    --monitoring counters
    signal ECR_counter      :  std_logic_vector(31 downto 0) := x"00000000";
    signal TTYPE_counter        :  std_logic_vector(31 downto 0) := x"00000000";
    signal BCR_MISMATCH_counter :  std_logic_vector(31 downto 0) := x"00000000";

    signal ECR_cntr_reset_0, ECR_cntr_reset_1 : std_logic;
    signal TTYPE_cntr_reset_0, TTYPE_cntr_reset_1 : std_logic;
    signal  BCR_MISMATCH_cntr_reset_0, BCR_MISMATCH_cntr_reset_1 : std_logic;
    signal BCR_period                         : std_logic_vector(11 downto 0):= x"000";

    signal TTC_BCR_COUNTER :  std_logic_vector(31 downto 0) := x"00000000"; --counts BCR pulses
    signal TTC_BCR_COUNTER_reset :  std_logic := '0'; --retimed reset from the register. rising edge is used to reset the counter
    signal TTC_BCR_COUNTER_reset_d :  std_logic := '0';

    signal TTC_DELAY  : std_logic_vector(3 downto 0);
    signal ExtendedTestPulse : std_logic;


    attribute IOB : string;
    attribute IOB of xcdrdata_in: signal is "TRUE";
    attribute ASYNC_REG : string;
    attribute ASYNC_REG of TTC_Out_sync: signal is "TRUE";
    attribute ASYNC_REG of busy_sync: signal is "TRUE";

    signal div_nrst_ttcdec_ttc_clk_gated, div_rst_ttcdec_ttc_clk_gated: std_logic;
    signal single_bit_error_ttcdec_ttc_clk_gated : std_logic;
    signal double_bit_error_ttcdec_ttc_clk_gated : std_logic;
    signal communication_error_ttcdec_ttc_clk_gated : std_logic;
    signal ready_ttc_clk_gated : std_logic;

    --Setting USE_FIFO_FOR_CDC to true instantiates a FIFO to sync between ttc domain and clk40, adds 6 cycles. false uses xpm_cdc, adds 2 cycles.
    constant USE_FIFO_FOR_CDC: boolean := false;

    --Signals connected to cdc_fifo_din
    signal l1a_dec_ttcdec_ttc_clk_gated : std_logic;
    signal channelB_ttcdec_ttc_clk_gated : std_logic;
    signal add_strobe_ttcdec_ttc_clk_gated : std_logic;
    signal brc_t2_ttcdec_ttc_clk_gated : std_logic_vector(1 downto 0);
    signal brc_d4_ttcdec_ttc_clk_gated : std_logic_vector(3 downto 0);
    signal brc_ei_ttcdec_ttc_clk_gated : std_logic;
    signal brc_bi_ttcdec_ttc_clk_gated : std_logic;
    signal add_e_ttcdec_ttc_clk_gated : std_logic;
    signal add_s8_ttcdec_ttc_clk_gated : std_logic_vector(7 downto 0);
    signal add_d8_ttcdec_ttc_clk_gated : std_logic_vector(7 downto 0);

    signal cdc_fifo_din, cdc_fifo_dout: std_logic_vector(27 downto 0);
    signal cdc_fifo_wr_en : std_logic;
    signal cdc_fifo_full : std_logic;
    signal cdc_fifo_rd_en : std_logic;
    signal cdc_fifo_empty : std_logic;
    signal Fifo_ToHostData_reclock_rd_en : std_logic;
    signal Fifo_ToHostData_reclock_empty : std_logic;

    signal testbeam_trigger_i : std_logic := '0';
    signal tb_trigger_sync_i : std_logic := '0';    --trigger pulse registered at clk40
    signal count_clk40 : std_logic_vector(3 downto 0) := (others => '0');

    signal l0a_shiftreg: std_logic_vector(63 downto 0);

    type tb_state_type is (idle, stretch);


    signal tb_state: tb_state_type := idle;--better to have initial value
    signal AsyncUserData : std_logic_vector(63 downto 0);
    signal AsyncUserData_wr_en : std_logic;
    signal AsyncUserData_full : std_logic;
    signal TTC_ASYNCUSERDATA_WR_EN_p1 : std_logic;
    signal AsyncUserData_rd_en : std_logic;
    signal AsyncUserData_empty : std_logic;
    signal AsyncUserData_dout : std_logic_vector(63 downto 0);
    signal TTC_out_p1 : TTC_data_type;

--  COMPONENT ila_testbeam

--  PORT (
--      clk : IN STD_LOGIC;
--      probe0 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--      probe1 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--      probe2 : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
--      probe3 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--      probe4 : in STD_LOGIC_VECTOR(3 downto 0)


--  );
--  END COMPONENT  ;

----

--signal tb_state_ila : std_logic;
--signal second_trigger_delay_ila : std_logic_vector(3 downto 0) := (others => '0');
begin

    --=====================--
    -- ttc wrapper control --
    --=====================--

    rst_TTCtoHost   <= to_sl(register_map_control.TTC_DEC_CTRL.TOHOST_RST) or (not div_nrst);

    BCID_reg        <= register_map_control.TTC_DEC_CTRL.BCID_ONBCR; -- BCID is set to this when reset
    XL1ID_SW        <= register_map_control.TTC_DEC_CTRL.XL1ID_SW;  --XL1ID is set to this value when reset

    register_map_ttc_monitor.TTC_DEC_MON.TTC_BIT_ERR <= TTC_BIT_ERR_REG;

    register_map_ttc_monitor.TTC_DEC_MON.TH_FF_FULL  <= TH_FF_FULL;
    register_map_ttc_monitor.TTC_DEC_MON.TH_FF_EMPTY <= TH_FF_EMPTY;
    register_map_ttc_monitor.TTC_DEC_MON.TH_FF_COUNT <= TH_FF_COUNT;

    g_links: for i in 0 to 23 generate
        register_map_ttc_monitor.TTC_BUSY_ACCEPTED(i) <= TTC_BUSY_mon_array(i);
    end generate;
    register_map_ttc_monitor.TTC_DEC_CTRL.BUSY_OUTPUT_STATUS <= (others => BUSY_IN);

    --L1A, ECR and TType, and a check if the BCR period is exactly 3564 BC.
    --
    --   L1A counter can be the value of L1ID + XL1ID as sent to the TTC ToHost.
    --   ECR can be a 32b counter
    --   TType can be a 32b counter (was implemented before but seems commented out)
    --
    --The BCR periodicity should be a check whether a BCR matches a period of 3564 BC, every time there
    --is a mismatch, a second counter should be incremented by one and added to a 32b monitor register.
    register_map_ttc_monitor.TTC_L1ID_MONITOR <= XL1ID & L1ID; --32b
    register_map_ttc_monitor.TTC_ECR_MONITOR.VALUE <= ECR_counter;  --32b  Counts the number of ECRs received from the TTC system
    register_map_ttc_monitor.TTC_TTYPE_MONITOR.VALUE <= TTYPE_counter; --32b Counts the number of TType received from the TTC system
    register_map_ttc_monitor.TTC_BCR_PERIODICITY_MONITOR.VALUE <= BCR_MISMATCH_counter; --32b Counts the number of times the BCR period does not match 3564
    register_map_ttc_monitor.TTC_BCR_COUNTER.VALUE <= TTC_BCR_COUNTER; --32b counter for BCRs from TTC

    ToHostFFMon: process (clk40)
    begin
        if (clk40'event and clk40='1') then

            if (register_map_control.TTC_DEC_CTRL.TOHOST_RST = "1") then --synchronous reset. comes with clk40
                TH_FF_FULL(0) <= '0';
                TH_FF_EMPTY(0) <= '1';
                TH_FF_COUNT <= (others=>'0');
            else
                if (ToHostData_full = '1') then
                    TH_FF_FULL(0) <= '1';
                end if;
                TH_FF_COUNT    <= ToHostData_count;
                TH_FF_EMPTY(0) <= ToHostData_empty;
            end if;

        end if;
    end process;


    --ECR counter for monitoring
    ECR_CNT: process (clk40)
    begin
        if (rising_edge(clk40)) then --40 MHz clock
            ECR_cntr_reset_0 <= to_sl(register_map_control.TTC_ECR_MONITOR.CLEAR);
            ECR_cntr_reset_1 <= ECR_cntr_reset_0;

            if (ECR_cntr_reset_0 = '1' and ECR_cntr_reset_1 = '0') then
                ECR_counter <= (others => '0');
            elsif (brc_e_40 = '1') then
                ECR_counter <= ECR_counter + 1;
            end if;
        end if;
    end process;

    --Trigger TYPE counter for monitoring
    TTYPE_CNT: process (clk40)
    begin
        if (rising_edge(clk40)) then --40 MHz clock
            TTYPE_cntr_reset_0 <= to_sl(register_map_control.TTC_TTYPE_MONITOR.CLEAR);
            TTYPE_cntr_reset_1 <= TTYPE_cntr_reset_0;

            if (TTYPE_cntr_reset_0 = '1' and TTYPE_cntr_reset_1 = '0') then
                TTYPE_counter <= (others => '0');
            elsif ( add_strobe_40 = '1' and add_e_40 = '1' and add_s8_40 = x"00") then --data from long b-channel commands
                TTYPE_counter <= TTYPE_counter + 1;
            end if;
        end if;
    end process;


    --BCR PERIODICITY MONITOR
    -- Counts the number of times the BCR period does not match 3564
    BCR_ERR_CNT: process (clk40)
    begin
        if (rising_edge(clk40)) then --40 MHz clock
            BCR_MISMATCH_cntr_reset_0 <= to_sl(register_map_control.TTC_BCR_PERIODICITY_MONITOR.CLEAR);
            BCR_MISMATCH_cntr_reset_1 <= BCR_MISMATCH_cntr_reset_0;


            if (BCR_MISMATCH_cntr_reset_1 = '0' and BCR_MISMATCH_cntr_reset_0 = '1') then
                BCR_MISMATCH_counter <= (others=>'0');

                --need to update the period counter
                if(brc_b_40 = '1') then
                    BCR_period <= (others=>'0');
                elsif (BCR_period /= "111111111111") then --Do not roll over the couter
                    BCR_period <= BCR_period + 1;
                end if;

            elsif (brc_b_40 = '1') then --check if BCR is arriving on time
                BCR_period <= (others=>'0');
                if (BCR_period /= "110111101011") then  --3564 is the number of buckets in an LHC orbit.
                    BCR_MISMATCH_counter <= BCR_MISMATCH_counter + 1;
                end if;
            elsif (BCR_period /= "111111111111") then --Do not roll over the couter
                BCR_period <= BCR_period + 1;
            else --BCR is not arriving at ALL
                if(BCR_MISMATCH_counter /= x"FFFFFFFF") then
                    BCR_MISMATCH_counter <= BCR_MISMATCH_counter + 1;
                end if;
            end if;


        end if;
    end process;




    --BCR  MONITOR
    -- Counts the number of BCR pulses from TTC
    BCR_CNT: process (clk40)
    begin
        if (rising_edge(clk40)) then --40 MHz clock
            TTC_BCR_COUNTER_reset <= to_sl(register_map_control.TTC_BCR_COUNTER.CLEAR);
            TTC_BCR_COUNTER_reset_d <= TTC_BCR_COUNTER_reset;

            if (TTC_BCR_COUNTER_reset_d = '0' and TTC_BCR_COUNTER_reset = '1') then -- the counter is reset on the rising edge of the register
                TTC_BCR_COUNTER <= (others=>'0');
            elsif (brc_b_40 = '1') then --check if BCR is arriving on time
                TTC_BCR_COUNTER <= TTC_BCR_COUNTER + 1; --the resigter will overflow.
            end if;

        end if; --clock
    end process;




    --TTC_EMU_SEL <= '0' when register_map_control.TTC_EMU.SEL = "0" else '1';

    ----local TTC clock
    --BUFGMUX_ttc_local: BUFGMUX
    --generic map(
    --  CLK_SEL_TYPE=>"SYNC"
    --)
    --port map(
    --  O       => local_ttc_clk, --40 MHz clock for the local TTC logic
    --  I0      => ttc_clk_gated, --40 MHz clock derived from the TTC data
    --  I1      => clk40, --emulator clock
    --  S       => TTC_EMU_SEL--1-bitinput:Clockselect
    --);

    --L1A throttle is in sync with TTC
    retiming: process(clk40)
    begin
        if (rising_edge(clk40)) then
            rst_TTCtoHost_40 <= rst_TTCtoHost;
            XL1ID_RST       <= to_sl(register_map_control.TTC_DEC_CTRL.XL1ID_RST);
            TT_Bch_En_Reg   <= to_sl(register_map_control.TTC_DEC_CTRL.TT_BCH_EN); -- The trigger ID commands are now also supported by the emulator
        end if;
    end process;

    --======================--
    --- Trigger Tag for ITk
    --======================--
    tag_gen_enable: if ITK_TRIGTAG_MODE >= 0 generate
        tag_gen: entity work.itk_trigtag_generator
            generic map (
                MODE => ITK_TRIGTAG_MODE
            )
            port map (
                clk => clk40,
                rst => rst_TTCtoHost_40,
                enc_trig => itk_trig,
                enc_sync => itk_sync,
                enc_tag => itk_tag,
                ttc2h_tag => itk_ttc2h_tag,
                ttc2h_tag_valid => open,
                ttc_l0a => l1a_40,
                ttc_bcr => brc_b_40,
                ttc_phase => "00"           -- TODO: connect to something meaningful in the regbank
            );
    end generate;

    tag_gen_disable: if ITK_TRIGTAG_MODE < 0 generate
        itk_trig <= "0000";
        itk_sync <= '0';
        itk_tag <= (others => '0');
        itk_ttc2h_tag <= (others => '0');
    end generate;


    --==============--
    -- To-Host Data --
    --==============--
    ToHostData_in <= '0' &  L0ID & reserved1 & orbit & L1ID & XL1ID & BCID & reserved0 & LEN & FMT;
    --                         32       16        32      24      8      12       4         8     8   =  144 bits = 20 bytes
    L0ID <= XL1ID & L1ID;


    -- used when TT_Bch_En_Reg = '0'
    ToHostData_ready <= l1a_40 when (rst_TTCtoHost_40 = '0') else '0';

    --We are writing into the FIFO on every L1A when TT_Bch_En_Reg='1')
    wr_en_ToHostData <= l1a_40 when TT_Bch_En_Reg='1' else '0';

    -- we have to delay the ToHostData and write-enables by four cycles to synchronize with the output of the trigger tag
    ToHostData_in_d1 <= ToHostData_in when rising_edge(clk40);
    ToHostData_in_d2 <= ToHostData_in_d1 when rising_edge(clk40);
    ToHostData_in_d3 <= ToHostData_in_d2 when rising_edge(clk40);
    ToHostData_in_d4 <= ToHostData_in_d3 when rising_edge(clk40);
    ToHostData_in_d5 <= ToHostData_in_d4 when rising_edge(clk40);
    ToHostData_ready_d1 <= ToHostData_ready when rising_edge(clk40);
    ToHostData_ready_d2 <= ToHostData_ready_d1 when rising_edge(clk40);
    ToHostData_ready_d3 <= ToHostData_ready_d2 when rising_edge(clk40);
    ToHostData_ready_d4 <= ToHostData_ready_d3 when rising_edge(clk40);
    ToHostData_ready_d5 <= ToHostData_ready_d4 when rising_edge(clk40);
    wr_en_ToHostData_d1 <= wr_en_ToHostData when rising_edge(clk40);
    wr_en_ToHostData_d2 <= wr_en_ToHostData_d1 when rising_edge(clk40);
    wr_en_ToHostData_d3 <= wr_en_ToHostData_d2 when rising_edge(clk40);
    wr_en_ToHostData_d4 <= wr_en_ToHostData_d3 when rising_edge(clk40);
    wr_en_ToHostData_d5 <= wr_en_ToHostData_d4 when rising_edge(clk40);


    --The counter is synchronous with the TTC clock
    BCID_gen: process (clk40)
    begin
        if (rising_edge(clk40)) then
            brc_b_40 <= brc_b;

            if (rst_TTCtoHost_40 = '1') then
                BCID <= (others=>'0');
            else
                if (BCID = "110111101011" and brc_b_40 = '0') then  --3564 is the number of buckets in an LHC orbit.
                    BCID <= (others=>'0');    --reset the orbit counter if needed
                elsif (brc_b_40 = '1') then
                    BCID <= BCID_reg;
                else
                    BCID <= BCID + 1;
                end if;
            end if;

        end if;
    end process;


    --Orbit counter is reset whel BCID rolls through 0. The orgibt counter is also incremented when BCID rolls through 0.
    --The counter is synchronous with the TTC clock
    --"OCR means reset the orbit counter on the next rollover of the BC counter"
    Orbit_gen: process (clk40)
    begin
        if (rising_edge(clk40)) then

            brcst7_40 <= brc_t2(1);

            --"A  double-width BCR is scheduled by sensing Brcst7 to indicate that the next BCR should be double-width."
            if(brcst7_40 = '1' and brc_b_40 = '1') then
                dbw_bcr <= '0';
                orbit_reset_latch <= '1';
            elsif (brcst7_40 = '1' and brc_b_40 = '0') then
                dbw_bcr <= '1';
            elsif (dbw_bcr = '1' and brc_b_40 = '1') then
                dbw_bcr <= '0';
                orbit_reset_latch <= '1';
            end if;


            if (rst_TTCtoHost_40 = '1') then
                orbit <= (others=>'0');
            else
                if (BCID = "110111101011") then -- 3564 is the number of buckets in an LHC orbit.
                    --reset the orbit counter if needed or increment

                    if(orbit_reset_latch = '1') then
                        orbit <= (others=>'0');
                        orbit_reset_latch <= '0';
                    else
                        orbit <= orbit + 1;
                    end if;

                end if;
            end if;

        end if;
    end process;


    --All the counters are using the TTC clock
    --XL1ID incermented in ECR and cleared with a PCIe register
    --ECR and BCR are transmitted in short broadcast commands.
    XL1ID_gen: process (clk40)
    begin
        if (rising_edge(clk40)) then
            XL1ID_RST_latch <= XL1ID_RST;
            brc_e_40 <= brc_e;

            if (rst_TTCtoHost_40 = '1') then -- synchronous reset
                XL1ID <= XL1ID_SW;
            else

                if (XL1ID_RST = '1' and XL1ID_RST_latch = '0' ) then  -- XL1ID reset
                    XL1ID     <= XL1ID_SW;
                elsif (brc_e_40 = '1') then --XL1ID incermented in ECR and cleared with a PCIe register
                    XL1ID <= XL1ID + 1;
                end if;

            end if;
        end if;
    end process;



    --The L1ID counter is in the TTC clock domain
    L1ID_gen: process (clk40)
    begin
        if (rising_edge(clk40)) then
            --if (cdrclk_en = '1') then --enable

            l1a_40 <= l1a or testbeam_trigger_i;

            brc_strobe_40 <= brc_strobe;
            brc_t2_d4_40 <= brc_t2 & brc_d4;
            --brc_d4_40 <= brc_d4;

            add_strobe_40 <= add_strobe;
            add_e_40 <= add_e;
            add_s8_40 <= add_s8;
            add_d8_40 <= add_d8;
            add_a14_40 <= add_a14;

            if (rst_TTCtoHost_40='1') then --synchronous reset
                L1ID <= (others=>'0');
                rd_en_ToHostData <= '0';
                Bch_timeout_cnt <= 0;
                Brcst_latched <= "000000";

            else

                if (brc_e_40 = '1') then --L1ID is cleared on ECRs and incremented on L1ID
                    L1ID  <= (others=>'0');
                elsif (l1a_40 = '1') then --L1ID counter
                    L1ID  <= L1ID + 1;
                end if;

                --setting the latched bits for the NSW
                --the address is required to be 0 and the sub-address is  0x04
                --E=0 accesses TTCrx internal registers. E=1 is for access to external subaddresses in the associated front-end electronics
                if ( add_strobe_40 = '1' and add_e_40 = '1' and add_s8_40 = x"04" and add_a14_40 = "00000000000000") then
                    if(add_d8_40(6) = '1') then
                        Brcst_latched <= Brcst_latched or add_d8_40(5 downto 0);
                    else
                        Brcst_latched <= Brcst_latched and (not add_d8_40(5 downto 0));
                    end if;
                elsif (brc_strobe_40 = '1') then
                    Brcst_latched <= Brcst_latched xor brc_t2_d4_40;
                end if;


                --From the TTCvi documentation:
                --After each L1A is transmitted, the contents of the 24-bit event/orbit counter isn the TTCvi is broadcast together
                --with an 8-bit trigger type parameter, which is received from the Central Trigger Processor via a front panel connection.
                --This broadcast, which is intended for check purposes, is made asynchronously and takes about 4.4 us if the B Channel is free.
                --The sub-address 0 is used for the 8-bit trigger type.


                --I am using a FWFT fifo
                --rd_en_ToHostData_align <= rd_en_ToHostData; --data from FIFO shows up on the next clock cycle
                --trigger_type_align <= trigger_type;

                if (TT_Bch_En_Reg='1') then  --Outputing the long b-channel trigger type together with the trigger and the counters

                    -- We read from the FIFO on timeout or when we get trigger type from the B-channel (long command)
                    if (ToHostData_empty = '0') then  --I am using a FWFT fifo
                        if ( add_strobe_40 = '1' and add_e_40 = '1' and add_s8_40 = x"00") then --data from long b-channel commands
                            Bch_timeout_cnt <= 0;
                            trigger_type(7 downto 0) <= add_d8_40;
                            rd_en_ToHostData <= '1';
                        elsif (Bch_timeout_cnt = 200) then --timeout
                            Bch_timeout_cnt <= 0;
                            trigger_type(7 downto 0) <= x"00"; --trigger type is not assigned
                            rd_en_ToHostData <= '1';
                        else --counting timeout when the fifo is not empty
                            Bch_timeout_cnt <= Bch_timeout_cnt +1;
                            rd_en_ToHostData <= '0';
                        end if;
                    else
                        rd_en_ToHostData <= '0';
                        Bch_timeout_cnt <= 0;
                    end if;

                end if;

            end if; --reset
        end if; --clock
    end process;

    ToHostData_in_fifo <= itk_ttc2h_tag & ToHostData_in_d5;


    Fifo_ToHostData : xpm_fifo_sync
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            FIFO_MEMORY_TYPE => "auto",
            FIFO_READ_LATENCY => 1,
            FIFO_WRITE_DEPTH => 1024,
            FULL_RESET_VALUE => 1,
            PROG_EMPTY_THRESH => 10,
            PROG_FULL_THRESH => 1019,
            RD_DATA_COUNT_WIDTH => 11,
            READ_DATA_WIDTH => 153,
            READ_MODE => "fwft",
            SIM_ASSERT_CHK => 0,
            USE_ADV_FEATURES => "0707",
            WAKEUP_TIME => 0,
            WRITE_DATA_WIDTH => 153,
            WR_DATA_COUNT_WIDTH => 11
        )
        port map (
            sleep => '0',
            rst => rst_TTCtoHost_40,
            wr_clk => clk40,
            wr_en => wr_en_ToHostData_d5,
            din => ToHostData_in_fifo,
            full => ToHostData_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_en => rd_en_ToHostData,
            dout => ToHostData_out,
            empty => ToHostData_empty,
            prog_empty => open,
            rd_data_count => ToHostData_count,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );


    --output TTC_ToHost_Data_out is retimed to the local 40 MHz clock domain
    data_mux: process (TT_Bch_En_Reg, ToHostData_out, trigger_type, rd_en_ToHostData, ToHostData_ready_d5, itk_ttc2h_tag, ToHostData_in_d5)
    begin
        if(TT_Bch_En_Reg = '1') then -- the data is taken from a FIFO if we are using B-channel to received the trigger type

            ToHostData_tmp(7 downto 0)        <= ToHostData_out(7 downto 0);
            ToHostData_tmp(15 downto 8)       <= ToHostData_out(15 downto 8);
            ToHostData_tmp(19 downto 16)      <= ToHostData_out(19 downto 16);
            ToHostData_tmp(31 downto 20)      <= ToHostData_out(31 downto 20);
            ToHostData_tmp(39 downto 32)      <= ToHostData_out(39 downto 32);
            ToHostData_tmp(63 downto 40)      <= ToHostData_out(63 downto 40);
            ToHostData_tmp(95 downto 64)      <= ToHostData_out(95 downto 64);
            ToHostData_tmp(111 downto 96)     <= trigger_type;
            ToHostData_tmp(127 downto 112)    <= ToHostData_out(111 downto 96);
            ToHostData_tmp(159 downto 128)    <= ToHostData_out(143 downto 112);
            ToHostData_tmp(167 downto 160)    <= ToHostData_out(152 downto 145);
            ToHostData_tmp(168)               <= rd_en_ToHostData;

        --          TTC_ToHost_Data_out.FMT             <= ToHostData_out(7 downto 0);
        --          TTC_ToHost_Data_out.LEN             <= ToHostData_out(15 downto 8);
        --          TTC_ToHost_Data_out.reserved0       <= ToHostData_out(19 downto 16);
        --          TTC_ToHost_Data_out.BCID            <= ToHostData_out(31 downto 20);
        --          TTC_ToHost_Data_out.XL1ID           <= ToHostData_out(39 downto 32);
        --          TTC_ToHost_Data_out.L1ID            <= ToHostData_out(63 downto 40);
        --          TTC_ToHost_Data_out.orbit           <= ToHostData_out(95 downto 64);
        --          TTC_ToHost_Data_out.trigger_type    <= trigger_type; --_align;
        --          TTC_ToHost_Data_out.reserved1       <= ToHostData_out(111 downto 96);
        --          TTC_ToHost_Data_out.L0ID            <= ToHostData_out(143 downto 112);
        --          TTC_ToHost_Data_out.data_rdy        <= rd_en_ToHostData;--_align;

        else  --the fifo is bypassed if the B-channel is not used

            ToHostData_tmp(7 downto 0)        <= ToHostData_in_d5(7 downto 0);
            ToHostData_tmp(15 downto 8)       <= ToHostData_in_d5(15 downto 8);
            ToHostData_tmp(19 downto 16)      <= ToHostData_in_d5(19 downto 16);
            ToHostData_tmp(31 downto 20)      <= ToHostData_in_d5(31 downto 20);
            ToHostData_tmp(39 downto 32)      <= ToHostData_in_d5(39 downto 32);
            ToHostData_tmp(63 downto 40)      <= ToHostData_in_d5(63 downto 40);
            ToHostData_tmp(95 downto 64)      <= ToHostData_in_d5(95 downto 64);
            ToHostData_tmp(111 downto 96)     <= x"0000";
            ToHostData_tmp(127 downto 112)    <= ToHostData_in_d5(111 downto 96);
            ToHostData_tmp(159 downto 128)    <= ToHostData_in_d5(143 downto 112);
            ToHostData_tmp(167 downto 160)    <= itk_ttc2h_tag;
            ToHostData_tmp(168)               <= ToHostData_ready_d5; --wr_en_ToHostData;

        --          TTC_ToHost_Data_out.FMT             <= ToHostData_in(7 downto 0);
        --          TTC_ToHost_Data_out.LEN             <= ToHostData_in(15 downto 8);
        --          TTC_ToHost_Data_out.reserved0       <= ToHostData_in(19 downto 16);
        --          TTC_ToHost_Data_out.BCID            <= ToHostData_in(31 downto 20);
        --          TTC_ToHost_Data_out.XL1ID           <= ToHostData_in(39 downto 32);
        --          TTC_ToHost_Data_out.L1ID            <= ToHostData_in(63 downto 40);
        --          TTC_ToHost_Data_out.orbit           <= ToHostData_in(95 downto 64);
        --          TTC_ToHost_Data_out.trigger_type    <= x"0000";
        --          TTC_ToHost_Data_out.reserved1       <= ToHostData_in(111 downto 96);
        --          TTC_ToHost_Data_out.L0ID            <= ToHostData_in(143 downto 112);
        --          TTC_ToHost_Data_out.data_rdy        <= wr_en_ToHostData;
        end if;
    end process;


    --A FIFO to pass TTC data to central router runing from the local oscillator clocl

    Fifo_ToHostData_reclock : xpm_fifo_async
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT"
            CDC_SYNC_STAGES => 2,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            FIFO_MEMORY_TYPE => "auto",
            FIFO_READ_LATENCY => 1,
            FIFO_WRITE_DEPTH => 16,
            FULL_RESET_VALUE => 0,
            PROG_EMPTY_THRESH => 6,
            PROG_FULL_THRESH => 10,
            RD_DATA_COUNT_WIDTH => 1,
            READ_DATA_WIDTH => 168,
            READ_MODE => "fwft",
            RELATED_CLOCKS => 0,
            SIM_ASSERT_CHK => 0,
            USE_ADV_FEATURES => "1000",
            WAKEUP_TIME => 0,
            WRITE_DATA_WIDTH => 168,
            WR_DATA_COUNT_WIDTH => 1
        )
        port map (
            sleep => '0',
            rst => rst_TTCtoHost,
            wr_clk => clk40,
            wr_en => ToHostData_tmp(168),
            din => ToHostData_tmp(167 downto 0),
            full => open,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => clk40,
            rd_en => Fifo_ToHostData_reclock_rd_en,
            dout => ToHostData_fifo(167 downto 0),
            empty => Fifo_ToHostData_reclock_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => ToHostData_fifo(168),
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    Fifo_ToHostData_reclock_rd_en <= not Fifo_ToHostData_reclock_empty;


    TTC_ToHost_Data_out.FMT             <= ToHostData_fifo(7 downto 0);
    TTC_ToHost_Data_out.LEN             <= ToHostData_fifo(15 downto 8);
    TTC_ToHost_Data_out.reserved0       <= ToHostData_fifo(19 downto 16);
    TTC_ToHost_Data_out.BCID            <= ToHostData_fifo(31 downto 20);
    TTC_ToHost_Data_out.XL1ID           <= ToHostData_fifo(39 downto 32);
    TTC_ToHost_Data_out.L1ID            <= ToHostData_fifo(63 downto 40);
    TTC_ToHost_Data_out.orbit           <= ToHostData_fifo(95 downto 64);
    TTC_ToHost_Data_out.trigger_type    <= ToHostData_fifo(111 downto 96);
    TTC_ToHost_Data_out.reserved1       <= ToHostData_fifo(127 downto 112);
    TTC_ToHost_Data_out.L0ID            <= ToHostData_fifo(159 downto 128);
    TTC_ToHost_Data_out.TAG             <= ToHostData_fifo(167 downto 160);
    TTC_ToHost_Data_out.data_rdy        <= ToHostData_fifo(168); --TODO: valid


    --  --this is counting ECRs as errors
    --  L1ID_check: process (clk40)
    --      begin

    --        if (clk40'event and clk40='1') then

    --          if(rst_TTCtoHost='1') then
    --                 L1ID_cnt     <= x"000000";
    --                 L1ID_prev    <= x"000000";
    --                 L1ID_err_cnt <= x"000000";
    --                 L1ID_err     <= '0';
    --           else
    --                 L1ID_prev <= ToHostData_out(63 downto 40);
    --                 L1ID_err  <= '0';
    --                 if(rd_en_ToHostData_align = '1' and L1ID_prev+1 /= ToHostData_out(63 downto 40)) then --This will not work then L1ID is cleared with an ECR
    --                      L1ID_err_cnt <= L1ID_err_cnt + 1;
    --                      L1ID_err <= '1';
    --                 end if;
    --                 if(rd_en_ToHostData_align = '1' and L1ID_prev+1 = ToHostData_out(63 downto 40)) then
    --                      L1ID_cnt <= L1ID_cnt + 1;
    --                 end if;
    --            end if; --reset
    --        end if; --clock
    --      end process;

    --L1ID_err_cnt_o <= L1ID_err_cnt;
    --L1ID_cnt_o     <= L1ID_cnt;
    --L1ID_prev_o    <= L1ID_prev;
    --L1ID_err_o     <= L1ID_err;


    --=====================================--
    -- ttc decoder --
    --=====================================--
    g_NUM_TTC_INPUTS: for i in 0 to NUM_TTC_INPUTS(CARD_TYPE)-1 generate
        signal cdrclk_buf: std_logic;
    begin
        ibuf_ttc_clk : ibufds -- @suppress "Generic map uses default values. Missing optional actuals: CAPACITANCE, DIFF_TERM, DQS_BIAS, IBUF_DELAY_VALUE, IBUF_LOW_PWR, IFD_DELAY_VALUE, IOSTANDARD"
            port map (I=> CLK_TTC_P(i), IB=> CLK_TTC_N(i), O=> cdrclk_buf); -- @suppress "The order of the associations is different from the declaration order"

        bufg_ttc_clk : bufg
            port map (I=> cdrclk_buf, O => cdrclk_in); -- @suppress "The order of the associations is different from the declaration order"

        -- ttc data
        ibuf_ttc_data : ibufds  -- @suppress
            port map (
                O  => xcdrdata_in,
                I  => DATA_TTC_P(i),
                IB => DATA_TTC_N(i)
            );

        cdrclk_in_locked <= not (LOS_ADN(i) or LOL_ADN(i));

        adn2814_monitor_proc: process(clk40)
        begin
            if rising_edge(clk40) then
                if register_map_control.TTC_CDRLOCK_MONITOR.CLEAR = "1" then
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOL_LATCHED <= "0";
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOS_LATCHED <= "0";
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.CDRLOCK_LOST <= "0";
                end if;
                if LOL_ADN(0) = '1' then
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOL_LATCHED <= "1";
                end if;
                if LOS_ADN(0) = '1' then
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOS_LATCHED <= "1";
                end if;
                if cdrlocked = '0' then
                    register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.CDRLOCK_LOST <= "1";
                end if;
                register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.CDRLOCKED <= (others => cdrlocked);
                register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOL <= (others => LOL_ADN(0));
                register_map_ttc_monitor.TTC_CDRLOCK_MONITOR.ADN_LOS <= (others => LOS_ADN(0));
            end if;
        end process;

    end generate;

    g_TB_TRIGGER_SYNC: if NUM_TB_TRIGGERS(CARD_TYPE) = 1 generate
        process(clk40)
        begin
            if rising_edge(clk40) then
                tb_trigger_sync_i <= TB_trigger(0);
            end if;
        end process;
    end generate;

    --SM to stretch trigger for 16 clk40 cycles. Assumption: separation between
    --trigger pulse > 16 clks
    testbeam_trig: if ISTESTBEAM  and CARD_TYPE = 712  generate
        process(clk40)
        begin
            if rising_edge(clk40) then
                case tb_state is
                    when idle =>
                        if tb_trigger_sync_i = '1' then
                            tb_state <= stretch;
                            --            count_clk40 <= count_clk40 + '1';
                            count_clk40 <= (others => '0');
                            testbeam_trigger_i <= '1';
                        else
                            tb_state <= idle;
                            count_clk40 <= (others => '0');
                            testbeam_trigger_i <= '0';
                        end if;
                    --stretch by 15 more clks
                    when stretch =>
                        if (tb_trigger_sync_i = '1') then
                            tb_state <= stretch;
                            count_clk40 <= (others => '0'); --reset counter if another trigger comes before the counter is maxed
                            testbeam_trigger_i <= '1';
                        elsif count_clk40 = X"F" then     --end stretching since no other trigger and counter is maxed
                            tb_state <= idle;
                            count_clk40 <= (others => '0');
                            testbeam_trigger_i <= '0';
                        else                              --keep stretching unntil counter is maxed
                            tb_state <= stretch;
                            count_clk40 <= count_clk40 + 1;
                            testbeam_trigger_i <= '1';
                        end if;
                end case; --the state machine
            end if; --clock
        end process;
    end generate;
    --tb_state_ila <= '0' when tb_state = idle
    --                else '1';

    --debugging
    --store delay of extra trigger which may arrive during the stretch of the
    --first one. Cannot be more than 15. Trigger in ila on delay >8?
    --process(clk40)
    --begin
    --  if rising_edge(clk40) then
    --    if (tb_state_ila = '1' and tb_trigger_sync_i = '1') then
    --      second_trigger_delay_ila <= count_clk40;
    --    end if;
    --  end if; --clock
    --end process;


    CDR_IN_LATCH : process (cdrclk_in)
    begin
        if (cdrclk_in'event AND (cdrclk_in='1')) then
            cdrdata_in <= xcdrdata_in;
        end if;
    end process;

    clk_adn_160 <= cdrclk_in;
    clk_ttc_40  <= ttc_clk_gated;


    TTC_Out_unsync(0)        <= testbeam_trigger_i or l1a;
    TTC_Out_unsync(1)        <= channelB;
    TTC_Out_unsync(2)        <= brc_b; --BCR
    TTC_Out_unsync(3)        <= brc_e; --ECR
    TTC_Out_unsync(4)        <= brc_d4(0);
    TTC_Out_unsync(5)        <= brc_d4(1);
    TTC_Out_unsync(6)        <= brc_d4(2);
    TTC_Out_unsync(7)        <= brc_d4(3);
    TTC_Out_unsync(8)        <= brc_t2(0);
    TTC_Out_unsync(9)        <= brc_t2(1);

    TTC_Out_unsync(15 downto 10) <= Brcst_latched;

    TTC_Out_unsync(16)           <= itk_sync;
    TTC_Out_unsync(20 downto 17) <= itk_trig;
    TTC_Out_unsync(27 downto 21) <= itk_tag;



    busy_unsync <= single_bit_error or double_bit_error or communication_error;

    sync: process(clk40)
    begin
        if rising_edge(clk40) then
            TTC_BIT_ERR_REG <= double_bit_error & single_bit_error & communication_error;
            --! Implement the behaviour of the TTC data switch. TTCToHost data still needs to be derived from the right source
            TTC_Out_sync(1 downto 0) <= TTC_Out_unsync(1 downto 0); --l1a and b-channel have constant delay
            TTC_Out_sync(27 downto 16) <= TTC_Out_unsync(27 downto 16); -- ITk signals also with constant delay
            busy_sync <= busy_unsync;
        end if;
    end process;

    BUSY    <= busy_sync;



    GEN_SHIFT:
  for I in 2 to 15 generate
        SRL16E_2 : SRL16E
            generic map (
                INIT => X"0000",        -- Initial contents of shift register
                IS_CLK_INVERTED => '0'  -- Optional inversion for CLK
            )
            port map (
                Q => TTC_out_delayed(I), -- 1-bit output: SRL Data
                -- Depth Selection inputs: A0-A3 select SRL depth
                --the minimal length of the shift register is 1 clock  cycle
                A0 => register_map_control.TTC_DEC_CTRL.B_CHAN_DELAY(27),
                A1 => register_map_control.TTC_DEC_CTRL.B_CHAN_DELAY(28),
                A2 => register_map_control.TTC_DEC_CTRL.B_CHAN_DELAY(29),
                A3 => register_map_control.TTC_DEC_CTRL.B_CHAN_DELAY(30),
                CE => '1', -- 1-bit input: Clock enable
                CLK => clk40, -- 1-bit input: Clock
                D => TTC_Out_unsync(I) -- 1-bit input: SRL Data

            );
    end generate GEN_SHIFT;





    --ECR and BCR are swapped in the LAr TTC system
    ECR_BCR_SWAP <= to_sl(register_map_control.TTC_DEC_CTRL.ECR_BCR_SWAP);
    brc_e <= brc_ei when (ECR_BCR_SWAP = '0') else brc_bi; --ECR by default
    brc_b <= brc_bi when (ECR_BCR_SWAP = '0') else brc_ei; --BCR by default

    --=====================================--
    ttc_dec: entity work.ttc_decoder_core
        --=====================================--
        port map
(
            --RESET_N                         => RESET_N_s,
            --== cdr interface ==--
            cdrclk_in_locked => cdrclk_in_locked,
            cdrclk_in => cdrclk_in,
            cdrdata_in => cdrdata_in,
            --== ttc decoder output ==--
            single_bit_error => single_bit_error_ttcdec_ttc_clk_gated,
            double_bit_error => double_bit_error_ttcdec_ttc_clk_gated,
            communication_error => communication_error_ttcdec_ttc_clk_gated,
            l1a => l1a_dec_ttcdec_ttc_clk_gated,
            channelB_o => channelB_ttcdec_ttc_clk_gated,
            brc_strobe => open, --brc_strobe_ttcdec, --not used
            add_strobe => add_strobe_ttcdec_ttc_clk_gated, --used for extracting the trigger ID from the long b-channel commands
            --TTDDDDDEB
            brc_t2 => brc_t2_ttcdec_ttc_clk_gated, -- 2 bit
            brc_d4 => brc_d4_ttcdec_ttc_clk_gated, -- 4 bit
            brc_e => brc_ei_ttcdec_ttc_clk_gated, --ECR
            brc_b => brc_bi_ttcdec_ttc_clk_gated, --BCR
            --AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD
            add_a14 => open, --add_a14_ttcdec, --not used
            add_e => add_e_ttcdec_ttc_clk_gated, --used for extracting the trigger ID from the long b-channel commands
            add_s8 => add_s8_ttcdec_ttc_clk_gated, --8 bit used for extracting the trigger ID from the long b-channel commands
            add_d8 => add_d8_ttcdec_ttc_clk_gated, --8 bit used for extracting the trigger ID from the long b-channel commands
            --== ttc decoder aux flags ==--
            ready => ready_ttc_clk_gated, -- the ttc_clk_gated is present
            div_nrst => div_nrst_ttcdec_ttc_clk_gated, --1 when the TTC bistream is aligned and recognied. the TTC decoder core outputs garbage before that happens
            ttc_clk_gated => ttc_clk_gated --,
        --cdrclk_en                       => cdrclk_en
        );

    --Status and reset signals are synchronized using XPM_CDC
    xpm_cdc_single_bit_error: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => ttc_clk_gated,
            src_in => single_bit_error_ttcdec_ttc_clk_gated,
            dest_clk => clk40,
            dest_out => single_bit_error_ttcdec
        );

    xpm_cdc_double_bit_error: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => ttc_clk_gated,
            src_in => double_bit_error_ttcdec_ttc_clk_gated,
            dest_clk => clk40,
            dest_out => double_bit_error_ttcdec
        );

    xpm_cdc_communication_error: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => ttc_clk_gated,
            src_in => communication_error_ttcdec_ttc_clk_gated,
            dest_clk => clk40,
            dest_out => communication_error_ttcdec
        );

    xpm_cdc_ready: xpm_cdc_single generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        ) port map(
            src_clk => ttc_clk_gated,
            src_in => ready_ttc_clk_gated,
            dest_clk => clk40,
            dest_out => ready
        );

    xpm_cdc_div_nrst: xpm_cdc_sync_rst     generic map(
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map(
            src_rst => div_nrst_ttcdec_ttc_clk_gated,
            dest_clk => clk40,
            dest_rst => div_nrst_ttcdec
        );

    --Invert nrst for cdc_fifo reset.
    div_rst_ttcdec_ttc_clk_gated <= not div_nrst_ttcdec_ttc_clk_gated;

    cdc_fifo_din <= l1a_dec_ttcdec_ttc_clk_gated & --27
                    channelB_ttcdec_ttc_clk_gated & --26
                    add_strobe_ttcdec_ttc_clk_gated & --25
                    brc_t2_ttcdec_ttc_clk_gated & --24..23
                    brc_d4_ttcdec_ttc_clk_gated & --22..19
                    brc_ei_ttcdec_ttc_clk_gated & --18
                    brc_bi_ttcdec_ttc_clk_gated & --17
                    add_e_ttcdec_ttc_clk_gated & --16
                    add_s8_ttcdec_ttc_clk_gated & -- 15 .. 8
                    add_d8_ttcdec_ttc_clk_gated; --7 .. 0


    l1a_dec_ttcdec    <= cdc_fifo_dout(27);
    channelB_ttcdec   <= cdc_fifo_dout(26);
    add_strobe_ttcdec <= cdc_fifo_dout(25);
    brc_t2_ttcdec     <= cdc_fifo_dout(24 downto 23);
    brc_d4_ttcdec     <= cdc_fifo_dout(22 downto 19);
    brc_ei_ttcdec     <= cdc_fifo_dout(18);
    brc_bi_ttcdec     <= cdc_fifo_dout(17);
    add_e_ttcdec      <= cdc_fifo_dout(16);
    add_s8_ttcdec     <= cdc_fifo_dout(15 downto 8);
    add_d8_ttcdec     <= cdc_fifo_dout(7 downto 0);

    g_use_fifo: if USE_FIFO_FOR_CDC generate
        --Add a FIFO to synchronize all TTC decoder core outputs to clk40, status and error signals can use XPM_CDC.

        cdc_fifo_wr_en <= not cdc_fifo_full;
        cdc_fifo_rd_en <= not cdc_fifo_empty;

        --With related clocks set to 1, the FIFO adds 6 clock cycles of latency.
        cdc_fifo: xpm_fifo_async     generic map( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, ECC_MODE, SIM_ASSERT_CHK, WAKEUP_TIME"
                FIFO_MEMORY_TYPE => "distributed",
                FIFO_WRITE_DEPTH => 16,
                RELATED_CLOCKS => 1,
                WRITE_DATA_WIDTH => 28,
                READ_MODE => "fwft",
                FIFO_READ_LATENCY => 0,
                FULL_RESET_VALUE => 1,
                USE_ADV_FEATURES => "0000",
                READ_DATA_WIDTH => 28,
                CDC_SYNC_STAGES => 2,
                WR_DATA_COUNT_WIDTH => 1,
                PROG_FULL_THRESH => 10,
                RD_DATA_COUNT_WIDTH => 1,
                PROG_EMPTY_THRESH => 6,
                DOUT_RESET_VALUE => "0"
            )
            port map(
                sleep => '0',
                rst => div_rst_ttcdec_ttc_clk_gated,
                wr_clk => ttc_clk_gated,
                wr_en => cdc_fifo_wr_en,
                din => cdc_fifo_din,
                full => cdc_fifo_full,
                prog_full => open,
                wr_data_count => open,
                overflow => open,
                wr_rst_busy => open,
                almost_full => open,
                wr_ack => open,
                rd_clk => clk40,
                rd_en => cdc_fifo_rd_en,
                dout => cdc_fifo_dout,
                empty => cdc_fifo_empty,
                prog_empty => open,
                rd_data_count => open,
                underflow => open,
                rd_rst_busy => open,
                almost_empty => open,
                data_valid => open,
                injectsbiterr => '0',
                injectdbiterr => '0',
                sbiterr => open,
                dbiterr => open
            );

    end generate;

    g_use_cdc: if USE_FIFO_FOR_CDC = false generate
        --Instead of a FIFO, use the XPM_CDC_ARRAY_SINGLE, only adds 2 clocks of latency but may lose some L1A if clocks are not synchronous
        xpm_cdc_array_single_inst : xpm_cdc_array_single
            generic map (
                DEST_SYNC_FF => 2,
                INIT_SYNC_FF => 0,
                SIM_ASSERT_CHK => 0,
                SRC_INPUT_REG => 0,
                WIDTH => 28
            )
            port map (
                src_clk => ttc_clk_gated,
                src_in => cdc_fifo_din,
                dest_clk => clk40,
                dest_out => cdc_fifo_dout
            );
    end generate;

    cdrlocked_out <= ready;


    xmp_cdrlocked: xpm_cdc_single
        generic map (
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map (
            dest_out => cdrlocked,
            dest_clk => clk40,
            src_clk => '0',
            src_in => ready
        );

    --TTC emulator
    TTCEmu: entity work.TTC_Emulator
        port map(
            Clock => clk40,
            Reset => rst_TTCtoHost,
            register_map_control => register_map_control,
            add_s8 => TTCEmu_add_s8,
            add_d8 => TTCEMu_add_d8,
            add_strobe => TTCEmu_add_strobe,
            add_e => TTCEmu_add_e,
            TTCout => TTCEmu_TTCout, --      : out std_logic_vector(9 downto 0)
            BUSYIn => BUSY_IN
        );


    --switch between the emulated and TTC data
    ttc_mux: process(register_map_control.TTC_EMU.SEL, single_bit_error_ttcdec, double_bit_error_ttcdec, communication_error_ttcdec, l1a_dec_ttcdec, add_strobe_ttcdec,brc_t2_ttcdec, brc_d4_ttcdec, brc_ei_ttcdec, brc_bi_ttcdec, add_e_ttcdec, add_s8_ttcdec, add_d8_ttcdec, div_nrst_ttcdec, channelB_ttcdec, TTCEmu_TTCout, TTCEmu_add_s8, TTCEmu_add_strobe, TTCEmu_add_e, TTCEMu_add_d8)
    begin
        if (register_map_control.TTC_EMU.SEL = "0") then --signals from the TTC decoder
            single_bit_error <= single_bit_error_ttcdec;
            double_bit_error <= double_bit_error_ttcdec;
            communication_error <= communication_error_ttcdec;
            l1a <= l1a_dec_ttcdec;
            --l1a_dec <= l1a_dec_ttcdec;
            brc_strobe <= '0'; --brc_strobe_ttcdec; --not used
            add_strobe <= add_strobe_ttcdec; --used for extracting the trigger ID from the long b-channel commands
            --TTDDDDDEB
            brc_t2 <= brc_t2_ttcdec;
            brc_d4 <= brc_d4_ttcdec;
            brc_ei <= brc_ei_ttcdec; --ECR
            brc_bi <= brc_bi_ttcdec; --BCR
            --AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD
            add_a14 <= (others => '0'); --add_a14_ttcdec; --not used
            add_e <= add_e_ttcdec;  --used for extracting the trigger ID from the long b-channel commands
            add_s8 <= add_s8_ttcdec; --used for extracting the trigger ID from the long b-channel commands
            add_d8 <= add_d8_ttcdec; --used for extracting the trigger ID from the long b-channel commands
            --== ttc decoder aux flags ==--
            --ready <= ready_ttcdec;
            div_nrst <= div_nrst_ttcdec; --1 when the TTC bistream is aligned and recognied. the TTC decoder core outputs garbage before that happens
            channelB <= channelB_ttcdec;

        else --emulated signals ---------------------------------------------------------------
            single_bit_error <= '0';
            double_bit_error <= '0';
            communication_error <= '0';
            l1a <= TTCEmu_TTCout(0);
            --l1a_dec <= TTCEmu_TTCout(0);
            brc_strobe <= '0'; --not used
            add_strobe <= TTCEmu_add_strobe; --used for extracting the trigger ID from the long b-channel commands
            --TTDDDDDEB
            brc_t2 <= TTCEmu_TTCout(9 downto 8);
            brc_d4 <= TTCEmu_TTCout(7 downto 4);
            brc_ei <= TTCEmu_TTCout(3); --ECR
            brc_bi <= TTCEmu_TTCout(2); --BCR
            --AAAAAAAAAAAAAAE1SSSSSSSSDDDDDDDD
            add_a14 <= "00000000000000"; --not used
            add_e   <= TTCEmu_add_e;  --used for extracting the trigger ID from the long b-channel commands
            add_s8  <= TTCEmu_add_s8; --used for extracting the trigger ID from the long b-channel commands
            add_d8  <= TTCEMu_add_d8;  --used for extracting the trigger ID from the long b-channel commands
            --== ttc decoder aux flags ==--
            --ready <='1';
            div_nrst <= '1'; --1 when the TTC bistream is aligned and recognized.
            channelB <= TTCEmu_TTCout(1);
        end if;
    end process;

    TTC_DELAY <= register_map_control.TTC_DELAY;


    ExtendingTestPulse: entity work.ExtendedTestPulse (Behavioral)
        port map(
            clk40                   => clk40,
            reset                 => rst_TTCtoHost,

            TTCin_TestPulse        => TTC_out_delayed(4),
            ExtendedTestPulse    => ExtendedTestPulse
        );


    TTC_out.L0A <= xTTCin_delayed(0);
    TTC_out.Bchan <= xTTCin_delayed(1);
    TTC_out.BCR <= xTTCin_delayed(2);
    TTC_out.ECR <= xTTCin_delayed(3);
    TTC_out.Brcst <= xTTCin_delayed(9 downto 4);
    TTC_out.Brcst_latched <= xTTCin_delayed(15 downto 10);
    TTC_out.ExtendedTestPulse <= xTTCin_delayed(16);
    TTC_out.L1Id <= L1ID;
    TTC_out.ITk_sync <= xTTCin_delayed(17);
    TTC_out.ITk_trig <= xTTCin_delayed(21 downto 18);
    TTC_out.ITk_tag <= xTTCin_delayed(28 downto 22);
    TTC_out.L1A <= l0a_shiftreg(to_integer(unsigned(register_map_control.TTC_L1A_DELAY)));

    TTC_out.BCID <= BCID;
    TTC_out.TriggerType <= trigger_type;
    TTC_out.LBID <= (others => '0');
    TTC_out.OrbitID <= orbit;
    TTC_out.L0ID(37 downto 0) <= "000000" & XL1ID & L1ID;
    TTC_out.SyncGlobalData <= (others => '0');
    TTC_out.TS <= '0';
    TTC_out.ErrorFlags   <= (others => '0');
    TTC_out.SL0ID        <= '0';
    TTC_out.SOrb         <= '0';
    TTC_out.Sync         <= '0';
    TTC_out.GRst         <= '0';
    TTC_out.PT           <= '0';
    TTC_out.Partition    <= (others => '0');
    TTC_out.SyncUserData <= (others => '0');
    TTC_out.AsyncUserData <= AsyncUserData;

    --Single shot write enable into fifo on the register map clock
    process(appreg_clk)
    begin
        if rising_edge(appreg_clk) then
            TTC_ASYNCUSERDATA_WR_EN_p1 <= register_map_control_appreg_clk.TTC_ASYNCUSERDATA.WR_EN(64);
        end if;
    end process;

    AsyncUserData_wr_en <= not AsyncUserData_full and (register_map_control_appreg_clk.TTC_ASYNCUSERDATA.WR_EN(64) and (not TTC_ASYNCUSERDATA_WR_EN_p1));

    fifo_AsyncUserData: xpm_fifo_async generic map(
            FIFO_MEMORY_TYPE => "auto",
            FIFO_WRITE_DEPTH => 16,
            CASCADE_HEIGHT => 0,
            RELATED_CLOCKS => 0,
            WRITE_DATA_WIDTH => 64,
            READ_MODE => "fwft",
            FIFO_READ_LATENCY => 0,
            FULL_RESET_VALUE => 1,
            USE_ADV_FEATURES => "0000",
            READ_DATA_WIDTH => 64,
            CDC_SYNC_STAGES => 2,
            WR_DATA_COUNT_WIDTH => 4,
            PROG_FULL_THRESH => 12,
            RD_DATA_COUNT_WIDTH => 4,
            PROG_EMPTY_THRESH => 2,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            SIM_ASSERT_CHK => 0,
            WAKEUP_TIME => 0
        )
        port map(
            sleep => '0',
            rst => rst_TTCtoHost_40,
            wr_clk => appreg_clk,
            wr_en => AsyncUserData_wr_en,
            din => register_map_control_appreg_clk.TTC_ASYNCUSERDATA.DATA,
            full => AsyncUserData_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => clk40,
            rd_en => AsyncUserData_rd_en,
            dout => AsyncUserData_dout,
            empty => AsyncUserData_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    AsyncUserData_proc: process(AsyncUserData_empty, AsyncUserData_dout, TTC_out, TTC_out_p1)
        variable frame_type: std_logic;
    begin
        if (
            TTC_out.SL0ID = '1' or
            TTC_out.SOrb = '1' or
            TTC_out.Sync = '1' or
            TTC_out.GRst = '1' or
            TTC_out.L0A = '1' or
            TTC_out.L0ID /= TTC_out_p1.L0ID or
            TTC_out.OrbitID /= TTC_out_p1.OrbitID or
            TTC_out.TriggerType /= TTC_out_p1.TriggerType or
            TTC_out.LBID /= TTC_out_p1.LBID
        ) then
            frame_type := '0';
        else
            frame_type := '1';
        end if;
        if frame_type = '1' and AsyncUserData_empty = '0' then
            AsyncUserData <= AsyncUserData_dout;
            AsyncUserData_rd_en <= '1';
        else
            AsyncUserData <= x"0000_0000_0000_0000";
            AsyncUserData_rd_en <= '0';
        end if;
    end process;

    AsyncUserData_pipeline: process(clk40)
    begin
        if rising_edge(clk40) then
            TTC_out_p1 <= TTC_out;
        end if;
    end process;



    l0a_shift_proc: process(clk40)
    begin
        if rising_edge(clk40) then
            l0a_shiftreg(63 downto 1) <= l0a_shiftreg(62 downto 0); --FLX-2069: Make a shift register with a delayed version of L1A to feed into TTC_out(16)
        end if;
    end process;

    l0a_shiftreg(0) <= xTTCin_delayed(0);

    ttcn: process(clk40)
    begin
        if clk40'event and clk40 = '1' then
            xTTCin(28 downto 17) <= TTC_Out_sync(27 downto 16);
            xTTCin(15 downto 2) <= TTC_out_delayed(15 downto 2);
            xTTCin(1 downto 0) <= TTC_Out_sync(1 downto 0);
        end if;
    end process;

    xTTCin(16)    <= ExtendedTestPulse; -- the ExtendedTestPulse already set in one clock delay, like in the "ttcn" process above

    -- If you want to generate a per-GBT (per front end) delay, that
    --  can be done here, but only in steps of the 40MHz clock.
    --
    --  If used connect DlydTTCin_array to upstream instead of TTCin_array.

    --IG ttcFanDly : for J in 0 to 9 generate
    ttcFanDly : for J in 0 to 28 generate --IG: add another bit to support the extended Test Pulse
        TTC_DELAY_SRL : SRL16E
            generic map(
                INIT => x"0000",
                IS_CLK_INVERTED => '0'
            )
            port map (
                Q => xTTCin_delayed(J), --delayed copy of TTCin
                A0 => TTC_DELAY(0), --input from control register
                A1 => TTC_DELAY(1), --input from control register
                A2 => TTC_DELAY(2), --input from control register
                A3 => TTC_DELAY(3), --input from control register
                ce => '1', --delay_en,     --input from control register
                clk => clk40,
                D => xTTCin(J) --latched copy of TTCin
            );
    end generate ttcFanDly;

--ila_testbeam_i : ila_testbeam
--PORT MAP (
--  clk => clk40,
--  probe0(0) => tb_trigger_sync_i,
--  probe1(0) => testbeam_trigger_i,
--        probe2(0) => tb_state_ila,
--        probe3    => count_clk40, --4b
--        probe4    => second_trigger_delay_ila
--);
--


end top;
