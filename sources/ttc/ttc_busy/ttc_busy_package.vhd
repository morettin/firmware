--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Soo Ryu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-- Author:   John Anderson
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  Library Declaration
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;      -- needed for logical operations
--use IEEE.std_logic_arith.all;     -- needed for +/- operations
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;

-- pragma translate_off
library UNISIM;
use unisim.all;
use unisim.vcomponents.all;
-- pragma translate_on
Library XilinxCoreLib;

--==========================================================================
-- This is a package definition file for custom data types
--==========================================================================

package FELIX_or_pkg is

	constant NUMBER_OF_GBT : integer := 4;		--# of GBT links in current build of FELIX


	type BUSY_ARR is array(integer range <>) of std_logic_vector(56 downto 0);

end package FELIX_or_pkg;

package body FELIX_or_pkg is
end FELIX_or_pkg;


