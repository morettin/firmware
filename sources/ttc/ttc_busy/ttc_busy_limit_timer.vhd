--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Soo Ryu
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

-------------------------------------------------------------------------------
--  Argonne National Laboratory
-------------------------------------------------------------------------------
--  Filename: FELIX_busy_limit_timer.vhd
--  Title: clock prescaler, adapted from Digital Gammasphere code.

--	Generates pre-scaled clock enable for busy_limit logic instantiations
--	to reduce counter sizes in elements replicated many times.
--
-------------------------------------------------------------------------------
-- Author:   John Anderson
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  Library Declaration
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;      -- needed for logical operations
use IEEE.std_logic_arith.all;     -- needed for +/- operations
use IEEE.std_logic_unsigned.all;

-- pragma translate_off
library UNISIM;
use unisim.all;
-- pragma translate_on


-------------------------------------------------------------------------------
--  PORT DECLARATION
-------------------------------------------------------------------------------

entity busy_limit_timer is
port (
		mclk	:	in std_logic;	--master clock (presumably 40-ish MHz in FELIX)
		mrst	:	in std_logic;	--global reset
		PRESCALE_VALUE : in std_logic_vector(19 downto 0);
		PRESCALED_CLOCK_ENABLE : out std_logic	--clock enable from external process (so can be shared by multiple copies of this)
												--one tick of mclk wide at lesser rate, used to sample BUSY_REQ.
   );
end busy_limit_timer;

architecture rtl of busy_limit_timer is

signal LIMIT_TIMER : std_logic_vector(19 downto 0);
signal LIMIT_TIMER_FLAG : std_logic;

begin

PRESCALED_CLOCK_ENABLE <= LIMIT_TIMER_FLAG;
-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------
--
--	Global timer for limit logic state machines.
--  Uses a 10-bit counter based on ~40Mhz, so the max period is
--  (25ns * 1024) = 25,600ns, or 25.6us.  The second-rank counter
--  LIMIT_TIMER2 is 10 bits at the 25.6us period, so max period is
--  every 26.214 milliseconds.
LIMIT_TIMER_PROC : process(mrst, mclk, PRESCALE_VALUE)
begin
	if (mrst = '1') then
		LIMIT_TIMER_FLAG <= '0';
	elsif (mclk'event and (mclk = '1')) then
		LIMIT_TIMER <= LIMIT_TIMER - 1;
		if (LIMIT_TIMER = "00000000000000000000") then
			LIMIT_TIMER <= PRESCALE_VALUE;
			LIMIT_TIMER_FLAG <= '1';
		else
			LIMIT_TIMER_FLAG <= '0';
		end if;
	end if;
end process;


end rtl;
