--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.


library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use ieee.numeric_std_unsigned.all;
    use work.centralRouter_package.all;
    use work.axi_stream_package.all;
Library xpm;
    use xpm.vcomponents.all;

entity TTCToHostVirtualElink is
    generic (
        BLOCKSIZE : integer;
        VERSAL : boolean
    );
    port (
        clk40 : in std_logic;
        aresetn : in std_logic;
        TTC_ToHost_Data_in  : in TTC_ToHost_data_type;
        Enable : in std_logic; --Virtual e-link enable.
        m_axis : out axis_32_type;
        m_axis_prog_empty : out std_logic;
        m_axis_tready : in std_logic;
        m_axis_aclk : in std_logic
    );
end TTCToHostVirtualElink;

architecture rtl of TTCToHostVirtualElink is

    signal s_axis: axis_32_type;
    signal s_axis_tready : std_logic;
    signal rst : std_logic;
    signal xpmfifo0_dout, xpmfifo0_din : std_logic_vector(215 downto 0);
    signal xpmfifo0_empty : std_logic;
    signal xpmfifo0_full : std_logic;
    signal xpmfifo0_rd_en : std_logic;
    signal xpmfifo0_wr_en : std_logic;
    signal m_axis_s : axis_32_type;
    signal m_axis_prog_empty_s : std_logic;
    signal Enable_aclk : std_logic;

    signal trunc, trunc_aclk: std_logic;

    --- L1A counter --
    --- SWROD should check the TTCToHost information for the absence of L1Ids.
    --- (each L1A is 1 higher than the preceding L1A)
    signal l1a_counter          : std_logic_vector(47 downto 0) := (others => '0');
    signal aresetn_m_axis_aclk : std_logic;

begin

    ----------------------------------------------------------------------
    --- Counter for L1A (no-reset upon ECR)
    ----------------------------------------------------------------------
    l1acnt_proc: process(clk40)
        variable data_rdy_p1: std_logic;
    begin
        if rising_edge(clk40) then
            if aresetn = '0' then
                l1a_counter  <= (others => '0');
                data_rdy_p1 := '0';
            elsif  data_rdy_p1 = '1' then
                l1a_counter <= l1a_counter + 1;
            else
                l1a_counter <= l1a_counter;
            end if;
            data_rdy_p1 := TTC_ToHost_Data_in.data_rdy; --Delay data_rdy one clock, because data_rdy is also delayed one clock (but combined with fifo full, so can't be used here).
        end if;
    end process;

    pipeline: process(clk40, aresetn)
    begin
        if aresetn = '0' then
            xpmfifo0_din <= (others => '0');
            xpmfifo0_wr_en <= '0';
            trunc <= '0';
        elsif rising_edge(clk40) then

            xpmfifo0_din <= TTC_ToHost_Data_in.TAG & -- 8 bit
                            l1a_counter &
                            TTC_ToHost_Data_in.L0ID & -- 32 bit
                            TTC_ToHost_Data_in.reserved1 &  -- 16 bit
                            TTC_ToHost_Data_in.trigger_type & -- 16 bit
                            TTC_ToHost_Data_in.orbit & -- 32 bit
                            TTC_ToHost_Data_in.XL1ID & -- 8 bit
                            TTC_ToHost_Data_in.L1ID & -- 24 bit
                            TTC_ToHost_Data_in.reserved0 & -- 4 bit
                            TTC_ToHost_Data_in.BCID & -- 12 bit
                            TTC_ToHost_Data_in.LEN & -- 8 bit
                            TTC_ToHost_Data_in.FMT; -- 8 bit;

            xpmfifo0_wr_en <= TTC_ToHost_Data_in.data_rdy and not xpmfifo0_full and Enable;
            trunc <= '0';
            if TTC_ToHost_Data_in.data_rdy = '1' and xpmfifo0_full = '1' then
                trunc <= '1';
            end if;
        end if;
    end process;

    xpmfifo0 : xpm_fifo_async
        generic map ( -- @suppress "Generic map uses default values. Missing optional actuals: CASCADE_HEIGHT, SIM_ASSERT_CHK"
            FIFO_MEMORY_TYPE => "distributed",
            FIFO_WRITE_DEPTH => 16,
            RELATED_CLOCKS => 0,
            WRITE_DATA_WIDTH => 216,
            READ_MODE => "std",
            FIFO_READ_LATENCY => 1,
            FULL_RESET_VALUE => 1,
            USE_ADV_FEATURES => "0000",
            READ_DATA_WIDTH => 216,
            CDC_SYNC_STAGES => 2,
            WR_DATA_COUNT_WIDTH => 1,
            PROG_FULL_THRESH => 10,
            RD_DATA_COUNT_WIDTH => 1,
            PROG_EMPTY_THRESH => 10,
            DOUT_RESET_VALUE => "0",
            ECC_MODE => "no_ecc",
            WAKEUP_TIME => 0
        )
        port map (
            sleep => '0',
            rst => rst,
            wr_clk => clk40,
            wr_en => xpmfifo0_wr_en,
            din => xpmfifo0_din,
            full => xpmfifo0_full,
            prog_full => open,
            wr_data_count => open,
            overflow => open,
            wr_rst_busy => open,
            almost_full => open,
            wr_ack => open,
            rd_clk => m_axis_aclk,
            rd_en => xpmfifo0_rd_en,
            dout => xpmfifo0_dout,
            empty => xpmfifo0_empty,
            prog_empty => open,
            rd_data_count => open,
            underflow => open,
            rd_rst_busy => open,
            almost_empty => open,
            data_valid => open,
            injectsbiterr => '0',
            injectdbiterr => '0',
            sbiterr => open,
            dbiterr => open
        );

    rst <= not aresetn;

    sync_aresetn : xpm_cdc_sync_rst
        generic map (
            DEST_SYNC_FF => 2,
            INIT => 0,
            INIT_SYNC_FF => 0,
            SIM_ASSERT_CHK => 0
        )
        port map (
            src_rst => aresetn,
            dest_clk => m_axis_aclk,
            dest_rst => aresetn_m_axis_aclk
        );

    toaxis: process(m_axis_aclk, aresetn_m_axis_aclk)
        variable PacketCnt : integer range 0 to 6;
        variable Trig: std_logic;
        variable trunc_v : std_logic;
        variable fifo_data_valid: std_logic;
    begin
        if aresetn_m_axis_aclk = '0' then
            Trig := '0';
            s_axis.tdata <= (others => '0');
            s_axis.tuser <= (others => '0');
            s_axis.tkeep <= "0000";
            s_axis.tvalid <= '0';
            s_axis.tlast <= '0';
            xpmfifo0_rd_en <= '0';
            trunc_v := '0';
            PacketCnt := 0;
        elsif rising_edge(m_axis_aclk) then
            s_axis.tdata <= (others => '0');
            s_axis.tuser <= (others => '0');
            s_axis.tkeep <= "0000";
            s_axis.tvalid <= '0';
            s_axis.tlast <= '0';
            xpmfifo0_rd_en <= '0';

            if trunc_aclk = '1' then
                trunc_v := '1';
            end if;
            if Trig = '0' then
                if xpmfifo0_empty = '0' then
                    xpmfifo0_rd_en <= '1';
                    Trig := '1';
                end if;
                PacketCnt := 0;
            else

                if fifo_data_valid = '1' then
                    if s_axis_tready = '1' then
                        if PacketCnt /= 6 then  --
                            s_axis.tdata <= xpmfifo0_dout(PacketCnt*32+31 downto PacketCnt*32);

                            s_axis.tuser <= (others => '0');
                            s_axis.tkeep <= "1111";
                            s_axis.tvalid <= '1';
                            s_axis.tlast <= '0';
                            PacketCnt := PacketCnt + 1;
                        else
                            Trig := '0';
                            fifo_data_valid := '0';
                            s_axis.tdata <= x"00" & xpmfifo0_dout(215 downto 192);
                            s_axis.tuser <= trunc_v & "000";
                            trunc_v := '0';
                            s_axis.tkeep <= "0111";
                            s_axis.tvalid <= '1';
                            s_axis.tlast <= '1';
                            PacketCnt := 0;
                        end if;
                    end if;
                end if;
                fifo_data_valid := Trig;
            end if;
        end if;
    end process;

    axis32fifo0: entity work.Axis32Fifo
        generic map(
            DEPTH => BLOCKSIZE/2,
            --CLOCKING_MODE => "common_clock",
            --RELATED_CLOCKS => 0,
            --FIFO_MEMORY_TYPE => "auto",
            --PACKET_FIFO => "false",
            USE_BUILT_IN_FIFO => '0',
            VERSAL => VERSAL,
            BLOCKSIZE => BLOCKSIZE,
            ISPIXEL => false
        )
        port map(
            -- axi stream slave
            s_axis_aresetn => aresetn_m_axis_aclk,
            s_axis_aclk => m_axis_aclk,
            s_axis => s_axis,
            s_axis_tready => s_axis_tready,

            -- axi stream master
            m_axis_aclk => m_axis_aclk,
            m_axis => m_axis_s,
            m_axis_tready => m_axis_tready,

            --Indication that the FIFO contains a block of data (for MUX).
            m_axis_prog_empty => m_axis_prog_empty_s
        );

    sync_enable: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 1,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => clk40,
            src_in => Enable,
            dest_clk => m_axis_aclk,
            dest_out => Enable_aclk
        );

    sync_trunc: xpm_cdc_single
        generic map(
            DEST_SYNC_FF => 2,
            INIT_SYNC_FF => 1,
            SIM_ASSERT_CHK => 0,
            SRC_INPUT_REG => 0
        )
        port map(
            src_clk => clk40,
            src_in => trunc,
            dest_clk => m_axis_aclk,
            dest_out => trunc_aclk
        );

    process(m_axis_s , m_axis_prog_empty_s, Enable_aclk)
    begin
        m_axis <= m_axis_s;
        m_axis_prog_empty <= m_axis_prog_empty_s;
        if Enable_aclk = '0' then
            m_axis.tvalid <= '0';
            m_axis_prog_empty <= '1';
        end if;
    end process;



end rtl;
