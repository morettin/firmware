--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Frans Schreuder
--! 
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/18/2015 06:18:50 PM
-- Design Name: 
-- Module Name: ttc_fmc_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity ttc_fmc_top is
Port ( 
  
  --CDCE_PD           : out std_logic;    -- Power down jitter clean ; H38  
  --CDCE_REF_SEL      : out std_logic;    -- Input select for jitter clean ; B34
  --CDCE_SYNC         : out std_logic;    -- SYNCH out CLK jitter clean ; A34 
  
  -- spi interface
  --SPI_MISO : in std_logic;--F36
  --SPI_LE : out std_logic;  --C38 
  --SPI_CLK : out std_logic; --C39
  --SPI_MOSI : out std_logic; --F37 
  
  --PLL_LOCK : in std_logic; -- Jitter clean PLL Lock Status; G38
  
  --CLK_REF_OUT_P : out std_logic; -- CLK to jitter clean;  D35
  --CLK_REF_OUT_N : out std_logic; -- CLK to jitter clean;  D36
  --CLK_REF0_P    : in std_logic; -- C35
  --CLK_REF0_N    : in std_logic; -- C36
  --CLK_REF1_P    : in std_logic; -- L31 
  --CLK_REF1_N    : in std_logic; -- K32
  --CLK_REF2_P    : in std_logic; -- AD32
  --CLK_REF2_N    : in std_logic; -- AD33
  
  -- i2c interface
  --SCL  : out std_logic;    --Board I2C SWITCH
  --SDA  : inout std_logic;  --Board I2C SWITCH
  
  CLK_TTC_P  : in std_logic;  -- J25
  CLK_TTC_N  : in std_logic;  -- J26
  DATA_TTC_P : in std_logic; -- G28
  DATA_TTC_N : in std_logic; -- G29
    
  BUSY : out std_logic;  --G32
  --BUSY_IN : in std_logic;  -- from somewhere ?
 
  -- interface to ELINK 
  --GBT_FRAME : out std_logic_vector(83 downto 0);
  
  -- system clk
  --SYSCLK_P            : in std_logic;  --AU38
  --SYSCLK_N            : in std_logic;  --AV38
  SYSRESET_SW1        : in std_logic  -- SW1
);
end ttc_fmc_top;

architecture Behavioral of ttc_fmc_top is
  signal cdrclk : std_logic;
  signal cdrdata : std_logic;

  --- interface to central router
  signal TTC_out     : std_logic_vector(9 downto 0);
  
  --signal divider_rst_b : std_logic;
  --signal cdrclk_locked : std_logic;
  --signal single_bit_error   : std_logic;
  --signal double_bit_error   : std_logic;
  ---- for ELINK
  --signal GBT_FRAME_OUT      : std_logic_vector(83 downto 0);

  
  --signal SYSCLK_i, SYSCLK   : std_logic;
  --signal CLK_TO_JC, CLK10MHz    : std_logic;
  --signal CLK_REF0, CLK_REF1, CLK_REF2   : std_logic;
  
  --signal DUMMY              : std_logic;
  
  --signal reg_spi_command    : std_logic_vector(31 downto 0);
  --signal reg_spi_txdata     : std_logic_vector(31 downto 0);
  --signal reg_spi_rxdata     : std_logic_vector(31 downto 0);
 
  ----==========================---  
  ---- for jitter cleaning
  ----==========================---
  --component clk_wiz_100to160 is
  --port (
  --  clk_in1           : in     std_logic;
  --  clk_out1          : out    std_logic;
  --  clk_out2          : out    std_logic;
  --  reset             : in     std_logic;
  --  locked            : out    std_logic );
  --end component;

begin
  -- ttc clk
  ibuf_ttc_clk : ibufds 
  port map (I=> CLK_TTC_P, IB=> CLK_TTC_N, O=> cdrclk);
  
  -- ttc data
  ibuf_ttc_data : ibufds 
  port map (I=> DATA_TTC_P, IB=> DATA_TTC_N, O=> cdrdata);
  
  --ibuf_CLK_REF0 : ibufds 
  --port map (I=> CLK_REF0_P, IB=> CLK_REF0_N, O=> CLK_REF0);
  
  --ibuf_CLK_REF1 : ibufds 
  --port map (I=> CLK_REF1_P, IB=> CLK_REF1_N, O=> CLK_REF1);
    
  --ibuf_CLK_REF2 : ibufds 
  --port map (I=> CLK_REF2_P, IB=> CLK_REF2_N, O=> CLK_REF2);
  
  
  ---- system reference clock : 100 MHz
  --sysclk_inst : ibufds 
  --generic map (DIFF_TERM => TRUE, IOSTANDARD => "DEFAULT")
  --port map (O=> SYSCLK_i, I => SYSCLK_P , IB => SYSCLK_N);
  --bufg_ref : bufg port map (I => SYSCLK_i, O => SYSCLK);
     
  ------ FOR jitter cleaning at TTCfx
  --clk160_gen: clk_wiz_100to160
  --port map  ( clk_in1=> SYSCLK, clk_out1 => CLK_TO_JC, clk_out2 => CLK10MHz, reset => SYSRESET_SW1, locked => DUMMY);
 
  --OBUFDS_inst : OBUFDS
  --generic map (IOSTANDARD => "DEFAULT")
  --port map (O => CLK_REF_OUT_P, OB => CLK_REF_OUT_N, I => CLK_TO_JC);


   --===================================--
  -- TTCfx Decoder   
   --===================================--
  ttcrx: entity work.ttc_fmc_wrapper
  generic map ( ELINK_OPTION => "000" )	
  port map (
     cdrclk_in    => cdrclk,     -- TTCFx
     cdrdata_in   => cdrdata,    -- TTCFx
     RESET => SYSRESET_SW1,
     BUSY => BUSY,
     TTC_out => TTC_out
  );

                   
   ----===================================--
   --spi: entity work.spi_master
   ----===================================--
   --port map
   --(
   --    reset_i     => SYSRESET_SW1,   --reset,
   --    clk_i       => CLK10MHz,       --ipb_clk,
   --    ------------------------------------
   --    data_i      => reg_spi_txdata,
   --    enable_i    => reg_spi_command(31),
   --    ssdelay_i   => reg_spi_command(27 downto 18),
   --    hold_i      => reg_spi_command(17 downto 15),
   --    msbfirst_i  => reg_spi_command(14),
   --    cpha_i      => reg_spi_command(13),
   --    cpol_i      => reg_spi_command(12),
   --    prescaler_i => reg_spi_command(11 downto 0),
   --    data_o      => reg_spi_rxdata,
   --    ------------------------------------
   --    ss_b_o      => SPI_LE,
   --    sck_o       => SPI_CLK,
   --   mosi_o      => SPI_MOSI,
  --     miso_i      => SPI_MISO
  -- );
   
end Behavioral;
