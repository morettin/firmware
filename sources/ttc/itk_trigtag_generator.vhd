--! This file is part of the FELIX firmware distribution (https://gitlab.cern.ch/atlas-tdaq-felix/firmware/).
--! Copyright (C) 2001-2021 CERN for the benefit of the ATLAS collaboration.
--! Authors:
--!               Marius Wensing
--!
--!   Licensed under the Apache License, Version 2.0 (the "License");
--!   you may not use this file except in compliance with the License.
--!   You may obtain a copy of the License at
--!
--!       http://www.apache.org/licenses/LICENSE-2.0
--!
--!   Unless required by applicable law or agreed to in writing, software
--!   distributed under the License is distributed on an "AS IS" BASIS,
--!   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--!   See the License for the specific language governing permissions and
--!   limitations under the License.

library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;

entity itk_trigtag_generator is
    generic (
        MODE : integer := 0        -- 0: RD53A, 1: ITkPixV1, 2: ABC*
    );
    port (
        clk : in std_logic;        -- TTC clock
        rst : in std_logic;        -- reset (sync to TTC clock)

        -- to encoders
        enc_trig : out std_logic_vector(3 downto 0);
        enc_sync : out std_logic;
        enc_tag : out std_logic_vector(6 downto 0);

        -- to TTC2H
        ttc2h_tag : out std_logic_vector(7 downto 0);
        ttc2h_tag_valid : out std_logic;

        -- from TTC
        ttc_l0a : in std_logic;
        ttc_bcr : in std_logic;
        ttc_phase : in std_logic_vector(1 downto 0)
    );
end itk_trigtag_generator;

architecture rtl of itk_trigtag_generator is
    -- trigger shift register
    signal trig_sr : std_logic_vector(2 downto 0);
    signal trig_sr_cnt : integer range 0 to 3;
    signal enc_trig_i : std_logic_vector(3 downto 0);

    -- tag counter
    signal tag_cnt : integer range 0 to 127;
    signal tag_used : std_logic_vector(3 downto 0);
begin


    --Sasha: bcr is not perfectly periodic. This will cause enc_sync to pulse more oftern than once in four clock cycles and this will impact front-end ITkPix.
    --Sasha: synchronization to BCR not needed for alignment anyway.
    --    -- generate 4BC boundary and sync pulse
    --    process (clk) begin
    --        if rising_edge(clk) then
    --            if ttc_bcr = '1' then
    --                trig_sr_cnt <= to_integer(unsigned(ttc_phase));
    --            else
    --                if trig_sr_cnt = 3 then
    --                    trig_sr_cnt <= 0;
    --                else
    --                    trig_sr_cnt <= trig_sr_cnt + 1;
    --                end if;
    --            end if;
    --        end if;
    --    end process;

    process (clk) begin
        if rising_edge(clk) then
            if rst = '1' then
                trig_sr_cnt <= 1; --this is done to keep the enc_sync low during the reset
            else
                if trig_sr_cnt = 3 then
                    trig_sr_cnt <= 0;
                else
                    trig_sr_cnt <= trig_sr_cnt + 1;
                end if;
            end if;
        end if;
    end process;

    -- generate synchronous trigger words with 4BC boundary
    process (clk) begin
        if rising_edge(clk) then
            if rst = '1' then --sync reset
                trig_sr <= (others => '0');
                enc_trig_i <= (others => '0');
            elsif trig_sr_cnt = 3 then
                trig_sr <= (others => '0');
                enc_trig_i <= ttc_l0a & trig_sr;
            else
                trig_sr <= ttc_l0a & trig_sr(2 downto 1);
            end if;
        end if;
    end process;
    enc_sync <= '1' when trig_sr_cnt = 0 else '0';
    enc_trig <= enc_trig_i;

    -- generate tag
    process (clk) begin
        if rising_edge(clk) then
            if rst = '1' then
                tag_used <= "0000";
                tag_cnt <= 0;
            -- before we issue the next tag we have to make sure it will be unique (for Strips we will use a new tag everytime)
            elsif (trig_sr_cnt = 3) then
                if ((tag_used and (ttc_l0a & trig_sr)) /= "0000") or (MODE = 2) then
                    -- select next base tag
                    if ((tag_cnt = 31) and (MODE = 0)) or      -- RD53A
                   ((tag_cnt = 53) and (MODE = 1)) or        -- ITkPixV1
                   ((tag_cnt = 127) and (MODE = 2)) then    -- Strips
                        tag_cnt <= 0;
                    else
                        tag_cnt <= tag_cnt + 1;
                    end if;
                    tag_used <= ttc_l0a & trig_sr;
                else
                    -- we can reuse same base tag
                    tag_used <= tag_used or (ttc_l0a & trig_sr);
                end if;
            end if;
        end if;

    end process;
    enc_tag <= std_logic_vector(to_unsigned(tag_cnt, enc_tag'length));

    -- generate TTC2H tags
    --the trigger tages are 5 clocks after the input triggers
    process (clk) begin
        if rising_edge(clk) then
            if rst = '1' then
                ttc2h_tag <= (others => '0');
                ttc2h_tag_valid <= '0';
            elsif enc_trig_i(trig_sr_cnt) = '1' then
                if MODE = 2 then
                    ttc2h_tag <= std_logic_vector(to_unsigned(tag_cnt, ttc2h_tag'length));
                else
                    ttc2h_tag <= std_logic_vector(to_unsigned(tag_cnt, ttc2h_tag'length-2)) & std_logic_vector(to_unsigned(trig_sr_cnt, 2));
                end if;
                ttc2h_tag_valid <= '1';
            else
                ttc2h_tag <= (others => '0');
                ttc2h_tag_valid <= '0';
            end if;
        end if;
    end process;

end architecture;
